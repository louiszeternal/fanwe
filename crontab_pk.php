<?php
////定时任务,在java定时访问调用
header("Content-Type:text/html; charset=utf-8");
define("FANWE_REQUIRE", true);
require './system/mapi_init.php';

fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/common.php');
fanwe_require(APP_ROOT_PATH.'mapi/lib/base.action.php');
fanwe_require(APP_ROOT_PATH.'mapi/lib/video.action.php');
fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/BaseRedisService.php');
fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserRedisService.php');
fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoRedisService.php');

$userRedis = new UserRedisService();
$video_redis = new VideoRedisService();


//数据整理
$m_config =  load_auto_cache("m_config");
$pk_punish_time = intval($m_config['punish_time']) ? intval($m_config['punish_time']) : 1;
$pk_punish_time = $pk_punish_time * 60 + 1;
$lianmai = $GLOBALS['db']->getAll("SELECT id,form_user_ticket, form_user_id, form_video_id, to_user_id, to_video_id, to_user_ticket,start_time,stop_time,lianmai_time,is_update,end_time FROM ".DB_PREFIX."video_lianmai_pk WHERE end_time = 0");
$millisecond = getMillisecond();
foreach ($lianmai as $v) {
	if ($v['lianmai_time']*60 + $v['start_time'] + $pk_punish_time <= NOW_TIME) {
		$video = new videoModule();
		$video->crontab_lianmai_pk_stop($v['form_video_id']);
	}
	$form_vote_number = $video_redis->getOne_db($v['form_video_id'],'vote_number');
	$to_vote_number = $video_redis->getOne_db($v['to_video_id'],'vote_number');
	if ($v['lianmai_time']*60 + $v['start_time'] >= NOW_TIME && $v['stop_time'] == 0) {

		$form_user = $userRedis->getRow_db($v['form_user_id'],array('ticket','nick_name','head_image','live_in'));
		$to_user = $userRedis->getRow_db($v['to_user_id'],array('ticket','nick_name','head_image','live_in'));
		$user_id = $v['form_user_id'];
		$time = intval($v['lianmai_time']*60 + $v['start_time'] - NOW_TIME);
		//消息整理
		$ext = array();

		$ext['type'] = 95; // 95
		$ext['timestamp'] = $millisecond;
		$pk['from_room_id'] = $v['form_video_id'];
		$pk['from_user_id'] = $v['form_user_id'];
		$pk['from_nick_name'] = $form_user['nick_name'];
		$pk['from_head_image'] = get_spec_image($form_user['head_image']);
		$pk['from_ticket'] = $form_vote_number - $v['form_user_ticket'];
		$pk['to_room_id'] = $v['to_video_id'];
		$pk['to_user_id'] = $v['to_user_id'];
		$pk['to_nick_name'] = $to_user['nick_name'];
		$pk['to_head_image'] = get_spec_image($to_user['head_image']);
		$pk['to_ticket'] = $to_vote_number - $v['to_user_ticket'];
		$pk['time'] = $time;
		$pk['total_time'] = $v['lianmai_time']*60;

		$pk['from_user_info'] = [];
		$pk['from_user_info']['room_id'] = $v['form_video_id'];
		$pk['from_user_info']['user_id'] = $v['form_user_id'];
		$pk['from_user_info']['nick_name'] = $form_user['nick_name'];
		$pk['from_user_info']['head_image'] = get_spec_image($form_user['head_image']);
		$pk['from_user_info']['ticket'] = $form_vote_number - $v['form_user_ticket'];
		$pk['to_user_info'] = [];
		$pk['to_user_info']['room_id'] = $v['to_video_id'];
		$pk['to_user_info']['user_id'] = $v['to_user_id'];
		$pk['to_user_info']['nick_name'] = $to_user['nick_name'];
		$pk['to_user_info']['head_image'] = get_spec_image($to_user['head_image']);
		$pk['to_user_info']['ticket'] = $to_vote_number - $v['to_user_ticket'];

		$ext['data'] = $pk;

		#构造高级接口所需参数
		$msg_content = array();
		//创建array 所需元素
		$msg_content_elem = array(
			'MsgType' => 'TIMCustomElem',       //自定义类型
			'MsgContent' => array(
				'Data' => json_encode($ext),
				'Desc' => '',
			)
		);
		array_push($msg_content, $msg_content_elem);
		fanwe_require(APP_ROOT_PATH.'system/tim/TimApi.php');
		$api = createTimAPI();
		$ret = $api->group_send_group_msg2($user_id, $pk['from_room_id'], $msg_content);
		if ($ret['ActionStatus'] == 'FAIL' || $ret['ErrorCode'] == 10002) {
			//10002 系统错误，请再次尝试或联系技术客服。
			log_err_file(array(__FILE__, __LINE__, __METHOD__, $ret));
			$api->group_send_group_msg2($user_id, $pk['from_room_id'], $msg_content);
		}

		$ext = array();
		$ext['type'] = 95; // 95
		$ext['timestamp'] = $millisecond;
		$pk['from_room_id'] = $v['form_video_id'];
		$pk['from_user_id'] = $v['form_user_id'];
		$pk['from_nick_name'] = $form_user['nick_name'];
		$pk['from_head_image'] = get_spec_image($form_user['head_image']);
		$pk['from_ticket'] = $form_vote_number - $v['form_user_ticket'];
		$pk['to_room_id'] = $v['to_video_id'];
		$pk['to_user_id'] = $v['to_user_id'];
		$pk['to_nick_name'] = $to_user['nick_name'];
		$pk['to_head_image'] = get_spec_image($to_user['head_image']);
		$pk['to_ticket'] = $to_vote_number - $v['to_user_ticket'];
		$pk['time'] = $time;
		$pk['total_time'] = $v['lianmai_time']*60;

		$pk['from_user_info'] = [];
		$pk['from_user_info']['room_id'] = $v['form_video_id'];
		$pk['from_user_info']['user_id'] = $v['form_user_id'];
		$pk['from_user_info']['nick_name'] = $form_user['nick_name'];
		$pk['from_user_info']['head_image'] = get_spec_image($form_user['head_image']);
		$pk['from_user_info']['ticket'] = $form_vote_number - $v['form_user_ticket'];
		$pk['to_user_info'] = [];
		$pk['to_user_info']['room_id'] = $v['to_video_id'];
		$pk['to_user_info']['user_id'] = $v['to_user_id'];
		$pk['to_user_info']['nick_name'] = $to_user['nick_name'];
		$pk['to_user_info']['head_image'] = get_spec_image($to_user['head_image']);
		$pk['to_user_info']['ticket'] = $to_vote_number - $v['to_user_ticket'];

		$ext['data'] = $pk;
		#构造高级接口所需参数
		$msg_content = array();
		//创建array 所需元素
		$msg_content_elem = array(
			'MsgType' => 'TIMCustomElem',       //自定义类型
			'MsgContent' => array(
				'Data' => json_encode($ext),
				'Desc' => '',
			)
		);
		//将创建的元素$msg_content_elem, 加入array $msg_content
		array_push($msg_content, $msg_content_elem);
		$ret = $api->group_send_group_msg2($user_id, $pk['to_room_id'], $msg_content);
		if ($ret['ActionStatus'] == 'FAIL' || $ret['ErrorCode'] == 10002) {
			//10002 系统错误，请再次尝试或联系技术客服。
			log_err_file(array(__FILE__, __LINE__, __METHOD__, $ret));
			$api->group_send_group_msg2($user_id, $pk['to_room_id'], $msg_content);
		}
		} else {
		if ($v['end_time'] == 0) {
			$pInTrans = $GLOBALS['db']->StartTrans();
			try{
				if (true) {
					$user_id = $v['form_user_id'];
					//消息整理
					$ext = array();
					$form_user = $userRedis->getRow_db($v['form_user_id'],array('ticket','nick_name','head_image'));
					$to_user = $userRedis->getRow_db($v['to_user_id'],array('ticket','nick_name','head_image'));
					$form_ticket = $form_vote_number - $v['form_user_ticket'];
					$to_ticket = $to_vote_number - $v['to_user_ticket'];
					if ($v['is_update'] == 0 && $v['stop_time'] == 0) {
						$GLOBALS['db']->query("update ".DB_PREFIX."video_lianmai_pk set stop_time = ".NOW_TIME.",is_update =1,form_user_ticket = ".$form_ticket.",to_user_ticket = ".$to_ticket." where end_time = 0 and stop_time = 0 and is_update = 0 and id =".$v['id']);
					}
					$get_ticket = $GLOBALS['db']->getRow("select form_user_ticket,to_user_ticket from ".DB_PREFIX."video_lianmai_pk where id =".$v['id']."");
					$form_ticket = intval($get_ticket['form_user_ticket']);
					$to_ticket = intval($get_ticket['to_user_ticket']);

					if ($form_ticket > $to_ticket) {
						$form_result = 1;
						$form_desc = '胜利';
					} elseif ($form_ticket == $to_ticket) {
						$form_result = 2;
						$form_desc = '平局';
					} elseif ($form_ticket < $to_ticket) {
						$form_result = 0;
						$form_desc = '失败';
					}
					if ($to_ticket > $form_ticket) {
						$to_result = 1;
						$to_desc = '胜利';
						
					} elseif ($to_ticket == $form_ticket) {
						$to_result = 2;
						$to_desc = '平局';
						
					} elseif ($to_ticket < $form_ticket) {
						$to_result = 0;
						$to_desc = '失败';
						
					}
					$punish_time = intval($v['lianmai_time']*60 + $v['start_time'] + $pk_punish_time - NOW_TIME);
					$ext['type'] = 96;
					$ext['timestamp'] = $millisecond;
					$pk['from_room_id'] = $v['form_video_id'];
					$pk['from_user_id'] = $v['form_user_id'];
					$pk['from_nick_name'] = $form_user['nick_name'];
					$pk['from_head_image'] = get_spec_image($form_user['head_image']);
					$pk['from_ticket'] = $form_ticket;
					$pk['from_result'] = $form_result;
					$pk['to_room_id'] = $v['to_video_id'];
					$pk['to_user_id'] = $v['to_user_id'];
					$pk['to_nick_name'] = $to_user['nick_name'];
					$pk['to_head_image'] = get_spec_image($to_user['head_image']);
					$pk['to_ticket'] = $to_ticket;
					$pk['to_result'] = $to_result;
					$pk['desc'] = $form_desc;
					$pk['punish_time'] = $punish_time > 0 ? $punish_time : '';

					$pk['from_user_info'] = [];
					$pk['from_user_info']['room_id'] = $v['form_video_id'];
					$pk['from_user_info']['user_id'] = $v['form_user_id'];
					$pk['from_user_info']['nick_name'] = $form_user['nick_name'];
					$pk['from_user_info']['head_image'] = get_spec_image($form_user['head_image']);
					$pk['from_user_info']['ticket'] = $form_ticket;
					$pk['from_user_info']['result'] = $form_result;
					$pk['to_user_info'] = [];
					$pk['to_user_info']['room_id'] = $v['to_video_id'];
					$pk['to_user_info']['user_id'] = $v['to_user_id'];
					$pk['to_user_info']['nick_name'] = $to_user['nick_name'];
					$pk['to_user_info']['head_image'] = get_spec_image($to_user['head_image']);
					$pk['to_user_info']['ticket'] = $to_ticket;
					$pk['to_user_info']['result'] = $to_result;

					$ext['data'] = $pk;
					$msg_content = array();
					$msg_content_elem = array(
						'MsgType' => 'TIMCustomElem',       //自定义类型
						'MsgContent' => array(
							'Data' => json_encode($ext),
							'Desc' => '',
						)
					);
					array_push($msg_content, $msg_content_elem);
					fanwe_require(APP_ROOT_PATH.'system/tim/TimApi.php');
					$api = createTimAPI();
					$ret = $api->group_send_group_msg2($user_id, $pk['from_room_id'], $msg_content);
					if ($ret['ActionStatus'] == 'FAIL' || $ret['ErrorCode'] == 10002) {
						//10002 系统错误，请再次尝试或联系技术客服。
						log_err_file(array(__FILE__, __LINE__, __METHOD__, $ret));
						$api->group_send_group_msg2($user_id, $pk['from_room_id'], $msg_content);
					}
					if ($punish_time <=0) {
						$punish_array = array();
						$punish_array['type'] = 97;
						$punish_array['data']['punish_text'] = '惩罚结束';
						$msg_content = array();
						$msg_content_elem = array(
							'MsgType' => 'TIMCustomElem',       //自定义类型
							'MsgContent' => array(
								'Data' => json_encode($punish_array),
								'Desc' => '',
							)
						);
						array_push($msg_content, $msg_content_elem);
						fanwe_require(APP_ROOT_PATH.'system/tim/TimApi.php');
						$api = createTimAPI();
						$ret = $api->group_send_group_msg2($user_id, $pk['from_room_id'], $msg_content);
						if ($ret['ActionStatus'] == 'FAIL' || $ret['ErrorCode'] == 10002) {
							log_err_file(array(__FILE__, __LINE__, __METHOD__, $ret));
							$api->group_send_group_msg2($user_id, $pk['from_room_id'], $msg_content);
						}
					}

					$ext = array();
					$ext['type'] = 96; // 95
					$ext['timestamp'] = $millisecond;
					$pk['from_room_id'] = $v['form_video_id'];
					$pk['from_user_id'] = $v['form_user_id'];
					$pk['from_nick_name'] = $form_user['nick_name'];
					$pk['from_head_image'] = get_spec_image($form_user['head_image']);
					$pk['from_ticket'] = $form_ticket;
					$pk['from_result'] = $form_result;
					$pk['to_room_id'] = $v['to_video_id'];
					$pk['to_user_id'] = $v['to_user_id'];
					$pk['to_nick_name'] = $to_user['nick_name'];
					$pk['to_head_image'] = get_spec_image($to_user['head_image']);
					$pk['to_ticket'] = $to_ticket;
					$pk['to_result'] = $to_result;
					$pk['desc'] = $to_desc;
					$pk['punish_time'] = $punish_time > 0 ? $punish_time : 0;

					$pk['from_user_info'] = [];
					$pk['from_user_info']['room_id'] = $v['form_video_id'];
					$pk['from_user_info']['user_id'] = $v['form_user_id'];
					$pk['from_user_info']['nick_name'] = $form_user['nick_name'];
					$pk['from_user_info']['head_image'] = get_spec_image($form_user['head_image']);
					$pk['from_user_info']['ticket'] = $form_ticket;
					$pk['from_user_info']['result'] = $form_result;
					$pk['to_user_info'] = [];
					$pk['to_user_info']['room_id'] = $v['to_video_id'];
					$pk['to_user_info']['user_id'] = $v['to_user_id'];
					$pk['to_user_info']['nick_name'] = $to_user['nick_name'];
					$pk['to_user_info']['head_image'] = get_spec_image($to_user['head_image']);
					$pk['to_user_info']['ticket'] = $to_ticket;
					$pk['to_user_info']['result'] = $to_result;

					$ext['data'] = $pk;
					#构造高级接口所需参数
					$msg_content = array();
					//创建array 所需元素
					$msg_content_elem = array(
						'MsgType' => 'TIMCustomElem',       //自定义类型
						'MsgContent' => array(
							'Data' => json_encode($ext),
							'Desc' => '',
						)
					);
				
					//将创建的元素$msg_content_elem, 加入array $msg_content
					array_push($msg_content, $msg_content_elem);
					$ret = $api->group_send_group_msg2($pk['to_user_id'], $pk['to_room_id'], $msg_content);
					if ($ret['ActionStatus'] == 'FAIL' || $ret['ErrorCode'] == 10002) {
						//10002 系统错误，请再次尝试或联系技术客服。
						log_err_file(array(__FILE__, __LINE__, __METHOD__, $ret));
						$api->group_send_group_msg2($pk['to_user_id'], $pk['to_room_id'], $msg_content);
					}
					if ($punish_time <=0) {
						$punish_array = array();
						$punish_array['type'] = 97;
						$punish_array['timestamp'] = getMillisecond();
						$punish_array['data']['punish_text'] = '惩罚结束';
						$msg_content = array();
						$msg_content_elem = array(
							'MsgType' => 'TIMCustomElem',       //自定义类型
							'MsgContent' => array(
								'Data' => json_encode($punish_array),
								'Desc' => '',
							)
						);
						array_push($msg_content, $msg_content_elem);
						fanwe_require(APP_ROOT_PATH.'system/tim/TimApi.php');
						$api = createTimAPI();
						$ret = $api->group_send_group_msg2($pk['to_user_id'], $pk['to_room_id'], $msg_content);
						if ($ret['ActionStatus'] == 'FAIL' || $ret['ErrorCode'] == 10002) {
							log_err_file(array(__FILE__, __LINE__, __METHOD__, $ret));
							$api->group_send_group_msg2($pk['to_user_id'], $pk['to_room_id'], $msg_content);
						}
					}
				}
				
				$GLOBALS['db']->Commit($pInTrans);
			} catch (Exception $e) {
				$GLOBALS['db']->Rollback($pInTrans);
				return $e->getMessage();
			}

		}
	}
}


