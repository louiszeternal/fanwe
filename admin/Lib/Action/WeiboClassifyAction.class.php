<?php

class WeiboClassifyAction extends CommonAction
{
    public function index()
    {
        $map =array();
        $model = D ('weibo_classify');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频分类');
        $this->display ();
    }
    public function edit()
    {
        $id = $_REQUEST ['id'];
        $classify = M('weibo_classify')->where(array('id'=>$id))->find();
        $this->assign ( 'classify', $classify );
        $this->assign('main_title','小视频分类编辑');
        $this->display ();
    }

    //更新
    public function update() {
        $data['id'] = $_REQUEST['id'];
        $data['title'] = $_REQUEST['title'];
        $data['brief'] = $_REQUEST['brief'];
        $data['is_effect'] = $_REQUEST['is_effect'];
        $data['sort'] = $_REQUEST['sort'];
        $data['weight'] = $_REQUEST['weight'];
        if(empty($data)){
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        }
        $re = M('weibo_classify')->save($data);
        if(false === $re ) {
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        } else {
            //成功提示
            $this->success(L("UPDATE_SUCCESS"),u("WeiboClassify/index"));
        }
    }

    public function delete()
    {
        //删除指定记录
        $ajax = intval($_REQUEST['ajax']);
        $id = $_REQUEST ['id'];
        if (isset ($id)) {
            $count = M(weibo)->where(array('classify_id'=>$id))->count();
            if ($count>0){
                $this->error('该分类被占用无法删除'. l("INVALID_OPERATION"), $ajax);
            }
            $res = M('weibo_classify')->where(array('id'=>$id))->delete();
            if ($res >0) {
                save_log('删除小视频分类'. l("DELETE_SUCCESS"), 1);
                $this->success(l("DELETE_SUCCESS"), $ajax);
            } else {
                save_log('删除小视频分类' . l("DELETE_FAILED"), 0);
                $this->error(l("DELETE_FAILED"), $ajax);
            }
        } else {
            $this->error(l("INVALID_OPERATION"), $ajax);
        }
    }

    public function add()
    {
		$this->assign("new_sort", M("weibo_classify")->max("sort")+1);
        $this->assign('main_title','小视频分类新增');
        $this->display ();
    }

    //新增
    public function insert() {

        $data['title'] = $_REQUEST['title'];
        $data['brief'] = $_REQUEST['brief'];
        $data['is_effect'] = $_REQUEST['is_effect'];
        $data['sort'] = $_REQUEST['sort'];
        $data['weight'] = $_REQUEST['weight'];

        if(empty($data['title'])){
            $this->error('请输入分类名称',0,L("UPDATE_FAILED"));
        }
        if(empty($data['brief'])){
            $this->error('请输入分类简介',0,L("UPDATE_FAILED"));
        }
        if(empty($data['weight'])){
            $this->error('请输入权重',0,L("UPDATE_FAILED"));
        }
        if(empty($data['sort'])){
            $this->error('请输入排序',0,L("UPDATE_FAILED"));
        }
        if($data['is_effect']==null){
            $this->error('请选择状态',0,L("UPDATE_FAILED"));
        }
        $re = M('weibo_classify')->add($data);
        if($re>0 ) {
            $this->success(L("UPDATE_SUCCESS"),u("WeiboClassify/index"));
        } else {
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        }
    }

    //设置状态
    public function set_effect()
    {
        $id = intval($_REQUEST['id']);
        $c_is_effect = M('weibo_classify')->where("id=".$id)->getField("is_effect");  //当前状态
        $n_is_effect = $c_is_effect == 0 ? 1 : 0; //需设置的状态
        $result=M('weibo_classify')->where("id=".$id)->setField("is_effect",$n_is_effect);
        $this->ajaxReturn($n_is_effect,l("SET_BAN_".$n_is_effect),1);

    }





}