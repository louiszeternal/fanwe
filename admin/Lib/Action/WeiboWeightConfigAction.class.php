<?php
// +----------------------------------------------------------------------
// | XX公司p2p借贷系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// |
// +----------------------------------------------------------------------

class WeiboWeightConfigAction extends CommonAction{

	public function index() {

        $config_list = M('weibo_weight_config')->select();
		$this->assign ( 'config_list', $config_list );
        $this->assign('main_title','小视频权重配置');
		$this->display ();
	}

    //更新权重配置
    public function update() {

        $title = $_REQUEST['title'];
        $user_weight = $_REQUEST['user_weight'];
        $classify_weight = $_REQUEST['classify_weight'];
        $tag_weight = $_REQUEST['tag_weight'];
        $music_weight = $_REQUEST['music_weight'];
        $address_weight = $_REQUEST['address_weight'];
        $is_effect = $_REQUEST['is_effect'];
        $type = $_REQUEST['type'];
        foreach ($user_weight as $k => $v) {
            $config_data[$k]['title'] = $title[$k];
            $config_data[$k]['user_weight'] = $user_weight[$k];
            $config_data[$k]['user_weight'] = $user_weight[$k];
            $config_data[$k]['classify_weight'] =$classify_weight[$k];
            $config_data[$k]['tag_weight'] =$tag_weight[$k];
            $config_data[$k]['music_weight'] = $music_weight[$k];
            $config_data[$k]['address_weight'] = $address_weight[$k];
            $config_data[$k]['is_effect'] = $is_effect[$k];
            $config_data[$k]['type'] = $type[$k];
        }
        if(empty($config_data)){
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        }
        $model=M('weibo_weight_config');
        try {
            $model->startTrans();
            //如果数据已经有存在，先清除掉在进行插入更新操作
            $model->where("id>".'0')->delete();
            //先建立数据
            $config_re = $model->addAll($config_data);
            if(false === $config_re ) {
                // 发生错误自动回滚事务
                $model->rollback();
                //错误提示
                $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
            } else {
                $model->commit();
                $this->create_httpd();
                //成功提示
                clear_auto_cache("cache_shop_acate_tree");
                clear_auto_cache("deal_shop_acate_belone_ids");
                clear_auto_cache("get_help_cache");
                $this->success(L("UPDATE_SUCCESS"));
            }
        } catch (Exception $e) {
            $model->rollback();
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        }

    }

    public function user_weight() {

        $user_name = $_REQUEST['user_name'];
        $start_time = strtotime($_REQUEST['start_time']) - date('Z');
        $end_time   = strtotime($_REQUEST['end_time']) - date('Z');

//        if (!empty($user_name)){
//            $user_id = M('user')->where(array('nick_name'=>$user_name))->getField('id');
//            $map['user_id'] = $user_id;
//        }
        if (intval($_REQUEST['user_id']) > 0) {
            $map['user_id'] = intval($_REQUEST['user_id']);
        }
        if ($start_time > 0 && $end_time > 0) {
            if ( $start_time >= $end_time ) {
                $this->error('开始时间必须小于结束时间');
            };
            $map['create_time'] = array('between', "{$start_time}, {$end_time}");
        }

        $model = D ('weibo_user_weight');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频人权重明细');
        $this->display ();
    }

    public function classify_weight() {

        $user_name = $_REQUEST['user_name'];
        $start_time = strtotime($_REQUEST['start_time']) - date('Z');
        $end_time   = strtotime($_REQUEST['end_time']) - date('Z');

//        if (!empty($user_name)){
//            $user_id = M('user')->where(array('nick_name'=>$user_name))->getField('id');
//            $map['user_id'] = $user_id;
//        }
        if (intval($_REQUEST['user_id']) > 0) {
            $map['user_id'] = intval($_REQUEST['user_id']);
        }
        if ($start_time > 0 && $end_time > 0) {
            if ( $start_time >= $end_time ) {
                $this->error('开始时间必须小于结束时间');
            };
            $map['create_time'] = array('between', "{$start_time}, {$end_time}");
        }

        $model = D ('weibo_classify_weight');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频分类权重明细');
        $this->display ();
    }

    public function tag_weight() {

        $user_name = $_REQUEST['user_name'];
        $start_time = strtotime($_REQUEST['start_time']) - date('Z');
        $end_time   = strtotime($_REQUEST['end_time']) - date('Z');

//        if (!empty($user_name)){
//            $user_id = M('user')->where(array('nick_name'=>$user_name))->getField('id');
//            $map['user_id'] = $user_id;
//        }
        if (intval($_REQUEST['user_id']) > 0) {
            $map['user_id'] = intval($_REQUEST['user_id']);
        }
        if ($start_time > 0 && $end_time > 0) {
            if ( $start_time >= $end_time ) {
                $this->error('开始时间必须小于结束时间');
            };
            $map['create_time'] = array('between', "{$start_time}, {$end_time}");
        }

        $model = D ('weibo_tag_weight');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频标签权重明细');
        $this->display ();
    }

    public function music_weight() {
        $user_name = $_REQUEST['user_name'];
        $start_time = strtotime($_REQUEST['start_time']) - date('Z');
        $end_time   = strtotime($_REQUEST['end_time']) - date('Z');

//        if (!empty($user_name)){
//            $user_id = M('user')->where(array('nick_name'=>$user_name))->getField('id');
//            $map['user_id'] = $user_id;
//        }
        if (intval($_REQUEST['user_id']) > 0) {
            $map['user_id'] = intval($_REQUEST['user_id']);
        }
        if ($start_time > 0 && $end_time > 0) {
            if ( $start_time >= $end_time ) {
                $this->error('开始时间必须小于结束时间');
            };
            $map['create_time'] = array('between', "{$start_time}, {$end_time}");
        }

        $model = D ('weibo_music_weight');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频歌曲权重明细');
        $this->display ();
    }

    public function address_weight() {

        $user_name = $_REQUEST['user_name'];
        $start_time = strtotime($_REQUEST['start_time']) - date('Z');
        $end_time   = strtotime($_REQUEST['end_time']) - date('Z');

//        if (!empty($user_name)){
//            $user_id = M('user')->where(array('nick_name'=>$user_name))->getField('id');
//            $map['user_id'] = $user_id;
//        }
        if (intval($_REQUEST['user_id']) > 0) {
            $map['user_id'] = intval($_REQUEST['user_id']);
        }
        if ($start_time > 0 && $end_time > 0) {
            if ( $start_time >= $end_time ) {
                $this->error('开始时间必须小于结束时间');
            };
            $map['create_time'] = array('between', "{$start_time}, {$end_time}");
        }

        $model = D ('weibo_address_weight');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频地点权重明细');
        $this->display ();
    }


    public function weibo_weight_log() {

        $user_name = $_REQUEST['user_name'];
        $status = $_REQUEST['status'];
        $start_time = strtotime($_REQUEST['start_time']) - date('Z');
        $end_time   = strtotime($_REQUEST['end_time']) - date('Z');

//        if (!empty($user_name)){
//            $user_id = M('user')->where(array('nick_name'=>$user_name))->getField('id');
//            $map['user_id'] = $user_id;
//        }
        if (intval($_REQUEST['user_id']) > 0) {
            $map['user_id'] = intval($_REQUEST['user_id']);
        }
        if ($status>0){
            $map['status'] = $status;
        }
        if ($start_time > 0 && $end_time > 0) {
            if ( $start_time >= $end_time ) {
                $this->error('开始时间必须小于结束时间');
            };
            $map['create_time'] = array('between', "{$start_time}, {$end_time}");
        }

        $model = D ('weibo_weight_log');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频用户权重日志');
        $this->display ();
    }


	
	public function get_bs(){
		$article_cates_bs= load_auto_cache("article_cates_bs",array(),false);
     	$article_cates_bs=array_keys($article_cates_bs);
     	return $article_cates_bs;
	}

    public function create_httpd(){
     	$htppd=APP_ROOT_PATH.".htaccess";
     	$content=file_get_contents($htppd);
     	if(strpos($content,'title')==false){
     		$htppd_old=APP_ROOT_PATH."public/rewrite_rule/.htaccess";
     		$content=file_get_contents($htppd_old);
     	}
     	$article_cates_bs=$this->get_bs();
     	$article_cates_bs=implode('|',$article_cates_bs);
      	$str=trim($article_cates_bs,'|');
       	$content=str_replace("title",$str,$content);
        file_put_contents($htppd,$content);
    }

}
?>