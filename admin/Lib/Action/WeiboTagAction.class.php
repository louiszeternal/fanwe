<?php

class WeiboTagAction extends CommonAction
{
    public function index()
    {

        $map =array();
        $model = D ('weibo_tag');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频标签');
        $this->display ();
    }
    public function edit()
    {
        $id = $_REQUEST ['id'];
        $tag = M('weibo_tag')->where(array('id'=>$id))->find();
        $this->assign ( 'tag', $tag );
        $this->assign('main_title','小视频标签编辑');
        $this->display ();
    }

    //更新
    public function update() {
        $data['id'] = $_REQUEST['id'];
        $data['title'] = $_REQUEST['title'];
        $data['is_effect'] = $_REQUEST['is_effect'];
        $data['sort'] = $_REQUEST['sort'];
        $data['weight'] = $_REQUEST['weight'];

        if(empty($data)){
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        }
        $re = M('weibo_tag')->save($data);
        if(false === $re ) {
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        } else {
            //成功提示
            $this->success(L("UPDATE_SUCCESS"),u("WeiboTag/index"));
        }
    }

    public function add()
    {
        $this->assign('main_title','小视频标签新增');
        $this->display ();
    }

    //新增
    public function insert() {

        $data['title'] = $_REQUEST['title'];
        $data['is_effect'] = $_REQUEST['is_effect'];
        $data['sort'] = $_REQUEST['sort'];
        $data['weight'] = $_REQUEST['weight'];

        if(empty($data['title'])){
            $this->error('请输入标签名称',0,L("UPDATE_FAILED"));
        }
        if(empty($data['weight'])){
            $this->error('请输入权重',0,L("UPDATE_FAILED"));
        }
        if(empty($data['sort'])){
            $this->error('请输入排序',0,L("UPDATE_FAILED"));
        }
        if($data['is_effect']==null){
            $this->error('请选择状态',0,L("UPDATE_FAILED"));
        }
        $re = M('weibo_tag')->add($data);
        if($re>0 ) {
            //成功提示
            $this->success(L("UPDATE_SUCCESS"),u("WeiboTag/index"));
        } else {
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        }
    }

    //设置状态
    public function set_effect()
    {
        $id = intval($_REQUEST['id']);
        $c_is_effect = M('weibo_tag')->where("id=".$id)->getField("is_effect");  //当前状态
        $n_is_effect = $c_is_effect == 0 ? 1 : 0; //需设置的状态
        $result=M('weibo_tag')->where("id=".$id)->setField("is_effect",$n_is_effect);
        $this->ajaxReturn($n_is_effect,l("SET_BAN_".$n_is_effect),1);

    }



}