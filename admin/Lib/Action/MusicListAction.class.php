<?php
// +----------------------------------------------------------------------
// | XX公司直播系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// |
// +----------------------------------------------------------------------

class MusicListAction extends CommonAction{
	//回播列表
	public function index() {

		$now=get_gmtime();
		if($_REQUEST['audio_name']!='')
		{
			$map['audio_name'] = strim($_REQUEST['audio_name']);
		}
		if($_REQUEST['artist_name']!='')
		{
			$map['artist_name'] = strim($_REQUEST['artist_name']);
		}
		if(intval($_REQUEST['classified_id'])>0)
		{
			$map['classified_id'] = intval($_REQUEST['classified_id']);
		}
		$create_time_2=empty($_REQUEST['create_time_2'])?to_date($now,'Y-m-d'):strim($_REQUEST['create_time_2']);
		$create_time_2=to_timespan($create_time_2)+24*3600;
		if(trim($_REQUEST['create_time_1'])!='')
		{
			$map['create_time'] = array('between',array(to_timespan($_REQUEST['create_time_1']),$create_time_2));
		}
		if (method_exists ( $this, '_filter' )) {
			$this->_filter ( $map );
		}
		$model = D (MODULE_NAME);
		if (! empty ( $model )) {
			$this->_list ( $model, $map );
		}
        $list = $this->get("list");
		foreach ($list as $k=>$v) {

			$list[$k]['time_len'] = $this->to_time($v['time_len']);
		}
        $this->assign ( 'list', $list );
		$cate_list = M("MusicClassified")->findAll();
		$this->assign("classified_list",$cate_list);
		$this->display ();
	}
    public function add_tecent_video(){
        if(!TECENT_VIDEO){
            admin_ajax_return(array(
                'status' => 0,
                'error'  => "模块开关未打开"
            ));
        }
        $m_config = load_auto_cache("m_config");
        $this->assign('secret_id', $m_config['qcloud_secret_id']);
        $this->display();
    }

	public function add_music(){
		$cate_list = M("MusicClassified")->findAll();
		$this->assign("classified_list",$cate_list);
		$this->assign("new_sort", M("MusicList")->max("sort")+1);
		$this->assign('max_size', conf('MAX_IMAGE_SIZE') / 100);
		$this->display();
	}

	public function edit(){
		$music_list = M('MusicList')->find(intval($_REQUEST['id']));
		$cate_list = M("MusicClassified")->findAll();
		$this->assign('data',$music_list);
		$this->assign("classified_list",$cate_list);
		$this->assign("new_sort", M("MusicList")->max("sort")+1);
		$this->assign('max_size', conf('MAX_IMAGE_SIZE') / 100);
		$this->display();
	}

	//上传OSS
	function upload_oss(){
		$result = array('status'=>1,'error'=>'添加音乐成功');

		$file_url = trim($_REQUEST['file_url']);
		$file_url = urldecode($file_url);
		$data['audio_name'] = trim($_REQUEST['audio_name']);
		$data['artist_name'] = trim($_REQUEST['artist_name']);
		$data['classified_id'] = intval($_REQUEST['classified_id']);
		$data['time_len'] = intval($_REQUEST['duration']);
		$data['create_time'] = NOW_TIME;
		$data['sort'] = intval($_REQUEST['sort']);
		$data['audio_image'] = strim($_REQUEST['image']);

		//判断音乐是否已添加 演唱者-歌手-音乐时长均相同
		$is_music = $GLOBALS['db']->getOne("SELECT id FROM ".DB_PREFIX."music_list WHERE audio_name = '".$data['audio_name']."' and artist_name = '".$data['artist_name']."' and time_len = {$data['time_len']}");
		if ($is_music > 0) {
			admin_ajax_return(array('status'=>'0','error'=>'歌曲已经上传过了'));
		}

		if (!check_empty($data['audio_name']))
		{
			admin_ajax_return(array('status'=>'0','error'=>'请输入演唱者名称'));
		}
		if (!check_empty($data['artist_name']))
		{
			admin_ajax_return(array('status'=>'0','error'=>'请输入音乐名称'));
		}
//		if ($data['audio_image'] == '') {
//			admin_ajax_return(array('status'=>'0','error'=>'音乐封面不能为空'));
//		}
		if ($data['classified_id'] <= 0)
		{
			admin_ajax_return(array('status'=>'0','error'=>'请选择音乐分类'));
		}
		if (!check_empty($file_url))
		{
			admin_ajax_return(array('status'=>'0','error'=>'音乐链接不能为空'));
		}
		if ($data['time_len'] == '') {
			admin_ajax_return(array('status'=>'0','error'=>'音乐时长尚未获取完毕，请等待'));
		}
		if ($data['time_len'] <= 10 ) {
			admin_ajax_return(array('status'=>'0','error'=>'请上传大于10秒的音乐'));
		}
		if ($GLOBALS['distribution_cfg']['OSS_TYPE']&&$GLOBALS['distribution_cfg']['OSS_TYPE']=='ALI_OSS'){
			$data['audio_link'] = get_spec_image($file_url);
		}else{
			$data['audio_link'] = str_replace("./public/",file_domain()."/public/",$file_url);
		}

		$list = $GLOBALS['db']->autoExecute(DB_PREFIX . "music_list", $data, 'INSERT');

		if ($list !== false){
			save_log("音乐:{$data['artist_name']}上传成功",1);
		}else{
			save_log("音乐:{$data['artist_name']}上传失败",0);
			admin_ajax_return(array('status'=>'0','error'=>'添加音乐失败，请稍后再试'));
		}
		admin_ajax_return($result);
	}

	//更新
	public function update()
	{
		$result = array('status'=>1,'error'=>'更新音乐成功');

		$file_url = trim($_REQUEST['file_url']);
		$file_url = urldecode($file_url);


		$data['audio_name'] = trim($_REQUEST['audio_name']);
		$data['artist_name'] = trim($_REQUEST['artist_name']);
		$data['classified_id'] = intval($_REQUEST['classified_id']);
		$data['time_len'] = intval($_REQUEST['duration']);
		$data['create_time'] = NOW_TIME;
		$data['sort'] = intval($_REQUEST['sort']);
        $data['weight'] = intval($_REQUEST['weight']);
		$data['id'] = intval($_REQUEST['id']);

		//判断音乐是否已添加 演唱者-歌手-音乐时长均相同
		$is_music = $GLOBALS['db']->getOne("SELECT id FROM ".DB_PREFIX."music_list WHERE audio_name = '".$data['audio_name']."' and id <> {$data['id']} and artist_name = '".$data['artist_name']."' and time_len = {$data['time_len']}");
		if ($is_music > 0) {
			admin_ajax_return(array('status'=>'0','error'=>'歌曲已经上传过了'));
		}

		if(!check_empty($data['audio_name']))
		{
			admin_ajax_return(array('status'=>'0','error'=>'请输入演唱者名称'));
		}
		if(!check_empty($data['artist_name']))
		{
			admin_ajax_return(array('status'=>'0','error'=>'请输入音乐名称'));
		}
		if($data['classified_id'] <= 0)
		{
			admin_ajax_return(array('status'=>'0','error'=>'请选择音乐分类'));
		}
		if(!check_empty($file_url))
		{
			admin_ajax_return(array('status'=>'0','error'=>'音乐链接不能为空'));
		}
		if($GLOBALS['distribution_cfg']['OSS_TYPE']&&$GLOBALS['distribution_cfg']['OSS_TYPE']=='ALI_OSS'){
			$data['audio_link'] = get_spec_image($file_url);
		}else{
			$data['audio_link'] = str_replace("./public/",file_domain()."/public/",$file_url);
		}


		$GLOBALS['db']->autoExecute(DB_PREFIX."music_list",$data,"UPDATE", "id=".$data['id']);

		if ($GLOBALS['db']->affected_rows()) {
			clear_auto_cache("music_list");
			save_log("音乐:{$data['artist_name']}更新成功",1);
		}else{
			save_log("音乐:{$data['artist_name']}更新失败",0);

			admin_ajax_return(array('status'=>'0','error'=>'更新失败'));
		}
		admin_ajax_return($result);
	}

    /**
     * 上传视频签名接口
     *
     * @return 签名
     */
    public function sign()
    {
        $args = $_REQUEST['args'];
        fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/video_factory.php');
        $video_factory = new VideoFactory();
        $result = $video_factory->Sign($args);
        $root = array('status' => 1, 'result' => $result);
        ajax_file_return($root);
    }
    /**
     * 新上传视频SDK签名接口
     *
     * @return 签名
     */
    public function new_sign(){
        fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/video_factory.php');
        $video_factory = new VideoFactory();
        $result = $video_factory->NewSign();
        $root = array('status' => 1, 'signature' => $result);
        ajax_file_return($root);
    }

    //设置状态
	public function set_effect()
	{
		$id = intval($_REQUEST['id']);
		$info = M(MODULE_NAME)->where("id=".$id)->getField("audio_name");
		$c_is_effect = M(MODULE_NAME)->where("id=".$id)->getField("is_effect");  //当前状态
		$n_is_effect = $c_is_effect == 0 ? 1 : 0; //需设置的状态
		M(MODULE_NAME)->where("id=".$id)->setField("is_effect",$n_is_effect);
		save_log('音乐:'.$info.l("SET_EFFECT_".$n_is_effect),1);
		clear_auto_cache("music_list");
		$this->ajaxReturn($n_is_effect,l("SET_EFFECT_".$n_is_effect),1)	;
	}

	//设置排序
	public function set_sort()
	{
		$id = intval($_REQUEST['id']);
		$sort = intval($_REQUEST['sort']);
		$info = M("MusicList")->where("id=".$id)->getField("audio_name");
		if(!check_sort($sort))
		{
			$this->error(l("SORT_FAILED"),1);
		}
		M(MODULE_NAME)->where("id=".$id)->setField("sort",$sort);
		save_log('音乐:'.$info.l("SORT_SUCCESS"),1);
		clear_auto_cache("music_list");
		$this->success(l("SORT_SUCCESS"),1);
	}

	//删除视频
	public function del_video(){
		require_once APP_ROOT_PATH."/mapi/lib/core/common.php";
		$id = $_REQUEST['id'];
		$result['status'] = 0;
		if (isset ( $id )) {
			$condition = array ('id' => array ('in', explode ( ',', $id ) ) );
			$rel_data = M('MusicList')->where($condition)->findAll();
			$success_info = array();
			$fail_info = array();
			foreach($rel_data as $music)
			{
				$sql = "delete from ".DB_PREFIX."music_list where id =".$music['id'];
				$GLOBALS['db']->query($sql);
				if($GLOBALS['db']->affected_rows()) {
					$result['status'] = 1;
					$success_info[] = $music['id'];
				}else{
					$fail_info[] = $music['id'];
				}
			}

			if($success_info) $success_info = implode(",",$success_info);
			if($fail_info) $fail_info = implode(",",$fail_info);
			if (!$fail_info) {
				save_log($success_info.l("FOREVER_DELETE_SUCCESS"),1);
				clear_auto_cache("music_list");
				$result['info'] = '删除成功！';
				//$this->success (l("FOREVER_DELETE_SUCCESS"),$ajax);
			} else {
				if($success_info){
					save_log($success_info.l("FOREVER_DELETE_SUCCESS"),1);
				}
				save_log($fail_info.l("FOREVER_DELETE_FAILED"),0);
				$result['info'] = $fail_info.'  删除失败！';
				//$this->error (l($fail_info),$ajax);
			}
		} else {
			$result['status'] = 0;
			$result['info'] = '编号错误';
		}
		admin_ajax_return($result);
	}

	/**
	 * 将秒转换为 分:秒
	 * s int 秒数
	 */
	protected function to_time($s=0){
		//计算分钟
		//算法：将秒数除以60，然后下舍入，既得到分钟数
		$h    =    floor($s/60);
		//取得秒%60的余数，既得到秒数
		$s    =    $s%60;
		//如果只有一位数，前面增加一个0
		$h    =    (strlen($h)==1)?'0'.$h:$h;
		$s    =    (strlen($s)==1)?'0'.$s:$s;
		return $h.':'.$s;
	}


}
?>