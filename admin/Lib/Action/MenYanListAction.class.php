<?php
class MenYanListAction extends CommonAction
{
	private $MODULE_NAME = 'MenYanList';
	public function index()
	{
		if (strim($_REQUEST['name']) != '') {
			$map['name'] = array ('like', '%' . strim($_REQUEST['name']) . '%');
		}
		if (method_exists($this, '_filter')) {
			$this->_filter($map);
		}
		$model = D($this->MODULE_NAME);
		if (!empty ($model)) {
			$this->_list($model, $map);
		}
		$this->display();
	}

	public function add()
	{
		$this->assign("new_sort", M($this->MODULE_NAME)->max("sort") + 1);
		$this->display();
	}

	public function edit()
	{
		$id = intval($_REQUEST ['id']);
		$condition['id'] = $id;
		$vo = M($this->MODULE_NAME)->where($condition)->find();
		$this->assign('vo', $vo);
		$this->display();
	}

	public function foreverdelete()
	{
		//彻底删除指定记录
		$ajax = intval($_REQUEST['ajax']);
		$id = $_REQUEST ['id'];
		if (isset ($id)) {
			$id = explode(',', $id);
			//分类下有音乐时不删除
			$id_count = count($id);
			$unset_num = '编号';
			foreach ($id as $ki => $vi) {
				$video = M('Music_List')->where("classified_id=" . $vi . " and live_in<>0")->findAll();
				if ($video) {
					$unset_num .= ($vi . ',');
					unset($id[$ki]);
				}
			}
			if (count($id) < $id_count) {
				$id = array_values($id);
				$unset_num = substr($unset_num, 0, -1);
				$unset_num .= '分类下有音乐不能删除';
			} else {
				$unset_num = '';
			}
			$condition = array ('id' => array ('in', $id));
			$rel_data = M($this->MODULE_NAME)->where($condition)->findAll();
			foreach ($rel_data as $data) {
				$info[] = $data['title'];
			}
			if ($info) $info = implode(",", $info);
			$list = M($this->MODULE_NAME)->where($condition)->delete();
			if ($list !== false) {
				$this->update_class();
				clear_auto_cache("music_classified");
				save_log($info . l("FOREVER_DELETE_SUCCESS"), 1);
				if ($unset_num == '') {
					$this->success(l("FOREVER_DELETE_SUCCESS"));
				} else {
					$this->success($unset_num);
				}
			} else {
				save_log($info . l("FOREVER_DELETE_FAILED"), 0);
				if ($unset_num == '') {
					$this->error(l("FOREVER_DELETE_FAILED"));
				} else {
					$this->error($unset_num);
				}

			}
		} else {
			$this->error(l("INVALID_OPERATION"));
		}
	}

	public function update_class()
	{
		clear_auto_cache("m_config");
		$sql = $GLOBALS['db']->query("update " . DB_PREFIX . "m_config set val=val+1 where code ='init_version'");
		return $sql;
	}

	public function insert()
	{
		B('FilterString');
		$data = M($this->MODULE_NAME)->create();
		//开始验证有效性
		$this->assign("jumpUrl", u($this->MODULE_NAME . "/add"));
		$data['name'] = strim($data['name']);
		$data['sort'] = intval($_REQUEST['sort']);
		$data['image'] = strim($data['image']);
		$data['file_name'] = strim($_REQUEST['file_name']);
		if (!check_empty($data['image'])) {
			$this->error("图片不能为空");
		}
		if (!check_empty($data['file_name'])) {
			$this->error("贴纸压缩包名称不能为空");
		}
		$data['download_link'] = strim($data['download_link']);
		if (!check_empty($data['download_link'])) {
			$this->error("下载地址不能为空");
		}

		if ($GLOBALS['distribution_cfg']['OSS_TYPE']&&$GLOBALS['distribution_cfg']['OSS_TYPE']=='ALI_OSS'){
			$data['download_link'] = get_spec_image($data['download_link']);
		}else{
			$data['download_link'] = str_replace("./public/",file_domain()."/public/",$data['download_link']);
		}


		// 更新数据
		$log_info = '贴纸' . $data['name'];
		$list = M($this->MODULE_NAME)->add($data);
		$m_config = load_auto_cache("m_config");//初始化手机端配置
		$init_version = intval($m_config['init_version']);//手机端配置版本号
		$info['init_version'] = $init_version + 1;
		if (false !== $list) {
			//成功提示
			$this->update_class();
//			clear_auto_cache("prop_burst");
			save_log($log_info . L("INSERT_SUCCESS"), 1);
			$this->success(L("INSERT_SUCCESS"));
		} else {
			//错误提示
			save_log($log_info . L("INSERT_FAILED"), 0);
			$this->error(L("INSERT_FAILED"));
		}
	}

	public function update()
	{
		B('FilterString');
		$data = M($this->MODULE_NAME)->create();
		//开始验证有效性
		$this->assign("jumpUrl", u($this->MODULE_NAME . "/edit", array ("id" => $data['id'])));
		$data['name'] = strim($data['name']);
		$data['sort'] = intval($_REQUEST['sort']);
		$data['image'] = strim($data['image']);
		$data['file_name'] = strim($_REQUEST['file_name']);
		if (!check_empty($data['image'])) {
			$this->error("图片不能为空");
		}
		if (!check_empty($data['file_name'])) {
			$this->error("贴纸压缩包名称不能为空");
		}
		$data['download_link'] = strim($data['download_link']);
		if (!check_empty($data['download_link'])) {
			$this->error("下载地址不能为空");
		}


		if ($GLOBALS['distribution_cfg']['OSS_TYPE']&&$GLOBALS['distribution_cfg']['OSS_TYPE']=='ALI_OSS'){
			$data['download_link'] = get_spec_image($data['download_link']);
		}else{
			$data['download_link'] = str_replace("./public/",file_domain()."/public/",$data['download_link']);
		}


		// 更新数据
		$log_info = '贴纸' . $data['name'];

		$list = M($this->MODULE_NAME)->save($data);

		if (false !== $list) {
			$this->update_class();
//			clear_auto_cache("prop_burst");
			save_log($log_info . L("UPDATE_SUCCESS"), 1);
			$this->success(L("UPDATE_SUCCESS"));
		} else {
			//错误提示
			save_log($log_info . L("UPDATE_FAILED"), 0);
			$this->error(L("UPDATE_FAILED"), 0, $log_info . L("UPDATE_FAILED"));
		}
	}

	public function set_effect()
	{
		$id = intval($_REQUEST['id']);
		$info = M($this->MODULE_NAME)->where("id=" . $id)->getField("name");
		$c_is_effect = M($this->MODULE_NAME)->where("id=" . $id)->getField("is_effect");  //当前状态
		$n_is_effect = $c_is_effect == 0 ? 1 : 0; //需设置的状态
		M($this->MODULE_NAME)->where("id=" . $id)->setField("is_effect", $n_is_effect);
		save_log('礼物连发配置'.$info . l("SET_EFFECT_" . $n_is_effect), 1);
		$this->update_class();
		clear_auto_cache("prop_burst");
		$this->ajaxReturn($n_is_effect, l("SET_EFFECT_" . $n_is_effect), 1);
	}

	public function set_sort()
	{
		$id = intval($_REQUEST['id']);
		$sort = intval($_REQUEST['sort']);
		$log_info = M("MusicClassified")->where("id=" . $id)->getField("title");
		if (!check_sort($sort)) {
			$this->error(l("SORT_FAILED"), 1);
		}
		M($this->MODULE_NAME)->where("id=" . $id)->setField("sort", $sort);
		save_log('礼物连发配置'.$log_info . l("SORT_SUCCESS"), 1);
		$this->update_class();
		clear_auto_cache("music_classified");
		$this->success(l("SORT_SUCCESS"), 1);
	}

}

?>