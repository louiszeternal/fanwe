<?php

class WeiboAddressAction extends CommonAction
{
    public function index()
    {
        $map =array();
        $model = D ('weibo_address');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
        $this->assign('main_title','小视频地点');
        $this->display ();
    }
    public function edit()
    {
        $id = $_REQUEST ['id'];
        $address = M('weibo_address')->where(array('id'=>$id))->find();
        $this->assign ( 'address', $address );
        $this->assign('main_title','小视频地点编辑');
        $this->display ();
    }

    //更新
    public function update() {
        $data['id'] = $_REQUEST['id'];
        $data['weight'] = $_REQUEST['weight'];

        if(empty($data)){
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        }
        $re = M('weibo_address')->save($data);
        if(false === $re ) {
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        } else {
            //成功提示
            $this->success(L("UPDATE_SUCCESS"),u("WeiboAddress/index"));
        }
    }






}