<?php
// +----------------------------------------------------------------------
// | XX公司直播系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// |
// +----------------------------------------------------------------------
fanwe_require(APP_ROOT_PATH . 'system/tencentcloud-sdk/TCloudAutoLoader.php');
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Vod\V20180717\VodClient;
use TencentCloud\Vod\V20180717\Models\ProcessMediaRequest;

class WeiboListAction extends CommonAction
{
    public function index($type = '')
    {

        $status = $_REQUEST['status'];
        $check_status = $_REQUEST['check_status'];
        $is_adv = $_REQUEST['is_adv'];
        $is_hot = $_REQUEST['is_hot'];
        $now = get_gmtime();

        if (intval($_REQUEST['weibo_id']) > 0) {
            $map['id'] = intval($_REQUEST['weibo_id']);
        }

        if (intval($_REQUEST['user_id']) > 0) {
            $map['user_id'] = intval($_REQUEST['user_id']);
            $luck_num = $GLOBALS['db']->getOne("SELECT id FROM ".DB_PREFIX."user WHERE luck_num = {$map['user_id']}");
            if ($luck_num > 0) {
				$map['user_id'] = $luck_num;
			}
        }

        if (!$type) {
            $type = $_REQUEST['type'];
        }
        if (trim($type)) {
            $map['type'] = trim($type);
        }

        if ($status!=null) {
            $map['status'] = intval($_REQUEST['status']);
        }
        if ($check_status!=null) {
            $map['check_status'] = intval($_REQUEST['check_status']);
        }
        if ($is_adv!=null) {
            $map['is_adv'] = intval($_REQUEST['is_adv']);
        }
        if ($is_hot!=null) {
            $map['is_hot'] = intval($_REQUEST['is_hot']);
        }

        $create_time_2 = empty($_REQUEST['create_time_2']) ? to_date($now, 'Y-m-d') : strim($_REQUEST['create_time_2']);
        $create_time_2 = to_timespan($create_time_2) + 24 * 3600;
        $create_time_2 = to_date($create_time_2, 'Y-m-d H:i:s');
        if (trim($_REQUEST['create_time_1']) != '') {
//            $map[DB_PREFIX.'weibo.create_time'] = array('between',array(to_timespan($_REQUEST['create_time_1']),$create_time_2));

//            $condition .= " and create_time>'" . $_REQUEST['create_time_1'] . "' and create_time<'" . $create_time_2 . "'";
            $map['create_time'] = array('between',array($_REQUEST['create_time_1'],$create_time_2));
        }


        $model = D ('Weibo');
        if (! empty ( $model )) {
            $this->_list ( $model, $map );
        }
		$list = $this->get('list');
		foreach ($list as $k => $v) {
			$res = $GLOBALS['db']->getOne("SELECT id FROM ".DB_PREFIX."active_apply WHERE user_id = {$v['user_id']}");
			$list[$k]['is_apply'] = $res > 0 ? '是' : '否';
		}
		$this->assign('list',$list);
        $this->assign('main_title','小视频列表');
        $this->display();
    }

    public function imagetext()
    {
        $weibo = $this->get_weibo_list('imagetext');
        $this->assign('list', $weibo);

        $this->display();
    }

    public function video()
    {
        $weibo = $this->get_weibo_list('video');
        $this->assign('list', $weibo);

        $this->display();
    }

    public function weixin()
    {
        $weibo = $this->get_weibo_list('weixin');
        $this->assign('list', $weibo);

        $this->display();
    }

    public function photo()
    {
        $weibo = $this->get_weibo_list('photo');
        $this->assign('list', $weibo);

        $this->display();
    }

    public function red_photo()
    {
        $weibo = $this->get_weibo_list('red_photo');
        $this->assign('list', $weibo);

        $this->display();
    }

    public function goods()
    {
        $weibo = $this->get_weibo_list('goods');
        $this->assign('list', $weibo);

        $this->display();
    }


    public function get_weibo_list($type = '', $is_recommend = 0)
    {

        $status = $_REQUEST['status'];
        $check_status = $_REQUEST['check_status'];
        $is_adv = $_REQUEST['is_adv'];
        $is_hot = $_REQUEST['is_hot'];
        $now = get_gmtime();
        $condition = ' 1 = 1 ';

        if (intval($_REQUEST['weibo_id']) > 0) {
            $condition .= "  and id =" . intval($_REQUEST['weibo_id']);
        }

        if (intval($_REQUEST['user_id']) > 0) {
            $condition .= "  and user_id =" . intval($_REQUEST['user_id']);
        }

        if (!$type) {
            $type = $_REQUEST['type'];
        }
        if (trim($type)) {
            $condition .= "  and type ='" . trim($type) . "'";
        }

        if (intval($is_recommend)) {
            $condition .= " and is_recommend =" . intval($is_recommend);
        }

        if ($status!=null) {
            $condition .= " and status =" . intval($status);
        }
        if ($check_status!=null) {
            $condition .= " and check_status =" . intval($_REQUEST['check_status']);
        }
        if ($is_adv!=null) {
            $condition .= " and is_adv =" . intval($_REQUEST['is_adv']);
        }
        if ($is_hot!=null) {
            $condition .= " and is_hot =" . intval($_REQUEST['is_hot']);
        }

        $create_time_2 = empty($_REQUEST['create_time_2']) ? to_date($now, 'Y-m-d') : strim($_REQUEST['create_time_2']);
        $create_time_2 = to_timespan($create_time_2) + 24 * 3600;
        $create_time_2 = to_date($create_time_2, 'Y-m-d H:i:s');
        if (trim($_REQUEST['create_time_1']) != '') {
//            $map[DB_PREFIX.'weibo.create_time'] = array('between',array(to_timespan($_REQUEST['create_time_1']),$create_time_2));

            $condition .= " and create_time>'" . $_REQUEST['create_time_1'] . "' and create_time<'" . $create_time_2 . "'";
        }
//        echo $condition;

        $count = M('Weibo')->where($condition)->count();
        $p = new Page($count, $listRows = 20);
        //举报类型

        $weibo = M('Weibo')->where($condition)->order('id desc')->limit($p->firstRow . ',' . $p->listRows)->findAll();

        $page = $p->show();
        $this->assign("page", $page);
        return $weibo;
    }


    public function foreverdelete()
    {
        //彻底删除指定记录
        $ajax = intval($_REQUEST['ajax']);
        $id = $_REQUEST ['id'];
        if (isset ($id)) {
            $condition = array('id' => array('in', explode(',', $id)));
            $rel_data = M('Weibo')->where($condition)->findAll();
            foreach ($rel_data as $data) {
                $info[] = $data['user_id'] . '的动态' . $data['id'];
            }
            if ($info) $info = implode(",", $info);
            $list = M('Weibo')->where($condition)->delete();
            //删除相关预览图
//				foreach($rel_data as $data)
//				{
//					@unlink(get_real_path().$data['preview']);
//				}
            if ($list !== false) {
                save_log($info . l("FOREVER_DELETE_SUCCESS"), 1);
                clear_auto_cache("get_help_cache");
                $this->success(l("FOREVER_DELETE_SUCCESS"), $ajax);
            } else {
                save_log($info . l("FOREVER_DELETE_FAILED"), 0);
                $this->error(l("FOREVER_DELETE_FAILED"), $ajax);
            }
        } else {
            $this->error(l("INVALID_OPERATION"), $ajax);
        }
    }

    //推荐微博
    public function weibo_recommend()
    {
        $weibo = $this->get_weibo_list('', 1);
        $this->assign('list', $weibo);

        $this->display();
    }

    //设置推荐
    public function set_recommend()
    {
        $id = intval($_REQUEST['id']);
        $c_is_effect = M('Weibo')->where("id=" . $id)->getField("is_recommend");  //当前状态
        $n_is_effect = $c_is_effect == 0 ? 1 : 0; //需设置的状态
        $result = M('Weibo')->where("id=" . $id)->setField("is_recommend", $n_is_effect);
        // save_log("房间号".$id.l("SET_RECOMMEND_".$n_is_effect),1);
        $this->ajaxReturn($n_is_effect, l("SET_BAN_" . $n_is_effect), 1);

    }

//    //设置状态
//    public function set_effect()
//    {
//        $id = intval($_REQUEST['id']);
//        $weibo_info = M('Weibo')->field('user_id,tags,audio_id,status')->find($id);  //小视频信息
//        $n_is_effect = $weibo_info['status'] == 0 ? 1 : 0; //需设置的状态
//        $weibo_info['user_id'] = intval($weibo_info['user_id']);
//        $weibo_info['audio_id'] = intval($weibo_info['audio_id']);
//        $result = M('Weibo')->where("id=" . $id)->setField("status", $n_is_effect);
//        //是否是第一次记录
//        $record = M('WeiboSameRecord')->where("weibo_id=" . $id)->count();
//        //发布者使用该音乐的次数，如果次数等于2，则不添加
//        $music_num = M('WeiboSameRecord')->where(array('user_id' => $weibo_info['user_id'], 'music_id' => $weibo_info['audio_id']))->count();
//        if ($record == 0 && $n_is_effect == 1) {
//            $music_arr = array();
//            $music_arr['user_id'] = $weibo_info['user_id'];
//            $music_arr['weibo_id'] = $id;
//            $music_arr['music_id'] = $weibo_info['audio_id'];
//            $music_arr['type'] = 1;
//
//            $pInTrans = $GLOBALS['db']->StartTrans();
//            try {
//                if ($music_num < 2) {
//                    if ($music_num == 1) {
//                        //6使用同歌曲
//                        fanwe_require(APP_ROOT_PATH . 'mapi/xr/core/common.php');
//                        $memo = "使用同歌曲，歌曲id为" . $music_arr['music_id'] . ",小视频id为" . $id;
//                        create_weight($weibo_info['user_id'], $id, $memo, 6);
//                    }
//                    //小视频音乐记录
//                    $res = M('WeiboSameRecord')->add($music_arr);
//                    if ($res == false) {
//                        throw new Exception('操作失败，请稍后再试');
//                    }
//                }
//
//                $tag_arr = array();
//                $tag_arr['user_id'] = $weibo_info['user_id'];
//                $tag_arr['weibo_id'] = $id;
//                $tag_arr['music_id'] = '';
//                $tags = json_decode($weibo_info['tags'], true);
//
//                foreach ($tags as $k => $v) {
//                    //发布者使用该标签的次数，如果次数等于2，则不添加
//                    $tag_num = M('WeiboSameRecord')->where(array('user_id' => $weibo_info['user_id'], 'tag_id' => $v['id']))->count();
//                    if ($tag_num >= 2) {
//                        continue;
//                    }
//                    if ($tag_num == 1) {
//                        //5发布同标签视频
//                        fanwe_require(APP_ROOT_PATH . 'mapi/xr/core/common.php');
//                        $memo = "使用同标签，标签id为" . $v['id'] . "标签名称为" . $v['title'] . ",小视频id为" . $id;
//                        create_weight($weibo_info['user_id'], $id, $memo, 5);
//                    }
//                    $tag_arr['tag_id'] = $v['id'];
//                    $tag_arr['tag_name'] = $v['title'];
//                    $tag_arr['type'] = 2;
//                    $res = M('WeiboSameRecord')->add($tag_arr);
//                    if ($res == false) {
//                        throw new Exception('操作失败，请稍后再试');
//                    }
//                }
//                $GLOBALS['db']->Commit($pInTrans);
//            } catch (Exception $e) {
//                $GLOBALS['db']->Rollback($pInTrans);
//                $this->error($e->getMessage(), 0, $e->getMessage());
//            }
//
//        }
//        $this->ajaxReturn($n_is_effect, l("SET_BAN_" . $n_is_effect), 1);
//
//    }

    public function edit()
    {
        $id = $_REQUEST['id'];
        $is_open_tencent = M("MConfig")->where(array('code' => 'is_open_tencent'))->getField('val');
        $check_status = M("weibo")->where(array('id' => $id))->getField('check_status');

        $list = M('Weibo')->where(array('id' => $id))->find();
        $list['classify_title'] = M('weibo_classify')->where(array('id' => $list['classify_id']))->getField('title');
        $tag_info = json_decode($list['tags'], true);
        $tags_list = M('weibo_tag')->where(array('is_effect' => 1))->select();

        $classify_list = M('weibo_classify')->where(array('is_effect' => 1))->select();
		$res = $GLOBALS['db']->getOne("SELECT id FROM ".DB_PREFIX."active_apply WHERE user_id = {$list['user_id']}");
		$is_apply = $res > 0 ? '是' : '否';
		$this->assign('is_apply', $is_apply);//是否已报名
        $this->assign('is_open_tencent', $is_open_tencent);//是否开启自动获取腾讯云标签分类
        $this->assign('check_status', $check_status);//是否已经审核编辑过
        $this->assign('tag_list', $tags_list);
        $this->assign('tag_info', $tag_info);
        $this->assign('list', $list);
        $this->assign('classify_list', $classify_list);
        $this->assign('main_title', '小视频编辑');
        $this->display();
    }

    //分类搜索列表
    public function search_classify()
    {
        $title = strim($_REQUEST['key']);
        $map['is_effect'] = 1;
        $map['title'] = array('like', '%' . $title . '%');
        $classify_list = M('weibo_classify')->where($map)->select();
        $this->ajaxReturn($classify_list);
    }

    //编辑(审核)小视频
    public function update()
    {
        $data['id'] = $_REQUEST['id'];
        $data['is_open_tencent'] = $_REQUEST['is_open_tencent'];//是否开启自动获取腾讯云标签分类
        $data['classify_id'] = $_REQUEST['classify_id'];
        $data['is_adv'] = $_REQUEST['is_adv'];
        $data['is_hot'] = $_REQUEST['is_hot'];
        $data['status'] = $_REQUEST['status'];
        $tag_ids = $_REQUEST['tag_ids'];
        $check_status = $_REQUEST['check_status'];

        if(!($data['is_open_tencent'] ==1 && $check_status!=1)){
            if (empty($data['classify_id'])) {
                $this->error('请选择分类', 0, L("UPDATE_FAILED"));
            }
            if (count($tag_ids) == 0) {
                $this->error('请选择标签', 0, L("UPDATE_FAILED"));
            }
        }

        if ($data['is_adv'] == null) {
            $this->error('请选择是否商业广告', 0, L("UPDATE_FAILED"));
        }
        if ($data['status'] == null) {
            $this->error('请选择状态', 0, L("UPDATE_FAILED"));
        }

        $tag_titles = M('weibo_tag')->where(array('is_effect' => 1))->getField('id,title');

        $tags[] = array();
        foreach ($tag_ids as $k => $v) {
            $tags[$k]['id'] = $tag_ids[$k];
            $tags[$k]['title'] = $tag_titles[$tag_ids[$k]];
        }
        $data['tags'] = json_encode($tags,JSON_UNESCAPED_UNICODE);



        if($data['is_open_tencent'] ==1 && $check_status!=1 ){
            $file_id = M('Weibo')->where(array('data'=>$weibo_info['data']))->getField('file_id');
            if(!empty($file_id)){
                $this->tencent_cloud($file_id);
            }
        }

        $data['check_status'] = 1;//已审核
        $res = M('weibo')->save($data);
		//小视频信息
		$weibo_info = M('Weibo')->field('user_id,tags,audio_id,status,data')->find($data['id']);
		if ($data['status'] == 1) {
			//5发布视频
			fanwe_require(APP_ROOT_PATH . 'mapi/xr/core/common.php');
			$memo = "用户id".$weibo_info['user_id']."发布小视频,小视频id为" . $data['id'];
			create_weight($weibo_info['user_id'], $data['id'], $memo, 5);
		}
        if ($res !== false) {
            $this->success(L("UPDATE_SUCCESS"),u("WeiboList/index"));
        } else {
            $this->error(L("UPDATE_FAILED"), 0, L("UPDATE_FAILED"));
        }

    }

    //小视频分析(获取腾讯云分类和标签)
    public function tencent_cloud($file_id)
    {

        $m_config = load_auto_cache("m_config");
        $secret_id = $m_config['qcloud_secret_id'];
        $secret_key = $m_config['qcloud_secret_key'];

        try {

            $cred = new Credential($secret_id, $secret_key);
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint("vod.tencentcloudapi.com");

            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            $client = new VodClient($cred, "", $clientProfile);

            $req = new ProcessMediaRequest();

            $params = '{"FileId":"'.$file_id.'","AiAnalysisTask":{"Definition":13033}}';
            $req->fromJsonString($params);
            $resp = $client->ProcessMedia($req);
//            print_r($resp->toJsonString());
        }
        catch(TencentCloudSDKException $e) {
//            echo $e;
        }

    }






}