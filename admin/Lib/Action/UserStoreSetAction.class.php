<?php

class UserStoreSetAction extends CommonAction
{
    public function index()
    {
        $store_condition = M('store_condition')->select();
        $this->assign ( 'list', $store_condition );
        $this->assign('main_title','店铺申请条件配置');
        $this->display ();
    }
    public function edit()
    {
        $id = $_REQUEST ['id'];
        $store_condition = M('store_condition')->where(array('id'=>$id))->find();
        $this->assign ( 'store_condition', $store_condition );
        $this->assign('main_title','店铺申请条件配置编辑');
        $this->display ();
    }

    //更新
    public function update() {
        $data['id'] = $_REQUEST['id'];
        $data['title'] = $_REQUEST['title'];
        $data['content'] = $_REQUEST['content'];
        $data['value'] = $_REQUEST['value'];
        $data['is_effect'] = $_REQUEST['is_effect'];
        $data['img'] = $_REQUEST['img'];

        if(empty($data)){
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        }
        $re = M('store_condition')->save($data);
        if(false === $re ) {
            $this->error(L("UPDATE_FAILED"),0,L("UPDATE_FAILED"));
        } else {
            //成功提示
            $this->success(L("UPDATE_SUCCESS"));
        }
    }


    //设置状态
    public function set_effect()
    {
        $id = intval($_REQUEST['id']);
        $c_is_effect = M('store_condition')->where("id=".$id)->getField("is_effect");  //当前状态
        $n_is_effect = $c_is_effect == 0 ? 1 : 0; //需设置的状态
        $result=M('store_condition')->where("id=".$id)->setField("is_effect",$n_is_effect);
        $this->ajaxReturn($n_is_effect,l("SET_BAN_".$n_is_effect),1);

    }



}