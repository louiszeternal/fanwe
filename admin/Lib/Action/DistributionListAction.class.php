<?php
class DistributionListAction extends CommonAction
{
	public function index()
	{
		$data = $_REQUEST;
		$user_id = intval($_REQUEST['id']);
		$user_info = M("User")->getById($user_id);
		$where = "1=1";
		$model = D ("yd_distribution_log");

		//查询ID
		if(strim($data['from_user_id'])!=''){
			$parameter.= "l.from_user_id=".intval($data['from_user_id']). "&";
			$sql_w .= "l.from_user_id=".intval($data['from_user_id'])." and ";
		}
		if(strim($data['to_user_id'])!=''){
			$parameter.= "l.to_user_id=".intval($data['to_user_id']). "&";
			$sql_w .= "l.to_user_id=".intval($data['to_user_id'])." and ";
		}

		//查询昵称
		if(trim($data['nick_name'])!='')
		{
			$parameter.= "u.nick_name like " . urlencode ( '%'.trim($data['nick_name']).'%' ) . "&";
			$sql_w .= "u.nick_name like '%".trim($data['nick_name'])."%' and ";

		}
		$map = $this->com_search(); //获取时间搜索状态
		//查看是否有进行时间搜索
		if($map['start_time'] != '' && $map['end_time'] != ''){
			$parameter.=" l.create_time between '". $map['start_time'] . "' and '". $map['end_time'] ."'&";
			$sql_w .=" l.create_time between '". $map['start_time']. "' and '". $map['end_time'] ."' and ";
		}
		$sql_str = "SELECT l.id,l.from_user_id,l.coin,l.create_time,l.msg,l.type,l.coin_ins_success,l.to_user_id,u.nick_name
                         FROM   ".DB_PREFIX."yd_distribution_log as l
                         LEFT JOIN ".DB_PREFIX."user AS u  ON l.from_user_id = u.id
                         WHERE $where "."  and ".$sql_w." 1=1  ";
		$count_sql = "SELECT count(l.id) as tpcount
                         FROM   ".DB_PREFIX."yd_distribution_log as l
                         LEFT JOIN ".DB_PREFIX."user AS u  ON l.from_user_id = u.id
                         WHERE $where "."  and ".$sql_w." 1=1  ";
		$volist = $this->_Sql_list($model,$sql_str,'&'.$parameter,'id',0,$count_sql);
		$sum_list = $GLOBALS['db']->getAll($sql_str);
		$sum = array ('invite_sum' => 0, 'pay_sum' => 0, 'error_sum' => 0);
		foreach($sum_list as $k=>$v){
			$sum_list[$k]['create_time'] = to_date($sum_list[$k]['create_time'],'Y-m-d H:i:s');
			switch ($v['type']) {
				case 30:
					$sum['invite_sum'] += $v['coin'];
					break;
				case 31:
					$sum['pay_sum'] += $v['coin'];
					break;
			};
			if ($v['coin_ins_success'] == 1) {
				$sum['error_sum'] += $v['coin'];
			}
		}
		$this->assign("user_info",$user_info);
		$this->assign("list",$volist);
		$this->assign("sum",$sum);
		$this->display ();
		return;
	}
}