<?php
define("IS_DEBUG",1);
define("DE_BUGE",1);//AES加密调试
define("SHOW_DEBUG",0);
define("SHOW_LOG",0);
define("MAX_DYNAMIC_CACHE_SIZE",1000);  //动态缓存最数量
define("SMS_TIMESPAN",60);  //短信验证码发送的时间间隔
define("SMS_EXPIRESPAN",300);  //短信验证码失效时间

define("PAI_PAGE_SIZE",10);
define("PIN_PAGE_SIZE",80);
define("PIN_SECTOR",10);
define("MAX_SP_IMAGE",20); //商家的最大图片量
define("MAX_LOGIN_TIME",0);  //登录的过期时间,单位：秒
define("ORDER_DELIVERY_EXPIRE",7);  //延期收货天

define("AES_DECRYPT_KEY",'fanwe');
define("SESSION_TIME",3600*1); //session超时时间
define("SMS_MOBILE_SEND_COUNT",9); //每个手机号每天最多只能发XX条
define("SMS_IP_SEND_COUNT",100); //每个IP每小时最多只能发XX条

/*
 * 是否开启 MYSQL 和 REDIS的长链接
 * 在高并发的情况下开启，链接数 会和 php进程数一致，nginx要重启服务后生效
 * 默认是关闭的
 */
define('IS_LONG_LINK',false);

define('IS_LONG_LINK_MYSQL',false);
/*
 * 是否开启事务
 */
define('IS_REDIS_WORK',false);

//靓号开关配置
define('OPEN_LUCK_NUM',1);//是否开启靓号

//每日首次登录赠送积分配置
define('OPEN_LOGIN_SEND_SCORE',0);//是否开启每日首次登录赠送积分

//每次登录时升级提示
define('OPEN_UPGRADE_PROMPT',1);//是否开启每次登录时升级提示

//是否开启排行榜
define('OPEN_RANKING_LIST',1);//是否开启排行榜

//是否开启分享加印票或是钻石
define('OPEN_SHARE_EXPERIENCE',1);//是否开启分享加印票或是钻石

//支付宝一键认证
define('OPEN_AUTHENT_ALIPAY',0);//支付宝一键认证  0 关闭 1 开启

//房间隐藏
define('OPEN_ROOM_HIDE',0);//房间隐藏  0 关闭 1 开启

//监控页面发送警告
define('OPEN_WARNING',1);//监控页面发送警告  0 关闭 1 开启

//后台手机验证码
define("OPEN_CHECK_ACCOUNT",0); //后台手机验证码功能

//是否游客登录
define('VISITORS',0);// 0 关闭 1 开启

//主播单独设置提现比例
define('OPEN_SCALE',0);// 0 关闭 1 开启

//是否开启腾讯云视频
define('TECENT_VIDEO',1);//是否开启腾讯云视频

// 强制开启手机绑定
define('OPEN_FORCE_MOBILE', 1); // 0 关闭 1 开启

define('CHECK_VIDEO',1); ;// 0 关闭 1 开启; //审核视频

//限时开放直播
define('OPEN_LIMINT_TIME',1);//限时开放直播  0 关闭 1 开启

define('CHANGE_NAV', 'default'); //默认为0，若有填写则为开启不同后台，xr为鲜肉直播

//主播收礼物日志
define('USER_PROP_CLOSED',1);//是否开启收礼物日志 0 关闭 1 开启

//手动置顶
define('OPEN_STICK',1);//是否开启收手动置顶 0 关闭 1 开启

//后台设置多少级才可发言
define('OPEN_SPEAK_LEVEL',1);// 0 关闭 1 开启

// 每日任务开关
define('OPEN_MISSION',1);// 0 关闭 1 开启

//大型礼物赠送——全服飞屏通告
define('PROP_NOTIFY',1);	// 0 关闭 1 开启

//上传视频到OSS
define('UPLOAD_OSS',1);	// 0 关闭 1 开启

define('EXAMINE_TIME',1); // 审核时间

// 开启小视屏
define('OPEN_SVIDEO_MODULE', 1);//0关闭 1开启

define("OPEN_VEHICLE_MODULE",1); //是否开启座驾 0 否 1是

define('OPEN_PLUGIN',1);//主播禁用插件

//是否付费直播
define('OPEN_LIVE_PAY',1);//是否付费直播

define('LIVE_PAY_TIME',1);//是否开启按时付费

define('LIVE_PAY_SCENE',1);//是否开启按场付费

//公会模块开关
define('OPEN_SOCIETY_MODULE',1);//公会模块开关 0 关闭 1 开启

define('SHARE_DISTRIBUTION',1);


define('SHOPPING_GOODS',1);//是否开启购物
define('SHOP_SHOPPING_CART',0);//是否开启购物车
define('OPEN_PODCAST_GOODS',1);//是否打开主播小店

define('PAI_MAX_VIOLATIONS',2);//竞拍 一个月最大违规次数(手动退出竞拍，或者直播掉线)
define('PAI_CLOSE_VIOLATIONS',15);//多久之后解禁(天)
define('SHOW_USER_ORDER',1);//是否显示【我的订单】 0否 1是
define('SHOW_USER_PAI',0);//是否显示【我的竞拍】 0否 1是
define('SHOW_PODCAST_ORDER',1);//是否显示星店订单(主播) 0否 1是
define('SHOW_PODCAST_PAI',0);//是否显示竞拍管理(主播) 0否 1是
define('SHOW_PODCAST_GOODS',1);//是否显示 商品管理（主播） 0否 1是
define('MAX_PAI_PAY_TIME',15*60);//竞拍付款倒计时时长，单位秒
define('MAX_USER_CONFIRM_TIME',3*24*3600);//用户确认约会完成倒计时时长，单位秒
define('MAX_PODCAST_CONFIRM_TIME',1*24*3600);//主播确认约会完成倒计时时长，单位秒
define('MAX_USER_CONFIRM_VIRTUAL_TIME',10*24*3600);//用户确认收货倒计时时长，单位秒
define('MAX_PODCAST_CONFIRM_VIRTUAL_TIME',3*24*3600);//主播确认发货倒计时时长，单位秒
define("FANWE_APP_ID_YM",'');//源码用户的app_id；需手动填写
define("FANWE_AES_KEY_YM",'');//源码用户的aes_key；需手动填写
define('OPEN_BURST', 1); //连发礼物
define('OPEN_LIANMAI', 0); //是否开启PK
define('OPEN_GAME_MODULE',1);//是否开启游戏
define('OPEN_DIAMOND_GAME_MODULE',1);//是否开启钻石游戏
define('SHOW_IS_GAMING',1);//首页显示正在游戏中
define('GAME_GAIN_FOR_ALERT',1);//游戏获胜弹幕
define('GAME_AUTO_START',1);//自动游戏开关







