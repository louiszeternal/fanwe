<?php

class vehicle_id_auto_cache extends auto_cache{
	public function load($param)
	{
		$id = intval($param['id']);
		$key = "vehicle:".$id;
		$list = $GLOBALS['cache']->get($key);
		if($list === false)
		{
			$sql = "select id as vehicle_id,name,diamonds as money,icon,is_animated from ".DB_PREFIX."vehicle where is_effect = 1 and id = {$id} order by sort desc";
			$list = $GLOBALS['db']->getRow($sql);
			$list['icon'] = get_spec_image($list['icon']);
			if ($list['is_animated'] == 1 || $list['is_animated'] == 3){
				//要缓存getAllCached
				$sql = "select id,url,play_count,delay_time,duration,show_user,type,gif_gift_show_style,animation_type,animation_width,animation_height from ".DB_PREFIX."vehicle_animated where prop_id = ".$id." order by sort desc";
				$anim_list = $GLOBALS['db']->getAll($sql);
				foreach ( $anim_list as $k => $v )
				{
					$anim_list[$k]['url'] = get_spec_image($v['url']);
				}
				
				$list['anim_cfg'] = $anim_list;
				//$ext['sql'] = $sql;
			}else{
				$list['anim_cfg'] = array();
			}


			$GLOBALS['cache']->set($key,$list,1800);
		}else{
			//echo 'cache';
		}
		return $list;
	}
	
	public function rm($param)
	{
		$id = intval($param['id']);
		$key = "vehicle:".$id;
		$GLOBALS['cache']->rm($key);
	}
	
	public function clear_all($param)
	{
		$id = intval($param['id']);
		$key = "vehicle:".$id;
		$GLOBALS['cache']->rm($key);
	}
}
?>