<?php

class svideo_comment_list_auto_cache extends auto_cache{
	private $key = "select:weibo_comment_list:";
	public function load($param)
	{
		fanwe_require(APP_ROOT_PATH.'mapi/xr/core/common.php');
		$this->key .= md5(serialize($param));
		$page=$param['page']>0?$param['page']:1;
		$user_id = intval($param['user_id']);
		$page_size=$param['page_size']>0?$param['page_size']:20;
//		$page_size = $page * $page_size;
//		$limit = 0 . "," . $page_size;
		$limit = (($page-1)*$page_size).",".$page_size;

		$child_page_size=$param['child_page_size']>0?$param['child_page_size']:5;
		$init_child_page_size=$param['child_page_size']>0?$param['child_page_size']:5;
//		$child_limit = 0 . "," . $child_page * $init_child_page_size;
		$comment_id = $param['comment_id'];
		$child_page = $param['child_page']>0?$param['child_page']:$page;
		$child_limit = (($child_page-1)*$child_page_size).",".$init_child_page_size;
		if ($comment_id > 0) {
			$child_page = $param['child_page']>0?$param['child_page']:$page;
			$child_limit = (($child_page-1)*$child_page_size).",".$init_child_page_size;
		}
		$weibo_id = intval($param['weibo_id']);
		$key_bf = $this->key.'_bf';
		
		//$list = $GLOBALS['cache']->get($this->key,true);
		$list = false;
		if ($list === false) {
			$is_ok =  $GLOBALS['cache']->set_lock($this->key);
			if(false){
				$list = $GLOBALS['cache']->get($key_bf,true);
			}else{
				$all_comment = $GLOBALS['db']->getAll("SELECT user_id,comment_id FROM ".DB_PREFIX."weibo_comment_digg WHERE weibo_id = {$weibo_id} and user_id = {$user_id} ");
				//评论列表
				if ($comment_id == '') {
					$comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and to_comment_id = 0 order by wc.comment_id desc limit $limit");
					$comment_list_count = $GLOBALS['db']->getOne("select count(wc.comment_id) from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and to_comment_id = 0 order by wc.comment_id ");
					if($comment_list){
						$to_comment_user = array();
						foreach($comment_list as $k=>$v){
							if($v){
								$comment_list[$k]['has_digg'] = 0;
								foreach ($all_comment as $comment_key => $comment_value) {
									if ($v['comment_id'] == $comment_value['comment_id']) {
										if ($user_id == $comment_value['user_id']) {
											$comment_list[$k]['has_digg'] = 1;
											break;
										}
									}
								}
								$comment_list[$k]['head_image'] = get_spec_image($v['head_image']);
								$comment_list[$k]['left_time'] = time_tran($v['create_time']);
								if($v['to_comment_id']){
									$comment_list[$k]['is_to_comment'] =1;
								}else{
									$comment_list[$k]['is_to_comment'] = 0;
								}
								$comment_list[$k]['to_nick_name'] = '';
								$child_comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2) order by wc.comment_id desc limit $child_limit");

								if ($child_page > 1 && $comment_id > 0 && $v['comment_id'] == $comment_id) {
//									$child_page_size = $child_page * $child_page_size;
//									$page_child_limit = 0 . "," . $child_page * $child_page_size;
									$page_child_limit = (($child_page-1)*$child_page_size).",".$init_child_page_size;
									$child_comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2) order by wc.comment_id desc limit $child_limit");
								}

								$child_comment_count = $GLOBALS['db']->getOne("select count(id) from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2)");

								foreach ($child_comment_list as $kk => $vv) {
									$child_comment_list[$kk]['head_image'] = get_spec_image($vv['head_image']);
									$child_comment_list[$kk]['left_time'] = time_tran($vv['create_time']);
									if($vv['to_comment_id']){
										$child_comment_list[$kk]['is_to_comment'] =1;
										$to_comment_user[] = $vv['to_user_id'];
									}else{
										$child_comment_list[$kk]['is_to_comment'] = 0;
									}
									$child_comment_list[$kk]['to_nick_name'] = '';
									if ($vv['to_user_id']) {
										$to_nick_name = $GLOBALS['db']->getOne("select nick_name from ".DB_PREFIX."user where id = {$vv['to_user_id']}");
										$child_comment_list[$kk]['to_nick_name'] = $to_nick_name;
									}
									$child_comment_list[$kk]['has_digg'] = 0;
									foreach ($all_comment as $comment_key => $comment_value) {
										if ($vv['comment_id'] == $comment_value['comment_id']) {
											if ($user_id == $comment_value['user_id']) {
												$child_comment_list[$kk]['has_digg'] = 1;
												break;
											}
										}
									}

								}
								$comment_list[$k]['child_comment_list'] = $child_comment_list;
								$comment_list[$k]['child_comment_count'] = $child_comment_count;
								$comment_list[$k]['page_info']['has_next'] = 0;
								$comment_list[$k]['page_info']['page'] = count($child_comment_list) > ($child_page - 1) * $init_child_page_size ? $child_page : 1;
								if ($child_comment_count > $child_page_size * $child_page) {
									$comment_list[$k]['page_info']['has_next'] = 1;
								}
							}
						}
					}
				} else {
					$comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$comment_id} and comment_level in (0,1,2) order by wc.comment_id desc limit $child_limit");
					$comment_list_count = $GLOBALS['db']->getOne("select count(wc.comment_id) from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$comment_id} and comment_level in (0,1,2) order by wc.comment_id desc");

					if($comment_list){
						$to_comment_user = array();
						foreach($comment_list as $k=>$v){
							if($v){
								$comment_list[$k]['has_digg'] = 0;
								foreach ($all_comment as $comment_key => $comment_value) {
									if ($v['comment_id'] == $comment_value['comment_id']) {
										if ($user_id == $comment_value['user_id']) {
											$comment_list[$k]['has_digg'] = 1;
											break;
										}
									}
								}
								$comment_list[$k]['head_image'] = get_spec_image($v['head_image']);
								$comment_list[$k]['left_time'] = time_tran($v['create_time']);
								if($v['to_comment_id']){
									$comment_list[$k]['is_to_comment'] =1;
								}else{
									$comment_list[$k]['is_to_comment'] = 0;
								}
								$comment_list[$k]['to_nick_name'] = '';
								$child_comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2) order by wc.comment_id desc limit $child_limit");

								if ($child_page > 1 && $comment_id > 0 && $v['comment_id'] == $comment_id) {
//									$child_page_size = $child_page * $child_page_size;
//									$page_child_limit = 0 . "," . $child_page * $child_page_size;
									$page_child_limit = (($child_page-1)*$child_page_size).",".$init_child_page_size;
									$child_comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2) order by wc.comment_id desc limit $child_limit");
								}

								$child_comment_count = $GLOBALS['db']->getOne("select count(id) from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2)");

								foreach ($child_comment_list as $kk => $vv) {
									$child_comment_list[$kk]['head_image'] = get_spec_image($vv['head_image']);
									$child_comment_list[$kk]['left_time'] = time_tran($vv['create_time']);
									if($vv['to_comment_id']){
										$child_comment_list[$kk]['is_to_comment'] =1;
										$to_comment_user[] = $vv['to_user_id'];
									}else{
										$child_comment_list[$kk]['is_to_comment'] = 0;
									}
									$child_comment_list[$kk]['to_nick_name'] = '';
									if ($vv['to_user_id']) {
										$to_nick_name = $GLOBALS['db']->getOne("select nick_name from ".DB_PREFIX."user where id = {$vv['to_user_id']}");
										$child_comment_list[$kk]['to_nick_name'] = $to_nick_name;
									}
									$child_comment_list[$kk]['has_digg'] = 0;
									foreach ($all_comment as $comment_key => $comment_value) {
										if ($vv['comment_id'] == $comment_value['comment_id']) {
											if ($user_id == $comment_value['user_id']) {
												$child_comment_list[$kk]['has_digg'] = 1;
												break;
											}
										}
									}

								}
								$comment_list[$k]['child_comment_list'] = $child_comment_list;
								$comment_list[$k]['child_comment_count'] = $child_comment_count;
								$comment_list[$k]['page_info']['page'] = $child_page;
								$comment_list[$k]['page_info']['has_next'] = 0;
								if ($child_comment_count > $child_page_size * $child_page) {
									$comment_list[$k]['page_info']['has_next'] = 1;
								}
							}
						}
					}
				}

				$list['list'] = $comment_list;
				$list['list_count'] = $comment_list_count;
				//$GLOBALS['cache']->set($this->key, $list, 5, true);
				
				//$GLOBALS['cache']->set($key_bf, $list, 86400, true);//备份
				//echo $this->key;
			}
 		}
 		
 		if ($list == false) $list = array();
 		
		return $list;
	}
	
	public function rm()
	{

		//$GLOBALS['cache']->clear_by_name($this->key);
	}
	
	public function clear_all()
	{
		
		//$GLOBALS['cache']->clear_by_name($this->key);
	}
}
?>

