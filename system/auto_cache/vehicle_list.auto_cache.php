<?php

class vehicle_list_auto_cache extends auto_cache{
	private $key = "vehicle:list";
	
	public function load($param)
	{
		$list = $GLOBALS['cache']->get($this->key);

		if($list === false)
		{
            $m_config =  load_auto_cache("m_config");
            $sql = "select id,name,score,diamonds,icon,sort,is_animated,gif_gift_show_style,open_time from ".DB_PREFIX."vehicle where is_effect = 1 order by sort desc";
//            if($m_config['ios_check_version'] != ''){
//                $sql = "select id,name,score,diamonds,icon,sort,is_animated,gif_gift_show_style  from ".DB_PREFIX."vehicle where is_effect = 1 and is_red_envelope<>1 order by sort desc";
//            }
			$sql = "select id as vehicle_id,name,diamonds as money,icon from ".DB_PREFIX."vehicle where is_effect = 1 order by sort desc";
            $list = $GLOBALS['db']->getAll($sql);

			foreach ( $list as $k => $v )
			{
				$list[$k]['icon'] = get_spec_image($v['icon'],200,200);
//				$list[$k]['open_time'] = explode(',',$v['open_time']);
			}
			$GLOBALS['cache']->set($this->key,$list,60);
		}
		return $list;
	}
	
	public function rm($param)
	{
		$GLOBALS['cache']->rm($this->key);
	}
	
	public function clear_all()
	{
		$GLOBALS['cache']->rm($this->key);
	}
}
?>