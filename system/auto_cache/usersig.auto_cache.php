<?php

class usersig_auto_cache extends auto_cache
{
	private $key = '';
	private $sdkappid = '';

	public function load($param)
	{
		//INSERT INTO `fanwe_m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'tim_key', '腾讯云云通信秘钥', '腾讯直播', NULL, 0, 0, NULL, NULL, '云通信秘钥-2019.10.20后创建的云通信需填写');
		$m_config = load_auto_cache("m_config");//参数
		$sdkappid = $m_config['tim_sdkappid'];

		$id = strim($param['id']);
		$key = "usersig:" . $sdkappid . ":" . $id;

		$root = $GLOBALS['cache']->get($key);

		$open_usersig_cache = intval($m_config['open_usersig_cache']);

		if ($root === false || $open_usersig_cache) {
			$private_pem_path = APP_ROOT_PATH . "system/tim/ec_key.pem";

			$root = array ();

			if ($id == '') {
				$root['error'] = "参数id不能为空";
				$root['status'] = 0;
			} else {
				require_once(APP_ROOT_PATH . 'system/tim/TimApi.php');
				require_once(APP_ROOT_PATH . 'system/tim/TimRestApi.php');

				$identifier = $m_config['tim_identifier'];

				$api = createRestAPI();
				$api->init($sdkappid, $identifier);

				$signature = get_signature();
				$expiry_after = 86400;//一天有效期

				$tim_key = $m_config['tim_key'];
				if ($tim_key > 0) {
					$this->sdkappid = $sdkappid;
					$this->key = $tim_key;
					$ret = $this->genSig($id, $expiry_after, '', false);
					if ($ret == null || strstr($ret, "failed")) {
						$root['error'] = $sdkappid . ":获取usrsig失败, 请检查后台-系统设置-移动平台配置-手机端配置-腾讯直播-云通信秘钥是否填写";
						$root['status'] = 0;
					} else {
						$root['usersig'] = $ret;
						$root['status'] = 1;
						$GLOBALS['cache']->set($key, $root, $expiry_after - 60);
					}
				} else {
					if (!file_exists($private_pem_path)) {
						$root['error'] = "私钥文件不存在:" . $private_pem_path;
						$root['status'] = 0;
						return $root;
					}
					$ret = $api->generate_user_sig((string)$id, $expiry_after, $private_pem_path, $signature);
					if ($ret == null || strstr($ret[0], "failed")) {
						$root['error'] = $sdkappid . ":获取usrsig失败, 请确保:" . $signature . " 文件有执行的权限.";
						$root['status'] = 0;
					} else {
						$root['usersig'] = $ret[0];
						$root['status'] = 1;
						$GLOBALS['cache']->set($key, $root, $expiry_after - 60);
					}
				}
			}
		}

		return $root;
	}

	/**
	 * 生成签名。
	 *
	 * @param $identifier 用户账号
	 * @param int $expire 过期时间，单位秒，默认 180 天
	 * @param $userbuf base64 编码后的 userbuf
	 * @param $userbuf_enabled 是否开启 userbuf
	 * @return string 签名字符串
	 * @throws \Exception
	 */
	private function genSig($identifier, $expire, $userbuf, $userbuf_enabled)
	{
		$curr_time = time();
		$sig_array = Array (
			'TLS.ver' => '2.0',
			'TLS.identifier' => strval($identifier),
			'TLS.sdkappid' => intval($this->sdkappid),
			'TLS.expire' => intval($expire),
			'TLS.time' => intval($curr_time)
		);
		$base64_userbuf = '';
		if (true == $userbuf_enabled) {
			$base64_userbuf = base64_encode($userbuf);
			$sig_array['TLS.userbuf'] = strval($base64_userbuf);
		}
		$sig_array['TLS.sig'] = $this->hmacsha256($identifier, $curr_time, $expire, $base64_userbuf, $userbuf_enabled);
		if ($sig_array['TLS.sig'] === false) {
			throw new \Exception('base64_encode error');
		}
		$json_str_sig = json_encode($sig_array);
		if ($json_str_sig === false) {
			throw new \Exception('json_encode error');
		}
		$compressed = gzcompress($json_str_sig);
		if ($compressed === false) {
			throw new \Exception('gzcompress error');
		}
		return $this->base64_url_encode($compressed);
	}

	/**
	 * 使用 hmac sha256 生成 sig 字段内容，经过 base64 编码
	 * @param $identifier 用户名，utf-8 编码
	 * @param $curr_time 当前生成 sig 的 unix 时间戳
	 * @param $expire 有效期，单位秒
	 * @param $base64_userbuf base64 编码后的 userbuf
	 * @param $userbuf_enabled 是否开启 userbuf
	 * @return string base64 后的 sig
	 */
	private function hmacsha256($identifier, $curr_time, $expire, $base64_userbuf, $userbuf_enabled)
	{
		$content_to_be_signed = "TLS.identifier:" . $identifier . "\n"
			. "TLS.sdkappid:" . $this->sdkappid . "\n"
			. "TLS.time:" . $curr_time . "\n"
			. "TLS.expire:" . $expire . "\n";
		if (true == $userbuf_enabled) {
			$content_to_be_signed .= "TLS.userbuf:" . $base64_userbuf . "\n";
		}
		return base64_encode(hash_hmac('sha256', $content_to_be_signed, $this->key, true));
	}

	/**
	 * 用于 url 的 base64 encode
	 * '+' => '*', '/' => '-', '=' => '_'
	 * @param string $string 需要编码的数据
	 * @return string 编码后的base64串，失败返回false
	 * @throws \Exception
	 */
	private function base64_url_encode($string)
	{
		static $replace = Array ('+' => '*', '/' => '-', '=' => '_');
		$base64 = base64_encode($string);
		if ($base64 === false) {
			throw new \Exception('base64_encode error');
		}
		return str_replace(array_keys($replace), array_values($replace), $base64);
	}

	public function rm($param)
	{
		$m_config = load_auto_cache("m_config");//参数
		$sdkappid = $m_config['tim_sdkappid'];

		$id = strim($param['id']);
		$key = "usersig:" . $sdkappid . ":" . $id;

		$GLOBALS['cache']->rm($key);
	}

	public function clear_all($param)
	{
		$m_config = load_auto_cache("m_config");//参数
		$sdkappid = $m_config['tim_sdkappid'];

		$id = strim($param['id']);
		$key = "usersig:" . $sdkappid . ":" . $id;

		$GLOBALS['cache']->rm($key);
	}

}

?>