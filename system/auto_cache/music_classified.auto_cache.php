<?php

class music_classified_auto_cache extends auto_cache{
	private $key = "music:classified";
	
	public function load($param)
	{
		$list = $GLOBALS['cache']->get($this->key);

		if($list === false)
		{
            $sql = "select id as classified_id,title from ".DB_PREFIX."music_classified where is_effect = 1 order by sort asc";
            $list = $GLOBALS['db']->getAll($sql,true,true);
			$GLOBALS['cache']->set($this->key, $list, 60, true);
		}
		if ($list == false) $list = array();
		return $list;
	}
	
	public function rm($param)
	{
		$GLOBALS['cache']->rm($this->key);
	}
	
	public function clear_all()
	{
		$GLOBALS['cache']->rm($this->key);
	}
}
?>