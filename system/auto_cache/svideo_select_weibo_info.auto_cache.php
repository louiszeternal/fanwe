<?php

class svideo_select_weibo_info_auto_cache extends auto_cache{
	private $key = "select:weibo_info:";
	public function load($param)
	{
//		$type = intval($param['type']);
//		$this->key .= $type;
		fanwe_require(APP_ROOT_PATH.'mapi/xr/core/common.php');
		$this->key .= md5(serialize($param));
		$page=$param['page']>0?$param['page']:1;
		$user_id = intval($param['user_id']);
		$page_size=$param['page_size']>0?$param['page_size']:20;
//		$page_size = $page * $page_size;
//		$limit = 0 . "," . $page_size;
		$limit = (($page-1)*$page_size).",".$page_size;
		$child_page = $param['child_page']>0?$param['child_page']:1;
		$child_page_size=$param['child_page_size']>0?$param['child_page_size']:5;
		$init_child_page_size=$param['child_page_size']>0?$param['child_page_size']:5;
		$child_limit = 0 . "," . $child_page_size;
		$comment_id = $param['comment_id'];
		$weibo_id = intval($param['weibo_id']);
		$key_bf = $this->key.'_bf';

		//$list = $GLOBALS['cache']->get($this->key,true);
		$list = false;
		if ($list === false) {
			$is_ok =  $GLOBALS['cache']->set_lock($this->key);
			if(false){
				$list = $GLOBALS['cache']->get($key_bf,true);
			}else{
				if($page >= 1 || $page_size >= 1){
					$weibo_info = $GLOBALS['db']->getRow("select w.user_id,w.id as weibo_id,u.head_image,u.is_authentication,w.content,w.red_count,w.unlike_count,w.digg_count,w.comment_count,w.video_count,w.data,w.video_width,w.video_height,u.v_icon,u.nick_name,w.sort_num,w.photo_image,u.city,w.is_top,w.price,w.type,w.create_time,u.sex,w.first_image,w.video_width,w.video_height,w.audio_id,w.repost_count,w.goods_id,w.is_podcast_goods
        from ".DB_PREFIX."weibo as w left join ".DB_PREFIX."user as u on w.user_id = u.id where w.id = ".$weibo_id);
					if(!$weibo_info){
						$root['error'] = "动态不存在!";
						$root['status'] = 0;
						api_ajax_return($root);
					}

					//3.1 小视频-查询出视频所有评论
					$all_comment = $GLOBALS['db']->getAll("SELECT user_id,comment_id FROM ".DB_PREFIX."weibo_comment_digg WHERE weibo_id = {$weibo_id} and user_id = {$user_id} ");
					//点击量过万 转换成带单位的字符串
					if ($weibo_info['video_count'] >= 10000) {
						$weibo_info['video_count'] = round($weibo_info['video_count']/10000,2)."万";
					}

					$weibo_info['v_icon'] = get_spec_image($weibo_info['v_icon'],50,50);
					$weibo_info['left_time'] = time_tran($weibo_info['create_time']);
					if($weibo_info['head_image']){
						$weibo_info['head_image'] = deal_weio_image($weibo_info['head_image']);
					}
					if($weibo_info['photo_image']){
						$weibo_info['photo_image'] = deal_weio_image($weibo_info['photo_image'],$weibo_info['type'].'_info');
					}
					$weibo_info['images_count'] = 0;
					if($weibo_info['type']=='video'){
						$weibo_info['images'] = array();
						$url = $weibo_info['data'];
						$weibo_info['video_url'] = get_file_oss_url($url);
					}else{
						$images = unserialize($weibo_info['data']);
//						if(in_array($weibo_info['weibo_id'],$order_list_array)){
//							$is_pay =1;
//						}else{
//							$is_pay =0;
//						}
						if(count($images)>0){

							$weibo_info['images'] = $images;
							$weibo_info['images_count'] = count($images);
						}else{
							$weibo_info['images'] = array();

						}
						$weibo_info['video_url'] = '';

					}
					//3.1 小视频-查询出小视频的音乐信息
					if (intval($weibo_info['audio_id']) > 0) {
//						$audio_info = $GLOBALS['db']->getRow("SELECT audio_name,artist_name,audio_image FROM ".DB_PREFIX."music_list WHERE id = {$v['audio_id']}");
						$audio_info = $GLOBALS['db']->getRow("SELECT id,audio_name,artist_name,audio_image,audio_link FROM ".DB_PREFIX."music_list WHERE id = {$weibo_info['audio_id']}");
						if ($audio_info) {
							$weibo_info['audio'] = array ();
							$weibo_info['audio']['audio_id'] = $audio_info['id'];
							$weibo_info['audio']['audio_name'] = $audio_info['audio_name'].'-'.$audio_info['artist_name']; //音乐名称-演唱者
							$weibo_info['audio']['audio_image'] = $audio_info['audio_image']; //音乐封面
							$weibo_info['audio']['audio_link'] = $audio_info['audio_link']; //音乐下载地址
						}
					}
					unset($weibo_info['audio_id']);
					$user_info = $GLOBALS['db']->getAll("SELECT head_image FROM ".DB_PREFIX."user WHERE is_robot = 1 ORDER BY RAND() LIMIT 2");
					foreach ($user_info as $k => $v) {
						$user_info[$k]['head_image'] = get_spec_image($v['head_image']);
					}
//					$weibo_info['video_commodity'] = [];
//					$weibo_info['video_commodity']['commodity_id'] = '1';
//					$weibo_info['video_commodity']['commodity_name'] = '一字肩宴会晚礼服连衣裙连衣裙连衣裙';
//					$weibo_info['video_commodity']['commodity_desc'] = '这是一个商品描述商品描述商品描述';
//					$weibo_info['video_commodity']['commodity_image'] = 'http://ilvbfanwe.oss-cn-shanghai.aliyuncs.com/public/attachment/201908/05/14/5d47cc01c9677.jpg';
//					$weibo_info['video_commodity']['commodity_user'] = $user_info;
//					$weibo_info['video_commodity']['commodity_seen'] = '187w人看过';
//					$weibo_info['video_commodity']['commodity_money'] = '9999';
//					$weibo_info['video_commodity']['commodity_url'] = 'https://www.baidu.com';

					if ($weibo_info['goods_id'] > 0) {
						//主播商品
						if ($weibo_info['is_podcast_goods'] > 0) {
							$video_commodity = $GLOBALS['db']->getRow("SELECT id,name,description,imgs,price,url,seen_num FROM ".DB_PREFIX."podcast_goods WHERE id = {$weibo_info['goods_id']}");
						} else {
							$video_commodity = $GLOBALS['db']->getRow("SELECT id as goods_id,name,description,imgs,price,url,seen_num FROM ".DB_PREFIX."goods WHERE id = {$weibo_info['goods_id']}");
						}
						$weibo_info['video_commodity'] = [];
						$video_commodity['seen_num'] = $video_commodity['seen_num'] > 10000 ? round($video_commodity['seen_num'] / 10000,2) .'W' : $video_commodity['seen_num'];
						$weibo_info['video_commodity']['commodity_id'] = $video_commodity['goods_id'];
						$weibo_info['video_commodity']['commodity_name'] = $video_commodity['name'];
						$weibo_info['video_commodity']['commodity_desc'] = $video_commodity['description'];
						$weibo_info['video_commodity']['commodity_image'] = json_decode($video_commodity['imgs'],1)[0];
						$weibo_info['video_commodity']['commodity_user'] = $user_info;
						$weibo_info['video_commodity']['commodity_seen'] = $video_commodity['seen_num'] > 0 ? $video_commodity['seen_num'] .'人看过': '';
						$weibo_info['video_commodity']['commodity_money'] = $video_commodity['price'];
						if ($video_commodity['url']) {
							$weibo_info['video_commodity']['commodity_url'] = $video_commodity['url'];
						} else {
							$weibo_info['video_commodity']['commodity_url'] = SITE_DOMAIN.APP_ROOT.'/wap/index.php?ctl=shop&act=shop_goods_details&podcast_id='.$weibo_info['user_id']."&goods_id=".$video_commodity['goods_id'];
						}
					}
					//是否正在直播中
					if(!$GLOBALS['db']->getOne("select id from ".DB_PREFIX."video where live_in = 1 and user_id=".$user_id,true,true)) {
						$video = $GLOBALS['db']->getRow("select id as room_id,group_id,live_in,user_id,head_image,live_image from " . DB_PREFIX . "video where user_id=" . $weibo_info['user_id'] . " and live_in = 1 and room_type = 3 order by sort_num desc,sort desc", true, true);
						if (intval($video['room_id'])) {
							$video['head_image'] = get_spec_image($video['head_image']);
							$video['live_image'] = get_spec_image($video['live_image']);
							if (defined('OPEN_EDU_MODULE') && OPEN_EDU_MODULE == 1) {
								$video['is_verify'] = intval($GLOBALS['db']->getOne("select is_verify from " . DB_PREFIX . "edu_video_info where video_id = " . $video['room_id']));
							}
							$weibo_info['live_video'] = $video;
						}
					}
					$weibo_info['is_show_weibo_report'] = 1; //是否显示举报动态
					$weibo_info['is_show_user_report'] = 1; //是否显示举报用户
					$weibo_info['is_show_deal_weibo'] = 1; //是否显示删除动态
					$root['info'] = $weibo_info;
					//点赞列表
					$digg_list = $GLOBALS['db']->getAll("select wc.user_id,u.v_icon,u.nick_name,u.head_image,u.is_authentication,u.sex from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 2   order by wc.comment_id desc limit 0,7");
					$digg_user_list = array();
					if($digg_list){
						foreach($digg_list as $k=>$v){
							if($v){
								$digg_user_list[] = $v['user_id'];
								$digg_list[$k]['head_image'] = get_spec_image($v['head_image']);
							}
						}
					}
					$root['digg_user_list'] = $digg_user_list;
					$root['digg_list'] = $digg_list;

					// 踩一下列表
					$unlike_list = $GLOBALS['db']->getAll("select wc.user_id,u.v_icon,u.nick_name,u.head_image,u.is_authentication from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 3 order by wc.comment_id desc limit 0,7");
					$unlike_user_list = array();
					if($unlike_list){
						foreach($unlike_list as $k=>$v){
							if($v){
								$unlike_user_list[] = $v['user_id'];
								$unlike_list[$k]['head_image'] = get_spec_image($v['head_image']);
							}
						}
					}
					$root['unlike_user_list'] = $unlike_user_list;
					$root['unlike_list'] = $unlike_list;
				}

				//评论列表
				$comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and to_comment_id = 0 order by wc.comment_id desc limit $limit");
				$comment_list_count = $GLOBALS['db']->getOne("select count(wc.comment_id) from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and to_comment_id = 0 order by wc.comment_id ");
				if($comment_list){
					$to_comment_user = array();
					foreach($comment_list as $k=>$v){
						if($v){
							$comment_list[$k]['has_digg'] = 0;
							foreach ($all_comment as $comment_key => $comment_value) {
								if ($v['comment_id'] == $comment_value['comment_id']) {
									if ($user_id == $comment_value['user_id']) {
										$comment_list[$k]['has_digg'] = 1;
										break;
									}
								}
							}
							$comment_list[$k]['head_image'] = get_spec_image($v['head_image']);
							$comment_list[$k]['left_time'] = time_tran($v['create_time']);
							$comment_list[$k]['content'] = emoji_decode($v['content']);
							if($v['to_comment_id']){
								$comment_list[$k]['is_to_comment'] =1;
							}else{
								$comment_list[$k]['is_to_comment'] = 0;
							}
							$comment_list[$k]['to_nick_name'] = '';
							$child_comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2) order by wc.comment_id desc limit $child_limit");

							if ($child_page > 1 && $comment_id > 0 && $v['comment_id'] == $comment_id) {
								$page_child_limit = (($child_page-1)*$child_page_size).",".$init_child_page_size;
//								$page_child_limit = 0 . "," . $child_page * $child_page_size;
								$child_comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,wc.digg_count from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2) order by wc.comment_id desc limit $page_child_limit");
							}

							$child_comment_count = $GLOBALS['db']->getOne("select count(id) from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$weibo_id." and type = 1 and is_del = 0 and father_comment_id = {$v['comment_id']} and comment_level in (0,1,2)");

							foreach ($child_comment_list as $kk => $vv) {
								$child_comment_list[$kk]['head_image'] = get_spec_image($vv['head_image']);
								$child_comment_list[$kk]['left_time'] = time_tran($vv['create_time']);
								$child_comment_list[$kk]['content'] = emoji_decode($vv['content']);
								if($vv['to_comment_id']){
									$child_comment_list[$kk]['is_to_comment'] =1;
									$to_comment_user[] = $vv['to_user_id'];
								}else{
									$child_comment_list[$kk]['is_to_comment'] = 0;
								}
								$child_comment_list[$kk]['to_nick_name'] = '';
								if ($vv['to_user_id']) {
									$to_nick_name = $GLOBALS['db']->getOne("select nick_name from ".DB_PREFIX."user where id = {$vv['to_user_id']}");
									$child_comment_list[$kk]['to_nick_name'] = $to_nick_name;
								}
								$child_comment_list[$kk]['has_digg'] = 0;
								foreach ($all_comment as $comment_key => $comment_value) {
									if ($vv['comment_id'] == $comment_value['comment_id']) {
										if ($user_id == $comment_value['user_id']) {
											$child_comment_list[$kk]['has_digg'] = 1;
											break;
										}
									}
								}

							}
							$comment_list[$k]['child_comment_list'] = $child_comment_list;
							$comment_list[$k]['child_comment_count'] = $child_comment_count;
							//  count($child_comment_list) >= $child_page * $init_child_page_size ? $child_page : 1;
							$comment_list[$k]['page_info']['page'] = $child_page;
							$comment_list[$k]['page_info']['has_next'] = 0;
							if ($child_comment_count > $child_page_size * $child_page) {
								$comment_list[$k]['page_info']['has_next'] = 1;
							}

						}
					}
//					if(count($to_comment_user)>0){
//						$user_list = $GLOBALS['db']->getAll("select id,nick_name from ".DB_PREFIX."user where id in (".implode(',',$to_comment_user).")");
//						$user_array = array();
//						foreach($user_list as $k=>$v){
//							$user_array[$v['id']] = $v['nick_name'];
//						}
//						foreach($comment_list as $k=>$v){
//							foreach ($v['child_comment_list'] as $kk => $vv) {
//								if($vv['to_user_id']){
//									$comment_list[$k]['child_comment_list'][$kk]['to_nick_name'] = $user_array[$v['to_user_id']];
//								}
//							}
//						}
//					}
				}

				$root['comment_list'] = $comment_list;
//				// 3.1 小视频-同款商品
//				$root['video_commodity'] = [];
//				$root['video_commodity']['commodity_id'] = '1';
//				$root['video_commodity']['commodity_name'] = '一字肩宴会晚礼服连衣裙连衣裙连衣裙';
//				$root['video_commodity']['commodity_desc'] = '这是一个商品描述商品描述商品描述';
//				$root['video_commodity']['commodity_image'] = 'http://ilvbfanwe.oss-cn-shanghai.aliyuncs.com/public/attachment/201908/05/14/5d47cc01c9677.jpg';
//				$root['video_commodity']['commodity_user'] = $user_info;
//				$root['video_commodity']['commodity_seen'] = '187w人看过';
//				$root['video_commodity']['commodity_money'] = '9999';
//				$root['video_commodity']['commodity_url'] = 'https://www.baidu.com';
				$root['comment_list_count'] = $comment_list_count;
				$list = $root;
				//$GLOBALS['cache']->set($this->key, $list, 5, true);

				//$GLOBALS['cache']->set($key_bf, $list, 86400, true);//备份
				//echo $this->key;
			}
		}

		if ($list == false) $list = array();

		return $list;
	}

	public function rm()
	{

		//$GLOBALS['cache']->clear_by_name($this->key);
	}

	public function clear_all()
	{

		//$GLOBALS['cache']->clear_by_name($this->key);
	}
}
?>