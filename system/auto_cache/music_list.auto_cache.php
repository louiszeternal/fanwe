<?php

class music_list_auto_cache extends auto_cache{
	private $key = "music:list:";
	public function load($param)
	{
		$key_bf = $this->key.'_bf';
		$classified_id = $param['classified_id'] ? $param['classified_id'] : 0;
		if($classified_id){
			$this->key .= '_' . 'classified_id:'.$classified_id;
		}
		$list = $GLOBALS['cache']->get($this->key,true);
		if ($list === false) {
			$is_ok =  $GLOBALS['cache']->set_lock($this->key);
			if(!$is_ok){
				$list = $GLOBALS['cache']->get($key_bf,true);
			}else{
				$m_config =  load_auto_cache("m_config");//初始化手机端配置
				$sql = "SELECT id as audio_id,audio_name,artist_name,audio_link,time_len,classified_id FROM ".DB_PREFIX."music_list WHERE is_effect = 1 ORDER BY sort DESC";
				if ($classified_id > 0) {
					$sql = "SELECT id as audio_id,audio_name,artist_name,audio_link,time_len,classified_id FROM ".DB_PREFIX."music_list WHERE is_effect = 1 AND classified_id = {$classified_id} ORDER BY  sort DESC";
				}

				$list = $GLOBALS['db']->getAll($sql,true,true);
				$GLOBALS['cache']->set($this->key, $list, 10, true);
				$GLOBALS['cache']->set($key_bf, $list, 86400, true);//备份
			}
 		}
 		
 		if ($list == false) $list = array();
		return $list;
	}
	
	public function rm()
	{
		$GLOBALS['cache']->rm($this->key);
	}
	
	public function clear_all()
	{
		$GLOBALS['cache']->rm($this->key);
	}
}
?>