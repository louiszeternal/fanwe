<?php

class prop_burst_auto_cache extends auto_cache{
	private $key = "prop:burst";
	
	public function load($param)
	{
		$new_list = $GLOBALS['cache']->get($this->key);
		if ($new_list == false || true) {
			$sql = "select id as burst_id,name,num from ".DB_PREFIX."prop_burst where is_effect = 1 order by sort asc";
			$list = $GLOBALS['db']->getAll($sql,true,true);
			$new_list = array();
			foreach ($list as $k => $v) {
				$new_list[$v['burst_id']]['burst_id'] = $v['burst_id'];
				$new_list[$v['burst_id']]['name'] = $v['name'];
				$new_list[$v['burst_id']]['num'] = $v['num'];
			}
			$GLOBALS['cache']->set($this->key, $new_list);
		}
		if ($new_list == false) $new_list = array();

		return $new_list;
	}
	
	public function rm($param)
	{
		$GLOBALS['cache']->rm($this->key);
	}
	
	public function clear_all()
	{
		$GLOBALS['cache']->rm($this->key);
	}
}
?>