<?php

class prop_list_auto_cache extends auto_cache{
	private $key = "prop:list";

	public function load($param)
	{
		$list = $GLOBALS['cache']->get($this->key);

		if($list === false)
		{
			$m_config =  load_auto_cache("m_config");
			if(intval(OPEN_REWARD_GIFT)){
				if(file_exists(APP_ROOT_PATH.'mapi/lib/core/award_function.php')) {
					fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/award_function.php');
					$list_award = get_award_info();
				}
			}
			$field_str = "id,name,score,diamonds,icon,pc_icon,pc_gif,ticket,is_much,sort,is_red_envelope,is_animated,anim_type,gif_gift_show_style";
			if(intval(OPEN_REWARD_GIFT)&&intval($list_award['is_open_award'])==1){
				$field_str .=',is_award ';
			}
			$sql = "select ".$field_str." from ".DB_PREFIX."prop where is_effect = 1 order by sort desc";
			if($m_config['ios_check_version'] != ''){
				$sql = "select ".$field_str." from ".DB_PREFIX."prop where is_effect = 1 and is_red_envelope<>1 order by sort desc";
			}
			$list = $GLOBALS['db']->getAll($sql,true,true);

			foreach ( $list as $k => $v )
			{
				$list[$k]['icon'] = get_spec_image($v['icon']);
				$list[$k]['ticket'] = intval($v['ticket']) ;
//				$list[$k]['svga_width'] = intval($v['svga_width']);
//				$list[$k]['svga_height'] = intval($v['svga_height']);
				$list[$k]['score_fromat'] = '+'.$v['score'].'经验值';
			}

			$GLOBALS['cache']->set($this->key,$list);
		}

		return $list;
	}

	public function rm($param)
	{
		$GLOBALS['cache']->rm($this->key);
	}

	public function clear_all()
	{
		$GLOBALS['cache']->rm($this->key);
	}
}
?>