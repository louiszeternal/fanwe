<?php

class new_prop_list_auto_cache extends auto_cache{
	private $key = "new:prop:list";

	public function load($param)
	{
		$list = $GLOBALS['cache']->get($this->key);
		if($list === false)
		{
			$list = array ();
			$m_config =  load_auto_cache("m_config");
			if(intval(OPEN_REWARD_GIFT)){
				if(file_exists(APP_ROOT_PATH.'mapi/lib/core/award_function.php')) {
					fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/award_function.php');
					$list_award = get_award_info();
				}
			}
			$field_str = "id,name,score,diamonds,icon,pc_icon,pc_gif,ticket,is_much,sort,is_red_envelope,is_animated,anim_type,gif_gift_show_style";
			if(intval(OPEN_REWARD_GIFT)&&intval($list_award['is_open_award'])==1){
				$field_str .=',is_award ';
			}
			if(defined('OPEN_LUCKY_PROP')&&OPEN_LUCKY_PROP){
				$where =' and is_animated <>3';
			}
			if (defined('OPEN_BURST')&&OPEN_BURST && $m_config['open_burst'] == 1) {
				$field_str .=',burst_id ';
			}
			$sql = "select ".$field_str." from ".DB_PREFIX."prop where is_effect = 1 $where order by sort desc";
			if($m_config['ios_check_version'] != ''){
				$sql = "select ".$field_str." from ".DB_PREFIX."prop where is_effect = 1 $where and is_red_envelope<>1 order by sort desc";
			}
			$prop = $GLOBALS['db']->getAll($sql,true,true);


			if (defined('OPEN_BURST')&&OPEN_BURST && $m_config['open_burst'] == 1) {
				//连发配置列表
				$burst = load_auto_cache('prop_burst');
			}
			foreach ($prop as $k => $v)
			{
				$prop[$k]['icon'] = get_spec_image($v['icon']);
				$prop[$k]['ticket'] = intval($v['ticket']) ;
				$prop[$k]['score_fromat'] = '+'.$v['score'].'经验值';

				if ($v['burst_id'] != '') {
					$burst_id = explode(',', $v['burst_id']);
					foreach ($burst_id as $kk => $vv) {
						$prop[$k]['burst'][] = $burst[$vv];
					}
				}
			}

			//幸运礼物
			if(defined('OPEN_LUCKY_PROP')&&OPEN_LUCKY_PROP){
				$field_str .=',is_lucky_prop ';
				$sql = "select ".$field_str." from ".DB_PREFIX."prop where is_effect = 1 and is_animated =3 order by sort desc";
				if($m_config['ios_check_version'] != ''){
					$sql = "select ".$field_str." from ".DB_PREFIX."prop where is_effect = 1 and is_animated =3 and is_red_envelope<>1 order by sort desc";
				}
				$lucky_prop = $GLOBALS['db']->getAll($sql,true,true);
				foreach ( $lucky_prop as $k => $v )
				{
					$lucky_prop[$k]['icon'] = get_spec_image($v['icon']);
					$lucky_prop[$k]['ticket'] = intval($v['ticket']) ;
					$lucky_prop[$k]['score_fromat'] = '+'.$v['score'].'经验值';
					if ($v['burst_id'] != '') {
						$burst_id = explode(',', $v['burst_id']);
						foreach ($burst_id as $kk => $vv) {
							$lucky_prop[$k]['burst'][] = $burst[$vv];
						}
					}
				}
				$list['lucky_prop'] = $lucky_prop;
			}
			$list['prop'] = $prop;
			$GLOBALS['cache']->set($this->key,$list);
		}
		return $list;
	}

	public function rm($param)
	{
		$GLOBALS['cache']->rm($this->key);
	}

	public function clear_all()
	{
		$GLOBALS['cache']->rm($this->key);
	}
}
?>