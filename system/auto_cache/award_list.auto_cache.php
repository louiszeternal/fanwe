<?php
// +----------------------------------------------------------------------
// | Fanwe 方维直播系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://www.fanwe.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: &雲飞水月& (172231343@qq.com)
// +----------------------------------------------------------------------

class award_list_auto_cache extends auto_cache{
    private $key = "award:list";

    public function load($param)
    {
        $list = $GLOBALS['cache']->get($this->key);

        if($list === false)
        {

            $sql = "select id,name,multiple,probability from ".DB_PREFIX."prop where is_effect = 1 order by id desc";
            $list = $GLOBALS['db']->getAll($sql,true,true);
            $GLOBALS['cache']->set($this->key,$list);
        }

        return $list;
    }

    public function rm($param)
    {
        $GLOBALS['cache']->rm($this->key);
    }

    public function clear_all()
    {
        $GLOBALS['cache']->rm($this->key);
    }
}