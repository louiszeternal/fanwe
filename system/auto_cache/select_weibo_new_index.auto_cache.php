<?php

class select_weibo_new_index_auto_cache extends auto_cache{
	private $key = "select:weibo_mew_index:";
	public function load($param)
	{
		fanwe_require(APP_ROOT_PATH.'mapi/xr/core/common.php');
		$this->key .= md5(serialize($param));
		$type = $param['type']; //type: photo or video
		$page=$param['page']>0?$param['page']:1;
		$user_id=$param['user_id'];
		$to_user_id=$param['to_user_id'];
		$page_size=$param['page_size']>0?$param['page_size']:20;
		$page_size=5;
		$limit = (($page-1) * $page_size) . "," . $page_size;

		fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/WeiboRedisService.php');
		$redis    = new WeiboRedisService();

		$m_config =  load_auto_cache("m_config");//初始化手机端配置

		$is_openweibotest = intval($m_config['is_openweibotest']);

		$new_weibo_list=$redis->get($user_id);
		//print_r($new_weibo_list);
		if ($new_weibo_list&&$to_user_id == 0&&$is_openweibotest){
			//存在推荐队列

			$has_is_authentication = intval($m_config['has_is_authentication'])?1:0;

			$new_user_number=intval($m_config['new_user_number'])?intval($m_config['new_user_number']):0;
			$adv_number=intval($m_config['adv_number'])?intval($m_config['adv_number']):0;
			$hot_number=intval($m_config['hot_number'])?intval($m_config['hot_number']):0;
			$where = '';
			$weibo_ids = array ();
			ksort($new_weibo_list);

			$where_ids=" ";
			//当前用户拉黑的id集合
			$user_black = $GLOBALS['db']->getAll("SELECT black_user_id FROM ".DB_PREFIX."black WHERE user_id = {$user_id}");
			//拉黑当前用户的id集合
			$user_self_black = $GLOBALS['db']->getAll("SELECT user_id as black_user_id FROM ".DB_PREFIX."black WHERE black_user_id = {$user_id}");
			$black_array_merge = array_merge($user_black,$user_self_black);
			if ($black_array_merge) {
				$user_black_ids = array ();
				foreach ($black_array_merge as $k => $v) {
					$user_black_ids[] = $v['black_user_id'];
				}
				$where_ids.= " and user_id not in (".implode(',',$user_black_ids).")";
			}

			//print_r($new_weibo_list);exit();
			if($new_user_number>0){
				$new_user_time=intval($m_config['new_user_time'])?intval($m_config['new_user_time']):1;
				$time=NOW_TIME-(86400*$new_user_time);
				$new_user_ids=$GLOBALS['db']->getAll("SELECT id FROM ".DB_PREFIX."weibo WHERE status = 1 and user_id in(SELECT id FROM t_user WHERE create_time >= ".$time.") ".$where_ids."  ORDER BY RAND() LIMIT ".$new_user_number);
				foreach ($new_user_ids as $k => $v) {
					$weibo_ids[] = $v['id'];

				}
			}
			if($adv_number){
				$adv_ids=$GLOBALS['db']->getAll("SELECT id FROM ".DB_PREFIX."weibo WHERE is_adv = 1 and status = 1 ORDER BY RAND() LIMIT ".$adv_number);
				foreach ($adv_ids as $k => $v) {
					$weibo_ids[] = $v['id'];

				}
			}
			if($hot_number){
				$hot_ids=$GLOBALS['db']->getAll("SELECT id FROM ".DB_PREFIX."weibo WHERE is_hot = 1 and status = 1 ".$where_ids." ORDER BY RAND() LIMIT ".$hot_number);
				foreach ($hot_ids as $k => $v) {
					$weibo_ids[] = $v['id'];

				}
			}

			$page_size=$page_size-count($weibo_ids);
			$page_size=$page_size>0?$page_size:1;
			foreach ($new_weibo_list as $k => $v) {
				if ($k>=$page_size) break;
				$weibo_ids[] = $v;

			}

			if (count($weibo_ids)>0){
				$where= " and w.id in (".implode(',',$weibo_ids).")";
			}
			$where .= $where_ids;




			if($has_is_authentication){
				$sql = "select w.user_id,w.id as weibo_id,u.nick_name,w.sort_num,w.photo_image as video_image ,u.city,u.user_level,w.data as video_url,w.xpoint,w.ypoint,w.content,u.head_image,w.video_count,u.show_image,w.first_image,w.comment_count,w.repost_count,w.digg_count,w.video_width,w.video_height,u.is_authentication,u.v_icon,u.sex,w.audio_id,w.goods_id,w.is_podcast_goods from ".DB_PREFIX."weibo as w
					left join ".DB_PREFIX."user as u on w.user_id = u.id where status = 1 and u.is_authentication = 2 and w.type = '".$type."' $where  ";
			}else{
				$sql = "select w.user_id,w.id as weibo_id,u.nick_name,w.sort_num,w.photo_image as video_image,u.city,u.user_level,w.data as video_url,w.xpoint,w.ypoint,w.content,u.head_image,w.video_count,u.show_image,w.first_image,w.comment_count,w.repost_count,w.digg_count,w.video_width,w.video_height,u.is_authentication,u.v_icon,u.sex,w.audio_id,w.goods_id,w.is_podcast_goods from ".DB_PREFIX."weibo as w
					left join ".DB_PREFIX."user as u on w.user_id = u.id where status = 1 and w.type = '".$type."' $where ";
			}

			//$sql .= "  order by w.create_time desc";
			$sql .= " limit " .$limit;
			$list = $GLOBALS['db']->getAll($sql,true,true);
			$user_info = $GLOBALS['db']->getAll("SELECT head_image FROM ".DB_PREFIX."user WHERE id in(100850,100849)");
			foreach ($user_info as $k => $v) {
				$user_info[$k]['head_image'] = get_spec_image($v['head_image']);
			}
			foreach($list as $k=>$v){
				//点击量过万 转换成带单位的字符串
				if ($list['video_count'] >= 10000) {
					$list['video_count'] = round($list['video_count']/10000,2)."万";
				}

				//3.1 小视频-查询出小视频的音乐信息
				if (intval($v['audio_id']) > 0) {
					$audio_info = $GLOBALS['db']->getRow("SELECT id,audio_name,artist_name,audio_image FROM ".DB_PREFIX."music_list WHERE id = {$v['audio_id']}");
//						$audio_info = $GLOBALS['db']->getRow("SELECT audio_name,artist_name,audio_image,audio_link FROM ".DB_PREFIX."music_list WHERE id = 33");
					if ($audio_info) {
						$list[$k]['audio']['audio_id'] = $audio_info['id'];
						$list[$k]['audio']['audio_name'] = $audio_info['audio_name'].'-'.$audio_info['artist_name']; //音乐名称-演唱者
						$list[$k]['audio']['audio_image'] = $audio_info['audio_image']; //音乐封面
						$list[$k]['audio']['audio_link'] = $audio_info['audio_link']; //音乐下载地址
					}
				}
				unset($list[$k]['audio_id']);

				//3.1 小视频-视频同款抢购
				if ($v['goods_id'] > 0) {
					//主播商品
					if ($v['is_podcast_goods'] > 0) {
						$video_commodity = $GLOBALS['db']->getRow("SELECT id,name,description,imgs,price,url,seen_num FROM ".DB_PREFIX."podcast_goods WHERE id = {$v['goods_id']}");
					} else {
						$video_commodity = $GLOBALS['db']->getRow("SELECT id as goods_id,name,description,imgs,price,url,seen_num FROM ".DB_PREFIX."goods WHERE id = {$v['goods_id']}");
					}
					$list[$k]['video_commodity'] = [];
					$video_commodity['seen_num'] = $video_commodity['seen_num'] > 10000 ? round($video_commodity['seen_num'] / 10000,2) .'W' : $video_commodity['seen_num'];
					$list[$k]['video_commodity']['commodity_id'] = $video_commodity['goods_id'];
					$list[$k]['video_commodity']['commodity_name'] = $video_commodity['name'];
					$list[$k]['video_commodity']['commodity_desc'] = $video_commodity['description'];
					$list[$k]['video_commodity']['commodity_image'] = json_decode($video_commodity['imgs'],1)[0];
					$list[$k]['video_commodity']['commodity_user'] = $user_info;
					$list[$k]['video_commodity']['commodity_seen'] = $video_commodity['seen_num'] > 0 ? $video_commodity['seen_num'] .'人看过': '';
					$list[$k]['video_commodity']['commodity_money'] = $video_commodity['price'];
					if ($video_commodity['url']) {
						$list[$k]['video_commodity']['commodity_url'] = $video_commodity['url'];
					} else {
						$list[$k]['video_commodity']['commodity_url'] = SITE_DOMAIN.APP_ROOT.'/wap/index.php?ctl=shop&act=shop_goods_details&podcast_id='.$list[$k]['user_id']."&goods_id=".$video_commodity['goods_id'];
					}
				}

				//是否正在直播中
				if(!$GLOBALS['db']->getOne("select id from ".DB_PREFIX."video where live_in = 1 and user_id=".$user_id,true,true)) {
					$video = $GLOBALS['db']->getRow("select id as room_id,group_id,live_in,user_id,head_image,live_image from " . DB_PREFIX . "video where user_id=" . $list[$k]['user_id'] . " and live_in = 1 and room_type = 3", true, true);
					if (intval($video['room_id'])) {
						$video['head_image'] = get_spec_image($video['head_image']);
						$video['live_image'] = get_spec_image($video['live_image']);
						if (defined('OPEN_EDU_MODULE') && OPEN_EDU_MODULE == 1) {
							$video['is_verify'] = intval($GLOBALS['db']->getOne("select is_verify from " . DB_PREFIX . "edu_video_info where video_id = " . $video['room_id']));
						}
						$list[$k]['live_video'] = $video;
					}
				}


				$list[$k]['is_show_weibo_report'] = 1; //是否显示举报动态
				$list[$k]['is_show_user_report'] = 1; //是否显示举报用户
				$list[$k]['is_show_deal_weibo'] = 1; //	是否显示删除动态
				//判断用户是否为今日创建的新用户，是：1，否：0
				if (date('Y-m-d') == date('Y-m-d',$list[$k]['user_create_time']+3600*8)){
					$list[$k]['today_create'] = 1;
				}else{
					$list[$k]['today_create'] = 0;
				}
				if($v['video_url']){
					$list[$k]['video_url']  = get_file_oss_url($v['video_url']);
				}
				$list[$k]['head_image'] = get_spec_image($v['head_image'],200,200,1);
				$list[$k]['video_image'] = get_spec_image($v['video_image'],200,200,1);
				if($type=='photo'){
					$list[$k]['head_image'] = get_spec_image($v['video_image'],200,200,1);
					$list[$k]['nick_name'] = $v['vide_desc'];
				}
				if($v['video_url']){
					$show_image_num = count(unserialize($v['video_url']));
				}else{
					$show_image_num = 0;
				}
				//写真照片数量
				$list[$k]['show_image_num'] = $show_image_num;

				//判断是否点赞过
				$digg_list = $GLOBALS['db']->getAll("select wc.user_id from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$v['weibo_id']." and type = 2 ");
				$list[$k]['has_digg'] = 0;
				if ($digg_list) {
					if(in_array($user_id,$digg_list)){
						$list[$k]['has_digg'] =1;
					}
				}

				//判断是否关注过
				fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserFollwRedisService.php');
				fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserRedisService.php');
				$userfollw_redis = new UserFollwRedisService($user_id);
				$list[$k]['has_focus'] = 0;
				if ($userfollw_redis->is_following($v['user_id'])){
					$list[$k]['has_focus'] = 1;//0:未关注;1:已关注
				}

				//评论列表
				$comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,u.v_icon from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$v['weibo_id']." and type = 1 and is_del = 0 order by wc.comment_id desc limit 0,1");
				if($comment_list){
					$to_comment_user = array();
					foreach($comment_list as $kk=>$vv){
						if($vv){
							$comment_list[$kk]['head_image'] = get_spec_image($vv['head_image']);
							$comment_list[$kk]['left_time'] = time_tran($vv['create_time']);
							if($vv['to_comment_id']){
								$comment_list[$kk]['is_to_comment'] =1;
								$to_comment_user[] = $vv['to_user_id'];
							}else{
								$comment_list[$kk]['is_to_comment'] = 0;
							}
							$comment_list[$kk]['to_nick_name'] = '';
						}
					}
					if(count($to_comment_user)>0){

						$user_list = $GLOBALS['db']->getAll("select id,nick_name from ".DB_PREFIX."user where id in (".implode(',',$to_comment_user).")");
						$user_array = array();
						foreach($user_list as $kk=>$vv){
							$user_array[$vv['id']] = $vv['nick_name'];
						}
						foreach($comment_list as $kk=>$vv){
							if($vv['to_user_id']){
								$comment_list[$kk]['to_nick_name'] = $user_array[$vv['to_user_id']];
							}
						}
					}
				}
				$key = $list[$k];
				unset($list[$k]);
				$list[$k]['info'] = array ();
				$list[$k]['info'] = $key;
				$list[$k]['comment_list'] = $comment_list;

			}


		}else{
			$sql = "select count(id) as num from ".DB_PREFIX."black where black_user_id = ".$to_user_id." and user_id = ".$user_id;
			//当前用户拉黑对方
			$user_black = $GLOBALS['db']->getOne($sql,true,true);
			//对方拉黑当前用户
			$sql = "select count(id) as num from ".DB_PREFIX."black where black_user_id = ".$user_id." and user_id = ".$to_user_id;
			$to_user_black = $GLOBALS['db']->getOne($sql,true,true);
			if ($user_black || $to_user_black) {
				return $list = array();
			}
			//取常用队列
			$key_bf = $this->key.'_bf';

			$list = $GLOBALS['cache']->get($this->key,true);

			if ($list === false) {
				$is_ok =  $GLOBALS['cache']->set_lock($this->key);
				if(!$is_ok){
					$list = $GLOBALS['cache']->get($key_bf,true);
				}else{
					$where = '';
					$m_config =  load_auto_cache("m_config");//初始化手机端配置
					$has_is_authentication = intval($m_config['has_is_authentication'])?1:0;
					//当前用户拉黑的id集合
					$user_black = $GLOBALS['db']->getAll("SELECT black_user_id FROM ".DB_PREFIX."black WHERE user_id = {$user_id}");
					//拉黑当前用户的id集合
					$user_self_black = $GLOBALS['db']->getAll("SELECT user_id as black_user_id FROM ".DB_PREFIX."black WHERE black_user_id = {$user_id}");
					$black_array_merge = array_merge($user_black,$user_self_black);
					if ($black_array_merge) {
						$user_black_ids = array ();
						foreach ($black_array_merge as $k => $v) {
							$user_black_ids[] = $v['black_user_id'];
						}
						$where.= " and w.user_id not in (".implode(',',$user_black_ids).")";
					}

					if($has_is_authentication){
						$sql = "select w.user_id,w.id as weibo_id,u.nick_name,w.sort_num,w.photo_image as video_image ,u.city,u.user_level,w.data as video_url,w.xpoint,w.ypoint,w.content,u.head_image,w.video_count,u.show_image,w.first_image,w.comment_count,w.repost_count,w.digg_count,w.video_width,w.video_height,u.is_authentication,u.v_icon,u.sex,w.audio_id,w.goods_id,w.is_podcast_goods from ".DB_PREFIX."weibo as w
					left join ".DB_PREFIX."user as u on w.user_id = u.id where status = 1 and u.is_authentication = 2 and w.type = '".$type."' $where  ";
					}else{
						$sql = "select w.user_id,w.id as weibo_id,u.nick_name,w.sort_num,w.photo_image as video_image,u.city,u.user_level,w.data as video_url,w.xpoint,w.ypoint,w.content,u.head_image,w.video_count,u.show_image,w.first_image,w.comment_count,w.repost_count,w.digg_count,w.video_width,w.video_height,u.is_authentication,u.v_icon,u.sex,w.audio_id,w.goods_id,w.is_podcast_goods from ".DB_PREFIX."weibo as w
					left join ".DB_PREFIX."user as u on w.user_id = u.id where status = 1 and w.type = '".$type."' $where ";
					}
					if ($to_user_id > 0) {
						$sql = "select w.user_id,w.id as weibo_id,u.nick_name,w.sort_num,w.photo_image as video_image,u.city,u.user_level,w.data as video_url,w.xpoint,w.ypoint,w.content,u.head_image,w.video_count,u.show_image,w.first_image,w.comment_count,w.repost_count,w.digg_count,w.video_width,w.video_height,u.is_authentication,u.v_icon,u.sex,w.audio_id,w.goods_id,w.is_podcast_goods from ".DB_PREFIX."weibo as w
					left join ".DB_PREFIX."user as u on w.user_id = u.id where status = 1 and w.user_id = {$to_user_id} and w.type = '".$type."' ";
					}
					if ($to_user_id > 0 && $to_user_id == $user_id) {
						$sql = "select w.user_id,w.id as weibo_id,u.nick_name,w.sort_num,w.photo_image as video_image,u.city,u.user_level,w.data as video_url,w.xpoint,w.ypoint,w.content,u.head_image,w.video_count,u.show_image,w.first_image,w.comment_count,w.repost_count,w.digg_count,w.video_width,w.video_height,u.is_authentication,u.v_icon,u.sex,w.audio_id,w.goods_id,w.is_podcast_goods from ".DB_PREFIX."weibo as w
					left join ".DB_PREFIX."user as u on w.user_id = u.id where w.user_id = {$to_user_id} and w.type = '".$type."' ";
					}
					$sql .= "  order by w.create_time desc";
					if ($to_user_id  == '') {
						$sql .= " limit " .$limit;
					}
//					log_result2('$sql',$sql);
					$list = $GLOBALS['db']->getAll($sql,true,true);
					$user_info = $GLOBALS['db']->getAll("SELECT head_image FROM " . DB_PREFIX . "user WHERE is_robot = 1 ORDER BY RAND() LIMIT 2");
					foreach ($user_info as $k => $v) {
						$user_info[$k]['head_image'] = get_spec_image($v['head_image']);
					}
					foreach($list as $k=>$v){
						//点击量过万 转换成带单位的字符串
						if ($list[$k]['video_count'] >= 10000) {
							$list[$k]['video_count'] = round($list[$k]['video_count']/10000,2)."万";
						}

						//3.1 小视频-查询出小视频的音乐信息
						if (intval($v['audio_id']) > 0) {
							$audio_info = $GLOBALS['db']->getRow("SELECT id,audio_name,artist_name,audio_image FROM ".DB_PREFIX."music_list WHERE id = {$v['audio_id']}");
//						$audio_info = $GLOBALS['db']->getRow("SELECT audio_name,artist_name,audio_image,audio_link FROM ".DB_PREFIX."music_list WHERE id = 33");
							if ($audio_info) {
								$list[$k]['audio']['audio_id'] = $audio_info['id'];
								$list[$k]['audio']['audio_name'] = $audio_info['audio_name'].'-'.$audio_info['artist_name']; //音乐名称-演唱者
								$list[$k]['audio']['audio_image'] = $audio_info['audio_image']; //音乐封面
								$list[$k]['audio']['audio_link'] = $audio_info['audio_link']; //音乐下载地址
							}
						}
						unset($list[$k]['audio_id']);

						//3.1 小视频-视频同款抢购
						if ($v['goods_id'] > 0) {
							//主播商品
							if ($v['is_podcast_goods'] > 0) {
								$video_commodity = $GLOBALS['db']->getRow("SELECT id,name,description,imgs,price,url FROM ".DB_PREFIX."podcast_goods WHERE id = {$v['goods_id']}");
							} else {
								$video_commodity = $GLOBALS['db']->getRow("SELECT id as goods_id,name,description,imgs,price,url,seen_num FROM ".DB_PREFIX."goods WHERE id = {$v['goods_id']}");
							}
							$list[$k]['video_commodity'] = [];
							$video_commodity['seen_num'] = $video_commodity['seen_num'] > 10000 ? round($video_commodity['seen_num'] / 10000,2) .'W' : $video_commodity['seen_num'];
							$list[$k]['video_commodity']['commodity_id'] = $video_commodity['goods_id'];
							$list[$k]['video_commodity']['commodity_name'] = $video_commodity['name'];
							$list[$k]['video_commodity']['commodity_desc'] = $video_commodity['description'];
							$list[$k]['video_commodity']['commodity_image'] = json_decode($video_commodity['imgs'],1)[0];
							$list[$k]['video_commodity']['commodity_user'] = $user_info;
							$list[$k]['video_commodity']['commodity_seen'] = $video_commodity['seen_num'] > 0 ? $video_commodity['seen_num'] .'人看过': '';
							$list[$k]['video_commodity']['commodity_money'] = $video_commodity['price'];
							if ($video_commodity['url']) {
								$list[$k]['video_commodity']['commodity_url'] = $video_commodity['url'];
							} else {
								$list[$k]['video_commodity']['commodity_url'] = SITE_DOMAIN.APP_ROOT.'/wap/index.php?ctl=shop&act=shop_goods_details&podcast_id='.$list[$k]['user_id']."&goods_id=".$video_commodity['goods_id'];
							}
						}
						//是否正在直播中
						if(!$GLOBALS['db']->getOne("select id from ".DB_PREFIX."video where live_in = 1 and user_id=".$user_id,true,true)) {
							$video = $GLOBALS['db']->getRow("select id as room_id,group_id,live_in,user_id,head_image,live_image from " . DB_PREFIX . "video where user_id=" . $list[$k]['user_id'] . " and live_in = 1 and room_type = 3", true, true);
							if (intval($video['room_id'])) {
								$video['head_image'] = get_spec_image($video['head_image']);
								$video['live_image'] = get_spec_image($video['live_image']);
								if (defined('OPEN_EDU_MODULE') && OPEN_EDU_MODULE == 1) {
									$video['is_verify'] = intval($GLOBALS['db']->getOne("select is_verify from " . DB_PREFIX . "edu_video_info where video_id = " . $video['room_id']));
								}
								$list[$k]['live_video'] = $video;
							}
						}


						$list[$k]['is_show_weibo_report'] = 1; //是否显示举报动态
						$list[$k]['is_show_user_report'] = 1; //是否显示举报用户
						$list[$k]['is_show_deal_weibo'] = 1; //	是否显示删除动态
						//判断用户是否为今日创建的新用户，是：1，否：0
						if (date('Y-m-d') == date('Y-m-d',$list[$k]['user_create_time']+3600*8)){
							$list[$k]['today_create'] = 1;
						}else{
							$list[$k]['today_create'] = 0;
						}
						if($v['video_url']){
							$list[$k]['video_url']  = get_file_oss_url($v['video_url']);
						}
						$list[$k]['head_image'] = get_spec_image($v['head_image'],200,200,1);
						$list[$k]['video_image'] = get_spec_image($v['video_image'],200,200,1);
						if($type=='photo'){
							$list[$k]['head_image'] = get_spec_image($v['video_image'],200,200,1);
							$list[$k]['nick_name'] = $v['vide_desc'];
						}
						if($v['video_url']){
							$show_image_num = count(unserialize($v['video_url']));
						}else{
							$show_image_num = 0;
						}
						//写真照片数量
						$list[$k]['show_image_num'] = $show_image_num;

						//判断是否点赞过
						$digg_list = $GLOBALS['db']->getAll("select wc.user_id from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$v['weibo_id']." and type = 2 ");
						$list[$k]['has_digg'] = 0;
						if ($digg_list) {
							if(in_array($user_id,$digg_list)){
								$list[$k]['has_digg'] =1;
							}
						}

						//判断是否关注过
						fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserFollwRedisService.php');
						fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserRedisService.php');
						$userfollw_redis = new UserFollwRedisService($user_id);
						$list[$k]['has_focus'] = 0;
						if ($userfollw_redis->is_following($v['user_id'])){
							$list[$k]['has_focus'] = 1;//0:未关注;1:已关注
						}

						//评论列表
						$comment_list = $GLOBALS['db']->getAll("select wc.comment_id, wc.user_id,u.v_icon,u.nick_name,u.head_image,wc.content,wc.to_comment_id,wc.to_user_id,wc.create_time,u.is_authentication,u.sex,u.v_icon from ".DB_PREFIX."weibo_comment as wc
            left join ".DB_PREFIX."user as u on wc.user_id = u.id where wc.weibo_id = ".$v['weibo_id']." and type = 1 and is_del = 0 order by wc.comment_id desc limit 0,1");
						if($comment_list){
							$to_comment_user = array();
							foreach($comment_list as $kk=>$vv){
								if($vv){
									$comment_list[$kk]['head_image'] = get_spec_image($vv['head_image']);
									$comment_list[$kk]['left_time'] = time_tran($vv['create_time']);
									if($vv['to_comment_id']){
										$comment_list[$kk]['is_to_comment'] =1;
										$to_comment_user[] = $vv['to_user_id'];
									}else{
										$comment_list[$kk]['is_to_comment'] = 0;
									}
									$comment_list[$kk]['to_nick_name'] = '';
								}
							}
							if(count($to_comment_user)>0){

								$user_list = $GLOBALS['db']->getAll("select id,nick_name from ".DB_PREFIX."user where id in (".implode(',',$to_comment_user).")");
								$user_array = array();
								foreach($user_list as $kk=>$vv){
									$user_array[$vv['id']] = $vv['nick_name'];
								}
								foreach($comment_list as $kk=>$vv){
									if($vv['to_user_id']){
										$comment_list[$kk]['to_nick_name'] = $user_array[$vv['to_user_id']];
									}
								}
							}
						}
						$key = $list[$k];
						unset($list[$k]);
						$list[$k]['info'] = array ();
						$list[$k]['info'] = $key;
						$list[$k]['comment_list'] = $comment_list;

					}
					$GLOBALS['cache']->set($this->key, $list, 5, true);
					$GLOBALS['cache']->set($key_bf, $list, 86400, true);//备份
				}
			}
		}

		if ($list == false) $list = array();

		return $list;
	}

	public function rm()
	{

		//$GLOBALS['cache']->clear_by_name($this->key);
	}

	public function clear_all()
	{

		//$GLOBALS['cache']->clear_by_name($this->key);
	}
}
?>
