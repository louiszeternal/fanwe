<?php

class key_list_auto_cache extends auto_cache{
	private $key = "key:list";
	public function load($param)
	{
		$aes_key_list = $GLOBALS['cache']->get($this->key);
		if($aes_key_list === false)
		{
			if(intval($param['is_aes_extra'])){
				$sql = "SELECT * from ".DB_PREFIX."key_list where  is_effect=1 and is_delete=0  order by id desc ";
			}else{
				$sql = "SELECT * from ".DB_PREFIX."key_list where  is_effect=1 and is_delete=0  order by id desc limit 1 ";
			}
			$aes_key_list = $GLOBALS['db']->getAll($sql);
			$GLOBALS['cache']->set($this->key,$aes_key_list,60,true);
		}
		return $aes_key_list;
	}
	
	public function rm($param)
	{
		$GLOBALS['cache']->rm($this->key);
	}
	
	public function clear_all()
	{
		$GLOBALS['cache']->rm($this->key);
	}
}
?>