<?php

/*
 * AES加密
 */
class opensslAes
{
    /**
     *var string $method 加解密方法，可通过openssl_get_cipher_methods()获得
    　　  */

    private $method;

    /**
     * var string $secret_key 加解密的密钥
     */
    private $secret_key;

    /**
     * var string $iv 密初始化向量,加解密的向量，有些方法需要设置
     */
    private $iv;

    /**
     * var string $options false-原始数据；true-base64后的数据
     */
    private $options;

    /**
     * 构造函数
     *
     * @param string $key 密钥
     * @param string $method 加密方式
     * @param string $iv iv向量
     * @param mixed $options false-原始数据；true-base64后的数据
     *
     */
    public function __construct()
    {
        //$this->secret_key   = 'xxxxxxxxxx'; //自己的秘钥
        $this->method       = 'AES-128-ECB';
        $this->iv           = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->method));
        $this->options      = OPENSSL_RAW_DATA;
    }
    public function set_key($key)
    {

        $this->secret_key = $key;

    }
    public function set_iv($iv)
    {

        $this->iv = $iv;

    }
    /**
     * 加密方法，对数据进行加密，返回加密后的数据
     *
     * @param string $data 要加密的数据
     *
     * @return string
     *
     */
    public function encrypt($data)
    {
        $encrypted = openssl_encrypt($data, $this->method, $this->secret_key, $this->options);
        return base64_encode($encrypted);
    }

    /**
     * 解密方法，对数据进行解密，返回解密后的数据
     *
     * @param string $data 要解密的数据
     *
     * @return string
     *
     */
    public function decrypt($data)
    {
        $encrypted_data = base64_decode($data);
        return openssl_decrypt($encrypted_data, $this->method, $this->secret_key, $this->options);
    }

}