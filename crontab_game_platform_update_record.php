<?php

chdir(__DIR__);
define("FANWE_REQUIRE",true);
require './system/mapi_init.php';

fanwe_require(APP_ROOT_PATH.'mapi/lib/core/common.php');

const LIB_PATH = APP_ROOT_PATH . 'mapi/lib/';
const MODULES_PATH = LIB_PATH . 'modules/game_platform/';
require_once MODULES_PATH . 'utils/utils.php';

$platformList = getModel('game_platform')->getList();

$startTime = strtotime('-2 day');
$endTime = time();

foreach($platformList as $platform){
    $module = getGameModule($platform['name']);
    $module->localizedGameRecord($startTime,$endTime);
}

exit(0);
