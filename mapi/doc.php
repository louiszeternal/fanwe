<?php

if(!empty($_GET['file'])){
    if(!preg_match('/^.+\.md$/',$_GET['file'])){
        echo '非法请求：仅能查看 markdown 文件';
        die;
    }
    if(!empty($_GET['method']) && $_GET['method'] == 'download'){
        header('Content-Type: text/markdown');
        echo file_get_contents("../doc/new/{$_GET['file']}");
        die;
    }
    require_once '../markdown_viewer/Parsedown.php';
    require_once '../markdown_viewer/ParsedownToC.php';

    $Parsedown = new ParsedownToC();

    echo '<title>';
    echo $_GET['file'];
    echo '</title>';

    echo '<style>';
    echo PHP_EOL;
    echo file_get_contents('../markdown_viewer/css/markdown_style.css');
    echo PHP_EOL;
    echo file_get_contents('../markdown_viewer/css/prism.css');
    echo PHP_EOL;
    echo '</style>';

    echo '<body>';
    echo $Parsedown->text(file_get_contents("../doc/new/{$_GET['file']}"));
    echo PHP_EOL;
    echo '<script>';
    echo file_get_contents('../markdown_viewer/js/prism.js');
    echo '</script>';
    echo '</body>';
} else{
    echo "<h2>线上文档列表：</h2>";
    $files = array_diff(scandir('../doc/new'), ['..', '.']);
    foreach($files as $file){
        $encoded = urlencode($file);
        $modifiedTime = filemtime('../doc/new/'.$file);
        echo "<a href='./doc.php?file=$encoded&timestamp=$modifiedTime'>$file</a>";
        echo " [ <a href='./doc.php?file=$encoded&timestamp=$modifiedTime' download='$file'>Download</a> ]";
        echo " [ <a href='./doc.php?file=$encoded&method=download&timestamp=$modifiedTime' download='$file'>Raw</a> ]";
        echo ' 文件更新时间：' . date('Y-m-d H:i:s',$modifiedTime);
        echo '<br>';
    }
}
