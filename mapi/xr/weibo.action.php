<?php
//动态详情页
class weiboCModule extends baseCModule
{
    //动态详情
    public function index()
    {
        fanwe_require(APP_ROOT_PATH.'mapi/xr/core/common.php');

        $weibo_id = intval($_REQUEST['weibo_id']);
        $is_paid = intval($_REQUEST['is_paid']);
		$page = intval($_REQUEST['p']) ? intval($_REQUEST['p']) : intval($_REQUEST['page']);//当前页
		$page = intval($page) > 0 ? intval($page) : 1;
		$page_size = 20;
		//子评论分页参数
		$child_page = intval($_REQUEST['child_page']);
		$child_page = intval($child_page) > 0 ? intval($child_page) : 1;
		$comment_id = intval($_REQUEST['comment_id']);
        if($GLOBALS['user_info']){
            $user_id = $GLOBALS['user_info']['id'];
            if($weibo_id){
                $weibo_see = $GLOBALS['db']->getRow("select *  from " . DB_PREFIX . "weibo_see where  weibo_id = ".$weibo_id." and  user_id=".$user_id);
                $weibo_user_id = $GLOBALS['db']->getOne("select user_id  from " . DB_PREFIX . "weibo where  id = ".$weibo_id." and  status = 1 and is_audit = 1");
				//观看记录
				$weibo_watch_log =array();
				$weibo_watch_log['weibo_id'] = $weibo_id;
				$weibo_watch_log['weibo_user_id'] = $weibo_user_id;
				$weibo_watch_log['user_id'] = $user_id;
				$weibo_watch_log['create_time'] = "".date('Y-m-d H:i:s',time())."";
				$GLOBALS['db']->autoExecute(DB_PREFIX."weibo_watch_log", $weibo_watch_log,'INSERT');
                if($weibo_see)
                {
                    //多次观看该视频
                    $number = $weibo_see['number'] + 1 ;
                    if($number==3){
                        //第3次播放同一视频,生成权重
                        $memo = "观看小视频id为".$weibo_id."三次";
                        create_weight ($user_id,$weibo_id,$memo,8);
                    }
                    $re = $GLOBALS['db']->query("update ".DB_PREFIX."weibo_see set number = ".$number.",create_time = ".NOW_TIME." where   weibo_id = ".$weibo_id." and  user_id=".$user_id);


                }else{
                    // 添加正常观看记录
                    $weibo_see =array();
                    $weibo_see['weibo_id'] = $weibo_id;
                    $weibo_see['weibo_user_id'] = $weibo_user_id;
                    $weibo_see['user_id'] = $user_id;
                    $weibo_see['number'] = 1;
                    $weibo_see['create_time'] = NOW_TIME;
                    $GLOBALS['db']->autoExecute(DB_PREFIX."weibo_see", $weibo_see,'INSERT');

                    //正常观看小视频,生成权重
                    $memo = "正常观看小视频id为".$weibo_id;
                    create_weight ($user_id,$weibo_id,$memo,10);

                    //观看同一作者3个视频,生成权重
                    $num = $GLOBALS['db']->getOne("select count(*)  from " . DB_PREFIX . "weibo_see where  weibo_user_id = ".$weibo_user_id." and  user_id=".$user_id);
                    if($num==3){
                        $memo = "观看小视频发布者user_id为".$weibo_user_id."三个视频";
                        create_weight ($user_id,$weibo_id,$memo,7);
                    }
                }

                //维护看过队列
                fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/WeiboRedisService.php');
                $redis    = new WeiboRedisService();
                $weibo_list=$redis->get($user_id);
                $tmp = $weibo_id;
                $new_weibo_list=array_merge(array_diff($weibo_list, array($tmp)));
                $redis->del($user_id);
                $redis->set($user_id, $new_weibo_list);

                $sql = "update  ".DB_PREFIX."user set weibo_num = ".count($new_weibo_list)." where id = ".$user_id;
                $res = $GLOBALS['db']->query($sql);


            }
        }else{
            $user_id = 0;
        }
		$GLOBALS['db']->query("update ".DB_PREFIX."weibo set video_count=video_count+1 where id = $weibo_id");
        if (defined('OPEN_SVIDEO_MODULE') && OPEN_SVIDEO_MODULE == 1) {
            $root = load_auto_cache("svideo_select_weibo_info",
                array('page' => $page, 'weibo_id' => $weibo_id, 'is_paid' => $is_paid, 'child_page' => $child_page, 'user_id' => $user_id, 'comment_id' => $comment_id));

            $sql = "select count(*) from " . DB_PREFIX . "weibo_comment where weibo_id = " . $weibo_id . " and type = 3 and user_id = " . $user_id;
            $root['info']['has_unlike'] = $GLOBALS['db']->getOne($sql) ? 1 : 0;  //是否踩过

            $sql = "select count(*) from " . DB_PREFIX . "qk_svideo_favor f where user_id = {$user_id} and weibo_id = {$weibo_id}";
            $root['info']['has_favor'] = $GLOBALS['db']->getOne($sql) ? 1 : 0;  //是否踩过
        } else {
            $root = load_auto_cache("select_weibo_info",
                array('page' => $page, 'weibo_id' => $weibo_id, 'is_paid' => $is_paid));
        }
		if($root['info']['first_image']){
			$root['info']['first_image'] = get_spec_image($root['info']['first_image']);
		}

        for($i=0;$i<count($root['comment_list']);$i++)
        {
            $id = $root['comment_list'][$i]['user_id'];
//            $root['comment_list'][$i]['v_icon'] = $GLOBALS['db']->getOne("select v_icon from " . DB_PREFIX . "user where id = " .$id);
            $root['comment_list'][$i]['nick_name'] = emoji_decode($root['comment_list'][$i]['nick_name']);
            $root['comment_list'][$i]['to_nick_name'] = emoji_decode($root['comment_list'][$i]['to_nick_name']);
            $root['comment_list'][$i]['content'] = emoji_decode($root['comment_list'][$i]['content']);
        }

        $root['info']['nick_name'] = emoji_decode($root['info']['nick_name']);
        //获取认证图标
        $sql = "select v_icon from " . DB_PREFIX . "user where id = " . intval($root['info']['user_id']);
        $root['info']['v_icon'] = $GLOBALS['db']->getOne($sql);

        $info =  $root['info'];
        $root['page'] = $page;
        $root['is_admin'] = 0; //是否可以管理评论
        $root['is_reply_but'] = 1;//是否可以评论动态

		//是否允许评论
		$weibo_user = $GLOBALS['db']->getRow("select comment_restrict,user_id from ".DB_PREFIX."weibo where id = ".$weibo_id);
		if ($weibo_user == 3) {
			$root['is_reply_but'] = 0;//是否可以评论动态
		}
        $root['is_reply_comment_but'] = 0; //是否可以回复评论

        $root['is_show_weibo_report'] = 1; //是否显示举报动态
        $root['is_show_user_report'] = 1; //是否显示举报动态
        $root['is_show_user_black'] = 0; //	是否显示拉黑用户

        $root['is_show_top'] = 0; //	是否显示置顶
        $root['is_show_deal_weibo'] = 1; //	是否显示删除动态

        //$root['is_show_deal_comment'] = 0; //	是否显示删除评论

        $root['info']['has_digg'] =0;  //是否点赞过
        if($info['price']){
            $is_pay =0;
        }

        if($user_id){
            //判断是否为本人
            $to_user_id = $info['user_id'];



            if($user_id==$to_user_id){
                $root['is_admin'] = 1;
                $root['is_show_deal_weibo'] = 1;
                $root['is_show_top'] = 1;
                $is_pay = 1;
            }else{
                $root['is_show_weibo_report'] = 1;
                $root['is_show_user_report'] = 1;
                $root['is_show_user_black'] = 1;
                $is_pay = 1;
                if($info['price']){
                    if(count($info['images'])>0){
                        $payed = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."payment_notice where user_id = ".$user_id." and type in (11,13,14) and is_paid = 1 and order_id = ".$info['weibo_id']);
                        if($payed>0){
                            $is_pay =1;
                        }else{
                            $is_pay =0;
                        }
                    }
                }
            }

            $root['is_reply_but'] = 1;
            $root['is_reply_comment_but'] = 1;
            //判断是否点赞过
            if(in_array($user_id,$root['digg_user_list'])){
                $root['info']['has_digg'] =1;
            }
			//判断是否关注过
			fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserFollwRedisService.php');
			fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserRedisService.php');
			$userfollw_redis = new UserFollwRedisService($user_id);
			$root['info']['has_focus'] = 0;
			if ($userfollw_redis->is_following($root['info']['user_id'])){
				$root['info']['has_focus'] = 1;//0:未关注;1:已关注
			}

        }
        //获取图片

        if(count($info['images'])>0){
            $images = $info['images'];
            foreach($images as $k1=>$v1){
                if(is_object($v1)){
                    $v1 = (array)$v1;
                }
                if($v1['url']){
                    $is_model = $v1['is_model'];
                    $images[$k1]['orginal_url'] =  '';
                    $info_type = $info['type'].'_info';
                    if($info['price']&&in_array($info['type'],$this->image_pay_type)){
                        if($is_model){
                            if($is_pay){
                                $images[$k1]['url'] =  deal_weio_image($v1['url'],$info_type);
                                $images[$k1]['is_model'] =  0;
                                $images[$k1]['orginal_url'] =  get_spec_image($v1['url']);
                            }else{
                                $images[$k1]['url'] = deal_weio_image($v1['url'],$info_type,1);
                            }
                        }else{
                            $images[$k1]['url'] =  deal_weio_image($v1['url'],$info_type);
                            $images[$k1]['orginal_url'] =  get_spec_image($v1['url']);
                        }
                    }else{
                        $images[$k1]['url'] =  deal_weio_image($v1['url'],$info_type);
                        $images[$k1]['is_model'] =  0;
                        $images[$k1]['orginal_url'] =  get_spec_image($v1['url']);
                    }


                }
            }

            $root['info']['images'] = $images;
        }
        $root['user_id'] = $user_id;
		$root['invite_info'] = $this->get_invite_info($root['info']['user_id'],$root['info']['weibo_id'],$user_id);
        //整理传递分享链接参数
        $root['invite_info']['imageUrl'] = $root['info']['photo_image'];
        $root['invite_info']['headImage'] = $root['info']['head_image'];
        $root['invite_info']['videoUrl'] = $root['info']['video_url'];

        if($root['comment_list_count'] > $page * $page_size){
            $root['has_next'] = 1;
        }else{
            $root['has_next'] = 0;
        }
        $root['status'] = 1;
        $root['error'] = '';
        $root['app_down_url'] = SITE_DOMAIN_DOWNLOAD."/appdown.php";
		$root['page_info'] = array('page' => $root['page'], 'has_next' => $root['has_next']);
        unset($root['info']['data']);
        unset($root['digg_user_list']);
        if($is_paid){
        	$root['info']['status'] = 1;
            $pay_list =  $root['info'];
            api_ajax_return($pay_list);
        }else{

            api_ajax_return($root);
        }

    }

	public function get_invite_info($user_id = '',$video_id = '',$share_id = ''){
		$m_config =  load_auto_cache("m_config");//初始化手机端配置
		$invite_url = SITE_DOMAIN.'/wap/xr/index.html#/activeIndex?user_id='.$GLOBALS['user_info']['id'];
		$invite_url = SITE_DOMAIN.APP_ROOT.'/wap/index.php?ctl=share&act=svideo&user_id='.$user_id.'&video_id='.$video_id.'&share_id='.$share_id;
		$invite_image = $m_config['weibo_distribution_img'];
		$invite_info['title'] = '【推荐】《'.$m_config['app_name'].'》';
		$invite_info['title'] = '悦塘，分享生活发现美!';
		$nick_name = emoji_decode($GLOBALS['user_info']['nick_name']);
		$invite_info['content'] = $nick_name.' 请您加入 '.$m_config['app_name']."APP,让您毫不费力地发照片挣红包，还有美女帅哥排队等您领！";
		$invite_info['imageUrl'] = empty($invite_image) ? $m_config['app_logo'] : $invite_image;
		$invite_info['clickUrl'] = $invite_url;
		return $invite_info;

	}

    //点赞列表
    public function digg_list(){
        $weibo_id = intval($_REQUEST['weibo_id']);
		$page = intval($_REQUEST['p']) ? intval($_REQUEST['p']) : intval($_REQUEST['page']);//当前页
		$page = intval($page) > 0 ? intval($page) : 1;
        $page_size =20;
        $list = load_auto_cache("select_weibo_digg",array('page'=>$page,'page_size'=>$page_size,'weibo_id'=>$weibo_id));

        if(count($list)==$page_size){
            $root['has_next'] = 1;
        }else{
            $root['has_next'] = 0;
        }

        $root['page'] = $page;
		$root['page_info'] = array('page' => $root['page'], 'has_next' => $root['has_next']);
        $root['status'] = 1;
        $root['error'] = '';
        $root['list'] = $list;
        api_ajax_return($root);

    }

    //商品详情
    public function goods_info(){
        $weibo_id = intval($_REQUEST['weibo_id']);
        $goods_info = $GLOBALS['db']->getRow("select id as weibo_id,price,data as images,photo_image,red_count,content,create_time from ".DB_PREFIX."weibo where id = ".$weibo_id." and type = 'goods'");
        if(!$goods_info){
            $root['error'] = "商品不存在!";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        $root = array(
            'status'=>1,
            'error'=>''
        );
        if($goods_info['photo_image']){
            $goods_info['photo_image'] = deal_weio_image($goods_info['photo_image'],'goods_info');
        }
        if(!$goods_info['images']){
            $root['error'] = "商品图片不存在!";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        $goods_info['images'] = unserialize($goods_info['images']);
        foreach($goods_info['images'] as $k=>$v){
            if($v){
                $goods_info['images'][$k]['url'] = get_spec_image($v['url']);
            }
        }

        $root['goods'] = $goods_info;

        api_ajax_return($root);
    }

    public function add_video_count(){
        if(!$GLOBALS['user_info']){
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        $weibo_id = intval($_REQUEST['weibo_id']);
        $weibo = $GLOBALS['db']->getRow("select id,video_count from ".DB_PREFIX."weibo where id = $weibo_id and type ='video'");
        if(!$weibo){
            $root['error'] = "该动态不存在！.";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        $re = $GLOBALS['db']->query("update ".DB_PREFIX."weibo set video_count=video_count+1 where id = $weibo_id");
        $root['error'] = "";
        $root['status'] = 1;
        $video_count = $GLOBALS['db']->getOne("select video_count from ".DB_PREFIX."weibo where id = $weibo_id ");
        $root['video_count'] = intval($video_count);
        api_ajax_return($root);
    }

    //评论列表分页数据
	public function comment_list()
	{
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			api_ajax_return($root);
		}
		$weibo_id = intval($_REQUEST['weibo_id']);
		$user_id = intval($GLOBALS['user_info']['id']);
		$comment_id = intval($_REQUEST['comment_id']);
		$page = intval($_REQUEST['p']) ? intval($_REQUEST['p']) : intval($_REQUEST['page']);//当前页
		$page = intval($page) > 0 ? intval($page) : 1;
		$page_size = 20;

		if ($comment_id > 0) {
			$child_page = intval($_REQUEST['child_page']) > 0 ? intval($_REQUEST['child_page']) : $page;
			$child_size = 5;
		}

		$svideo_comment_list = load_auto_cache("svideo_comment_list",
			array('page' => $page, 'weibo_id' => $weibo_id, 'child_page' => $child_page, 'user_id' => $user_id, 'comment_id' => $comment_id));
		$root['list'] = $svideo_comment_list['list'];
		$root['list_count'] = $svideo_comment_list['list_count'];
		$root['error'] = "";
		$root['status'] = 1;
		if($root['list_count'] > $page * $page_size){
			$has_next = 1;
		}else{
			$has_next = 0;
		}
		$root['page_info'] = array('page' => $page, 'has_next' => $has_next);
		if ($comment_id > 0) {
			if($root['list_count'] > $child_page * $child_size){
				$has_next = 1;
			}else{
				$has_next = 0;
			}
			$root['page_info'] = array('page' => $child_page, 'has_next' => $has_next);
		}
		api_ajax_return($root);
	}

}

?>