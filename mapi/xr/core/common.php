<?php
/**
 *
 * @param unknown_type $to_user_id  被查看的人
 * @param unknown_type $user_id  查看人
 * @return Ambigous <mixed, multitype:number unknown mixed >
 */
function get_weibo_userinfo($to_user_id,$user_id=0,$page=1,$page_size=20,$pay_type= array()){
    $root = array();
    fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserFollwRedisService.php');
    fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserRedisService.php');
    if($page==1){

//        $user_redis = new UserRedisService();
//        $fields = array('id','fans_count','focus_count','is_authentication','authentication_type','nick_name','signature','sex','province','city','head_image',
//            'user_level','v_type','v_explain','v_icon',
//            'is_remind','birthday','job',
//            'is_robot','room_title','is_nospeaking',
//            'show_image','weibo_count','weixin_account','weixin_price','xpoint','ypoint','weibo_money','weibo_refund_money','weibo_photo_img','weibo_chat_price'
//        );

        //$userinfo = $user_redis->getRow_db($to_user_id,$fields);
        $userinfo = $GLOBALS['db']->getRow("select id,fans_count,focus_count,is_authentication,authentication_type,nick_name,signature,sex,province,city,head_image,user_level,v_type,v_explain,v_icon,is_remind,birthday,job,is_robot,room_title,is_nospeaking,show_image,weibo_count,weixin_account,
            weixin_price,xpoint,ypoint,weibo_money,weibo_refund_money,weibo_photo_img,weibo_chat_price from ".DB_PREFIX."user where id =$to_user_id");
        if($userinfo['id'] === false){
            $userinfo['id'] = $user_id;
        }
        foreach($userinfo as $k=>$v){
            if($v===false){
                if($k=='city'||$k=='head_image'||$k=='v_explain'||$k=='v_icon'){
                    $userinfo[$k] = '';
                }elseif($k=='user_level'){
                    $userinfo[$k] = 1;
                }
                else{
                    $userinfo[$k] = intval($v);
                }
            }
        }

        $userinfo['signature'] = htmlspecialchars_decode($userinfo['signature']);
        $userinfo['nick_name'] = htmlspecialchars_decode($userinfo['nick_name']);
        $userinfo['user_id'] = $to_user_id;
        if($userinfo['show_image']){
            $userinfo['show_image'] = unserialize( $userinfo['show_image']);

            foreach($userinfo['show_image'] as $k=>$v){
                if($v){
                    $userinfo['show_image'][$k] = array(
                        'url'=>get_spec_image($v,100,100,1),
                        'is_model'=>0,
                        'orginal_url'=>get_spec_image($v)
                    );
                }
            }
        }else{
            $userinfo['show_image'] = array();
        }
        $userinfo['head_image'] = deal_weio_image($userinfo['head_image'],'head_image');
        if($userinfo['weibo_photo_img']){
            $userinfo['weibo_photo_img'] = get_spec_image($userinfo['weibo_photo_img'],320,180,1);
        }else{
            $userinfo['weibo_photo_img'] = '';
        }

        if($user_id==$to_user_id){
            $root['is_admin'] = 1;
            $root['is_edit_weixin'] = 1;
            $root['is_show_money'] = 1;
            $root['is_show_talk'] = 0;
            $root['is_show_ds'] = 0;
            $userinfo['money'] = $userinfo['weibo_money'];
            $now=get_gmtime();
            $create_time_1=to_date($now,'Y-m-d');
            $create_time_1=to_timespan($create_time_1);
            $create_time_2=$create_time_1+24*3600;
            $today_money = $GLOBALS['db']->getOne("select sum(money) from ".DB_PREFIX."payment_notice where to_user_id = ".$user_id." and is_paid = 1 and create_time >$create_time_1 and create_time<$create_time_2 ");
            $userinfo['today_money'] = floatval($today_money);
            $userinfo['has_focus'] = 0;
            $userinfo["has_weixin"] = 0;
            $userinfo['weibo_chatprice_hadpay'] = 1;
        }else{
            $root['is_admin'] = 0;
            $root['is_edit_weixin'] = 0;
            $root['is_show_money'] = 0;
            $root['is_show_talk'] = 1;
            $root['is_show_ds'] = 1;
            $userinfo['weibo_chatprice_hadpay'] = 0;;
            //关注
            $userfollw_redis = new UserFollwRedisService($user_id);
            if ($userfollw_redis->is_following($to_user_id)){
                $userinfo['has_focus'] = 1;//0:未关注;1:已关注
            }else{
                $userinfo['has_focus'] = 0;//0:未关注;1:已关注
            }
            $userinfo["has_weixin"] = 0;
            //是否支付过微信
            if($user_id>0){
//                $weixin_pay = $GLOBALS['db']->getOne("select order_id from ".DB_PREFIX."payment_notice where user_id = ".$user_id." and  is_paid =1 and type_cate = 'weixin' and to_user_id = ".$to_user_id);
//               if($weixin_pay){
//                   $userinfo['weibo_chatprice_hadpay'] = 1;
//               }
               $weixin_pay = $GLOBALS['db']->getOne("select sum(money) from ".DB_PREFIX."payment_notice where user_id = ".$user_id." and  is_paid =1 and type_cate = 'reward' and to_user_id = ".$to_user_id);
               if($weixin_pay >= $userinfo['weibo_chat_price']){
                   $userinfo['weibo_chatprice_hadpay'] = 1;
               }

            }

        }
        $root['user'] = $userinfo;
    }
    $root['is_focus'] = 0;
    if($to_user_id==$user_id) {
        $list = load_auto_cache("select_weibo_list", array('page' => $page, 'page_size' => $page_size, 'user_id' => $user_id));

    }else{
        $list = load_auto_cache("select_weibo_other_list", array('page' => $page, 'page_size' => $page_size, 'to_user_id' => $to_user_id));
            if($user_id>0){
                $pay_digg_list = load_auto_cache("select_user_pay_list",array('page'=>$page,'page_size'=>$page_size,'user_id'=>$user_id));
                $order_list_array = $pay_digg_list['order'];
                $diggs_array =  $pay_digg_list['digg'];

                //$root['is_focus'] = 0;
                $user_redis = new UserFollwRedisService($user_id);
                $root['is_focus']  = intval( $user_redis->is_following($to_user_id));
            }else{
                $order_list_array = array();
                $diggs_array =  array();
            }

            foreach($list as $k=>$v){
                if(in_array($v['weibo_id'],$diggs_array)){
                    $list[$k]['has_digg'] = 1;
                }
                if($user_id !=$v['user_id']){
                    $list[$k]['is_top'] = 0;
                }
                if($v['price']>0&&in_array($v['type'],$pay_type)){
                    if(in_array($v['weibo_id'],$order_list_array)){
                        $is_pay =1;
                    }else{
                        $is_pay =0;
                    }
                    if($list[$k]['images_count']>0){
                        $images = $list[$k]['images'] ;
                        foreach($images as $k1=>$v1){
                            if($v1['is_model']){
                                if($is_pay){
                                    $images[$k1]['url'] =  deal_weio_image($v1['url']);
                                    $images[$k1]['is_model'] =  0;
                                    $images[$k1]['orginal_url'] =  get_spec_image($v1['url']);
                                }else{
                                    $images[$k1]['url'] =  deal_weio_image($v1['url'],$v['type'],1);
                                    $images[$k1]['orginal_url'] = '';
                                }
                            }
                        }
                        $list[$k]['images'] = $images;
                    }

                }

            }

    }


    if(count($list)==$page_size){
        $root['has_next'] = 1;
    }else{
        $root['has_next'] = 0;
    }
    $root['list'] = $list;
    $root['page'] = $page;
    $root['status'] = 1;
    $root['error'] = 0;
    return $root;
}

function time_tran($the_time)
{
    $now_time = to_date(NOW_TIME,"Y-m-d H:i:s");
    $now_time = to_timespan($now_time);
    $show_time = to_timespan($the_time);
    $dur = $now_time - $show_time;
    if ($dur < 0) {
        return to_date($show_time,"Y-m-d");
    } else {
        if ($dur < 60) {
            return $dur . '秒前';
        } else {
            if ($dur < 3600) {
                return floor($dur / 60) . '分钟前';
            } else {
                if ($dur < 86400) {
                    return floor($dur / 3600) . '小时前';
                } else {
                    if ($dur < 2592000) {//30天内
                        return floor($dur / 86400) . '天前';
                    } else {
                        return to_date($show_time,"Y-m-d");
                    }
                }
            }
        }
    }
}

function get_file_oss_url($url){
    if($url){
        $pos = strpos($url, $GLOBALS['distribution_cfg']['OSS_DOMAIN']);
        $httppos = strpos($url, 'http');
        if ($pos === false && $httppos === false) {
            $url = str_replace("./public/", "/public/", $url);
            return $GLOBALS['distribution_cfg']['OSS_DOMAIN'] . $url;
        }else{
            return $url;
        }
    }
}

function deal_weio_image($url,$type='',$is_mode=0){
    if($type==''){
        return get_spec_image($url,200,200,1);
    }
    if($is_mode==0){
        switch($type){
            case 'photo_info':
                return get_spec_image($url,375,210,1);
                break;
            case 'video':
                if (defined('OPEN_SVIDEO_MODULE') && OPEN_SVIDEO_MODULE) {
                    return get_spec_image($url,250,250,1);
                } else {
                    return get_spec_image($url,250,140,1);
                }
                break;
            case 'video_info':
                return get_spec_image($url,375,210,1);
                break;
            case 'head_image':
                return get_spec_image($url,300,300,1);
                break;
            default:
                return get_spec_image($url,200,200,1);
                break;
        }
    }else{
        switch($type){
            case 'photo_info':
                return get_spec_image($url,375,210,1,30,15);
                break;
            case 'video_info':
                return get_spec_image($url,375,210,1,30,15);
                break;
            default:
                return get_spec_image($url,200,200,1,30,15);
                break;
        }
    }



}

//人权重变化
function user_weight_insert($user_id,$object_user_id,$weight= 0,$type=0,$memo='',$weibo_id=0,$operation_type=1){
    if(isset($object_user_id)){
        $user_weight = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."weibo_user_weight where user_id = ".$user_id." and  object_user_id =  ".$object_user_id);
        $re = 0;
        if($user_weight){
            $now_weight = $weight + $user_weight['weight'];
            $re = $GLOBALS['db']->query("update ".DB_PREFIX."weibo_user_weight set weight = ".$now_weight.",create_time = ".NOW_TIME." where user_id = ".$user_id." and  object_user_id =  ".$object_user_id);
        }else{
            $data['user_id'] = $user_id;
            $data['object_user_id'] = $object_user_id;
            $data['weight'] = $weight;
            $data['create_time'] = NOW_TIME;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX."weibo_user_weight", $data,'INSERT');
        }
        if($re==1){
            $log = array();
            $log['change_weight'] = $weight;
            $log['memo'] = $memo.',用户id为'.$object_user_id;
            $log['type'] = $type;
            if($user_weight){
                $log['now_weight'] = $user_weight['weight'] ;
                $log['weight'] = $weight + $user_weight['weight'];
            }else{
                $log['now_weight'] = 0 ;
                $log['weight'] = $weight;
            }
            $log['create_time'] =NOW_TIME;
            $log['user_id'] = $user_id;
            $log['status'] = 1;
            $log['weibo_id'] = $weibo_id;
            $log['operation_type'] = $operation_type;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX."weibo_weight_log", $log,'INSERT');
        }
        return $re;
    }
}

//分类权重变化
function classify_weight_insert($user_id,$classify_id,$classify_name,$weight= 0,$type=0,$memo='',$weibo_id=0,$operation_type=1){
    if(isset($classify_id)){
        $classify_weight = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."weibo_classify_weight where user_id = ".$user_id." and  classify_id =  ".$classify_id);
        $re = 0;
        if($classify_weight){
            $now_weight = $weight + $classify_weight['weight'];
            $re = $GLOBALS['db']->query("update ".DB_PREFIX."weibo_classify_weight set weight = ".$now_weight.",create_time = ".NOW_TIME." where user_id = ".$user_id." and  classify_id =  ".$classify_id);
        }else{
            $data['user_id'] = $user_id;
            $data['classify_id'] = $classify_id;
            $data['weight'] = $weight;
            $data['classify_name'] = $classify_name;
            $data['create_time'] = NOW_TIME;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX . "weibo_classify_weight", $data, 'INSERT');
        }
        if($re==1){
            $log = array();
            $log['change_weight'] = $weight;
            $log['memo'] = $memo.',分类:'.$classify_name.'id为'.$classify_id;
            $log['type'] = $type;
            if($classify_weight){
                $log['now_weight'] = $classify_weight['weight'] ;
                $log['weight'] = $weight + $classify_weight['weight'];
            }else{
                $log['now_weight'] = 0 ;
                $log['weight'] = $weight;
            }
            $log['create_time'] =NOW_TIME;
            $log['user_id'] = $user_id;
            $log['status'] = 2;
            $log['weibo_id'] = $weibo_id;
            $log['operation_type'] = $operation_type;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX."weibo_weight_log", $log,'INSERT');
        }
        return $re;
    }
}

//标签权重变化
function tag_weight_insert($user_id,$tag,$tag_id,$weight= 0,$type=0,$memo='',$weibo_id=0,$operation_type=1){
    if(isset($tag_id)){
        $tag_weight = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."weibo_tag_weight where user_id = ".$user_id." and  tag_id =  ".$tag_id);
        $re = 0;
        if($tag_weight){
            $now_weight = $weight + $tag_weight['weight'];
            $re = $GLOBALS['db']->query("update ".DB_PREFIX."weibo_tag_weight set weight = ".$now_weight.",create_time = ".NOW_TIME." where user_id = ".$user_id." and  tag_id =  ".$tag_id);
        }else{
            $data['user_id'] = $user_id;
            $data['tag_id'] = $tag_id;
            $data['weight'] = $weight;
            $data['tag'] = $tag;
            $data['create_time'] = NOW_TIME;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX . "weibo_tag_weight", $data, 'INSERT');
        }
        if($re==1){
            $log = array();
            $log['change_weight'] = $weight;
            if($operation_type== 5){
                $log['memo'] = $memo;
            }else{
                $log['memo'] = $memo.',标签:'.$tag.'id为'.$tag_id;
            }
            $log['type'] = $type;
            if($tag_weight){
                $log['now_weight'] = $tag_weight['weight'] ;
                $log['weight'] = $weight + $tag_weight['weight'];
            }else{
                $log['now_weight'] = 0 ;
                $log['weight'] = $weight;
            }
            $log['create_time'] =NOW_TIME;
            $log['user_id'] = $user_id;
            $log['status'] = 3;
            $log['weibo_id'] = $weibo_id;
            $log['operation_type'] = $operation_type;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX."weibo_weight_log", $log,'INSERT');
        }
        return $re;
    }
}

//音乐权重变化
function music_weight_insert($user_id,$music_id,$weight= 0,$type=0,$memo='',$weibo_id=0,$operation_type=1){
    if(isset($music_id)){
        $music_weight = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."weibo_music_weight where user_id = ".$user_id." and  music_id =  ".$music_id);
        $re = 0;
        if($music_weight){
            $now_weight = $weight + $music_weight['weight'];
            $re = $GLOBALS['db']->query("update ".DB_PREFIX."weibo_music_weight set weight = ".$now_weight.",create_time = ".NOW_TIME." where user_id = ".$user_id." and  music_id =  ".$music_id);
        }else{
            $data['user_id'] = $user_id;
            $data['music_id'] = $music_id;
            $data['weight'] = $weight;
            $data['create_time'] = NOW_TIME;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX . "weibo_music_weight", $data, 'INSERT');
        }
        if($re==1){
            $log = array();
            $log['change_weight'] = $weight;
            if($operation_type== 6){
                $log['memo'] = $memo;
            }else{
                $log['memo'] = $memo.',音乐id为'.$music_id;
            }
            $log['type'] = $type;
            if($music_weight){
                $log['now_weight'] = $music_weight['weight'] ;
                $log['weight'] = $weight + $music_weight['weight'];
            }else{
                $log['now_weight'] = 0 ;
                $log['weight'] = $weight;
            }
            $log['create_time'] =NOW_TIME;
            $log['user_id'] = $user_id;
            $log['status'] = 4;
            $log['weibo_id'] = $weibo_id;
            $log['operation_type'] = $operation_type;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX."weibo_weight_log", $log,'INSERT');
        }
        return $re;
    }
}

//地点权重变化
function address_weight_insert($user_id,$province,$city,$weight= 0,$type=0,$memo='',$weibo_id=0,$operation_type=1){
    if(isset($city) && isset($province)){
        $address_weight = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."weibo_address_weight where user_id = ".$user_id." and city = '".$city."' and province = '".$province."'");
        $re = 0;
        if($address_weight){
            $now_weight = $weight + $address_weight['weight'];
            $re = $GLOBALS['db']->query("update ".DB_PREFIX."weibo_address_weight set weight = ".$now_weight.",create_time = ".NOW_TIME." where user_id = ".$user_id." and city = '".$city."' and province = '".$province."'");
        }else{
            if($type==0) {
                $data['user_id'] = $user_id;
                $data['province'] = $province;
                $data['city'] = $city;
                $data['weight'] = $weight;
                $data['create_time'] = NOW_TIME;
                $re = $GLOBALS['db']->autoExecute(DB_PREFIX . "weibo_address_weight", $data, 'INSERT');
            }
        }
        if($re==1){
            $log = array();
            $log['change_weight'] = $weight;
            $log['memo'] = $memo.',地点为'.$province.$city;
            $log['type'] = $type;
            if($address_weight){
                $log['now_weight'] = $address_weight['weight'] ;
                $log['weight'] = $weight + $address_weight['weight'];
            }else{
                $log['now_weight'] = 0 ;
                $log['weight'] = $weight;
            }
            $log['create_time'] =NOW_TIME;
            $log['user_id'] = $user_id;
            $log['status'] = 5;
            $log['weibo_id'] = $weibo_id;
            $log['operation_type'] = $operation_type;
            $re = $GLOBALS['db']->autoExecute(DB_PREFIX."weibo_weight_log", $log,'INSERT');
        }
        return $re;
    }

}
/*
 * 小视频权重变化
 * user_id用户id
 * weibo_id小视频id
 * memo备注
 * status权重类型：1点赞,2评论，3关注，4转发，5发布同标签视频，6使用同歌曲,7连续观看同一作者3个视频，8重复播放同一视频3次,9观看作者个人中心，10正常观看视频，11快速翻过5秒时间内，12取消点赞,13删除评论，14取消关注，15不感兴趣
 */
function create_weight ($user_id,$weibo_id,$memo,$status=1,$tag_title='',$tag_id=0){
    $config_info = $GLOBALS['db']->getRow("SELECT * FROM ".DB_PREFIX."weibo_weight_config WHERE is_effect = 1 and type = ".$status);
    $weibo_info = $GLOBALS['db']->getRow("SELECT * FROM ".DB_PREFIX."weibo WHERE id = ".$weibo_id);
    if($config_info){
        if($config_info['user_weight']!=0){
            if($config_info['user_weight']>0){
                $type = 0 ;
            }else{
                $type = 1 ;
            }
            user_weight_insert($user_id,$weibo_info['user_id'],$config_info['user_weight'],$type,$memo,$weibo_id,$status);
        }
        if($config_info['classify_weight']!=0){
            if($config_info['classify_weight']>0){
                $type = 0 ;
            }else{
                $type = 1 ;
            }
            if ($weibo_info['classify_id'] > 0) {
                $classify_name = $GLOBALS['db']->getOne("SELECT title FROM ".DB_PREFIX."weibo_classify WHERE id = ".$weibo_info['classify_id']);
                classify_weight_insert($user_id,$weibo_info['classify_id'],$classify_name,$config_info['classify_weight'],$type,$memo,$weibo_id,$status);
            }
        }
        if($config_info['tag_weight']!=0){
            if($config_info['tag_weight']>0){
                $type = 0 ;
            }else{
                $type = 1 ;
            }
            if($tag_id>0){
                tag_weight_insert($user_id,$tag_title,$tag_id,$config_info['tag_weight'],$type,$memo,$weibo_id,$status);
            }else{
                $tags =array();
                if ($weibo_info['tags'] != '') {
                    $tags = json_decode($weibo_info['tags'],TRUE);
                    if($tags){
                        foreach ($tags as $k=>$v){
                            tag_weight_insert($user_id,$v['title'],$v['id'],$config_info['tag_weight'],$type,$memo,$weibo_id,$status);
                        }
                    }
                }
            }

        }
        if($config_info['music_weight']!=0){
            if($config_info['music_weight']>0){
                $type = 0 ;
            }else{
                $type = 1 ;
            }
            if ($weibo_info['audio_id'] > 0) {
                music_weight_insert($user_id,$weibo_info['audio_id'],$config_info['music_weight'],$type,$memo,$weibo_id,$status);
            }
        }
        if($config_info['address_weight']!=0){
            if($config_info['address_weight']>0){
                $type = 0 ;
            }else{
                $type = 1 ;
            }
            if($weibo_info['province']!= '' && $weibo_info['city']!=''){
                address_weight_insert($user_id,$weibo_info['province'],$weibo_info['city'],$config_info['address_weight'],$type,$memo,$weibo_id,$status);
            }

        }
    }

}
