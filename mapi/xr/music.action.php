<?php
class MusicCModule extends baseCModule
{
	public function music_list()
	{
		if (!$GLOBALS['user_info']){
		$root['error'] = "用户未登陆,请先登陆.";
		$root['status'] = 0;
		$root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
		} else {
			$root['status'] = 1;
			$root['error'] = '';
			$root['music_classified'] = array();
			$music_classified = load_auto_cache("music_classified");
			if($music_classified){
				$root['music_classified'] = $music_classified;
			}
			$classified_id = intval($_REQUEST['classified_id']);
			if ($classified_id == 99) {
				$my_music = $GLOBALS['db']->getAll("SELECT m.audio_id,m.audio_name,m.artist_name,m.audio_link,m.time_len,m.classified_id
												FROM ".DB_PREFIX."svideo_music as m LEFT JOIN ".DB_PREFIX."music_list as l ON m.audio_id = l.id
												WHERE m.user_id = {$GLOBALS['user_info']['id']} AND l.is_effect = 1 ");
				$root['list'] = $my_music;
			} else {
				$root['list'] = load_auto_cache('music_list',array ('classified_id'=>$classified_id));
			}
		}
//		$root['list']['我的'] = $my_music;
		ajax_return($root);
	}

	public function add_music()
	{
		if (!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
		} else {
			$root['status'] = 1;
			$root['error'] = '';
			$audio_id = intval($_REQUEST['audio_id']);
			if ($audio_id == '') {
				$root['status'] = 0;
				$root['error'] = '音乐id为空';
				ajax_return($root);
			}
			$music_info = $GLOBALS['db']->getRow("SELECT id as audio_id,audio_name,artist_name,audio_link,time_len,classified_id FROM ".DB_PREFIX."music_list WHERE id = {$audio_id} and is_effect = 1");
			if ($music_info == '') {
				$root['status'] = 0;
				$root['error'] = '音乐无效或不存在';
				ajax_return($root);
			}
			$music_info['create_time'] = NOW_TIME;
			$music_info['user_id'] = intval($GLOBALS['user_info']['id']);
			$get_user_music = $GLOBALS['db']->getOne("SELECT audio_id FROM ".DB_PREFIX."svideo_music WHERE audio_id = {$music_info['audio_id']} and user_id = {$music_info['user_id']} ");
			if (!$get_user_music) {
				$res = $GLOBALS['db']->autoExecute(DB_PREFIX."svideo_music", $music_info,'INSERT');
				if ($res == false) {
					$root['status'] = 0;
					$root['error'] = '使用失败';
					ajax_return($root);
				}
			}
		}
		ajax_return($root);
	}
}
?>
