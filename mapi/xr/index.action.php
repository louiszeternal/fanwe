<?php
// +----------------------------------------------------------------------
// | XX 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// | Author:
// +----------------------------------------------------------------------
class indexCModule extends baseCModule
{
	public function new_index()
	{
		$to_user_id = intval($_REQUEST['user_id']);
		if($GLOBALS['user_info']){
			$user_id = $GLOBALS['user_info']['id'];
		}else{
			$user_id = 0;
		}
		//更新用户登录时间
		if($user_id!=0){
		    if($GLOBALS['user_info']['is_login'] == 0 ){
                fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/BaseRedisService.php');
                fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserRedisService.php');
                $user_redis = new UserRedisService();
                $GLOBALS['db']->query("update ".DB_PREFIX."user set is_login = 1,login_time= '".to_date(NOW_TIME)."' where id =".$user_id);
                $user_redis->update_db($user_id,array("login_time"=>to_date(NOW_TIME)));
            }

        }
		$page = intval($_REQUEST['p']) ? intval($_REQUEST['p']) : intval($_REQUEST['page']);//当前页
		$page = $page ? $page : 1;
		$m_config =  load_auto_cache("m_config");//初始化手机端配置
		if($m_config['weibo_list_label']){
			$weibo_list_label = explode(',',$m_config['weibo_list_label']);
		}else{
			$weibo_list_label = array();
		}
		$root = array(
//			'first_label'=>$m_config['weibo_first_label'],
//			'list_label'=>$weibo_list_label,

			'has_next'=>1,
			'page'=>$page,
			'status'=>1,
			'error'=>''
		);

		$page_size = 5;
		$list = load_auto_cache("select_weibo_new_index",array('page'=>$page,'page_size'=>$page_size,'type'=>'video','user_id' => $user_id,'to_user_id' => $to_user_id));
		if ($to_user_id == '') {
			shuffle($list);
		}
		$root['list'] = $list;
		if(count($list)>=$page_size){
			$root['has_next'] = 1;
		}else{
			$root['has_next'] = 0;
		}
		if ($to_user_id > 0) {
			$root['has_next'] = 0;
		}
		$root['invite_info'] = $this->get_invite_info2();
		$root['page_info'] = array('page' => $root['page'], 'has_next' => $root['has_next']);
		api_ajax_return($root);
	}

	public function test_index()
	{
		$to_user_id = intval($_REQUEST['user_id']);
		if($GLOBALS['user_info']){
			$user_id = $GLOBALS['user_info']['id'];
		}else{
			$user_id = 0;
		}
		$page = intval($_REQUEST['page'])?intval($_REQUEST['page']):intval($_REQUEST['p']);
		$page = $page ? $page : 1;
		$m_config =  load_auto_cache("m_config");//初始化手机端配置
		if($m_config['weibo_list_label']){
			$weibo_list_label = explode(',',$m_config['weibo_list_label']);
		}else{
			$weibo_list_label = array();
		}
		$root = array(
//			'first_label'=>$m_config['weibo_first_label'],
//			'list_label'=>$weibo_list_label,

			'has_next'=>1,
			'page'=>$page,
			'status'=>1,
			'error'=>''
		);

		$page_size = 50;
		$list = load_auto_cache("select_weibo_new_index",array('page'=>$page,'page_size'=>$page_size,'type'=>'video','user_id' => $user_id,'to_user_id' => $to_user_id));
		shuffle($list);
		$root['list'] = $list;
		if(count($list)==$page_size){
			$root['has_next'] = 1;
		}else{
			$root['has_next'] = 0;
		}
		$root['invite_info'] = $this->get_invite_info2();
		api_ajax_return($root);
	}

	public function get_invite_info2($distribution_rate,$invite_image,$app_name = ''){
		$m_config =  load_auto_cache("m_config");//初始化手机端配置
		$invite_url = SITE_DOMAIN.'/wap/xr/index.html#/activeIndex?user_id='.$GLOBALS['user_info']['id'];
		$invite_image = $m_config['weibo_distribution_img'];
		$invite_info['title'] = '【推荐】《'.$m_config['program_title'].'》';
		$nick_name = emoji_decode($GLOBALS['user_info']['nick_name']);
		$invite_info['content'] = $nick_name.' 请您加入 '.$app_name."APP,让您毫不费力地发照片挣红包，还有美女帅哥排队等您领！";
		$invite_info['imageUrl'] = empty($invite_image) ? $m_config['app_logo'] : $invite_image;
		$invite_info['clickUrl'] = $invite_url;
		return $invite_info;

	}

    //首页-美女列表
    public function index()
    {
		$page = intval($_REQUEST['p']) ? intval($_REQUEST['p']) : intval($_REQUEST['page']);//当前页
		$page = intval($page) > 0 ? intval($page) : 1;
        $m_config =  load_auto_cache("m_config");//初始化手机端配置
        if($m_config['weibo_list_label']){
            $weibo_list_label = explode(',',$m_config['weibo_list_label']);
        }else{
            $weibo_list_label = array();
        }



        $root = array(
            'first_label'=>$m_config['weibo_first_label'],
            'list_label'=>$weibo_list_label,
            'has_next'=>1,
            'page'=>$page,
            'status'=>1,
            'error'=>''
        );

        $root['invite_info'] = $this->get_invite_info($m_config['distribution_rate'],$m_config['weibo_distribution_img'],$m_config['program_title']);

        $page_size =20;

        if($page>1){
            $list = load_auto_cache("select_user_index",array('page'=>$page,'page_size'=>$page_size));
            $root['list'] = $list;
            unset($root['first']);
        }else{

            $list =  load_auto_cache("select_user_index",array('page'=>1,'page_size'=>$page_size));
            if(count($list)>6){
                $root['first'] = array_slice($list,0,6,true);
                $root['list'] =  array_slice($list,6);
            }else{
                $root['first'] = $list;
                $root['list'] = array();
            }
            $root['banner'] = load_auto_cache("banner_list_xr",array('type'=>10));
            if($root['banner']==false){
                $root['banner'] = array();
            }
        }
        if(count($list)==$page_size){
            $root['has_next'] = 1;
        }else{
            $root['has_next'] = 0;
        }
		$root['page_info'] = array('page' => $root['page'], 'has_next' => $root['has_next']);
        api_ajax_return($root);
    }
    public function get_invite_info($distribution_rate,$invite_image,$app_name = ''){
        if($GLOBALS['user_info'])
        {
           // $invite_url = url('live#show', array('user_id' => $GLOBALS['user_info']['id'],'itype'=>'xr'));
            $invite_url = SITE_DOMAIN.'/wap/xr/index.html#/activeIndex?user_id='.$GLOBALS['user_info']['id'];
            //$invite_url = SITE_DOMAIN.'/wap/index.php?ctl=distribution&act=init_register&user_id='.$GLOBALS['user_info']['id'];

            $invite_info = array(
                'invite_url'=>$invite_url, //用户推荐链接
                'invite_image'=>$invite_image, //推荐图片
                'sharing_info' =>'他(她)的每一笔消费，您都分成'.intval($distribution_rate) .'%',
            );
        }else{
            $invite_info = array(
                'invite_url'=>'', //用户推荐链接
                'invite_image'=>$invite_image, //推荐图片
                'sharing_info' => intval($distribution_rate) .'%',
            );
        }
        $invite_info['title'] = '【推荐】《'.$app_name.'》';
        $invite_info['content'] = $GLOBALS['user_info']['nick_name'].' 请您加入 '.$app_name."APP,让您毫不费力地发照片挣红包，还有美女帅哥排队等您铃！";
        $invite_info['imageUrl'] = $invite_image;
        $invite_info['clickUrl'] = $invite_url;
        return $invite_info;

    }
    //首页-写真列表
    public  function select_photo(){
        if(!$GLOBALS['user_info']){
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 2;
            api_ajax_return($root);
        }
		$page = intval($_REQUEST['p']) ? intval($_REQUEST['p']) : intval($_REQUEST['page']);//当前页
		$page = intval($page) > 0 ? intval($page) : 1;
        $m_config =  load_auto_cache("m_config");//初始化手机端配置
        if($m_config['weibo_list_label']){
            $weibo_list_label = explode(',',$m_config['weibo_list_label']);
        }else{
            $weibo_list_label = array();
        }
        $root = array(
            'first_label'=>$m_config['weibo_first_label'],
            'list_label'=>$weibo_list_label,
            'has_next'=>1,
            'page'=>$page,
            'status'=>1,
            'error'=>''
        );
//        if(!$GLOBALS['user_info']){
//            $root['user_login_status'] = 0;
//        }else{
//            $root['user_login_status'] = 1;
//        }

        $root['invite_info'] = $this->get_invite_info($m_config['distribution_rate'],$m_config['weibo_distribution_img']);

        $page_size =20;
        $list = load_auto_cache("select_weibo_index",array('page'=>$page,'page_size'=>$page_size,'type'=>'photo'));
        if($page>1){
            $root['list'] = $list;
            unset($root['first']);
        }else{
            if(count($list)>6){
                $root['first'] = array_slice($list,0,6,true);
                $root['list'] =  array_slice($list,6);
            }else{
                $root['first'] = $list;
                $root['list'] = array();
            }
            $root['banner'] = load_auto_cache("banner_list_xr",array('type'=>11));
            if($root['banner']==false){
                $root['banner'] = array();
            }
        }
        if(count($list)==$page_size){
            $root['has_next'] = 1;
        }else{
            $root['has_next'] = 0;
        }
		$root['page_info'] = array('page' => $root['page'], 'has_next' => $root['has_next']);
        api_ajax_return($root);

    }

    //首页-视频列表
    public function select_video(){
		$page = intval($_REQUEST['p']) ? intval($_REQUEST['p']) : intval($_REQUEST['page']);//当前页
		$page = intval($page) > 0 ? intval($page) : 1;
        $m_config =  load_auto_cache("m_config");//初始化手机端配置
        if($m_config['weibo_list_label']){
            $weibo_list_label = explode(',',$m_config['weibo_list_label']);
        }else{
            $weibo_list_label = array();
        }
        $root = array(
            'first_label'=>$m_config['weibo_first_label'],
            'list_label'=>$weibo_list_label,

            'has_next'=>1,
            'page'=>$page,
            'status'=>1,
            'error'=>''
        );

        $page_size =20;
        $list = load_auto_cache("select_weibo_index",array('page'=>$page,'page_size'=>$page_size,'type'=>'video'));
        $root['list'] = $list;
        if(count($list)==$page_size){
            $root['has_next'] = 1;
        }else{
            $root['has_next'] = 0;
        }
		$root['page_info'] = array('page' => $root['page'], 'has_next' => $root['has_next']);
        api_ajax_return($root);
    }

    //获取美女列表
    public function get_user_list(){
        $root['list'] = load_auto_cache("select_user_index",array('page'=>1));
         echo json_encode($root);
    }
    //获取写真列表
    public function get_photo_list(){
        $root['banner'] = load_auto_cache("banner_list_xr",array('type'=>11));
        var_dump($root);
    }
    //获取视频列表
    public function get_video_list(){
        echo $this->time_tran('2017-03-15 20:17:00');
    }
    //热门搜索
    public function hot_search(){
    	$m_config =  load_auto_cache("m_config");//初始化手机端配置
        if($m_config['weibo_list_label']){
            $weibo_list_label = explode(',',$m_config['weibo_list_label']);
        }else{
            $weibo_list_label = array(
                '鲜肉','客服妹妹','松果儿','赵雅依'
            );
        }
        $root = array(
            'first_label'=>$m_config['weibo_first_label'],
            'list_label'=>$weibo_list_label,

            'status'=>1,
            'error'=>''
        );
        api_ajax_return($root);
    }

    public  function time_tran($the_time)
    {
        $now_time = to_date(NOW_TIME,"Y-m-d H:i:s");
        $now_time = to_timespan($now_time);
        $show_time = to_timespan($the_time);
        $dur = $now_time - $show_time;
        if ($dur < 0) {
            return $the_time;
        } else {
            if ($dur < 60) {
                return $dur . '秒前';
            } else {
                if ($dur < 3600) {
                    return floor($dur / 60) . '分钟前';
                } else {
                    if ($dur < 86400) {
                        return floor($dur / 3600) . '小时前';
                    } else {
                        if ($dur < 2592000) {//30天内
                            return floor($dur / 86400) . '天前';
                        } else {
                            return to_date($show_time,"Y-m-d");;
                        }
                    }
                }
            }
        }
    }


    //获取个人的视频列表
    public  function syn_user_weibo_list($user_id)
    {

        if(intval($_REQUEST['user_id']))
        $user_id = intval($_REQUEST['user_id']);
        $m_config =  load_auto_cache("m_config");//初始化手机端配置
        $total_user_number = intval($m_config['total_user_number'])?intval($m_config['total_user_number']):1000;

        //$has_seen_list=array();//看过的视频
        //$user_id=$GLOBALS['user_info']['id'];
        //当前用户拉黑的id集合

        $user_black = $GLOBALS['db']->getAll("SELECT black_user_id FROM ".DB_PREFIX."black WHERE user_id = ".$user_id);
        $user_self_black = $GLOBALS['db']->getAll("SELECT user_id as black_user_id FROM ".DB_PREFIX."black WHERE black_user_id = ".$user_id);
        $black_array_merge = array_merge($user_black,$user_self_black);
        $where="";
        if ($black_array_merge) {
            $user_black_ids = array ();
            foreach ($black_array_merge as $k => $v) {
                $user_black_ids[] = $v['black_user_id'];
            }
            $where= " and user_id not in (".implode(',',$user_black_ids).")";
        }
        //剔除看过的
        $weibo_list=$GLOBALS['db']->getAll("SELECT * FROM ".DB_PREFIX."weibo WHERE status=1 and is_adv=0 and is_audit=1  and id NOT IN(SELECT weibo_id from ".DB_PREFIX."weibo_see WHERE user_id=".$user_id.")  ".$where." order by create_time desc limit 0,".$total_user_number);
        /*if (count($weibo_list)<=30){
            $weibo_list=$GLOBALS['db']->getAll("SELECT * FROM ".DB_PREFIX."weibo WHERE status=1 and is_adv=0 and is_audit=1 and create_time >'".to_date(NOW_TIME-172800,"Y-m-d H:i:s")."' and id NOT IN(SELECT weibo_id from ".DB_PREFIX."weibo_see WHERE user_id=".$user_id.")");
        }
        if (count($weibo_list)<=30) {
            $weibo_list = $GLOBALS['db']->getAll("SELECT * FROM " . DB_PREFIX . "weibo WHERE status=1 and is_adv=0 and is_audit=1 and create_time >'" . to_date(NOW_TIME - 259200, "Y-m-d H:i:s") . "' and id NOT IN(SELECT weibo_id from " . DB_PREFIX . "weibo_see WHERE user_id=" . $user_id . ")");
        }*/

        //个人权重
        $weibo_user_weight_list=$GLOBALS['db']->getAll("SELECT * FROM ".DB_PREFIX."weibo_user_weight WHERE user_id=".$user_id);
        $weibo_classify_weight_list=$GLOBALS['db']->getAll("SELECT * FROM ".DB_PREFIX."weibo_classify_weight WHERE user_id=".$user_id);
        $weibo_tag_weight_list=$GLOBALS['db']->getAll("SELECT * FROM ".DB_PREFIX."weibo_tag_weight WHERE user_id=".$user_id);
        $weibo_address_weight_list=$GLOBALS['db']->getAll("SELECT * FROM ".DB_PREFIX."weibo_address_weight WHERE user_id=".$user_id);
        $weibo_music_weight_list=$GLOBALS['db']->getAll("SELECT * FROM ".DB_PREFIX."weibo_music_weight WHERE user_id=".$user_id);

        //计算分值
        foreach($weibo_list as $key => $value){
            $all_weight=0;
            //个人分
            foreach($weibo_user_weight_list as $k => $v){
                if ($v['object_user_id']==$value['user_id']){
                    $all_weight=$all_weight+$v['weight']+$GLOBALS['db']->getOne("SELECT weight FROM ".DB_PREFIX."user WHERE id=".$value['user_id']);
                    break;
                }
            }

            //分类分
            foreach($weibo_classify_weight_list as $k => $v){
                if ($v['classify_id']==$value['classify_id']){
                    $all_weight=$all_weight+$v['weight']+$GLOBALS['db']->getOne("SELECT weight FROM ".DB_PREFIX."weibo_classify WHERE id=".$value['classify_id']);
                    break;
                }
            }

            $value['tags']=json_decode($value['tags'],true);
            $count_num=count($value['tags']);
            //标签分
            $list_i=0;
            foreach($weibo_tag_weight_list as $k => $v){
                //标签组
                $i=0;
                foreach($value['tags'] as $tagk => $tagv){
                    if ($tagv['id']==$v['tag_id']){
                        $all_weight=$all_weight+$v['weight']+$GLOBALS['db']->getOne("SELECT weight FROM ".DB_PREFIX."weibo_tag WHERE id=".$tagv['id']);
                        $i++;
                        $list_i++;
                        if ($i>=$count_num){
                            break;
                        }
                    }
                }

                if ($list_i>=$count_num){
                    break;
                }

            }

            //区域分
            foreach($weibo_address_weight_list as $k => $v){

                //city
                if ($v['city']==$value['city']){
                    $all_weight=$all_weight+$v['weight']+$GLOBALS['db']->getOne("SELECT weight FROM ".DB_PREFIX."weibo_address WHERE city='".$value['city']."'");
                    break;
                }
            }

            //音乐分
            foreach($weibo_music_weight_list as $k => $v){
                if ($v['music_id']==$value['audio_id']){
                    $all_weight=$all_weight+$v['weight']+$GLOBALS['db']->getOne("SELECT weight FROM ".DB_PREFIX."music_list WHERE id=".$value['audio_id']);
                    break;
                }
            }

            $weibo_list[$key]['all_weight']=$all_weight;
            //$weibo_list[$key]['all_weight']=$key;
        }

        //冒泡
        $new_weibo_list=array();
        $weight_arrayt_test=array();
        foreach ($weibo_list as $key => $row)
        {
            $weight_array[$key]  = $row['all_weight'];
            $new_weibo_list[$key] = $row;
            $weight_arrayt_test[$row['id']]  = $row['all_weight'];
        }

        array_multisort($weight_array, SORT_DESC, $new_weibo_list, SORT_ASC);

        /*if (count($new_weibo_list)==0){
            $new_weibo_list=$GLOBALS['db']->getAll("SELECT * FROM ".DB_PREFIX."weibo WHERE status=1 limit 0,3");
        }*/
        //print_r($new_weibo_list);
        $new_weibo_list_redis=array();
        foreach ($new_weibo_list as $key => $row){
            //$new_weibo_list_redis[$key]=json_encode($row,true);
            $new_weibo_list_redis[$key]=$row['id'];
        }
        //print_r($new_weibo_list_redis);
        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/WeiboRedisService.php');
        $redis    = new WeiboRedisService();
        $redis->del($user_id);
        $redis->set($user_id, $new_weibo_list_redis);


        //测试功能
//        $redis->del("1000".$user_id);
//        $redis->set("1000".$user_id, $weight_arrayt_test);

        $new_weibo_list11=$redis->get($user_id);
        $sql = "update  ".DB_PREFIX."user set weibo_num = ".count($new_weibo_list11).", is_update=1 where id = ".$user_id;
        $res = $GLOBALS['db']->query($sql);

        return $res;
        //print_r($new_weibo_list11);

    }


    //定时重置用户没有更新
    function weibo_user(){
        $GLOBALS['db']->query("update ".DB_PREFIX."user set is_update = 0 where is_update = 1 ");
        $GLOBALS['db']->query("update ".DB_PREFIX."user set is_login = 0 where is_login = 1 ");
    }
    //有效登录的用户定时更新
    function video(){
//            set_time_limit(0);
        $login_time =NOW_TIME - 7*24*60*60;
        $user_list = $GLOBALS['db']->getAll("select login_time,id from " . DB_PREFIX . "user where is_effect = 1 and is_update = 0 and login_time > ".$login_time." order by weibo_num asc limit 100");
        $num = 0;
        foreach ($user_list as $k=>$value){
                $num++;
                //生成记录，根据权重
                $this->syn_user_weibo_list($value['id']);
        }
        echo $num;
    }

    //定时器更新一次
    function once_video(){
//        set_time_limit(0);
        $m_config = load_auto_cache('m_config');
        $sql = "SELECT count(*) FROM " . DB_PREFIX . "weibo WHERE status=1 and is_adv=0 and is_audit=1 and create_time > '" . to_date(NOW_TIME - (7*24*60*60), "Y-m-d H:i:s") . "' ";
        $weibo_count = $GLOBALS['db']->getOne($sql);
        $login_time =NOW_TIME - 7*24*60*60;
        $num = 0;
        if($weibo_count >= $m_config['update_user_number']) {
            $user_list = $GLOBALS['db']->getAll("select login_time,id from " . DB_PREFIX . "user where is_effect = 1 and weibo_num < ".$m_config['update_user_number']." and login_time > ".$login_time."  order by weibo_num asc limit 100 ");
            foreach ($user_list as $k => $value) {
                $num++;
                $this->syn_user_weibo_list($value['id']);
            }
        }
        echo $num;
    }

    //定时器更新一次
    function get_test(){

        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/WeiboRedisService.php');
        $redis    = new WeiboRedisService();
        $new_weibo_list=$redis->get(intval($_REQUEST['user_id']));
        //print_r($new_weibo_list);
        foreach ($new_weibo_list as $k => $value) {
            print_r($new_weibo_list[$k]);
            echo "<pre>";
        }

    }

    function get_weight(){

        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/WeiboRedisService.php');
        $redis    = new WeiboRedisService();
        $new_weibo_list=$redis->get("1000".intval($_REQUEST['user_id']));
        foreach ($new_weibo_list as $k => $value) {
            print_r("id=".$k."  weight=".$new_weibo_list[$k]);
            echo "<pre>";
        }


    }

}

?>