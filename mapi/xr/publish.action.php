<?php
// +----------------------------------------------------------------------
// | XX 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// | Author:
// +----------------------------------------------------------------------c
class publishCModule extends baseCModule
{
	//发布小视频时验证
	public function verify_user()
	{
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			api_ajax_return($root);
		}
		$m_config = load_auto_cache('m_config');
		$publish_svideo_level = intval($m_config['publish_svideo_level']);
		$user_id = intval($GLOBALS['user_info']['id']);
		fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserRedisService.php');
		$user_redis = new UserRedisService();
		$userinfo = $user_redis->getRow_db($user_id,array ('user_level'));
		if ($userinfo['user_level'] < $publish_svideo_level) {
			$root['error'] = "用户等级大于等于{$publish_svideo_level}级才可发布小视频";
			$root['status'] = 0;
			api_ajax_return($root);
		}
		$root['error'] = "";
		$root['status'] = 1;
		api_ajax_return($root);
	}

	//小视频发布页面 初始化
	public function init()
	{
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			api_ajax_return($root);
		}
		$m_config = load_auto_cache("m_config");
		$secret_id = $m_config['qcloud_secret_id'];
		$secret_key = $m_config['qcloud_secret_key'];
		if ($secret_id == '' || $secret_key == '') {
			$root = array(
				'status'=>0,
				'error'=>'生成签名失败,云API帐户或云API密钥为空',
				'sign'=>''
			);
			api_ajax_return($root);
		}
		// 确定签名的当前时间和失效时间
		$current = time();
		$expired = $current + 86400;  // 签名有效期：1天
		// 向参数列表填入参数
		$arg_list = array(
			"secretId" => $secret_id,
			"currentTimeStamp" => $current,
			"expireTime" => $expired,
			"random" => rand());
		// 计算签名
		$orignal = http_build_query($arg_list);
		$signature = base64_encode(hash_hmac('SHA1', $orignal, $secret_key, true).$orignal);
		if (!$signature) {
			$root = array(
				'status'=>0,
				'error'=>'生成签名失败',
				'sign'=>''
			);
			api_ajax_return($root);
		}

        $root['status']=1;
		//签名
		$root['sign'] = $signature;
		//评论权限
		$root['comment_restrict_list'] = [
			['content' => '所有人可评论','comment_restrict' =>1],
			['content' => '仅关注的人可评论','comment_restrict' =>2],
			['content' => '不可评论','comment_restrict' =>3],
		];
		//分享信息
		$root['share_info'] = $this->share_info(1);
        $store_status=$GLOBALS['db']->getOne("SELECT store_status FROM  ".DB_PREFIX."user  WHERE id= ".$GLOBALS['user_info']['id']);
        if ($store_status==2){
            $root['show_goods']=1;
        }else{
            $root['show_goods']=0;
        }

		api_ajax_return($root);
	}
	
    //发布信息
    public function do_publish(){
        $root = array();
        $data = array();
        $m_config =  load_auto_cache("m_config");//初始化手机端配置
        if(!$GLOBALS['user_info']){
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        
        if ($m_config['svideo_must_authentication']) {
            $sql = "select is_authentication from ".DB_PREFIX."user where id = ".$user_id;
            $is_authentication = $GLOBALS['db']->getOne($sql);
            if ($is_authentication != 2) {
                api_ajax_return(array("error"=>"请认证后再发布小视频 ", "status"=>0));
            }
        }

        $type_array = array(
            'imagetext','video','weixin','goods','red_photo','photo'
        );
        $type = 'video';
        if(!in_array($type,$type_array)){
            $root = array(
                'status'=>0,
                'error'=>$type.'上传类型错误'
            );
            api_ajax_return($root);
        }
        if($type=='photo'||$type=='goods'||$type=='weixin'){
            if(floatval($_REQUEST['price'])<=0){
                $root = array(
                    'status'=>0,
                    'error'=>'价格不能为空'
                );
                api_ajax_return($root);
            }
        }

        if($type == 'weixin'){
            $data['weixin_account'] =$_REQUEST['data'];
            $data['weixin_price'] =floatval($_REQUEST['price']);
            $data['weixin_account_time'] = to_date( get_gmtime());
            $GLOBALS['db']->autoExecute(DB_PREFIX."user", $data,'UPDATE','id = '.$user_id);
        }else{
            $data['content'] = strim($_REQUEST['content']);
            if(empty($data['content'])){
                $root = array(
                    'status'=>0,
                    'error'=>'内容不能为空'
                );
                api_ajax_return($root);
            }

            if($type=='photo'||$type=='video'){
                if(empty($_REQUEST['photo_image'])){
                    $root = array(
                        'status'=>0,
                        'error'=>'封面不能为空'
                    );
                    api_ajax_return($root);
                }else{
                    $data['photo_image'] = strim($_REQUEST['photo_image']);
                    $data['first_image'] = strim($_REQUEST['first_image']);
                    if ($data['first_image'] == '') {
						$data['first_image'] = strim($_REQUEST['photo_image']);
					}
					$data['comment_restrict'] = intval($_REQUEST['comment_restrict']);
                }
            }


            if($type=='video'){
               // $data['photo_image'] = strim($_REQUEST['image_url']);
               $video_url = strim($_REQUEST['video_url']);
                if(!$video_url){
                    $root = array(
                        'status'=>0,
                        'error'=>'视频不能为空'
                    );
                    api_ajax_return($root);
                }
                $data['data'] = $video_url;

            }else{

                $_REQUEST['data'] = json_decode($_REQUEST['data'],TRUE);

                if(is_array($_REQUEST['data'])){
                    $image_array = $_REQUEST['data'];
                    if($type=='goods'&&$image_array[0]['url']){
                        $data['photo_image'] = $image_array[0]['url'];
                    }
                    if($type=='red_photo'){

                        $_REQUEST['price'] = floatval(count( $image_array))*floatval($m_config['weibo_red_price']);

                        if(floatval($_REQUEST['price'])<=0){
                            $root = array(
                                'status'=>0,
                                'error'=>'红包价格不能小于0'
                            );
                            api_ajax_return($root);
                        }
                    }
                    $data['data'] = serialize($image_array);


                }else{
                    $root = array(
                        'status'=>0,
                        'error'=>'图片结构错误'
                    );
                    api_ajax_return($root);
                }
            }
            $data['type'] = strim($type);
            //小视频默认为无效，后台审核有效后前端才会显示
            $data['status'] = 0;
            $data['price'] = floatval($_REQUEST['price']);

            $data['xpoint'] = strim($_REQUEST['xpoint']);
            $data['ypoint'] = strim($_REQUEST['ypoint']);
            $data['title'] = serialize(json_decode($_REQUEST['title']));
            $data['video_width'] = strim($_REQUEST['video_width']);
            $data['video_height'] = strim($_REQUEST['video_height']);
            $data['create_time'] = to_date(NOW_TIME);
            $data['user_id'] = $user_id;
            $data['is_audit'] = 1;
            //$ipinfo = get_ip_info();
           // $province = $ipinfo['province'];
            // $city = $ipinfo['city'];
            $data['province'] = strim($_REQUEST['province']);
            $address =array();
            $address['province'] = strim($_REQUEST['province']);
            $address['city'] = strim($_REQUEST['city']);
            $data['city'] = strim($_REQUEST['city']);
            $data['address'] = strim($_REQUEST['address']);
            if($data['city'] =='不显示'||$data['address'] =='不显示'){
                $data['city'] = '';
                $data['address'] = '';
                $data['province'] = '';
            }

            //3.1 小视频-上传新增参数
			$data['audio_id'] = intval($_REQUEST['audio_id']); //小视频使用的音乐id
			$data['goods_id'] = intval($_REQUEST['goods_id']); //商品id
            $data['is_podcast_goods'] = intval($_REQUEST['is_podcast_goods']); //商品id

            $GLOBALS['db']->autoExecute(DB_PREFIX."weibo", $data,'INSERT');
            if($address['province']!='' && $address['city'] != ''){
                $weibo_address_count = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."weibo_address where province = '".$address['province']."' and city = '".$address['city']."' ");
                if($weibo_address_count==0){
                    $address['create_time'] = NOW_TIME;
                    $GLOBALS['db']->autoExecute(DB_PREFIX."weibo_address", $address,'INSERT');
                }
            }
        }
        if($GLOBALS['db']->affected_rows()){
            $root = array(
                'status'=>1,
                'error'=>'发布成功'
            );
            $weibo_count = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."weibo where user_id = ".$user_id);
             $re = $GLOBALS['db']->query("update ".DB_PREFIX."user set weibo_count = ".intval($weibo_count)." where id = ".$user_id);
         }else{
            $root = array(
                'status'=>0,
                'error'=>'上传失败'
            );
        }
        api_ajax_return($root);
    }

    //@好友 我的好友-我关注的人
	public function user_follow(){
		$root = array();
		$root['status'] = 1;
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
		}else{

			$user_id = intval($GLOBALS['user_info']['id']);//id
			$page = intval($_REQUEST['p']);//取第几页数据

			if($page==0){
				$page = 1;
			}

			if (isset($_REQUEST['to_user_id'])){
				$to_user_id = strim($_REQUEST['to_user_id']);//被查看的用户id
			}else{
				$to_user_id = $user_id;//id
			}
			$page_size = 20;
			fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserFollwRedisService.php');
			$user_redis = new UserFollwRedisService($user_id);
			$list = $user_redis->get_follonging_user($to_user_id,$page,$page_size);
			foreach($list as $k=>$v){
				$list[$k]['head_image'] = get_spec_image($v['head_image']);
				$list[$k]['signature'] = htmlspecialchars_decode($list[$k]['signature']);
				$list[$k]['nick_name'] = htmlspecialchars_decode($list[$k]['nick_name']);
				$list[$k]['signature'] = emoji_decode($list[$k]['signature']);
				$list[$k]['nick_name'] = emoji_decode($list[$k]['nick_name']);
			}
			$root['list'] = $list;
			if($page==0){
				$root['has_next'] = 0;
			}else{
				if (count($list) >= $page_size)
					$root['has_next'] = 1;
				else
					$root['has_next'] = 0;
			}
			$root['page'] = $page;
		}
		ajax_return($root);
	}

	//搜索好友
	public function user_follow_search(){
		$root = array();
		$root['status'] = 1;
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
		}else{
			$root['list'] = array();
			$user_id = intval($GLOBALS['user_info']['id']);//当前用户;
			$keyword = strim($_REQUEST['keyword']);//搜索关键字
			if ($keyword != "") {
				fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserFollwRedisService.php');
				$user_redis = new UserFollwRedisService($user_id);
				$list = $user_redis->get_follonging_user($user_id, 1, 10000);
				foreach ($list as $k => $v) {
					$list[$k]['head_image'] = get_spec_image($v['head_image']);
					$list[$k]['signature'] = htmlspecialchars_decode($list[$k]['signature']);
					$list[$k]['nick_name'] = htmlspecialchars_decode($list[$k]['nick_name']);
					$list[$k]['signature'] = emoji_decode($list[$k]['signature']);
					$list[$k]['nick_name'] = emoji_decode($list[$k]['nick_name']);
				}
				$root['list'] = array ();
				//匹配到搜索的关键字，返回用户信息
				foreach ($list as $item) {
					if (strstr($item['nick_name'], $keyword) !== false || strstr($item['user_id'], $keyword) !== false) {
						$root['list'][] = $item;
					} else {
						continue;
					}
				}
			}
		}
		ajax_return($root);
	}


    //微信下架
   public function off_weixin(){
       if(!$GLOBALS['user_info']){
           $root['error'] = "用户未登陆,请先登陆.";
           $root['status'] = 2;
           api_ajax_return($root);
       }
       $user_id = intval($GLOBALS['user_info']['id']);//用户ID
       $data = array('weixin_account'=>'','weixin_price'=>0);
       $where = 'id = '.$user_id;
       $res = $GLOBALS['db']->autoExecute(DB_PREFIX."user",$data,'UPDATE',$where);
       if($res){
           $root = array(
               'status'=>1,
               'error'=>'微信下架成功'
           );
       }else{
           $root = array(
               'status'=>0,
               'error'=>'微信下架失败'
           );
       }
       api_ajax_return($root);
   }
    //获取会员权限
    public function check_type(){
        if(!$GLOBALS['user_info']){
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        $user_info = $GLOBALS['db']->getRow("select is_authentication,weibo_count from ".DB_PREFIX."user where id = ".$GLOBALS['user_info']['id']);
        $root = array(
            'status'=>1,
            'error'=>'',
            'info'=>$user_info
        );
        api_ajax_return($root);
    }

	//生成签名
	public function get_sign(){
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			api_ajax_return($root);
		}
		$m_config = load_auto_cache("m_config");
		$secret_id = $m_config['qcloud_secret_id'];
		$secret_key = $m_config['qcloud_secret_key'];
		if ($secret_id == '' || $secret_key == '') {
			$root = array(
				'status'=>0,
				'error'=>'生成签名失败,云API帐户或云API密钥为空',
				'sign'=>''
			);
			api_ajax_return($root);
		}
		// 确定签名的当前时间和失效时间
		$current = time();
		$expired = $current + 86400;  // 签名有效期：1天
		// 向参数列表填入参数
		$arg_list = array(
			"secretId" => $secret_id,
			"currentTimeStamp" => $current,
			"expireTime" => $expired,
			"random" => rand());
		// 计算签名
		$orignal = http_build_query($arg_list);
		$signature = base64_encode(hash_hmac('SHA1', $orignal, $secret_key, true).$orignal);
		$root = array(
			'status'=>1,
			'error'=>'',
			'sign'=>$signature
		);
		if (!$signature) {
			$root = array(
				'status'=>0,
				'error'=>'生成签名失败',
				'sign'=>''
			);
		} 
		api_ajax_return($root);
	}

	//分享信息
	public function share_info($transfer = 0){
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			api_ajax_return($root);
		}
		$m_config =  load_auto_cache("m_config");//初始化手机端配置
		$app_name = $m_config['program_title'];
		$invite_url = SITE_DOMAIN.'/wap/xr/index.html#/activeIndex?user_id='.$GLOBALS['user_info']['id'];
		$invite_image = $m_config['weibo_distribution_img'];
		$invite_info['title'] = '【推荐】《'.$m_config['program_title'].'》';
		$nick_name = emoji_decode($GLOBALS['user_info']['nick_name']);
		$invite_info['content'] = $nick_name.' 请您加入 '.$app_name."APP,让您毫不费力地发照片挣红包，还有美女帅哥排队等您领！";
		$invite_info['imageUrl'] = empty($invite_image) ? $m_config['app_logo'] : $invite_image;
		$invite_info['clickUrl'] = $invite_url;
		if ($transfer == 1) {
			return $invite_info;
		} else {
			ajax_file_return($invite_info);
		}

	}

	//评论权限列表
	public function comment_restrict_list()
	{
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			api_ajax_return($root);
		}
		$root['error'] = '';
		$root['status'] = 1;
		$root['list'] = [
			['content' => '所有人可评论','comment_restrict' =>1],
			['content' => '仅关注的人可评论','comment_restrict' =>2],
			['content' => '不可评论','comment_restrict' =>3],
			];
		api_ajax_return($root);
	}
}

?>