<?php

class bankcardModule  extends baseModule
{
    public function card_list(){
        $userID = requireLogin();

        $list = getModel('user_bankcard')->getListByUser($userID);
        array_walk($list, [$this, 'cutCardNo']);

        output($list,'card_list');
    }

    public function card_info(){
        $userID = requireLogin();
        $argRules = [
            'card_id' => 'required|int',
        ];
        validArgs($argRules);

        $info = getModel('user_bankcard')->getInfo($userID, $_REQUEST['card_id']);
        if(empty($info)){
            outputError('找不到查找的银行卡');
        }
        $this->cutCardNo($info);
        $this->cutName($info);

        output($info, 'card_info');
    }

    public function add_card(){
        $userID = requireLogin();
        $argRules = [
            'name' => 'required|max:30',
            'bank_code' => 'required|int',
            'card_no' => 'required|bankcard',
            'province_code' => 'required|int',
            'city_code' => 'required|int',
            'branch' => 'required|max:30',
            'is_default' => 'required|bool',
            'pin' => 'required|int|length:4',
        ];

        $cardInfo = validArgs($argRules, true)['filtered'];

        $this->validPin($userID, $cardInfo['pin']);
        unset($cardInfo['pin']);

        $validResult = $this->validCard($cardInfo);
        if(!$validResult['status']){
            outputError($validResult['msg']);
        };

        $cardInfo['user_id'] = $userID;

        // 限制上限5张
        $result = getModel('user_bankcard')->addCard($cardInfo, 5);

        if($result === false){
            outputError('绑定银行卡失败，若持续出现此问题，请洽客服');
        }

        outputDone($result, 'card_id');
    }

    public function edit_card(){
        $userID = requireLogin();
        $argRules = [
            'card_id' => 'required|int',
            'name' => 'max:30',
            'bank_code' => 'int',
            'card_no' => 'bankcard',
            'province_code' => 'int',
            'city_code' => 'int',
            'branch' => 'max:30',
            'is_default' => 'bool',
            'pin' => 'required|int|length:4',
        ];

        $newInfo = validArgs($argRules, true)['filtered'];

        $this->validPin($userID, $newInfo['pin']);
        unset($newInfo['pin']);

        $cardID = $newInfo['card_id'];
        unset($newInfo['card_id']);

        $model = getModel('user_bankcard');
        $oldInfo = $model->getInfo($cardID);
        if(empty($oldInfo)){
            outputError('<card_id>找不到该银行卡');
        }
        $cardInfo = $newInfo + $oldInfo;

        $validResult = $this->validCard($cardInfo);
        if(!$validResult['status']){
            outputError($validResult['msg']);
        };

        $cardInfo['user_id'] = $userID;

        $result = getModel('user_bankcard')->editCard($cardID, $cardInfo);

        if($result === false){
            outputError('更新银行卡失败，若持续出现此问题，请洽客服');
        }

        outputDone();
    }

    public function set_def_card(){
        $userID = requireLogin();
        $argRules = [
            'card_id' => 'required|int',
        ];

        validArgs($argRules);

        $cardID = $_REQUEST['card_id'];
        $result = getModel('user_bankcard')->setDefCard($userID,$cardID);

        if(empty($result)){
            outputError('默认银行卡设置失败，若持续出现此问题，请洽客服');
        }

        outputDone();
    }

    public function del_card(){
        $userID = requireLogin();
        $argRules = [
            'card_id' => 'required|int',
            'pin' => 'required|int|length:4',
        ];

        validArgs($argRules);

        $this->validPin($userID, $_REQUEST['pin']);

        $cardID = $_REQUEST['card_id'];
        $result = getModel('user_bankcard')->delCard($userID, $cardID);

        if(empty($result)){
            outputError('删除银行卡失败，若持续出现此问题，请洽客服');
        }

        outputDone();
    }

    public function valid_card_no(){
        $argRules = [
            'card_no' => 'required|bankcard',
        ];

        $result = validArgs($argRules);

        api_ajax_return($result);
    }

    private function validCardNo($cardNo){
        return validVal($cardNo, 'bankcard', 'card_no');
    }

    private function validCard($cardInfo){
        $passed = strlen($cardInfo['pin']) === 4;

        return [
            'status' => $passed,
            'msg' => $passed ? '' : '银行卡验证失败，请确认资料是否正确',
        ];
    }

    private function cutCardNo(&$cardInfo){
        $cut = substr($cardInfo['card_no'], -4);
        unset($cardInfo['card_no']);
        $cardInfo['card_no_cut'] = $cut;
    }

    private function cutName(&$cardInfo){
        $cut = substr_cut($cardInfo['name']);
        unset($cardInfo['name']);
        $cardInfo['name_cut'] = $cut;
    }

    private function validPin($userID, $pin){
        $userModel = getModel('user');
        if(!$userModel->validPin($userID, $pin)){
            outputError('交易密码错误！');
        }
        return true;
    }
}

?>
