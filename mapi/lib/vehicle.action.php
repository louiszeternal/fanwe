<?php
// +----------------------------------------------------------------------
// | XX 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// | Author:
// +----------------------------------------------------------------------

clASs vehicleModule  extends bASeModule
{
	//商城座驾列表
	public function vehicle_list()
	{
		$root = array();
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;
			ajax_return($root);
		}
		$root['status'] = 1;
		$root['list'] = load_auto_cache("vehicle_list");
		ajax_return($root);
	}

	//座驾详情
	public function vehicle_show()
	{
		$root = array();
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;
			ajax_return($root);
		}
		$vehicle_id = intval($_REQUEST['vehicle_id']);
		$root = $GLOBALS['db']->getRow("select id AS vehicle_id,name,diamonds AS money,icon,open_time from ".DB_PREFIX."vehicle where is_effect = 1 AND id = {$vehicle_id}");
		if ($root == '') {
			$root['status'] = 0;
			$root['error'] = '座驾不存在';
			ajax_return($root);
		}
		$open_time = explode(',',$root['open_time']);
		foreach ($open_time AS $k => $v) {
			$arr[] = ['time' => $v.'个月', 'money' => $v * $root['money']];
		}
		$root['open_time'] = $arr;
		$root['status'] = 1;
		ajax_return($root);
	}

	//购买座驾
	public function pay()
	{
		$root = array();
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;
			ajax_return($root);
		}
		$vehicle_id = intval($_REQUEST['vehicle_id']); //礼物ID
		$money = strim($_REQUEST['money']); //购买金额
		$user_id = intval($GLOBALS['user_info']['id']);//用户ID
		$user_diamonds = $GLOBALS['db']->getOne("SELECT diamonds FROM " . DB_PREFIX . "user WHERE id = {$user_id}");
		if ($money == '') {
			$root['status'] = 0;
			$root['error'] = "购买金额不能为空";
			ajax_return($root);
		}
		if ($money > $user_diamonds) {
			$root['status'] = 2;
			$root['error'] = "余额不足,请先充值";
			ajax_return($root);
		}
		$pInTrans = $GLOBALS['db']->StartTrans();
		$res = $GLOBALS['db']->query("UPDATE " . DB_PREFIX . "user SET diamonds = diamonds - $money WHERE id = {$user_id} AND diamonds >= $money");
		try {
			if ($GLOBALS['db']->affected_rows() && $res === true) {
				$vehicle = $GLOBALS['db']->getRow("SELECT diamonds,name FROM " . DB_PREFIX . "vehicle WHERE id = {$vehicle_id}");
				if ($vehicle == '') {
					throw new Exception("座驾不存在");
				}

//				$time = $money / $vehicle['diamonds'] * 2592000; // 购买时间（月）
				$month = $money / $vehicle['diamonds']; // 购买时间（月）
				$time = date("Y-m-d H:i:s",strtotime("+{$month} month"));
				$str_time = strtotime($time);
				$user_vehicle = $GLOBALS['db']->getRow("SELECT id,expire_time FROM ".DB_PREFIX."pay_vehicle_log  WHERE vehicle_id = {$vehicle_id} AND user_id = {$user_id} AND expire_time > ".time()." ");

				//用户已购买且未过期，叠加时间
				if ($user_vehicle['id'] > 0) {
					$msg = '续费座驾' . $vehicle['name'] . '成功,消耗钻石' . $money . ',到期时间为' . (date('Y-m-d H:i:s',$user_vehicle['expire_time'] + $str_time-time()));
					$res = $GLOBALS['db']->query("UPDATE " . DB_PREFIX . "pay_vehicle_log SET expire_time = expire_time + ".($str_time-time()).",msg = '".$msg."',money = money + {$money},time = time + {$month} WHERE user_id = {$user_id} AND vehicle_id = {$vehicle_id} AND expire_time > ".time()."");
				} else {
					//写入座驾购买日志
					$msg = '购买座驾' . $vehicle['name'] . '成功,消耗钻石' . $money . ',到期时间为' . $time;
					$table = DB_PREFIX . 'pay_vehicle_log';
					$vehicle_log['user_id'] = $user_id;
					$vehicle_log['vehicle_id'] = $vehicle_id;
					$vehicle_log['create_time'] = time();
					$vehicle_log['expire_time'] = $str_time;
					$vehicle_log['money'] = $money;
					$vehicle_log['time'] = $month;
					$vehicle_log['msg'] = $msg;
					$res = $GLOBALS['db']->autoExecute($table, $vehicle_log, 'INSERT');
				}
				if ($res === true) {
						//插入用户日志
						$data = array ();
						$data['diamonds'] = $money;
						$param['type'] = 14;
						account_log_com($data, $user_id, $msg, $param);
				} else {
					throw new Exception('购买失败,请稍后再试');
				}
				$GLOBALS['db']->Commit($pInTrans);
				$root['status'] = 1;
				$root['error'] = "购买成功";
				user_deal_to_reids(array($user_id));
				ajax_return($root);
			} else {
				throw new Exception('余额不足,请先充值');
			}
		} catch (Exception $e) {
			//异常回滚
			$GLOBALS['db']->Rollback($pInTrans);
			$root['status'] = 0;
			$root['error'] = $e->getMessage();
			ajax_return($root);
		}
	}

	//用户座驾列表
	public function user_vehicle()
	{
		$root = array();
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;
			ajax_return($root);
		}
		$user_id = $GLOBALS['user_info']['id'];
		//用户坐骑信息
		$vehicle = $GLOBALS['db']->getAll("SELECT p.vehicle_id,v.name,v.icon,p.expire_time FROM ".DB_PREFIX."vehicle AS v LEFT JOIN ".DB_PREFIX."pay_vehicle_log AS p ON v.id = p.vehicle_id WHERE p.user_id = $user_id AND p.expire_time > ".time()." ");
		$vehicle_id = $GLOBALS['db']->getOne("SELECT vehicle_id FROM " . DB_PREFIX . "user WHERE id = {$user_id}");
		foreach ($vehicle AS $k => $v) {
			$vehicle[$k]['expire_time'] = date('Y-m-d',$vehicle[$k]['expire_time']);
			$vehicle[$k]['current_vehicle'] = 0;
			if ($vehicle[$k]['vehicle_id'] == $vehicle_id) {
				$vehicle[$k]['current_vehicle'] = 1;
			}
		}
		$root['list'] = $vehicle;
		$root['status'] = 1;
		$root['error'] = "";
		ajax_return($root);
	}

	//用户座驾列表
	public function user_vehicle22()
	{
		$root = array();
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;
			ajax_return($root);
		}
		$user_id = $GLOBALS['user_info']['id'];
		//用户坐骑信息
		$vehicle = $GLOBALS['db']->getAll("SELECT p.vehicle_id,v.name,v.icon,p.expire_time FROM ".DB_PREFIX."vehicle AS v LEFT JOIN ".DB_PREFIX."pay_vehicle_log AS p ON v.id = p.vehicle_id WHERE p.user_id = $user_id AND p.expire_time > ".time()." ");
		$vehicle_id = $GLOBALS['db']->getOne("SELECT vehicle_id FROM " . DB_PREFIX . "user WHERE id = {$user_id}");
		foreach ($vehicle AS $k => $v) {
			$vehicle[$k]['expire_time'] = date('Y-m-d H:i:s',$vehicle[$k]['expire_time']);
			$vehicle[$k]['current_vehicle'] = 0;
			if ($vehicle[$k]['vehicle_id'] == $vehicle_id) {
				$vehicle[$k]['current_vehicle'] = 1;
			}
		}
		$root['list'] = $vehicle;
		$root['status'] = 1;
		$root['error'] = "";
		ajax_return($root);
	}

	//用户装备座驾
	public function employ_vehicle()
	{
		$root = array();
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;
			ajax_return($root);
		}
		$vehicle_id = intval($_REQUEST['vehicle_id']);
		$user_id = intval($GLOBALS['user_info']['id']);

		$user_vehicle = $GLOBALS['db']->getOne("SELECT id FROM " . DB_PREFIX . "user WHERE id = {$user_id} AND vehicle_id = {$vehicle_id}");
		if ($user_vehicle > 0) {
			$GLOBALS['db']->query("UPDATE " . DB_PREFIX . "user SET vehicle_id = 0 WHERE id = {$user_id}");
			if($GLOBALS['db']->affected_rows()){
				$root['status'] = 0;
				$root['error'] = "已取消使用";
				ajax_return($root);
			}
			$root['status'] = 0;
			$root['error'] = "该座驾正在使用中";
			ajax_return($root);
		}

		$vehicle = $GLOBALS['db']->getOne("SELECT id FROM " . DB_PREFIX . "vehicle WHERE id = {$vehicle_id}");
		if ($vehicle == '') {
			$root['status'] = 0;
			$root['error'] = "该座驾不存在";
			ajax_return($root);
		}

		$vehicle_log = $GLOBALS['db']->getOne("SELECT id FROM ".DB_PREFIX."pay_vehicle_log  WHERE vehicle_id = {$vehicle_id} AND expire_time > ".time()." ");
		if ($vehicle_log['id'] == '') {
			$root['status'] = 0;
			$root['error'] = "该座驾未购买或已过期";
			ajax_return($root);
		}

		$sql = "UPDATE ".DB_PREFIX."user SET vehicle_id = {$vehicle_id} WHERE id = {$user_id}";
		$res = $GLOBALS['db']->query($sql);
		if ($GLOBALS['db']->affected_rows() && $res === true) {
			$root['status'] = 1;
			$root['error'] = "使用成功";
			ajax_return($root);
		} else {
			$root['status'] = 1;
			$root['error'] = "使用失败,请稍后再试";
			ajax_return($root);
		}
	}

	//座驾在直播间的显示次数控制
	public function user_vehicle_show()
	{
		$root = array();
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;
			ajax_return($root);
		}
		$video_id = intval($_REQUEST['room_id']);
		$user_id = intval($GLOBALS['user_info']['id']);//用户ID
		//查询出用户当前使用的座驾以及座驾限制的次数
		$vehicle_id = $GLOBALS['db']->getOne("SELECT vehicle_id FROM " . DB_PREFIX . "user WHERE id = {$user_id}");
		if ($vehicle_id == '') {
			$root['status'] = 0;
			$root['error'] = '未装备座驾';
			ajax_return($root);
		}
		$vehicle = $GLOBALS['db']->getOne("SELECT v.show_num FROM ".DB_PREFIX."user AS u LEFT JOIN ".DB_PREFIX."vehicle AS v ON u.vehicle_id = v.id LEFT JOIN ".DB_PREFIX."pay_vehicle_log AS p ON u.vehicle_id = p.vehicle_id WHERE v.id = {$vehicle_id} AND p.expire_time >  ".time()." ");
		if (is_null($vehicle)) {
			$root['status'] = 0;
			$root['error'] = '座驾已过期';
			ajax_return($root);
		}
		if ($vehicle != 0) {
			fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VehicleRedisService.php');
			$redis = new VehicleRedisService();
			$res = $redis->vehicleNum($user_id,$video_id,$vehicle);
			if ($res === false) {
				$root['status'] = 0;
				$root['error'] = '次数上限';
				ajax_return($root);
			}
		}

		$root['status'] = 1;
		$root['error'] = '';
		ajax_return($root);

	}

	public function test()
	{
		$prop = load_auto_cache("vehicle_id",array('id'=>$_REQUEST['id']));
//		$prop = load_auto_cache("prop_id",array('id'=>$_REQUEST['id']));
		var_dump(json_encode($prop));
	}
}

?>