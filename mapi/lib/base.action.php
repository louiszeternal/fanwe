<?php
// +----------------------------------------------------------------------
// | XX 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// | Author:
// +----------------------------------------------------------------------

class baseModule
{
	public function __construct()
	{
//		register_shutdown_function(function(){
//			if(isset($GLOBALS['redisdb'])){
//				$redis = $GLOBALS['redisdb'];
//				$redis->close();
//			}
//		});
	}

	public function __destruct()
	{
//		if(isset($GLOBALS['redisdb'])){
//			$redis = $GLOBALS['redisdb'];
//			$redis->close();
//		}
		register_shutdown_function(function(){
			if(isset($GLOBALS['redisdb'])){
				$redis = $GLOBALS['redisdb'];
				$redis->close();
			}
		});
	}

	/**
	 * 支付宝提现绑定接口
	 */
	public function base_binding_alipay()
	{
		$root = array();
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
		}else{
			$user_id = $GLOBALS['user_info']['id'];
			$alipay_name = trim($_REQUEST['alipay_name']) != '' ? trim($_REQUEST['alipay_name']) : trim($_REQUEST['azf_name']);
			$alipay_account = trim($_REQUEST['alipay_account']) != '' ? trim($_REQUEST['alipay_account']) : trim($_REQUEST['azf_account']);

			if ($alipay_name != '' && $alipay_account != ''){
				$alipay = array();
				$alipay['alipay_name'] = strim($alipay_name);
				$alipay['alipay_account'] = strim($alipay_account);
				$alipay['binding_alipay'] = 1;
				$where = "id=".$user_id;
				$result = $GLOBALS['db']->autoExecute(DB_PREFIX."user",$alipay,"UPDATE",$where);

				if (!$result){
					$root['error'] = "绑定失败";
					$root['status'] = 0;
				}else{
					fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserRedisService.php');
					$user_redis = new UserRedisService();
					$user_ticket_info = $user_redis->update_db($user_id,$alipay);

					$root['error'] = '绑定成功';
					$root['status'] = 1;
				}
			}else{
				$root['error'] = "支付宝账户与名称不能为空";
				$root['status'] = 0;
			}
		}
		ajax_return($root);
	}

	//支付宝提现界面初始化接口
	public function base_money_carry_alipay()
	{
		$root = array('status'=>1,'error'=>'初始化成功');
		if(!$GLOBALS['user_info']){
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
		}else{
			$m_config =  load_auto_cache("m_config");//初始化手机端配置
			$user_id = intval($GLOBALS['user_info']['id']);
			fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/UserRedisService.php');
			$user_redis = new UserRedisService();
			$ticket = $user_redis->getRow_db($user_id,array('ticket','refund_ticket'));
			//提现比例 如果主播提现比例为空，则使用后台通用比例
			$root['ratio'] = $GLOBALS['db']->getOne("select alone_ticket_ratio from ".DB_PREFIX."user where id=".$user_id);
			if($root['ratio']==''){
				$root['ratio'] = $m_config['ticket_catty_ratio'];
			}
			//公会长提现比例特殊
			if (defined('OPEN_SOCIETY_MODULE') && OPEN_SOCIETY_MODULE && $m_config['society_pattern']==2){
				$society_info = $GLOBALS['db']->getRow("select society_id,society_chieftain from ".DB_PREFIX."user where id=".$user_id);
				if (intval($society_info['society_chieftain'])){
					$refund_rate = $GLOBALS['db']->getOne("select refund_rate from ".DB_PREFIX."society where id=".$society_info['society_id']);
					$root['ratio'] = floatval($refund_rate);
					if ($root['ratio'] > 1 || $root['ratio'] <= 0){
						$root['ratio'] = $m_config['society_public_rate'];
					}
				}
			}
			$root['can_use_ticket'] =intval($ticket['ticket']-$ticket['refund_ticket']);//可提现印票

			if (floatval($m_config['ticket_catty_ratio']) > 0){
				//每日可提现印票
				$root['day_ticket_max'] = intval(intval($m_config['day_cash_max'])/floatval($root['ratio']));
				//最小提现印票
				$root['ticket_catty_min'] = intval(intval($m_config['ticket_catty_min'])/floatval($root['ratio']));
			}

			//计算已提现印票
			$user_refund = $GLOBALS['db']->getRow("SELECT SUM(ticket) AS refund_sum FROM ".DB_PREFIX.
				"user_refund WHERE user_id=".$user_id." AND is_pay=3");
			$root['ready_refund_ticket'] = intval($user_refund['refund_sum']);
		}
		ajax_return($root);
	}

	//支付宝提现接口
	public function base_submit_refund_alipay()
	{
		$root = array('status' => 1, 'error' => '成功');
		if (!$GLOBALS['user_info']) {
			$root['error'] = "用户未登陆,请先登陆.";
			$root['status'] = 0;
			$root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
		} else {
			$m_config = load_auto_cache("m_config");//初始化手机端配置
			$user_id = $GLOBALS['user_info']['id'];//会员ID

			//判断是否在直播中，如是则不让进入提现界面
			$is_live =  $GLOBALS['db']->getRow("select user_id,live_in from ".DB_PREFIX."video where  user_id=".$user_id." order by id desc");
			//异常退出后，等待公会长抽成完成后才能提现ljz
			if (!empty($is_live) && $is_live['live_in']==1)
			{
				$root['status'] = 0;
				$root['error'] = '由于您的视频还在直播，暂时无法提现，请稍后再试';
				ajax_return($root);
			}

			$rate = $GLOBALS['db']->getOne("select alone_ticket_ratio from ".DB_PREFIX."user where id=".intval($user_id));
			if($rate==''){
				$rate = $m_config['ticket_catty_ratio'];
			}
			$ticket = intval($_REQUEST['ticket']);//提现印票
			$memo = "支付宝提现";
			$money = floatval($ticket * $rate);
			//未处理提现
			$ready_refund_id = intval($GLOBALS['db']->getOne("select id from " . DB_PREFIX . "user_refund where user_id = " . intval($user_id) . " and (is_pay =0 or is_pay=1)"));
			if ($ready_refund_id) {
				$root['error'] = '您还有未处理的提现！';
				$root['status'] = 0;
				ajax_return($root);
			}
			//会员当前印票
			$user_ticket_info = $GLOBALS['db']->getRow("select ticket,refund_ticket from ".DB_PREFIX."user where id = '".intval($user_id)."'");
			$user_ticket = $user_ticket_info['ticket'] - $user_ticket_info['refund_ticket'];//可使用的印票
			//超额判断
			if ($ticket > $user_ticket) {
				$root['error'] = '提现超出限制!不能大于可用提现';
				$root['status'] = 0;
				ajax_return($root);
			}
			// 进入每月提现一次流程
			$month_carry_one = intval($m_config['month_carry_one']) ? 1 : 0;//提现配置，1：每月提现1次，0：无限制
			if ($month_carry_one) {
				$ready_refund_info = $GLOBALS['db']->getRow("select pay_time from " . DB_PREFIX . "user_refund where user_id = " . intval($user_id) . " and is_pay = 3");
				$pay_time = $ready_refund_info['pay_time'];
				//本月是否有提现
				if ($pay_time != '' && (to_date($pay_time, "Ym") == to_date(get_gmtime(), 'Ym'))) {
					$root['error'] = '每月只能提现一次，本月已经提现过了';
					$root['status'] = 0;
					ajax_return($root);
				} else {
					//查看本月允许提现时间，精确计算到天
					if ((to_date(get_gmtime(), 'd') > intval($m_config['month_carry_max']) || to_date(get_gmtime(), 'd') < intval($m_config['month_carry_min']))&&intval($m_config['month_carry_max'])!=0&&intval($m_config['month_carry_min'])!=0){
						$root['error'] = '每月' . intval($m_config['month_carry_min']) . '日到' . intval($m_config['month_carry_max']) . '日才能提现';
						$root['status'] = 0;
						ajax_return($root);
					}
				}
			}
			//百媚模式下代理会员提现
			if(defined('OPEN_BM') && OPEN_BM){
				$bm_config=load_auto_cache("bm_config");
				$bm_pid=intval($bm_config['bm_pid']);
				$bm_user = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."user where id=".intval($user_id)." and( bm_pid=".$bm_pid." or bm_pid=0)");
				$bm_promoter = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."bm_promoter where user_id=".intval($user_id));
				if($bm_user==''||$bm_promoter){
					$root['error'] =$m_config['bm_point'];
					$root['status'] = 0;
					ajax_return($root);
				}

			}


			//如果开启公会并且开启无抽成模式，支付宝 将无法提现ljz
			if(defined('OPEN_SOCIETY_MODULE') && OPEN_SOCIETY_MODULE == 1 && $m_config['society_pattern'] == 2){
				$society_info = $GLOBALS['db']->getRow("select society_id,society_chieftain from ".DB_PREFIX."user where id=".$user_id);
				if(intval($society_info['society_chieftain']) == 1){
					$root['error'] ='会长提现，请联系客服!';
					$root['status'] = 0;
					ajax_return($root);
				}elseif(intval($society_info['society_id'])) {
					$root['error'] ='公会成员不允许提现!';
					$root['status'] = 0;
					ajax_return($root);
				}
			}

			//提现最小值
			$ticket_catty_min = $m_config['ticket_catty_min'] > 1 ? $m_config['ticket_catty_min'] : 1;
			// 提现比例
			$ticket_catty_ratio = floatval($m_config['ticket_catty_ratio']);
			//最小判断
			if ($ticket * $ticket_catty_ratio < $ticket_catty_min) {
				$root['error'] = '支付宝提现单笔不能少于' . $ticket_catty_min . '元('.bcdiv($ticket_catty_min,$ticket_catty_ratio,2).'印票)';
				$root['status'] = 0;
				ajax_return($root);
			}

			//取用户当日提现金额之和
			//create_time储存的是格林威治时间。使用时需转换为北京时间
			$refunded_money_sql = "select sum(money) from ".DB_PREFIX."user_refund where user_id=$user_id and (is_pay=0 or is_pay=1 or is_pay=3) 
			                        and DATE_FORMAT(FROM_UNIXTIME(create_time+28800),'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d')";
			$refunded_money = $GLOBALS['db']->getOne($refunded_money_sql);
			$total_money = floatval($refunded_money) + $money;
			//提现最大值
			$day_cash_max = intval($m_config['day_cash_max']);
			//总和判断
			if($total_money > $day_cash_max){
				$root['error'] = '支付宝每日提现总额不能多于' . $day_cash_max . '元';
				$root['status'] = 0;
				ajax_return($root);
			}

			$refund_data['money'] = $money;
			$refund_data['user_bank_id'] = -1;
			$refund_data['ticket'] = $ticket;
			$refund_data['user_id'] = $user_id;
			$refund_data['create_time'] = NOW_TIME;
			$refund_data['memo'] = $memo;
			$refund_data['withdrawals_type'] = 1;
			$GLOBALS['db']->autoExecute(DB_PREFIX . "user_refund", $refund_data);
		}
		ajax_return($root);
	}
}