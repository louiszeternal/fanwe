<?php

class dataModule  extends baseModule
{
    public function bank_list(){
        ajax_return(
            [
                'bank_list' => $GLOBALS['db']->getAll('SELECT * FROM t_bank'),
            ]
        );
    }

    public function province_list(){
        ajax_return(
            [
                'province_list' => $GLOBALS['db']->getAll('SELECT * FROM t_district_province'),
            ]
        );
    }

    public function city_list(){
        $argRules = [
            'province_code' => 'int'
        ];
        validArgs($argRules);

        $cityListSQL = 'SELECT * FROM t_district_city';

        if(!empty($_REQUEST['province_code'])){
            $cityListSQL .= " WHERE parent_code = {$_REQUEST['province_code']}";
        }

        $cityList = $GLOBALS['db']->getAll($cityListSQL);

        ajax_return(
            [
                'city_list' => $cityList,
            ]
        );
    }

    public function banner_list(){
        $argRules = [
            'position' => 'int'
        ];
        validArgs($argRules);

        $listArg = null;

        if(!blank($_REQUEST['position'])){
            $listArg = [
                'position' => $_REQUEST['position']
            ];
        }

        $list = load_auto_cache("banner_list", $listArg);

        ajax_return(
            [
                'banner_list' => $list,
            ]
        );
    }
}

?>
