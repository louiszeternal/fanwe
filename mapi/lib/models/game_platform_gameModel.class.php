<?php


class game_platform_gameModel extends NewModel
{
    public function getList(){
        return $this->field('id, platform_id, category_id, screen_name, icon')
                    ->order('sort')
                    ->select(['status' => 1]);
    }

    public function getInfo($id){
        return $this->selectOne(['id' => intval($id)]);
    }

    public function getPromoted($limit = null){
        $query = empty($limit) ? $this : $this->limit($limit);
        return $query->field('id, platform_id, screen_name, icon')
                    ->select(['status' => 1, 'promoted' => 1]);
    }

    public function getCategoryList(){
        return $GLOBALS['db']->getAll('
            SELECT id, name FROM t_game_platform_category ORDER BY sort, id
        ');
    }

//    public function getInfoByName($name){
//        return $this->selectOne(['name' => $name]);
//    }
}
