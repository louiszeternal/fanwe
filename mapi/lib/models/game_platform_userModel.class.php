<?php


class game_platform_userModel extends NewModel
{
    public function getInfo($userID, $platformID){
        return $this->selectOne(['user_id'=>$userID, 'platform_id' => $platformID]);
    }

    public function initUserRecord($platformID, $platformUserID = null){
        $userID = requireLogin();
        $platformUserID === null ? $userID : $platformUserID;

        Connect::beginTransaction();

        $isInited = $this->checkInit($userID, $platformID);

        if($isInited){
            Connect::commit();
            return;
        }

        $this->insert([
            'platform_id' => $platformID,
            'user_id' => $userID,
            'platform_user_id' => $platformUserID === null ? $userID : $platformUserID
        ]);

        Connect::commit();
    }

    public function checkInit($userID, $platformID){
        $checkSQL = "
            SELECT 1
            FROM t_game_platform_user
            WHERE
                user_id = '$userID'
                AND platform_id = '$platformID'
        ";

        return Connect::exec($checkSQL);
    }

    public function getCreditBalance($userID, $platformID){
        if(!$this->checkInit($userID, $platformID)){
            return false;
        }
        return $this->field('credit')->selectOne([
            'user_id' => $userID,
            'platform_id' => $platformID
        ])['credit'];
    }

    public function getAssocUserPlatform($userID){
        $SQL = "
            SELECT platform.id, name, screen_name, icon, platform_user_id
            FROM t_game_platform AS platform
            LEFT JOIN t_game_platform_user AS userRecord
            ON
                platform_id = platform.id
                AND user_id = '$userID'
            WHERE
                platform.status = 1
            ORDER BY platform_id DESC, sort, platform.id DESC
        ";

        return Connect::query($SQL);
    }

    public function addCredit($userID, $platformID, $amount){
        Connect::beginTransaction();

        $userDiamondsSQL = "SELECT diamonds FROM t_user WHERE id = '$userID' FOR UPDATE";
        $currentDiamonds = Connect::query($userDiamondsSQL, false);

        if(empty($currentDiamonds)){
            outputError('找不到用户信息');
        }

        if(empty($currentDiamonds['diamonds']) || $currentDiamonds['diamonds'] < $amount){
            outputError('用户余额不足！');
        }

        $consumeSQL = "UPDATE t_user SET diamonds = diamonds - '$amount' WHERE id = '$userID'";

        Connect::exec($consumeSQL);

        $addCreditSQL = "
            UPDATE t_game_platform_user
            SET credit = credit + '$amount'
            WHERE
                platform_id = '$platformID'
                AND user_id = '$userID'
        ";

        $result = Connect::exec($addCreditSQL);

        if(!$result){
            Connect::rollback();
            outputError('转入余额失败，用户可能尚未登入过该平台');
        }

        Connect::commit();

        return 1;
    }

    public function withdrawCredit($userID, $platformID, $amount){
        Connect::beginTransaction();

        $creditInfoSQL = "
            SELECT id, credit
            FROM t_game_platform_user
            WHERE
                platform_id = '$platformID'
                AND user_id = '$userID'
            FOR UPDATE
        ";
        $creditInfo = Connect::query($creditInfoSQL, false);

        if(empty($creditInfo)){
            outputError('找不到记录，用户可能尚未登入过该平台');
        }

        if(empty($creditInfo['credit']) || $creditInfo['credit'] < $amount){
            outputError('用户在平台中的余额不足！');
        }

        $consumeCreditSQL = "UPDATE t_game_platform_user SET credit = credit - '$amount' WHERE id = '{$creditInfo['id']}'";
        $result = Connect::exec($consumeCreditSQL);

        if(!$result){
            Connect::rollback();
            outputError('提现操作失败');
        }

        $addDiamondsSQL = "UPDATE t_user SET diamonds = diamonds + '$amount' WHERE id = '$userID'";

        Connect::exec($addDiamondsSQL);

        Connect::commit();

        return 1;
    }

    // consume only
    public function consumeCredit($userID, $platformID, $amount){
        Connect::beginTransaction();

        $creditInfoSQL = "
            SELECT id, credit
            FROM t_game_platform_user
            WHERE
                platform_id = '$platformID'
                AND user_id = '$userID'
            FOR UPDATE
        ";
        $creditInfo = Connect::query($creditInfoSQL, false);

        try {
            if($creditInfo['credit'] < $amount){
                return -1;   // 积分不足的情况
            }
        } catch (Exception $e){
            return 0;
        }

        $consumeCreditSQL = "UPDATE t_game_platform_user SET credit = credit - '$amount' WHERE id = '{$creditInfo['id']}'";
        $result = Connect::exec($consumeCreditSQL);

        Connect::commit();

        return $result;
    }

    // increase only
    public function increaseCredit($userID, $platformID, $amount){
        $sql = "
            UPDATE t_game_platform_user
            SET credit = credit + '$amount'
            WHERE
                platform_id = '$platformID'
                AND user_id = '$userID'
        ";

        return Connect::exec($sql);
    }

    public function setCredit($userID, $platformID, $credit){
        return $this->update(
            [
                'credit' => $credit
            ],
            [
                'platform_id' => $platformID,
                'user_id'     => $userID
            ]
        );
    }
}
