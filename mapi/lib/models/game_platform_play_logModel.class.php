<?php


class game_platform_play_logModel extends NewModel
{
    // TODO: game_id to game.id
    public function getStats($userID, $month, $timestamp, $page, $pageSize){
        $logTable = DB_PREFIX . $this->table_name;
        $platformTable = DB_PREFIX . 'game_platform';
        $gameTable = DB_PREFIX . 'game_platform_game';

        $page--;

        $startTime = wholeMonth($month, true)['start'];
        $endTime = wholeMonth($month, true)['end'];
        $now = toDatetime($timestamp);

        $offset = $page * $pageSize;

        $result = [];

        if(!$page){
            $result['summary'] = $this->getStatsSummary($userID, $startTime, $endTime);
            $result['summary']['total_consume'] = empty($result['summary']['total_consume']) ? '0.00' : $result['summary']['total_consume'];
            $result['summary']['total_earn'] = empty($result['summary']['total_earn']) ? '0.00' : $result['summary']['total_earn'];
        }

        $SQL = "
            SELECT
                log.id,
                log.consume_amount,
                log.earn_amount,
                game.screen_name AS game_screen_name,
                platform.screen_name AS platform_screen_name,
                game.icon
            FROM {$logTable} AS log
            JOIN {$platformTable} AS platform
                ON platform.id = log.platform_id
            JOIN {$gameTable} AS game
                ON game.game_id = log.game_id
            WHERE
                `user_id` = {$userID}
                AND `datetime` >= '{$startTime}'
                AND `datetime` <= '{$endTime}'
            ORDER BY `datetime` DESC
            LIMIT {$pageSize} OFFSET {$offset}
        ";

        $result['game_log'] = $GLOBALS['db']->getAll($SQL);

        return $result;
    }

    public function getLog($logID){
        $logTable = DB_PREFIX . $this->table_name;
        $platformTable = DB_PREFIX . 'game_platform';
        $gameTable = DB_PREFIX . 'game_platform_game';

        $SQL = "
            SELECT
                log.consume_amount,
                log.earn_amount,
                game.screen_name AS game_screen_name,
                platform.screen_name AS platform_screen_name,
                game.icon,
                out_order_no,
                datetime
            FROM {$logTable} AS log
            LEFT JOIN {$platformTable} AS platform
                ON platform.id = log.platform_id
            LEFT JOIN {$gameTable} AS game
                ON game.game_id = log.game_id
            WHERE
                log.id = {$logID}
        ";
        return $GLOBALS['db']->getAll($SQL)[0];
    }

    public function getStatsSummary($userID, $startTime, $endTime){
        $logTable = DB_PREFIX . $this->table_name;
        $gameTable = DB_PREFIX . 'game_platform_game';

        $SQL = "
            SELECT
                count(1) AS total_play,
                sum(consume_amount) AS total_consume,
                sum(earn_amount) AS total_earn
            FROM {$logTable} AS log
            JOIN {$gameTable} AS game
                ON game.game_id = log.game_id
            WHERE
                `user_id` = {$userID}
                AND `datetime` >= '{$startTime}'
                AND `datetime` <= '{$endTime}'
        ";

        return $GLOBALS['db']->getAll($SQL)[0];
    }
}
