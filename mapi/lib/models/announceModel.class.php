<?php


class announceModel extends NewModel
{
    public function getList(){
        return $this->field('id, title')
                    ->order('sort,update_time DESC,id DESC') // check out that this method cannot follow a space with comma
                    ->limit(5)
                    ->select(['status' => 1]);
    }

    public function getDetail($id){
        return $this
            ->field('id,title,body,update_time,create_time')
            ->selectOne(array('id' => intval($id)));
    }
}
