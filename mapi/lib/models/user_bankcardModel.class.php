<?php


class user_bankcardModel extends NewModel
{
    public function getInfo($userID, $cardID){
        return $this->selectOne(['id' => intval($cardID), 'user_id' => $userID]);
    }

    public function getListByUser($userID){
        $sql = "
            SELECT card.id, card_no, bank.name AS bank_name, is_default
            FROM t_user_bankcard AS card
            LEFT JOIN t_bank AS bank
            ON bank.code = card.bank_code
            WHERE user_id = '$userID'
            ORDER BY is_default DESC, card.id
        ";

        $result = $GLOBALS['db']->getAll($sql);

        return empty($result) ? [] : $result;
    }

    /**
     * @param array $cardInfo 银行卡信息
     * @param int $limit    可新增卡片上限
     *
     * @return int|bool 新增银行卡记录ID，若为false则为失败
     */
    public function addCard($cardInfo, $limit = 0){
        $userID = $cardInfo['user_id'];
        $existCards = $this->field('card_no')->select(['user_id' => $userID]);
        $existCardsNo = array_column($existCards,'card_no');
        if(!empty($limit)){
            if(count($existCardsNo) >= $limit){
                outputError("绑定银行卡已达上限{$limit}张，不可再添增新卡");
            }
        }
        if(in_array($cardInfo['card_no'],$existCardsNo)){
            outputError("已绑定过此张银行卡，不可重复绑定");
        }

        $isDefault = $cardInfo['is_default'];

        if($isDefault){
            unset($cardInfo['is_default']);
        }

        $cardID = $this->insert($cardInfo);

        if($isDefault){
            self::setDefCard($userID,$cardID);
        }

        return $cardID;
    }

    public function editCard($cardID, $cardInfo){
        $userID = $cardInfo['user_id'];
        $existCards = $this->field('card_no')
                           ->select(
                               [
                                   'id'      => ['!=', $cardID],
                                   'user_id' => $userID,
                               ]
                           );

        if(dataset_search($existCards,'card_no', $cardInfo['card_no']) !== false){
            outputError("已绑定过此张银行卡，不可重复绑定");
        }

        $isDefault = $cardInfo['is_default'];

        if($isDefault){
            unset($cardInfo['is_default']);
        }

        $result = $this->update($cardInfo, ['id' => $cardID]);

        if($isDefault){
            self::setDefCard($userID,$cardID);
        }

        return $result;
    }

    public function delCard($userID, $cardID){
        $existCard = $this->field('id, user_id')
                          ->selectOne(['id' => intval($cardID)]);
        if(empty($existCard)){
            outputError("<card_id>{$cardID}：找不到该卡片");
        }
        if($existCard['user_id'] != $userID){
            outputError("<card_id>{$cardID}：卡片关联错误");
        }

        return $this->delete(['id' => intval($cardID)]);
    }

    public function setDefCard($userID,$cardID){
        $SQL = "
            UPDATE t_user_bankcard
            SET is_default = (CASE WHEN id = '$cardID' THEN 1 ELSE 0 END)
            WHERE user_id = '$userID';
        ";
        return $GLOBALS['db']->query($SQL);
    }
}
