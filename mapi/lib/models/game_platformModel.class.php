<?php


class game_platformModel extends NewModel
{
    public function getList(){
        return $this->select(['status' => 1]);
    }

    public function getInfo($id){
        return $this->selectOne(['id' => intval($id)]);
    }

    public function getInfoByName($name){
        return $this->selectOne(['name' => $name]);
    }
}
