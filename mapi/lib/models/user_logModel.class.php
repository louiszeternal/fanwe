<?php

const USER_LOG_CREDIT_ADD = 0;
const USER_LOG_CREDIT_WITHDRAW = 1;
const USER_LOG_PROP_PRESENT = 2;
const USER_LOG_PAID_ROOM = 6;
const USER_LOG_GAME_CREDIT_ADD = 50;
const USER_LOG_GAME_CREDIT_WITHDRAW = 51;
const USER_LOG_GAME_LOGIN = 52;

class user_logModel extends NewModel
{
    public function log($type, $data = []){
        if(!is_int($type)){
            return false;
        }

        $data['user_id'] = requireLogin();
        $data['type'] = $type;
        return $this->insert($data);
    }

    // TODO: game_id to game.id
    public function getLogTable($userID, $month, $type, $timestamp, $page, $pageSize){
        $logTable = DB_PREFIX . $this->table_name;

        $page--;

        $startTime = wholeMonth($month, true)['start'];
        $endTime = wholeMonth($month, true)['end'];
        $now = toDatetime($timestamp);

        $offset = $page * $pageSize;

        $result = [
            'filter' => []
        ];

        $typeSQL = '';
        if(!empty($type)){
            $typeSQL = "
                AND type = $type
            ";
            $result['filter']['type'] = [$type];
        }

        if(!$page){
            $result['summary'] = $this->getLogSummary($userID, $startTime, $endTime);
            $result['summary']['total_expenditure'] = empty($result['summary']['total_expenditure'])
                ? '0.00'
                : toFloatAmount($result['summary']['total_expenditure']);
            $result['summary']['total_income'] = empty($result['summary']['total_income'])
                ? '0.00'
                : toFloatAmount($result['summary']['total_income']);
        }

        $SQL = "
            SELECT
                log.id,
                log.type,
                item_name,
                log.diamonds AS amount,
                log_time
            FROM {$logTable} AS log
            WHERE
                `user_id` = {$userID}
                AND `log_time` >= '{$startTime}'
                AND `log_time` <= '{$endTime}'
                AND type < 52
                {$typeSQL}
            ORDER BY `log_time` DESC, `id` DESC
            LIMIT {$pageSize} OFFSET {$offset}
        ";

        $result['trans_log'] = $GLOBALS['db']->getAll($SQL);

        return $result;
    }

    public function getLogSummary($userID, $startTime, $endTime){
        $logTable = DB_PREFIX . $this->table_name;

        $SQL = "
            SELECT
                sum(CASE WHEN type = 51 THEN diamonds ELSE 0 END) AS total_income,
                sum(CASE WHEN type = 50 THEN diamonds ELSE 0 END) AS total_expenditure
            FROM {$logTable} AS log
            WHERE
                `user_id` = {$userID}
                AND `log_time` >= '{$startTime}'
                AND `log_time` <= '{$endTime}'
                AND type < 52
        ";

        return $GLOBALS['db']->getAll($SQL)[0];
    }
}
