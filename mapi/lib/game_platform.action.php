<?php

class game_platformModule  extends baseModule
{
    private $module;

    public function game_list()
    {
        $model = getModel('game_platform_game');

        ajax_return([
            'category_list' => $model->getCategoryList(),
            'game_list' => $model->getList(),
            'promoted_games' => $model->getPromoted(),
        ]);
    }

    /**
     * 直接登入游戏
     */
    public function direct_login()
    {
        $userID = requireLogin();

        $argRules = [
            'game_id' => 'required'
        ];
        validArgs($argRules);

        $module = self::getModule();

        $platformInfo = self::getPlatformInfo();

        getModel('game_platform_user')->initUserRecord($platformInfo['id']);


        ajax_return($module->directLogin($userID, $_REQUEST['game_id']));
    }

    /**
     * 取得第三方游戏平台余额
     * @param bool $notRequest
     *
     * @return array
     */
    public function credit_balance($notRequest = false)
    {
        $userID = requireLogin();
        $argRules = [
            'game_id' => 'requiredGroup:1',
            'platform_id' => 'requiredGroup:1'
        ];
        validArgs($argRules);

        $module = self::getModule();

        $platformBalance = toFloatAmount($module->creditBalance($userID));
        $diamonds = getModel('user')->getOneById($userID, 'diamonds')['diamonds'];
        $diamonds = toFloatAmount($diamonds);

        $result = [
            'balance'          => "$diamonds",
            'platform_balance' => "$platformBalance",
            'status' => 1
        ];

        if($notRequest){
            return $result;
        }

        ajax_return($result);
    }

    /**
     * 平台充值
     */
    public function credit_add()
    {
        $this->operateCredit('add');
    }

    /**
     * 平台提现
     */
    public function credit_withdraw()
    {
        $this->operateCredit('withdraw');
    }

    /**
     * 平台余额操作
     * @param $type
     */
    private function operateCredit($type)
    {
        $userID = requireLogin();

        $argRules = [
            'game_id' => 'requiredGroup:1',
            'platform_id' => 'requiredGroup:1',
            'amount' => 'required'
        ];
        validArgs($argRules);

        $amount = $_REQUEST['amount'];

        $amountUnit = $_REQUEST['amount_unit'];

        $module = self::getModule();

        if($amountUnit == null || $amountUnit == AmountUnitType::cent){
            validVal($amount, 'amountInt', 'amount');
        } else {
            validVal($amount, 'amount', 'amount');
            $amount = toIntAmount($amount);
        }

        $methodName = $type . 'Credit';
        $result = $module->$methodName($userID, $amount);
        $currentBalance = $this->credit_balance(true);

        outputDone($currentBalance);
    }

    public function stats(){
        $userID = requireLogin();

        $argRules = [
            'month' => 'required|month',
            'timestamp' => 'timestamp',
            'page' => 'int',
            'page_size' => 'int',
        ];
        validArgs($argRules);

        $month = $_REQUEST['month'];
        $timestamp = empty($_REQUEST['timestamp']) ? time() : $_REQUEST['timestamp'];
        $page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
        $pageSize = empty($_REQUEST['page_size']) ? 20 : $_REQUEST['page_size'];

        $stats = getModel('game_platform_play_log')
            ->getStats($userID, $month, $timestamp, $page, $pageSize);

        $result = [
                      'timestamp' => $timestamp,
                      'month' => $month,
                      'page' => $page,
                      'page_size' => $pageSize,
                  ] + $stats;

        output($result);
    }

    public function game_log(){
        requireLogin();

        $argRules = [
            'log_id' => 'required|int',
        ];
        validArgs($argRules);

        $log = getModel('game_platform_play_log')->getLog($_REQUEST['log_id']);

        output($log,'log');
    }

    public static function getModule(){
        $platformInfo = self::getPlatformInfo();
        return getGameModule($platformInfo['name']);
    }

    private static function getPlatformInfo(){
        $platformTable = DB_PREFIX . 'game_platform';
        $gameTable = DB_PREFIX . 'game_platform_game';
        if($_REQUEST['game_id']){
            return $GLOBALS['db']->getRow("
                SELECT `$platformTable`.*
                FROM `$platformTable`, `$gameTable`
                WHERE
                    `$gameTable`.id = {$_REQUEST['game_id']}
                    AND `$platformTable`.id = `$gameTable`.platform_id
            ");
        } else{
            return getModel('game_platform')->getInfo($_REQUEST['platform_id']);
        }
    }
}
