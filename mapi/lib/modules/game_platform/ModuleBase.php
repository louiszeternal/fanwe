<?php

abstract class ModuleBase
{
    abstract protected function directLogin($userID, $gameID);
    abstract protected function creditBalance($userID);
    abstract protected function addCredit($userID, $amount);
    abstract protected function withdrawCredit($userID, $amount);
    abstract public function localizedGameRecord($startTime, $endTime);

    protected $moduleName;
    protected $info;
    protected $gameInfo;
    protected $userPlatformInfo;

    public $timeout = 60;
    public $silentMode = false;
    public $queryResult;

    protected $autoLog = true;
    protected $logFirst = false;
    protected $logID;

    const METHODS_NEED_USER_PLATFORM_INFO = [
        'directLogin',
        'creditBalance',
        'addCredit',
        'withdrawCredit'
    ];

    public function __construct()
    {
        $this->moduleName = strtolower(get_called_class());
        $this->info = getModel('game_platform')->getInfoByName($this->moduleName);
        if($_REQUEST['game_id']){
            $this->gameInfo = getModel('game_platform_game')->getInfo($_REQUEST['game_id']);
        }
    }

    // unit of amount on 3rd platform
    protected function toPlatformAmount($amount){
        return $this->info['amount_unit'] == AmountUnitType::yuan ? $amount : toIntAmount($amount);
    }

    // unit of amount on local
    protected function amountAsYuan($amount){
        return $this->info['amount_unit'] == AmountUnitType::yuan ? $amount : toFloatAmount($amount);
    }

    protected function encode_user_nickname(){
        return urlencode($GLOBALS['user_info']['nick_name']);
    }

    public function log($type, $amount = null, $status = 1){
        $model = getModel('user_log');

        fixConst($type);

        $data = [
            'out_platform_id' => $this->info['id'],
            'status' => $status
        ];

        if(!blank($amount)){
            $data['diamonds'] = $amount;
        }

        if(!empty($this->gameInfo['id'])){
            $data['prop_id'] = $this->gameInfo['id'];
        }
        if(!empty($this->gameInfo['game_id'])){
            $data['out_item_id'] = $this->gameInfo['game_id'];
        }

        if(!empty($this->userPlatformInfo['platform_user_id'])){
            $data['out_account'] = $this->userPlatformInfo['platform_user_id'];
        }

        return $model->log($type, $data);
    }

    public function autoLogger($methodName, $amount = null){
        $logArgs = null;
        switch($methodName){
            case 'directLogin':
                $logArgs = [USER_LOG_GAME_LOGIN];
                break;
            case 'addCredit':
                $logArgs = [USER_LOG_GAME_CREDIT_ADD, $amount];
                break;
            case 'withdrawCredit':
                $logArgs = [USER_LOG_GAME_CREDIT_WITHDRAW, $amount];
                break;
            default:
                return false;
        }
        return $this->log(...$logArgs);
    }

    public function getUserPlatformInfo($userID, $platformID){
        if(empty($this->userPlatformInfo)){
            $this->userPlatformInfo = getModel('game_platform_user')->getInfo($userID, $platformID);
        }
    }

    public function setLogStatus($logID, $status){
        getModel('game_platform_log')->update(
            ['status' => $status],
            ['id' => $logID]
        );
    }

    public function outputError($error = '发生不明错误', $status = 0, $data = []){
        if(!$this->silentMode){
            outputError($error, $status, $data);
        }
        $this->queryResult = compact('status','error','data');
        return false;
    }

    public function __call($name, $args)
    {
        if(in_array($name, self::METHODS_NEED_USER_PLATFORM_INFO)){
            $this->getUserPlatformInfo($args[0],$this->info['id']);       // $args[0] have to be userID
            if(empty($this->userPlatformInfo)){
                $this->outputError('此用户无此游戏平台帐号',ErrorCode::PLATFORM_ERROR_NO_ACCOUNT);
            }
        }
        $result = false;
        if($this->autoLog){
            if($this->logFirst){
                $this->logID = $this->autoLogger($name, $args[1]);
                $result = $this->$name(...$args);
            } else{
                $result = $this->$name(...$args);
                $this->logID = $this->autoLogger($name, $args[1]);
            }
        }
        return $result;
    }
}
