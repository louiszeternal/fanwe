<?php

require_once 'ModuleBase.php';

class Duiba extends ModuleBase
{
    protected function directLogin($userID, $gameID)
    {
        $credit = getModel('game_platform_user')
            ->getCreditBalance($userID,$this->info['id']);

        return [
            'game_link' => $this->buildCreditAutoLoginRequest($gameID, $credit),
            'orientation' => $this->gameInfo['orientation'],
        ];
    }

    protected function creditBalance($userID)
    {
        return $this->getCredit($userID);
    }

    protected function addCredit($userID, $amount)
    {
        return $this->operateCredit($amount, $userID, 'add');
    }

    protected function withdrawCredit($userID, $amount)
    {

        return $this->operateCredit($amount, $userID, 'withdraw');
    }

    public function listenConsumeCredit(){
        $args = $this->getArgs();
        $currentCredit = 0;
        if(!$this->signVerify($args)){
            try {
                $currentCredit = $this->getCredit($args['uid']);
            }catch (Exception $e){}
            api_ajax_return([
                'status' => 'fail',
                'errorMessage' => '用户积分取得失败',
                'credits' => $currentCredit
            ]);
        }

        try {
            $result = getModel('game_platform_user')->consumeCredit($args['uid'], $this->info['id'], $this->amountAsYuan($args['credits']));
            $currentCredit = $this->getCredit($args['uid']);
            if($result == -1){
                api_ajax_return([
                    'status' => 'fail',
                    'errorMessage' => '平台积分不足！',
                    'credits' => $currentCredit
                ]);
            }
        } catch (Exception $e){
            api_ajax_return([
                'status' => 'fail',
                'errorMessage' => '积分消费发生错误',
                'credits' => $currentCredit
            ]);
        }

        api_ajax_return([
            'status' => 'ok',
            'bizId' => time(),  // TODO: change to correct ticket ID
            'credits' => $currentCredit
        ]);
    }

    public function listenNotify(){
        echo 'ok';
        exit;
    }

    private function getCredit($userID){
        return getModel('game_platform_user')->getCreditBalance($userID, $this->info['id']);
    }

    private function operateCredit($amount, $userID, $type){
        $method = $type . 'Credit';
        return getModel('game_platform_user')->$method($userID, $this->info['id'], $amount);
    }

    private function getArgs(){
        $args = $_GET;
        unset($args['ctl']);
        unset($args['act']);
        unset($args['platform']);
        return $args;
    }

    // md5签名
    private function sign($array){
        $array['appKey'] = empty($array['appKey']) ? $this->info['key'] : $array['appKey'];
        $array['appSecret'] = empty($array['appSecret']) ? $this->info['secret'] : $array['appSecret'];
        $array['timestamp'] = empty($array['timestamp']) ? get_microtime() : $array['timestamp'];

        ksort($array);
        $string="";
        while (list($key, $val) = each($array)){
            $string = $string . $val ;
        }
        return md5($string);
    }

    // 签名验证
    private function signVerify($array){
        $newArray=array();
        $newArray["appSecret"] = $this->info['secret'];
        reset($array);
        while(list($key,$val) = each($array)){
            if($key != "sign" ){
                $newArray[$key]=$val;
            }

        }
        $sign=$this->sign($newArray);
        if($sign == $array["sign"]){
            return true;
        }
        return false;
    }

    // 生成自动登录地址
    private function buildCreditAutoLoginRequest($userID, $credits){
        $url = "//www.duiba.com.cn/autoLogin/autologin?";
        $timestamp = get_microtime();
        $array=array("uid"=>$userID,"credits"=>$credits,"timestamp"=>$timestamp);
        $sign = $this->sign($array);
        $url .= "uid={$userID}&credits={$credits}&appKey={$this->info['key']}&sign={$sign}&timestamp={$timestamp}";
        return $url;
    }
    /*
    *  生成订单查询请求地址
    *  orderNum 和 bizId 二选一，不填的项目请使用空字符串
    */
    private function buildCreditOrderStatusRequest($appKey,$appSecret,$orderNum,$bizId){
        $url="//www.duiba.com.cn/status/orderStatus?";
        $timestamp=time()*1000 . "";
        $array=array("orderNum"=>$orderNum,"bizId"=>$bizId,"appKey"=>$appKey,"appSecret"=>$appSecret,"timestamp"=>$timestamp);
        $sign=$this->sign($array);
        $url=$url . "orderNum=" . $orderNum . "&bizId=" . $bizId . "&appKey=" . $appKey . "&timestamp=" . $timestamp . "&sign=" . $sign ;
        return $url;
    }
    /*
    *  兑换订单审核请求
    *  有些兑换请求可能需要进行审核，开发者可以通过此API接口来进行批量审核，也可以通过兑吧后台界面来进行审核处理
    */
    private function buildCreditAuditRequest($appKey,$appSecret,$passOrderNums,$rejectOrderNums){
        $url="//www.duiba.com.cn/audit/apiAudit?";
        $timestamp=time()*1000 . "";
        $array=array("appKey"=>$appKey,"appSecret"=>$appSecret,"timestamp"=>$timestamp);
        if($passOrderNums !=null && !empty($passOrderNums)){
            $string=null;
            while(list($key,$val)=each($passOrderNums)){
                if($string == null){
                    $string=$val;
                }else{
                    $string= $string . "," . $val;
                }
            }
            $array["passOrderNums"]=$string;
        }
        if($rejectOrderNums !=null && !empty($rejectOrderNums)){
            $string=null;
            while(list($key,$val)=each($rejectOrderNums)){
                if($string == null){
                    $string=$val;
                }else{
                    $string= $string . "," . $val;
                }
            }
            $array["rejectOrderNums"]=$string;
        }
        $sign = $this->sign($array);
        $url=$url . "appKey=".$appKey."&passOrderNums=".$array["passOrderNums"]."&rejectOrderNums=".$array["rejectOrderNums"]."&sign=".$sign."&timestamp=".$timestamp;
        return $url;
    }
    /*
    *  积分消耗请求的解析方法
    *  当用户进行兑换时，兑吧会发起积分扣除请求，开发者收到请求后，可以通过此方法进行签名验证与解析，然后返回相应的格式
    *  返回格式为：
    *  {"status":"ok","message":"查询成功","data":{"bizId":"9381"}} 或者
    *  {"status":"fail","message":"","errorMessage":"余额不足"}
    */
    private function parseCreditConsume($appKey,$request_array){
        if($request_array["appKey"] != $appKey){
            throw new Exception("appKey not match");
        }
        if($request_array["timestamp"] == null ){
            throw new Exception("timestamp can't be null");
        }
        $verify = $this->signVerify($request_array);
        if(!$verify){
            throw new Exception("sign verify fail");
        }
        $ret=array("appKey"=>$request_array["appKey"],"credits"=>$request_array["credits"],"timestamp"=>$request_array["timestamp"],"description"=>$request_array["description"],"orderNum"=>$request_array["orderNum"]);
        return $ret;
    }
    /*
    *  兑换订单的结果通知请求的解析方法
    *  当兑换订单成功时，兑吧会发送请求通知开发者，兑换订单的结果为成功或者失败，如果为失败，开发者需要将积分返还给用户
    */
    private function parseCreditNotify($appKey,$request_array){
        if($request_array["appKey"] != $appKey){
            throw new Exception("appKey not match");
        }
        if($request_array["timestamp"] == null ){
            throw new Exception("timestamp can't be null");
        }
        $verify = $this->signVerify($request_array);
        if(!$verify){
            throw new Exception("sign verify fail");
        }
        $ret=array("success"=>$request_array["success"],"errorMessage"=>$request_array["errorMessage"],"bizId"=>$request_array["bizId"]);
        return $ret;
    }

    public function localizedGameRecord($startTime, $endTime)
    {
        // TODO: Implement localizedGameRecord() method.
    }
}
