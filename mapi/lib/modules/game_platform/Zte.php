<?php

require_once 'ModuleBase.php';

class Zte extends ModuleBase
{
    const PLATFORM_OP_LOGIN = 0;
    const PLATFORM_OP_GET_BALANCE = 1;
    const PLATFORM_OP_CREDIT_ADD = 2;
    const PLATFORM_OP_CREDIT_WITHDRAW = 3;
    const PLATFORM_OP_GET_RECORD = 6;

    const TRANS_ERROR_CODE = [
        '1' => '重复订单',
        '2' => '下分分数不足',
        '3' => '游戏账户最多不能超过50000000限制',
        '5' => '游戏中不能下分 请退出游戏再试',
        '6' => '游戏金币不足',
    ];

    protected $logFirst = true;

    protected function directLogin($userID, $gameID)
    {
        $account = $this->userPlatformInfo['platform_user_id'];

        $param = [
            's' => self::PLATFORM_OP_LOGIN,
            'account' => $account,
            'money' => 0,
            'orderid' => $this->logID,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'lineCode' => $this->info['sub_key'],
            'KindID' => $this->gameInfo['game_id'],
            'nick' => $this->encode_user_nickname(),
        ];

        $result = $this->query($param);
        if(empty($result['url'])){
            outputError('取得游戏页面失败');
        }

        return [
            'game_link' => $result['url'],
            'orientation' => $this->gameInfo['orientation'],
        ];
    }

    // TODO: Error handle
    protected function creditBalance($userID)
    {
        $account = $this->userPlatformInfo['platform_user_id'];
        $param = [
            's' => self::PLATFORM_OP_GET_BALANCE,
            'account' => $account,
        ];

        $result = $this->query($param);

        if(blank($result['money'])){
            return false;
        }

        return toIntAmount($result['money']);
    }

    protected function addCredit($userID, $amount)
    {
        $account = $this->userPlatformInfo['platform_user_id'];
        $userModel = getModel('user');
        $credit = $userModel->selectOne(['id' => $userID])['diamonds'];
        if($credit < $amount){
            outputError('用户余额不足！');
        }

        $param = [
            's'       => self::PLATFORM_OP_CREDIT_ADD,
            'account' => $account,
            'money'   => toFloatAmount($amount),
            'orderid' => $this->logID,
        ];
        $res = $this->query($param);

        if($res['code'] != 0){
            outputError(self::TRANS_ERROR_CODE[$res['code']]);
        }
        $this->creditBalance($userID);

        $SQL = "
            UPDATE t_user
            SET diamonds = diamonds - {$amount}
            WHERE id = $userID
        ";
        $GLOBALS['db']->query($SQL);

        return true;
    }

    protected function withdrawCredit($userID, $amount)
    {
        $account = $this->userPlatformInfo['platform_user_id'];
        $param = [
            's' => self::PLATFORM_OP_CREDIT_WITHDRAW,
            'account' => $account,
            'money' => toFloatAmount($amount),
            'orderid' => $this->logID,
        ];

        $res = $this->query($param);
        if($res['code'] != 0){
            outputError(self::TRANS_ERROR_CODE[$res['code']]);
        }

        $SQL = "
            UPDATE t_user
            SET diamonds = diamonds + {$amount}
            WHERE id = $userID
        ";
        $GLOBALS['db']->query($SQL);

        return true;

        //return $this->operateCredit($amount, self::CREDIT_WITHDRAW);
    }

    public function localizedGameRecord($startTime, $endTime)
    {
        $param = [
            's' => self::PLATFORM_OP_GET_RECORD,
            'starttime' => $startTime,
            'endtime' => $endTime,
            'chid' => $this->info['sub_key'],
        ];
        $response = $this->query($param);
        if(empty($response['data'])){
            return false;
        }
        $table = DB_PREFIX.'game_platform_play_log';
        $valuesSql = '';

        foreach($response['data'] as $record){
            $earnAmount = $record['bet_sum'] + $record['change'];
            $datetime = date('Y-m-d H:i:s',$record['time']);
            $valuesSql.="
            (
                {$record['account']},
                {$this->info['id']},
                {$record['gameid']},
                {$record['bet_sum']},
                {$earnAmount},
                '{$record['id']}',
                '$datetime'
            ),";
        }

        $valuesSql = rtrim($valuesSql,',');
        $SQL = "
            INSERT INTO `$table`
            (user_id, platform_id, game_id, consume_amount, earn_amount, out_order_no, datetime)
            VALUES
            {$valuesSql}
            ON DUPLICATE KEY UPDATE id = id
        ";

        Connect::exec($SQL);
    }

    private function query($param){
        $encrypted = $this->buildParamStr($param);
        $agent = $this->info['key'];
        $timestamp = time();
        $query = [
            'agent' => $agent,
            'timestamp' => $timestamp,
            'param' => $encrypted,
            'key' => $this->sign($timestamp),
        ];
        $queryStr = http_build_query($query);
        $URL = "http://{$this->info['host']}/channelHandle?$queryStr";

        $res = httpGet($URL, $errorCode, $this->timeout);

        if($res === false){
            if($errorCode === 28){
                return $this->outputError('游戏平台请求超时',0, [
                    'code' => ErrorCode::PLATFORM_ERROR_RESPONSE_TIMEOUT
                ]);
            }
            return $this->outputError('游戏平台请求失败',0, [
                'code' => ErrorCode::PLATFORM_ERROR_REQUEST_FAIL
            ]);
        }

        if(empty($res)){
            return $this->outputError('游戏平台返回异常',0,[
                'code' => ErrorCode::PLATFORM_ERROR_RESPONSE_ERROR
            ]);
        }

        $data = json_decode($res, true);

        if(empty($data['d'])){
            $errorMsg = '游戏平台返回异常'
                        . (empty($data['error']['message']) ? '' : '：'.$data['error']['message']);
            return $this->outputError($errorMsg, 0, [
                'code'=>ErrorCode::PLATFORM_ERROR_RESPONSE_ERROR
            ]);
        }

        return $data['d'];
    }

    private function buildParamStr($param){

        $paramStr = http_build_query($param);
        $encrypted = openssl_encrypt($paramStr,'AES-128-CBC', $this->info['secret'], OPENSSL_PKCS1_PADDING, $this->info['iv']);
        return base64_encode($encrypted);
    }

    private function sign($timestamp){
        return md5($this->info['key'].$timestamp.$this->info['checksum']);
    }
}
