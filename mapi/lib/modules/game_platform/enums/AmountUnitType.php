<?php

// 金额单位类型枚举
class AmountUnitType
{
    const yuan = 0; // 人民币-元，带小数两位至分
    const cent = 1; // 人民币-分，整数
}