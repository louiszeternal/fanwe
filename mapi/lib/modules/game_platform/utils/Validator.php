<?php
/**
 * Yet Another Simple Validator
 *
 * @author    Code418(code418@qq.com)
 * @copyright Code418(code418@qq.com)
 * @license   MIT License
 * @version   0.65
 */

namespace code418;

class Validator
{
    private static $regexList = [
        'int' => '/^\d+$/',
        'amount' => '/^\d+(\.\d{1,2})?$/',
        'amountInt' => '/^\d+$/',
        'bool' => '/^[01]$/',
        'month' => '/^(19|20|21)\d{2}-(0[1-9]|1[012])$/',
        'timestamp' => '/^\d{10}$/',
    ];

    private static $funcList = [
        'bankcard' => 'bankcard',
        'length' => 'length',
        'max' => 'max',
        'min' => 'min',
        //'default' => 'default'
    ];

    private static $failMsgList = [
        'int' => '需为整数',
        'amount' => '需为正确的金额数字（最多小数点后两位至分）',
        'amountInt' => '需为正确的金额数字（整数，单位为分）',
        'bankcard' => '银行卡号校验失败，请确认',
        'bool' => '需为真假值',
        'length' => '长度不正确',
        'max' => '太长',
        'min' => '太短',
        'month' => '请正确输入月份',
        'timestamp' => '请求时间戳错误'
    ];

    public static function valid($array, $rules, $filter = false){
        // Recoding for rule:requiredGroup
        $reqGroups = [];
        foreach($rules as $argName => $rule){
            $ruleList = explode('|',$rule);
            // check rule:required first for performance
            if(self::_blank($array[$argName])){
                if(in_array('required', $ruleList)){
                    return self::_fail("参数错误：缺少<$argName>");
                }
                continue;
            }
            $reqIndex = array_search('required',$ruleList);
            if($reqIndex !== false){
                unset($ruleList[$reqIndex]);
            }

            // Handle rule:requireGroup
            $regGroupRegex = '/^requiredGroup:(\w+)$/';
            $reqGroupList = preg_grep($regGroupRegex, $ruleList);
            if(!empty($reqGroupList)){
                foreach($reqGroupList as $reqGroup){
                    preg_match($regGroupRegex,$reqGroup, $march);
                    if(!empty($march[1])){
                        $groupName = $march[1];
                        if(empty($reqGroups[$groupName])){
                            $reqGroups[$groupName] = [
                                'args' => [],
                                'valid' => false,
                            ];
                        }
                        $reqGroups[$groupName]['args'][] = $argName;
                        if(!empty($array[$argName])){
                            $reqGroups[$groupName]['valid'] = true;
                        }
                    }
                }
                $ruleList = array_diff($ruleList, $reqGroupList);
            }
            // Do valid rules
            foreach($ruleList as $ruleName){
                if(!self::check($ruleName, $array[$argName])){
                    return self::_errorMsg($ruleName,$argName);
                }
            };
        }

        // Check if empty requireGroup exist
        foreach($reqGroups as $reqGroup){
            if(!$reqGroup['valid']){
                $failArgs = '<'.join('>, <',$reqGroup['args']).'>';
                return self::_fail('参数错误：缺少参数组' . $failArgs);
            }
        }

        // Clear
        $result = self::_pass();

        // Args filtering
        if($filter){
            $result['filtered'] = array_intersect_key($_REQUEST,$rules);
        }

        return $result;
    }

    public static function check($ruleName, $value, $argName = null){
        if(array_key_exists($ruleName, self::$regexList)){
            $regex = self::$regexList[$ruleName];
            if(!preg_match($regex, $value)){
                return empty($argName) ? false : self::_errorMsg($ruleName,$argName);
            }
        } else{
            $ruleArgs = self::_splitRuleArgs($ruleName);
            if (array_key_exists($ruleName, self::$funcList)){
                $validMethod = '_valid__'.self::$funcList[$ruleName];
                if( !self::$validMethod($value,...$ruleArgs)){
                    return empty($argName) ? false : self::_errorMsg($ruleName, $argName);
                }
            }
        }
        return empty($argName) ? true : self::_pass();
    }

    //private static function _valid__default(&$value, $defVal){
    //    if(self::_blank($value)){
    //        $value = $defVal;
    //    }
    //    return true;
    //}

    private static function _valid__length($value, $min, $max){
        $len = mb_strlen($value);
        return empty($max) ? $len == $min : ($len >= $min && $len <= $max);
    }

    private static function _valid__max($value, $max){
        $len = mb_strlen($value);
        return ($len <= $max);
    }

    private static function _valid__min($value, $min){
        $len = mb_strlen($value);
        return ($len >= $min);
    }

    private static function _valid__bankcard($value){
        if(!is_numeric($value) || strlen($value) < 13 || strlen($value) > 19){
            return false;
        }
        $arr_no = str_split($value);
        $last_n = $arr_no[count($arr_no)-1];
        krsort($arr_no);
        $i = 1;
        $total = 0;
        foreach ($arr_no as $n){
            if($i%2==0){
                $ix = $n*2;
                if($ix>=10){
                    $nx = 1 + ($ix % 10);
                    $total += $nx;
                }else{
                    $total += $ix;
                }
            }else{
                $total += $n;
            }
            $i++;
        }
        $total -= $last_n;
        $x = 10 - ($total % 10);
        if($x == $last_n){
            return true;
        }else{
            return false;
        }
    }

    private static function _errorMsg($ruleName,$argName){
        self::_splitRuleArgs($ruleName);
        $errorMsg = self::$failMsgList[$ruleName];
        return self::_fail("参数错误：<$argName>$errorMsg");
    }

    private static function _fail($msg = '数据验证失败'){
        return [
            'status' => false,
            'msg' => $msg
        ];
    }

    private static function _pass(){
        return [
            'status' => true
        ];
    }

    private static function _splitRuleArgs(&$ruleName){
        $ruleArgs = explode(':',$ruleName);
        $ruleName = array_shift($ruleArgs);
        return empty($ruleArgs) ? $ruleArgs : explode(',',$ruleArgs[0]);
    }

    private static function _blank($var)
    {
        return ($var !== '0' && $var !== 0 && $var !== false && empty($var));
    }
}
