<?php

require_once 'Validator.php';
require_once MODULES_PATH.'enums/AmountUnitType.php';
require_once MODULES_PATH.'enums/ErrorCode.php';

use code418\Validator;

function outputError($error = '发生不明错误', $status = 0, $data = [])
{
    api_ajax_return(compact('error','status','data'));
}

function outputDone($data = null, $wrapKey = null)
{
    $output = ['status' => 1];
    if( !blank($data)){
        $output += formatOutput($data, $wrapKey);
    }
    ajax_return($output);
}

function output($data, $wrapKey = null)
{
    ajax_return(formatOutput($data, $wrapKey));
}

function formatOutput($data, $wrapKey = null)
{
    if(is_array($data)){
        $data = outputStrArray($data);
        if( !empty($wrapKey)){
            $data = [$wrapKey => $data];
        }
    }else{
        $data = outputStr($data);
        if(empty($wrapKey)){
            $wrapKey = 'value';
        }
        $data = [$wrapKey => $data];
    }

    return $data;
}

function noLoginError(){
    ajax_return(
        [
            'error'             => '用户未登录，请先登录。',
            'status'            => 0,
            'user_login_status' => 0 // user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
        ]
    );
}

function requireLogin()
{
    if(empty($GLOBALS['user_info']['id'])){
        noLoginError();
    }
    return $GLOBALS['user_info']['id'];
}



function toFloatAmount($intAmount)
{
    return bcdiv($intAmount, 100, 2);
}

function toIntAmount($floatAmount)
{
    return bcmul($floatAmount, 100);
}

/*
 * Dataset: Arrays which have data structure with rows and columns.
 * Just like SQL query results.
 */

function dataset_search($dataset, $column, $value)
{
    return array_search($value, array_column($dataset, $column));
}

// Lookup just one matched row by column
function dataset_lookup($dataset, $column, $value)
{
    $index = dataset_search($dataset, $column, $value);

    return $index === false ? null : $dataset[$index];
}

// Select all matched rows by column
function dataset_select($dataset, $column, $value)
{
    $matchedIndexes = array_keys(array_column($dataset, $column), $value);

    return array_map(function($index) use ($dataset){
        return $dataset[$index];
    }, $matchedIndexes);
}

// Remove column in dataset
function dataset_del_col(&$dataset, $column)
{
    return array_walk($dataset, function(&$row) use ($column){
        unset($row[$column]);
    });
}

function dataset_col_walk(&$dataset, $column, $callable, ...$extraArgs)
{
    array_walk($dataset, function(&$row, $index) use ($callable, $column, $extraArgs){
        $row[$column] = $callable($row[$column], $index, ...$extraArgs);
    });
}

function initModel()
{
    require_once LIB_PATH.'core/Model.class.php';
    Model::$lib = LIB_PATH;
}

function getModel($name)
{
    initModel();

    return Model::build($name);
}

function validArgs($rules, $filter = false)
{
    return valid($_REQUEST, $rules, $filter);
}

function valid($array, $rules, $filter = false)
{
    $result = Validator::valid($array, $rules, $filter);
    if( !$result['status']){
        outputError($result['msg']);
    }
    $result['status'] = 1;

    return $result;
}

function validVal($value, $rule, $argName)
{
    $result = Validator::check($rule, $value, $argName);
    if( !$result['status']){
        outputError($result['msg']);
    }
    $result['status'] = 1;

    return $result;
}

function blank($var)
{
    return ($var !== '0' && $var !== 0 && $var !== false && empty($var));
}

function httpGet($URL, &$error, $timeout = 60)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    $response = curl_exec($ch);

    if($response === false){
        $error = curl_errno($ch);
    }

    curl_close($ch);

    return $response;
}

function outputStr($var)
{
    if(is_string($var)){
        return $var;
    }
    if(is_bool($var)){
        return $var ? '1' : '0';
    }
    if($var === null){
        return '';
    }
    if( !is_scalar($var)){
        var_dump($var);die;
        outputError('内部错误：输出中包括非标量');
    }

    return "$var";
}

// Output a array with values in string
function outputStrArray($array)
{
    array_walk($array, function(&$val, $key){
        // do not wrap `status`
        if($key === 'status' || $key === 'error_code'){
            return;
        }
        $val = is_array($val) ? outputStrArray($val) : outputStr($val);
    });
    return $array;
}

function getGameModule($platformName){
    $moduleClass = ucfirst(strtolower($platformName));
    $modulePath = LIB_PATH . "modules/game_platform/$moduleClass.php";
    if(!is_file($modulePath)){
        outputError('未找到平台对应模组');
    }

    require_once $modulePath;

    return new $moduleClass();
}

function wholeMonth($month, $datetime = false){
    $result = [
        'start' => strtotime("{$month}-01 00:00:00"),
        'end' => strtotime("last day of {$month}-01 23:59:59")
    ];

    if($datetime){
        $result = array_map('toDatetime', $result);
    }

    return $result;
}

function wholeDay($date = 'today'){
    return [
        'start' => strtotime($date.' 0:0:0'),
        'end' => strtotime($date.' 23:59:59')
    ];
}

function timeByTenMin($time = 'now'){
    if(!is_numeric($time) || $time != (int)$time){
        $time = strtotime($time);
    }
    $min = date('Y-m-d H:i', $time);
    return strtotime(substr_replace($min,'0:00', -1));
}

function toDatetime($timestamp){
    return date('Y-m-d H:i:s', $timestamp);
}

function fixConst(&$const){
    if(!is_int($const)){
        $const = constant($const);
    }
}
