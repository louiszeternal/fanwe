<?php
// +----------------------------------------------------------------------
// | XX 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// | Author:
// +----------------------------------------------------------------------

class game_platform_listenModule  extends baseModule
{
    private $module;

    public function __construct()
    {
        parent::__construct();

        if(!$_REQUEST['platform']){
            outputError('未给定平台名');
        }

        $moduleClass = ucfirst(strtolower($_REQUEST['platform']));
        $modulePath = LIB_PATH . "modules/game_platform/$moduleClass.php";
        if(!is_file($modulePath)){
            outputError('未找到平台对应模组');
        }

        require_once $modulePath;

        $this->module = new $moduleClass();
    }

    public function notify()
    {
        $this->module->listenNotify();
    }

    public function credit_add()
    {
//        $this->module->listenAddCredit();
    }

    public function credit_withdraw()
    {
//        $this->module->listenWithdrawCredit();
    }

    // 给如兑吧这类无积分系统，纯粹由我方实现积分并消费之的监听接口
    public function credit_consume()
    {
        $this->module->listenConsumeCredit();
    }

    // 给如兑吧这类无积分系统，纯粹由我方实现积分并增加之的监听接口
    public function credit_increase()
    {
        $this->module->listenIncreaseCredit();
    }
}
