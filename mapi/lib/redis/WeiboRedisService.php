<?php

class WeiboRedisService extends BaseRedisService
{
    /**
     * @var string 用户小视频播放列表
     */
    public $video_weibo_db;

    /**
     * WeiboRedisService constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->video_weibo_db = $this->prefix . 'weibo:';
    }


    /**
     * @param int   $user_id 用户id
     * @param array $data
     * @return bool|int
     */
    public function set($user_id, $data)
    {
        $user_id = intval($user_id);
        if (!$user_id) {
            return false;
        }
        $redis = $this->redis->multi();
        $redis->hMset($this->video_weibo_db . $user_id, $data);
        $res = $redis->exec();
        if (isset($res[0]) && $res[0] !== false) {
            return $user_id;
        } else {
            return false;
        }
    }

    public function get($user_id, $field = '')
    {
        $user_id = intval($user_id);
        if (!$user_id) {
            return false;
        }
        if ($field) {
            if (is_string($field)) {
                $field = explode(',', $field);
            }
            return $this->redis->hMGet($this->video_weibo_db . $user_id, $field);
        } else {
            return $this->redis->hGetAll($this->video_weibo_db . $user_id);
        }
    }

    public function del($user_id)
    {
        $id = intval($user_id);
        if (!$id) {
            return false;
        }
        return $this->redis->delete($this->video_weibo_db . $user_id);
    }


}
