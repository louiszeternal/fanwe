<?php

class VehicleRedisService extends BaseRedisService
{
	/**
	 * @var string 数据前缀
	 */
	var $user_vehicle;

	/**
	 * GamesRedisService constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->user_vehicle = $this->prefix . 'vehicle_num:';
	}


	public function vehicleNum($user_id, $video_id, $num = 0)
	{

		if ($video_id <=0 || $user_id <= 0) {
			return false;
		}
//		fanwe_require(APP_ROOT_PATH.'mapi/lib/redis/VideoRedisService.php');
//		$video_redis = new VideoRedisService();
//		$video = $video_redis->getOne_db($video_id,'live_in');

		$time = 20; //key一小时过期
		$key = $this->user_vehicle.$video_id.':'.$user_id;
		//		//检查key是否存在
		$exists = $this->redis->exists($key);
		//key数量+1
		$this->redis->incr($key);
		if ($exists) {
			$count = $this->redis->get($key);
			if ($count > $num) {
				return false;
			}
		} else {
			// 首次计数 设定过期时间
			$this->redis->expire($key, $time);
		}
		return true;
	}
}
