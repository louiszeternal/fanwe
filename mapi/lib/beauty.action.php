<?php
class beautyModule extends baseModule
{
	public function sticker_list()
	{
		$root['list'] = array();
		$sql = "select name,file_name,image,download_link from ".DB_PREFIX."men_yan_list order by sort asc";
		$res = $GLOBALS['db']->getAll($sql);
		foreach ($res as $k => $v) {
			$res[$k]['image'] = get_spec_image($res[$k]['image']);
			if (strstr($res[$k]['download_link'],'http') === false) {
				$res[$k]['download_link'] = SITE_DOMAIN.$res[$k]['download_link'];
			}
		}
		$root['list'] = $res;
		$root['status'] = 1;
		$root['error'] = '';
		ajax_return($root);
	}
}
