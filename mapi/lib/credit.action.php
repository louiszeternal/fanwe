<?php

class creditModule  extends baseModule
{
    private $module;

    public function get_balance()
    {
        $userID = requireLogin();
        $balance = getModel('user')->getBalance($userID);

        $balance = array_map('toFloatAmount', $balance);

        output($balance);
    }

    public function withdraw()
    {
        $userID = requireLogin();
        $argRules = [
            'amount' => 'required',
            'pin' => 'required|int|length:4'
        ];

        validArgs($argRules);

        $userModel = getModel('user');
        if(!$userModel->validPin($userID, $_REQUEST['pin'])){
            outputError('交易密码错误！');
        }

        $result = $userModel->withdrawCredit($userID, $_REQUEST['amount']);

        outputDone($result);
    }

    // 设置交易密码
    public function set_pin(){
        $userID = requireLogin();
        $argRules = [
            'pin' => 'required|int|length:4'
        ];

        validArgs($argRules);
        getModel('user')->setPin($userID, $_REQUEST['pin']);

        outputDone();
    }
}
