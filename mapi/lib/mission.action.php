<?php
// +----------------------------------------------------------------------
// | XX 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) .
// +----------------------------------------------------------------------
// | Author:
// +----------------------------------------------------------------------

class missionModule extends baseModule
{
    /**
     * 构造函数，导入模型库
     */
    public function __construct()
    {
        parent::__construct();
        require_once APP_ROOT_PATH . 'mapi/lib/core/Model.class.php';
        Model::$lib = dirname(__FILE__);
    }

    public function __destruct()
    {
        parent::__destruct();
    }
    /**
     * api返回信息
     * @param  string  $error  [description]
     * @param  integer $status [description]
     * @param  array   $data   [description]
     * @return [type]          [description]
     */
    protected static function returnError($error = '出错了！', $status = 0, $data = [])
    {
        $data['status'] = $status;
        $data['error']  = $error;
        if ($error == '参数错误') {
            $data['data'] = $_REQUEST;
        }
        api_ajax_return($data);
    }
    /**
     * 日志写入
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    protected static function pushLog($data)
    {
        if (IS_DEBUG) {
            fanwe_require(APP_ROOT_PATH . 'mapi/lib/tools/PushLog.class.php');
            PushLog::log($data);
        }
    }
    /**
     * 获取用户id
     * @return [type] [description]
     */
    protected static function getUserId()
    {
        $user_id = intval($GLOBALS['user_info']['id']);
        if ($_REQUEST['test'] == 'test' && IS_DEBUG) {
            return 1;
        }
        if (!$user_id) {
            self::returnError('未登录');
        }
        return $user_id;
    }
    public function commitMission()
    {
        $user_id  = self::getUserId();
        $type     = intval($_REQUEST['type']);
        $m_config = load_auto_cache("m_config");
        if (!$m_config['mission_switch'] && OPEN_MISSION == 1) {
            self::returnError('未开启每日在线任务');
        }
        $res = Model::build('mission')->commitMission($user_id, $type);
        if (is_array($res)) {
            self::returnError('领取成功', 1, $res);
        } else {
            self::returnError($res);
        }
    }
    public function getMission()
    {
        $user_id = self::getUserId();
        $m_config = load_auto_cache("m_config");
        if (!$m_config['mission_switch'] && OPEN_MISSION == 1) {
            self::returnError('未开启每日在线任务');
        }
        $mission = Model::build('mission')->getMissionInfo($user_id);
        self::returnError('领取成功', 1, $mission);
    }
    public function getMissionList()
    {
        $user_id = self::getUserId();
        $m_config = load_auto_cache("m_config");
        if (!$m_config['mission_switch'] && OPEN_MISSION == 1) {
            self::returnError('未开启每日在线任务');
        }
        $list    = Model::build('mission')->getMissionList($user_id);
        self::returnError('', 1, compact('list'));
    }

	public function host_mission_list()
	{
		$user_id = self::getUserId();
		$m_config = load_auto_cache("m_config");
		$reward_live_time = intval($m_config['reward_live_time']) * 60;
		$reward_live_minute = intval($m_config['reward_live_time']);
		$reward_ticket = intval($m_config['reward_ticket']);
		$reward_title = strim($m_config['reward_title']);
		$list = array ();
		if ($m_config['open_host_mission'] == 0 || (!defined('OPEN_HOST_MISSION') && OPEN_HOST_MISSION != 1)) {
			self::returnError('', 0, compact('list'));
		}
		if ($m_config['is_reward_show_mission'] == 1) {
			$excellent_id = $GLOBALS['db']->getOne("SELECT excellent_id FROM ".DB_PREFIX."user WHERE id = {$user_id} and excellent_id > 0 ");
			if ($excellent_id == '') {
				self::returnError('', 0, compact('list'));
			}
		}

		$video = $GLOBALS['db']->getRow("SELECT id,create_time,prop_table FROM " . DB_PREFIX . "video WHERE user_id = $user_id AND live_in = 1");
		if ($video['id'] == '') {
			self::returnError('不在直播中');
		}
		//主播直播时长每日任务，创建直播同时生成一条记录
		$today_start = strtotime(date('Y-m-d 00:00:00', time())); //00:00:00
		$today_end = strtotime(date('Y-m-d 23:59:59', time())); //23:59:59
		$achieve_mission = $GLOBALS['db']->getOne("SELECT id FROM " . DB_PREFIX . "host_mission WHERE create_time BETWEEN $today_start AND $today_end AND user_id = $user_id AND status = 2");
//		if ($achieve_mission == '') {
//			$host_mission = $GLOBALS['db']->getRow("SELECT * FROM ".DB_PREFIX."host_mission WHERE video_id = {$video['id']} AND user_id = $user_id");
//			if ($host_mission == '') {
//				$mission_data = array ();
//				$mission_data['video_id'] = $video['id'];
//				$mission_data['user_id'] = $user_id;
//				$mission_data['create_time'] = $video['create_time'] + 8 * 3600;
//				$mission_data['end_time'] = '';
//				$mission_data['status'] = 0;
//				$GLOBALS['db']->autoExecute(DB_PREFIX."host_mission", $mission_data,'INSERT');
//			}
//		}

		$list = array ();
		$status = 0;
		//当日已完成
		if ($achieve_mission > 0) {
			$list[] = [
				'mission_id' => intval($achieve_mission),
				'image' => get_domain().'/public/images/host_watch_live.png',
				'title' => $reward_title,
				'desc' => '奖励'.$reward_ticket.'水滴',
				'money' => 10,
				'time' => 0,    //剩余时间
				'status' => 2,
				'left_times' => 0,
			];
		} else {
			$achieve_mission = $GLOBALS['db']->getOne("SELECT id FROM " . DB_PREFIX . "host_mission WHERE create_time BETWEEN $today_start and $today_end and user_id = $user_id and status = 1");
			if ($achieve_mission > 0) {
				$list[] = [
					'mission_id' => intval($achieve_mission),
					'image' => get_domain().'/public/images/host_watch_live.png',
					'title' => $reward_title,
					'desc' => '奖励'.$reward_ticket.'水滴',
					'money' => 10,
					'time' => 0,    //剩余时间
					'status' => 1,
					'left_times' => 1,
					'progress' => 100,
				];
			} else {
				//当前直播任务状态
				$host_mission = $GLOBALS['db']->getRow("SELECT * FROM " . DB_PREFIX . "host_mission WHERE video_id = {$video['id']} AND user_id = $user_id");
				if ($host_mission['status'] == 0) {
					//任务剩余时间 创建时间 + 任务时间 - 当前时间
					$last_time = $host_mission['create_time'] + $reward_live_time - time() > 0 ? $host_mission['create_time'] + $reward_live_time - time() : 0;
					//更新状态
					if (intval($last_time) == 0) {
						$prop_ticket = $GLOBALS['db']->getOne("SELECT sum(total_ticket) FROM " . $video['prop_table'] . " WHERE video_id = {$video['id']}");
						if ($prop_ticket >= $reward_ticket) {
							//任务剩余时间 小于等于0 表示已完成
							$status = $last_time <= 0 ? 1 : 0;
							$GLOBALS['db']->query("update " . DB_PREFIX . "host_mission SET status = 1 WHERE id = {$host_mission['id']}");
							if ($GLOBALS['db']->affected_rows() === false) {
								self::returnError('系统错误，请稍后再试');
							}
						}
					}
				} else if ($host_mission['status'] == 1) {
					$last_time = 0;
					$status = intval($host_mission['status']);
				}
				if ($status == 2) {
					$list[] = [
						'mission_id' => intval($host_mission['id']),
						'image' => get_domain().'/public/images/host_watch_live.png',
						'title' => $reward_title,
						'desc' => '奖励'.$reward_ticket.'水滴',
						'money' => 10,
						'time' => $last_time,    //剩余时间
						'status' => $status,
						'left_times' => 0,
						'progress' => 100,
					];
				} else {
					$arr = ['mission_id' => intval($host_mission['id']),
						'image' => get_domain().'/public/images/host_watch_live.png',
						'title' => $reward_title,
						'desc' => '奖励'.$reward_ticket.'水滴',
						'money' => 10,
						'time' => $last_time,    //剩余时间
						'status' => $status,
						'left_times' => 1,
						'progress' => 0
					];
					if ($status == 0) {
						$arr = array_merge($arr,['target' => 1, 'current' => 0]);
					} else {
						$arr = array_merge($arr,['progress' => 100]);
					}
					$list[] = $arr;
				}
			}
		}
		$history_mission_day = intval($m_config['history_mission_day']);
		$history_time = strtotime(date("Y-m-d 00:00:00", strtotime("-{$history_mission_day} day")));
		$yester_day_time = strtotime(date("Y-m-d 23:59:59", strtotime("-1 day")));
		$history_mission_arr = $GLOBALS['db']->getAll("SELECT * FROM " . DB_PREFIX . "host_mission WHERE create_time BETWEEN $history_time AND $yester_day_time AND user_id = {$user_id} AND status = 1");
		foreach ($history_mission_arr as $k => $v) {
			$list[] = array (
				'mission_id' => intval($v['id']),
				'image' => get_domain().'/public/images/host_watch_live.png',
				'title' => $reward_title,
				'desc' => '奖励'.$reward_ticket.'水滴',
				'money' => 10,
				'time' => 0,
				'status' => intval($v['status']),
				'left_times' => 1,
				'progress' => 100,
			);
		}

		self::returnError('', 1, compact('list'));
	}

	public function get_mission_reward()
	{
		$user_id = self::getUserId();
		$mission_id = intval($_REQUEST['mission_id']);
		$m_config = load_auto_cache("m_config");
		$reward_live_time = intval($m_config['reward_live_time']) * 60;
		$reward_ticket = intval($m_config['reward_ticket']);
		$reward_title = strim($m_config['reward_title']);
		$ticket_name = $m_config['ticket_name'];
		if (!$m_config['mission_switch'] && OPEN_MISSION == 1) {
			self::returnError('未开启每日在线任务');
		}

//		$video = $GLOBALS['db']->getRow("SELECT id,create_time FROM ".DB_PREFIX."video WHERE user_id = $user_id AND live_in = 1");
//		if ($video['id'] == '') {
//			self::returnError('不在直播中');
//		}
		//当前直播任务状态
//		$host_mission = $GLOBALS['db']->getRow("SELECT * FROM ".DB_PREFIX."host_mission WHERE video_id = {$video['id']} AND user_id = $user_id AND id = {$mission_id}");
		$host_mission = $GLOBALS['db']->getRow("SELECT * FROM " . DB_PREFIX . "host_mission WHERE user_id = $user_id AND id = {$mission_id}");
		if ($host_mission == '') {
			self::returnError('任务不存在');
		} else if ($host_mission['status'] == 2) {
			self::returnError('已领取');
		} else if ($host_mission['status'] == 1 || $host_mission['status'] == 0) {
			$video = $GLOBALS['db']->getRow("SELECT id,create_time,prop_table FROM " . DB_PREFIX . "video WHERE id = (SELECT video_id FROM " . DB_PREFIX . "host_mission where id = {$mission_id})
			union all SELECT id,create_time,prop_table FROM " . DB_PREFIX . "video_history WHERE id = (SELECT video_id FROM " . DB_PREFIX . "host_mission where id = {$mission_id}) ");

			$prop_ticket = $GLOBALS['db']->getOne("SELECT sum(total_ticket) FROM " . $video['prop_table'] . " WHERE video_id = {$video['id']}");
			if ($prop_ticket < $reward_ticket) {
				self::returnError('当前直播间获得的' . $ticket_name . '未达到要求');
			}
			$reward_live_time = intval($m_config['reward_live_time']) * 60;
			//任务剩余时间 创建时间 + 任务时间 - 当前时间
			$last_time = $host_mission['create_time'] + $reward_live_time - time() > 0 ? $host_mission['create_time'] + $reward_live_time - time() : 0;
			//更新状态
			if ($last_time > 0) {
				self::returnError('直播时长未达到');
			}

			//已完成未领取 更新状态
			$GLOBALS['db']->query("update " . DB_PREFIX . "host_mission SET status = 2 WHERE id = {$host_mission['id']}");
			if ($GLOBALS['db']->affected_rows() === false) {
				self::returnError('领取失败,请稍后再试');
			}
			$GLOBALS['db']->query("update " . DB_PREFIX . "user SET ticket = ticket + $reward_ticket WHERE id = {$user_id}");
			if ($GLOBALS['db']->affected_rows() === false) {
				self::returnError('领取失败,请稍后再试');
			}
			user_deal_to_reids(array($user_id));//同步用户redis

			//写入用户日志
			$data = array();
			$data['ticket'] = $reward_ticket;
			$data['user_id'] = $user_id;
			$data['video_id'] = intval($video['id']) > 0 ? $video['id'] : 0;
			$param['type'] = 17; //主播每日任务
			$log_msg = '领取优质主播奖励，获得'.$ticket_name.$reward_ticket.',直播间id为'.$data['video_id'];
			account_log_com($data,$user_id,$log_msg,$param);
			$mission = [
				'mission_id' => intval($mission_id),
				'image' => get_domain().'/public/images/host_watch_live.png',
				'title' => $reward_title,
				'desc' => '奖励'.$reward_ticket.'水滴',
				'money' => 10,
				'time' => 0,    //剩余时间
				'status' => 2,
				'left_times' => 0,
			];
			self::returnError('领取成功', 1,compact('mission'));
		}
	}
}
