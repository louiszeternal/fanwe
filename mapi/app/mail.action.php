<?php

class mailCModule extends baseModule
{
    public function register()
    {
        $email = strim($_REQUEST['email']);
        $password = strim($_REQUEST['password']);

        if (empty($email)) {
            api_ajax_return([
                "status" => 0,
                "error" => "请输入邮箱"
            ]);
        }

        if (strlen($password) < 6) {
            api_ajax_return([
                "status" => 0,
                "error" => "请输入至少 6 位数密码"
            ]);
        }

        $sql = "select id,email,nick_name,user_pwd,is_effect from " . DB_PREFIX . "user where email = '{$email}'";
        $user = $GLOBALS['db']->getRow($sql);
        if (!empty($user)) {
            api_ajax_return([
                "status" => 0,
                "error" => "此邮箱已被占用"
            ]);
        }

        $user_id = get_max_user_id(0);
        $user = [
            "id" => $user_id,
            "nick_name" => $user_id,
            "email" => $email,
            "user_pwd" => md5($password),
            "is_effect" => false,
            "create_time" => NOW_TIME,
        ];

        $GLOBALS['db']->autoExecute(DB_PREFIX . "user", $user);
        if (!$GLOBALS['db']->affected_rows()) {
            api_ajax_return([
                "status" => 0,
                "error" => "服务器内部错误"
            ]);
        }

        $send_code = md5(mt_rand());
        $GLOBALS['tmpl']->assign("email", $email);
        $GLOBALS['tmpl']->assign("url", SITE_DOMAIN . url('mail#verify', [
                "email" => $email,
                "code" => $send_code,
            ]));
        require APP_ROOT_PATH . 'system/utils/message_send.php';
        $msg = new message_send();
        $msg->manage_msg(message_send::MAIL_VERIFY, $email, array('code' => $send_code));
        api_ajax_return([
            "status" => 0,
            "error" => "请前往邮箱激活账户"
        ]);
    }

    public function login()
    {
        $email = strim($_REQUEST['email']);
        $password = strim($_REQUEST['password']);

        if (empty($email)) {
            api_ajax_return([
                "status" => 0,
                "error" => "请输入邮箱"
            ]);
        }

        if (strlen($password) < 6) {
            api_ajax_return([
                "status" => 0,
                "error" => "请输入至少 6 位数密码"
            ]);
        }

        $sql = "select id,email,nick_name,user_pwd,is_effect from " . DB_PREFIX . "user where email = '{$email}'";
        $user = $GLOBALS['db']->getRow($sql);
        if (empty($user)) {
            api_ajax_return([
                "status" => 0,
                "error" => "邮箱未注册"
            ]);
        } elseif (md5($password) != $user['user_pwd']) {
            api_ajax_return([
                "status" => 0,
                "error" => "密码不正确"
            ]);
        }

        if (!$user['is_effect']) {
            // send email
            $send_code = md5(mt_rand());
            $GLOBALS['tmpl']->assign("email", $email);
            $GLOBALS['tmpl']->assign("url", SITE_DOMAIN . url('mail#verify', [
                    "email" => $email,
                    "code" => $send_code,
                ]));
            require APP_ROOT_PATH . 'system/utils/message_send.php';
            $msg = new message_send();
            $msg->manage_msg(message_send::MAIL_VERIFY, $email, array('code' => $send_code));
            api_ajax_return([
                "status" => 0,
                "error" => "请前往邮箱激活账户"
            ]);
        }

        es_cookie::set("client_ip", CLIENT_IP, 3600 * 24 * 30);
        es_cookie::set("nick_name", $user['nick_name'], 3600 * 24 * 30);
        es_cookie::set("user_id", $user['id'], 3600 * 24 * 30);
        es_cookie::set("user_pwd", md5($user['user_pwd'] . "_EASE_COOKIE"), 3600 * 24 * 30);
        es_cookie::set("PHPSESSID2", es_session::id(), 3600 * 24 * 30);

        api_ajax_return([
            "status" => 1,
            "user" => $user,
        ]);
    }

    public function verify()
    {
        $email = strim($_REQUEST['email']);
        $code = strim($_REQUEST['code']);

        $verify_time = NOW_TIME - app_conf("USER_SEND_VERIFY_TIME") ?: 300;
        $user_id = $GLOBALS['db']->getOne("select user_id from t_deal_msg_list where dest = '{$email}' and `code` = '{$code}' and send_time > {$verify_time} order by id desc limit 1");

        if (empty($user_id)) {
            app_redirect(url('error#no_page', [
                "status" => 0,
                "error" => "链接已失效",
            ]));
        }

        $user = $GLOBALS['db']->getRow("select id,nick_name,user_pwd,is_effect from t_user where id = " . $user_id);
        if (empty($user_id)) {
            app_redirect(url('error#no_page', [
                "status" => 0,
                "error" => "请先注册",
            ]));
        }

        if (!$user['is_effect']) {
            $GLOBALS['db']->autoExecute(DB_PREFIX . "user", ['is_effect' => true], 'UPDATE', "id=" . $user_id);
        }

        es_cookie::set("client_ip", CLIENT_IP, 3600 * 24 * 30);
        es_cookie::set("nick_name", $user['nick_name'], 3600 * 24 * 30);
        es_cookie::set("user_id", $user['id'], 3600 * 24 * 30);
        es_cookie::set("user_pwd", md5($user['user_pwd'] . "_EASE_COOKIE"), 3600 * 24 * 30);
        es_cookie::set("PHPSESSID2", es_session::id(), 3600 * 24 * 30);

        app_redirect(url('error#no_page', [
            'status' => 1,
            'error' => '激活成功'
        ]));
    }

    public function forgot()
    {
        $email = strim($_REQUEST['email']);

        if (empty($email)) {
            api_ajax_return([
                "status" => 0,
                "error" => "请输入邮箱",
            ]);
        }

        $sql = "select id,email,nick_name,user_pwd,is_effect from " . DB_PREFIX . "user where email = '{$email}'";
        $user = $GLOBALS['db']->getRow($sql);
        if (empty($user)) {
            api_ajax_return([
                "status" => 0,
                "error" => "邮箱未注册",
            ]);
        }

        $send_code = md5(mt_rand());
        $GLOBALS['tmpl']->assign("email", $email);
        $GLOBALS['tmpl']->assign("url", SITE_DOMAIN . url('mail#reset', [
                "email" => $email,
                "code" => $send_code,
            ]));
        fanwe_require(APP_ROOT_PATH . 'system/utils/message_send.php');
        $msg = new message_send();
        $msg->manage_msg(message_send::MAIL_PASSWORD, $email, array('code' => $send_code));

        api_ajax_return([
            "status" => 0,
            "error" => "请前往邮箱点击重置链接",
        ]);
    }

    public function reset()
    {
        $email = strim($_REQUEST['email']);
        $code = strim($_REQUEST['code']);

        $verify_time = NOW_TIME - app_conf("USER_SEND_VERIFY_TIME") ?: 300;
        $user_id = $GLOBALS['db']->getOne("select user_id from t_deal_msg_list where dest = '{$email}' and `code` = '{$code}' and send_time > {$verify_time} order by id desc limit 1");

        if (empty($user_id)) {
            app_redirect(url('error#no_page', [
                "status" => 0,
                "error" => "链接已失效",
            ]));
        }

        $user = $GLOBALS['db']->getRow("select id,nick_name,user_pwd,is_effect from t_user where id = " . $user_id);
        if (empty($user)) {
            app_redirect(url('error#no_page', [
                "status" => 0,
                "error" => "邮箱未注册",
            ]));
        }

        api_ajax_return([
            "status" => 1,
            "email" => $email,
            "code" => $code,
        ]);
    }

    public function reset_password()
    {
        $email = strim($_REQUEST['email']);
        $code = strim($_REQUEST['code']);

        $verify_time = NOW_TIME - app_conf("USER_SEND_VERIFY_TIME") ?: 300;
        $user_id = $GLOBALS['db']->getOne("select user_id from t_deal_msg_list where dest = '{$email}' and `code` = '{$code}' and send_time > {$verify_time} order by id desc limit 1");

        if (empty($user_id)) {
            api_ajax_return([
                "status" => 0,
                "error" => "链接已失效",
            ]);
        }

        $user = $GLOBALS['db']->getRow("select id,nick_name,user_pwd,is_effect from t_user where id = " . $user_id);
        if (empty($user)) {
            api_ajax_return([
                "status" => 0,
                "error" => "邮箱未注册",
            ]);
        }

        $password = strim($_REQUEST['password']);
        if (empty($password)) {
            api_ajax_return([
                "status" => 0,
                "error" => "请输入密码",
            ]);
        }

        if ($password != strim($_REQUEST['confirm_password'])) {
            api_ajax_return([
                "status" => 0,
                "error" => "密码输入不一致",
            ]);
        }

        $GLOBALS['db']->autoExecute(DB_PREFIX . 'user', [
            'user_pwd' => md5($password),
        ], 'UPDATE', 'id=' . $user_id);

        if ($GLOBALS['db']->affected_rows()) {
            api_ajax_return([
                "status" => 0,
                "error" => "服务器内部错误",
            ]);
        }

        api_ajax_return([
            "status" => 1,
            "error" => "成功修改密码，请使用新密码登录",
        ]);
    }
}
