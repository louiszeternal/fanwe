<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/19
 * Time: 9:05
 */
fanwe_require(APP_ROOT_PATH . 'mapi/app/page.php');

class edu_teacherCModule extends baseModule
{
    protected function auth($user_id)
    {
        $user = $GLOBALS['db']->getRow("select is_ban,ban_time,is_effect,authentication_type,is_authentication,is_agree from " . DB_PREFIX . "user where id = " . $user_id,
            true, true);

        if ($user['is_authentication'] != 2) { // 未认证
            api_ajax_return(array("status" => 0, "error" => "请先进行认证"));
        } elseif ($user['authentication_type'] != AUTH_TYPE_TEACHER) {
            api_ajax_return(array("status" => 0, "error" => "认证类型需为教师"));
        }
        return true;
    }

    //教师信息
    public function edit_teacher()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $root = array();
        $root = $GLOBALS['db']->getRow("SELECT id,description,desc_image,tags,file_id FROM " . DB_PREFIX . "edu_teacher where user_id =" . $user_id);
        $tags = $GLOBALS['db']->getAll("SELECT title FROM " . DB_PREFIX . "edu_tags where  type = 0");
        $root['tags'] = empty($root['tags']) ? array() : explode(',', $root['tags']);
        foreach ($tags as &$tag) {
            if (in_array($tag['title'], $root['tags'])) {
                $tag['is_checked'] = true;
            }
        }
        unset($tag);
        $root['tags_list'] = $tags;
        $root['error'] = '';
        $root['status'] = 1;
        api_ajax_return($root);
    }

    //编辑教师信息
    public function save_edit_teacher()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $teacher_id = strim($_REQUEST['id']);
        $data['description'] = strim($_REQUEST['description']); // 教师简介
        $data['file_id'] = strim($_REQUEST['file_id']);//宣传视频
        $teacher = $GLOBALS['db']->getRow("SELECT * FROM " . DB_PREFIX . "edu_teacher where id =" . $teacher_id);
        $data['desc_image'] = strim($_REQUEST['desc_image']);//封面图
        $data['tags'] = strim($_REQUEST['tags']);//标签

        $root = array();
        if ($teacher_id) {
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_teacher", $data, $mode = 'UPDATE', "id=" . $teacher_id)) {
                if (!empty($data['file_id']) && $teacher['file_id'] != $data['file_id']) {
                    fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/common_edu.php');
                    upload_edu_video($data['file_id'], 'edu_teacher', 'desc_video', $teacher_id);
                }
                $root['error'] = '编辑信息成功';
                $root['status'] = 1;
            } else {
                $root['error'] = '编辑信息失败';
                $root['status'] = 0;
            }
        } else {
            $root['error'] = '非教师不能编辑';
            $root['status'] = 0;
        }
        api_ajax_return($root);
    }

    //添加课程-页面数据
    public function add_lesson()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);
        $courses_id = intval($_REQUEST['courses_id']);//课程ID

        $teacher = $GLOBALS['db']->getRow("SELECT desc_image FROM " . DB_PREFIX . "edu_teacher where user_id =" . $user_id);
        $image = $teacher['desc_image'];

        $root = array();
        if ($courses_id) {
            $list = $GLOBALS['db']->getRow("SELECT category_id,price,title,image,tags,description FROM " . DB_PREFIX . "edu_courses where id =" . $courses_id);
        } else {
            $list = array();
        }
        if (empty($image)) {
            $root['error'] = '请先上传教师封面图';
            $root['status'] = 0;
        } else {
            $tags = $GLOBALS['db']->getAll("SELECT title FROM " . DB_PREFIX . "edu_tags where type = 2");//标签
            $list['tags'] = empty($list['tags']) ? array() : explode(',', $list['tags']);
            foreach ($tags as &$tag) {
                if (in_array($tag['title'], $list['tags'])) {
                    $tag['is_checked'] = true;
                }
            }
            unset($tag);
            $root['courses_id'] = $courses_id;
            $root['tags_list'] = $tags;

            $category_list = $GLOBALS['db']->getAll("SELECT id,title FROM " . DB_PREFIX . "edu_course_category where is_effect = 1");//分类
            foreach ($category_list as $k => $v) {
                if ($v['id'] == $list['category_id']) {
                    $category_list[$k]['is_selected'] = true;
                }
            }
            $root['category_list'] = $category_list;

            $root['user_id'] = $user_id;
            if (empty($list['image'])) {
                $list['image'] = $image;
            }
            $root['list'] = $list;
            $root['status'] = 1;
            $root['error'] = '';
        }
        api_ajax_return($root);
    }

    //添加课程
    public function add_courses()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $data['category_id'] = intval($_REQUEST['category_id']);//分类
        $data['price'] = intval($_REQUEST['price']);//费用
        $data['title'] = strim($_REQUEST['title']);//标题
        $data['image'] = strim($_REQUEST['image']);//封面图
        $data['tags'] = strim($_REQUEST['tags']);//标签
        $data['description'] = strim($_REQUEST['description']);//课程简介
        $data['user_id'] = $user_id;

        $courses_id = intval($_REQUEST['courses_id']);
        $mode = strim($_REQUEST['mode']);

        if (empty($data['title'])) {
            $root['error'] = '请输入标题';
            $root['status'] = 0;
            api_ajax_return($root);
        }

        if (empty($data['image'])) {
            $root['error'] = '请先上传封面';
            $root['status'] = 0;
            api_ajax_return($root);
        }

        if (empty($data['tags'])) {
            $data['tags'] = array();
        }

        $root = array();
        if ($mode == 'INSERT') {
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_courses", $data, $mode = 'INSERT')) {
                $root['error'] = '添加课程成功';
                $root['status'] = 1;
            } else {
                $root['error'] = '添加失败';
                $root['status'] = 0;
            }
        } else {
            if ($courses_id) {
                if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_courses", $data, $mode = 'UPDATE',
                    "id=" . $courses_id)
                ) {
                    $root['error'] = '更新课程信息成功';
                    $root['status'] = 1;
                }
            } else {
                $root['error'] = '更新课程信息失败';
                $root['status'] = 0;
            }
        }
        api_ajax_return($root);
    }

    //课程列表
    public function lesson_list()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $title = strim($_REQUEST['title']);//标题
        $p = intval($_REQUEST['p']);
        $p = $p > 0 ? $p : 1;
        $page_size = 20;
        $limit = ($p - 1) * $page_size . ',' . $page_size;

        $root = array();
        $root['error'] = '';
        $root['status'] = 1;

        if (!empty($title)) {
            $root['list'] = $GLOBALS['db']->getAll("SELECT id,title,price,courses_count,long_time,sale_count FROM " . DB_PREFIX . "edu_courses WHERE is_delete =0 and user_id ={$user_id} and title like " . "'%" . $title . "%' order by sort desc limit " . $limit);
            $count = $GLOBALS['db']->getOne("SELECT count(1) as count FROM " . DB_PREFIX . "edu_courses WHERE is_delete =0 and user_id ={$user_id} and title like " . "'%" . $title . "%'");
        } else {
            $root['list'] = $GLOBALS['db']->getAll("SELECT id,title,price,courses_count,long_time,sale_count FROM " . DB_PREFIX . "edu_courses WHERE is_delete =0  and user_id ={$user_id} order by sort desc limit " . $limit);
            $count = $GLOBALS['db']->getOne("SELECT count(1) as count FROM " . DB_PREFIX . "edu_courses WHERE is_delete =0 and user_id ={$user_id}");
        }
        $root['title'] = $title;
        //分页
        $page_model = new Page($count, $page_size);
        $root['page'] = $page_model->show();
        api_ajax_return($root);
    }

    //删除课程
    public function del_courses()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $courses_id = intval($_REQUEST['courses_id']);
        $data['is_delete'] = 1;
        if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_courses", $data, $mode = 'UPDATE', "id=" . $courses_id)) {
            $root['error'] = "删除成功";
            $root['status'] = 1;
        } else {
            $root['error'] = "删除失败";
            $root['status'] = 0;
        }
        api_ajax_return($root);
    }

    //目录列表
    public function class_group()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $courses_id = intval($_REQUEST['courses_id']);
        $title = strim($_REQUEST['title']);//标题

        $p = intval($_REQUEST['p']);
        $p = $p > 0 ? $p : 1;
        $page_size = 20;
        $limit = ($p - 1) * $page_size . ',' . $page_size;

        $course_title = $GLOBALS['db']->getOne("SELECT title FROM " . DB_PREFIX . "edu_courses WHERE id = " . $courses_id);
        if (!empty($title)) {
            $root['list'] = $GLOBALS['db']->getAll("SELECT id,course_id,title FROM " . DB_PREFIX . "edu_class_group WHERE is_delete =0 and course_id ={$courses_id} and title like " . "'%" . $title . "%' order by sort desc limit " . $limit);
            $count = $GLOBALS['db']->getOne("SELECT count(1) as count FROM " . DB_PREFIX . "edu_class_group WHERE is_delete =0  and course_id ={$courses_id} and title like " . "'%" . $title . "%'");
        } else {
            $root['list'] = $GLOBALS['db']->getAll("SELECT id,course_id,title FROM " . DB_PREFIX . "edu_class_group WHERE is_delete =0  and course_id ={$courses_id} order by sort desc limit " . $limit);
            $count = $GLOBALS['db']->getOne("SELECT count(1) as count FROM " . DB_PREFIX . "edu_class_group WHERE is_delete =0 and course_id ={$courses_id}");
        }
        $root['course_title'] = $course_title;
        $root['title'] = $title;
        $root['courses_id'] = $courses_id;
        //分页
        $page_model = new Page($count, $page_size);
        $root['page'] = $page_model->show();

        api_ajax_return($root);
    }

    //目录详情
    public function group_info()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $course_id = intval($_REQUEST['course_id']);
        $group_id = intval($_REQUEST['group_id']);
        $root = array();
        if ($group_id) {
            $root['list'] = $GLOBALS['db']->getRow("SELECT course_id,title,musicofasong,file_id FROM " . DB_PREFIX . "edu_class_group WHERE is_delete =0  and id = " . $group_id);
        } else {
            $root['list'] = array();
        }
        $root['group_id'] = $group_id;
        $root['course_id'] = $course_id;
        $root['status'] = 1;
        $root['error'] = '';
        api_ajax_return($root);
    }

    //编辑目录
    public function edit_group()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $data['title'] = strim($_REQUEST['title']);//标题
        $data['musicofasong'] = strim($_REQUEST['musicofasong']);//曲谱
        $data['file_id'] = strim($_REQUEST['file_id']);//视频
        $data['course_id'] = intval($_REQUEST['courses_id']);

        $group_id = intval($_REQUEST['group_id']);
        if (empty($data['title'])) {
            $root['error'] = '请输入标题';
            $root['status'] = 0;
            api_ajax_return($root);
        }
        if (!$group_id) {
            $root['course_id'] = $data['course_id'];
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_class_group", $data, $mode = 'INSERT')) {
                $id = $GLOBALS['db']->insert_id();
                if (!empty($data['file_id'])) {
                    fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/common_edu.php');
                    upload_edu_video($data['file_id'], 'edu_class_group', 'play_url', $id);
                }
                $root['error'] = '添加目录成功';
                $root['status'] = 1;
            } else {
                $root['error'] = '添加失败';
                $root['status'] = 0;
            }
        } else {
            $group_info = $GLOBALS['db']->getRow("SELECT file_id,course_id FROM " . DB_PREFIX . "edu_class_group WHERE id = " . $group_id);
            $root['course_id'] = $group_info['course_id'];
            unset($data['course_id']);
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_class_group", $data, $mode = 'UPDATE',
                "id=" . $group_id)
            ) {
                if (!empty($data['file_id']) && $group_info['file_id'] != $data['file_id']) {
                    fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/common_edu.php');
                    upload_edu_video($data['file_id'], 'edu_class_group', 'play_url', $group_id);
                }
                $root['error'] = '更新目录信息成功';
                $root['status'] = 1;
            } else {
                $root['error'] = '更新目录信息失败';
                $root['status'] = 0;
            }
        }
        api_ajax_return($root);
    }

    public function del_class_group()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $group_id = intval($_REQUEST['group_id']);
        if ($group_id) {
            $group_info = $GLOBALS['db']->getRow("SELECT id,course_id FROM " . DB_PREFIX . "edu_class_group WHERE id = " . $group_id);
            if (!empty($group_info)) {
                $data['is_delete'] = 1;
                $root = array();
                $root['course_id'] = $group_info['course_id'];
                if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_class_group", $data, $mode = 'UPDATE',
                    "id=" . $group_id)
                ) {
                    $root['error'] = '删除成功';
                    $root['status'] = 1;
                } else {
                    $root['error'] = '删除失败';
                    $root['status'] = 1;
                }
            } else {
                $root['error'] = '信息错误';
                $root['status'] = 1;
            }
        }
        api_ajax_return($root);
    }

    //视频列表
    public function class_list()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $course_id = intval($_REQUEST['course_id']);
        $group_id = intval($_REQUEST['group_id']);
        $title = strim($_REQUEST['title']);

        $p = intval($_REQUEST['p']);
        $p = $p > 0 ? $p : 1;
        $page_size = 20;
        $limit = ($p - 1) * $page_size . ',' . $page_size;

        $root = array();
        $filed = "id,title,price,long_time,(SELECT title FROM " . DB_PREFIX . "edu_class_group WHERE id = c.group_id) as class_group";
        $table = DB_PREFIX . "edu_class c";
        $where = "c.is_delete = 0 and c.course_id =" . $course_id;
        $order = "by c.sort";
        if ($group_id) {
            $where .= " and c.group_id =" . $group_id;
        }
        if ($title) {
            $where .= " and c.title like '%" . $title . "%'";
        }
        $root['list'] = $GLOBALS['db']->getAll("SELECT " . $filed . " FROM " . $table . " WHERE " . $where . " order " . $order . " limit " . $limit);
        $count = $GLOBALS['db']->getOne("SELECT count(1) as count FROM " . $table . " WHERE " . $where);
        //分页
        $page_model = new Page($count, $page_size);
        $root['page'] = $page_model->show();
        $root['title'] = $title;
        $root['group_id'] = $group_id;
        $root['course_id'] = $course_id;
        $root['error'] = '';
        $root['status'] = 1;
        api_ajax_return($root);
    }

    //课时信息
    public function class_info()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $course_id = intval($_REQUEST['course_id']);
        $class_id = intval($_REQUEST['class_id']);

        $root = array();
        if ($course_id) {
            $groups = $GLOBALS['db']->getAll("SELECT id,title FROM " . DB_PREFIX . "edu_class_group WHERE course_id =" . $course_id);
            $root['course_id'] = $course_id;
            if (!empty($groups)) {
                $root['error'] = '';
                $root['status'] = 1;
                $root['group'] = $groups;
            } else {
                $root['error'] = '请先添加目录';
                $root['status'] = 2;
            }
            api_ajax_return($root);
        }
        if ($class_id) {
            $filed = "id,course_id,group_id,title,price,type,file_id,play_url,long_time,musicofasong";
            $table = DB_PREFIX . "edu_class";
            $where = " id =" . $class_id;
            $list = $GLOBALS['db']->getRow("SELECT " . $filed . " FROM " . $table . " WHERE " . $where);
            $groups = $GLOBALS['db']->getAll("SELECT id,title FROM " . DB_PREFIX . "edu_class_group WHERE course_id =" . $list['course_id']);
            foreach ($groups as &$group) {
                if (in_array($groups['id'], $list['group_id'])) {
                    $group['is_selected'] = true;
                }
            }
            unset($group);
            $root['list'] = $list;
            $root['group'] = $groups;
            $root['error'] = '';
            $root['status'] = 1;
            api_ajax_return($root);
        }
    }

    //保存课时信息
    public function save_class()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $course_id = intval($_REQUEST['course_id']);//课程ID
        $class_id = intval($_REQUEST['class_id']);//课时ID
        $data['group_id'] = intval($_REQUEST['group_id']);//分组
        $data['title'] = strim($_REQUEST['title']);//课时名称
        $data['file_id'] = intval($_REQUEST['file_id']);//视频文件ID
        $data['long_time'] = intval($_REQUEST['long_time']);//课时时长
        $data['musicofasong'] = strim($_REQUEST['musicofasong']);//曲谱
        $data['type'] = intval($_REQUEST['type']);//类型  1:音频 0：视频
        $data['play_url'] = strim($_REQUEST['play_url']);//音频播放地址

        $root = array();
        if (empty($data['title'])) {
            $root['error'] = '请输入标题';
            $root['status'] = 0;
            api_ajax_return($root);
        }

        if (empty($data['group_id'])) {
            $root['error'] = '请先添加目录';
            $root['status'] = 0;
            api_ajax_return($root);
        }

        if ($data['type'] == 0 && empty($data['file_id'])) {
            $root['error'] = '请上传视频';
            $root['status'] = 0;
            api_ajax_return($root);
        } elseif ($data['type'] == 1 && empty($data['play_url'])) {
            $root['error'] = '请上传音频';
            $root['status'] = 0;
            api_ajax_return($root);
        }

        if (!$data['long_time'] > 0) {
            $root['error'] = '请输入时长';
            $root['status'] = 0;
            api_ajax_return($root);
        }
        if ($course_id) {
            $data['course_id'] = $course_id;
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_class", $data, $mode = 'INSERT')) {
                if (!empty($data['file_id'])) {
                    $id = $GLOBALS['db']->insert_id();
                    fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/common_edu.php');
                    upload_edu_video($data['file_id'], 'edu_class', 'play_url', $id);
                }
                $class_info = $GLOBALS['db']->getRow("select count(*) as num,sum(long_time) as long_time from t_edu_class where course_id = {$data['course_id']} and is_delete = 0");
                $course_data = array(
                    'courses_count' => $class_info['num'],
                    'long_time' => $class_info['long_time'],
                );
                if ($class_info['num'] >= 1) {
                    $course_data['is_effect'] = true;
                }
                $root['error'] = '添加成功';
                $root['status'] = 1;
            }
        }

        if ($class_id) {
            $class = $GLOBALS['db']->getRow("SELECT course_id,file_id FROM " . DB_PREFIX . "edu_class WHERE id =" . $class_id);
            $course_id = $class['course_id'];
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_class", $data, $mode = 'UPDATE', "id =" . $class_id)) {
                if (!empty($data['file_id']) && $class['file_id'] != $data['file_id']) {
                    fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/common_edu.php');
                    upload_edu_video($data['file_id'], 'edu_class', 'play_url', $class_id);
                }
                $class_info = $GLOBALS['db']->getRow("select count(*) as num,sum(long_time) as long_time from " . DB_PREFIX . "edu_class where course_id = {$class['course_id']} and is_delete = 0");
                $root['error'] = '更新成功';
                $root['status'] = 1;
            }
        }
        $course_data = array(
            'courses_count' => $class_info['num'],
            'long_time' => $class_info['long_time'],
        );
        $GLOBALS['db']->autoExecute(DB_PREFIX . "edu_courses", $course_data, $mode = 'UPDATE', 'id =' . $course_id);
        $root['course_id'] = $course_id;
        api_ajax_return($root);
    }

    //删除课时
    public function del_class()
    {
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID
        $this->auth($user_id);

        $class_id = intval($_REQUEST['class_id']);
        $root = array();

        if ($class_id) {
            $class = $GLOBALS['db']->getRow("SELECT course_id FROM " . DB_PREFIX . "edu_class WHERE id =" . $class_id);
            $root['course_id'] = $class['course_id'];
            $data['is_delete'] = 1;
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_class", $data, $mode = 'UPDATE', 'id =' . $class_id)) {
                $root['error'] = '删除成功';
                $root['status'] = 1;
            } else {
                $root['error'] = '删除失败';
                $root['status'] = 0;
            }
        } else {
            $root['error'] = '信息错误';
            $root['status'] = 0;
        }
        api_ajax_return($root);
    }
}
