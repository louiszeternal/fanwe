<?php
// +----------------------------------------------------------------------
// | FANWE 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://www.fanwe.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 云淡风轻(88522820@qq.com)
// +----------------------------------------------------------------------
fanwe_require(APP_ROOT_PATH . 'mapi/lib/pay.action.php');
class payCModule extends payModule
{
    protected static function getUserId()
    {
        if (!$GLOBALS['user_info']['id']) {
            api_ajax_return(array(
                'error'             => '用户未登陆,请先登陆.',
                'status'            => 0,
                'user_login_status' => 0,
            ));
        }
        return intval($GLOBALS['user_info']['id']);
    }
    /**
     * 支付列表
     * @return [type] [description]
     */
    public function recharge()
    {
        $user_id = self::getUserId();

        $field    = 'id,name,class_name,logo';
        $table    = DB_PREFIX . 'payment';
        $where    = 'is_effect=1 and online_pay=1';
        $order    = 'sort';
        $pay_list = $GLOBALS['db']->getAll("SELECT $field FROM $table WHERE $where ORDER BY $order", 1, 1);

        $field     = 'id,name,money,(diamonds+gift_diamonds) as diamonds,iap_money';
        $table     = DB_PREFIX . 'recharge_rule';
        $where     = 'is_effect=1 and is_delete=0';
        $rule_list = $GLOBALS['db']->getAll("SELECT $field FROM $table WHERE $where ORDER BY $order", 1, 1);

        $m_config = load_auto_cache("m_config");

        api_ajax_return(array(
            'status'     => 1,
            'show_other' => intval($m_config['diamonds_rate']) > 0 ? 1 : 0,
            'pay_list'   => $pay_list,
            'rule_list'  => $rule_list,
            'rate'       => intval($m_config['diamonds_rate']),
        ));
    }
    public function pay()
    {
        $user_id = self::getUserId();
        $pay_id  = intval($_REQUEST['pay_id']);
        $rule_id = intval($_REQUEST['rule_id']);
        $money   = floatval($_REQUEST['money']);
        if ($pay_id <= 0) {
            ajax_return(array(
                'error'  => '支付id无效',
                'status' => 0,
            ));
        }
        if ($rule_id == 0 && $money == 0) {
            ajax_return(array(
                'error'  => '项目id无效或充值金额不能为0',
                'status' => 0,
            ));
        }
        $sql = "select id,name,class_name,logo from " . DB_PREFIX . "payment where is_effect = 1 and online_pay = 1 and id =" . $pay_id;
        $pay = $GLOBALS['db']->getRow($sql, true, true);

        if ($rule_id > 0) {
            $sql  = "select money,name,iap_money,product_id,(diamonds+gift_diamonds) as diamonds from " . DB_PREFIX . "recharge_rule where is_effect = 1 and is_delete = 0 and id =" . $rule_id;
            $rule = $GLOBALS['db']->getRow($sql, true, true);

            if ($pay['class_name'] == 'Iappay') {
                $money = $rule['iap_money'];
            } else {
                $money = $rule['money'];
            }

            $diamonds = $rule['diamonds'];

        } else if ($money > 0) {
            $m_config      = load_auto_cache("m_config");
            $diamonds_rate = intval($m_config['diamonds_rate']);
            $diamonds      = intval($money * $diamonds_rate);
        } else {
            $pay   = null;
            $money = 0;
        }
        if (!$pay || $money == 0) {
            ajax_return(array(
                'error'  => '支付id或 项目id无效',
                'status' => 0,
                'rule'   => $rule,
                'pay'    => $pay,
                'money'  => $money,
            ));
        }
        if ($pay['class_name'] != 'Iappay') {
            $payment_notice['create_time'] = NOW_TIME;
            $payment_notice['user_id']     = $user_id;
            $payment_notice['payment_id']  = $pay_id;
            $payment_notice['money']       = $money;
            $payment_notice['diamonds']    = $diamonds; //充值时,获得的钻石数量

            //$payment_notice['bank_id'] = '';//strim($_REQUEST['bank_id']);
            if ($rule_id > 0) {
                $payment_notice['recharge_id']   = $rule_id;
                $payment_notice['recharge_name'] = $rule['name'];
                $payment_notice['product_id']    = $rule['product_id'];
            } else {
                $payment_notice['recharge_name'] = '自定义充值';
            }

            do {
                $payment_notice['notice_sn'] = to_date(NOW_TIME, "YmdHis") . rand(100, 999);
                $GLOBALS['db']->autoExecute(DB_PREFIX . "payment_notice", $payment_notice, "INSERT", "", "SILENT");
                $notice_id = $GLOBALS['db']->insert_id();
            } while ($notice_id == 0);
        } else {
            $notice_id = $rule['product_id'];
        }

        $class_name = $pay['class_name'] . "_payment";
        // fanwe_require(APP_ROOT_PATH . "system/payment/" . $class_name . ".php");
        // $o   = new $class_name;
        // $pay = $o->get_payment_code($notice_id);

        // ajax_return(array(
        //     'status' => 1,
        //     'pay'    => $pay,
        // ));
        fanwe_require(APP_ROOT_PATH . "system/payment/" . $class_name . ".php");
        $o   = new $class_name();
        ajax_return($o->get_payment_code($notice_id));

    }
    public function exchange()
    {
        $root     = array('status' => 1, 'error' => '');
        $m_config = load_auto_cache("m_config"); //初始化手机端配置
        if (!$GLOBALS['user_info']) {
            $root['error']             = "用户未登陆,请先登陆.";
            $root['status']            = 0;
            $root['user_login_status'] = 0; //有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
        } else {
            $exchange_rules         = $GLOBALS['db']->getAll("select er.id,er.diamonds,er.ticket from " . DB_PREFIX . "exchange_rule as er where is_effect=1 and is_delete=0 order by er.diamonds");
            $root['exchange_rules'] = $exchange_rules;

            //$user =  $GLOBALS['db']->getRow("select ticket,diamonds from ".DB_PREFIX."user where id=".$GLOBALS['user_info']['id'],true,true);

            fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserRedisService.php');
            $user_redis = new UserRedisService();
            $user       = $user_redis->getRow_db($GLOBALS['user_info']['id'], array('ticket', 'diamonds', 'refund_ticket'));

            $GLOBALS['user_info']['ticket'] = intval($user['ticket']);
            $root['ticket']                 = intval($user['ticket']);

            $GLOBALS['user_info']['refund_ticket'] = intval($user['refund_ticket']);
            $root['refund_ticket']                 = intval($user['refund_ticket']); //已使用的印票

            $GLOBALS['user_info']['diamonds'] = intval($user['diamonds']);
            $root['diamonds']                 = intval($user['diamonds']);
            $root['useable_ticket']           = intval($user['ticket'] - $user['refund_ticket']);
            //兑换规则
            //$ratio = floatval(app_conf('TICKET_CATTY_RATIO'));
            $ratio         = $m_config['exchange_rate'];
            $root['ratio'] = $ratio;

            $m_config      = load_auto_cache("m_config");
            $exchange_rate = floatval($m_config['exchange_rate']);
            //兑换最低票数
            if ($exchange_rate > 0) {
                $min_ticket         = floatval(1 / $exchange_rate);
                $root['min_ticket'] = $min_ticket;
            } else {
                $root['min_ticket'] = 0;
            }
        }
        api_ajax_return($root);
    }
    public function do_exchange()
    {
        $user_id = self::getUserId();
        $rule_id = intval($_REQUEST['rule_id']);
        $ticket  = intval($_REQUEST['ticket']);

        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserRedisService.php');
        $user_redis = new UserRedisService();
        $user       = $user_redis->getRow_db($user_id, array('ticket', 'diamonds', 'refund_ticket'));
        $m_config   = load_auto_cache("m_config");

        if ($rule_id) {
            $exchange_rule = $video_new = $GLOBALS['db']->getRow("select er.* from " . DB_PREFIX . "exchange_rule as er where is_effect=1 and is_delete=0 and id = " . $rule_id);
            if ($exchange_rule) {
                $diamonds = intval($exchange_rule['diamonds']);

                $ticket = intval($exchange_rule['ticket']);
            } else {
                ajax_return(array(
                    'status'        => 0,
                    'error'         => '兑换出错',
                    'ticket'        => $user['ticket'],
                    'refund_ticket' => $user['refund_ticket'],
                    'diamonds'      => $user['diamonds'],
                ));
            }
        } else if ($ticket) {
            $ratio    = $m_config['exchange_rate'];
            $diamonds = intval($ticket * $ratio);
        } else {
            ajax_return(array(
                'status' => 0,
                'error'  => '参数错误',
            ));
        }
        if ($user['ticket'] - $user['refund_ticket'] < $ticket) {
            ajax_return(array(
                'status'        => 0,
                'error'         => $m_config['ticket_name'] . '不足',
                'ticket'        => $user['ticket'],
                'refund_ticket' => $user['refund_ticket'],
                'diamonds'      => $user['diamonds'],
            ));
        }
        if ($diamonds > 0) {
            //使用兑换列表的值
            $table = DB_PREFIX . 'user';
            $GLOBALS['db']->query("UPDATE $table set refund_ticket = refund_ticket + $ticket , diamonds = diamonds + $diamonds where ticket >= refund_ticket + $ticket and id = $user_id");
            if ($GLOBALS['db']->affected_rows()) {
                //redis 更新信息
                user_deal_to_reids(array($user_id));
                $exchange_log = array(
                    'user_id'     => $user_id,
                    'rule_id'     => $rule_id,
                    'diamonds'    => $diamonds,
                    'ticket'      => $ticket,
                    'create_time' => get_gmtime(),
                    'is_success'  => 1,
                );
                $GLOBALS['db']->autoExecute(DB_PREFIX . "exchange_log", $exchange_log, "INSERT", "", "SILENT");
                //写入用户日志
                $data = array(
                    'diamonds' => intval($diamonds),
                    'ticket'   => intval($ticket),
                );
                $ticket_name = $m_config['ticket_name'] != '' ? $m_config['ticket_name'] : '印票';
                $log_msg     = $ticket . $ticket_name . '兑换成' . $diamonds . '钻石';
                //类型 0表示充值 1表示提现 2赠送道具 3 兑换印票
                account_log_com($data, $user_id, $log_msg, array('type' => 3));
            } else {
                $GLOBALS['db']->autoExecute(DB_PREFIX . "exchange_log", array('is_success' => 0), "INSERT", "", "SILENT");
                ajax_return(array(
                    'status'        => 0,
                    'error'         => '兑换失败',
                    'ticket'        => $user['ticket'],
                    'refund_ticket' => $user['refund_ticket'],
                    'diamonds'      => $user['diamonds'],
                ));
            }
        }
        $user = $user_redis->getRow_db($user_id, array('ticket', 'diamonds', 'refund_ticket'));
        ajax_return(array(
            'status'         => 1,
            'error'          => '兑换成功',
            'ticket'         => $user['ticket'],
            'refund_ticket'  => $user['refund_ticket'],
            'diamonds'       => $user['diamonds'],
            'useable_ticket' => intval($user['ticket'] - $user['refund_ticket']),
        ));
    }

    private function get_vip_type($type)
    {
        if (!$type) {
            return;
        }

        return $type == 1 ? 'VIP' : 'VVIP';
    }

    public function vip()
    {
        $vip_rules = load_auto_cache('hk_vip_rule_list', ['type' => 1]);
        $vvip_rules = load_auto_cache('hk_vip_rule_list', ['type' => 2]);

        if ($GLOBALS['user_info']['id']) {
            $user_vip = get_user_vip($GLOBALS['user_info']['id']);
            $discount_diamonds = get_hk_vip_discount_diamonds($user_vip['vip_type'], $user_vip['vip_expire_time']);
            $vip_type = $this->get_vip_type($user_vip['vip_type']);
            $vip_expire_time = to_date($user_vip['vip_expire_time'], 'Y-m-d');
        } else {
            $discount_diamonds = 0;
            $vip_type = '';
            $vip_expire_time = 0;
        }

        api_ajax_return([
            "vip_rules" => $vip_rules,
            "vvip_rules" => $vvip_rules,
            "discount_diamonds" => $discount_diamonds,
            "vip_type" => $vip_type,
            "vip_expire_time" => $vip_expire_time,
        ]);
    }

    //VIP付费
    public function vip_pay()
    {
        $user_id = self::getUserId();
        $id = intval($_REQUEST['id']);

        if ($id <= 0) {
            api_ajax_return(array(
                'error' => 'VIP付费规则id无效',
                'status' => 0,
            ));
        }

        $sql = "select diamonds,month_num,type from " . DB_PREFIX . "hk_vip_rule where id = {$id}";
        $vip_rule = $GLOBALS['db']->getRow($sql, true, true);

        if (!$vip_rule) {
            api_ajax_return(array(
                'error' => '该特权规则不存在',
                'status' => 0,
            ));
        }

        $pay_diamonds = $vip_rule['diamonds'];
        $user_vip = get_user_vip($user_id);
        // 如果会员还未过期
        if ($user_vip['vip_expire_time'] > NOW_TIME) {
            if ($user_vip['vip_type'] > $vip_rule['type']) {
                api_ajax_return(array(
                    'error' => '已购买更高等级VIP，无法降级',
                    'status' => 0,
                ));
            } elseif ($user_vip['vip_type'] < $vip_rule['type']) {
                // 升级过期时间重置
                $pay_diamonds -= get_hk_vip_discount_diamonds($user_vip['vip_type'], $user_vip['vip_expire_time']);
                $user_vip['vip_expire_time'] = NOW_TIME;
            }
        } else {
            $user_vip['vip_expire_time'] = NOW_TIME;
        }

        $key = "pay_vip_user_{$user_id}";
        $is_ok = $GLOBALS['cache']->set_lock($key);
        if (!$is_ok) {
            api_ajax_return(array(
                'error' => '不需要重复请求',
                'status' => 0,
            ));
        }

        $expire_time = strtotime("+{$vip_rule['month_num']} months", $user_vip['vip_expire_time']);

        $sql = "UPDATE " . DB_PREFIX . "user
SET vip_expire_time = {$expire_time} ,
    vip_type = {$vip_rule['type']},
 diamonds = diamonds - {$pay_diamonds}
WHERE
	id = {$user_id}
AND diamonds >= {$pay_diamonds}";
        $GLOBALS['db']->query($sql);

        $GLOBALS['cache']->del_lock($key);

        if (!$GLOBALS['db']->affected_rows()) {
            api_ajax_return(array(
                'error' => '钻石余额不足，请充值',
                'status' => 0,
            ));
        }

        //写入用户日志
        $data = array();
        $data['diamonds'] = $pay_diamonds;
        $param['type'] = 40;//类型 0表示充值 1表示提现 2赠送道具 3 兑换印票 30 购买VIP消费
        $log_msg = '购买VIP,抵扣' . ($vip_rule['diamonds'] - $pay_diamonds);//备注
        account_log_com($data, $user_id, $log_msg, $param);

        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserRedisService.php');
        user_deal_to_reids(array($user_id));
        $user_redis = new UserRedisService();
        $account_diamonds = $user_redis->getOne_db($user_id, 'diamonds');
        api_ajax_return(array(
            'status' => 1,
            'error' => 'VIP支付成功',
            'rest_money' => $account_diamonds,
            'vip_expire_time' => to_date($expire_time, 'Y-m-d'),
        ));
    }

    public function download()
    {
        if (!$GLOBALS['user_info']['id']) {
            api_ajax_return([
                'error' => '用户未登陆,请先登陆.',
                'status' => 0,
                'user_login_status' => 0,
            ]);
        }

        $user_id = $GLOBALS['user_info']['id'];
        $id = intval($_REQUEST['id']);
        $type = strim($_REQUEST['type']);
        $video = get_hk_video_history($id);
        if (empty($video)) {
            api_ajax_return(array(
                'error' => '视频不存在',
                'status' => 0,
            ));
        }

        $user = get_user_vip($user_id);
        switch ($type) {
            case 'hd':
                $pay_diamonds = $user['vip_type'] >= $video['down_level_hd'] ? 0 : $video['price_hd'];
                $play_url = $video['play_url_hd'];
                break;
            case 'sd':
                $pay_diamonds = $user['vip_type'] >= $video['down_level_sd'] ? 0 : $video['price_sd'];
                $play_url = $video['play_url_sd'];
                break;
            case 'ori':
                $pay_diamonds = $user['vip_type'] >= $video['down_level'] ? 0 : $video['price'];
                $play_url = $video['play_url'];
                break;
            default:
                throw new Exception;
        }

        if (empty($play_url)) {
            api_ajax_return(array(
                'error' => '视频不存在',
                'status' => 0,
            ));
        }

        if ($pay_diamonds > 0) {
            $already_pay = $GLOBALS['db']->getOne("select count(*) from " . DB_PREFIX . "hk_video_download where user_id = {$user_id} and video_id = {$id} and type = '{$type}'");
            if (!$already_pay) {
                $sql = "UPDATE " . DB_PREFIX . "user
SET diamonds = diamonds - {$pay_diamonds}
WHERE
	id = {$user_id}
AND diamonds >= {$pay_diamonds}";
                $GLOBALS['db']->query($sql);

                if (!$GLOBALS['db']->affected_rows()) {
                    api_ajax_return(array(
                        'error' => '钻石余额不足，请充值',
                        'status' => 0,
                    ));
                }

                $GLOBALS['db']->autoExecute(DB_PREFIX . 'hk_video_download', [
                    'user_id' => $user_id,
                    'video_id' => $id,
                    'type' => $type,
                    'create_time' => NOW_TIME,
                ]);

                $GLOBALS['db']->query("update " . DB_PREFIX . "hk_video_history set down_num = down_num + 1 where id = " . $id);

                //写入用户日志
                $data = array();
                $data['diamonds'] = $pay_diamonds;
                $param['type'] = 31;//类型 0表示充值 1表示提现 2赠送道具 3 兑换印票 30 购买VIP消费 31 下载视频
                $log_msg = '下载视频';//备注
                account_log_com($data, $user_id, $log_msg, $param);

                fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserRedisService.php');
                user_deal_to_reids(array($user_id));
            }
        }

        api_ajax_return([
            'status' => '1',
            'url' => $play_url,
        ]);
    }
}
