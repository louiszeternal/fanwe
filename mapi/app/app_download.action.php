<?php
// +----------------------------------------------------------------------
// | Fanwe 方维o2o商业系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://www.fanwe.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 云淡风轻(97139915@qq.com)
// +----------------------------------------------------------------------

fanwe_require(APP_ROOT_PATH.'mapi/lib/app_download.action.php');
class app_downloadCModule  extends app_downloadModule
{
	public function index()
	{
		$root = array();
		$list = array();
		$root['status'] = 1;

		$m_config = load_auto_cache('m_config');
		$ios_down_url = $m_config['ios_down_url'];
		$android_down_url = $m_config['android_filename'];
		
		$has_ios = $ios_down_url?1:0;
		$has_android = $android_down_url?1:0;
		

		$list = array(
			"ios" =>array(
				'has_ios'=>$has_ios,
				'ios_down_url'=>$ios_down_url,
			),
			"android" =>array(
				'has_android'=>$has_android,
				'android_down_url'=>$android_down_url,
			),
			"wechat" =>array(
				'has_wechat'=>1,
				'url'=>SITE_DOMAIN.'/mapi/index.php?ctl=app_download',
			)
		);
		$root['list'] = $list;
		$root['obs_download'] = empty($m_config['obs_download']) ? add_domain_url('./public/attachment/attachment/fanwe-obs-releases-1.0.2-x86.zip') : $m_config['obs_download'];
		$root['page_title'] = 'APP下载';
		api_ajax_return($root);
	}	
	
}
?>