<?php
// +----------------------------------------------------------------------
// | FANWE 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://www.fanwe.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 云淡风轻(88522820@qq.com)
// +----------------------------------------------------------------------
fanwe_require(APP_ROOT_PATH . 'mapi/app/page.php');
fanwe_require(APP_ROOT_PATH . 'mapi/lib/video.action.php');

class videoCModule extends videoModule
{
    /**
     * 当前房间用户列表（包括机器人，但不包括虚拟人数）
     */
    public function viewer()
    {
        $root = array();
        $group_id = strim($_REQUEST['group_id']); //聊天群id
        $page = intval($_REQUEST['p']); //取第几页数据
        $root = load_auto_cache("video_viewer", array('group_id' => $group_id, 'page' => $page));


        if (OPEN_PAI_MODULE == 1 && $page == 1) {
            //增加竞拍排序
            $sql = "select pai_id from " . DB_PREFIX . "video where group_id = '" . $group_id . "'";
            $video['pai_id'] = $GLOBALS['db']->getOne($sql);
            if (intval($video['pai_id']) > 0) {
                $user_list = $GLOBALS['db']->getAll("SELECT user_id,pai_status,order_id,order_status,pai_diamonds FROM " . DB_PREFIX . "pai_join WHERE pai_id=" . $video['pai_id'] . " ORDER BY pai_diamonds DESC limit 0,5");
                //log_result("SELECT user_id,pai_status,order_id,order_status,pai_diamonds FROM ".DB_PREFIX."pai_join WHERE pai_id=".$video['pai_id']." ORDER BY pai_diamonds DESC ");
                if ($user_list) {

                    fanwe_require(APP_ROOT_PATH . '/mapi/lib/redis/BaseRedisService.php');
                    fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserFollwRedisService.php');
                    fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserRedisService.php');
                    $user_redis = new UserRedisService();
                    $fields = array(
                        'is_authentication',
                        'head_image',
                        'user_level',
                        'v_type',
                        'v_icon',
                        'nick_name',
                        'signature',
                        'sex',
                        'province',
                        'city',
                        'thumb_head_image',
                        'v_explain',
                        'emotional_state',
                        'job',
                        'birthday',
                        'apns_code'
                    );

                    foreach ($user_list as $k => $v) {

                        if (intval($v['user_id']) > 0) {
                            $user_list[$k] = $user_redis->getRow_db(intval($v['user_id']), $fields);
                            $user_list[$k]['user_id'] = intval($v['user_id']);
                            $user_list[$k]['type'] = intval($v['pai_status']);
                            $user_list[$k]['pai_diamonds'] = intval($v['pai_diamonds']);
                        }

                    }

                    $root['tag'] = 1;
                }
                $list_1 = $root['list'];
                foreach ($list_1 as $k => $v) {
                    foreach ($user_list as $k2 => $v2) {
                        if ($list_1[$k]['user_id'] == $v2['user_id']) {
                            unset($list_1[$k]);
                            break;
                        }

                    }
                }
                if (!$list_1) {
                    $list_1 = array();
                }
                if (!$user_list) {
                    $user_list = array();
                }
                //log_result("==user_list==");
                //log_result($user_list);
                //log_result("==list_1==");
                //log_result($list_1);
                $root['list'] = array_merge($user_list, $list_1);

                //log_result("==list_2==");
                //log_result($root['list']);
                //$root['list']=$user_list;
                //$root['has_next']=0;
                //$root['page']=1;
                //$root['status']=1;
                ajax_return($root);
            }
        }
        ajax_return($root);
        //$GLOBALS['user_info']['id'] = 1;

        $group_id = strim($_REQUEST['group_id']); //聊天群id
        $page = intval($_REQUEST['p']); //取第几页数据
        if ($page == 0) {
            $page = 1;
        }

        $page_size = 50;
        $limit = (($page - 1) * $page_size) . "," . $page_size;

        $sql = "select u.id as user_id,u.head_image,u.user_level,u.v_type,u.v_icon from " . DB_PREFIX . "video_viewer v left join " . DB_PREFIX . "user u on u.id = v.user_id where v.end_time = 0 and v.group_id = '" . $group_id . "' order by u.is_robot,u.user_level desc limit " . $limit;
        //$sql = "select u.id as user_id,u.head_image,u.user_level,u.v_type,u.v_icon from ".DB_PREFIX."user u order by u.user_level desc limit ".$limit;
        $list = $GLOBALS['db']->getAll($sql);
        //处理头像绝对路径
        foreach ($list as $k => $v) {
            $list[$k]['head_image'] = get_spec_image($v['head_image']);
            //$list[$k]['v_icon'] = get_abs_img_root($v['v_icon']);
        }
        $root['list'] = $list;
        //$root['sql'] = $sql;
        if (count($list) == $page_size) {
            $root['has_next'] = 1;
        } else {
            $root['has_next'] = 0;
        }

        //当前房间人数 = 当前实时观看人数（实际,不含虚拟人数,不包含机器人) + 当前虚拟观看人数 + 机器人
        //$root['viewer_num'] = $root['watch_number'] + $root['virtual_watch_number'] + $root['robot_num'];
        $sql = "select (watch_number + robot_num+ virtual_watch_number) as num from " . DB_PREFIX . "video where group_id = '" . $group_id . "'";
        //$sql = "select count(*) from ".DB_PREFIX."user";
        $root['watch_number'] = $GLOBALS['db']->getOne($sql);
        $root['page'] = $page; //
        $root['status'] = 1;

        //print_r($root);exit;
        //log_result($root);
        ajax_return($root);
    }

    /*
     * 直播详情
     */
    function get_video()
    {
        if (!$GLOBALS['user_info']['id']) {
            //有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            ajax_return(array('error' => '用户未登陆,请先登陆.', 'status' => 0, 'user_login_status' => 0));
        }
        //$user_id = intval($GLOBALS['user_info']['id']);//登陆用户ID
        $room_id = intval($_REQUEST['room_id']); //房间号id
        $is_vod = intval($_REQUEST['is_vod']); //0:观看直播;1:点播

        $table = DB_PREFIX . ($is_vod ? 'video_history' : 'video');
        $where = ($is_vod ? "live_in=0 and id=" : "id=") . $room_id;
        $video = $GLOBALS['db']->getRow("SELECT id as roome_id,group_id,watch_number,video_vid,room_type,vote_number,channelid,live_in,cate_id,user_id FROM $table WHERE $where",
            true, true); //查找是否有此房间
        if (!$video) {
            ajax_return(array('status' => 10001));
        }

        $cate_info = $GLOBALS['db']->getRow("SELECT id as cate_id,title FROM " . DB_PREFIX . "video_cate WHERE id=" . $video['cate_id'],
            true, true);
        unset($video['cate_id']);

        $user_info = $GLOBALS['db']->getRow("SELECT id,nick_name,signature,is_authentication,province,city,sex,user_level,head_image,thumb_head_image,v_type,v_icon,fans_count FROM " . DB_PREFIX . "user WHERE id=" . $video['user_id'],
            true, true);
        unset($video['user_id']);

        //播放地址
        $video_vid_list = array();
        $video_vid = get_vodset_by_video_id($room_id);
        foreach ($video_vid['vodset'] as $k => $v) {
            foreach ($v['fileSet'][0]['playSet'] as $kk => $vv) {
                if (strpos($vv['url'], 'f0.mp4')) {
                    $video_vid_list[$k]['url'] = $vv['url'];
                }
            }
            $video_vid_list[$k]['image_url'] = get_spec_image($v['fileSet'][0]['image_url']);
            $video_vid_list[$k]['fileId'] = $v['fileSet'][0]['fileId'];
        }
        $video['video_list'] = $video_vid_list; //视频数据

        $gift_list = $GLOBALS['db']->getAll("SELECT id,name,score,diamonds,icon,ticket,is_much,sort,is_red_envelope,is_animated,anim_type,robot_diamonds FROM " . DB_PREFIX . "prop WHERE is_effect=1",
            true, true);

        ajax_return(array(
            'cate_info' => $cate_info,//话题信息
            'user_info' => $user_info,//直播信息
            'video' => $video,//视频信息
            'page_title' => $user_info['nick_name'] . "的直播间",//标题***的直播间
            'gift_list' => $gift_list,//礼物列表
            'status' => 1,
        ));
    }

    //PC端全部直播接口
    public function video_list()
    {
        $root = array();
        $root['page_title'] = "全部直播";
        $p = intval($_REQUEST['p']);//页码
        $cate_id = intval($_REQUEST['cate_id']);//话题ID
        $is_recommend = intval($_REQUEST['is_recommend']);//推荐列表
        $is_hot = intval($_REQUEST['is_hot']);//热门列表
        $is_new = intval($_REQUEST['is_new']);//最新列表
        $is_family_hot = intval($_REQUEST['is_family_hot']);//热门家族列表
        $change_new = intval($_REQUEST['change_new']);//换一换
        $is_hot_cate = intval($_REQUEST['is_hot_cate']);//热门话题
        $jump_type = intval($_REQUEST['jump_type']);//热门话题
        $page_size = 12;//分页数量
        if ($p == 0 || $p == '') {
            $p = 1;
        }
        if ($jump_type == 1) {
            $this->live_list($jump_type, $cate_id);
        } elseif ($jump_type == 2) {
            $this->focus_list($jump_type);
        } elseif ($jump_type == 3) {
            $this->history_list($jump_type);
        }
        $param = array('page' => $p, 'page_size' => $page_size);

        $m_config = load_auto_cache("m_config");//初始化手机端配置
        $has_is_authentication = intval($m_config['has_is_authentication']) ? 1 : 0;
        if ($has_is_authentication && $m_config['ios_check_version'] == '') {
            $countsql = "SELECT count(v.id) FROM " . DB_PREFIX . "video v
			LEFT JOIN " . DB_PREFIX . "user u ON u.id = v.user_id where v.live_in in (1,3) and u.is_authentication = 2 ";
        } elseif ($GLOBALS['user_info']) {
            $countsql = "SELECT count(v.id) from " . DB_PREFIX . "video v where v.live_in in (1,3) and v.room_type in (1,3)";
        } else {
            $countsql = "SELECT count(v.id) from " . DB_PREFIX . "video v where v.live_in in (1,3) and v.room_type = 3 ";
        }
        if ($cate_id) {
            $countsql .= " and  v.cate_id = " . $cate_id;
        }
        if ($is_new) {
            $param['is_new'] = $is_new;
            $param['cate_id'] = $cate_id;
            $root['cate_top'] = load_auto_cache("cate_top");
            $list = load_auto_cache("selectpc_video", $param);
            $root['list'] = $list;
            $root['type'] = "is_new";
        } elseif ($is_recommend) {
            $countsql .= ' and v.is_recommend = ' . $is_recommend;
            $param['is_recommend'] = $is_recommend;
            $list = load_auto_cache("selectpc_video", $param);
            $root['list'] = $list;
            $root['type'] = "is_recommend";
        } elseif ($is_family_hot) {
            $countsql = "SELECT count(v.id) FROM (SELECT family_id FROM " . DB_PREFIX . "family_join GROUP BY family_id ORDER BY count(*) DESC) f, " . DB_PREFIX . "video v LEFT JOIN " . DB_PREFIX . "user u ON u.id = v.user_id where u.family_id = f.family_id and u.family_id >0 and v.live_in in (1,3) and v.room_type = 3 ";
            $param['is_family_hot'] = $is_family_hot;
            $list = load_auto_cache("selectpc_video", $param);
            $root['list'] = $list;
            $root['type'] = "is_family_hot";
        } elseif ($is_hot) {
            $param['is_hot'] = $is_hot;
            $list = load_auto_cache("selectpc_video", $param);
            $root['list'] = $list;
            $root['type'] = "is_hot";
        } else {
            $param['page_size'] = 12;//分页数量
            $page_size = 12;
            $param['cate_id'] = $cate_id;
            if ($change_new) {
                $param['change'] = 1;
                $list = load_auto_cache("new_list", $param);
            } else {
                $list = load_auto_cache("new_list");
            }
            if ($is_hot_cate) {
                $param['is_hot_cate'] = 1;
            }
            $root['new_list'] = $list;
            $root['cate_top'] = load_auto_cache("cate_top");
            $info = load_auto_cache("all_video", $param);
            $root['list'] = $info['list'];
        }

        // 广告列表
        $place_id = 2;
        $root['ad_list'] = load_auto_cache("ad_list", $place_id);

        $rs_count = $GLOBALS['db']->getOne($countsql, true, true);
        $page = new Page($rs_count, $page_size);   //初始化分页对象
        $root['page'] = $page->show();
        $root['cate_id'] = $cate_id;
        $root['status'] = empty($root) ? 0 : 1;
        $root['jump_type'] = $jump_type;
        $root['qq_wpa_key'] = $m_config['qq_wpa_key'];
        api_ajax_return($root);
    }

    public function live_list($jump_type, $cate_id)
    {
        $p = intval($_REQUEST['p']);//页码
        $page_size = 12;
        $cate_id = empty($_REQUEST['cate_id']) ? $cate_id : intval($_REQUEST['cate_id']);//话题ID
        if (empty($jump_type)) {
            $jump_type = 1;
        }
        $param = array('page' => $p, 'page_size' => $page_size, 'cate_id' => $cate_id);
        $info = load_auto_cache("all_video", $param);
        $countsql = "SELECT count(id) from " . DB_PREFIX . "video v where v.live_in in (1,3) and v.room_type = 3 ";
        if ($cate_id) {
            $countsql .= " and  v.cate_id = " . $cate_id;
        }
        $rs_count = $GLOBALS['db']->getOne($countsql, true, true);
        $page = new Page($rs_count, $page_size);
        $cate_top = load_auto_cache("cate_top");
        $m_config = load_auto_cache("m_config");//初始化手机端配置
        $root = array(
            'list' => $info['list'],
            'page' => $page->show(),
            'jump_type' => $jump_type,
            'cate_top' => $cate_top,
            'cate_id' => $cate_id,
            'qq_wpa_key' => $m_config['qq_wpa_key']
        );
        api_ajax_return($root);
    }

    public function new_list()
    {
        $param = array('change' => 1);
        $list = load_auto_cache("new_list", $param);
        $root = array('new_list' => $list);
        api_ajax_return($root);
    }

    public function history_list($jump_type)
    {
        if (!$GLOBALS['user_info']) {
            $root = array(
                "error" => "用户未登陆,请先登陆.",
                "status" => 0,
                "user_login_status" => 0,//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            );
            api_ajax_return($root);
        }
        if (empty($jump_type)) {
            $jump_type = 3;
        }
        $p = intval($_REQUEST['p']);//页码
        $page_size = 12;
        $user_id = intval($GLOBALS['user_info']['id']);//用户ID

        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserViewHistoryRedisService.php');
        $history_redis = new UserViewHistoryRedisService($user_id);
        $room_id = intval($_REQUEST['del_room_id']);
        if ($room_id > 0) {
            $history_redis->remove($room_id);
        }

        $list = $history_redis->get_history($p, $page_size);
        $rs_count = $history_redis->count();
        $m_config = load_auto_cache("m_config");//初始化手机端配置
        $page = new Page($rs_count, $page_size);
        $root = array(
            'list' => $list,
            'page' => $page->show(),
            'jump_type' => $jump_type,
            'qq_wpa_key' => $m_config['qq_wpa_key']
        );
        api_ajax_return($root);
    }

    public function focus_list($jump_type)
    {
        if (!$GLOBALS['user_info']) {
            $root = array(
                "error" => "用户未登陆,请先登陆.",
                "status" => 0,
                "user_login_status" => 0,//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            );
            api_ajax_return($root);
        }
        if (empty($jump_type)) {
            $jump_type = 2;
        }
        $p = intval($_REQUEST['p']);//页码
        $user_id = intval($GLOBALS['user_info']['id']);//登录用户id
        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserFollwRedisService.php');
        $userfollw_redis = new UserFollwRedisService($user_id);
        $user_list = $userfollw_redis->following();
        //私密直播  video_private,私密直播结束后， 本表会清空
        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoPrivateRedisService.php');
        $video_private_redis = new VideoPrivateRedisService();
        $private_list = $video_private_redis->get_video_list($user_id);
        $list = array();
        $param = array('has_private' => 1);
        if (sizeof($private_list) || sizeof($user_list)) {
            $info = load_auto_cache("focus_video", $param);
            $list_all = $info;
            foreach ($list_all as $k => $v) {
                if (($v['room_type'] == 1 && in_array($v['room_id'],
                            $private_list)) || ($v['room_type'] == 3 && in_array($v['user_id'], $user_list))
                ) {
                    $list[] = $v;
                }
            }
        }

        if ($p < 1) {
            $p = 1;
        }
        $page_size = 18;
        $start = ($p - 1) * $page_size;
        $new_list = array_slice($list, $start, $page_size);
        $page = new Page(count($list), $page_size);   //初始化分页对象
        $root = array('list' => $new_list, 'page' => $page->show(), 'jump_type' => $jump_type);
        api_ajax_return($root);
    }

    //我的关注
    public function focus_video()
    {
        $root = array();
        $page = intval($_REQUEST['p']);//取第几页数据
        $cateid = intval($_REQUEST['cate_id']);
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
        } else {
            //关注
            $user_id = intval($GLOBALS['user_info']['id']);//登录用户id
            fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserFollwRedisService.php');
            $userfollw_redis = new UserFollwRedisService($user_id);
            $user_list = $userfollw_redis->following();
            //私密直播  video_private,私密直播结束后， 本表会清空
            fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoPrivateRedisService.php');
            $video_private_redis = new VideoPrivateRedisService();
            $private_list = $video_private_redis->get_video_list($user_id);
            $list = array();
            if ($page == 0 || $page == '') {
                $page = 1;
            }
            $page_size = 10;
            $start = ($page - 1) * $page_size;
            $param = array('has_private' => 1, 'cate_id' => $cateid);
            if (sizeof($private_list) || sizeof($user_list)) {
                $info = load_auto_cache("focus_video", $param);
                $list_all = $info;
                foreach ($list_all as $k => $v) {
                    if (($v['room_type'] == 1 && in_array($v['room_id'],
                                $private_list)) || ($v['room_type'] == 3 && in_array($v['user_id'], $user_list))
                    ) {
                        $list[] = $v;
                    }
                }
            }
            $new_list = array_slice($list, $start, $page_size);

            $root['list'] = $new_list;
            $root['cate_top'] = load_auto_cache("cate_top");
            $page = new Page(count($list), $page_size);   //初始化分页对象
            $root['status'] = 1;
            $root['page'] = $page->show();
        }
        api_ajax_return($root);
    }

    //判断是否为主播
    public function is_podcast($room_id)
    {
        if (!$room_id) {
            return false;
        }
        $user_id = $GLOBALS['user_info']['id'];
        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoRedisService.php');
        $video_redis = new VideoRedisService();
        $video = $video_redis->getRow_db($room_id, array('id', 'user_id', 'group_id'));
        if ($user_id != $video['user_id']) {
            $root['error'] = "您不是该房间的主播，没有权限";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        return true;
    }

    //用户列表
    public function viewer_list()
    {
        $root = array();
        $page = intval($_REQUEST['p']);//取第几页数据
        $room_id = intval($_REQUEST['room_id']);
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        } else {
            if (defined('CHILD_ROOM') && CHILD_ROOM == 1) {
                $user_id = $GLOBALS['user_info']['id'];
                $this->is_podcast($room_id);
                $to_user_id = intval($_REQUEST['user_id']);
                $where = '';
                if ($to_user_id) {
                    $where = 'user_id =' . $to_user_id . ' AND';
                }

                $list = array();
                if ($page == 0 || $page == '') {
                    $page = 1;
                }
                $page_size = 10;
                $limit = (($page - 1) * $page_size) . "," . $page_size;

                fanwe_require(APP_ROOT_PATH . 'mapi/lib/ChildRoom.class.php');
                $child_room = new child_room();
                fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoRedisService.php');
                $video_redis = new VideoRedisService();
                $parent_id = $child_room->parent_id($room_id);
                $video = $video_redis->getRow_db($room_id, array('id', 'user_id', 'group_id','live_in'));
                $p_video = $video_redis->getRow_db($parent_id, array('id', 'user_id', 'group_id'));
                if($video && $video['live_in'] ==3){
                    $group_id = $video['group_id'];
                }else{
                    $group_id = $p_video['group_id'];
                }


                if ($child_room->is_child_room($room_id)) {
                    $list = $child_room->get_viewer_list($room_id, $limit, $where);
                    if (!empty($list['viewer_user'])) {
                        foreach ($list['viewer_user'] as &$item) {
                            $user = $GLOBALS['db']->getRow("SELECT id,nick_name,head_image,thumb_head_image,sex,mobile FROM " . DB_PREFIX . "user WHERE id =" . $item['user_id']);
                            if (empty($user['head_image'])) {
                                $item['head_image'] = get_spec_image($user['thumb_head_image']);
                            } else {
                                $item['head_image'] = get_spec_image($user['head_image']);
                            }
                            if ($user['sex']) {
                                $item['sex'] = $user['sex'] > 1 ? '女' : '男';
                            } else {
                                $item['sex'] = '未知';
                            }
                            $item['mobile'] = $user['mobile'] ? $user['mobile'] : '未知';
                            $item['nick_name'] = $user['nick_name'];
                            //优化
                            $forbid_info = $video_redis->has_forbid_msg($group_id, $item['user_id']);
                            if ($forbid_info) {
                                $item['is_nospeaking'] = 1;
                            } else {
                                $item['is_nospeaking'] = 0;
                            }
                            $sql = "select id from " . DB_PREFIX . "user_admin where podcast_id = '" . $user_id . "' and user_id = " . $item['user_id'];
                            $user_admin_id = $GLOBALS['db']->getOne($sql);
                            if ($user_admin_id > 0) {
                                $item['is_admin'] = 1;
                            } else {
                                $item['is_admin'] = 0;
                            }
                        }
                    }
                } else {
                    fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoViewerRedisService.php');
                    $video_viewer_redis = new VideoViewerRedisService();
                    $list = $video_viewer_redis->get_viewer_list($group_id, $page, $page_size);
                    if (!empty($list['list'])) {
                        foreach ($list['list'] as &$value) {
                            if ($value['is_robot'] == 1) {
                                unset($value);
                                continue;
                            }
                            $user = $GLOBALS['db']->getRow("SELECT id,nick_name,head_image,thumb_head_image,sex,mobile FROM " . DB_PREFIX . "user WHERE id =" . $value['user_id']);

                            $value['head_image'] = get_spec_image($value['head_image']);
                            if ($value['sex']) {
                                $value['sex'] = $value['sex'] > 1 ? '女' : '男';
                            } else {
                                $value['sex'] = '未知';
                            }
                            $value['mobile'] = $user['mobile'] ? $user['mobile'] : '未知';

                            //优化
                            $forbid_info = $video_redis->has_forbid_msg($group_id, $value['user_id']);
                            if ($forbid_info) {
                                $value['is_nospeaking'] = 1;
                            } else {
                                $value['is_nospeaking'] = 0;
                            }
                            $sql = "select id from " . DB_PREFIX . "user_admin where podcast_id = '" . $user_id . "' and user_id = " . $value['user_id'];
                            $user_admin_id = $GLOBALS['db']->getOne($sql);
                            if ($user_admin_id > 0) {
                                $value['is_admin'] = 1;
                            } else {
                                $value['is_admin'] = 0;
                            }
                            $list['viewer_user'][] = $value;
                        }
                    }
                }

                $page = new Page($list['rs_count'], $page_size);   //初始化分页对象
                $list['page'] = $page->show();
                $list['error'] = "";
                $list['status'] = 1;
                $list['room_id'] = $room_id;
                api_ajax_return($list);
            } else {
                $root['error'] = "系统错误";
                $root['status'] = 0;
                api_ajax_return($root);
            }
        }
    }

    //礼物列表
    public function prop_list()
    {
        $root = array();
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $page = intval($_REQUEST['p']);//取第几页数据
        $room_id = intval($_REQUEST['room_id']);
        $is_history = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM " . DB_PREFIX . "video WHERE id =" . $room_id);
        $this->is_podcast($room_id);
        if ($is_history > 0) {
            $table = DB_PREFIX . "video";
        } else {
            $table = DB_PREFIX . "video_history";
        }
        $prop_table = $GLOBALS['db']->getOne("SELECT prop_table FROM $table WHERE id =" . $room_id);

        if (!isset($_REQUEST['prop_id'])) {
            $_REQUEST['prop_id'] = -1;
        }
        $root['prop_list'] = load_auto_cache("prop_list");

        $where = '';
        $prop_id = intval($_REQUEST['prop_id']);
        if ($_REQUEST['prop_id'] != -1) {
            $where .= "l.prop_id=" . intval($_REQUEST['prop_id']) . " and ";
        }

        if ($page == 0 || $page == '') {
            $page = 1;
        }
        $page_size = 10;
        $limit = (($page - 1) * $page_size) . "," . $page_size;

        $where .= " video_id =" . $room_id;
        $sql = "SELECT l.id,l.create_ym,l.to_user_id, l.create_time,l.prop_id,l.prop_name,l.from_user_id,l.create_date,l.num,l.total_ticket,u.nick_name,u.head_image,l.from_ip
                         FROM {$prop_table} as l
                         LEFT JOIN " . DB_PREFIX . "user AS u  ON l.from_user_id = u.id" . " LEFT JOIN " . DB_PREFIX . "prop AS v ON l.prop_name = v.name" . "
                         WHERE $where limit $limit ";

        $count_sql = "SELECT count(*)
                         FROM   {$prop_table} as l
                         LEFT JOIN " . DB_PREFIX . "user AS u  ON l.from_user_id = u.id" . " LEFT JOIN " . DB_PREFIX . "prop AS v ON l.prop_name = v.name" . "
                         WHERE $where ";
        $list = $GLOBALS['db']->getAll($sql, true, true);
        $rs_count = $GLOBALS['db']->getOne($count_sql, true, true);

        if (empty($list)) {
            $root['error'] = "暂无数据";
            $root['status'] = 0;
            api_ajax_return($root);
        }

        $page = new Page($rs_count, $page_size);   //初始化分页对象
        $root['page'] = $page->show();
        foreach ($list as $k => $v) {
            $list[$k]['head_image'] = get_spec_image($v['head_image']);
            if ($list[$k]['prop_id'] == 12) {
                $list[$k]['total_ticket'] = '';
            }
            $list[$k]['create_time'] = date('Y-m-d', $list[$k]['create_time']);
            $list[$k]['nick_name'] = emoji_decode($list[$k]['nick_name']);
        }

        $root['error'] = "";
        $root['status'] = 1;
        $root['list'] = $list;
        $root['room_id'] = $room_id;
        $root['prop_id'] = $prop_id;

        api_ajax_return($root);
    }

    //付费列表
    public function live_pay_list()
    {
        $root = array();
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $page = intval($_REQUEST['p']);//取第几页数据
        $room_id = intval($_REQUEST['room_id']);
        $is_history = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM " . DB_PREFIX . "video WHERE id =" . $room_id);
        $this->is_podcast($room_id);

        if ($is_history > 0) {
            $table = DB_PREFIX . "live_pay_log";
        } else {
            $table = DB_PREFIX . "live_pay_log_history";
        }

        if ($page == 0 || $page == '') {
            $page = 1;
        }
        $page_size = 10;
        $limit = (($page - 1) * $page_size) . "," . $page_size;

        $where = "video_id =" . $room_id;

        $sql = "SELECT l.id ,l.video_id,l.create_ym,l.to_user_id,l.create_time,l.from_user_id,l.create_date,l.total_diamonds,l.live_fee,l.live_pay_time,u.nick_name,u.head_image
                         FROM {$table} as l
                         LEFT JOIN " . DB_PREFIX . "user AS u  ON l.from_user_id = u.id" . "
                         WHERE $where limit $limit";

        $count_sql = "SELECT count(l.id)  as tpcount
                         FROM  {$table} as l
                         LEFT JOIN " . DB_PREFIX . "user AS u  ON l . from_user_id = u . id" . "
                         WHERE $where ";
        $list = $GLOBALS['db']->getAll($sql, true, true);
        $rs_count = $GLOBALS['db']->getOne($count_sql, true, true);

        $page = new Page($rs_count, $page_size);   //初始化分页对象
        $root['page'] = $page->show();

        foreach ($list as $k => $v) {
            $list[$k]['create_time'] = to_date($list[$k]['create_time']);
            $list[$k]['nick_name'] = emoji_decode($list[$k]['nick_name']);
        }
        if ($rs_count) {
            $root['error'] = "";
            $root['status'] = 1;
            $root['list'] = $list;
        } else {
            $root['error'] = "";
            $root['status'] = 0;
        }
        api_ajax_return($root);
    }

    //付费设置
    public function set_live_pay()
    {
        $root = array();
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $room_id = intval($_REQUEST['room_id']);
        $live_fee = intval($_REQUEST['live_fee']);
        $data['live_fee'] = $live_fee;
        $root['room_id'] = $room_id;
        $type = strim($_REQUEST['type']);
        $m_config = load_auto_cache('m_config');
        $data['is_live_pay'] = 0;
        $this->is_podcast($room_id);

        if ($data['live_fee'] > 0) {
            $data['is_live_pay'] = 1;
        }

        if (defined('OPEN_LIVE_PAY') && OPEN_LIVE_PAY == 0) {
            $root['error'] = "付费直播未开启";
            $root['status'] = 0;
            api_ajax_return($root);
        }

        if (defined('LIVE_PAY_SCENE') && LIVE_PAY_SCENE == 0) {
            $root['error'] = "按场付费未开启";
            $root['status'] = 0;
            api_ajax_return($root);
        }

        //判断是否是直播状态
        $is_live = $GLOBALS['db']->getRow("SELECT is_live_pay,live_pay_type FROM " . DB_PREFIX . "video WHERE live_in =1 AND id =" . $room_id,
            true, true);
        if (!$is_live) {
            $root['error'] = "直播不存在，请刷新后重试";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        if ($is_live['is_live_pay'] == 1 && $is_live['live_pay_type'] == 0) {
            $root['error'] = "已经是按时付费状态，不能切换";
            $root['status'] = 0;
            api_ajax_return($root);
        }

        if ($type == 'set') {
            $list = $GLOBALS['db']->getRow("SELECT is_live_pay,live_fee FROM " . DB_PREFIX . "video WHERE id =" . $room_id,
                true, true);
            $root['error'] = "";
            $root['status'] = 1;
            $root['list'] = $list;
            api_ajax_return($root);
        } else {
            if ($data['is_live_pay']) {
                $m_config['live_count_down'] = intval($m_config['live_count_down']) ? intval($m_config['live_count_down']) : 120;
                $live_pay_time = intval(NOW_TIME + $m_config['live_count_down']);
                $data['live_pay_time'] = $live_pay_time;
                $data['live_pay_type'] = 1;

                $diamond_name = $m_config['diamonds_name'];
                //按场
                $live_pay_scene_max = intval($m_config['live_pay_scene_max']);//付费直播收费最高
                $live_pay_scene_min = intval($m_config['live_pay_scene_min']);//付费直播收费最低
                if ($live_pay_scene_max < $live_fee) {
                    $root['error'] = "按场收费不能高于" . $live_pay_scene_max . $diamond_name;
                    $root['status'] = 0;
                    api_ajax_return($root);
                }
                if ($live_pay_scene_min > $live_fee) {
                    $root['error'] = "按场收费不能低于" . $live_pay_scene_min . $diamond_name;
                    $root['status'] = 0;
                    api_ajax_return($root);
                }
            }

            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "video", $data, 'UPDATE', 'id =' . $room_id)) {
                sync_video_to_redis($room_id, '*', false);
                $root['error'] = "设置成功";
                $root['status'] = 1;
                api_ajax_return($root);
            } else {
                $root['error'] = "设置失败";
                $root['status'] = 0;
                api_ajax_return($root);
            }
        }

    }

    //设置验证码
    public function set_video_code()
    {
        $root = array();
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $room_id = intval($_REQUEST['room_id']);
        $video_code = strim($_REQUEST['video_code']);
//        $data['user_id'] = intval($GLOBALS['user_info']['id']);
        $root['room_id'] = $room_id;
        $type = strim($_REQUEST['type']);
        $is_verify = 0;
        $this->is_podcast($room_id);

        if ($type == 'set') {
            $root['list'] = $GLOBALS['db']->getRow("SELECT video_code,is_verify FROM " . DB_PREFIX . "edu_video_info WHERE video_id =" . $room_id);
            $root['error'] = "";
            $root['status'] = 1;
            api_ajax_return($root);

        }

        //判断是否是直播状态
        $is_live = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM " . DB_PREFIX . "video WHERE live_in =1 AND id =" . $room_id,
            true, true);
        if (!$is_live) {
            $root['error'] = "直播不存在，请刷新后重试";
            $root['status'] = 0;
            api_ajax_return($root);
        }

        //edu_video插入教育直播数据
        $edu_video_data['video_id'] = $room_id;
        $edu_video_data['deal_id'] = 0;
        $edu_video_data['edu_cate_id'] = 0;
        $edu_video_data['tags'] = '';
        $edu_video_data['video_code'] = $video_code;
        $edu_video_data['is_verify'] = $is_verify;
        $edu_video_data['booking_class_id'] = 0;
        $is_set = $GLOBALS['db']->getOne("SELECT COUNT(video_id) FROM " . DB_PREFIX . "edu_video_info WHERE video_id =" . $room_id);

        $data['title'] = '';
        if ($video_code != '') {
            if (!preg_match("/^[A-Za-z0-9]+$/", $video_code)) {
                $return['error'] = "验证码只能是字母和数字";
                $return['status'] = 0;
                api_ajax_return($return);
            }
            $edu_video_data['is_verify'] = 1;

            $data['title'] = "验证码直播";

            $cate_id = $GLOBALS['db']->getOne("select id from " . DB_PREFIX . "video_cate where title='" . $data['title'] . "'",
                true, true);
            if ($cate_id) {
                $is_newtitle = 0;
            } else {
                $is_newtitle = 1;
            }
            if ($is_newtitle) {
                $data_cate = array();
                $data_cate['title'] = $data['title'];
                $data_cate['is_effect'] = 1;
                $data_cate['is_delete'] = 0;
                $data_cate['create_time'] = NOW_TIME;

                $GLOBALS['db']->autoExecute(DB_PREFIX . "video_cate", $data_cate, 'INSERT');
                $cate_id = $GLOBALS['db']->insert_id();
            }
            $data['cate_id'] = $cate_id;
        }

        if ($GLOBALS['db']->autoExecute(DB_PREFIX . "video", $data, 'UPDATE', 'id =' . $room_id)) {
            sync_video_to_redis($room_id, '*', false);
        }

        $root['error'] = "设置失败";
        $root['status'] = 0;
        if ($is_set) {
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_video_info", $edu_video_data, 'UPDATE',
                'video_id =' . $room_id)
            ) {
                $root['error'] = "设置成功";
                $root['status'] = 1;
            }
        } else {
            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "edu_video_info", $edu_video_data, 'INSERT')) {
                $root['error'] = "设置成功";
                $root['status'] = 1;
            }
        }
        api_ajax_return($root);

    }

    //房间设置
    public function set_video()
    {
        $root = array();
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $room_id = intval($_REQUEST['room_id']);
        $data['room_title'] = strim($_REQUEST['room_title']);
        if (strim($_REQUEST['live_image']) != '' && strim($_REQUEST['live_image']) != './(null)') {
            $data['live_image'] = strim($_REQUEST['live_image']);
        }
        $this->is_podcast($room_id);
        $root['room_id'] = $room_id;
        $type = strim($_REQUEST['type']);

        //判断是否是直播状态
        $is_live = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM " . DB_PREFIX . "video WHERE live_in =1 AND id =" . $room_id,
            true, true);
        if (!$is_live) {
            $root['error'] = "直播不存在，请刷新后重试";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        if ($type == 'set') {
            $root['list'] = $GLOBALS['db']->getRow("SELECT live_image,room_title FROM " . DB_PREFIX . "video WHERE id =" . $room_id);
            $root['error'] = "";
            $root['status'] = 1;
            api_ajax_return($root);

        }
        if ($data['room_title']) {
            if (mb_strlen($data['room_title'], "utf-8") < 5) {
                api_ajax_return(array('error' => '直播间名称长度至少5个汉字或5个字母', 'status' => 0));
            }
            if (mb_strlen($data['room_title'], "utf-8") >= 20) {
                api_ajax_return(array('error' => '直播间名称长度不超过20个汉字', 'status' => 0));
            }
            $data['title'] = $data['room_title'];

            $cate_id = $GLOBALS['db']->getOne("select id from " . DB_PREFIX . "video_cate where title='" . $data['title'] . "'",
                true, true);
            if ($cate_id) {
                $is_newtitle = 0;
            } else {
                $is_newtitle = 1;
            }
            if ($is_newtitle) {
                $data_cate = array();
                $data_cate['title'] = $data['title'];
                $data_cate['is_effect'] = 1;
                $data_cate['is_delete'] = 0;
                $data_cate['create_time'] = NOW_TIME;

                $GLOBALS['db']->autoExecute(DB_PREFIX . "video_cate", $data_cate, 'INSERT');
                $cate_id = $GLOBALS['db']->insert_id();
            }
            $data['cate_id'] = $cate_id;

            if ($GLOBALS['db']->autoExecute(DB_PREFIX . "video", $data, 'UPDATE', 'id =' . $room_id)) {
                sync_video_to_redis($room_id, '*', false);
                $root['error'] = "设置成功";
                $root['status'] = 1;
                api_ajax_return($root);
            } else {
                $root['error'] = "设置失败";
                $root['status'] = 0;
                api_ajax_return($root);
            }
        } else {
            api_ajax_return(array('error' => '标题不能为空', 'status' => 0));
        }
    }

    //子房间关联信息
    public function room_account()
    {
        $root = array();
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $user_id = intval($GLOBALS['user_info']['id']);
        $page = intval($_REQUEST['p']);

        if ($page == 0 || $page == '') {
            $page = 1;
        }
        $page_size = 10;
        $limit = (($page - 1) * $page_size) . "," . $page_size;

        if (defined('CHILD_ROOM') && CHILD_ROOM == 1) {
            fanwe_require(APP_ROOT_PATH . 'mapi/lib/ChildRoom.class.php');
            $child_room = new child_room();
            $list = $child_room->account_info($user_id, $limit);
        }

        if ($list['rs_count'] && !empty($list['p_user'])) {
            foreach ($list['p_user'] as &$item) {
                if ($item['sex'] == 1) {
                    $item['sex'] = '男';
                } elseif ($item['sex'] == 2) {
                    $item['sex'] = '女';
                } else {
                    $item['sex'] = '未知';
                }
                $item['live_image'] = get_spec_image($item['live_image']);
                if (!empty($item['head_image'])) {
                    $item['head_image'] = get_spec_image($item['head_image']);
                } else {
                    $item['head_image'] = get_spec_image($item['thumb_head_image']);
                }
                $item['is_live'] = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM " . DB_PREFIX . "video WHERE user_id = {$item['user_id']} AND live_in =1");

                unset($item['thumb_head_image']);
            }

            $root['list'] = $list['p_user'];
            $page = new Page($list['rs_count'], $page_size);   //初始化分页对象
            $root['page'] = $page->show();
            $root['error'] = "";
            $root['status'] = 1;
            api_ajax_return($root);
        } else {
            $root['error'] = "暂无数据";
            $root['status'] = 0;
            api_ajax_return($root);
        }
    }

    //设置禁言
    public function set_forbid()
    {
        $root = array();
        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }

        $user_id = $GLOBALS['user_info']['id'];
        $to_user_id = intval($_REQUEST['user_id']);
        $room_id = intval($_REQUEST['room_id']);
        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoRedisService.php');
        $video_redis = new VideoRedisService();
        //子房间
        if (defined('CHILD_ROOM') && CHILD_ROOM == 1) {
            fanwe_require(APP_ROOT_PATH . 'mapi/lib/ChildRoom.class.php');
            $child_room = new child_room();
            $parent_id = $child_room->parent_id($room_id);
            $video = $video_redis->getRow_db($room_id, array('id', 'user_id', 'group_id','live_in'));
            $p_video = $video_redis->getRow_db($parent_id, array('id', 'user_id', 'group_id'));
            if($video && $video['live_in'] == 3){
                $group_id = $video['group_id'];
            }else{
                $group_id = $p_video['group_id'];
            }
            $video = $video_redis->getRow_db($room_id, array('id', 'user_id'));
            $podcast_id = intval($video['user_id']);

        } else {
            $root['error'] = "功能暂未开放";
            $root['status'] = 0;
            api_ajax_return($root);
        }

        $is_nospeaking = $GLOBALS['db']->getOne("SELECT is_nospeaking FROM " . DB_PREFIX . "user WHERE id =" . $to_user_id,
            true, true);
        if (intval($is_nospeaking) == 1) {
            $root['status'] = 0;
            $root['error'] = "该用户已被im全局禁言.";
            api_ajax_return($root);
        }

        //优化
        $forbid_info = $video_redis->has_forbid_msg($group_id, $to_user_id);//判断某个用户是否被禁言(被禁言返回：true; 未被禁言返回：false)
        if ($forbid_info) {
            $second = 0;
        } else {
            $second = 10000;
        }

        //查看自己
        if ($to_user_id == $user_id) {
            $root['status'] = 0;
            $root['error'] = "不能自己给自己禁言";
            api_ajax_return($root);
        } else {
            $allow = false;
            //主播查看
            if ($podcast_id == $user_id) {
                $allow = true;//主播 有权限禁言
            } else {
                $sql = "select count(id) as num from " . DB_PREFIX . "user_admin where podcast_id = " . $podcast_id . " and user_id = " . $user_id;
                if ($GLOBALS['db']->getOne($sql, true, true) > 0) {
                    $allow = true;//管理员 有权限禁言按钮
                }
            }
        }

        if ($allow) {
            $sql = "select is_robot,nick_name from " . DB_PREFIX . "user where id = '" . $to_user_id . "'";
            $user = $GLOBALS['db']->getRow($sql, true, true);
            $nick_name = $user['nick_name'];

            if ($user['is_robot'] == 1) {
                $root['status'] = 1;

                $msg = $nick_name . " 被禁言";

                $root['error'] = $msg;

            } else {
                fanwe_require(APP_ROOT_PATH . 'system/tim/TimApi.php');
                $api = createTimAPI();
                //设置：禁言(second>0)，取消禁言(second = 0)

                $ret = $api->group_forbid_send_msg($group_id, (string)$to_user_id, $second);

                if ($ret['ActionStatus'] == 'OK') {
                    if ($second > 0) {
                        //禁言到期时间
                        $shutup_time = NOW_TIME + $second;
                        $video_redis->set_forbid_msg($group_id, $to_user_id, $shutup_time);
                        $msg = $nick_name . " 被禁言";
                    } else {
                        $msg = $nick_name . " 取消禁言";
                        $video_redis->unset_forbid_msg($group_id, $to_user_id);
                    }
                    $root['error'] = $msg;
                    $root['status'] = 1;
                } else {
                    $root['status'] = 0;
                    $root['error'] = $ret['ErrorInfo'] . ":" . $ret['ErrorCode'];
                }
            }

            if ($root['status'] == 1) {
                if (!$api) {
                    fanwe_require(APP_ROOT_PATH . 'system/tim/TimApi.php');
                    $api = createTimAPI();
                }

                //群播一个：禁言通知
                $ext = array();
                $ext['type'] = 4; //0:普通消息;1:礼物;2:弹幕消息;3:主播退出;4:禁言;5:观众进入房间；6：观众退出房间；7:直播结束
                $ext['room_id'] = $room_id;//直播ID 也是room_id;只有与当前房间相同时，收到消息才响应
                $ext['fonts_color'] = '';//字体颜色
                $ext['desc'] = $msg;//禁言通知消息;
                $ext['desc2'] = $msg;//禁言通知消息;

                //消息发送者
                $sender = array();
                $sender['user_id'] = $GLOBALS['user_info']['id'];//发送人昵称
                $sender['nick_name'] = $GLOBALS['user_info']['nick_name'];//发送人昵称
                $sender['head_image'] = $GLOBALS['user_info']['head_image'];//发送人头像
                $sender['user_level'] = $GLOBALS['user_info']['user_level'];//用户等级

                $ext['sender'] = $sender;


                #构造高级接口所需参数
                $msg_content = array();
                //创建array 所需元素
                $msg_content_elem = array(
                    'MsgType' => 'TIMCustomElem',       //自定义类型
                    'MsgContent' => array(
                        'Data' => json_encode($ext),
                        'Desc' => '',
                        //	'Ext' => $ext,
                        //	'Sound' => '',
                    )
                );
                //将创建的元素$msg_content_elem, 加入array $msg_content
                array_push($msg_content, $msg_content_elem);

                $ret = $api->group_send_group_msg2($GLOBALS['user_info']['id'], $group_id, $msg_content);
            }


        } else {
            $root['status'] = 0;
            $root['error'] = "无禁言权限";
        }
        api_ajax_return($root);
    }

    /**
     * 设置/取消 管理员
     */
    public function set_admin()
    {

        $root = array();
        $root['status'] = 1;

        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
        } else {

            $user_id = intval($GLOBALS['user_info']['id']);
            $to_user_id = intval($_REQUEST['to_user_id']);//被关注或取消 管理员用户
            $room_id = strim($_REQUEST['room_id']);
            $this->is_podcast($room_id);
            //子房间
            if (defined('CHILD_ROOM') && CHILD_ROOM == 1) {
                fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoRedisService.php');
                $video_redis = new VideoRedisService();
                fanwe_require(APP_ROOT_PATH . 'mapi/lib/ChildRoom.class.php');
                $child_room = new child_room();
                $parent_id = $child_room->parent_id($room_id);
                $video = $video_redis->getRow_db($room_id, array('id', 'user_id', 'group_id','live_in'));
                $p_video = $video_redis->getRow_db($parent_id, array('id', 'user_id', 'group_id'));
                if($video && $video['live_in'] == 3){
                    $group_id = $video['group_id'];
                }else{
                    $group_id = $p_video['group_id'];
                }
                $video = $video_redis->getRow_db($room_id, array('id', 'user_id'));
                $podcast_id = intval($video['user_id']);

            } else {
                $root['error'] = "功能暂未开放";
                $root['status'] = 0;
                api_ajax_return($root);
            }

            if ($podcast_id != $user_id) {
                $root['error'] = "没有权限";
                $root['status'] = 0;
                api_ajax_return($root);
            }

            $sql = "select id from " . DB_PREFIX . "user_admin where podcast_id = '" . $user_id . "' and user_id = " . $to_user_id;
            $user_admin_id = $GLOBALS['db']->getOne($sql);
            if ($user_admin_id > 0) {
                //取消管理员操作;
                $sql = "delete from " . DB_PREFIX . "user_admin where id = " . $user_admin_id;
                $GLOBALS['db']->query($sql);

            } else {
                $sql = "select count(id) as num from " . DB_PREFIX . "user_admin where podcast_id = " . $GLOBALS['user_info']['id'];
                if ($GLOBALS['db']->getOne($sql) < 5) {
                    //设置管理员操作;
                    $user_admin = array();
                    $user_admin['podcast_id'] = $user_id;
                    $user_admin['user_id'] = $to_user_id;
                    $user_admin['create_time'] = NOW_TIME;
                    $GLOBALS['db']->autoExecute(DB_PREFIX . "user_admin", $user_admin, "INSERT");
                } else {
                    $root['error'] = "已经超过5位管理员,不能再设置新的管理员";
                    $root['status'] = 0;
                }
            }

            $sql = "select id from " . DB_PREFIX . "user_admin where podcast_id = '" . $user_id . "' and user_id = " . $to_user_id;
            if ($GLOBALS['db']->getOne($sql) > 0) {
                $root['has_admin'] = 1;//0:非管理员;1:是管理员
            } else {
                $root['has_admin'] = 0;
            }


            if ($root['status'] == 1 && $room_id != '') {

                $sql = "select nick_name from " . DB_PREFIX . "user where id = '" . $to_user_id . "'";
                if ($root['has_admin'] == 1) {
                    $msg = $GLOBALS['db']->getOne($sql) . " 被设置为管理员";
                } else {
                    $msg = $GLOBALS['db']->getOne($sql) . " 管理员被取消";
                }

                $root['error'] = $msg;
                fanwe_require(APP_ROOT_PATH . 'system/tim/TimApi.php');
                $api = createTimAPI();

                //群播一个：直播消息
                $ext = array();
                $ext['type'] = 9; //0:普通消息;1:礼物;2:弹幕消息;3:主播退出;4:禁言;5:观众进入房间；6：观众退出房间；7:直播结束
                $ext['room_id'] = $room_id;//直播ID 也是room_id;只有与当前房间相同时，收到消息才响应
                $ext['fonts_color'] = '';//字体颜色
                $ext['desc'] = $msg;//禁言通知消息;
                $ext['desc2'] = $msg;//禁言通知消息;

                //消息发送者
                $sender = array();
                $sender['user_id'] = $GLOBALS['user_info']['id'];//发送人昵称
                $sender['nick_name'] = $GLOBALS['user_info']['nick_name'];//发送人昵称
                $sender['head_image'] = $GLOBALS['user_info']['head_image'];//发送人头像
                $sender['user_level'] = $GLOBALS['user_info']['user_level'];//用户等级

                $ext['sender'] = $sender;


                #构造高级接口所需参数
                $msg_content = array();
                //创建array 所需元素
                $msg_content_elem = array(
                    'MsgType' => 'TIMCustomElem',       //自定义类型
                    'MsgContent' => array(
                        'Data' => json_encode($ext),
                        'Desc' => '',
                        //	'Ext' => $ext,
                        //	'Sound' => '',
                    )
                );
                //将创建的元素$msg_content_elem, 加入array $msg_content
                array_push($msg_content, $msg_content_elem);

                $ret = $api->group_send_group_msg2($GLOBALS['user_info']['id'], $group_id, $msg_content);
                if ($ret['ActionStatus'] == 'FAIL' && $ret['ErrorCode'] == 10002) {
                    //10002 系统错误，请再次尝试或联系技术客服。
                    $ret = $api->group_send_group_msg2($GLOBALS['user_info']['id'], $group_id, $msg_content);
                }

            }

        }

        api_ajax_return($root);
    }

    //付费按月统计
    public function live_pay_count()
    {
        $root = array();
        $root['status'] = 1;

        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $user_id = $GLOBALS['user_info']['id'];
        $p_user_id = intval($_REQUEST['p_user_id']);

        if (strim($_REQUEST['date'])) {
            $month = str_replace('-', '', strim($_REQUEST['date']));
            $search = strim($_REQUEST['date']);
            $now = to_date(NOW_TIME, 'Ym');
            if ($month > $now) {
                $root['error'] = "超出查询范围";
                $root['status'] = 0;
                api_ajax_return($root);
            }
        } else {
            $month = to_date(NOW_TIME, 'Ym');
            $search = to_date(NOW_TIME, 'Y-m');
        }

        $parent_id = $GLOBALS['db']->getAll("SELECT vh.id FROM " . DB_PREFIX . "video_history as vh LEFT JOIN " . DB_PREFIX . "child_room_account as cra ON cra.p_user_id = vh.user_id WHERE cra.c_user_id = {$user_id} AND cra.p_user_id ={$p_user_id}");
        $p_room_id = $child_ids = implode(',', array_column($parent_id, 'id'));

        if ($parent_id) {
            $sql = "SELECT SUM(total_ticket) as sum_ticket FROM " . DB_PREFIX . "live_pay_log_history as lp ," . DB_PREFIX . "child_room as cr WHERE lp.video_id = cr.child_id AND lp.to_user_id ={$user_id} AND lp.create_ym = $month AND cr.parent_id IN ($p_room_id)";
            $ticket = $GLOBALS['db']->getRow($sql, true, true);
            $root['ticket'] = intval($ticket['sum_ticket']);
        } else {
            $root['ticket'] = 0;
        }

        $root['p_user_id'] = intval($p_user_id);
        $root['date'] = $search;
        $root['error'] = '';
        api_ajax_return($root);
    }

    //礼物统计
    public function prop_count()
    {
        $root = array();
        $root['status'] = 1;

        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $user_id = $GLOBALS['user_info']['id'];
        $p_user_id = intval($_REQUEST['p_user_id']);

        if (strim($_REQUEST['date'])) {
            $month = str_replace('-', '', strim($_REQUEST['date']));
            $search = strim($_REQUEST['date']);
            $now = to_date(NOW_TIME, 'Ym');
            if ($month > $now) {
                $root['error'] = "超出查询范围";
                $root['status'] = 0;
                api_ajax_return($root);
            }
        } else {
            $month = to_date(NOW_TIME, 'Ym');
            $search = to_date(NOW_TIME, 'Y-m');
        }

        $parent_id = $GLOBALS['db']->getAll("SELECT vh.id FROM " . DB_PREFIX . "video_history as vh LEFT JOIN " . DB_PREFIX . "child_room_account as cra ON cra.p_user_id = vh.user_id WHERE cra.c_user_id = {$user_id} AND cra.p_user_id ={$p_user_id}");
        $p_room_id = $child_ids = implode(',', array_column($parent_id, 'id'));
        if ($parent_id) {
            $sql = "SELECT SUM(total_ticket) as sum_ticket FROM " . DB_PREFIX . "child_video_prop as lp ," . DB_PREFIX . "child_room as cr WHERE lp.video_id = cr.child_id AND lp.to_user_id ={$user_id} AND lp.create_ym = $month AND cr.parent_id IN ($p_room_id)";
            $ticket = $GLOBALS['db']->getRow($sql, true, true);
            $root['ticket'] = intval($ticket['sum_ticket']);
        } else {
            $root['ticket'] = 0;
        }

        $root['p_user_id'] = intval($p_user_id);
        $root['date'] = $search;
        $root['error'] = '';
        api_ajax_return($root);
    }

    //关联信息编辑
    public function edit_account()
    {
        $root = array();
        $root['status'] = 1;

        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $id = intval($_REQUEST['id']);
        $account_info = $GLOBALS['db']->getRow("SELECT * FROM " . DB_PREFIX . "child_room_account WHERE id =" . $id);
        if (empty($account_info)) {
            $root['error'] = "关联信息错误";
            $root['status'] = 0;
            api_ajax_return($root);
        }
        $account_info['live_image'] = get_spec_image($account_info['live_image']);

        $root['error'] = "";
        $root['list'] = $account_info;
        api_ajax_return($root);
    }

    //关联信息修改
    public function set_account()
    {
        $root = array();

        if (!$GLOBALS['user_info']) {
            $root['error'] = "用户未登陆,请先登陆.";
            $root['status'] = 0;
            $root['user_login_status'] = 0;//有这个参数： user_login_status = 0 时，表示服务端未登陆、要求登陆，操作
            api_ajax_return($root);
        }
        $id = intval($_REQUEST['id']);
        $data['room_title'] = strim($_REQUEST['room_title']);
        $data['video_code'] = strim($_REQUEST['video_code']);
        $data['live_fee'] = intval($_REQUEST['live_fee']);

        if (strim($_REQUEST['live_image']) != '' && strim($_REQUEST['live_image']) != './(null)') {
            $data['live_image'] = strim($_REQUEST['live_image']);
        }

        if ($data['room_title']) {
            if (mb_strlen($data['room_title'], "utf-8") < 5) {
                api_ajax_return(array('error' => '直播间名称长度至少5个汉字或5个字母', 'status' => 0));
            }
            if (mb_strlen($data['room_title'], "utf-8") >= 20) {
                api_ajax_return(array('error' => '直播间名称长度不超过20个汉字', 'status' => 0));
            }
        }

        if (!empty($data['video_code'])) {
            if (!preg_match("/^[A-Za-z0-9]+$/", $data['video_code'])) {
                api_ajax_return(array('error' => '验证码只能是字母和数字', 'status' => 0));
            }
        }
        if ($data['live_fee'] > 0) {

            $m_config = load_auto_cache('m_config');
            $diamond_name = $m_config['diamonds_name'];

            //按场
            $live_pay_scene_max = intval($m_config['live_pay_scene_max']);//付费直播收费最高
            $live_pay_scene_min = intval($m_config['live_pay_scene_min']);//付费直播收费最低
            if ($live_pay_scene_max < $data['live_fee']) {
                api_ajax_return(array('error' => "按场收费不能高于" . $live_pay_scene_max . $diamond_name, 'status' => 0));
            }

            if ($live_pay_scene_min > $data['live_fee']) {
                api_ajax_return(array('error' => "按场收费不能低于" . $live_pay_scene_min . $diamond_name, 'status' => 0));
            }
        }

        if ($GLOBALS['db']->autoExecute(DB_PREFIX . "child_room_account", $data, 'UPDATE', 'id =' . $id)) {
            $root['error'] = "设置成功";
            $root['status'] = 1;
            api_ajax_return($root);
        } else {
            $root['error'] = "设置失败";
            $root['status'] = 0;
            api_ajax_return($root);
        }
    }
}


