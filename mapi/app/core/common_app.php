<?php

function get_recommend_suites()
{
    $sql = "select id,title,image,cad from " . DB_PREFIX . "hk_video_suite where is_recommend = 1 order by sort desc, id desc limit 100";
    $suites = $GLOBALS['db']->getAll($sql);

    $suite_ids = [];
    $suite_map = [];
    foreach ($suites as $suite) {
        $suite['image'] = get_spec_image($suite['image'], 720, 288);
        $suite_ids[] = $suite['id'];
        $suite['videos'] = [];
        $suite_map[$suite['id']] = $suite;
    }

    $sql = "select id,title,image,suite_id,create_time,watch_num + virtual_watch_num as watch_num,down_num + virtual_down_num as down_num from " . DB_PREFIX . "hk_video_history where suite_id in (" . implode(',',
            $suite_ids) . ") order by sort desc, id desc";
    $suite_videos = $GLOBALS['db']->getAll($sql);
    foreach ($suite_videos as $video) {
        $video['create_text'] = to_date($video['create_time'], 'Y.m.d');
        $video['image'] = get_spec_image($video['image'], 520, 345);
        $suite_map[$video['suite_id']]['videos_history'][] = $video;
    }

    return array_values($suite_map);
}

function get_recommend_videos_history()
{
    $sql = "select id,title,image,suite_id,create_time,watch_num + virtual_watch_num as watch_num,down_num + virtual_down_num as down_num from " . DB_PREFIX . "hk_video_history where is_recommend = 1 order by sort desc, id desc limit 3";
    $videos = $GLOBALS['db']->getAll($sql);
    foreach ($videos as &$video) {
        $video['image'] = get_spec_image($video['image'], 520, 345);
    }
    unset($video);
    return $videos;
}

function get_suite($id)
{
    $sql = "select id,title,image,cad from " . DB_PREFIX . "hk_video_suite where id = " . $id;
    $suite = $GLOBALS['db']->getRow($sql);
    $suite['image'] = get_spec_image($suite['cad'], 720, 288);
    $suite['cad'] = get_spec_image($suite['cad'], 180, 108);
    return $suite;
}

function get_suite_videos($suite_id)
{
    $sql = "select id,title,image,x,y,z,suite_id,watch_num + virtual_watch_num as watch_num,create_time,view_level_sd,view_level_hd from " . DB_PREFIX . "hk_video where suite_id = {$suite_id} order by sort desc, id desc";
    $videos = $GLOBALS['db']->getAll($sql);

    foreach ($videos as &$video) {
        $video['image'] = get_spec_image($video['image'], 520, 345);
    }
    unset($video);
    return $videos;
}

function get_download_videos($user_id, $video_ids = [])
{
    if (!is_array($video_ids)) {
        $video_ids = [$video_ids];
    }

    $download_videos = $GLOBALS['db']->getAll("select video_id,`type` from " . DB_PREFIX . "hk_video_download where user_id = {$user_id} and video_id in (" . implode(',',
            $video_ids) . ")");

    $videos = [];
    foreach ($download_videos as $video) {
        if (isset($videos[$video['video_id']])) {
            $videos[$video['video_id']][] = $video['type'];
        } else {
            $videos[$video['video_id']] = [$video['type']];
        }
    }
    return $videos;
}

function get_user_vip($id)
{
    $sql = "select id,vip_type,vip_expire_time from " . DB_PREFIX . "user where id = " . $id;
    $user = $GLOBALS['db']->getRow($sql, true, true);
    if (!empty($user) && $user['vip_expire_time'] < NOW_TIME) {
        $user['vip_type'] = 0;
    }
    return $user;
}

function get_suite_url($id, $room_id)
{
    return url('live#detail', ['id' => $id, 'room_id' => $room_id]);
}

function get_vip_rules($type)
{
    $rules = $GLOBALS['db']->getAll("select id, name, diamonds from " . DB_PREFIX . "hk_vip_rule where `type` = {$type} order by sort desc");

    return $rules;
}

function get_hk_video($id)
{
    $video = $GLOBALS['db']->getRow("select id,title,image,x,y,z,suite_id,watch_num + virtual_watch_num as watch_num,create_time,view_level_sd,view_level_hd from " . DB_PREFIX . "hk_video where id =" . $id);

    fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoRedisService.php');
    $video_redis = new VideoRedisService();

    $video_data = $video_redis->getRow_db($id,
        [
            'id',
            'live_in',
            'play_flv_sd',
            'play_hls_sd',
            'play_flv_hd',
            'play_hls_hd',
            'group_id',
        ]);

    return array_merge($video_data, $video);
}

function get_hk_video_history($id)
{
    $video = $GLOBALS['db']->getRow("select id,title,image,duration,watch_num + virtual_watch_num as watch_num,down_num + virtual_down_num as down_num,create_time,play_url,play_url_hd,play_url_sd,price,price_hd,price_sd,view_level,view_level_hd,view_level_sd,down_level,down_level_hd,down_level_sd,suite_id from " . DB_PREFIX . "hk_video_history where id =" . $id);

    return $video;
}

function get_suite_videos_history($suite_id)
{
    $videos = $GLOBALS['db']->getAll("select id,suite_id,title,image,watch_num + virtual_watch_num as watch_num,down_num + virtual_down_num as down_num,create_time,view_level_sd,view_level_hd,view_level from " . DB_PREFIX . "hk_video_history where suite_id =" . $suite_id);

    foreach ($videos as &$video) {
        $video['image'] = get_spec_image($video['image'], 520, 345);
    }
    unset($video);
    return $videos;
}

function create_hk_video($video_id, $title, $live_image, $user_id = 0)
{
    $m_config = load_auto_cache("m_config");

    $data = array();
    $data['id'] = $video_id;

    $data['live_image'] = $live_image;
    $data['room_type'] = 3;
    $data['virtual_number'] = intval($m_config['virtual_number']);
    $data['max_robot_num'] = intval($m_config['robot_num']);//允许添加的最大机器人数;

    $data['video_type'] = intval($m_config['video_type']);//0:腾讯云互动直播;1:腾讯云直播
    $data['monitor_time'] = to_date(NOW_TIME + 86400 * 365 * 10, 'Y-m-d H:i:s');//主播心跳监听

    $data['push_url'] = '';//video_type=1;1:腾讯云直播推流地址
    $data['play_url'] = '';//video_type=1;1:腾讯云直播播放地址(rmtp,flv)

    $data['title'] = $title;
    $data['user_id'] = $user_id;
    $data['live_in'] = 2;//live_in:是否直播中 1-直播中 0-已停止;2:正在创建直播;
    $data['watch_number'] = '';//'当前观看人数';
    $data['vote_number'] = '';//'获得票数';
    $data['create_time'] = NOW_TIME;//'创建时间';
    $data['begin_time'] = NOW_TIME;//'开始时间';
    $data['end_time'] = '';//'结束时间';
    $data['is_hot'] = 1;//'1热门; 0:非热门';
    $data['is_new'] = 1; //'1新的; 0:非新的,直播结束时把它标识为：0？'
    $data['online_status'] = 1;//主播在线状态;1:在线(默认); 0:离开
    $data['sort_init'] = 0;
    $data['sort_num'] = $data['sort_init'];

    $channel_info = get_aliyun_push($video_id, 'a');
    $data['channelid_sd'] = $channel_info['stream_id'];
    $data['push_rtmp_sd'] = $channel_info['push_rtmp'];
    $data['play_rtmp_sd'] = $channel_info['play_rtmp'];
    $data['play_flv_sd'] = $channel_info['play_flv'];
    $data['play_hls_sd'] = $channel_info['play_hls'];

    $channel_info2 = get_aliyun_push($video_id, 'b');
    $data['channelid_hd'] = $channel_info2['stream_id'];
    $data['push_rtmp_hd'] = $channel_info2['push_rtmp'];
    $data['play_rtmp_hd'] = $channel_info2['play_rtmp'];
    $data['play_flv_hd'] = $channel_info2['play_flv'];
    $data['play_hls_hd'] = $channel_info2['play_hls'];

    //直播分类
    $GLOBALS['db']->autoExecute(DB_PREFIX . "video", $data, 'INSERT');

    if ($GLOBALS['db']->affected_rows()) {
        $return['status'] = 1;
        $return['error'] = '';
        $return['room_id'] = $video_id;
        $return['video_type'] = intval($data['video_type']);

        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/BaseRedisService.php');
        sync_video_to_redis($video_id, '*', false);

        fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoRedisService.php');
        $video_redis = new VideoRedisService();
        $video_redis->video_online($video_id, $video_id);

    } else {
        $return['status'] = 0;
        $return['error'] = '创建房间失败！';
    }
}

/**
 * 获取推流地址
 * 如果不传key和过期时间，将返回不含防盗链的url
 * @param bizId 您在腾讯云分配到的bizid
 *        streamId 您用来区别不同推流地址的唯一id
 *        key 安全密钥
 *        time 过期时间 sample 2016-11-12 12:00:00
 * @return String url
 */
function getPushUrl($bizId, $streamId, $key = null, $time = null)
{

    if ($key && $time) {
        $txTime = strtoupper(base_convert(strtotime($time), 10, 16));
        //txSecret = MD5( KEY + livecode + txTime )
        //livecode = bizid+"_"+stream_id  如 8888_test123456
        $livecode = $bizId . "_" . $streamId; //直播码
        $txSecret = md5($key . $livecode . $txTime);
        $ext_str = "?" . http_build_query(array(
                "bizid" => $bizId,
                "txSecret" => $txSecret,
                "txTime" => $txTime
            ));
    }
    return "rtmp://" . $bizId . ".livepush.myqcloud.com/live/" . $livecode . (isset($ext_str) ? $ext_str : "");
}

/**
 * 获取播放地址
 * @param bizId 您在腾讯云分配到的bizid
 *        streamId 您用来区别不同推流地址的唯一id
 * @return String url
 */
function getPlayUrl($bizId, $streamId)
{
    $livecode = $bizId . "_" . $streamId; //直播码
    return array(
        "rtmp" => "rtmp://" . $bizId . ".liveplay.myqcloud.com/live/" . $livecode,
        "flv" => "http://" . $bizId . ".liveplay.myqcloud.com/live/" . $livecode . ".flv",
        "hls" => "http://" . $bizId . ".liveplay.myqcloud.com/live/" . $livecode . ".m3u8"
    );
}

function get_push_url($video_id, $user_id, $layer = 'a')
{
    $m_config = load_auto_cache('m_config');
    $bizId = $m_config['qcloud_bizid'];
    $key = $m_config['qcloud_security_key'];

    $stream_id = $video_id . $layer . "_" . substr(md5($user_id . microtime_float() . mt_rand()), 12, 4);
    $time = to_date(get_gmtime() + 86400 * 30, 'Y-m-d H:i:s');

    $push_rtmp = getPushUrl($bizId, $stream_id, $key, $time);

    if (intval($m_config['has_save_video'])) {
        $save_video = "&record=hls&record_interval=5400";
    } else {
        $save_video = '';
    }

    return array(
        'channel_id' => $bizId . "_" . $stream_id,
        'upstream_address' => $push_rtmp . $save_video,
        'downstream_address' => getPlayUrl($bizId, $stream_id),
    );
}

function check_self_build_video()
{
    $sql = "select id,group_id,channelid_sd as channelid,room_type,video_type from " . DB_PREFIX . "video where live_in = 2";
    $videos = $GLOBALS['db']->getAll($sql);

    fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/VideoRedisService.php');
    $video_redis = new VideoRedisService();

    $m_config = load_auto_cache('m_config');
    fanwe_require(APP_ROOT_PATH . 'mapi/lib/core/video_aliyun.php');
    $service = new VideoAliyun($m_config);

    $ret_array = [];
    foreach ($videos as $v) {

        if (empty($v['channelid'])) {
            continue;
        }

        $channel_info = $service->Query($v['channelid']);
        $channel_info['channel_id'] = $v['channelid'];
        $ret_array[] = $channel_info;
        // 正在直播，继续监听
        if ($channel_info['stream_status'] != 1) {
            continue;
        }

        $data = array('live_in' => 1, 'begin_time' => NOW_TIME);
        $GLOBALS['db']->autoExecute(DB_PREFIX . "video", $data, 'UPDATE', " id=" . $v['id']);

        if (!$GLOBALS['db']->affected_rows()) {
            continue;
        }

        $room_id = $v['id'];
        $video_redis->video_online($room_id, $v['group_id']);
        //将mysql数据,同步一份到redis中
        sync_video_to_redis($room_id, '*', false);

        if ($v['room_type'] == 3) {
            crontab_robot($room_id);
        }
    }
    return $ret_array;
}

function get_hk_vip_discount_diamonds($type, $expire_time)
{
    if ($type <= 0 || $expire_time < NOW_TIME) {
        return 0;
    }

    $sql = "SELECT
    diamonds,
    month_num
FROM
    " . DB_PREFIX . "hk_vip_rule
WHERE
    `type` = {$type}
AND is_effect = 1
ORDER BY
    month_num limit 1";
    $vip_rule = $GLOBALS['db']->getRow($sql);
    if (empty($vip_rule)) {
        return 0;
    }

    $diamonds = ceil($vip_rule['diamonds'] / (NOW_TIME - strtotime("-{$vip_rule['month_num']} months",
                NOW_TIME)) * ($expire_time - NOW_TIME));
    return min($diamonds, $vip_rule['diamonds']);
}

function get_aliyun_vhost()
{
    $max = 10;
    $videos = $GLOBALS['db']->getAll("select vhost , count(*) as num from " . DB_PREFIX . "video_aliyun group by vhost");
    $vhost_max = array();
    foreach ($videos as $video) {
        if ($video['num'] < $max) {
            return $video['vhost'];
        }
        $vhost_max[] = $video['vhost'];
    }

    $m_config = load_auto_cache('m_config');
    $vhosts = preg_split("/[\r\n]+/", $m_config['aliyun_vhost']);
    foreach ($vhosts as $vhost) {
        if (!in_array($vhost, $vhost_max)) {
            return trim($vhost);
        }
    }
    return false;
}

function build_auth_key($uri, $time)
{
    $m_config = load_auto_cache('m_config');
    $auth_key = md5("{$uri}-{$time}-0-0-" . $m_config['aliyun_private_key']);
    return "{$uri}?auth_key={$time}-0-0-" . $auth_key;
}

/** https://help.aliyun.com/document_detail/29957.html?spm=5176.doc35409.6.546.IMpk1g
 * @param $video_id
 * @return array
 */
function get_aliyun_push($video_id, $prefix = "")
{
    $vhost = get_aliyun_vhost();
    if (!$vhost) {
        return array('status' => 0, "error" => '推流数量已超出最大值');
    }

    $stream_id = $video_id . "_" . substr(md5($video_id . microtime_float()), 12);
    if (!empty($prefix)) {
        $stream_id = $prefix . "_" . $stream_id;
    }

    $app_name = 'live';
    $upstream_time = NOW_TIME + 86400 * 30;
    $download_time = $upstream_time + 6 * 3600;

    $GLOBALS['db']->autoExecute(DB_PREFIX . "video_aliyun", array(
        'vhost' => $vhost,
        'stream_id' => $stream_id,
        'create_time' => NOW_TIME,
    ));

    return array(
        'status' => 1,
        'stream_id' => $stream_id,
        'push_rtmp' => "rtmp://video-center-bj.alivecdn.com" . build_auth_key("/{$app_name}/{$stream_id}",
                $upstream_time) . "&vhost={$vhost}",
        'play_rtmp' => "rtmp://{$vhost}" . build_auth_key("/{$app_name}/{$stream_id}", $download_time),
        'play_flv' => "http://{$vhost}" . build_auth_key("/{$app_name}/{$stream_id}.flv", $download_time),
        'play_hls' => "http://{$vhost}" . build_auth_key("/{$app_name}/{$stream_id}.m3u8", $download_time),
    );
}
