<?php
// +----------------------------------------------------------------------
// | FANWE 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://www.fanwe.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 云淡风轻(88522820@qq.com)
// +----------------------------------------------------------------------

class dealCModule extends baseModule
{
    /**
     * 商品详细页接口
     * 输入：
     * data_id: int 商品ID
     *
     *
     *
     * 输出：
     *
    [id] => 73 [int] 商品ID
    [name] => 仅售388元！价值899元的福州明视眼镜单人配镜套餐，含全场599元以内镜框1次+全场300元以内镜片1次。 [string] 商品名称
    [share_url] => [string] 分享链接
    [sub_name] => 明视眼镜  [string] 简短商品名称
    [brief] => 【37店通用】明视眼镜  [string] 简介
    [current_price] => 388  [ float] 当前价格
    [origin_price] => 899  [ float] 原价
    [return_score_show] => 0 [int] 所需要的积分，buy_type为1时显示的价格
    [icon] => http://localhost/o2onew/public/attachment/201502/25/17/54ed9b8e44904_600x364.jpg   [string] 商品缩略图 300X182
    [begin_time] => 1424829400  [string] 开始时间
    [end_time] => 1519782997    [string] 结束时间
    [time_status] => 1  [int] 时间状态  (0 未开始 / 1 可以兑换或者购买 / 2 已经过期)
    [now_time] => 1429125598 [string] 当前时间
    [buy_count] => 7 [int] 销量（购买的件数）
    [buy_type] => 0 [int] 购买类型， 团购商品的类型0：普通 1:积分商品
    [is_shop] => 0 [int] 是否为商城商品0：否 1:是
    [is_collect] =>0 [int ] 是否收藏商品   0：否 1：是
    [is_my_fx]=>0 [int] 是否是我的分销商品    0：否 1：是
    [deal_attr] => Array [array] 商品属性数据
    [0] => Array
    (
    [id] => 17  [int] 属性分类 ID
    [name] => 时段    [string] 属性名称
    [attr_list] => Array [array] 属性下的套餐
    (
    [0] => Array
    (
    [id] => 274  [int]套餐编号
    [name] => 早上  [string] 套餐名称
    [price] => 0.0000 [float] 递增的价格
    )

    )

    )
    [avg_point] => 3 [float] 商品点评平均分
    [dp_count] => 5 [int] 点评人数
    [supplier_location_count] => 4  [int] 门店总数
    [last_time] => 90572607 [int] 剩余的秒数
    [last_time_format] => 1048天以上   [string] 剩余的天数 (结束为0)
    [deal_tags] => Array [array] 商品标签
    (
    [0] => Array
    (
    [k] => 2  [int] 标签编号
    [v] => 多套餐 [string] 标签名称
    )

    )
    [images] => Array [array] 商品图集 230X140
    (
    [0] => http://localhost/o2onew/public/attachment/201502/25/17/54ed9b8e44904_460x280.jpg
    [1] => http://localhost/o2onew/public/attachment/201504/17/11/5530793f0e95d_460x280.jpg
    [2] => http://localhost/o2onew/public/attachment/201504/17/11/553079440bbbf_460x280.jpg
    )

    [oimages] => Array  商品图集原图
    (
    [0] => http://localhost/o2onew/public/attachment/201502/25/17/54ed9b8e44904.jpg
    [1] => http://localhost/o2onew/public/attachment/201504/17/11/5530793f0e95d.jpg
    [2] => http://localhost/o2onew/public/attachment/201504/17/11/553079440bbbf.jpg
    )
    [description]=> <li id="side;"><b>店内部分菜品价格参考</b>[string] 商品详情 HTML 格式
    [notes] => <span style="font-family:ht;background-color:#e2e8eb;">购买须知</span> [string] 购买须知 HTML 格式
    [xpoint] => [float] 所在经度
    [ypoint] => [float] 所在纬度
    [supplier_location_list] => Array [array] 门店数据列表
    (
    [0] => Array
    (
    [id] => 35  [int] 门店编号
    [name] => 明视眼镜（台江万达店）  [string] 门店名称
    [address] => 台江区鳌江路8号金融街万达广场一层B区37号 [string] 门店地址
    [tel] => 0591-89800987 [string] 门店联系方式
    [xpoint] => 经度 [float]
    [ypoint] => 纬度 [float]
    )

    )
    [dp_list] => Array [array] 点评数据列表
    (
    [4] => Array
    (
    [id] => 5 [int] 点评数据ID
    [create_time] => 2015-04-07 [string] 点评时间
    [content] => 不错不错   [string] 点评内容
    [reply_content] => 那是不错的了，可以信任的品牌 [string] 管理员回复内容
    [point] => 5    [int] 点评分数
    [user_name] => fanwe  [string] 点评用户名称
    [images] => Array [array] 点评图集 压缩后的图片
    (
    [0] => http://localhost/o2onew/public/comment/201504/07/14/bca3b3e37d26962d0d76dbbd91611a6a36_120x120.jpg   [string] 点评图片 60X60
    [1] => http://localhost/o2onew/public/comment/201504/07/14/5ea94540a479810a7559b5db909b09e986_120x120.jpg   [string] 点评图片 60X60
    [2] => http://localhost/o2onew/public/comment/201504/07/14/093e5a064c4081a83f31864881bd802061_120x120.jpg   [string] 点评图片 60X60
    )

    [oimages] => Array [array] 点评图集 原图
    (
    [0] => http://localhost/o2onew/public/comment/201504/07/14/bca3b3e37d26962d0d76dbbd91611a6a36.jpg [string] 点评图片 原图
    [1] => http://localhost/o2onew/public/comment/201504/07/14/5ea94540a479810a7559b5db909b09e986.jpg [string] 点评图片 原图
    [2] => http://localhost/o2onew/public/comment/201504/07/14/093e5a064c4081a83f31864881bd802061.jpg [string] 点评图片 原图
    )

    )

    ),


    //当前门店的商品数据
    [other_location_deal]	=>	Array (
    [0] =>
    Array (
    [id] => '3872' [int] 商品ID
    [name] => '【嵊州】巴蜀石锅鱼 仅售199元！价值346元的石锅鱼套餐，建议6-8人使用，提供免费WiFi。' [string] 商品名称
    [sub_name] => '巴蜀石锅鱼199元石锅鱼套餐' [string] 简短商品名称
    [current_price] => 199 [ float] 当前价格
    [origin_price] => 346 [ float] 原价
    [icon] => 'http://192.168.3.148/fwshop/public/attachment/201509/10/15/55f12d3c1721a_180x164.jpg' [string] 商品缩略图 90X82
    [buy_count] => '1' [int] 销量（购买的件数）
    )
    )

    // 推荐商品数据
    [recommend_data]	=>	Array (
    [0] =>
    Array (
    [id] => '3872' [int] 商品ID
    [name] => '【嵊州】巴蜀石锅鱼 仅售199元！价值346元的石锅鱼套餐，建议6-8人使用，提供免费WiFi。' [string] 商品名称
    [sub_name] => '巴蜀石锅鱼199元石锅鱼套餐' [string] 简短商品名称
    [current_price] => 199 [ float] 当前价格
    [origin_price] => 346 [ float] 原价
    [icon] => 'http://192.168.3.148/fwshop/public/attachment/201509/10/15/55f12d3c1721a_180x164.jpg' [string] 商品缩略图 90X82
    [buy_count] => '1' [int] 销量（购买的件数）
    )
    )




    //如果该商品有关联商品
    [relate_data]	=>	Array[array]	关联商品数据
    'goodsList'	=>	array(
    //其他字段与主商品一致，增加两个key(属性和库存)
    [stock] => Array(
    [335_337] => Array(
    [id] => 162
    [deal_id] => 64
    [attr_cfg] => Array(
    [19] => 棕色
    [20] => 均码
    )
    [stock_cfg] => 2
    [attr_str] => 棕色均码
    [buy_count] => 0
    [attr_key] => 335_337
    )
    )
    [deal_attr] => Array(
    [0] => Array(
    [id] => 20
    [name] => 尺码
    [attr_list] => Array(
    [0] => Array(
    [id] => 337
    [name] => 均码
    [price] => 3.0000
    [is_checked] => 1
    )
    )
    )

    [1] => Array(
    [id] => 19
    [name] => 颜色
    [attr_list] => Array(
    [0] => Array(
    [id] => 335
    [name] => 棕色
    [price] => 1.0000
    [is_checked] => 1
    )
    )
    )
    )
    )
    ),
    'dealArray'	=>	array(
    'id'=>array(
    'name'=>'',
    'origin_price'=>'',
    'current_price'=>'',
    'min_bought'=>'',
    'max_bought'=>''
    ),
    ),
    'attrArray'	=>	array(
    'id'=>array(
    '规格类型'=>array(
    '规格id'=>array(),
    ),
    ),
    ),
    'stockArray'	=>	array(
    'id'=>array(
    '规格类型_规格类型'=>array(),
    ),
    ),
    )

     *
     */
    public function index()
    {
        //商品id
        $data_id = intval($_REQUEST['data_id']);

        $location_id = intval($_REQUEST['location_id']);

        if($data_id==0)
            $data_id = intval($GLOBALS['db']->getOne("select id from ".DB_PREFIX."deal where uname = '".strim($_REQUEST['data_id'])."'"));

        $type = 1;

        $user = $GLOBALS['user_info'];
        $user_id  = intval($user['id']);
        $root['status']=1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }
        require_once(APP_ROOT_PATH."system/model/deal.php");
        fanwe_require(APP_ROOT_PATH."system/model/Pintuan.php");
        //统计购物车商品数量
        $cart_num = intval($GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_cart where  buy_type = 0 and in_cart=1 and user_id =".$user_id));

        //商品详情
        $deal_info =  $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$data_id."' and is_delete = 0 or (uname= '".$data_id."' and uname <> '')");
        if($deal_info)
        {

            if($deal_info['uname']!='')
                $durl = url("index","deal#".$deal_info['uname']);
            else
                $durl = url("index","deal#".$deal_info['id']);

            $deal_info['url'] = $durl;
        }else{
            $root['status'] = 0;
            $root['error']  = "商品不存在";
            api_ajax_return($root);
        }
        //图片
        $deal_info['image_list'] = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_gallery where deal_id=".intval($data_id)." order by sort asc");

        //规格属性选择
        //$deal_attr = array();
        //$deal_attr = $GLOBALS['db']->getAll("select id,name,is_checked from ".DB_PREFIX."deal_attr where deal_id = ".$data_id);
        //$deal_info['deal_attr'] = $deal_attr;

        $data=$deal_info;
        //if ($data_id==418){print_r($data['is_pt']);exit();}
        //规格属性选择
        $deal_attr = array();
        if($data['deal_goods_type']){
            $deal_attr = $GLOBALS['db']->getAll("select id,name from ".DB_PREFIX."goods_type_attr where goods_type_id = ".$data['deal_goods_type']);
            foreach($deal_attr as $k=>$v)
            {
                $deal_attr[$k]['attr_list'] = $GLOBALS['db']->getAll("select id,name,is_checked from ".DB_PREFIX."deal_attr where deal_id = ".$data['id']." and goods_type_attr_id = ".$v['id']);
                if(!$deal_attr[$k]['attr_list'])
                    unset($deal_attr[$k]);
            }
        }
        $data['deal_attr'] = $deal_attr;
        //开始输出库存json
        $attr_stock_list =$GLOBALS['db']->getAll("SELECT id, deal_id, attr_cfg, stock_cfg, attr_str, buy_count, attr_key,price, add_balance_price, presell_deposit_money, presell_discount_money, presell_buy_count, pt_money, pt_buy_count FROM ".DB_PREFIX."attr_stock WHERE deal_id=".$data['id'],false);

        $group_id = $GLOBALS['user_info']['group_id'];
        $discount=1;
        $is_presell = 0;
        if($group_id && $data['allow_user_discount']){
            $group_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_group where id = ".$group_id);

            if($group_info && $group_info['discount']<1 && $is_presell==0){
                if($data['allow_user_fx_discount']&&$GLOBALS['user_info']['is_fx']){
                    $discount=$group_info['discount']*$data['user_fx_discount'];
                }else{
                    $discount=$group_info['discount'];
                }
                //$row['price'] = round($row['price']*$group_info['discount'],2);
            }elseif($data['allow_user_fx_discount'] && $GLOBALS['user_info']['is_fx'] && $is_presell==0){
                $discount=$data['user_fx_discount'];
            }
        }elseif($data['allow_user_fx_discount'] && $GLOBALS['user_info']['is_fx'] && $is_presell==0){
            $discount=$data['user_fx_discount'];
        }
        $attr_stock_data = array();
        foreach($attr_stock_list as $row)
        {
            $row['attr_cfg'] = unserialize($row['attr_cfg']);
            //$row['presell_deposit_money'] = round($row['presell_deposit_money'],2);

            $row['attr_price'] = round(($row['price']+$data['current_price'])*$discount,2);
            $row['price'] = round($row['price']*$discount,2);
            $attr_stock_data[$row['attr_key']] = $row;
        }

        if($_REQUEST['from']=='app'){
            $data['deal_attr_stock_json'] = count($attr_stock_data)>0?json_encode($attr_stock_data):'';
        }else{
            $data['deal_attr_stock_json'] = json_encode($attr_stock_data);
        }
        //获取真实库存（非属性库存 在属性库存不存在时候使用）
        $data['deal_stock'] = intval($GLOBALS['db']->getOne("select stock_cfg from ".DB_PREFIX."deal_stock where deal_id = ".$data['id']));

        //---------------------------------------


        //商品描述替换成手机端描述
        //$data['description']=get_abs_img_root($data['phone_description']);

        $cate_id=$data['cate_id'];
        $return_money=$data['return_money'];
        $return_score=$data['return_score'];
        if($data['id']>0 && $data['is_pick']==0)
        {
            $join = '';
            $field_append = '';
            //开始身边团购的地理定位
            $geo=$GLOBALS['geo'];
            $ypoint =  $geo['ypoint'];  //ypoint
            $xpoint =  $geo['xpoint'];  //xpoint

            $address = $geo['address'];
            $sort="";
            if($xpoint>0)/* 排序（$order_type）  default 智能（默认）*/
            {
                $pi = PI;  //圆周率
                $r = EARTH_R;  //地球平均半径(米)
                $field_append = ", (ACOS(SIN(($ypoint * $pi) / 180 ) *SIN((sl.ypoint * $pi) / 180 ) +COS(($ypoint * $pi) / 180 ) * COS((sl.ypoint * $pi) / 180 ) *COS(($xpoint * $pi) / 180 - (sl.xpoint * $pi) / 180 ) ) * $r) as distance ";
                $sort=" distance ";
            }

            $join .= " left join ".DB_PREFIX."deal_location_link as l on sl.id = l.location_id ";
            $where = " l.deal_id = ".$data['id']." ";
            $locations = get_location_list($data['supplier_location_count'],array("supplier_id"=>$data['supplier_id']),$join,$where, $sort, $field_append, $groupby='',$is_deal_cate=false);
        }
        elseif($data['is_pick']==1){
            $locations = get_location_list("",array("supplier_id"=>$data['supplier_id']), $join = '', $where='',$orderby = '',$field_append="", $groupby='',$is_deal_cate=false);
        }
        else{
            $locations = get_location_list($data['supplier_location_count'],array("supplier_id"=>$data['supplier_id']), $join = '', $where='',$orderby = '',$field_append="", $groupby='',$is_deal_cate=false);
        }

        $data['supplier_location_count']=count($locations['list']);

        //拼团
        if($data['is_pt']==1){
            $data['is_pt'] = 1;
            $data['pt_id']=$data['id'];
            $data['pt_start_time']="";
            $data['pt_end_time']="";
            $data['pt_success_user_limit']=$data['pt_team_num'];
            $data['pt_success_time_limit']=$data['pt_useful_time'];
            $data['pt_cate_id']=0;
        }else{
            $data['is_pt']=0;
            $data['pt_start_time']=0;
            $data['pt_end_time']=0;
        }


        $data = format_deal_item($data);

        //额外参数
        $data['cart_num'] = $cart_num;//购物车数量

        //无需积分-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $data['buy_type']=0;
        if($data['buy_type']==1){ //积分商品才判断登录状态和会员积分是否足够兑换


            /*$user_login_status = check_login();
            if($user_login_status==LOGIN_STATUS_TEMP)
            {
                $user_login_status = LOGIN_STATUS_LOGINED;
            }*/
            $user_login_status=1;
            $score_button=array();
            $data['user_login_status'] = $user_login_status;
            if($data['user_login_status']==1){
                if( $GLOBALS['user_info']['score'] + $return_score >=0){//足够兑换
                    $score_button['name']='立即兑换';
                    $score_button['status']=1;
                }else{
                    $score_button['name']='积分不足';
                    $score_button['status']=-1;
                }
            }else{
                $score_button['name']='立即兑换';
                $score_button['status']=1;
            }
            $data['score_button'] = $score_button;
        }

        if($user_id>0){
            $is_collect = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_collect where user_id = ".$user_id." and deal_id = ".$data_id);
            if(defined("FX_LEVEL"))
                $is_my_fx = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."user_deal where user_id = ".$user_id." and deal_id = ".$data_id);
        }
        $data['is_my_fx'] =$is_my_fx?$is_my_fx:0;
        $data['is_collect'] = $is_collect>0?1:0;

        /*门店数据*/
        $supplier_location_list = array();
        $location_id=0;
        if($locations){
            if(ENTRY_NAME=='nzy_o2o'){
                foreach ($locations['list'] as $k=>$v){
                    $temp_location = array();
                    $temp_location['id'] = $v['id'];
                    $temp_location['name'] = $v['name'];
                    $temp_location['address'] = $v['address'];
                    $temp_location['tel'] = $v['tel'];
                    $temp_location['xpoint'] = $v['xpoint'];
                    $temp_location['ypoint'] = $v['ypoint'];
                    $temp_location['distance'] = 0;//$v['distance'];
                    $temp_location['is_main'] = $v['is_main'];
                    if($v['is_main']==1)
                        $location_id=$v['id'];
                    $supplier_location_list[] = $temp_location;
                }
            }else{
                foreach ($locations['list'] as $k=>$v){
                    $temp_location = array();
                    $temp_location['id'] = $v['id'];
                    $temp_location['name'] = $v['name'];
                    $temp_location['address'] = $v['address'];
                    $temp_location['tel'] = $v['tel'];
                    $temp_location['xpoint'] = $v['xpoint'];
                    $temp_location['ypoint'] = $v['ypoint'];
                    $temp_location['distance'] = $v['distance'];
                    $temp_location['is_main'] = $v['is_main'];
                    if($v['is_main']==1)
                        $location_id=$v['id'];
                    $supplier_location_list[] = $temp_location;
                }
            }

        }

        $data['supplier_location_list'] = $supplier_location_list;

        if(!$location_id&&$supplier_location_list['0']['id'])
            $location_id=$supplier_location_list['0']['id'];
        if($data['supplier_id']<=0)
            $location_id=0;
        $data["location_id"]=$location_id;
        sort($data['deal_attr']);
        $data['page_title'] = $data['sub_name'];



        // 获取当前门店的商品数据
        if($supplier_location_list){

            $join = ' LEFT JOIN '.DB_PREFIX.'deal_location_link dl ON d.id = dl.deal_id ';

            foreach ($supplier_location_list as $key=>$value){
                $deal_ids[] = $value['id'];
            }
            $ext_con = join(',', $deal_ids);
            if(!$location_id){
                $location_id=$supplier_location_list[0]['id'];
            }
            if($data['is_shop']==0){
                $other_location_deal = get_deal_list(4,array(DEAL_ONLINE,DEAL_NOTICE), '', $join, " dl.location_id={$location_id} and id <> {$data_id} and d.is_shop =".$data['is_shop']." and ((d.is_coupon = 1 AND (d.coupon_end_time >= ".NOW_TIME." or d.coupon_end_time=0)) or d.is_coupon=0)");
            }else{
                $other_location_deal = array();
            }

            $temp_deal = array();
            foreach ($other_location_deal['list'] as $key=>$val){
                $temp_deal[] = format_short_deal_item($val);
            }
            $data['other_location_deal'] = $temp_deal;
            $data['count_other_location_deal'] = count($temp_deal)-1;
        }

        // 是否有优惠
        if($data['allow_promote'] == 1){
            // 获取1个全站优惠
            if(APP_INDEX=='wap')
                $promote_where=" and class_name <> 'Appdiscount'";
            $sql = " select * from ".DB_PREFIX."promote where type = '0' ".$promote_where."  order by id desc";
            $promotes = $GLOBALS['db']->getAll($sql);

            foreach ($promotes as $k=>$v){
                if($v['class_name']=='Appdiscount' || $v['class_name']=='Discountamount'){
                    $pro['content'] = $v['description'];
                    $pro['type'] = "minus";
                    $promotes_list[] = $pro;
                }else{
                    if(!$cate_id){
                        $pro['content'] = $v['description'];
                        $pro['type'] = "free";
                        $promotes_list[] = $pro;
                    }
                }
            }
        }
        $pro = array();
        $pro['type'] = "return";
        if($return_money>0){
            $pro['content'].="购买返现".round($return_money,2)."元";
        }
        if($return_score>0){
            $send_integral_data=send_integral_conf();
            if($send_integral_data['send_integral_switch']==0){
                if($pro['content']){
                    $pro['content'].=" ，";
                }
                $pro['content'].="购买返".$return_score."积分";
            }
        }
        if( $pro['content']){
            $promotes_list[] = $pro;
        }

        if($data['delivery_free_info']['is_open_delivery_free']==1 && $data['is_shop']==1 && $data['delivery_type']!=2){
            $pro['content']="满".round($data['delivery_free_info']['delivery_free_money'],2)."元免运费";
            $pro['type'] = "free";
            $promotes_list[] = $pro;
        }

        $data['promotes_list'] = $promotes_list;

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------
        /*if ($pro['type'] == "free"){
            $data['delivery_content'] = $pro['content'];
        }else{
            $data['delivery_content'] = "包邮";
        }*/
        if ($deal_info['carriage_template_id']>0){
            $sql = "select name from  ".DB_PREFIX."carriage_template  WHERE id =".$deal_info['carriage_template_id'];
            $data['delivery_content'] = $GLOBALS['db']->getOne($sql);;
        }else{
            $data['delivery_content'] = "包邮";
        }


        //关联商品数据
        //$relate_data = $this->getRelateData($data_id);
        if( !empty($relate_data) ){
            //app版本不需要
            $type = intval($_REQUEST['type']);//商品ID
            if( empty($type) ){
                unset($relate_data['attrArray']);
                unset($relate_data['stockArray']);
            }
            foreach ($relate_data['goodsList'] as $k=>$v){
                if(intval($v['id'])!=$data_id){
                    $format_relate_data['goodsList'][] = array(
                        'id' =>$v['id'],
                        'f_icon_middle' =>$v['f_icon_middle'],
                        'current_price'=>$v['current_price'],
                        'end_price'=>$v['end_price'],
                    );
                }

                if (count($format_relate_data['goodsList'])==3){
                    break;
                }
            }
            $data['relate_data'] = $format_relate_data;
        }

        //$data['check_deal_time']=check_deal_time($data_id);
        $data['check_deal_time']['DEAL_NOTICE']=DEAL_NOTICE;
        $data['check_deal_time']['DEAL_HISTORY']=DEAL_HISTORY;
        $data['check_deal_time']['COUPON_HISTORY']=COUPON_HISTORY;
        $data['collect_count']=$user_id>0?$GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_collect where deal_id=".$data_id):0;
        //$cart_list_data=load_cart_list(0,true);
        $cart_list_data=array();
        $data['cart_total_num'] = $cart_list_data['total_data']['total_num'];
        //$data['supplier_name']=$GLOBALS['db']->getOne("select name from ".DB_PREFIX."supplier where id=".$data['supplier_id']);
        //$data['supplier_name']="";
        //店铺信息 //标识is_pay_deposit 认证类型：authentication_type
        $supplier_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."supplier where id=".$data['supplier_id']);
        if ($supplier_info){
            $data['supplier_name']=$supplier_info['name'];
            $data['is_pay_deposit']=$supplier_info['is_pay_deposit'];
            $data['authentication_type']=$supplier_info['authentication_type'];
            $data['supplier_user_id']=$supplier_info['user_id'];
            $data['supplier_img']=get_spec_image($GLOBALS['db']->getOne("select head_image from " . DB_PREFIX . "user where id=" . intval($supplier_info['user_id'])));
            $authentication_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."supplier_authent_list where id=".$data['authentication_type']);
            if ($authentication_info){
                $data['authentication_type_image']=get_supplier_authentication_type_icon($data['authentication_type'],null);
                $data['authentication_type_content']=$authentication_info['name'];
            }
            if ($data['is_pay_deposit'])
            {
                $data['is_pay_deposit_image']=get_supplier_deposit_icon(null);
                $data['is_pay_deposit_content']="诚信保证金商户";
            }

        }else{
            /*$data['supplier_name']="这是店铺名称";
            $data['is_pay_deposit']=1;
            $data['is_pay_deposit_image']=SITE_DOMAIN.'/public/images/supplier_authent_icon/1_small.png';
            $data['is_pay_deposit_content']="诚信保证金商户";
            $data['authentication_type']=1;
            $data['authentication_type_image']=SITE_DOMAIN.'/public/images/supplier_authent_icon/1_small.png';
            $data['authentication_type_content']="实体店认证";*/
        }

        $data['avg_point']=sprintf("%.1f",$data['avg_point']);
        $data['share_title'] = $data['name'];
        //$data['share_img'] = $data['icon'];
        $data['share_imageUrl'] = $data['icon'];
        $data['share_content'] = $data['brief'];
        //$cart_data = get_cart_type($data_id,$app_index='wap');
        //$data['in_cart'] = $cart_data['in_cart'];
        $data['in_cart'] =0;
        //重新生成二维码链接json_url
        if($data['buy_type']==1)
            $json['type']=415;
        else
            $json['type']=414;
        $json['data_id']=$data['id'];
        if($GLOBALS['user_info'])
        {
            $json['user_id']=intval($GLOBALS['user_info']['id']);
            $data['json_url']= SITE_DOMAIN.url("index","deal",array('data_id'=>$data['id'],'r'=>base64_encode(intval($GLOBALS['user_info']['id'])),'json'=>base64_encode(json_encode($json))));

        }else{
            $data['json_url']= SITE_DOMAIN.url("index","deal",array('data_id'=>$data['id'],'json'=>base64_encode(json_encode($json))));
        }

        // 小能链接参数
        //$this->_getCustomServiceSetting($data,$data);
        //$data['user_login_status'] = check_login();

        if($data['is_presell']==1){
            $data['count_time'] = $data['presell_end_time']-NOW_TIME;
            $data['count_time'] = $data['count_time'] > 0 ? $data['count_time'] : 0;
            $tmpl_path = APP_ROOT_PATH.'wap/Tpl/main/fanwe/style5.2/inc/';
            $presell_css_path = $tmpl_path.'presell_css.html';
            $presell_css = file_get_contents($presell_css_path);
            $presell_rule_path = $tmpl_path.'presell_rule.html';
            $presell_rule = $presell_css.file_get_contents($presell_rule_path);
            $presell_notes_path = $tmpl_path.'presell_notes.html';
            $presell_notes = $presell_css.file_get_contents($presell_notes_path);
            $presell_notes = str_replace('{$data.notes}', $data['notes'], $presell_notes);
            $data['presell_rule'] = $presell_rule;  //预售规则
            $data['presell_notes'] = $presell_notes; //购买需知
            $data['name_prefix']=get_name_prefix_color(0);

        }elseif($data['is_pt']){
            $data['count_time']=$data['pt_end_time']-NOW_TIME;
            $data['count_time'] = $data['count_time'] > 0 ? $data['count_time'] : 0;
            $data['name_prefix']=get_name_prefix_color(1);
            $data['pt_other_group']=Pintuan::getPtGroupData($data['id'],$data['id']);//pt_id取消不用，统一用商品id
            $tmpl_path = APP_ROOT_PATH.'wap/Tpl/main/fanwe/style5.2/inc/';

            $pt_css = file_get_contents($tmpl_path.'presell_css.html');
            $pt_notes = $pt_css.file_get_contents($tmpl_path.'presell_notes.html');
            $pt_notes = str_replace('{$data.notes}', $data['notes'], $pt_notes);
            $data['pt_notes'] = $pt_notes; //购买需知

            $data['pt_acount'] = $GLOBALS['db']->getOne('select sum(join_user_limit) from '.DB_PREFIX.'pt_group 
                    where pt_id='.$data['id'].' 
                    and status = 1'." 
                    and expired_time>".NOW_TIME." 
                     ");
        }

        if(ENTRY_NAME=='nzy_o2o'){
            $data['is_display']=1;
        }


        //---------------------------
        $data['images_count'] = count($data['images']);

        if(intval($data['id'])==0)
        {

            $root['status'] = 0;
            $root['error']  = "商品不存在";
            api_ajax_return($root);
        }else{
            $data['detail_url'] = url("index", 'deal_detail', array('data_id'=>$data['id']) );
            $data['dp_url'] = url("index", 'dp_list', array('data_id'=>$data['id'], 'type'=>'deal') );


            // 优惠互动
            $data['promotes_list_arr'] = $data['promotes_list'];
            $data['promotes_list'] = join('，',  $data['promotes_list']);

            // 商家其它团购商品
            /*foreach ($data['other_location_deal'] as $k=>$v){
                $data['other_location_deal'][$k]['old_url'] =  url("index", 'deal', array('data_id'=>$v['id']) );

            }*/

            // 商家其它门店
            /*foreach ($data['supplier_location_list'] as $k=>$v){
                $data['supplier_location_list'][$k]['location_url'] =  url("index", 'store', array('data_id'=>$v['id']) );


                $data['supplier_location_list'][$k]['distance'] = format_distance_str($v['distance']);


            }*/

            //当前门店
            foreach ($data['supplier_location_list'] as $t => $v){
                if($v['id']==$location_id){
                    $data['supplier_0'] = $data['supplier_location_list'][$t];
                }else {
                    $data['supplier_0'] = $data['supplier_location_list'][0];
                }
            }


            //是否存在关联商品
            $relate_data = $data['relate_data'];
            /*if($relate_data){
                //goodsList wap展示为两个商品一组，需要改造一下
                $rsGoodsList = array();
                for( $k=0;$k<ceil(count($newGoodsList)/2);$k++ ){
                    $item1 = $newGoodsList[$k*2];
                    $item2 = $newGoodsList[$k*2+1];
                    if(!$item2){
                        $item1['widthP'] = '50%';
                    }else{
                        $item1['widthP'] = '100%';
                    }
                    $rsGoodsList[$k][] = $item1;
                    if($item2){
                        $item2['widthP'] = '100%';
                        $rsGoodsList[$k][] = $item2;
                    }
                }
                $root['goodsList'] = $rsGoodsList;
                $root['jsonDeal'] = json_encode($relate_data['dealArray']);
                $root['jsonAttr'] = json_encode($relate_data['attrArray']);
                $root['jsonStock'] = json_encode($relate_data['stockArray']);
            }*/
            $hasRelateGoods = !empty($relate_data)?1:0;

            //$GLOBALS['tmpl']->assign("hasRelateGoods",$hasRelateGoods);
            $root['hasRelateGoods'] = $hasRelateGoods;

            //$GLOBALS['tmpl']->assign("download",url("index","app_download"));
            $root['download'] = url("index","app_download");
            $data['presell_end_time'] = to_date($data['presell_end_time'],"Y/m/d H:i:s");//预售结束时间 2017/09/20 00:00:00
            /*if($data['is_presell']==1){
                require_once(APP_ROOT_PATH."system/model/deal.php");
                $data['name'] = format_deal_name($data['name']);
            }elseif($data['is_pt']==1){
                require_once(APP_ROOT_PATH."system/model/deal.php");
                $data['name'] = format_deal_name($data['name'],1);
            }*/
            //$GLOBALS['tmpl']->assign("share_img",$data['f_icon']);//微信分享LOGO
            //$root['share_img'] = $data['f_icon'];
            $root['share_imageUrl'] = $data['f_icon'];

            //$GLOBALS['tmpl']->assign("share_title",$data['sub_name']);//微信分享标题
            $root['share_title'] = $data['sub_name'];

            //$GLOBALS['tmpl']->assign("data",$data);
            $root['data'] = $data;
            /*if($data['is_presell']==1&&IS_PRESELL){
                $GLOBALS['tmpl']->assign("is_presell",1);
                $GLOBALS['tmpl']->display("presell_detail.html");
            }elseif($data['is_pt']==1&&IS_PT){
                $GLOBALS['tmpl']->assign("is_pt",1);
                $GLOBALS['tmpl']->display("pt_detail.html");
            }else{
                $GLOBALS['tmpl']->display("deal.html");
            }*/

        }

        //判断是否开播
        $is_vodeo=$GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."video where user_id=".$supplier_info['user_id']);
        if ($is_vodeo){
            $root['vodeo_content']='该商家正在直播';
        }else{
            $root['vodeo_content']='该商家未在直播';
        }

        //判断地址（用户地址）
        $address=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_address where user_id=".$user_id);
        //$address['consignee_district']='{"province":"","city":"北京市","area":"东城区"}';
        if ($address){
            $address_info=json_decode($address['consignee_district'],ture);
            $root['address_content']=$address_info['province'].$address_info['city'].$address_info['area'];
        } else{
            $root['address_content']='福建省福州市鼓楼区';
        }

        //评论
        $limit = '0,2';
        $root['data']['dp_list']=get_dp_list($limit,$param=array("deal_id"=>$data_id),"","");
        $sql = "select count(*) from ".DB_PREFIX."supplier_location_dp where  deal_id=".$data_id;
        $root['data']['dp_list_count']=$GLOBALS['db']->getOne($sql);

        //首页视频
        //$root['data']['video_url']='http://1804.vod.myqcloud.com/1804_8544aecedeba11e680aa5114891992e5.f30.mp4';
        $root['data']['video_url']=$deal_info['description_video'];

        //首页推荐商品is_recommend
        $root['data']['recommend_list']=$GLOBALS['db']->getAll("select id,name,sub_name,img,origin_price,current_price,buy_count from ".DB_PREFIX."deal where is_recommend=1 and  is_effect=1 and  supplier_id=".$data['supplier_id']." and id <>".$data_id);
        foreach ($root['data']['recommend_list'] as $l => $v){
            $root['data']['recommend_list'][$l]['img']=get_spec_image($v['img']);
        }
        //print_r("select id,name,sub_name,img,origin_price,current_price,buy_count from ".DB_PREFIX."deal where is_recommend=1 and  is_effect=1 and  supplier_id=".$data['supplier_id']." and id <>".$data_id);exit();

        api_ajax_return($root);
    }


    public function dp_list()
    {
        $user = $GLOBALS['user_info'];
        $user_id  = intval($user['id']);
        //商品id
        $data_id = intval($_REQUEST['data_id']);
        $page = intval($_REQUEST['page']);

        $root['status']=1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }
        $deal =   $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$data_id."' and is_delete = 0 or (uname= '".$data_id."' and uname <> '')");
        //评论
        $page_size=10;
        $page=$page?$page:1;
        $limit = (($page -1) * $page_size).','.$page_size;
        //$root['dp_list']=get_dp_list($limit,$param=array("deal_id"=>$data_id),"","");
        $dp_list = get_dp_list($limit,$param=array("deal_id"=>$data_id),"","");
        $format_dp_list = array();
        $dp_count = 0;
        if ($dp_list['list']) {
            $dp_count_sql = 'SELECT count(*) FROM '.DB_PREFIX.'supplier_location_dp where '.$dp_list['condition'];
            $dp_count = $GLOBALS['db']->getOne($dp_count_sql);
        }
        foreach($dp_list['list'] as $k=>$v){

            $temp_arr = array();

            $temp_arr['id'] = $v['id'];
            $temp_arr['create_time'] = $v['create_time'] > 0 ?to_date($v['create_time'],'Y-m-d'):'';
            $temp_arr['content'] = $v['content'];

            if($_REQUEST['from']=="app"){
                $temp_arr['content']=str_replace("<br/>","\n",$v['content']);
                $temp_arr['content']=str_replace("<br />","\n",$v['content']);
            }

            $temp_arr['reply_content']= $v['reply_content']?$v['reply_content']:'';
            $temp_arr['point'] = $v['point'];
            $temp_arr['point_percent'] = $v['point_percent'];
            $temp_arr['user_avatar'] = get_spec_image($GLOBALS['db']->getOne("select head_image from " . DB_PREFIX . "user where id =" . $v['user_id']));
            $temp_arr['format_show_date'] = $v['create_time'] > 0 ?format_show_date($v['create_time']):'';
            //$uinfo = load_user($v['user_id']);
            //$temp_arr['user_name'] = $uinfo['user_name'];
            $temp_arr['user_name'] = $GLOBALS['db']->getOne("select nick_name from " . DB_PREFIX . "user where id =" . $v['user_id']);
            $v['images'] = unserialize($v['images_cache']);

            $images = array();
            $oimages = array();

            if($v['images']){
                foreach ($v['images'] as $ik=>$iv){
                    $images[] = get_abs_img_root(get_spec_image($iv,400,400,1));
                    $oimages[] = get_spec_image($iv);
                }

            }
            $temp_arr['images'] = $images;
            $temp_arr['oimages'] = $oimages;

            $format_dp_list[] = $temp_arr;
        }
        $root['db_list'] = $format_dp_list;
        $root['dp_count']=$dp_count;
        $root['buy_type']=$deal['buy_type'];
        $root['supplier_info']['avg_point']=sprintf("%.1f",$deal['avg_point']);
        $root['supplier_info']['avg_point_percent']=$deal['avg_point']/5*100;
        $has_next = ($dp_count > $page * $page_size) ? 1 : 0;
        $root['has_next']=$has_next;
        $root['page']=$page;
        api_ajax_return($root);
    }


    /**
     * 收藏接口
     * 输入：
     * data_id: int 商品ID

     *
     *
     *
     * 输出：
     * is_collect [int] 0：未收藏 ，1已收藏
     *
     */
    public function add_collect()
    {
        //检查用户,用户密码
        $user = $GLOBALS['user_info'];
        $user_id  = intval($user['id']);
        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }

        $user_login_status = 1;
        {
            $root['user_login_status'] = $user_login_status;

            $goods_id = intval($_REQUEST['id']);
            $goods_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = ".$goods_id." and is_effect = 1 and is_delete = 0");
            if($goods_info)
            {
                $sql = "INSERT INTO `".DB_PREFIX."deal_collect` (`id`,`deal_id`, `user_id`, `create_time`) select '0','".$goods_info['id']."','".$user_id."','".get_gmtime()."' from dual where not exists (select * from `".DB_PREFIX."deal_collect` where `deal_id`= '".$goods_info['id']."' and `user_id` = ".$user_id.")";
                $GLOBALS['db']->query($sql);
                if($GLOBALS['db']->affected_rows()>0){
                    $root['is_collect'] = 1;
                    $root['error'] = "收藏成功";
                    $root['status'] = 1;
                }
                else
                {
                    $root['is_collect'] = 1;
                    $root['error'] = "您已经收藏了该商品";
                    $root['status'] = 1;
                }
                $root['collect_count']=$GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_collect where deal_id=".$goods_id);

            }else{
                $root['error'] = "商品信息错误";
                $root['status'] = 0;
            }
        }
        api_ajax_return($root);
    }

    public function del_collect()
    {
        //检查用户,用户密码
        $user = $GLOBALS['user_info'];
        $user_id  = intval($user['id']);

        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }
        $user_login_status = 1;
        {
            $root['user_login_status'] = $user_login_status;

            $goods_id = intval($_REQUEST['id']);
            $goods_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = ".$goods_id." and is_effect = 1 and is_delete = 0");
            if($goods_info)
            {
                $sql = "delete from  ".DB_PREFIX."deal_collect where user_id=".$user_id." and deal_id=".$goods_id;
                $GLOBALS['db']->query($sql);
                if($GLOBALS['db']->affected_rows()>0){
                    $root['is_collect'] = 0;
                    $root['error'] = "取消成功";
                    $root['status'] = 1;
                }
                else
                {
                    $root['is_collect'] = 0;
                    $root['error'] = "您已经取消了该商品";
                    $root['status'] = 1;
                }
                $root['collect_count']=$GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_collect where deal_id=".$goods_id);

            }else{
                $root['error'] = "商品信息错误";
                $root['status'] = 0;
            }
        }
        api_ajax_return($root);
    }

    public function collect_deal()
    {
        //检查用户,用户密码
        $user = $GLOBALS['user_info'];
        $user_id  = intval($user['id']);

        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }
        $user_login_status = 1;

        $root['user_login_status'] = $user_login_status;

        //$sql="select * from ".DB_PREFIX."deal_collect where user_id =  ".$user_id;
        //$sql="select * from ".DB_PREFIX."deal_collect where user_id > 0 ";
        $now_time=NOW_TIME;
        $page = intval($_REQUEST['page']);
        $page = $page == 0 ? 1 : $page;
        $page_size = 10;
        $limit = (($page - 1) * $page_size) . "," . $page_size;

        $sql = "select d.id,d.name,d.sub_name,d.end_time,d.origin_price,d.current_price,d.buy_count,d.buy_type,d.brief,d.icon, case when d.end_time < ".$now_time." and d.end_time > 0 then 0 else 1 end as is_valid,c.create_time as add_time ,c.id as cid from ".DB_PREFIX."deal_collect as c left join ".DB_PREFIX."deal as d on d.id = c.deal_id where c.user_id = ".$user_id." and d.is_delete = 0 and d.is_effect = 1 order by is_valid desc,c.create_time desc limit ".$limit;
        $sql_count = "select count(*) from ".DB_PREFIX."deal_collect where user_id = ".$user_id;
        $list = $GLOBALS['db']->getAll($sql);
        $count = $GLOBALS['db']->getOne($sql_count);
        foreach($list as $key=>$value){
            $list[$key]['icon']=get_spec_image($value['icon']);
        }
        $root['list']=$list;
        $has_next = ($count > $page * $page_size) ? 1 : 0;
        $root['has_next']=$has_next;
        $root['error']="";

        api_ajax_return($root);
}

    public function get_pt_list()
    {
        //检查用户,用户密码
        $user = $GLOBALS['user_info'];
        $user_id  = intval($user['id']);

        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }
        $user_login_status = 1;

        $root['user_login_status'] = $user_login_status;
        $deal_id = intval($_REQUEST['data_id']);
        $data = $GLOBALS['db']->getAll('select * from '.DB_PREFIX.'pt_group 
                    where pt_id='.$deal_id.' 
                    and status = 1'." 
                    and expired_time>".NOW_TIME." 
                    order by id desc limit 10");
        fanwe_require(APP_ROOT_PATH."system/model/Pintuan.php");
        $pintuan=new Pintuan();
        foreach($data as $key=>$value){
            $user_info_v=$GLOBALS['db']->getRow("select nick_name,head_image from " . DB_PREFIX . "user where id =" . $value['user_id']);
            $value['img']=get_spec_image($user_info_v['head_image']);
            $value['nick_name']=$user_info_v['nick_name'];
            $value['less_user_number']=$value['success_user_limit']-$value['join_user_limit'];
            $value['count_time']=$value['expired_time']-NOW_TIME;
            $value['count_time'] = $value['count_time'] > 0 ? $value['count_time'] : 0;
            $value['url']=url("index","pt_view#index",array("pt_group_id"=>$value['id'],"deal_id"=>$deal_id));
            $value['pt_group_list_img']=$pintuan->getGroupListImg($GLOBALS['db']->getRow('select * from '.DB_PREFIX.'pt_group where id = '.$value['id']),$GLOBALS['db']->getAll('select user_id,is_pt_head from '.DB_PREFIX.'pt_group_list where pt_group_id='.$value['id']));

            $pt_group_list[$key]=$value;
        }
        array_values($pt_group_list);
        $root['pt_list']=$pt_group_list;
        api_ajax_return($root);

    }

    public function pt_view(){
        $root['status'] = 1;
        $root['error']="";
        $data=array();
        $deal_id=intval($_REQUEST['deal_id']);
        $user_info  = $GLOBALS['user_info'];
        $pt_group_id=intval($_REQUEST['pt_group_id']);
        require_once(APP_ROOT_PATH."system/model/deal.php");
        fanwe_require(APP_ROOT_PATH."system/model/Pintuan.php");


        $pintuan=new Pintuan();
        $user_info['user_id']=$user_info['id'];
        $user_info['user_name']=$user_info['nick_name'];
        $pintuan->setPtUser($user_info);
        //$data = get_deal($deal_id);

        $data=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$deal_id."' and is_delete = 0 or (uname= '".$deal_id."' and uname <> '')");
        $deal_attr = array();
        if($data['deal_goods_type']){
            $deal_attr = $GLOBALS['db']->getAll("select id,name from ".DB_PREFIX."goods_type_attr where goods_type_id = ".$data['deal_goods_type']);
            foreach($deal_attr as $k=>$v)
            {
                $deal_attr[$k]['attr_list'] = $GLOBALS['db']->getAll("select id,name,is_checked from ".DB_PREFIX."deal_attr where deal_id = ".$data['id']." and goods_type_attr_id = ".$v['id']);
                if(!$deal_attr[$k]['attr_list'])
                    unset($deal_attr[$k]);
            }
        }
        $data['deal_attr'] = $deal_attr;
        $attr_stock_list =$GLOBALS['db']->getAll("SELECT id, deal_id, attr_cfg, stock_cfg, attr_str, buy_count, attr_key,price, add_balance_price, presell_deposit_money, presell_discount_money, presell_buy_count, pt_money, pt_buy_count FROM ".DB_PREFIX."attr_stock WHERE deal_id=".$data['id'],false);

        $group_id = $GLOBALS['user_info']['group_id'];
        $discount=1;
        $is_presell = 0;
        if($group_id && $data['allow_user_discount']){
            $group_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_group where id = ".$group_id);

            if($group_info && $group_info['discount']<1 && $is_presell==0){
                if($data['allow_user_fx_discount']&&$GLOBALS['user_info']['is_fx']){
                    $discount=$group_info['discount']*$data['user_fx_discount'];
                }else{
                    $discount=$group_info['discount'];
                }
                //$row['price'] = round($row['price']*$group_info['discount'],2);
            }elseif($data['allow_user_fx_discount'] && $GLOBALS['user_info']['is_fx'] && $is_presell==0){
                $discount=$data['user_fx_discount'];
            }
        }elseif($data['allow_user_fx_discount'] && $GLOBALS['user_info']['is_fx'] && $is_presell==0){
            $discount=$data['user_fx_discount'];
        }

        $attr_stock_data = array();
        foreach($attr_stock_list as $row)
        {
            $row['attr_cfg'] = unserialize($row['attr_cfg']);
            //$row['presell_deposit_money'] = round($row['presell_deposit_money'],2);

            $row['attr_price'] = round(($row['price']+$data['current_price'])*$discount,2);
            $row['price'] = round($row['price']*$discount,2);
            $attr_stock_data[$row['attr_key']] = $row;
        }

        if($_REQUEST['from']=='app'){
            $data['deal_attr_stock_json'] = count($attr_stock_data)>0?json_encode($attr_stock_data):'';
        }else{
            $data['deal_attr_stock_json'] = json_encode($attr_stock_data);
        }
        //获取真实库存（非属性库存 在属性库存不存在时候使用）
        $data['deal_stock'] = intval($GLOBALS['db']->getOne("select stock_cfg from ".DB_PREFIX."deal_stock where deal_id = ".$data['id']));
        if($data['is_pt']==1){
            $data['is_pt'] = 1;
            $data['pt_id']=$data['id'];
            $data['pt_start_time']="";
            $data['pt_end_time']="";
            $data['pt_success_user_limit']=$data['pt_team_num'];
            $data['pt_success_time_limit']=$data['pt_useful_time'];
            $data['pt_cate_id']=0;
        }else{
            $data['is_pt']=0;
            $data['pt_start_time']=0;
            $data['pt_end_time']=0;
        }

        $data = format_deal_item($data);
        if($data['is_pt']){
            $data['name_prefix']=get_name_prefix_color(1);
        }
        $data['url']=url("index","deal#index",array("data_id"=>$data['id']));
        $tmpl_path = APP_ROOT_PATH.'wap/Tpl/main/fanwe/style5.2/inc/';
        $pt_rule_path = $tmpl_path.'pt_view_rule.html';
        $pt_rule = file_get_contents($pt_rule_path);
        $pt_notes_path = $tmpl_path.'pt_view_notes.html';
        $pt_notes = file_get_contents($pt_notes_path);
        $pt_notes = str_replace('{$data.notes}', $data['notes'], $pt_notes);
        if(APP_INDEX=='app'){
            $pt_rule=str_replace('{$active}','active',$pt_rule);
        }else{
            $pt_rule=str_replace('{$active}','',$pt_rule);
        }
        $data['pt_rule'] = $pt_rule;
        $data['pt_notes'] = $pt_notes;

        $status_info=$pintuan->checkSuccessGroup($pt_group_id);
        $data['check_pt_status']=$status_info['status'];
        $data['check_pt_msg']=$status_info['msg'];
        $data['check_pt_type']=$status_info['type'];
        $data['pt_group_id']=$pt_group_id;
        $data['pt_other_group']=$pintuan->getPtGroupData($data['id'],$data['pt_id'],$pt_group_id);

        $pt_group_info=$status_info['pt_group_info'];
        $data['pt_less_user_number']=$pt_group_info['success_user_limit']-$pt_group_info['join_user_limit'];
        $data['pt_count_time']=$pt_group_info['expired_time']-NOW_TIME;
        $data['pt_count_time']=$data['pt_count_time']>0?$data['pt_count_time']:0;
        $data['pt_group_list_img']=$pintuan->getGroupListImg($status_info['pt_group_info'],$status_info['pt_group_list_info']);
        $data['page_title']="拼团详情";
        //$data['pt_group_info']=$pt_group_info;

        //首页推荐商品is_recommend
        $data['recommend_list']=$GLOBALS['db']->getAll("select id,name,sub_name,img,origin_price,current_price,buy_count from ".DB_PREFIX."deal where is_recommend=1 and  is_effect=1 and  supplier_id=".$data['supplier_id']." and id <>".$deal_id);
        foreach ($data['recommend_list'] as $l => $v){
            $data['recommend_list'][$l]['img']=get_spec_image($v['img']);
        }

        //是否已参与
        if($status_info['status']==4){
            $data['has_join']=1;
        }else{
            $data['has_join']=0;
        }

        $root['data']=$data;
        api_ajax_return($root);
    }


    public function pt_share(){
        $root['status'] = 1;
        $root['error']="";
        $data=array();
        $user_ids=json_decode($_REQUEST['user_ids'],true);
        $user_info  = $GLOBALS['user_info'];
        $user_id  = intval($user_info['id']);
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }
        $user_login_status = 1;
        $user_infos=array();
        if (count($user_ids)>0){
            $user_infos=$GLOBALS['db']->getAll("select * from ".DB_PREFIX."user where  id  in (".implode(",", $user_ids).")");
        }else{
            $root['status'] = 0;
            $root['error']  = "分享队列为空";
            api_ajax_return($root);
        }

        require_once(APP_ROOT_PATH."system/model/deal.php");
        fanwe_require(APP_ROOT_PATH."system/model/Pintuan.php");
        $pintuan=new Pintuan();
        $user_info['user_id']=$user_info['id'];
        $user_info['user_name']=$user_info['nick_name'];
        $pintuan->setPtUser($user_info);

        $pt_group_id=intval($_REQUEST['pt_group_id']);
        $pt_group=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."pt_group where  id  =".$pt_group_id);
        if(!$pt_group_id){
            $root['status'] = 0;
            $root['error']  = "拼团信息错误";
            api_ajax_return($root);
        }

        $deal_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$pt_group['pt_id']."'");

        $m_config =  load_auto_cache("m_config");

        $str=$user_info['nick_name']."用户邀请您参与《".$deal_info['sub_name']."》的拼团，原价".$deal_info['current_price']."，拼团价".$deal_info['pt_money'];
        $user_info_sender=getuserinfo($user_info['id'],$user_info['id'],$user_info['id']);

        //-----------------------------------
        $ext2 = array();
        $ext2['type'] = 90; //发起拼团邀请
        $ext2['str']=$str;
        $ext2['time']=NOW_TIME;
        $ext2['deal_id']=$pt_group['pt_id'];
        $ext2['pt_group_id']=$pt_group_id;
        $ext2['deal_img']=get_spec_image($deal_info['icon']);

        //$user_info['head_image']=get_spec_image($user_info['head_image']);//发送人头像
        //$ext2['sender']=$user_info_sender['user'];
        if ($_REQUEST['sdk_type']=='android'){
            $ext2['deviceType']='Android';
        }else{
            $ext2['deviceType']=$_REQUEST['sdk_type'];
        }
        $root['ext']=$ext2;
        $root['user_ids']=$user_ids;
        $root['error']  = "分享成功";
        api_ajax_return($root);

        //推送
        foreach ($user_infos as $l => $v){

            $user_id=$v['id'];

            $system_user_id =$m_config['tim_identifier'];//系统消息
            $ext2 = array();
            $ext2['type'] = 90; //发起拼团邀请
            $ext2['str']=$str;
            $ext2['time']=NOW_TIME;
            $ext2['deal_id']=$pt_group['pt_id'];
            $ext2['pt_group_id']=$pt_group_id;
            $ext2['deal_img']=$deal_info['icon'];

            //$user_info['head_image']=get_spec_image($user_info['head_image']);//发送人头像
            $ext2['sender']=$user_info_sender['user'];
            if ($_REQUEST['sdk_type']=='android'){
                $ext2['deviceType']='Android';
            }else{
                $ext2['deviceType']=$_REQUEST['sdk_type'];
            }

            /*$sender = array();
            $sender['user_id'] = $user_info['id'];//发送人昵称
            $sender['nick_name'] = $user_info['nick_name'];//发送人昵称
            $sender['nick_name'] = emoji_decode($user_info['nick_name']);//发送人昵称
            $sender['head_image'] = get_spec_image($user_info['head_image']);//发送人头像
            $sender['user_level'] = $user_info['user_level'];//用户等级
            $sender['v_icon'] = $user_info['v_icon'];//认证图标

            $ext2['sender'] = $sender;
            $ext2['user_id'] = $user_id;
            $ext2['user'] = $v;*/
            $msg_content2 = array();
            //创建array 所需元素
            $msg_content_elem2 = array(
                'MsgType' => 'TIMCustomElem',       //自定义类型
                'MsgContent' => array(
                    'Data' => json_encode($ext2),
                    'Desc' => ''
                )
            );
            //将创建的元素$msg_content_elem, 加入array $msg_content
            array_push($msg_content2, $msg_content_elem2);
            fanwe_require(APP_ROOT_PATH.'system/tim/TimApi.php');
            $api = createTimAPI();
            $ret2 = $api->openim_send_msg2($system_user_id, (string)$user_id, $msg_content2);
            //print_r($ret2);exit();
            if($ret2['ActionStatus'] != 'OK') {
                //二次处理
                $ret2 = $api->openim_send_msg2($system_user_id, (string)$user_id, $msg_content2);
            }

            //友盟
            fanwe_require(APP_ROOT_PATH . 'system/schedule/android_list_schedule.php');
            fanwe_require(APP_ROOT_PATH . 'system/schedule/ios_list_schedule.php');

            fanwe_require(APP_ROOT_PATH . 'system/schedule/android_file_schedule.php');
            fanwe_require(APP_ROOT_PATH . 'system/schedule/ios_file_schedule.php');
            fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/BaseRedisService.php');
            fanwe_require(APP_ROOT_PATH . 'mapi/lib/redis/UserFollwRedisService.php');

            /*//$user_type = $GLOBALS['db']->getRow("SELECT apns_code,device_type FROM " . DB_PREFIX . "user WHERE id=" . $user_id);
            $user_type=$v;
            if (intval($user_type['device_type']) == 1) {
                $apns_app_code_list   = array();
                $apns_app_code_list[] = $user_type['apns_code'];

                $AndroidList = new android_list_schedule();
                $data        = array(
                    'dest'    => implode(",", $apns_app_code_list),
                    'content' => $str,
                    'user_id' => $user_id,
                    'room_id' => 0,
                    'group_id' => 0,
                    'deal_id' => $pt_group['pt_id'],
                    'pt_group_id' => $pt_group_id,
                    'type'    => 9,
                );
                $ret_android = $AndroidList->exec($data);
            } elseif (intval($user_type['device_type']) == 2) {
                $apns_ios_code_list   = array();
                $apns_ios_code_list[] = $user_type['apns_code'];

                $IosList  = new ios_list_schedule();
                $ios_data = array(
                    'dest'    => implode(",", $apns_ios_code_list),
                    'content' => $str,
                    'user_id' => $user_id,
                    'room_id' => 0,
                    'group_id' => 0,
                    'deal_id' => $pt_group['pt_id'],
                    'pt_group_id' => $pt_group_id,
                    'type'    => 9,
                );
                $ret_ios = $IosList->exec($ios_data);
            }*/

        }
        $root['error']  = "分享成功";
        api_ajax_return($root);

    }

    public function pt_crontab()
    {
        //查询过期团
        $pt_list = $GLOBALS['db']->getAll("select * from " . DB_PREFIX . "pt_group where status=1 and expired_time<" . NOW_TIME);
        foreach ($pt_list as $k => $v) {
            $this->pt_end($v['id']);
        }
    }


    //定时判断成功失败
    public function pt_end($pt_group_id){

        //更新团状态,开团失败
        $GLOBALS['db']->query("update ".DB_PREFIX."pt_group set status = 3 where id=".$pt_group_id);

        //判断是否成团->否->所有参团用户进行退款
        $order_list = $GLOBALS['db']->getAll("select 
    											o.id,
    											o.user_id,
    											o.pay_status,
    											o.total_price,
    											o.order_status,
    											o.deal_total_price,
    											o.delivery_fee,
    											o.pt_group_status,
    											oi.id as order_item_id,
    											oi.deal_id,
    											oi.refund_status,
    											oi.attr_str,
    											oi.number,
    											oi.unit_price,
    											oi.delivery_status,
    											oi.name,
    											oi.supplier_id 
    											 from ".DB_PREFIX."deal_order o 
    											 left join ".DB_PREFIX."deal_order_item oi on o.id = oi.order_id 
    											 where 
    											 o.is_pt_order=1 
    											 and o.pay_status=2 
    											 and oi.refund_status=0 
    											 and o.pt_group_id = ".$pt_group_id);

        $content = "开团失败系统自动退款";

        foreach ($order_list as $k => $v) {
            $price = $v['unit_price']*$v['number']+$v['delivery_fee'];
            $order_id = $v['id'];
            $order_item_id = $v['order_item_id'];

            //更新订单状态
            $GLOBALS['db']->query("update ".DB_PREFIX."deal_order_item set 
	    													refund_money=refund_money+{$price}, 
											    			refund_status = 2,
											    			is_arrival = 0, 
											    			admin_memo='{$content}' where id = ".$order_item_id);


            $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set 
															refund_amount = refund_amount + ".$price.",
															refund_money = refund_money + ".$price.",
															refund_status = 2,
															after_sale = 1,
															is_refuse_delivery=0 
															where id = ".$order_id);

            //更新库存状态
            $attr_stock = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."attr_stock 
															where deal_id = ".$v['deal_id']." and locate(attr_str,'".$v['attr_str']."')");
            if($attr_stock)
            {
                if($attr_stock['stock_cfg']>=0)
                {
                    $sql = "update ".DB_PREFIX."attr_stock set 
				        								stock_cfg = stock_cfg + ".$v['number']." 
				        								where deal_id = ".$v['deal_id']." and locate(attr_str,'".$v['attr_str']."') > 0 ";
                }
            }

            $GLOBALS['db']->query($sql);

            if($price>0)
            {
                /*require_once(APP_ROOT_PATH."system/model/user.php");

                modify_account(array("money"=>$price,"is_admin"=>1,"supplier_id"=>$v['supplier_id']),
                    $v['user_id'],
                    $v['name']." 退款成功");
                modify_statements($price, 6, $v['name']."用户退款");*/
                require_once(APP_ROOT_PATH."system/libs/user.php");
                $m_config =  load_auto_cache("m_config");
                modify_account(array("diamonds"=>$price*10),
                    $v['user_id'],
                    $v['name']." 拼团失败，退款".($price*10).$m_config['diamonds_name']);

            }



            //order_log($v['name']."退款成功 ".format_price($price)." ".$content, $order_id);
            $data['id'] = 0;
            $data['log_info'] = $v['name']."退款成功 ".format_price($price)." ".$content;
            $data['log_time'] = NOW_TIME;
            $data['order_id'] = $order_id;
            $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order_log", $data);
            fanwe_require(APP_ROOT_PATH.'mapi/shop/cart.action.php');
            $cart = new cartCModule();
            $cart->auto_over_status($order_id);
            $cart->update_order_cache($order_id);


        }


    }

}
function get_location_list($limit, $param=array("cid"=>0,"tid"=>0,"aid"=>0,"qid"=>0,"city_id"=>0,"supplier_id"=>0), $join = '', $where='',$orderby = '',$field_append="", $groupby='',$is_deal_cate=true)
{
    return false;
    if(empty($param))
        $param=array("cid"=>0,"tid"=>0,"aid"=>0,"qid"=>0,"city_id"=>0,"supplier_id"=>0);

    $tname = "sl";
    $tname_field = "sl.id,
                    sl.name,
                    sl.address,
                    sl.preview,
                    sl.index_img,
                    sl.tel,
                    sl.contact,
                    sl.supplier_id,
                    sl.open_time,
                    sl.is_main,
                    sl.city_id,
                    sl.xpoint,
                    sl.ypoint,
                    sl.deal_cate_id,
                    sl.is_recommend,
                    sl.is_verify,
                    sl.avg_point,
                    sl.total_point,
                    sl.dp_count,
                    sl.open_store_payment,
                    sl.is_dc,
                    sl.dc_online_pay,
                    sl.supplier_promote,
                    sl.is_close
                    ";

    $condition = ' '.$tname.'.is_effect = 1 ';
    if($is_deal_cate){
        $condition .=' and '.$tname.'.deal_cate_id > 0 ';
    }
    $param_condition = build_location_filter_condition($param,$tname);
    $condition.=" ".$param_condition;

    if($where != '')
    {
        $condition.=" and ".$where;
    }

    if ($groupby) {
        if($join)
            $sql = "select ".$tname_field.$field_append." from ".DB_PREFIX."supplier_location as ".$tname." ".$join." where  ".$condition.$groupby;
        else
            $sql = "select ".$tname_field.$field_append." from ".DB_PREFIX."supplier_location as ".$tname." where  ".$condition.$groupby;
    }else{
        if($join)
            $sql = "select ".$tname_field.$field_append." from ".DB_PREFIX."supplier_location as ".$tname." ".$join." where  ".$condition;
        else
            $sql = "select ".$tname_field.$field_append." from ".DB_PREFIX."supplier_location as ".$tname." where  ".$condition;
    }



    if($orderby=='')
        $sql.=" order by ".$tname.".sort desc ";
    else
        $sql.=" order by ".$orderby;

    if($limit)
        $sql .= " limit ".$limit;

    $location_list = $GLOBALS['db']->getAll($sql);

    if($location_list)
    {
        foreach($location_list as $k=>$store)
        {

            $durl = url("index","store#".$store['id']);
            $store['share_url'] = SITE_DOMAIN.$durl;
            $store['url'] = $durl;
            $store['good_rate_precent'] = round($store['good_rate']*100,1);
            $store['ref_avg_price'] = round($store['ref_avg_price'],2);

            //生成二维码链接json_url
            $json['type']=430;
            $json['data_id']=$store['id'];


            if($GLOBALS['user_info'])
            {
                if(app_conf("URL_MODEL")==0)
                {
                    $store['share_url'] .= "&r=".base64_encode(intval($GLOBALS['user_info']['id']));
                }
                else
                {
                    $store['share_url'] .= "?r=".base64_encode(intval($GLOBALS['user_info']['id']));
                }

                $json['user_id']=intval($GLOBALS['user_info']['id']);
                $store['json_url']  = SITE_DOMAIN.url("index","store#".$store['id'],array('r'=>base64_encode(intval($GLOBALS['user_info']['id'])),'json'=>base64_encode(json_encode($json))));
            }else{
                $store['json_url']  = SITE_DOMAIN.url("index","store#".$store['id'],array('json'=>base64_encode(json_encode($json))));
            }
            $store['app_url'] = SITE_DOMAIN.url("index","store#index",array('data_id'=>$store['id']))."&page_finsh=1";
            $location_list[$k] = $store;
        }
    }

    return array('list'=>$location_list,'condition'=>$condition);
}

/**
 * 格式化商品的返回数据
 * @param unknown_type $v
 */
function format_deal_item($deal)
{
//     echo 90572744%86400>0?intval(90572744/86400)."天以上":90572744/86400;exit;
    $deal['name']=htmlspecialchars_decode($deal['name']);
    $deal['sub_name']=htmlspecialchars_decode($deal['sub_name']);
    $data['current_price_fixed'] = round($deal['current_price'],2);


    $data['id'] = $deal['id'];
    $data['name'] = $deal['name'];
    $data['sub_name'] = $deal['sub_name'];
    $data['brief'] = $deal['brief'];
    $data['max_bought'] = $deal['max_bought'];
    $data['current_price'] = round($deal['current_price'],2);
    $data['origin_price'] = round($deal['origin_price'],2);
    $data['icon'] = get_abs_img_root(get_spec_image($deal['icon'],300,182,1));
    //2016-09-18 改版数据
    //补零处理
    $f_current_price = sprintf("%01.2f",$deal['current_price']);
    $data['f_current_price'] = $f_current_price;
    $data['f_current_price_arr'] = explode(".",$f_current_price);
    $data['f_origin_price'] = sprintf("%01.2f",$deal['origin_price']);
    //倒计时
    $data['f_end_time'] = $deal['end_time'] - NOW_TIME;
    $data['supplier_id'] = $deal['supplier_id'];
    $data['f_icon'] = get_abs_img_root(get_spec_image($deal['icon'],370,370,1));
    $data['f_icon_middle'] = get_abs_img_root(get_spec_image($deal['icon'],184,184,1));
    $data['percent'] = $deal['percent']; //算好的评分百分比
    $data['phone_description'] = $deal['phone_description'];
    //end 2016-09-18 改版数据
    $data['begin_time'] = $deal['begin_time'];
    $data['end_time'] = $deal['end_time'];
    $data['time_status'] = $deal['time_status'];
    $data['now_time'] = NOW_TIME;
    $data['buy_count'] = $deal['buy_count'];//总销量(已包含预售销量)
    $data['buy_type'] = $deal['buy_type'];
    $data['is_shop'] = $deal['is_shop'];
    if($data['buy_type']==1)
        $data['return_score_show'] = abs($deal['return_score']);
    $data['deal_attr'] = $deal['deal_attr'];
    $data['avg_point'] = round($deal['avg_point'],2);
    $data['dp_count'] = $deal['dp_count'];
    $data['supplier_location_count'] = $deal['supplier_location_count'];
// 	[less_time] => 90576716
// 	[less_time_format] => 1048天以上
    $data['last_time'] = intval($deal['end_time'])>NOW_TIME?(intval($deal['end_time'])-NOW_TIME):0;
    $data['last_time_format'] = $data['last_time']%86400>0?intval($data['last_time']/86400)."天以上":$data['last_time']/86400;

    $data['delivery_type'] = $deal['delivery_type'];
    $deal_tags = $deal['deal_tags'];
    $deal_tags_txt = array("0元抽奖","免预约","多套餐","可订座","折扣券","过期退","随时退","七天退","免运费","满立减","","闪送","平台自营","货到付款","厂家直发","当日达","需预订","全国联保");
    $deal_tags_icon = array("&#xe61a;","&#xe622;","&#xe628;","&#xe627;","&#xe626;","&#xe621;","&#xe620;","&#xe625;","&#xe617;","&#xe623;","&#xe623;","&#xe623;","&#xe623;","&#xe623;","&#xe623;","&#xe623;");
    $data['deal_tags'] = array();
    if(ENTRY_NAME=='nzy_o2o'){
        //鸟之鱼隐藏随时退标签
        foreach($deal_tags as $k=>$v)
        {
            if($v!=6){
                $tag['k'] = $v;
                $tag['v'] = $deal_tags_txt[$v];
                $tag['icon'] = $deal_tags_icon[$v];
                $data['deal_tags'][$k] = $tag;
            }
        }
    }else{
        foreach($deal_tags as $k=>$v)
        {
            $tag['k'] = $v;
            $tag['v'] = $deal_tags_txt[$v];
            $tag['icon'] = $deal_tags_icon[$v];
            $data['deal_tags'][$k] = $tag;
        }
    }
    sort($data['deal_tags']);
    $images = array();
    $oimages = array();
    $f_images = array();
    foreach ($deal['image_list'] as $k=>$v){
        $images[] = get_spec_image(($v['img']));
        $oimages[] = get_spec_image($v['img']);
        //2016-09-18改版数据
        $f_images[] = get_spec_image(($v['img']));
    }
    $data['images'] = $images;
    $data['oimages'] = $oimages;
    $data['f_images'] = $f_images;
    //$data['description']=get_abs_img_root($deal['description']);
    if($GLOBALS['distribution_cfg']['OSS_TYPE']&&$GLOBALS['distribution_cfg']['OSS_TYPE']!="NONE")
    {
        $domain = $GLOBALS['distribution_cfg']['OSS_DOMAIN'];
    }
    else
    {
        $domain = SITE_DOMAIN.APP_ROOT;
    }
    //$out = str_replace(APP_ROOT."./public/",$domain."/public/",$deal['description']);
    $out = str_replace("./public/",$domain."/public/",$deal['description']);
    $data['description']=$out;
    //$data['description']=$deal['description'];
    $data['notes']=get_spec_image($deal['notes']);
    $data['set_meal']=get_spec_image($deal['set_meal']);
    $data['share_url'] = $deal['share_url'];
    $data['ypoint'] = floatval($deal['ypoint']);
    $data['xpoint'] = floatval($deal['xpoint']);
    $data['buyin_app'] = intval($deal['buyin_app']);
    $data['is_fx'] = intval($deal['is_fx']);
    $data['allow_promote'] = intval($deal['allow_promote']);
    $data['auto_order'] = intval($deal['auto_order']);
    $data['expire_refund'] = intval($deal['expire_refund']);
    $data['any_refund'] = intval($deal['any_refund']);

    //库存信息
    $data['deal_stock'] = $deal['deal_stock'];
    $data['deal_attr_stock_json'] = $deal['deal_attr_stock_json'];
    $data['user_min_bought'] = $deal['user_min_bought'];
    $data['user_max_bought'] = $deal['user_max_bought'];
    $data['allow_user_discount'] = $deal['allow_user_discount'];
    $data['is_delivery'] = $deal['is_delivery'];
    $data['json_url'] = $deal['json_url'];
    $data['delivery_free_info'] = $deal['delivery_free_info'];

    //预售信息
    $data['presell_type'] = $deal['presell_type'];//0.订金1.定金
    $data['presell_deposit_money'] = round($deal['presell_deposit_money'],2);//预售订金
    $data['presell_discount_money'] = round($deal['presell_discount_money'],2);//抵扣金额
    $data['presell_buy_count'] = $deal['presell_buy_count'];//预售销量
    $data['is_presell'] = $deal['is_presell'];//预售销量
    //拼团信息
    $data['is_pt'] = $deal['is_pt'];
    $data['pt_id']=$deal['pt_id'];
    $data['pt_money']=number_format($deal['pt_money'],2);
    $data['pt_start_time']=$deal['pt_start_time'];
    $data['pt_end_time']=$deal['pt_end_time'];
    $data['pt_success_user_limit']=$deal['pt_success_user_limit'];
    $data['pt_success_time_limit']=$deal['pt_success_time_limit'];
    $data['pt_cate_id']=$deal['pt_cate_id'];
    $data['pt_buy_count']=$deal['pt_buy_count'];
    $data['pt_cut_money']=number_format($deal['current_price']-$deal['pt_money'],2);//拼图价相对售价的优惠
    //补零处理
    $f_presell_deposit_money = sprintf("%01.2f",$deal['presell_deposit_money']);
    $data['f_presell_deposit_money'] = $f_presell_deposit_money;
    $data['f_presell_deposit_money_arr'] = explode(".",$f_presell_deposit_money);
    $data['f_presell_discount_money'] = sprintf("%01.2f",$deal['presell_discount_money']);
    $data['presell_end_time'] = $deal['presell_end_time'];//预售结束时间 2017/09/20 00:00:00

    //新增商品描述（图+字）、商品信息、品牌承诺
    if($deal['deal_img']){
        $data['deal_img'] = get_abs_img_root(get_spec_image($deal['deal_img'],150,220,1));
    }else{
        $data['brand_img'] = '';
    }
    $data['deal_text'] = $deal['deal_text'];
    $data['deal_info']=get_spec_image($deal['deal_info']);
    if($deal['brand_img']){
        $data['brand_img'] = get_abs_img_root(get_spec_image($deal['brand_img'],375,130,1));
    }else{
        $data['brand_img'] = '';
    }

    //商品参与会员分销价
    $data['allow_user_fx_discount'] = $deal['allow_user_fx_discount'];
    $data['user_fx_discount'] = $deal['user_fx_discount'];
    $data['user_fx_price'] = round($deal['current_price']*$deal['user_fx_discount'],2);
    $data['user_fx_price'] = $f_user_fx_price_arr = sprintf("%01.2f",$data['user_fx_price']);
    $data['f_user_fx_price_arr']=explode(".",$f_user_fx_price_arr);

    //
    if($GLOBALS['user_info']['is_fx']){
        //加入网宝跳转的url
        $data['user_fx_url']=SITE_DOMAIN.url("index", 'uc_fx');
        //最终计算的商品金额
        if($data['allow_user_fx_discount']){
            $data['end_price'] = $data['user_fx_price'];
        }else{
            $data['end_price'] = $data['f_current_price'];
        }
    }else{
        $data['user_fx_url']=SITE_DOMAIN.url("index", 'uc_fx#vip_buy');
        $data['end_price'] = $data['f_current_price'];
    }

    return $data;
}

function get_dp_list($limit,$param=array("deal_id"=>0,"youhui_id"=>0,"event_id"=>0,"location_id"=>0,"tag"=>""),$where="",$orderby="")
{
    if (empty($param))
        $param = array("deal_id" => 0, "youhui_id" => 0, "event_id" => 0, "location_id" => 0, "tag" => "");

    $condition = " 1=1 ";

    if ($param['deal_id'] > 0) {
        $condition .= " and deal_id = " . $param['deal_id'] . " ";
    } elseif ($param['youhui_id'] > 0) {
        $condition .= " and youhui_id = " . $param['youhui_id'] . " ";
    } elseif ($param['event_id'] > 0) {
        $condition .= " and event_id = " . $param['event_id'] . " ";
    } elseif ($param['supplier_id'] > 0) {
        $condition .= " and supplier_id = " . $param['supplier_id'] . " ";
    } elseif ($param['location_id'] > 0) {
        $condition .= " and supplier_location_id = " . $param['location_id'] . " ";
    }

    if ($param['tag'] != "") {
        $tag_unicode = str_to_unicode_string($param['tag']);
        $condition .= " and (match(tags_match) against('" . $tag_unicode . "' IN BOOLEAN MODE)) ";
    }

    if ($where != '') {
        $condition .= " and " . $where;
    }

    $sql = "select * from " . DB_PREFIX . "supplier_location_dp where  " . $condition;

    if ($orderby == '')
        $sql .= " order by is_index desc,create_time desc limit " . $limit;
    else
        $sql .= " order by is_index desc," . $orderby . " limit " . $limit;

    $dp_list = $GLOBALS['db']->getAll($sql);


    if ($dp_list) {
        foreach ($dp_list as $k => $v) {
            if ($v['is_img'] == 1)
                $dp_list[$k]['images'] = unserialize($v['images_cache']);
            if ($dp_list[$k]['images']) {
                foreach ($dp_list[$k]['images'] as $k1 => $v1) {
                    $dp_list[$k]['images'][$k1] = get_spec_image($v1);
                }
            }


            if ($v['user_id']) {
                $head_image = $GLOBALS['db']->getOne("select head_image from " . DB_PREFIX . "user where id =" . $v['user_id']);
                $dp_list[$k]['head_image'] = get_spec_image($head_image);
                if (!$dp_list[$k]['head_image']) {
                    $dp_list[$k]['head_image'] = 'http://q.qlogo.cn/qqapp/1105519446/74D8EE43442C907E47A5C8A7609F320D/100';
                }
                $dp_list[$k]['user_name'] = substr($v['user_id'], 0, 1) . "*****" . substr($v['user_id'], -1, 1);
            } else {
                $dp_list[$k]['head_image'] = 'http://q.qlogo.cn/qqapp/1105519446/74D8EE43442C907E47A5C8A7609F320D/100';
                $dp_list[$k]['user_name'] = "3*****78";
            }
// 			$dp_list[$k]['images'] = array(
// 				"http://p0.meituan.net/deal/0bd31cc0be3a91f4ff1bc785c4cd5567100264.jpg",
// 				"./public/attachment/201202/16/11/4f3c7f1d37dea.jpg",
// 				"./public/attachment/201202/16/11/4f3c7ea394a90.jpg",
// 				"./public/attachment/201201/4f0110c586c48.jpg",
// 				"./public/attachment/201201/4f0113ce66cd4.jpg",
// 				"http://p1.meituan.net/deal/cdba2654c4b3493f45052c831166ee1e479253.jpg",
// 				"http://p0.meituan.net/deal/__49109589__8248077.jpg",

// 			);
            $dp_list[$k]['content'] = nl2br($v['content']);
            $dp_list[$k]['create_time_format'] = to_date($v['create_time'], "Y-m-d");
            $dp_list[$k]['point_percent'] = $v['point'] / 5 * 100;
        }
    }
    return array('list' => $dp_list, 'condition' => $condition);
}

