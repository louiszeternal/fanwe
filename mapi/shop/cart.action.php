<?php
// +----------------------------------------------------------------------
// | FANWE 直播系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://www.fanwe.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 云淡风轻(88522820@qq.com)
// +----------------------------------------------------------------------

class cartCModule  extends baseModule
{

    /**
     * 获取购物车列表
     *
     * 输入:
     * 无
     *
     * 输出:
     * is_score: int 当前购物车中的商品类型 0:普通商品，展示时显示价格 1:积分商品，展示时显示积分
     * cart_list: object 购物车列表内容，结构如下
     * Array
    (
    [478] => Array key [int] 购物车表中的主键
    (
    [id] => 478 [int] 同key
    [return_score] => 0 [int] 当is_score为1时单价的展示
    [return_total_score] => 0 [int] 当is_score为1时总价的展示
    [unit_price] => 108 [float] 当is_score为0时单价的展示
    [total_price] => 108 [float] 当is_score为0时总价的展示
    [number] => 1 [int] 购买件数
    [deal_id] => 57 [int] 商品ID
    [attr] => 287,290 [string] 购买商品的规格ID组合，用逗号分隔的规格ID
    [name] => 桥亭活鱼小镇 仅售88元！价值100元的代金券1张 [9点至18点,2-5人套餐] [string] 商品全名，包含属性
    [sub_name] => 88元桥亭活鱼小镇代金券 [9点至18点,2-5人套餐] [string] 商品缩略名，包含属性
    [max] => int 最大购买量 加减时用
    [icon] => string 商品图标 140x85
    )
    )

     * total_data: array 购物车总价统计,结构如下
     *	Array
    (
    [total_price] => 108 [float] 当is_score为0时的总价显示
    [return_total_score] => 0 [int] 当is_score为1时的总价显示
    )
     *  user_login_status:int 用户登录状态(1 已经登录/0 用户未登录) 该接口不返回临时登录状态，未登录时使用手机短信验证自动注册登录，已登录时判断is_mobile
     *  has_mobile: int 是否有手机号 0无 1有
     */
    public function index()
    {
        $root = array();
        $user_info=$GLOBALS['user_info'];
        $user_id = intval($GLOBALS['user_info']['id']);
        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }

        //把购物车中用户当时设为无效的购物记录设为有效
        //$GLOBALS['db']->query("update ".DB_PREFIX."deal_cart set is_effect=1 where user_id = " . $user_info['id'] );

        if( $_REQUEST['from']=='wap'){
            $is_wap=true;
        }else{
            $is_wap=false;
        }

        $cart_result = $this->load_cart_list(0,$wap_show_disable=true);

        $cart_list_o = $cart_result['cart_list'];
        $cart_list_data = array();

        $total_data_o = $cart_result['total_data'];
        $is_score = 0;
        foreach($cart_list_o as $k=>$v)
        {
            $bind_data = array();
            $bind_data['id'] = $v['id'];
            if($v['buy_type']==1)
            {
                $is_score = 1;
                $bind_data['return_score'] = abs($v['return_score']);
                $bind_data['return_total_score'] = abs($v['return_total_score']);
                $bind_data['unit_price'] = 0;
                $bind_data['total_price'] = 0;
            }
            else
            {
                $bind_data['return_score'] = 0;
                $bind_data['return_total_score'] = 0;
                $bind_data['unit_price'] = round($v['unit_price'],2);
                $bind_data['current_price'] = round($v['current_price'],2);
                $bind_data['total_price'] = round($v['total_price'],2);
            }
            $bind_data['number'] = $v['number'];
            $bind_data['supplier_id'] = $v['supplier_id'];
            $bind_data['deal_id'] = $v['deal_id'];


            $bind_data['attr'] = $v['attr'];
            $bind_data['attr_str'] = $v['attr_str'];
            if($bind_data['attr']){
                $deal_name=$GLOBALS['db']->getOne("select name from ".DB_PREFIX."deal where id=".$v['deal_id']);
                $bind_data['deal_name']=$deal_name;
                $max_bought=$GLOBALS['db']->getRow("select stock_cfg from ".DB_PREFIX."attr_stock where deal_id=".$v['deal_id']." and attr_str='".$v['attr_str']."'");
                /* $deal_user_unpaid_count = intval($GLOBALS['db']->getOne("select sum(oi.number) from ".DB_PREFIX."deal_order_item as oi left join ".DB_PREFIX."deal_order as o on oi.order_id = o.id where o.user_id = ".intval($GLOBALS['user_info']['id'])." and o.pay_status <> 2 and o.order_status = 0 and oi.deal_id = ".$v['deal_id']." and o.is_delete = 0 and oi.attr_str like '%".$v['attr_str']."%'"));
                $num=$max_bought['max_bought']-$deal_user_unpaid_count; */
                if($max_bought['stock_cfg']>0){
                    $bind_data['max_bought']=$max_bought['stock_cfg'];
                    /* if($num>0){
                        $bind_data['max_bought'] = $num;
                    }else {
                        $bind_data['max_bought'] = 0;
                    } */
                }else{
                    $bind_data['max_bought']=-1;
                }
            }else {

                $max_bought=$GLOBALS['db']->getRow("select max_bought from ".DB_PREFIX."deal where id=".$v['deal_id']);
                if($max_bought['max_bought']>0){
                    $bind_data['max_bought']=$max_bought['max_bought'];
                }
            }
            if($v['user_min_bought']){
                $bind_data['user_min_bought'] = $v['user_min_bought'];
            }
            if($v['user_max_bought']){
                $bind_data['user_max_bought'] = $v['user_max_bought'];
            }
            $bind_data['is_effect'] = $v['is_effect'];
            $bind_data['is_disable'] = $v['is_disable'];
            $deal_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$v['deal_id']."' and is_delete = 0 or (uname= '".$v['deal_id']."' and uname <> '')");


            //规格属性选择
            $deal_attr = array();
            if($deal_info['deal_goods_type']){
                $deal_attr = $GLOBALS['db']->getAll("select id,name from ".DB_PREFIX."goods_type_attr where goods_type_id = ".$deal_info['deal_goods_type']);
                foreach($deal_attr as $k1=>$v1)
                {
                    $deal_attr[$k1]['attr_list'] = $GLOBALS['db']->getAll("select id,name,is_checked from ".DB_PREFIX."deal_attr where deal_id = ".$deal_info['id']." and goods_type_attr_id = ".$v1['id']);
                    if(!$deal_attr[$k1]['attr_list'])
                        unset($deal_attr[$k1]);
                }
            }
            $deal_info['deal_attr'] = $deal_attr;
            //开始输出库存json
            $attr_stock_list =$GLOBALS['db']->getAll("SELECT id, deal_id, attr_cfg, stock_cfg, attr_str, buy_count, attr_key,price, add_balance_price, presell_deposit_money, presell_discount_money, presell_buy_count, pt_money, pt_buy_count FROM ".DB_PREFIX."attr_stock WHERE deal_id=".$deal_info['id'],false);

            $group_id = $GLOBALS['user_info']['group_id'];
            $discount=1;
            $is_presell = 0;
            if($group_id && $deal_info['allow_user_discount']){
                $group_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_group where id = ".$group_id);

                if($group_info && $group_info['discount']<1 && $is_presell==0){
                    if($deal_info['allow_user_fx_discount']&&$GLOBALS['user_info']['is_fx']){
                        $discount=$group_info['discount']*$deal_info['user_fx_discount'];
                    }else{
                        $discount=$group_info['discount'];
                    }
                    //$row['price'] = round($row['price']*$group_info['discount'],2);
                }elseif($deal_info['allow_user_fx_discount'] && $GLOBALS['user_info']['is_fx'] && $is_presell==0){
                    $discount=$deal_info['user_fx_discount'];
                }
            }elseif($deal_info['allow_user_fx_discount'] && $GLOBALS['user_info']['is_fx'] && $is_presell==0){
                $discount=$deal_info['user_fx_discount'];
            }
            $attr_stock_data = array();
            foreach($attr_stock_list as $row)
            {
                $row['attr_cfg'] = unserialize($row['attr_cfg']);
                //$row['presell_deposit_money'] = round($row['presell_deposit_money'],2);

                $row['attr_price'] = round(($row['price']+$deal_info['current_price'])*$discount,2);
                $row['price'] = round($row['price']*$discount,2);
                $attr_stock_data[$row['attr_key']] = $row;
            }

            if($_REQUEST['from']=='app'){
                $deal_info['deal_attr_stock_json'] = count($attr_stock_data)>0?json_encode($attr_stock_data):'';
            }else{
                $deal_info['deal_attr_stock_json'] = json_encode($attr_stock_data);
            }
            //获取真实库存（非属性库存 在属性库存不存在时候使用）
            $deal_info['deal_stock'] = intval($GLOBALS['db']->getOne("select stock_cfg from ".DB_PREFIX."deal_stock where deal_id = ".$deal_info['id']));


            $bind_data['deal_attr_stock_json'] = $deal_info['deal_attr_stock_json'];
            $bind_data['deal_attr'] = $deal_info['deal_attr'];
            //如果商家或者平台有重新编辑过商品属性，更新购物车商品属性信息
            //sys_cart_attr($bind_data['deal_id'],$bind_data['attr_str']);
            //$bind_data['attr_str_format'] = $GLOBALS['db']->getOne("select group_concat(name) from ".DB_PREFIX."deal_attr where deal_id=".$v['deal_id']." and id in (".$v['attr'].") group by deal_id");

            $bind_data['name'] = htmlspecialchars_decode($v['name']);
            $bind_data['sub_name'] = htmlspecialchars_decode($v['sub_name']);
            $bind_data['max'] = 100;
            $bind_data['allow_user_discount'] = $v['allow_user_discount'];
            $bind_data['allow_user_fx_discount'] = $v['allow_user_fx_discount'];
            $bind_data['user_fx_discount'] = $v['user_fx_discount'];
            if($is_wap){
                $bind_data['icon'] = get_abs_img_root(get_spec_image($v['icon'],280,280,1)) ;
                $bind_data['f_icon'] = get_abs_img_root(get_spec_image($v['icon'],280,280,1)) ;
            }else{
                $bind_data['icon'] = get_abs_img_root(get_spec_image($v['icon'],140,85,1)) ;
                $bind_data['f_icon'] = get_abs_img_root(get_spec_image($v['icon'],140,85,1)) ;
            }

            $cart_list_data[] = $bind_data;
        }


        $total_data = array();

        if($is_score)
        {
            $total_data['total_num'] = $total_data_o['total_num'];
            $total_data['total_price'] = 0;
            $total_data['return_total_score'] = abs($total_data_o['return_total_score']);
        }
        else
        {
            $total_data['total_num'] = $total_data_o['total_num'];
            $total_data['total_price'] = round($total_data_o['total_price'],2);
            $total_data['return_total_score'] = 0;
        }

        $root['total_data'] = $total_data;
        $root['is_score'] = $is_score;

        //$user_login_status = check_login();


        if($GLOBALS['user_info']['mobile']=="")
            $root['has_mobile'] = 0;
        else
            $root['has_mobile'] = 1;

        /*if($user_login_status==LOGIN_STATUS_TEMP)
        {
            $user_login_status = LOGIN_STATUS_LOGINED; //购物车页不存在临时状态
        }*/

        //$root['user_login_status'] = $user_login_status;
        $root['user_login_status'] = 1;
        $root['page_title'] = "购物车";

        $user_discount_percent = 1;
        if (!empty($user_info['id'])) {
            $user_discount_percent = $this->getUserDiscount($user_info['id']);
        }
        if($cart_list_data)
        {


            $goods_list = $cart_list_data;
            $cart_list=array();
            foreach($cart_list_data as $k=>$v){
                //最大购买量
                //本团购当前会员已付款的数量
                $deal_user_paid_count = intval($GLOBALS['db']->getOne("select sum(oi.number) from ".DB_PREFIX."deal_order_item as oi left join ".DB_PREFIX."deal_order as o on oi.order_id = o.id where o.user_id = ".intval($GLOBALS['user_info']['id'])." and o.pay_status = 2 and oi.deal_id = ".$v['deal_id']." and o.is_delete = 0"));
                //本团购当前会员未付款的数量
                $deal_user_unpaid_count = intval($GLOBALS['db']->getOne("select sum(oi.number) from ".DB_PREFIX."deal_order_item as oi left join ".DB_PREFIX."deal_order as o on oi.order_id = o.id where o.user_id = ".intval($GLOBALS['user_info']['id'])." and o.pay_status <> 2 and o.order_status = 0 and oi.deal_id = ".$v['deal_id']." and o.is_delete = 0"));
                if($v['user_max_bought']>0){
                    $v['user_max_bought']=$v['user_max_bought']-$deal_user_paid_count-$deal_user_unpaid_count;
                    if($v['user_max_bought']<=0){
                        $v['user_max_bought']=1;
                    }
                }

                $attr = array();
                $attr['attr_id'] = explode("_",$v['attr']);
                $attr['attr_str'] = $v['attr_str'];
                $check_info = $this->check_deal_status($v['deal_id'],$attr,0);

                if($check_info['stock']<10 && $check_info['stock']!=-1){
                    $v['stock'] = $check_info['stock'] ;
                }

                $unit_price = $v['unit_price'];
                $user_discount = 1;
                if ($v['allow_user_discount']) {
                    if($v['allow_user_fx_discount']==1&&$user_info['is_fx']){
                        $user_discount = $user_discount_percent * $v['user_fx_discount'];
                    }else{
                        $user_discount = $user_discount_percent;
                    }//$unit_price = round($unit_price * $user_discount_percent, 2);
                }else{
                    if($v['allow_user_fx_discount']==1&&$user_info['is_fx']){
                        $user_discount =$v['user_fx_discount'];
                    }
                }
                $unit_price = round($unit_price * $user_discount, 2);
                $v['unit_price'] = $unit_price;
                $bai = floor($unit_price);
                $fei = str_pad(round(($unit_price - $bai) * 100,2),2,'0',STR_PAD_LEFT);
                $v['unit_price_format'] = "&yen; <i class='j-goods-money'>" .$bai.".</i>".$fei;
                $v['url'] = url("index","deal",array("data_id"=>$v['deal_id']));
                $v['check_info'] = $check_info;
                $v['allow_promote'] = intval($GLOBALS['db']->getOne("select allow_promote from ".DB_PREFIX."deal where id=".$v['deal_id']));
                if($check_info['status']==0){
                    $root['total_data']['total_price']-=$v['total_price'];
                    $root['total_data']['total_num']-=1;
                    $cart_list['disable'][$v['id']]=$v;
                }else{
                    if($v['is_disable']==1){
                        //$data['total_data']['total_price']-=$v['total_price'];
                        //$data['total_data']['total_num']-=1;
                        $v['check_info']['status']=0;
                        $v['check_info']['info']='商品更改属性，商品失效';
                        $cart_list['disable'][$v['id']]=$v;
                    }else{

                        $cart_list[$v['supplier_id']][]=$v;
                    }
                }

            }
            /*
             $allow_promote = 1; //默认为支持促销接口
             foreach($goods_list as $k=>$v)
             {
             $allow_promote = $GLOBALS['db']->getOne("select allow_promote from ".DB_PREFIX."deal where id = ".$v['deal_id']);
             if($allow_promote == 0)
             {
             break;
             }
             }
             */
            //满减计算
            if(APP_INDEX=='app'){
                $promote_where=" class_name in ('Discountamount','Appdiscount') ";
            }else{
                $promote_where=" class_name='Discountamount'";
            }
            $promote_where .= ' AND type = 0';
            $promote_obj = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."promote where ".$promote_where);
            $root['total_data']['discount_amount'] = 0;
            $total=$root['total_data']['total_price'];
            if($promote_obj){
                $promote_arr=array();
                foreach ($promote_obj as $t => $v){
                    $promote_arr[$t]=unserialize($v['config']);
                    if($total >= $promote_arr[$t]['discount_limit']){
                        $root['total_data']['total_price']-=$promote_arr[$t]['discount_amount'];
                        $root['total_data']['discount_amount'] += $promote_arr[$t]['discount_amount'];
                    }
                }
                $root['promote_cfg'] = $promote_arr;
            }else{
                $root['promote_cfg'] = array();
            }

            $cart_list_new=array();

            foreach($cart_list as $k=>$v){
                $cart_list_new[$k]['id']=$k;

                $supplier_name = $GLOBALS['db']->getOne("select name from ".DB_PREFIX."supplier where id=".intval($k));


                $youhui_sql="select count(*) from ".DB_PREFIX."youhui where 
		                    youhui_type=2 and is_effect=1 and (total_num>user_count or total_num=0) and (end_time>".NOW_TIME." or end_time=0)
	                        and (begin_time=0 or begin_time<".NOW_TIME.") and supplier_id=".intval($k).' and scope_goods in(0, 1)';

                $cart_list_new[$k]['youhui_count']=$GLOBALS['db']->getOne($youhui_sql);
                $cart_list_new[$k]['supplier_id']=intval($k);
                if($k==='disable'){
                    $cart_list_new[$k]['id']=0;
                    $cart_list_new[$k]['supplier_name']='失效商品';
                }else{

                    $cart_list_new[$k]['supplier_name']=$supplier_name?$supplier_name:'平台自营';
                }
                sort($v);
                $cart_list_new[$k]['list'] =$v;
                $is_effect=1;
                foreach($v as $kk=>$vv){
                    if($vv['is_effect']==0){
                        $is_effect=0;
                        break;
                    }
                }
                $cart_list_new[$k]['is_effect'] =$is_effect;
            }
            //print_r($cart_list_new);exit;
            rsort($cart_list_new);

            $root['cart_list'] = $cart_list_new?$cart_list_new:null;

            $bai = floor($root['total_data']['total_price']);
            $fei = str_pad(round(($root['total_data']['total_price'] - $bai) * 100,2),2,'0',STR_PAD_LEFT);
            $root['total_data']['total_price_format'] = "&yen; <i class='j-price-int'>" .$bai."</i>.<em class='j-price-piont'>".$fei."</em>";
        }else{

            $root['total_data']['total_price_format'] = "&yen; <i class='j-price-int'>0</i>.<em class='j-price-piont'>00</em>";
        }
        api_ajax_return($root);
    }

    /**
     *
     * @param bool $ajaxReturn 是否以ajax返回
     * @param array $param 该值不为空，则加入购物车的id,attr以此为准，否则取$_REQUEST
     */

    public function addcart()
    {
        $user_id = intval($GLOBALS['user_info']['id']);
        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }
        $id = intval($_REQUEST['id']);
        $deal_info =  $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$id."' and is_delete = 0 ");
        if(!$deal_info)
        {
            $root['status'] = 0;
            $root['error'] = "没有可以购买的产品";
            api_ajax_return($root);
        }

        //拼团
        $is_pt_buy=intval($_REQUEST['is_pt_buy']);
        $pt_group_id=intval($_REQUEST['pt_group_id']);
        if($is_pt_buy){
            fanwe_require(APP_ROOT_PATH.'system/model/Pintuan.php');
            $Pintuan=new Pintuan();
            $user_info=$GLOBALS['user_info'];
            $user_info['user_id']=$user_id;
            $user_info['user_name']=$user_info['nick_name'];
            $Pintuan->setPtUser($user_info);
            //判断拼团商品已经开团的是否有效
            if($pt_group_id){
                $checkSuccessGroupData=$Pintuan->checkSuccessGroup($pt_group_id);
                if($checkSuccessGroupData['status']!==1){
                    $root['status'] = 0;
                    $root['error'] = $checkSuccessGroupData['msg'];
                    api_ajax_return($root);
                }
            }
        }


        //检测团购
        /*$check = check_deal_time($id);
        if($check['status'] == 0)
        {
            $res['status'] = 0;
            $res['info'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];
            if($ajaxReturn){
                ajax_return($res);
            }else{
                return $res;
            }
        }*/

        $attr =$_REQUEST['deal_attr'];
        $attr=json_decode($attr,true);
        //trace($attr);
        //if( $id==84 ){trace($attr);}



        $data=$deal_info;
        //规格属性选择
        $deal_attr = array();
        if($data['deal_goods_type']){
            $deal_attr = $GLOBALS['db']->getAll("select id,name from ".DB_PREFIX."goods_type_attr where goods_type_id = ".$data['deal_goods_type']);
            foreach($deal_attr as $k=>$v)
            {
                $deal_attr[$k]['attr_list'] = $GLOBALS['db']->getAll("select id,name,is_checked from ".DB_PREFIX."deal_attr where deal_id = ".$data['id']." and goods_type_attr_id = ".$v['id']);
                if(!$deal_attr[$k]['attr_list'])
                    unset($deal_attr[$k]);
            }
        }
        $deal_info['deal_attr'] = $deal_attr;

        if(count($attr)!=count($deal_info['deal_attr']))
        {
            $root['status'] = 0;
            $root['error'] = "请选择商品规格";
            api_ajax_return($root);
        }
        else
        {
            //加入购物车处理，有提交属性， 或无属性时
            $attr_str = '0';
            $attr_name = '';
            $attr_name_str = '';
            $attr_key = '';

            if($attr)
            {
                foreach($attr as $kk=>$vv)
                {
                    //$attr[$kk] = intval($vv[0]);
                    $attr[$kk] = intval($vv);
                }

                $attr_str = implode(",",$attr);
                $attr_key = implode('_', $attr);
                $attr_names = $GLOBALS['db']->getAll("select name from ".DB_PREFIX."deal_attr where id in(".$attr_str.")");

                $attr_name = '';
                foreach($attr_names as $attr)
                {
                    $attr_name .=$attr['name'].",";
                    $attr_name_str.=$attr['name'];
                }
                $attr_name = substr($attr_name,0,-1);
            }

            $verify_code = md5($id."_".$attr_str);
            $session_id = es_session::id();

            /*if(app_conf("CART_ON")==0)
            {
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where session_id = '".$session_id."'");
                $this->load_cart_list(true);
            }*/

            $cart_data=$this->get_cart_type($id,'wap',array("is_pt_buy"=>$is_pt_buy));

            if($cart_data['in_cart']==0){  //如果是不经过购物车的商品下单，先删掉之前的购物车历史记录
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where user_id = '".intval($GLOBALS['user_info']['id'])."' and deal_id=".$id);

            }else if($id>0&&$is_pt_buy==0){
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where user_id = '".intval($GLOBALS['user_info']['id'])."' and in_cart=0 and deal_id=".$id);
            }
            $cart_result = $this->load_cart_list($id);
            foreach($cart_result['cart_list'] as $k=>$v)
            {
                if($v['verify_code']==$verify_code)
                {
                    $cart_item = $v;
                }
            }


            $_REQUEST['number'] = !empty($param['number'])?intval($param['number']):intval($_REQUEST['number']);

            $add_number = $number = intval($_REQUEST['number'])<=0?1:intval($_REQUEST['number']);


            //开始运算购物车的验证
            if($cart_item)
            {
                //属性库存的验证
                $attr_setting_str = '';
                if($cart_item['attr']!='')
                {
                    $attr_setting_str = $cart_item['attr_str'];
                }



                if($attr_setting_str!='')
                {

                    $check = $this->check_deal_number_attr($cart_item['deal_id'],$attr_setting_str,$add_number);
                    if($check['status']==0)
                    {
                        $root['status'] = 0;
                        $root['error'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];
                        api_ajax_return($root);
                    }
                }

                $check = $this->check_deal_number($cart_item['deal_id'],$add_number);
                if($check['status']==0)
                {
                    $root['status'] = 0;
                    $root['error'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];
                    api_ajax_return($root);
                }
                //属性库存的验证
            }
            else //添加时的验证
            {
                //属性库存的验证
                $attr_setting_str = '';
                if($attr_name_str!='')
                {
                    $attr_setting_str =$attr_name_str;
                }



                if($attr_setting_str!='')
                {

                    $check = $this->check_deal_number_attr($deal_info['id'],$attr_setting_str,$add_number);
                    if($check['status']==0)
                    {

                        $root['status'] = 0;
                        $root['error'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];
                        api_ajax_return($root);
                    }
                }

                $check = $this->check_deal_number($deal_info['id'],$add_number);
                if($check['status']==0)
                {
                    $root['status'] = 0;
                    $root['error'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];
                    api_ajax_return($root);

                }
                //属性库存的验证
            }

            if($deal_info['return_score']<0)
            {
                //需要积分兑换
                $user_score = intval($GLOBALS['db']->getOne("select score from ".DB_PREFIX."user where id = ".intval($GLOBALS['user_info']['id'])));
                if($user_score < abs(intval($deal_info['return_score'])*$add_number))
                {

                    $root['status'] = 0;
                    $root['error'] = $check['info']." ".$GLOBALS['lang']['NOT_ENOUGH_SCORE'];
                    api_ajax_return($root);

                }
            }

            //验证over

            if(!$cart_item || $cart_data['in_cart']==0)
            {
                /*$attr_price = $GLOBALS['db']->getOne("select sum(price) from ".DB_PREFIX."deal_attr where id in($attr_str)");
                $add_balance_price = $GLOBALS['db']->getOne("select sum(add_balance_price) from ".DB_PREFIX."deal_attr where id in($attr_str)");*/
                $add_balance_price = 0;
                $attr_price = 0;

                if ($attr_name_str) {
                    $add_price_sql = "SELECT price, add_balance_price,presell_deposit_money,presell_discount_money FROM " . DB_PREFIX . "attr_stock WHERE deal_id=" . $id . " AND locate (attr_str,'".$attr_name_str."')";
                    $add_price_info = $GLOBALS['db']->getRow($add_price_sql);
                    $add_balance_price = $add_price_info['add_balance_price'];
                    $attr_price = $add_price_info['price'];
                }



                $cart_item['session_id'] = $session_id;
                $cart_item['user_id'] = intval($GLOBALS['user_info']['id']);
                $cart_item['deal_id'] = $id;
                //属性
                if($attr_name != '')
                {
                    $cart_item['name'] = $deal_info['name']." [".$attr_name."]";
                    $cart_item['sub_name'] = $deal_info['sub_name']." [".$attr_name."]";
                }
                else
                {
                    $cart_item['name'] = $deal_info['name'];
                    $cart_item['sub_name'] = $deal_info['sub_name'];
                }
                $cart_item['attr'] = $attr_str;
                $cart_item['add_balance_price'] = $add_balance_price;
                $unit_price = $deal_info['current_price'] + $attr_price;
                //如果是拼团
                if($is_pt_buy){
                    if ($attr_str) {
                        $deal_info['pt_money'] = $GLOBALS['db']->getOne("select pt_money from ".DB_PREFIX."attr_stock where attr_str='{$attr_name_str}' and deal_id='{$id}'");
                        //if($user_id==100436){log_result("select pt_money from ".DB_PREFIX."attr_stock where attr_str='{$attr_name_str}' and deal_id='{$id}'");exit();}
                    }
                    $unit_price = floatval($deal_info['pt_money']);

                }
                $cart_item['unit_price'] = $unit_price;
                $cart_item['number'] = $number;
                $cart_item['total_price'] = $unit_price * $cart_item['number'];
                $cart_item['verify_code'] = $verify_code;
                $cart_item['create_time'] = NOW_TIME;
                $cart_item['update_time'] = NOW_TIME;
                $cart_item['return_score'] = $deal_info['return_score'];
                $cart_item['return_total_score'] = $deal_info['return_score'] * $cart_item['number'];
                $cart_item['return_money'] = $deal_info['return_money'];
                $cart_item['return_total_money'] = $deal_info['return_money'] * $cart_item['number'];
                $cart_item['buy_type']	=	$deal_info['buy_type'];
                $cart_item['supplier_id']	=	$deal_info['supplier_id'];
                $cart_item['attr_str'] = $attr_name_str;
                $cart_item['is_pick'] = $deal_info['is_pick'];
                if($is_pt_buy){
                    $cart_item['is_pt_buy']=1;
                    $pt_info = $this->is_pt_deal($deal_info['id']);
                    $cart_item['pt_id'] = $pt_info['id'];
                    $cart_item['pt_group_id']=$pt_group_id;
                    $cart_item['return_total_score'] = 0;
                    $cart_item['return_total_money'] = 0;
                }else{
                    $cart_item['is_pt_buy']=0;
                    $cart_item['pt_id'] = 0;
                }
                $cart_item['in_cart'] = $cart_data['in_cart'];
                $cart_item['is_effect'] = 1;
                $GLOBALS['db']->autoExecute(DB_PREFIX."deal_cart",$cart_item);

            }
            else
            {
                if($number>0)
                {
                    $cart_item['number'] += $number;
                    $cart_item['in_cart'] = $cart_data['in_cart'];
                    $cart_item['total_price'] = $cart_item['unit_price'] * $cart_item['number'];
                    $cart_item['return_total_score'] = $deal_info['return_score'] * $cart_item['number'];
                    $cart_item['return_total_money'] = $deal_info['return_money'] * $cart_item['number'];
                    $cart_item['is_effect'] = 1;
                    if($is_pt_buy){
                        $cart_item['is_pt_buy']=1;
                        $pt_info = $this->is_pt_deal($deal_info['id']);
                        $cart_item['pt_id'] = $pt_info['id'];
                        $cart_item['pt_group_id']=$pt_group_id;
                        $cart_item['return_total_score'] = 0;
                        $cart_item['return_total_money'] = 0;
                    }else{
                        $cart_item['is_pt_buy']=0;
                        $cart_item['pt_id'] = 0;
                    }
                    $GLOBALS['db']->autoExecute(DB_PREFIX."deal_cart",$cart_item,"UPDATE","id=".$cart_item['id']);
                }
            }



            $this->syn_cart(); //同步购物车中的状态 cart_type
            $cart_result = $this->load_cart_list(0,true);
            $cart_total = count($cart_result['cart_list']);
            $res['cart_total'] = $cart_total;
            //$GLOBALS['tmpl']->assign("cart_total",$cart_total);

            //$relate_list = get_deal_list(4,array(DEAL_ONLINE),array("cid"=>$deal_info['cate_id'],"city_id"=>$GLOBALS['city']['id']),"","d.id<>".$deal_info['id']);

            //$GLOBALS['tmpl']->assign("relate_list",$relate_list['list']);

            //$res['html'] = $GLOBALS['tmpl']->fetch("inc/pop_cart.html");
            $res['status'] = 1;
            $res['in_cart']=$cart_data['in_cart'];
            $res['jump']=$cart_data['jump'];
            api_ajax_return($res);

        }
    }



    /**
     * 提交修改购物车，并生成会员接口
     *
     * 输入
     * num: 购物车列表的数量修改 array
     * 结构如下
     * Array(
     * 	"123"=>1  key[int] 购物车主键   value[int] 数量
     * )
     *
     * mobile string 手机号
     * sms_verify string 手机验证码
     *
     * 输出
     * status: int 状态 0失败 1成功
     * info: string 消息
     * user_data: 当前的会员信息，用于同步本地信息 array
     * Array(
     * 	id:int 会员ID
     *  user_name:string 会员名
     *  user_pwd:string 加密过的密码
     *  email:string 邮箱
     *  mobile:string 手机号
     *  is_tmp: int 是否为临时会员 0:否 1:是
     * )
     */
    public function check_cart()
    {

        $root = array();
        $root['status'] = 1;
        $num_req = $_REQUEST['num'];
        $num = array();
        foreach ($num_req as $k=>$v)
        {
            $sv = intval($v);
            if($sv)
                $num[$k] = intval($sv);
        }
        $user_mobile = strim($_REQUEST['mobile']);
        $sms_verify = strim($_REQUEST['sms_verify']);
        $user_login_status = 1;

        $user_info=$GLOBALS['user_info'];
        $user_id = intval($GLOBALS['user_info']['id']);
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }

        if( $_REQUEST['from']=='wap'){
            $is_wap=true;
        }else{
            $is_wap=false;
        }



        /*if($user_login_status==LOGIN_STATUS_NOLOGIN)
        {
            //自动创建会员或手机登录
            if(app_conf("SMS_ON")==0)
            {
                return output($root,0,"短信功能未开启");
            }
            if($user_mobile=="")
            {
                return output($root,0,"请输入手机号");
            }
            if($sms_verify=="")
            {
                return output($root,0,"请输入收到的验证码");
            }

            $sql = "DELETE FROM ".DB_PREFIX."sms_mobile_verify WHERE add_time <=".(NOW_TIME-SMS_EXPIRESPAN);
            $GLOBALS['db']->query($sql);

            $mobile_data = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."sms_mobile_verify where mobile_phone = '".$user_mobile."'");

            if($mobile_data['code']==$sms_verify)
            {
                //开始登录
                //1. 有用户使用已有用户登录
                //2. 无用户产生一个用户登录
                require_once(APP_ROOT_PATH."system/model/user.php");

                $user_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user where mobile = '".$user_mobile."'");
                $GLOBALS['db']->query("delete from ".DB_PREFIX."sms_mobile_verify where mobile_phone = '".$user_mobile."'");
                if($user_info)
                {
                    //使用已有用户
                    $result = do_login_user($user_info['user_name'],$user_info['user_pwd']);

                    if($result['status'])
                    {

                        $s_user_info = es_session::get("user_info");
                        $userdata['id'] = $s_user_info['id'];
                        $userdata['user_name'] = $s_user_info['user_name'];
                        $userdata['user_pwd'] = $s_user_info['user_pwd'];
                        $userdata['email'] = $s_user_info['email'];
                        $userdata['mobile'] = $s_user_info['mobile'];
                        $userdata['is_tmp'] = $s_user_info['is_tmp'];

                    }
                    else
                    {
                        if($result['data'] == ACCOUNT_NO_EXIST_ERROR)
                        {
                            $field = "";
                            $err = "用户不存在";
                        }
                        if($result['data'] == ACCOUNT_PASSWORD_ERROR)
                        {
                            $field = "";
                            $err = "密码错误";
                        }
                        if($result['data'] == ACCOUNT_NO_VERIFY_ERROR)
                        {
                            $field = "";
                            $err = "用户未通过验证";
                        }
                        return output($root,0,$err);
                    }
                }
                else
                {
                    //ip限制
                    $ip = get_client_ip();
                    $ip_nums = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."user where login_ip = '".$ip."'");
                    if($ip_nums>intval(app_conf("IP_LIMIT_NUM"))&&intval(app_conf("IP_LIMIT_NUM"))>0)
                    {
                        return output($root,0,"IP受限");
                    }

                    if($GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."user where user_name = '".$user_mobile."' or mobile = '".$user_mobile."' or email = '".$user_mobile."'")>0)
                    {
                        return output($root,0,"手机号已被抢占");
                    }

                    //生成新用户
                    $user_data = array();
                    $user_data['mobile'] = $user_mobile;


                    $rs_data = auto_create($user_data, 1);
                    if(!$rs_data['status'])
                    {
                        return output($root,0,$rs_data['info']);
                    }

                    $result = do_login_user($rs_data['user_data']['user_name'],$rs_data['user_data']['user_pwd']);

                    if($result['status'])
                    {
                        $s_user_info = es_session::get("user_info");
                        $userdata['id'] = $s_user_info['id'];
                        $userdata['user_name'] = $s_user_info['user_name'];
                        $userdata['user_pwd'] = $s_user_info['user_pwd'];
                        $userdata['email'] = $s_user_info['email'];
                        $userdata['mobile'] = $s_user_info['mobile'];
                        $userdata['is_tmp'] = $s_user_info['is_tmp'];

                    }
                    else
                    {
                        return output($root,0,"登录失败");
                    }
                }

            }
            else
            {
                return output($root,0,"验证码错误");
            }

            //end 自动创建会员或手机登录

        }
        else*/
        {
            /*if($GLOBALS['user_info']['mobile']=="")
            {
                //绑定手机号
                if(app_conf("SMS_ON")==0)
                {
                    return output($root,0,"短信功能未开启");
                }
                if($user_mobile=="")
                {
                    return output($root,0,"请输入手机号");
                }
                if($sms_verify=="")
                {
                    return output($root,0,"请输入收到的验证码");
                }

                $sql = "DELETE FROM ".DB_PREFIX."sms_mobile_verify WHERE add_time <=".(NOW_TIME-SMS_EXPIRESPAN);
                $GLOBALS['db']->query($sql);

                $mobile_data = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."sms_mobile_verify where mobile_phone = '".$user_mobile."'");

                if($mobile_data['code']==$sms_verify)
                {
                    //开始绑定
                    //1. 未登录状态提示登录
                    //2. 已登录状态绑定
                    require_once(APP_ROOT_PATH."system/model/user.php");

                    $user_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user where mobile = '".$user_mobile."'");
                    if($user_info)
                    {
                        $supplier_user_origin = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."weixin_user where account_id = '".$GLOBALS['supplier_info']['id']."' and user_id = '".$GLOBALS['user_info']['id']."'");
                        //return output($root,0,"手机号已被抢占");
                        $result = do_login_user($user_info['user_name'],$user_info['user_pwd']);

                        if($result['status'])
                        {
                            $s_user_info = es_session::get("user_info");
                            $userdata['id'] = $s_user_info['id'];
                            $userdata['user_name'] = $s_user_info['user_name'];
                            $userdata['user_pwd'] = $s_user_info['user_pwd'];
                            $userdata['email'] = $s_user_info['email'];
                            $userdata['mobile'] = $s_user_info['mobile'];
                            $userdata['is_tmp'] = $s_user_info['is_tmp'];

                            if($supplier_user_origin)
                            {
                                $supplier_user = array();
                                $supplier_user['user_id'] = $s_user_info['id'];
                                $supplier_user['account_id'] = $GLOBALS['supplier_info']['id'];
                                $supplier_user['openid'] = $supplier_user_origin['openid']; //商户openid
                                $supplier_user['nickname'] = $s_user_info['user_name'];
                                $GLOBALS['db']->autoExecute(DB_PREFIX."weixin_user",$supplier_user);
                                $supplier_user['id'] = $GLOBALS['db']->insert_id();
                            }
                        }
                        else
                        {
                            return output($root,0,"登录失败");
                        }
                    }
                    else
                    {
                        $GLOBALS['db']->query("delete from ".DB_PREFIX."sms_mobile_verify where mobile_phone = '".$user_mobile."'");
                        $GLOBALS['db']->query("update ".DB_PREFIX."user set mobile = '".$user_mobile."' where id = ".$GLOBALS['user_info']['id']);

                        $result = do_login_user($user_mobile,$GLOBALS['user_info']['user_pwd']);

                        if($result['status'])
                        {
                            $s_user_info = es_session::get("user_info");
                            $userdata['id'] = $s_user_info['id'];
                            $userdata['user_name'] = $s_user_info['user_name'];
                            $userdata['user_pwd'] = $s_user_info['user_pwd'];
                            $userdata['email'] = $s_user_info['email'];
                            $userdata['mobile'] = $s_user_info['mobile'];
                            $userdata['is_tmp'] = $s_user_info['is_tmp'];
                        }
                        else
                        {
                            return output($root,0,"登录失败");
                        }
                    }
                }
                else
                {
                    return output($root,0,"验证码错误");
                }

                //end 绑定手机号
            }
            else*/
            {
                $s_user_info = es_session::get("user_info");
                $userdata['id'] = $s_user_info['id'];
                $userdata['user_name'] = $s_user_info['user_name'];
                $userdata['user_pwd'] = $s_user_info['user_pwd'];
                $userdata['email'] = $s_user_info['email'];
                $userdata['mobile'] = $s_user_info['mobile'];
                $userdata['is_tmp'] = $s_user_info['is_tmp'];
            }
        }

        $total_score = 0;
        $total_money = 0;
        foreach ($num as $k=>$v)
        {
            $id = intval($k);
            $number = $v;

            $cart_data = $GLOBALS['db']->getRow("select return_score,return_money from ".DB_PREFIX."deal_cart where id=".$id);
            $total_score+=$cart_data['return_score']*$number;
            $total_money+=$cart_data['return_money']*$number;
        }

        //验证积分
        // 		$total_score = $cart_result['total_data']['return_total_score'];
        /*if($GLOBALS['user_info']['score']+$total_score<0)
        {
            return output($root,0,"积分不足");
        }*/
        //验证积分


        //关于现金的验证
        // 		$total_money = $cart_result['total_data']['return_total_money'];
        /*if($GLOBALS['user_info']['money']+$total_money<0)
        {
            $root['status'] = 0;
            $root['error']  = "余额不足";
            api_ajax_return($root);
        }*/
        //关于现金的验证

        foreach ($num as $k=>$v)
        {
            $id = intval($k);
            $number = intval($v);
            $data = $this->check_cart1($id, $number);
            if(!$data['status'])
            {
                $root['status'] = 0;
                $root['error']  = $data['info'];
                api_ajax_return($root);
            }
        }

        foreach ($num as $k=>$v)
        {
            $id = intval($k);
            $number = intval($v);
            if($is_wap){
                $GLOBALS['db']->query("update ".DB_PREFIX."deal_cart set number =".$number.", total_price = ".$number."* unit_price, return_total_score = ".$number."* return_score, return_total_money = ".$number."* return_money where id =".$id);
            }else{
                $GLOBALS['db']->query("update ".DB_PREFIX."deal_cart set number =".$number.", total_price = ".$number."* unit_price, return_total_score = ".$number."* return_score, return_total_money = ".$number."* return_money where id =".$id." and session_id = '".es_session::id()."'");
            }

            //load_cart_list(true,$is_wap);
        }
        $root['user_data'] = $userdata;
        api_ajax_return($root);
    }

    /**
     * 购物车的提交页
     * 输入:id 直接购买，不进入购物车的时，商品ID
     * lid;  //自提门店ID
     * address_id 配送地址ID
     * 无
     *
     * 输出:
     * status: int 状态 1:正常 -1未登录需要登录
     * info:string 信息
     * cart_list: object 购物车列表，如该列表为空数组则跳回首页,结构如下
     * Array
    (
    [478] => Array key [int] 购物车表中的主键
    (
    [id] => 478 [int] 同key
    [return_score] => 0 [int] 当is_score为1时单价的展示
    [return_total_score] => 0 [int] 当is_score为1时总价的展示
    [unit_price] => 108 [float] 当is_score为0时单价的展示
    [total_price] => 108 [float] 当is_score为0时总价的展示
    [number] => 1 [int] 购买件数
    [deal_id] => 57 [int] 商品ID
    [attr] => 287,290 [string] 购买商品的规格ID组合，用逗号分隔的规格ID
    [name] => 桥亭活鱼小镇 仅售88元！价值100元的代金券1张 [9点至18点,2-5人套餐] [string] 商品全名，包含属性
    [sub_name] => 88元桥亭活鱼小镇代金券 [9点至18点,2-5人套餐] [string] 商品缩略名，包含属性
    [max] => int 最大购买量 加减时用
    [icon] => string 商品图标 140x85
    )
    )

     * total_data: array 购物车总价统计,结构如下
     *	Array
    (
    [total_price] => 108 [float] 当is_score为0时的总价显示
    [return_total_score] => 0 [int] 当is_score为1时的总价显示
    )
     * is_score: int 当前购物车中的商品类型 0:普通商品，展示时显示价格 1:积分商品，展示时显示积分
     * is_delivery: int 是否需要配送 0无需 1需要
     * consignee_count: int 预设的配送地址数量 0：提示去设置收货地址 1以及以上显示选择其他收货方式
     * consignee_info: object 当前配送地址信息，结构如下
     * Array
    (
    [id] => 19 int 配送方式的主键
    [user_id] => 71 int 当前会员ID
    [region_lv1] => 1 int 国ID
    [region_lv2] => 4 int 省ID
    [region_lv3] => 53 int 市ID
    [region_lv4] => 519 int 区ID
    [address] => 群升国际 string 详细地址
    [mobile] => 13555566666 string 手机号
    [zip] => 350001 string 邮编
    [consignee] => 李四 string 收货人姓名
    [is_default] => 1
    [region_lv1_name] => 中国 string 国名
    [region_lv2_name] => 福建 string 省名
    [region_lv3_name] => 福州 string 市名
    [region_lv4_name] => 台江区 string 区名
    )

     * delivery_list: array 配送方式列表，结构如下
     * Array
    (
    [0] => Array
    (
    [id] => 8 [int] 主键
    [name] => 顺风快递 [string] 名称
    [description] => 顺风快递,福州地区2元 [string] 描述
    )

    )
     * payment_list: array 支付方式列表，结构如下
     * Array
    (
    [0] => Array
    (
    [id] => 20 [int] 支付方式主键
    [code] => Walipay [string] 类名
    [logo] => http://192.168.1.41/o2onew/public/attachment/sjmapi/4f2ce3d1827e4.jpg [string] 图标 40x40
    [name] => 支付宝支付 [string] 显示的名称
    )
    )
     * is_coupon: int 是否为发券订单，0否 1:是
     * show_payment: int 是否要显示支付方式 0:否（0元抽奖类） 1:是
     * has_account: int 是否显示余额支付 0否  1是
     * has_ecv: int 是否显示代金券支付 0否  1是
     * voucher_list:array 可用的代金券列表
     * array(
     * array(
     * 	"sn"=>"xxxxx" string 代金券序列号,
     *  "name" => "红包名称" string
     * )
     * )
     * account_money:float 余额
     *
     */
    public function check()
    {
        //获取页面传参
        $id = intval($_REQUEST['id']);  //直接购买，不进入购物车的时，商品ID
        $lid = intval($_REQUEST['lid']);  //自提门店ID
        $address_id = intval($_REQUEST['address_id']);	//配送地址id
        $is_wap = $_REQUEST['from'] == 'wap'?true:false;	//是否为wap 端请求

        //定义初始化相关变量
        $root = array();
        $is_evc = 0; //代金券是否可用


        /* 用户登陆检测 --开始-- */

        $user_info=$GLOBALS['user_info'];
        $user_id = intval($GLOBALS['user_info']['id']);
        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }
        $root['user_login_status']=1;

        /* 用户登陆检测 --结束-- */



        //如果有[自提]地址选择的时候获取选择的[自提]地址
        if ($lid > 0) {
            $location = $GLOBALS['db']->getRow("select id,name,address,tel from " . DB_PREFIX . "supplier_location where id=" . $lid);
            $root['location'] = $location;
        }

        //获取购物车里面的数据  如果：id 存在则是跳过购物车的商品（团购、拼团、预售）
        if ($id > 0) {
            $cart_result = $this->load_cart_list($id);
        } else {
            $cart_result = $this->load_cart_list($id = 0, false);
        }

        //是否为预售
        $is_presell = $cart_result['total_data']['is_presell'];
        //是否为拼团
        $is_pt = $cart_result['total_data']['is_pt'];
        $is_pt_buy = $cart_result['total_data']['is_pt_buy'];
        //是否为积分商品
        $is_score = 0;

        //用户折扣比率：默认为1 = 100%
        $user_discount_percent = 1;
        if($is_presell==1 || $is_pt==1){ //拼团和预售不参与会员折扣
            $total_price = $cart_result['total_data']['total_price'];
        }else{
            if (!empty($GLOBALS['user_info']['id'])) {
                $user_discount_percent = $this->getUserDiscount($GLOBALS['user_info']['id']);
            }
            //$total_price = $cart_result['total_data']['total_price'] * $user_discount_percent;
            $total_price = 0;
        }

        //处理购物车输出
        $cart_list_o = $cart_result['cart_list']; //购物车商品列表数据
        $total_data_o = $cart_result['total_data'];	//购物车统计数据


        /* 格式化购物车中的商品数据 --开始-- */

        $supplier_ids = array();	//存储商户id用于获取商户开票设置
        $cart_list = array();	//格式化购物车中的商品数据数组
        $buy_type = 0;	//商品类型
        $delivery_count = 0;//查询配送商品总数
        $is_shop=1;
        foreach ($cart_list_o as $k => $v) {
            //定义基础数据，循环初始化
            $bind_data = array();
            $bind_data['id'] = $v['id'];
            $bind_data['is_presell'] = $is_presell;
            $bind_data['is_pt'] = $is_pt;
            $bind_data['is_pt_buy'] = $is_pt_buy;

            //预售、拼团 不涉及积分部分默认为 0
            $bind_data['return_score'] = 0;
            $bind_data['return_total_score'] = 0;

            if ($v['buy_type'] == 1) { //积分商品
                $is_score = 1;
                $bind_data['return_score'] = abs($v['return_score']);
                $bind_data['return_total_score'] = abs($v['return_total_score']);
                $bind_data['unit_price'] = 0;
                $bind_data['total_price'] = 0;
                $buy_type = 1;
            } elseif ( $is_presell==1) { // 预售商品
                $root['presell_deposit_money'] = $v['presell_deposit_money'];//订金金额
                $root['presell_discount_money'] = $v['presell_discount_money'];//抵扣金额

                if ($v['presell_type'] == 0) {//订金
                    $bind_data['presell_deposit_money'] = $v['presell_deposit_money'];
                }
                $bind_data['presell_discount_money'] = $v['presell_discount_money'];//抵扣金额
                $u_price = $v['unit_price'];
                $t_price = $v['total_price'];
                $bind_data['unit_price'] = $u_price;
                $bind_data['total_price'] = $t_price;
            }elseif($is_pt_buy && $is_pt==1){ //拼团的时候修改价格
                $pt_money = $v['pt_money'];
                //获取规格属性数据
                if ($v['attr_str']) {
                    $pt_money = $GLOBALS['db']->getOne("select pt_money from ".DB_PREFIX."attr_stock where attr_str='{$v['attr_str']}' and deal_id='{$v['deal_id']}'");
                    //if($user_id==100436){log_result("pt_money=".$pt_money."  id=".$id);log_result($v);exit();}
                }
                $bind_data['unit_price'] = $pt_money;
                $bind_data['total_price'] = $pt_money*$v['number'];
                $total_data_o['total_price']=$bind_data['total_price'];
            }else {
                //$u_price = $v['unit_price'];
                //$t_price = $v['total_price'];
                $user_discount = 1;
                if ($v['allow_user_discount']) {
                    if($v['allow_user_fx_discount']&&$GLOBALS['user_info']['is_fx']){
                        $user_discount = $user_discount_percent * $v['user_fx_discount'];
                    }else{
                        $user_discount = $user_discount_percent;
                    }
                }elseif($v['allow_user_fx_discount']&&$GLOBALS['user_info']['is_fx']){
                    $user_discount = $v['user_fx_discount'];
                }
                $u_price = round($v['unit_price'] * $user_discount, 2);
                $t_price = $u_price * $v['number'];

                $bind_data['unit_price'] = $u_price;
                $bind_data['total_price'] = $t_price;

                $total_price+=$bind_data['total_price'];
            }

            /*if($is_presell==1){ //是预售商品，名称会加上前缀 如： 【预售】潮香村牛排套餐1680g
                $bind_data['name'] = format_deal_name(htmlspecialchars_decode($v['name']));
                $bind_data['sub_name'] = format_deal_name(htmlspecialchars_decode($v['sub_name']));
            }elseif($is_pt==1){//是拼团商品，名称会加上前缀 如： 【拼团】潮香村牛排套餐1680g
                $bind_data['name'] = format_deal_name(htmlspecialchars_decode($v['name']),1);
                $bind_data['sub_name'] = format_deal_name(htmlspecialchars_decode($v['sub_name']),1);
            }else*/{	// 普通商品
                $bind_data['name'] = htmlspecialchars_decode($v['name']);
                $bind_data['sub_name'] = htmlspecialchars_decode($v['sub_name']);
            }



            $bind_data['allow_promote'] = $v['allow_promote'];
            $bind_data['number'] = $v['number'];
            $bind_data['is_pick'] = $v['is_pick'];
            $bind_data['deal_id'] = $v['deal_id'];
            $bind_data['attr'] = $v['attr'];
            $bind_data['attr_str'] = $v['attr_str'];
            $bind_data['is_pt'] = $v['is_pt'];
            $bind_data['shop_cate_id'] = $v['shop_cate_id'];
            $bind_data['pt_group_id'] = $v['pt_group_id'];
            $bind_data['is_shop'] = $v['is_shop'];
            $bind_data['supplier_id'] = $v['supplier_id'];
            $bind_data['icon'] = get_abs_img_root(get_spec_image($v['icon'], 140, 85, 1));
            $bind_data['f_icon'] = get_abs_img_root(get_spec_image($v['icon'], 280, 280, 1));
            $cart_list[$v['id']] = $bind_data;
            if($v['is_shop']==0){
                $is_shop=0;
            }
            //保存商户id数组
            $supplier_ids[] = $v['supplier_id'];

            //查询配送商品总数
            if ($v['is_delivery'] == 1) {
                $delivery_count++;
            }
        }

        $is_coupon = 0;//是否发券，驿站商品也需要发劵
        foreach ($cart_list_o as $k => $v) {
            if ($GLOBALS['db']->getOne("select is_coupon from " . DB_PREFIX . "deal where id = " . $v['deal_id'] . " and forbid_sms = 0") == 1) {
                $is_coupon = 1;
                break;
            }
        }

        /* 格式化购物车中的商品数据 --结束-- */


        /*购物车统计数组处理 --开始-- */
        $total_data = array();
        if ($is_score) {
            $total_data['total_price'] = 0;
            $total_data['return_total_score'] = abs($total_data_o['return_total_score']);
        } else {
            $total_data['total_price'] = round($total_data_o['total_price'], 2);
            $total_data['return_total_score'] = 0;
        }
        /*购物车统计数组处理 --开始-- */


        // 获取商户的开票信息配置
        if (!$is_score) {
            $invoice_list = array();	//发票内容关键字列表
            $invoice_notice = '';
            if (count($supplier_ids)>0){
            $invoice_sql = 'SELECT * FROM ' . DB_PREFIX . 'invoice_conf WHERE supplier_id in (' . implode(',', $supplier_ids) . ')';
            $db_invoice_list = $GLOBALS['db']->getAll($invoice_sql);
            }
            foreach ($db_invoice_list as $value) {
                if ($value['invoice_type'] > 0) {
                    if (!empty($value['invoice_content'])) {
                        $value['invoice_content'] = explode(' ', $value['invoice_content']);
                    } else {
                        $value['invoice_content'] = array('明细');
                    }
                }
                $invoice_list[$value['supplier_id']] = $value;
            }
            if (!empty($invoice_list)) {
                $invoice_notice = app_conf('INVOICE_NOTICE');
            }

            $root['invoice_notice'] = $invoice_notice; //发票规则
            $root['invoice_list'] = $invoice_list;	//发票不同商户的设置列表
        }


        /*关于配送部分处理  --开始-- */
        $is_delivery = 0;	//是否需要配送
        foreach ($cart_list_o as $k => $v) {
            if ($v['is_delivery'] == 1) {
                $is_delivery = 1;
                break;
            }
        }

        if ($is_delivery) {
            //输出配送方式
            $consignee_count = $GLOBALS['db']->getOne("select count(*) from " . DB_PREFIX . "user_consignee where user_id = " . $GLOBALS['user_info']['id']);
            $address_id = $GLOBALS['db']->getOne("select id from " . DB_PREFIX . "user_consignee where id=" . $address_id . " and user_id=" . $GLOBALS['user_info']['id']);
            if ($address_id) {
                $consignee_id = $address_id;
            } else {
                $consignee_id = $GLOBALS['db']->getOne("select id from " . DB_PREFIX . "user_consignee where user_id = " . $GLOBALS['user_info']['id'] . " and is_default = 1");
            }
            if ($lid > 0) {
                $consignee_id = 0;
            }
        }
        $root['consignee_count'] = intval($consignee_count);
        //$consignee_info = load_auto_cache("consignee_info", array("consignee_id" => $consignee_id));

        $param['consignee_id'] = intval($GLOBALS['db']->getOne("select id from ".DB_PREFIX."user_consignee where id = ".intval($consignee_id)));
        $consignee_id = intval($param['consignee_id']);
        $consignee_info =  $consignee_data['consignee_info'] = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_consignee where id = ".$consignee_id);
        $region_lv1 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = 0");  //一级地址
        foreach($region_lv1 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv1'])
            {
                $region_lv1[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv1_name'] = $v['name'];
                break;
            }
        }
        $consignee_data['region_lv1'] = $region_lv1;

        $region_lv2 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv1']));  //二级地址
        foreach($region_lv2 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv2'])
            {
                $region_lv2[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv2_name'] = $v['name'];
                break;
            }
        }

        $consignee_data['region_lv2'] = $region_lv2;

        $region_lv3 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv2']));  //三级地址
        foreach($region_lv3 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv3'])
            {
                $region_lv3[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv3_name'] = $v['name'];
                $consignee_data['consignee_info']['region_lv3_code'] = $v['code'];
                break;
            }
        }
        $consignee_data['region_lv3'] = $region_lv3;

        $region_lv4 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv3']));  //四级地址
        foreach($region_lv4 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv4'])
            {
                $region_lv4[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv4_name'] = $v['name'];
                break;
            }
        }
        $consignee_data['region_lv4'] = $region_lv4;
        if($consignee_data['consignee_info']){
            $consignee_data['consignee_info']['full_address'] = $consignee_data['consignee_info']['region_lv1_name'].$consignee_data['consignee_info']['region_lv2_name'].$consignee_data['consignee_info']['region_lv3_name'].$consignee_data['consignee_info']['region_lv4_name'].$consignee_data['consignee_info']['address'].$consignee_data['consignee_info']['street'].$consignee_data['consignee_info']['doorplate'];
        }

        $consignee_info=$consignee_data;

        if ($consignee_info){
            $region_id = intval($consignee_info['region_lv4']);
        }

        $consignee_info = $consignee_info['consignee_info'] ? $consignee_info['consignee_info'] : (object) array();
        $root['consignee_info'] = $consignee_info;




        //配送方式由ajax由 consignee 中的地区动态获取
        /* 配送地址输出内容处理   --结束-- */


        /* 支付方式输出处理   --开始-- */
        $payment_list = array();
        /*if ($is_wap) {
            //支付列表
            $sql = "select id, class_name as code, logo from " . DB_PREFIX . "payment where (online_pay = 2 or online_pay = 4 or online_pay = 5) and is_effect = 1";
        } else {
            //支付列表
            $sql = "select id, class_name as code, logo from " . DB_PREFIX . "payment where (online_pay = 3 or online_pay = 4 or online_pay = 5) and is_effect = 1";
        }*/
        $sql = "select id,name,class_name as code,logo from " . DB_PREFIX . "payment where is_effect = 1 ";
        $payment_list = $GLOBALS['db']->getAll($sql);
        //是否在审核，审核中不查询数据
        /*if (allow_show_api()) {
            $payment_list = $GLOBALS['db']->getAll($sql);
        }*/
        if($payment_list){

            foreach ($payment_list as $k => $v) {
                $directory = APP_ROOT_PATH . "system/payment/";
                $file = $directory . '/' . $v['code'] . "_payment.php";
                if (file_exists($file)) {
                    require_once($file);
                    $payment_class = $v['code'] . "_payment";
                    $payment_object = new $payment_class();
                    $payment_list[$k]['name'] = $payment_object->get_display_code();
                }

                if ($v['logo'] != "")
                    $payment_list[$k]['logo'] = get_abs_img_root(get_spec_image($v['logo'], 40, 40, 1));
            }

        }

        $root['payment_list'] = $payment_list;



        //商品总金额 > 0 或者 有需要配送的商品 > 0 有可能出现运费的情况
        if ($total_price > 0 || $delivery_count > 0)
            $show_payment = 1;
        else
            $show_payment = 0;
        $root['show_payment'] = $show_payment;

        if ($show_payment) {
            //$web_payment_list = load_auto_cache("cache_payment");
            /* online_pay 支付方式：1：在线支付；0：线下支付; 2:手机支付(wap); 3:手机SDK */
            $payment_list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."payment where online_pay in (0,1) and is_effect = 1 order by sort desc");


            foreach($payment_list as $k=>$v)
            {
                $directory = APP_ROOT_PATH."system/payment/";
                $file = $directory. '/' .$v['class_name']."_payment.php";
                if(file_exists($file))
                {
                    require_once($file);
                    $payment_class = $v['class_name']."_payment";
                    $payment_object = new $payment_class();
                    $payment_list[$k]['display_code'] = $payment_object->get_display_code();
                    $payment_list[$k]['is_bank'] = intval($payment_object->is_bank);

                }
                else
                {
                    unset($payment_list[$k]);
                }
            }
            $web_payment_list=$payment_list;
            foreach ($web_payment_list as $k => $v) {
                if ($v['class_name'] == "Account" && $GLOBALS['user_info']['money'] > 0) {
                    $root['has_account'] = 1;
                }
                //预售和拼团不可使用代金券
                /*if ($is_presell == 0 && $is_pt == 0) {
                    if ($v['class_name'] == "Voucher") {
                        $root['has_ecv'] = 1;

                        $sql = "select e.sn as sn,t.name as name,e.money as money,t.start_use_price from " . DB_PREFIX . "ecv as e left join " . DB_PREFIX . "ecv_type as t on e.ecv_type_id = t.id where " .
                            " e.user_id = '" . $GLOBALS['user_info']['id'] . "' and (e.begin_time < " . NOW_TIME . ") and (e.end_time = 0 or e.end_time > " . NOW_TIME . ") " .
                            " and e.use_count=0 and (t.start_use_price<=" . $total_price . " or t.start_use_price=0)";


                        $sql_count = "select count(*) from " . DB_PREFIX . "ecv as e left join " . DB_PREFIX . "ecv_type as t on e.ecv_type_id = t.id where " .
                            " e.user_id = '" . $GLOBALS['user_info']['id'] . "' and (e.begin_time < " . NOW_TIME . ") and (e.end_time = 0 or e.end_time > " . NOW_TIME . ") " .
                            " and e.use_count=0 and (t.start_use_price<=" . $total_price . " or t.start_use_price=0)";

                        $voucher_list = $GLOBALS['db']->getAll($sql);

                        foreach ($voucher_list as $t => $v) {
                            $voucher_list[$t]['money'] = round($v['money']);
                            $voucher_list[$t]['start_use_price'] = round($v['start_use_price']);
                        }

                        $root['voucher_count'] = $GLOBALS['db']->getOne($sql_count);
                        $root['voucher_list'] = $voucher_list;
                    }
                }*/

            }
        }else{
            $root['has_account'] = 0;
            $root['has_ecv'] = 0;
        }

        //用户余额
        $root['account_money'] = (string)round($GLOBALS['user_info']['money'], 2);
        /* 支付方式输出处理   --结束-- */


        /* 关于属性库存和 自提判断   --开始-- */
        $cart_list_x = array();
        $is_pick = 1;
        foreach ($cart_list as $k => $v) {
            if ($id > 0) {
                $stock = $GLOBALS['db']->getOne("select stock_cfg from " . DB_PREFIX . "deal_stock where deal_id=" . $v['deal_id']);
                if ($stock < 10 && $stock != -1) {
                    $v['stock'] = $stock;
                }
            } else {
                $attr = array();
                $attr['attr_id'] = explode("_",$v['attr']);
                $attr['attr_str'] = $v['attr_str'];
                $check_info = $this->check_deal_status($v['deal_id'], $attr, 0, true);//进入确认订单，数量用0判断
                if ($check_info['stock'] < 10 && $check_info['stock'] != -1) {
                    $v['stock'] = $check_info['stock'];//加上被减去的购物车商品数量
                }
            }
            $bai = floor($v['unit_price']);
            $fei = str_pad(round(($v['unit_price'] - $bai) * 100, 2), 2, '0', STR_PAD_LEFT);
            $v['unit_price_format'] = "&yen; <i>" . $bai . "</i>." . $fei;
            $back_deal_url = $v['url'] = url("index", "deal", array("data_id" => $v['deal_id']));
            $v['return_score_format'] = "<i>" . $v['return_score'] . "</i> 积分";
            $v['return_total_score_format'] = "<i>" . $v['return_total_score'] . "</i> 积分";


            $delivery_type = $GLOBALS['db']->getOne("select delivery_type from " . DB_PREFIX . "deal where id=" . $v['deal_id']);
            $v['delivery_type'] = $delivery_type;

            if ($v['supplier_id'] == 0) {
                if ($delivery_type == 1) { //平台物流配送商品
                    $cart_list_x['p_wl'][$v['id']] = $v;
                } elseif ($delivery_type == 3) { //平台驿站配送商品
                    $cart_list_x['p_yz'][$v['id']] = $v;
                } else { //平台无需配送商品，直接进入订单提交页面，不进入购物车页面
                    $cart_list_x[$v['supplier_id']][$v['id']] = $v;
                }
            } else {
                $cart_list_x[$v['supplier_id']][$v['id']] = $v;
            }

            if ($v['is_shop'] == 1 && $v['is_pick'] == 0) {
                $is_pick = 0;

            }

        }

        $cart_list_new = array();
        $supplier = array();
        $is_zy = 0;//是否是平台自营商品


        if ($consignee_id > 0) {
            $delivery_list = $this->get_express_fee($cart_result['cart_list'], $consignee_id);
        }

        $p_wl_youhui_id = 0;
        $p_yz_youhui_id = 0;

        foreach ($cart_list_x as $k => $v) {
            $cart_list_new[$k]['id'] = $k;

            $supplier_total_pirce = 0;
            foreach ($v as $tt => $vv) {
                $supplier_total_pirce += $vv['total_price'];
            }

            $cart_list_new[$k]['total_price'] = $supplier_total_pirce;

            if ($k == 'p_wl') {
                $supplier_name = '平台自营';

                $youhui_supplier_id = 0;

            } elseif ($k == 'p_yz') {
                $supplier_name = '平台自营（驿站配送）';

                $youhui_supplier_id = 0;

            } else {
                $supplier_name = $GLOBALS['db']->getOne("select name from " . DB_PREFIX . "supplier where id=" . $k);

                $youhui_supplier_id = $k;

            }
            $supplier_user_id=intval( $GLOBALS['db']->getOne("select user_id from " . DB_PREFIX . "supplier where id=" . $k));
            $supplier_img=get_spec_image($GLOBALS['db']->getOne("select head_image from " . DB_PREFIX . "user where id=" . $supplier_user_id));

            $cart_list_new[$k]['supplier_id'] = $k;
            $cart_list_new[$k]['supplier_name'] = $supplier_name;
            $cart_list_new[$k]['supplier_img'] = $supplier_img;
            $cart_list_new[$k]['supplier_user_id'] = $supplier_user_id;
            if ($buy_type == 0 && $is_presell == 0 && $is_pt == 0 && $is_shop==1) {

                $log_sql = "select yl.id,y.scope_type,y.shop_cate_id,y.deal_id,y.youhui_value,y.start_use_price from " . DB_PREFIX . "youhui_log as yl left join " . DB_PREFIX . "youhui as y on y.id=yl.youhui_id
              where y.supplier_id = " . $youhui_supplier_id . " and yl.confirm_time=0 and (yl.start_time=0 or yl.start_time<" . NOW_TIME . ") and (yl.expire_time=0 or yl.expire_time>" . NOW_TIME . ") and y.youhui_type=2
              and (y.start_use_price<=" . $supplier_total_pirce . " or y.start_use_price=0) and y.scope_goods in(0,1) and yl.user_id=" . $GLOBALS['user_info']['id'] . " order by y.youhui_value desc";

                $log_info = $GLOBALS['db']->getAll($log_sql);

                //$log_info = $this->reset_youhui_list($log_info,$v);
                $log_info=array();
// print_r($log_info);exit;



                if ($log_info) {

                    if ($k == 'p_wl' && $p_yz_youhui_id != $log_info[0]["id"]) {
                        $p_wl_youhui_id = $log_info[0]["id"];
                        $log_info[0]['is_checked'] = 1;

                        $youhui_value = $log_info[0]["youhui_value"];
                    } elseif ($k == 'p_wl') {
                        $p_wl_youhui_id = $log_info[1]["id"];
                        $log_info[1]['is_checked'] = 1;

                        $youhui_value = $log_info[1]["youhui_value"];
                    } elseif ($k == 'p_yz' && $p_wl_youhui_id != $log_info[0]["id"]) {
                        $p_yz_youhui_id = $log_info[0]["id"];
                        $log_info[0]['is_checked'] = 1;

                        $youhui_value = $log_info[0]["youhui_value"];
                    } elseif ($k == 'p_yz') {
                        $p_yz_youhui_id = $log_info[1]["id"];
                        $log_info[1]['is_checked'] = 1;

                        $youhui_value = $log_info[1]["youhui_value"];
                    } else {
                        $log_info[0]['is_checked'] = 1;

                        $youhui_value = $log_info[0]["youhui_value"];
                    }

                    $cart_list_new[$k]['youhui_value'] = $youhui_value;
                    $cart_list_new[$k]['youhui_list'] = $log_info;
                }

            }
            sort($v);
            $cart_list_new[$k]['list'] = $v;

            if ($delivery_list[$k]['is_delivery_free'] == 1) {
                $delivery_info = "满" . round($delivery_list[$k]['delivery_free_money'], 2) . "免运费";
            } else {
                if ($delivery_list[$k]['total_fee'] > 0) {
                    $delivery_info = format_price($delivery_list[$k]['total_fee']);
                    if($is_pt==1&&$is_pt_buy==1){
                        //修复拼团价格
                        $total_data['total_price']+=$delivery_list[$k]['total_fee'];

                    }
                } else {
                    $delivery_info = '包邮';
                }
            }

            $cart_list_new[$k]['delivery_fee'] = $delivery_info;

            if (!in_array($k, $supplier)) {
                $supplier[] = $k;
            }
            if ($k == 0) {  //如果是平台自营，不能自提
                $is_zy = 1;
            }

            $ivoKey = 0;
            if (intval($k) > 0) {
                $ivoKey = $k;
            }
            if (isset($invoice_list[$ivoKey])) {
                $cart_list_new[$k]['invoice_conf'] = $invoice_list[$ivoKey];
            }
        }

        if ($cart_list_new['p_wl'])
            $cart_list_new['p_wl']['p_youhui_id'] = $p_yz_youhui_id;

        if ($cart_list_new['p_yz'])
            $cart_list_new['p_yz']['p_youhui_id'] = $p_wl_youhui_id;

        sort($cart_list_new);

        $all_num=0;
        foreach($cart_list_new as $k=>$v){
            $cart_list_new[$k]['total_num']=0;
            $cart_list_l=$cart_list_new[$k]['list'];
            foreach($cart_list_l as $kk=>$vv){
                $cart_list_new[$k]['total_num']+=$vv['number'];
            }
            $all_num+=$cart_list_new[$k]['total_num'];
        }
        $root['total_num'] = $all_num;
        $root['cart_list'] = $cart_list_new;

        //只有普通商家才能上门自提,且多商家下单时，不允许自提
        if ($is_pick == 1 && count($supplier) == 1 && $is_zy == 0) {
            $supplier_id = $supplier[0];
            $is_pick = 1;
        } else {
            $supplier_id = 0;
            $is_pick = 0;
        }
        if($is_presell==1){
            $is_pick = 0;
        }

        /* 关于属性库存和 自提判断   --结束-- */

        $root['page_title'] = "提交订单";

        //返回结构基本数据
        $root['total_data'] = $total_data;	//统计数据
        $root['is_score'] = $is_score;		//是否是积分商品
        $root['is_presell'] = $is_presell;	//是否是预售商品
        $root['is_pt'] = $is_pt;			//是否是拼团商品
        $root['supplier_id'] = $supplier_id;
        $root['is_pick'] = $is_pick;		//是否支持自提
        $root['is_delivery'] = $is_delivery;	//是否需要配送
        $root['is_coupon'] = $is_coupon;	//是否发券
        $root['buy_type'] = $buy_type;

        api_ajax_return($root);

    }

    public function set_cart_status(){
        $root = array();

        $user_info=$GLOBALS['user_info'];
        $user_id = intval($GLOBALS['user_info']['id']);
        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }

        $root['user_login_status']=1;
        if(!$user_info['mobile']){
            $root['status'] = 0;
            $root['error']  = "请先绑定手机号";
            api_ajax_return($root);
        }
        $checked_ids= $_REQUEST['checked_ids'];
        $nochecked_ids= $_REQUEST['nochecked_ids'];


        // app_type，来判断是从原生页面过来，还是套壳的H5页面, app_type为1时，为原生页面提交过来的JSON字符串，需要JSON解析。
        $app_type = intval($_REQUEST['app_type']);
        if($app_type==1){
            $checked_ids=json_decode($checked_ids,true);
            $nochecked_ids=json_decode($nochecked_ids,true);
        }


        $checked_id = array();
        if($checked_ids){ //选择的，把购物车的状态改为有效
            foreach($checked_ids as $k=>$v){
                $checked_id[] = $v['id'];
            }

            $GLOBALS['db']->query("update ".DB_PREFIX."deal_cart set is_effect=1 where user_id = " . $user_info['id']." and id in (".implode(",",$checked_id).")");
        }

        $cart_data_all = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_cart where user_id = " . $user_info['id']." and id in (".implode(",",$checked_id).")");
        if(empty($cart_data_all)){
            $root['jump'] = url('index','cart');
            $root['status'] = 0;
            $root['error']  = "数据不存在";
            api_ajax_return($root);
        }

        //验证提交
        $result = $this->cheak_wap_cart($checked_ids);

        if($result['status']==0){
            $root['status'] = 0;
            $root['error']  = $result['info'];
            api_ajax_return($root);
        }

        $no_checked_id = array();
        if($nochecked_ids){ //未选择的，把购物车的状态改为无效
            foreach($nochecked_ids as $k=>$v){
                $no_checked_id[] = $v['id'];
            }
            $GLOBALS['db']->query("update ".DB_PREFIX."deal_cart set is_effect=0 where user_id = " . $user_info['id']." and id in (".implode(",",$no_checked_id).")");
        }


        if($checked_ids){  //把选择中的商品，更新购物车中的信息，包括数量，属性，单价，总价

            foreach($checked_ids as $k=>$v){
                $cart_data = array();
                // $cart_data['id'] = $v['id'];
                $cart_data['attr'] = $v['attr'];
                $cart_data['attr_str'] = $v['attr_str'];
                $cart_data['number'] = $v['number'];

                $cart_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_cart where user_id = " . $user_info['id']." and id=".$v['id']);
                $deal_info=$GLOBALS['db']->getRow("select id ,name,sub_name, current_price , balance_price,return_score,return_money from ".DB_PREFIX."deal where id=".$cart_info['deal_id']);
                $cart_data['unit_price'] = $deal_info['current_price'];
                $cart_data['return_score'] = $deal_info['return_score'];
                $cart_data['return_money'] = $deal_info['return_money'];
                $cart_data['add_balance_price'] =0;
                // $cart_attr = explode(",",$cart_data['attr']);
                if($cart_data['attr']){


                    $add_price_sql = "SELECT price, add_balance_price FROM " . DB_PREFIX . "attr_stock WHERE deal_id=" . $cart_info['deal_id'] . " AND locate (attr_str,'".$cart_data['attr_str']."')";
                    $add_price_info = $GLOBALS['db']->getRow($add_price_sql);
                    if ($add_price_info) {
                        $cart_data['unit_price'] += $add_price_info['price'];
                        $cart_data['add_balance_price'] += $add_price_info['add_balance_price'];
                    }
                    $cart_data['name']=$deal_info['name']."[".$v['attr_str']."]";
                    $cart_data['sub_name']=$deal_info['sub_name']."[".$v['attr_str']."]";
                    /*$deal_attr_info=$GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_attr where deal_id = ".$cart_info['deal_id']." and id in (".$cart_data['attr'].")");

                    foreach($deal_attr_info as $kk=>$vv){
                        $cart_data['unit_price'] += $vv['price'];
                        $cart_data['add_balance_price'] += $vv['add_balance_price'];
                    }*/
                }

                $cart_data['total_price'] = $cart_data['unit_price'] *  $cart_data['number'];
                $cart_data['return_total_score'] = $cart_data['return_score'] *  $cart_data['number'];
                $cart_data['return_total_money'] = $cart_data['return_money'] *  $cart_data['number'];
                $GLOBALS['db']->autoExecute(DB_PREFIX."deal_cart", $cart_data, $mode = 'UPDATE', 'id ='.$v['id'], $querymode = 'SILENT');

            }

        }



        $root['status'] = 1;
        $root['error']  = "成功";
        api_ajax_return($root);

    }

    /**
     * 购物车提交订单接口
     * 输入：
     * address_id: int 配送方式主键
     * ecvsn:string 代金券序列号
     * content:string 订单备注
     * buy_type 如果是积分兑换，传1
     * id 直接购买，不进入购物车的时，商品ID
     *
     * 输出：
     * status: int 状态 0:失败 1:成功 -1:未登录
     * info: string 失败时返回的错误信息，用于提示
     * 以下参数为status为1时返回
     * pay_status:int 0未支付成功 1全部支付
     * order_id:int 订单ID
     *
     */
    public function done()
    {
        $root=array();
        $user_info=$GLOBALS['user_info'];
        $root = array();
        $user_id = intval($GLOBALS['user_info']['id']);
        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }

        $root['user_login_status']=1;


        $delivery_id =  intval($_REQUEST['delivery_id']); //配送方式
        $address_id=intval($_REQUEST['address_id']);
        $location_id=intval($_REQUEST['location_id']);  //自提门店的ID
        $youhui_ids=$_REQUEST['youhui_ids'];  //优惠ID


        if($address_id==0){
            $address_id = $GLOBALS['db']->getOne("select id from " . DB_PREFIX . "user_consignee where user_id=" . $GLOBALS['user_info']['id']);
        }
        if($location_id > 0){
            $address_id=0;
        }
        if( $_REQUEST['from']=='wap'){

            $is_wap=true;
        }else{
            $is_wap=false;
        }
        $buy_type=intval($_REQUEST['buy_type']);
        $id=intval($_REQUEST['id']);

        $root['buy_type']=$buy_type;

        $region_id = 0; //配送地区



        $consignee_id=$address_id;

        //$consignee_info = load_auto_cache("consignee_info",array("consignee_id"=>$consignee_id));
        $param['consignee_id'] = intval($GLOBALS['db']->getOne("select id from ".DB_PREFIX."user_consignee where id = ".intval($consignee_id)));
        $consignee_id = intval($param['consignee_id']);
        $consignee_info =  $consignee_data['consignee_info'] = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_consignee where id = ".$consignee_id);
        $region_lv1 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = 0");  //一级地址
        foreach($region_lv1 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv1'])
            {
                $region_lv1[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv1_name'] = $v['name'];
                break;
            }
        }
        $consignee_data['region_lv1'] = $region_lv1;

        $region_lv2 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv1']));  //二级地址
        foreach($region_lv2 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv2'])
            {
                $region_lv2[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv2_name'] = $v['name'];
                break;
            }
        }

        $consignee_data['region_lv2'] = $region_lv2;

        $region_lv3 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv2']));  //三级地址
        foreach($region_lv3 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv3'])
            {
                $region_lv3[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv3_name'] = $v['name'];
                $consignee_data['consignee_info']['region_lv3_code'] = $v['code'];
                break;
            }
        }
        $consignee_data['region_lv3'] = $region_lv3;

        $region_lv4 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv3']));  //四级地址
        foreach($region_lv4 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv4'])
            {
                $region_lv4[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv4_name'] = $v['name'];
                break;
            }
        }
        $consignee_data['region_lv4'] = $region_lv4;
        if($consignee_data['consignee_info']){
            $consignee_data['consignee_info']['full_address'] = $consignee_data['consignee_info']['region_lv1_name'].$consignee_data['consignee_info']['region_lv2_name'].$consignee_data['consignee_info']['region_lv3_name'].$consignee_data['consignee_info']['region_lv4_name'].$consignee_data['consignee_info']['address'].$consignee_data['consignee_info']['street'].$consignee_data['consignee_info']['doorplate'];
        }

        $consignee_info=$consignee_data;

        if($consignee_info)
            $region_id = intval($consignee_info['region_lv4']);


        $consignee_info = $consignee_info['consignee_info'] ? $consignee_info['consignee_info'] : (object) array();
        $root['consignee_info'] = $consignee_info;

        //$consignee_info = $consignee_info['consignee_info']?$consignee_info['consignee_info']:array();


        $payment = intval($_REQUEST['payment']);
        $all_account_money = intval($_REQUEST['all_account_money']);
        $all_score = intval($_REQUEST['all_score']);
        $ecvsn = $_REQUEST['ecvsn']?strim($_REQUEST['ecvsn']):'';
        $ecvpassword = $_REQUEST['ecvpassword']?strim($_REQUEST['ecvpassword']):'';
        //$memo = substr(strim($_REQUEST['content']), 0, 100);
        if($_REQUEST['from']=="app"){
            $_REQUEST['content']=json_decode($_REQUEST['content'],true);
        }
        $content = json_decode($_REQUEST['content'],true);
        //log_result('content');log_result($content);log_result($_REQUEST['content']);
        if(count($content)==1){
            $memo = end($content);
        }
        //log_result('memo');log_result($memo);
        $supplier_data = array();
        if($content){
            foreach($content as $k=>$v){
                $supplier_data[$k]['memo'] = $v;
            }
        }
        //log_result('supplier_data');log_result($supplier_data);
        // 发票的数据处理
        $invoice_types = $_REQUEST['invoice_type'];
        $invoice_titles = $_REQUEST['invoice_title'];
        $invoice_persons = $_REQUEST['invoice_person'];
        $invoice_taxnus = $_REQUEST['invoice_taxnu'];
        $invoice_contents = $_REQUEST['invoice_content'];
        $firstInvoice = '';
        if ($invoice_types) {
            $invIndex = 0;
            foreach ($invoice_types as $key => $value) {
                $value = intval($value);
                if ($value !== 0) {
                    $invoices['type'] = 1;
                    $invoices['title'] = $value == 1 ? 0 : 1;
                    $invoices['persons'] = strim($invoice_titles[$key]);
                    if ($invoices['title'] == 1) {
                        $invoices['taxnu'] = strim($invoice_taxnus[$key]);
                    }
                    $invoices['content'] = strim($invoice_contents[$key]);
                    $seriInv = serialize($invoices);
                    $supplier_data[$key]['invoice_info'] = $seriInv;
                    if ($invIndex === 0) {
                        $firstInvoice = $seriInv;
                    }
                    $invIndex++;
                }
            }
        }

        if($id > 0){
            $cart_result = $this->load_cart_list($id);
        }else{
            $cart_result = $this->load_cart_list($id=0,false);
        }


        //基本参数
        $is_presell = $cart_result['total_data']['is_presell'];		//预售
        $is_pt = $cart_result['total_data']['is_pt'];				//拼团
        $pt_id = $cart_result['total_data']['pt_id'];				//拼团活动ID
        $is_pt_buy = $cart_result['total_data']['is_pt_buy'];
        $pt_group_id = $cart_result['total_data']['pt_group_id'];

        //如果是拼团购买，判断拼团活动是否有效
        if($is_pt_buy){
            if (!$is_pt) { //无效拼团活动
                $root['status'] = 0;
                $root['error']  = "非法数据2149";
                api_ajax_return($root);
            }
        }

        $goods_list = $cart_result['cart_list'];

        if(!$goods_list)
        {
            $root['status'] = 0;
            $root['error']  = "非法数据2159";
            api_ajax_return($root);
        }

        //验证购物车
        $deal_ids = array();
        foreach($goods_list as $row)
        {
            $checker = $this->check_deal_number($row['deal_id'],0);
            if($checker['status']==0)
            {
                $root['status'] = 0;
                $root['error']  = $checker['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$checker['data']];
                api_ajax_return($root);
            }
        }
        foreach($goods_list as $k=>$v)
        {
            $checker = $this->check_deal_number_attr($v['deal_id'],$v['attr_str'],0);
            if($checker['status']==0)
            {
                $root['status'] = 0;
                $root['error']  = $checker['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$checker['data']];
                api_ajax_return($root);
            }
        }
        //验证商品是否过期
        /*foreach($goods_list as $row)
        {

            $checker = $this->check_deal_time($row['deal_id']);

            if($checker['status']==0)
            {
                return output($root,0,$checker['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$checker['data']]);
            }
        }*/

        //如果是拼团商品重新更新属性规格价格...
        if( $is_pt){
            foreach ($goods_list as $key => $value) { //拼团价没有递增价格，拼团就是当前价格
                if($value['attr_str']){
                    $pt_attr_price = $GLOBALS['db']->getOne("select pt_money from ".DB_PREFIX."attr_stock where attr_str='{$value['attr_str']}' and deal_id='{$value['deal_id']}'");
                }else{
                    $pt_attr_price=$goods_list[$key]['pt_money'];
                }
                $goods_list[$key]['unit_price'] = $pt_attr_price;
                $goods_list[$key]['current_price'] = $pt_attr_price;
                $goods_list[$key]['total_price'] = $pt_attr_price * $value['number'];
                $goods_list[$key]['balance_unit_price'] = $pt_attr_price;
                $goods_list[$key]['balance_total_price'] = $pt_attr_price * $value['number'];
                $goods_list[$key]['is_pt_order'] = 1;

            }
        }

        //拼团商品规格属性价格结束



        $supplier=array();
        $has_wuliu=0;//是否有物流配送
        $has_yizhan=0; //是否有驿站配送
        $has_stuan=0;  //是否有商家团购商品
        $has_sshop=0;  //是否有商家商城商品
        foreach($goods_list as $k=>$v)
        {
            $data = $this->check_cart1($v['id'], $v['number']);
            if(!$data['status']){
                $root['status'] = 0;
                $root['error']  = $data['info'];
                api_ajax_return($root);
            }
            $deal_ids[$v['deal_id']]['deal_id'] = $v['deal_id'];


            if(!in_array($v['supplier_id'], $supplier)){
                $supplier[]=$v['supplier_id'];
            }
            $order_deal = $GLOBALS['db']->getRow("select delivery_type,id,is_shop from ".DB_PREFIX."deal where id=".$v['deal_id']);
            if($v['supplier_id']==0){  //平台自营，平台自营商品物流配送和驿站配送，都要拆单

                if($order_deal['delivery_type']==1){
                    $has_wuliu=1;
                }elseif($order_deal['delivery_type']==3){
                    $has_yizhan=1;
                }elseif($order_deal['delivery_type']==2){  //平台无需配送商品
                    $has_nodlivery=1;
                }
            }else{
                if($order_deal['is_shop']==0){  //团购
                    $has_stuan=1;
                }else{//商城商品
                    $has_sshop=1;
                }
            }

        }
        //判断该订单是否需要拆单，$is_main，是否是订单的主单，1为订单的主单,则需要进行拆单，0为订单的子单
        $is_main=0;
        if(count($supplier)>1 ||( $has_wuliu==1 && $has_yizhan==1)){ //多个普通商家和平台自营商品物流配送和驿站配送，都要拆单
            $is_main=1;
        }

        /*分离出收银台，不需要验证了
        foreach($deal_ids as $row)
        {
            //验证支付方式的支持
            if($GLOBALS['db']->getOne("select define_payment from ".DB_PREFIX."deal where id = ".$row['deal_id'])==1)
            {
                if($GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_payment where deal_id = ".$row['deal_id']." and payment_id = ".$payment))
                {
                    return output($root,0,"支付方式不支持");
                }
            }
        }
        */

        //结束验证购物车
        //开始验证订单接交信息

        $data = $this->count_buy_total($region_id,$consignee_id,$payment,0,$all_account_money,$ecvsn,$ecvpassword,$goods_list,0,0,'',$all_score,0,$youhui_ids);


        if(!$consignee_info && $location_id==0 && ( $data['is_delivery']==1 || $data['is_pick']==1))
        {
            $root['status'] = 0;
            $root['error']  = "请设置收货地址";
            api_ajax_return($root);
        }

        //结束验证订单接交信息

        if(count($supplier)==1){  //订单只有单个商家时，保存商家IDR
            $order['supplier_id']=end($supplier);
        }else{
            $has_stuan=0;  //是否有商家团购商品
            $has_sshop=0;  //是否有商家商城商品
            $has_wuliu=0;  //平台自营物流配送订单
            $has_yizhan=0; //平台自营驿站配送订单
        }

        $user_id = $GLOBALS['user_info']['id'];


        //开始生成订单
        $now = NOW_TIME;
        $type=0;
        if($data['return_total_score'] < 0){
            $type=2;  //积分兑换订单
        }elseif($has_stuan==1){
            $type=5;   //商家团购订单
        }elseif($has_sshop==1){
            $type=6;   //商家商品订单
        }elseif( ($has_wuliu==1 && $has_yizhan==0) || $has_nodlivery==1){
            $type=3;  //平台自营物流配送订单
        }elseif($has_wuliu==0 && $has_yizhan==1){
            $type=4;  //平台自营驿站配送订单
        }

        //计算运费
        $delivery_fee = 0;
        if($consignee_id > 0){
            $delivery_list = $this->get_express_fee($cart_result['cart_list'],$consignee_id);
            if($delivery_list){
                foreach($delivery_list as $k=>$v){
                    $supplier_data[$k]['delivery_fee'] = $v['total_fee'];

                    $delivery_fee += $v['total_fee'];
                }
            }
        }

        //订单分配代理商id
        if($is_main==1){//需要拆单的订单
            foreach($supplier_data as $k=>$v){
                if($k=="p_wl"){//物流
                    $supplier_data[$k]['agency_id']=0;
                }elseif($k=="p_yz"){//驿站
                    if($consignee_info){
                        if($consignee_info['region_lv3_code']){
                            $agency_id=$GLOBALS['db']->getOne("select id from ".DB_PREFIX."agency where city_code = '".$consignee_info['region_lv3_code']."'");
                            $supplier_data[$k]['agency_id']=intval($agency_id);
                        }
                    }
                }else{//商户
                    $agency_id=$GLOBALS['db']->getOne("select agency_id from ".DB_PREFIX."supplier where id = '".$k."'");
                    $supplier_data[$k]['agency_id']=intval($agency_id);
                }
                $supplier_data[$k]['youhui_data'] = $data['youhui_data'][$k];
            }
            $order['youhui_money']=$data['youhui_money'];

        }else{//不需要拆单的订单
            if($order['supplier_id']>0){//存在商户的订单
                $agency_id= $GLOBALS['db']->getOne("select agency_id from ".DB_PREFIX."supplier where id = ".$order['supplier_id']);
                $order['agency_id']=intval($agency_id);
            }elseif($type==4){//驿站订单
                if($consignee_info){
                    if($consignee_info['region_lv3_code']){
                        $agency_id=$GLOBALS['db']->getOne("select id from ".DB_PREFIX."agency where city_code = '".$consignee_info['region_lv3_code']."'");
                        $order['agency_id']=intval($agency_id);
                    }
                }
            }
            $youhui_data = end($data['youhui_data']);
            $order['youhui_money']=$youhui_data['youhui_money'];
            $order['youhui_log_id']=$youhui_data['youhui_log_id'];
        }

        //预售商品属性
        $order['presell_deposit_money'] = $data['presell_deposit_money'];
        $order['presell_discount_money'] = $data['presell_discount_money'];
        $order['presell_left_price'] = $data['presell_left_price'];
        $order['presell_type'] = $data['presell_type'];
        $order['presell_end_time'] = $data['presell_end_time'];
        if($is_presell==1){
            $order['is_presell_order']=1;
        }

        //拼团商品属性
        if($is_pt==1){
            $order['pt_id'] =$data['pt_id'];
            $order['is_pt'] =1;
            $order['is_pt_order'] = 1;
            $order['pt_group_id'] = intval($data['pt_group_id']);
        }

        $order['type'] = $type; //普通订单
        $order['user_id'] = $user_id;
        $order['create_time'] = $now;
        $order['total_price'] = $data['pay_total_price'];  //应付总额  商品价 - 会员折扣 + 运费 + 支付手续费

        $order['pay_amount'] = 0;
        $order['pay_status'] = 0;  //新单都为零， 等下面的流程同步订单状态

        $order['delivery_status'] = $data['is_consignment']==0?5:0;
        $order['order_status'] = 0;  //新单都为零， 等下面的流程同步订单状态
        $order['return_total_score'] = $data['return_total_score'];  //结单后送的积分
        $order['return_total_money'] = $data['return_total_money'];  //结单后送的现金
        $order['memo'] = $memo;
        $order['region_lv1'] = intval($consignee_info['region_lv1']);
        $order['region_lv2'] = intval($consignee_info['region_lv2']);
        $order['region_lv3'] = intval($consignee_info['region_lv3']);
        $order['region_lv4'] = intval($consignee_info['region_lv4']);
        $order['address']	= strim($consignee_info['address']);
        $order['mobile']	=	strim($consignee_info['mobile']);
        $order['consignee']	=	strim($consignee_info['consignee']);
        $order['street']	=	strim($consignee_info['street']);
        $order['doorplate']	=	strim($consignee_info['doorplate']);
        $order['zip']	=	strim($consignee_info['zip']);
        $order['xpoint']	=	strim($consignee_info['xpoint']);
        $order['ypoint']	=	strim($consignee_info['ypoint']);
        $order['consignee_id']	=	intval($consignee_id);
        $order['deal_total_price'] = $data['total_price'];   //团购商品总价
        $order['discount_price'] = $data['user_discount'];
        $order['delivery_fee'] = $delivery_fee;
        $order['record_delivery_fee'] = $delivery_fee;
        $order['ecv_money'] = 0;
        $order['account_money'] = 0;
        $order['ecv_sn'] = '';
        $order['delivery_id'] =0;
        $order['payment_id'] = $data['payment_info']['id'];
        $order['payment_fee'] = $data['payment_fee'];
        $order['bank_id'] = "";
        $order['is_main'] = $is_main;
        $order['location_id'] = $location_id;
        $order['supplier_data'] = serialize($supplier_data);
        $order['invoice_info'] = $firstInvoice;
        $order['is_all_balance'] = 0;
        foreach($data['promote_description'] as $promote_item)
        {
            $order['promote_description'].=$promote_item."<br />";
        }
        $order['promote_arr']=serialize($data['promote_arr']);
        //更新来路
        $order['referer'] =	$GLOBALS['referer'];
        $user_info = es_session::get("user_info");
        $order['user_name'] = $user_info['nick_name'];
        $order['exchange_money'] = $data['exchange_money'];
        $order['score_purchase'] = serialize($data['score_purchase']);

        if($is_main==0&&($type==5||$type==6)&&defined("FX_LEVEL")){
            $ref_salary_conf = unserialize(app_conf("REF_SALARY"));
            $ref_salary_switch=intval($ref_salary_conf['ref_salary_switch']);
            if($ref_salary_switch==1){//判断后台是否开启推荐商家入驻三级分销
                $order['is_participate_ref_salary'] = 1;
            }
        }
        //消费送积分
        $order['is_consumer_send_score'] = $data['is_consumer_send_score'];
        $order['consumer_send_score'] = $data['consumer_send_score'];
        $order['consumer_send_score_data'] = serialize($data['consumer_send_score_data']);

        do
        {
            $order['order_sn'] = to_date(NOW_TIME,"Ymdhis").rand(10,99);
            $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order",$order,'INSERT','','SILENT');
            $order_id = intval($GLOBALS['db']->insert_id());

        }while($order_id==0);

        //先计算用户等级折扣
        $user_id = intval($GLOBALS['user_info']['id']);
        $user_discount_percent = 1;
        if ($user_id) {
            $user_discount_percent = $this->getUserDiscount($user_id);
        }

        foreach($goods_list as $k=>$v)
        {
            //$deal_info = load_auto_cache("deal",array("id"=>$v['deal_id']));

            $deal_info =  $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$v['deal_id']."' and is_delete = 0 or (uname= '".$v['deal_id']."' and uname <> '')");
            if($deal_info)
            {

                if($deal_info['uname']!='')
                    $durl = url("index","deal#".$deal_info['uname']);
                else
                    $durl = url("index","deal#".$deal_info['id']);

                $deal_info['url'] = $durl;
            }
            $deal_info;

            $goods_item = array();

            //关于fx
            if($deal_info['is_fx'])
            {
                /*$fx_user_id = intval($GLOBALS['ref_uid']);
                if($fx_user_id)
                {
                    $user_deal = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_deal where deal_id = '".$deal_info['id']."' and user_id = '".$fx_user_id."'");
                    if($user_deal||$deal_info['is_fx'])
                        $goods_item['fx_user_id'] =  $fx_user_id;
                }*/

                // 更改为推荐注册人
                $fx_user_id = intval($GLOBALS['user_info']['pid']);
                if ($fx_user_id) {
                    $goods_item['fx_user_id'] =  $fx_user_id;
                }
            }
            //关于fx
            $goods_item['deal_id'] = $v['deal_id'];
            $goods_item['number'] = $v['number'];
            //$unit_price = $v['unit_price'];
            $user_discount = 1;
            if ($v['allow_user_discount'] && $is_presell==0 && $is_pt==0) {
                if($v['allow_user_fx_discount']&&$GLOBALS['user_info']['is_fx']){
                    $user_discount = $user_discount_percent * $v['user_fx_discount'];
                }else{
                    $user_discount = $user_discount_percent;
                }
                //$unit_price = round($unit_price * $user_discount_percent, 2);
            }elseif($v['allow_user_fx_discount'] && $GLOBALS['user_info']['is_fx'] && $is_presell==0 && $is_pt==0){
                $user_discount = $v['user_fx_discount'];
            }
            $unit_price = round($v['unit_price'] * $user_discount, 2);

            $goods_item['unit_price'] = $v['unit_price'];
            $goods_item['total_price'] = $v['total_price'];
            $goods_item['discount_unit_price'] = $unit_price;  //商品折扣后单价
            $goods_item['name'] = $v['name'];
            $goods_item['sub_name'] = $v['sub_name'];
            $goods_item['attr'] = $v['attr'];
            $goods_item['verify_code'] = $v['verify_code'];
            $goods_item['order_id'] = $order_id;
            if($order['is_consumer_send_score']==0&&$data['is_open_consumer_send_score']==0){
                $goods_item['return_score'] = $v['return_score'];
                $goods_item['return_total_score'] = $v['return_total_score'];
            }else{//订单参与消费返积分，不参与商品返积分
                $goods_item['return_score'] = 0;
                $goods_item['return_total_score'] = 0;
            }
            $goods_item['return_money'] = $v['return_money'];
            $goods_item['return_total_money'] = $v['return_total_money'];
            $goods_item['buy_type']	=	$v['buy_type'];
            $goods_item['attr_str']	=	$v['attr_str'];

            $goods_item['supplier_id'] = $deal_info['supplier_id'];
            $goods_item['delivery_type'] = $deal_info['delivery_type'];

            //关于结算费用部分
            if($is_pt == 1){ //是否为拼团
                $goods_item['add_balance_price'] = 0;
                $goods_item['add_balance_price_total'] = 0;
                $goods_item['balance_unit_price'] = $v['balance_unit_price'];
                $goods_item['balance_total_price'] = $v['balance_total_price'];
                //为计算商品利润补充字段
                $goods_item['balance_price_sum_unit']=$v['balance_unit_price'];
                $goods_item['balance_price_sum_total']=$v['balance_total_price'];
            }else{
                $goods_item['add_balance_price'] = $v['add_balance_price'];
                $goods_item['add_balance_price_total'] = $v['add_balance_price'] * $v['number'];
                $goods_item['balance_unit_price'] = $deal_info['balance_price'];
                $goods_item['balance_total_price'] = $deal_info['balance_price'] * $v['number'];
                //为计算商品利润补充字段
                $goods_item['balance_price_sum_unit']=$goods_item['balance_unit_price']+$goods_item['add_balance_price'];
                $goods_item['balance_price_sum_total']=$goods_item['balance_total_price']+$goods_item['add_balance_price_total'];
            }



            //订单商品驿站服务费 = （订单应付总额-结算总额-递增结算总额）* 驿站服务费比率
            $goods_item['distribution_fee'] = ($goods_item['total_price'] - $goods_item['balance_total_price'] - $goods_item['add_balance_price_total'] )*$deal_info['dist_service_rate']/100;
            $exchange_money=0;
            if($is_main==1){ //如果是需要拆单的主单
                // $supplier_data
                if($goods_item['supplier_id']==0){
                    if($goods_item['delivery_type']==1){  //物流配送
                        $supplier_id = 'p_wl';
                    }elseif($goods_item['delivery_type']==2){  //无需配送
                        $supplier_id = 0;
                    }elseif($goods_item['delivery_type']==3){  //驿站配送
                        $supplier_id = 'p_yz';
                    }
                }else{
                    $supplier_id = $goods_item['supplier_id'];
                }
                $youhui_money = $supplier_data[$supplier_id]['youhui_data']['youhui_money'];
                $total_price = $supplier_data[$supplier_id]['youhui_data']['total_price'];
                $origin_total_price = $supplier_data[$supplier_id]['youhui_data']['origin_total_price'];

            }else{
                $youhui_money = $order['youhui_money'];
                $total_price = $order['total_price'];
                $origin_total_price = $order['deal_total_price'];
                $exchange_money=$order['exchange_money'];
            }



            $youhuix_money = 0;
            //优惠券金额 > 应付总额，就没有结算的金额
            if($youhui_money >= ($total_price+$exchange_money)){
                $goods_item['add_balance_price'] = 0;
                $goods_item['add_balance_price_total'] = 0;
                $goods_item['balance_unit_price'] = 0;
                $goods_item['balance_total_price'] = 0;

            }else{
                //如果有优惠券抵扣的金额，谁出（平台、商户）的优惠券，就从谁那边扣除结算的钱

                $rate = $goods_item['total_price'] / $origin_total_price;	//子单占用主单的金额比例 = 商品总额/订单总而
                $youhui_money = $youhui_money * $rate; //当前订单优惠券的使用金额

                //优惠券的金额 >= 订单商品总递增结算价+总结算价的金额，结算金额=0
                if($youhui_money >= $goods_item['add_balance_price_total'] + $goods_item['balance_total_price']){
                    $goods_item['add_balance_price'] = 0;
                    $goods_item['add_balance_price_total'] = 0;
                    $goods_item['balance_unit_price'] = 0;
                    $goods_item['balance_total_price'] = 0;
                }else{
                    //递增结算金额占用比例 = 递增结算总金额/（总结算价+总递增结算价）结算总额
                    $rate2 = $goods_item['add_balance_price_total'] /($goods_item['balance_total_price'] + $goods_item['add_balance_price_total']);

                    //结算金额占用比例 = 结算价/（递增结算价+结算价）结算总额
                    $rate3 = $goods_item['balance_total_price'] /($goods_item['balance_total_price'] + $goods_item['add_balance_price_total']);

                    //订单商品递增结算总金额 = 优惠总金额*递增结算总金额比例
                    $goods_item['add_balance_price_total'] -= $youhui_money * $rate2 ;

                    //订单商品递增结算金额 = 订单商品递增结算金额 / 商品数量
                    $goods_item['add_balance_price'] = $goods_item['add_balance_price_total'] / $v['number'] ;

                    //订单商品结算金额 = 优惠总金额*结算金额占用比例
                    $goods_item['balance_total_price'] -= $youhui_money * $rate3 ;

                    //订单商品总优惠金额 = 总优惠金额*递增结算金额占用比例 + 总优惠金额*结算金额占用比例
                    $youhuix_money = $youhui_money * $rate2 + $youhui_money * $rate3 ;

                    //订单商品单个结算金额 = 订单商品结算总金额 / 商品购买数量
                    $goods_item['balance_unit_price'] = $goods_item['balance_total_price'] / $v['number'] ;
                }

            }


            $goods_item['deal_icon'] = $deal_info['icon'];
            $goods_item['is_refund'] = $deal_info['is_refund'];
            $goods_item['user_id'] = $user_id;
            $goods_item['order_sn'] = $order['order_sn'];
            $goods_item['is_shop'] = $deal_info['is_shop'];

            $deal_data = array();
            $deal_data['dist_service_rate'] = $deal_info['dist_service_rate'];
            $deal_data['recommend_user_id'] = $deal_info['recommend_user_id'];
            $deal_data['recommend_user_return_ratio'] = $deal_info['recommend_user_return_ratio'];
            $goods_item['deal_data'] = serialize($deal_data);

            if($location_id > 0){
                $goods_item['is_pick'] = 1;
            }else{
                $goods_item['is_pick'] = 0;
            }

            $goods_item['is_coupon'] = $goods_item['is_pick']==1?1:$deal_info['is_coupon'];

            $goods_item['delivery_status'] = $data['is_consignment']==0?5:0;
            $goods_item['is_delivery'] = $location_id > 0 ? 0:$deal_info['is_delivery'];

            //拼团商品属性
            if($is_pt==1){
                $goods_item['pt_id'] = intval($data['pt_id']);
                $goods_item['is_pt_order'] = 1;
                $goods_item['pt_group_id'] = intval($data['pt_group_id']);
            }

            $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order_item",$goods_item,'INSERT','','SILENT');
            $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where id = '".$v['id']."'");
        }

        //开始更新订单表的deal_ids
        $deal_ids = $GLOBALS['db']->getOne("select group_concat(deal_id) from ".DB_PREFIX."deal_order_item where order_id = ".$order_id);
        $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set deal_ids = '".$deal_ids."' where id = ".$order_id);

        //电子卷
        /*if($data['youhui_data']){
            require_once(APP_ROOT_PATH."system/model/biz_verify.php");
            foreach($data['youhui_data'] as $k=>$youhui){
                $this->online_youhui_use($youhui['youhui_log_id']);
            }
        }*/

        //生成order_id 后
        //1. 代金券支付
        $ecv_data = $data['ecv_data'];
        /*if($ecv_data)
        {
            $ecv_payment_id = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."payment where class_name = 'Voucher'");
            if($ecv_data['money']>$order['total_price'])$ecv_data['money'] = $order['total_price'];
            $payment_notice_id = make_payment_notice($ecv_data['money'],$order_id,$ecv_payment_id,"",$ecv_data['id']);
            require_once(APP_ROOT_PATH."system/payment/Voucher_payment.php");
            $voucher_payment = new Voucher_payment();
            $voucher_payment->direct_pay($ecv_data['sn'],$ecv_data['password'],$payment_notice_id);
        }*/
        //积分抵扣
        /*if($all_score==1){
            $score_purchase = $data['score_purchase'];
            if($score_purchase['score_purchase_switch']==1&&$data['exchange_money']>0){
                score_purchase_paid($score_purchase,$order_id);
            }

        }*/

        //2. 余额支付------------------------------------------------------------------------------------------------------------
        $account_money = $data['account_money'];
        /*if(floatval($account_money) > 0)
        {
            $account_payment_id = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."payment where class_name = 'Account'");
            $payment_notice_id = make_payment_notice($account_money,$order_id,$account_payment_id);
            require_once(APP_ROOT_PATH."system/payment/Account_payment.php");
            $account_payment = new Account_payment();
            $account_payment->get_payment_code($payment_notice_id);
        }*/

// 		//3. 相应的支付接口
// 		$payment_info = $data['payment_info'];
// 		if($payment_info&&$data['pay_price']>0)
// 		{
// 			$payment_notice_id = make_payment_notice($data['pay_price'],$order_id,$payment_info['id']);
// 			//创建支付接口的付款单
// 		}
        if($is_main==1){ //如果是需要拆单的主单，则进行拆单
            $this->syn_order($order_id);
        }
        $rs = $this->order_paid($order_id);
        $this->update_order_cache($order_id);
        if($rs)
        {
            $root['pay_status'] = 1;
            $root['order_id'] = $order_id;
        }
        else
        {
            $this->distribute_order($order_id);
            $root['pay_status'] = 0;
            $root['order_id'] = $order_id;

        }

        api_ajax_return($root);
    }


    /**
     * 收银台
     *
     * 输入:
     * 无
     * id: int 订单ID
     * payment: int 支付方式
     * all_account_money 是否全额支付
     *
     *
     * 输出:
     * order_id : int 订单ID
     * total_price 合计
     * pay_amount 已付金额
     * payment_fee 手续费
     * pay_price 待付金额
     * has_account是否显示余额支付
     * account_money 用户wtp
     *  payment_list => Array 支付方式
    (
    [0] => Array
    (
    [id] => 22
    [code] => WxApp
    [logo] =>
    [name] => 微信支付
    )
    [1] => Array
    (
    [id] => 24
    [code] => Upacpapp
    [logo] =>
    [name] => 银联支付
    )

    )
     */
    public function pay()
    {

        $root = array();
        $user_id = intval($GLOBALS['user_info']['id']);
        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }

        $root['user_login_status']=1;

        $order_id = intval($_REQUEST['id']);
        $order_status = $this->check_order($order_id);

        if(!$order_status){
            $root['status'] = 0;
            $root['error']  = "非法数据";
            api_ajax_return($root);
        }

        $payment = intval($_REQUEST['payment']);
        $all_account_money = intval($_REQUEST['all_account_money']);

        $root['order_id'] = $order_id;
        $order_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id." and is_delete = 0 and user_id = ".$GLOBALS['user_info']['id']);
        if(empty($order_info))
        {
            $root['status'] = 0;
            $root['error']  = "订单不存在";
            api_ajax_return($root);
        }
        if($order_info['pay_status']==2)
        {
            $root['status'] = 2;
            $root['error']  = "订单已付款";
            api_ajax_return($root);
        }
        $total_price = $order_info['total_price'];


        $is_weixin = isWeixin();


        //处理购物车输出
        $cart_list_select = "doi.*,d.id as did,d.icon,d.uname as duname ,  d.allow_user_discount ,do.presell_deposit_money,do.presell_discount_money,do.presell_type,d.presell_delivery_time,d.is_presell,d.presell_begin_time,do.presell_end_time,d.allow_user_fx_discount,d.user_fx_discount";
        $cart_list = $GLOBALS['db']->getAll("select ".$cart_list_select." from ".DB_PREFIX."deal_order_item as doi left join ".DB_PREFIX."deal as d on doi.deal_id = d.id left join ".DB_PREFIX."deal_order as do on doi.order_id=do.id where doi.order_id = ".$order_info['id']);
        $root['cart_list'] = $cart_list;
        $root['is_main'] = $order_info['is_main'];
        //输出支付方式
        //$app_index = APP_INDEX; APP,SDK支付做好后，使用这个判断


        $consignee_id=$order_info['consignee_id'];
        if($order_info['is_presell_order']==1 && $order_info['pay_status']>0){
            $order_info['account_money'] =  $order_info['pay_amount'];
        }

        $result = $this->count_buy_total($order_info['region_lv4'],$consignee_id,$payment,0,$all_account_money,$ecvsn='',$ecvpassword='',$cart_list,$order_info['account_money'],$order_info['ecv_money'],$bank_id=0,0,$order_info['exchange_money']);

        $root['total_price'] = $order_info['total_price'] - $order_info['payment_fee'];
        $root['total_price'] = round($root['total_price'],2);
        $root['pay_amount'] = round($order_info['pay_amount'],2);

        if($result['is_presell']==1){

            if($order_info['pay_status']==0){
                $root['pay_price'] = $order_info['presell_deposit_money'] + $result['payment_fee'];
            }else{
                $root['pay_price'] = $order_info['total_price'] - ( $order_info['presell_discount_money'] - $order_info['presell_deposit_money'] ) - $order_info['payment_fee'] - $order_info['pay_amount'] -$order_info['youhui_money'] + $result['payment_fee'];

            }
        }else{
            $root['pay_price'] = $order_info['total_price'] - $order_info['payment_fee'] - $order_info['pay_amount'] -$order_info['youhui_money'] + $result['payment_fee'];
        }

        $root['pay_price'] = round($root['pay_price'],2);
        $root['payment_fee'] = round($result['payment_fee'],2);
        $root['all_account_money']=$result['all_account_money'];
        $root['payment']=0;
        if($result['payment_info']){
            $root['payment']=$result['payment_info']['id'];
            $root['rel']=$_REQUEST['rel'];
        }

        $root['show_payment'] = 1;

        //$web_payment_list = load_auto_cache("cache_payment");
        $web_payment_list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."payment where online_pay in (0,1) and is_effect = 1 order by sort desc");
        foreach($web_payment_list as $k=>$v)
        {
            if($v['class_name']=="Account"&&($GLOBALS['user_info']['money']+$GLOBALS['user_info']['non_pre_money'])>=$root['pay_price'])
            {
                $root['has_account'] = 1;
            }
        }


        if(intval($_REQUEST['is_ajax'])==0&&$root['has_account']==1){
            $root['all_account_money']=1;
        }
        $root['account_money'] = round($GLOBALS['user_info']['money'],2)+round($GLOBALS['user_info']['non_pre_money'],2);//可提现+不可提现


        if($root['pay_price'] > 0){

            if (APP_INDEX=='wap'  && $is_weixin)
            {
                //支付列表
                $sql = "select id, class_name as code, logo,config from ".DB_PREFIX."payment where (online_pay = 2 or online_pay = 4 or online_pay = 5 or online_pay = 6) and is_effect = 1 and class_name !='Walipay'";
            }elseif (APP_INDEX=='wap' && !$is_weixin)
            {
                //支付列表
                $sql = "select id, class_name as code, logo,config from ".DB_PREFIX."payment where (online_pay = 2 or online_pay = 4 or online_pay = 5 or online_pay = 6) and is_effect = 1 and class_name !='Wwxjspay'";
            }
            else
            {
                //支付列表
                $sql = "select id, class_name as code, logo,config from ".DB_PREFIX."payment where (online_pay = 3 or online_pay = 4 or online_pay = 5 or online_pay = 6) and is_effect = 1";
                if ($_REQUEST['sdk_type']=="android"){
                    $sql.="  and class_name !='Iappay'";
                }
            }


            //if(allow_show_api())
            {
                $payment_list = $GLOBALS['db']->getAll($sql);
            }


            foreach($cart_list as $k=>$v)
            {
                if($GLOBALS['db']->getOne("select define_payment from ".DB_PREFIX."deal where id = ".$v['deal_id'])==1)
                {
                    $define_payment_list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_payment where deal_id = ".$v['deal_id']);
                    $define_payment = array();
                    foreach($define_payment_list as $kk=>$vv)
                    {
                        array_push($define_payment,$vv['payment_id']);
                    }
                    foreach($payment_list as $k=>$v)
                    {
                        if(in_array($v['id'],$define_payment))
                        {
                            unset($payment_list[$k]);
                        }
                    }
                }
            }


            foreach($payment_list as $k=>$v)
            {

                $directory = APP_ROOT_PATH."system/payment/";
                $file = $directory. '/' .$v['code']."_payment.php";
                if(file_exists($file))
                {
                    require_once($file);
                    $payment_class = $v['code']."_payment";
                    $payment_object = new $payment_class();
                    $payment_list[$k]['name'] = $payment_object->get_display_code();
                }

                if($v['logo']!="")
                    $payment_list[$k]['logo'] = get_abs_img_root(get_spec_image($v['logo'],40,40,1));

                if($v['code']=="Cod"){//非驿站订单
                    $v=$payment_list[$k];
                    unset($payment_list[$k]);
                }
                //echo $order_info['type'];echo $v['code'];
                if($order_info['type']==4&&$v['code']=="Cod"){
                    $Cod_config=unserialize($v['config']);
                    //echo "<pre>";print_r($Cod_config);exit;
                    $payment_lang=$payment_object->payment_lang;
                    if(count($Cod_config)>0){
                        $Cod_arr=array();
                        $Cod=$v['name'];
                        foreach($Cod_config['COD_PAYMENT'] as $key=>$val){
                            $v['name']=$Cod."(".$payment_lang['COD_PAYMENT_'.$key].")";
                            $v['rel']=$key;
                            $Cod_arr[]=$v;
                        }
                    }else{
                        $Cod_arr[]=$v;
                    }

                }
            }
            if(count($payment_list)>0&&count($Cod_arr)>0){
                $payment_list=array_merge($payment_list,$Cod_arr);
            }elseif(count($payment_list)>0&&count($Cod_arr)==0){

            }elseif(count($payment_list)==0&&count($Cod_arr)>0){
                $payment_list=$Cod_arr;
            }else{

            }
            sort($payment_list);
            $root['payment_list'] = $payment_list;

        }

        $root['page_title'] = "收银台";
        if(TWO_LEVEL_PAY_PASSWORD==1&&app_conf("TWO_LEVEL_PAY_PASSWORD")==1){
            require_once(APP_ROOT_PATH."system/model/vali_password.php");//清空支付密码
            $user_is_vali=vali_user_password($GLOBALS['user_info']['id'],"deal");
            $root["TWO_LEVEL_PAY_PASSWORD"]= 1;
            if(!$GLOBALS['user_info']['pay_password']){
                $root['no_pay_password'] = 1;
                $root['password_url'] = SITE_DOMAIN.url("index","user#pay_password",array("type"=>1,"order_id"=>$order_id));
            }
        }

        //是否开启微豆支付
        $root['open_balance_module']=1;
        $root['diamonds']=$GLOBALS['db']->getOne("select diamonds from ".DB_PREFIX."user where id = ".$user_id);

        $root['diamonds_view']=number_format(($root['diamonds']*0.1),2,".","");


        //判断密码
        $paypassword=$GLOBALS['db']->getOne("select paypassword from ".DB_PREFIX."user where id = ".$user_id);
        $root['has_paypassword']=$paypassword?1:0;


        api_ajax_return($root);
    }

    /**
     * 订单继续支付接口
     * 输入：
     * order_id:int 订单ID
     * payment:int 支付方式ID
     * all_account_money:int 是否使用余额支付 0否 1是
     *
     * 输出：
     * status: int 状态 0:失败 1:成功 -1:未登录
     * info: string 失败时返回的错误信息，用于提示
     * 以下参数为status为1时返回
     * pay_status:int 0未支付成功 1全部支付
     * order_id:int 订单ID
     * sdk_code 第三方支付的SDK_CODE
     *
     */
    public function order_done()
    {

        $root = array();
        $user_id = intval($GLOBALS['user_info']['id']);
        $root['status'] = 1;
        if ($user_id == 0) {
            $root['status'] = 10007;
            $root['error']  = "请先登录";
            api_ajax_return($root);
        }

        $order_id = intval($_REQUEST['order_id']);

        $order_status = $this->check_order($order_id);

        if(!$order_status){
            $root['status'] = 2;
            $root['error']  = "非法数据";
            api_ajax_return($root);
        }


        $order = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id." and is_delete = 0 and user_id = ".$GLOBALS['user_info']['id']);

        if(empty($order))
        {
            $root['status'] = 0;
            $root['error']  = "订单不存在";
            api_ajax_return($root);
        }
        if($order['refund_status'] == 1)
        {
            $root['status'] = 0;
            $root['error']  = "订单退款中";
            api_ajax_return($root);
        }
        if($order['refund_status'] == 2)
        {
            $root['status'] = 0;
            $root['error']  = "订单已退款";
            api_ajax_return($root);
        }

        $delivery_id =  $order['delivery_id']; //配送方式
        $consignee_id =  $order['consignee_id'];//会员收货地址id

        if( $_REQUEST['from']=='wap'){
            $is_wap=true;
        }else{
            $is_wap=false;
        }

        $region_id = 0; //配送地区

        //$consignee_info = load_auto_cache("consignee_info",array("consignee_id"=>$consignee_id));
        $param['consignee_id'] = intval($GLOBALS['db']->getOne("select id from ".DB_PREFIX."user_consignee where id = ".intval($consignee_id)));
        $consignee_id = intval($param['consignee_id']);
        $consignee_info =  $consignee_data['consignee_info'] = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_consignee where id = ".$consignee_id);
        $region_lv1 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = 0");  //一级地址
        foreach($region_lv1 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv1'])
            {
                $region_lv1[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv1_name'] = $v['name'];
                break;
            }
        }
        $consignee_data['region_lv1'] = $region_lv1;

        $region_lv2 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv1']));  //二级地址
        foreach($region_lv2 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv2'])
            {
                $region_lv2[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv2_name'] = $v['name'];
                break;
            }
        }

        $consignee_data['region_lv2'] = $region_lv2;

        $region_lv3 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv2']));  //三级地址
        foreach($region_lv3 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv3'])
            {
                $region_lv3[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv3_name'] = $v['name'];
                $consignee_data['consignee_info']['region_lv3_code'] = $v['code'];
                break;
            }
        }
        $consignee_data['region_lv3'] = $region_lv3;

        $region_lv4 = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."delivery_region where pid = ".intval($consignee_info['region_lv3']));  //四级地址
        foreach($region_lv4 as $k=>$v)
        {
            if($v['id'] == $consignee_info['region_lv4'])
            {
                $region_lv4[$k]['selected'] = 1;
                $consignee_data['consignee_info']['region_lv4_name'] = $v['name'];
                break;
            }
        }
        $consignee_data['region_lv4'] = $region_lv4;
        if($consignee_data['consignee_info']){
            $consignee_data['consignee_info']['full_address'] = $consignee_data['consignee_info']['region_lv1_name'].$consignee_data['consignee_info']['region_lv2_name'].$consignee_data['consignee_info']['region_lv3_name'].$consignee_data['consignee_info']['region_lv4_name'].$consignee_data['consignee_info']['address'].$consignee_data['consignee_info']['street'].$consignee_data['consignee_info']['doorplate'];
        }

        $consignee_info=$consignee_data;

        $consignee_info = $consignee_info['consignee_info']?$consignee_info['consignee_info']:array();
        if($consignee_info)
            $region_id = intval($consignee_info['region_lv4']);

        $payment = intval($_REQUEST['payment']);
        $rel = strim($_REQUEST['rel']);
        $all_account_money = intval($_REQUEST['all_account_money']);
        $ecvsn = $_REQUEST['ecvsn']?strim($_REQUEST['ecvsn']):'';
        $ecvpassword = $_REQUEST['ecvpassword']?strim($_REQUEST['ecvpassword']):'';
        if(!$is_wap){
            $memo = strim($_REQUEST['content']);
        }
        if($all_account_money){
            //验证支付密码
            $pay_result=check_paypassword(strim($_REQUEST['paypassword']));
            if ($pay_result['status']!=1){
                api_ajax_return($pay_result);
            }
        }


        /*if($all_account_money&&TWO_LEVEL_PAY_PASSWORD==1&&app_conf("TWO_LEVEL_PAY_PASSWORD")==1){
            //验证支付密码
            require_once(APP_ROOT_PATH."system/model/vali_password.php");
            $user_is_vali=vali_user_password($GLOBALS['user_info']['id'],"deal");
            if($user_is_vali['status']==0){
                $root['status'] = 0;
                $root['error']  = $user_is_vali['info'];
                api_ajax_return($root);
            }
        }*/

        $goods_list_select = " doi.* ,d.pt_money, d.allow_user_discount ,d.presell_deposit_money,d.presell_discount_money,d.presell_type,d.presell_delivery_time,d.is_presell,d.presell_begin_time,d.presell_end_time,d.allow_user_fx_discount,d.user_fx_discount";
        $goods_list = $GLOBALS['db']->getAll("select ".$goods_list_select." from ".DB_PREFIX."deal_order_item as doi left join ".DB_PREFIX."deal as d on doi.deal_id=d.id where doi.order_id = ".$order['id']);

        //结束验证购物车
        $deal_s = $GLOBALS['db']->getAll("select distinct(deal_id) as deal_id ,number  from ".DB_PREFIX."deal_order_item where order_id = ".$order['id']);
        //如果属于未支付的

        if($order['pay_status'] == 0)
        {
            foreach($deal_s as $row)
            {
                $checker = $this->check_deal_number($row['deal_id'],$row['number']);
                if($checker['status']==0)
                {
                    $root['status'] = 0;
                    $root['error']  = $checker['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$checker['data']];
                    api_ajax_return($root);
                }
            }

            foreach($goods_list as $k=>$v)
            {
                $checker = $this->check_deal_number_attr($v['deal_id'],$v['attr_str'],$v['number']);
                if($checker['status']==0)
                {
                    $root['status'] = 0;
                    $root['error']  = $checker['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$checker['data']];
                    api_ajax_return($root);
                }
            }

            //验证商品是否过期
            /*foreach($deal_s as $row)
            {
                $checker = $this->check_deal_time($row['deal_id']);
                if($checker['status']==0)
                {
                    $root['status'] = 0;
                    $root['error']  = $checker['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$checker['data']];
                    api_ajax_return($root);
                }
            }*/
        }


        //结束验证购物车
        //开始验证订单接交信息
        $data = $this->count_buy_total($region_id,$consignee_id,$payment,0,$all_account_money,$ecvsn,$ecvpassword,$goods_list,$order['account_money'],$order['ecv_money'],'',0,$order['exchange_money'],$youhui_ids=array(),$order['youhui_money'],$order['is_consumer_send_score'],unserialize($order['consumer_send_score_data']));

        if(round($data['pay_price'],4)>0&&!$data['payment_info']&&!$all_account_money)
        {
            $root['status'] = 0;
            $root['error']  = "请选择支付方式";
            api_ajax_return($root);
        }
        //结束验证订单接交信息

        $user_id = $GLOBALS['user_info']['id'];
        //开始生成订单
        $now = NOW_TIME;
        $order['total_price'] = $data['pay_total_price'];  //应付总额  商品价 - 会员折扣 + 运费 + 支付手续费
        $order['deal_total_price'] = $data['total_price'];   //团购商品总价
        $order['discount_price'] = $data['user_discount'];
        $order['delivery_fee'] = $data['delivery_fee'];
        $order['record_delivery_fee'] = $data['record_delivery_fee'];
        $order['payment_id'] = $data['payment_info']['id'];
        if($data['payment_info']['class_name']=="Cod"){
            $order['cod_mode'] = $rel;
        }else{
            $order['cod_mode'] = '';
        }
        $order['payment_fee'] = $data['payment_fee'];
        $order['bank_id'] = "";

        // 生成订单时已经写入了促销信息。这里重复了
        /*foreach($data['promote_description'] as $promote_item)
        {
            $order['promote_description'].=$promote_item."<br />";
        }*/
        $order['promote_arr']=serialize($data['promote_arr']);
        //更新来路

        //消费送积分
        $order['is_consumer_send_score'] = $data['is_consumer_send_score'];
        $order['consumer_send_score'] = $data['consumer_send_score'];
        $order['consumer_send_score_data'] = serialize($data['consumer_send_score_data']);


        $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order",$order,'UPDATE','id='.$order['id'],'SILENT');

        //生成order_id 后
        //if($user_id==100436){log_result($data);exit();}
        //2. 余额支付
        $account_money = $data['account_money'];
        //$pay_price = $data['pay_price'];
        if(floatval($account_money) > 0)
        {
            //$account_payment_id = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."payment where class_name = 'Account'");
            //$payment_notice_id = make_payment_notice($account_money,$order_id,$account_payment_id);
            //require_once(APP_ROOT_PATH."system/payment/Account_payment.php");
            //$account_payment = new Account_payment();
            //$account_payment->get_payment_code($payment_notice_id);
            //$payment_notice = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."payment_notice where id = ".$payment_notice_id);

            $user_money = $GLOBALS['db']->getRow("select diamonds from ".DB_PREFIX."user where id = ".$user_id);

            $payment_notice_id = $this->make_payment_notice($account_money,$order_id,0);

            $payment_notice = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."payment_notice where id = ".$payment_notice_id);

                $order_sn = $GLOBALS['db']->getOne("select order_sn from ".DB_PREFIX."deal_order where id = ".$payment_notice['order_id']);
                require_once(APP_ROOT_PATH."system/libs/user.php");
                $m_config =  load_auto_cache("m_config");
                $msg = sprintf('%s订单付款,付款单号%s,支付%s'.$m_config['diamonds_name'],$order_sn,$payment_notice['notice_sn'],($payment_notice['money']*10));

                if($user_money['diamonds']*0.1>=$payment_notice['money']){
                    //优先使用不可提现余额付款
                    $account_res=modify_account(array('diamonds'=>"-".$payment_notice['money']*10,0),$payment_notice['user_id'],$msg,array('type'=>12));
                    if ($account_res){
                        $order_item=array();
                        $order_item['pay_amount']=$payment_notice['money'];
                        $order_item['account_money']=$payment_notice['money'];
                        $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order",$order_item,"UPDATE","id=".$order_id);

                        $notice_item=array();
                        $notice_item['is_paid']=1;
                        $notice_item['pay_time']=time();
                        $GLOBALS['db']->autoExecute(DB_PREFIX."payment_notice",$notice_item,"UPDATE","id=".$payment_notice_id);
                    }else{
                        $root['status'] = 0;
                        $root['error'] = "余额不足";
                        api_ajax_return($root);
                    }
                }else {

                    $root['status'] = 0;
                    $root['error'] = "余额不足";
                    api_ajax_return($root);
                }




        }
        //货到付款
        /*if($data['payment_info']['class_name']=="Cod"&&$data['pay_price']>0){
            $account_payment_id = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."payment where class_name = 'Cod'");
            $payment_notice_id = make_payment_notice($data['pay_price'],$order_id,$account_payment_id,'',0,array('COD_PAYMENT'=>$rel));
            require_once(APP_ROOT_PATH."system/payment/Cod_payment.php");
            $account_payment = new Cod_payment();
            $account_payment->get_payment_code($payment_notice_id);
        }*/
// 		//3. 相应的支付接口
// 		$payment_info = $data['payment_info'];
// 		if($payment_info&&$data['pay_price']>0)
// 		{
// 			$payment_notice_id = make_payment_notice($data['pay_price'],$order_id,$payment_info['id']);
// 			//创建支付接口的付款单
// 		}
        $root['app_index'] = APP_INDEX;
        $rs = $this->order_paid($order_id);
        $this->update_order_cache($order_id);

        $root['supplier_id']=$order['supplier_id'];
        if($rs)
        {
            //是否有拼团is_pt, pt_group_id, deal_id
            $order_new = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id." and is_delete = 0 and user_id = ".$GLOBALS['user_info']['id']);
            $root['is_pt'] = $order_new['is_pt'];
            $root['pt_group_id'] = $order_new['pt_group_id'];
            $root['deal_id'] = $order_new['pt_id'];
            $root['pay_status'] = 1;
            $root['order_id'] = $order_id;
        }
        else
        {
            $this->distribute_order($order_id);

            if(APP_INDEX=='app'){
                //$data_pay = call_api_core("payment","done",array("id"=>$order_id));
                $data_pay = $this->payment_done();
                $root['sdk_code'] = $data_pay['payment_code']['sdk_code'];
                $root['pay_url'] = $data_pay['payment_code']['pay_action'];
                $root['online_pay'] = $data['payment_info']['online_pay'];
                $root['title'] = $data_pay['title'];
                $root['is_account_pay'] =$data_pay['is_account_pay'];
            }

            $root['pay_status'] = 0;
            $root['order_id'] = $order_id;

        }
        api_ajax_return($root);
    }

    public function clear_deal_cart(){
        $root = array();
        /*if($GLOBALS['request']['from']=="app"){
            $GLOBALS['request']['id']=json_decode($GLOBALS['request']['id'],true);
        }
        $id= $GLOBALS['request']['id'];*/
        $id=json_decode($_REQUEST['id'],true);
        $user_info=$GLOBALS['user_info'];
        $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where user_id = " . intval($user_info['id'])." and id in (".implode(",",$id).")");
        if($GLOBALS['db']->affected_rows() > 0){
            $root['status'] = 1;
            $root['error']  = "删除成功";
            api_ajax_return($root);
        }else{
            $root['status'] = 0;
            $root['error']  = "删除失败";
            api_ajax_return($root);
        }
    }


    /**
     *
     * @param boolean $reload 是否重新加载，废弃
     * @param boolean $is_wap 是否是wap端
     * @param boolean $wap_show_disable wap端是否显示未选择中商品，用于wap端购物车和订单提交页面分别显示
     * @param int $in_cart 购物车中的商品是否需要经过购物车页面，1为要经过购物车页面，0为不需要经过购物车页面，直接进入订单提交页面
     */

    function load_cart_list($deal_id=0,$wap_show_disable=false)
    {

        if($wap_show_disable){
            $deal_id=0; //如果进入购物车，则$deal_id=0;调用 所有购物车中的商品信息
        }
        $base_sql_select= "c.*,d.shop_cate_id,d.icon,d.id as did,d.delivery_type,d.current_price,d.uname as duname,d.is_delivery as is_delivery,
                    d.allow_promote as allow_promote,d.user_min_bought as user_min_bought,d.user_max_bought as user_max_bought,
                    d.is_shop,d.allow_user_discount,d.presell_deposit_money,d.presell_discount_money,d.presell_type,d.presell_delivery_time,
                    d.is_presell,d.presell_begin_time,d.presell_end_time,d.pt_money,d.allow_user_fx_discount,d.user_fx_discount";
        $base_sql = "select ".$base_sql_select." from ".DB_PREFIX."deal_cart as c left join ".DB_PREFIX."deal as d on c.deal_id = d.id where ";
        $base_total_sql = "select sum(total_price) as total_price,sum(return_total_score) as return_total_score,sum(return_total_money) as return_total_money ,count(*) as total_num from ".DB_PREFIX."deal_cart where ";
        if($GLOBALS['user_info']){

            if($deal_id > 0){
                //单个走这里
                $cart_list_res = $GLOBALS['db']->getAll($base_sql."c.deal_id=".$deal_id." and c.user_id = ".intval($GLOBALS['user_info']['id']));
                $total_data = $GLOBALS['db']->getRow($base_total_sql."deal_id=".$deal_id." and user_id = ".intval($GLOBALS['user_info']['id']));
                /*print_r(3391);
                print_r($base_sql."c.deal_id=".$deal_id." and c.user_id = ".intval($GLOBALS['user_info']['id']));
                print_r($base_total_sql."deal_id=".$deal_id." and user_id = ".intval($GLOBALS['user_info']['id']));
                exit();*/
            }else{
                if($wap_show_disable){
                    $cart_list_res = $GLOBALS['db']->getAll($base_sql."((c.in_cart=1) ) and c.user_id = ".intval($GLOBALS['user_info']['id']));
                    $total_data = $GLOBALS['db']->getRow($base_total_sql."((in_cart=1) ) and user_id = ".intval($GLOBALS['user_info']['id']));
                }else{
                    //全部走这里
                    $cart_list_res = $GLOBALS['db']->getAll($base_sql."((c.in_cart=1) ) and c.is_effect=1 and c.user_id = ".intval($GLOBALS['user_info']['id']));
                    $total_data = $GLOBALS['db']->getRow($base_total_sql."((in_cart=1) ) and is_effect=1 and user_id = ".intval($GLOBALS['user_info']['id']));
                    /*print_r(3400);
                    print_r($base_sql."c.in_cart=1 and c.is_effect=1 and c.user_id = ".intval($GLOBALS['user_info']['id']));
                    print_r($base_total_sql."in_cart=1 and is_effect=1 and user_id = ".intval($GLOBALS['user_info']['id']));
                    exit();*/
                }
            }
        }else{
            if($deal_id > 0){
                $cart_list_res = $GLOBALS['db']->getAll($base_sql."c.deal_id=".$deal_id." and c.user_id =0 and c.session_id = '".es_session::id()."'");
                $total_data = $GLOBALS['db']->getRow($base_total_sql."deal_id=".$deal_id." and user_id =0 and session_id = '".es_session::id()."'");
            }else{
                if($wap_show_disable){
                    $cart_list_res = $GLOBALS['db']->getAll($base_sql."c.in_cart=1 and c.user_id =0 and c.session_id = '".es_session::id()."'");
                    $total_data = $GLOBALS['db']->getRow($base_total_sql."in_cart=1 and user_id =0 and session_id = '".es_session::id()."'");
                }else{
                    $cart_list_res = $GLOBALS['db']->getAll($base_sql."c.in_cart=1 and c.is_effect=1 and c.user_id =0 and c.session_id = '".es_session::id()."'");
                    $total_data = $GLOBALS['db']->getRow($base_total_sql."in_cart=1 and is_effect=1 and user_id =0 and session_id = '".es_session::id()."'");
                }
            }
        }


        //print_r($cart_list_res);exit();
        $cart_list = array();
        $is_presell=0;
        $is_pt=0;
        $is_pt_buy = 0;
        foreach($cart_list_res as $k=>$v) {
            if($v['duname']!=""){
                $v['url'] = url("index","deal#".$v['duname']);
            } else {
                $v['url'] = url("index","deal#".$v['did']);
            }

            if($v['supplier_id']>0){
                $v['supplier_part'] = $v['supplier_id'];
            }else{
                if($v['delivery_type']==1){//平台物流配送
                    $v['supplier_part'] = 'p_wl';
                }elseif ($v['delivery_type']==2){//平台无需配送
                    $v['supplier_part'] = 0;
                }elseif ($v['delivery_type']==3){//平台驿站配送
                    $v['supplier_part'] = 'p_yz';
                }
            }

            if(IS_PRESELL && $v['is_presell']==1 && $v['presell_begin_time'] < NOW_TIME &&  $v['presell_end_time'] > NOW_TIME){
                $is_presell = 1;
            }
            if($is_presell==1){

                $v['return_score']=0;
                $v['return_total_score']=0;
                $v['return_money']=0;
                $v['return_total_money']=0;

            }

            //判断商品是否还符合，拼团设置条件
            $is_pt_buy = $v['is_pt_buy'];
            if($is_pt_buy){ //如果是拼团购买
                $pt=$this->is_pt_deal($v['deal_id']);
                $is_pt = intval($pt['is_pt']);
            }

            $user_info=$GLOBALS['user_info'];
            $user_discount_percent = 1;
            /*if (!empty($user_info['id']) && $v['allow_user_discount']) {
                $user_discount_percent = getUserDiscount($user_info['id']);
                if($v['allow_user_fx_discount']&&$user_info['is_fx']){
                    $user_discount_percent *=$v['user_fx_discount'];
                }
            }elseif($v['allow_user_fx_discount']&&$user_info['is_fx']){
                $user_discount_percent =$v['user_fx_discount'];
            }*/
            $user_discount_percent =$v['user_fx_discount'];

            $v['current_price'] *= $user_discount_percent;
            $cart_list[$v['id']] = $v;
        }

        $total_data['is_presell'] = $is_presell;
        $total_data['is_pt'] = $is_pt;	//拼团信息有效
        $total_data['is_pt_buy'] = $is_pt_buy;	//拼团购买,检查订单还会用到

        $result = array("cart_list"=>$cart_list,"total_data"=>$total_data);
        es_session::set("cart_result", $result);
        return $result;

    }

    /**
     * 判断商品是什么类型，是否直接有进入购物车页面
     * @param unknown $deal_id 商品ID
     * @param string $app_index 终端， wap和PC ,该参数决定返回的页面
     * @param array $param 额外参数数组
     * $result['in_cart']=1，有进入购物车， $result['in_cart']=0，直接进入订单提交页面
     * $result['jump'] 加入购物车成功后的跳转页面
     */

    function get_cart_type($deal_id,$app_index='wap',$param=array()){

        $deal_info = $GLOBALS['db']->getRow("select 
									    	id,is_delivery,buy_type,is_shop,supplier_id,
									    	is_pt,is_presell,presell_begin_time,presell_end_time 
									    	from ".DB_PREFIX."deal where id=".$deal_id);

        $is_presell = $deal_info['is_presell'];
        $is_pt_buy = $param['is_pt_buy'];
        if($deal_info['is_shop']==0 || ( $deal_info['is_shop']==1 && ($deal_info['is_delivery']==0 ||$is_pt_buy&& $this->is_pt_deal($deal_info['id'])||IS_PRESELL &&$is_presell&& $deal_info['presell_begin_time']<NOW_TIME && $deal_info['presell_end_time']>NOW_TIME)) || $deal_info['buy_type']==1 ){//团购，积分，预售商品，无需发货的商品 直接进入提交订单页，不进入购物车页面
            $result['in_cart'] = 0;
            if($app_index=='wap'){
                $result['jump'] = url("index","cart#check",array("id"=>$deal_id,'is_presell'=>$is_presell,'is_pt_buy'=>$is_pt_buy));

            }else{
                $result['jump'] = url("index","cart#check",array("id"=>$deal_id,'is_presell'=>$is_presell,'is_pt_buy'=>$is_pt_buy));
            }

        }else{
            $result['in_cart'] = 1;
            if($app_index=='wap'){
                $result['jump'] = url("index","cart");
            }else{
                $result['jump'] = url("index","cart");
            }

        }
        return $result;

    }

    /**
     * 检测团购的属性数量状态
     * $id 团购ID
     * $attr_setting 属性组合的字符串
     * $number 数量
     */
    function check_deal_number_attr($id,$attr_setting,$number=0)
    {

        if($attr_setting){
            $cart_result = $this->load_cart_list($id);

            $id = intval($id);
            $deal_info =  $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$id."' and is_delete = 0 or (uname= '".$id."' and uname <> '')");

            $attr_stock_cfg = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."attr_stock where deal_id = ".$id." and locate(attr_str,'".$attr_setting."') > 0 ");

            if(!$attr_stock_cfg){
                $result['status'] = 0;
                $result['data'] = "规格已失效";
                $result['info'] = "规格:".$attr_setting."已下架，请选择其他规格";
                $result['attr'] = $attr_setting;
                return $result;
            }

            $stock_setting = $attr_stock_cfg?intval($attr_stock_cfg['stock_cfg']):-1;
            $stock_attr_setting = $attr_stock_cfg['attr_str'];
            // 获取到当前规格的库存

            /*验证数量*/
            //定义几组需要的数据
            //1. 本团购记录下的购买量
            $deal_buy_count = intval($attr_stock_cfg['buy_count']);
            //2. 本团购当前会员的购物车中数量
            $deal_user_cart_count = 0;
            /*foreach($cart_result['cart_list'] as $k=>$v)
            {
                if($v['deal_id']==$id&&strpos($v['attr_str'],$stock_attr_setting)!==false)
                {
                    $deal_user_cart_count+=intval($v['number']);
                }
            }*/
            //3. 本团购当前会员未付款的数量

            $stock = $stock_setting;

            $result['stock'] = intval($stock);


            if($stock_setting == 0||($deal_user_cart_count+$number>$stock_setting&&$stock_setting>=0))
            {
                $result['status'] = 0;
                $result['data'] = DEAL_OUT_OF_STOCK;  //库存不足
                $result['info'] = $deal_info['sub_name'].$stock_attr_setting." ".sprintf('总库存为%s件',$stock_setting);
                $result['attr'] = $stock_attr_setting;
                return $result;
            }

            /*验证数量*/
        }

        $result['status'] = 1;
        $result['info'] = $deal_info['sub_name'];
        return $result;

    }

    /**
     * 检测团购的数量状态
     * $id 团购ID
     * $number 数量
     */
    function check_deal_number($id,$number = 0,$is_no_addcart=true)
    {

        $cart_result = $this->load_cart_list($id);

        $id = intval($id);
        $deal_info =  $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$id."' and is_delete = 0 or (uname= '".$id."' and uname <> '')");


        /*验证数量*/
        //定义几组需要的数据
        //1. 本团购记录下的购买量
        $deal_buy_count = $deal_info['buy_count'];
        //2. 本团购当前会员的购物车中数量
        $deal_user_cart_count = 0;
        foreach($cart_result['cart_list'] as $k=>$v)
        {
            if($v['deal_id']==$id)
            {
                $deal_user_cart_count += intval($v['number']);
            }
        }
        //3. 本团购当前会员已付款的数量
        // $deal_user_paid_count = intval($GLOBALS['db']->getOne("select sum(oi.number) from ".DB_PREFIX."deal_order_item as oi left join ".DB_PREFIX."deal_order as o on oi.order_id = o.id where o.is_main=0 and  o.user_id = ".intval($GLOBALS['user_info']['id'])." and o.pay_status = 2 and oi.deal_id = ".$id." and o.is_delete = 0"));
        $deal_user_paid_count = $this->countUserBuy($id, $GLOBALS['user_info']['id']);
        //4. 本团购当前会员未付款的数量

        $invalid_count = 0;
        foreach($cart_result['cart_list'] as $k=>$v)
        {
            if($v['number']<=0)
            {
                $invalid_count++;
            }
        }

        $stock = $deal_info['max_bought'];

        $result['stock'] = intval($stock);
        if($invalid_count>0 && $deal_info['is_presell']==0)
        {
            $result['status'] = 0;
            $result['data'] = DEAL_ERROR_MIN_USER_BUY;  //用户最小购买数不足
            $result['info'] = $deal_info['sub_name']." ".sprintf('每个用户最少要买%s件',1);
            return $result;
        }

        if($deal_info['max_bought'] == 0||($deal_user_cart_count+$number>$deal_info['max_bought']&&$deal_info['max_bought']>0))
        {
            $result['status'] = 0;
            $result['data'] = DEAL_OUT_OF_STOCK;  //库存不足
            $result['info'] = $deal_info['sub_name']." ".sprintf('总库存为%s件',$deal_info['max_bought']);
            return $result;
        }


        if($is_no_addcart){
            if($deal_user_cart_count + $number < $deal_info['user_min_bought'] && $deal_info['user_min_bought'] > 0 && $deal_info['is_presell']==0)
            {
                $result['status'] = 0;
                $result['data'] = DEAL_ERROR_MIN_USER_BUY;  //用户最小购买数不足
                $result['info'] = $deal_info['sub_name']." ".sprintf('每个用户最少要买%s件',$deal_info['user_min_bought']).",已购买".$deal_user_cart_count."件";
                return $result;
            }
        }

        if($deal_user_cart_count + $deal_user_paid_count + $number > $deal_info['user_max_bought'] && $deal_info['user_max_bought'] > 0)
        {
            $result['status'] = 0;
            $result['data'] = DEAL_ERROR_MAX_USER_BUY;  //用户最大购买数超出
            $result['info'] = $deal_info['sub_name']." ".sprintf('每个用户最多买%s件',$deal_info['user_max_bought']).",已购买".$deal_user_paid_count."件";
            if(!$is_no_addcart){
                $result['info'] = $deal_info['sub_name']." ".sprintf('每个用户最多买%s件',$deal_info['user_max_bought']).",已购买".($deal_user_paid_count+$deal_user_cart_count)."件";
            }
            return $result;
        }


        /*验证数量*/
        $result['status'] = 1;
        $result['info'] = $deal_info['sub_name'];
        return $result;
    }

    //处理同步购物车中的相关状态的团购产品
    /**
     *
    1. 如果购物车中有禁用(3), 如果禁用项最后加入，保留禁用项，反之，删除禁用项
    2. 如购物车中有按商户禁用(2), 如果加入商户禁用是最后加入，删除与之不相同的商户的商品，反之删除需商户禁用的所有相关的商品
    3. 如购物车中有按商品禁用(1), 如果加入商品禁用是最后加入，删除与之不相同的商品，反之删除该商品
     */
    function syn_cart()
    {

        $first_row = $GLOBALS['db']->getRow("select dc.*,d.cart_type as cart_type from ".DB_PREFIX."deal_cart as dc left join ".DB_PREFIX."deal as d on dc.deal_id = d.id where dc.session_id = '".es_session::id()."' and dc.user_id = ".intval($GLOBALS['user_info']['id'])." order by dc.create_time desc");
        //1. 处理禁用全部的状态 cart_type 3
        $result = $GLOBALS['db']->getAll("select dc.id,dc.deal_id,dc.supplier_id from ".DB_PREFIX."deal_cart as dc left join ".DB_PREFIX."deal as d on dc.deal_id = d.id where dc.session_id = '".es_session::id()."' and dc.user_id = ".intval($GLOBALS['user_info']['id'])." and d.cart_type = 3");
        if($result)
        {
            if($first_row['cart_type']==3)
            {
                //保留禁用购物车的产品，其他删除
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where session_id = '".es_session::id()."' and user_id = ".intval($GLOBALS['user_info']['id'])." and id <> ".$first_row['id']);
                return;
            }
            else
            {
                $ids = array(0);
                foreach($result as $row)
                {
                    array_push($ids,$row['id']);
                }
                //删除禁用购物车的产品
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where session_id = '".es_session::id()."' and user_id = ".intval($GLOBALS['user_info']['id'])." and id in (".implode(",",$ids).")");
                return;
            }
        }

        //2. 处理按商户禁用的状态 cart_type 2
        $result = $GLOBALS['db']->getAll("select dc.id,dc.deal_id,dc.supplier_id from ".DB_PREFIX."deal_cart as dc left join ".DB_PREFIX."deal as d on dc.deal_id = d.id where dc.session_id = '".es_session::id()."' and dc.user_id = ".intval($GLOBALS['user_info']['id'])." and d.cart_type = 2");

        if($result)
        {
            if($first_row['cart_type']==2)
            {
                //保留禁用商户的产品以及同商户商品，其他删除
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where session_id = '".es_session::id()."' and user_id = ".intval($GLOBALS['user_info']['id'])." and supplier_id <> ".$first_row['supplier_id']);
                return;
            }
            else
            {
                $ids = array(0);
                foreach($result as $row)
                {
                    array_push($ids,$row['supplier_id']);
                }
                //删除禁用商户的产品以及同商户商品
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where session_id = '".es_session::id()."' and user_id = ".intval($GLOBALS['user_info']['id'])." and supplier_id in (".implode(",",$ids).")");
                return;
            }
        }

        //3. 处理按商品禁用的状态 cart_type 1
        $result = $GLOBALS['db']->getAll("select dc.id,dc.deal_id,dc.supplier_id from ".DB_PREFIX."deal_cart as dc left join ".DB_PREFIX."deal as d on dc.deal_id = d.id where dc.session_id = '".es_session::id()."' and dc.user_id = ".intval($GLOBALS['user_info']['id'])." and d.cart_type = 1");

        if($result)
        {

            if($first_row['cart_type']==1)
            {
                //保留禁用商品以及其他款式的商品，其他删除
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where session_id = '".es_session::id()."' and user_id = ".intval($GLOBALS['user_info']['id'])." and deal_id <> ".$first_row['deal_id']);
                return;
            }
            else
            {
                $ids = array(0);
                foreach($result as $row)
                {
                    array_push($ids,$row['deal_id']);
                }
                //删除禁用商户的产品以及同款商品
                $GLOBALS['db']->query("delete from ".DB_PREFIX."deal_cart where session_id = '".es_session::id()."' and user_id = ".intval($GLOBALS['user_info']['id'])." and deal_id in (".implode(",",$ids).")");
                return;
            }
        }

    }



    /**
     * @判断是否为拼团商品（如果为过期或者无效也为 非拼团商品）
     * @author    吴庆祥
     * @param array $deal_info
     * @return array 拼团活动配置信息
     */
    function is_pt_deal($deal_id){
        $pt_deal_info = array();
        //if(IS_PT){
            //拼团商品
            $pt_deal_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id=".intval($deal_id)." and is_effect = 1 and is_pt=1");
            if($pt_deal_info){
                //是否有效；开始时间为不限或者开始时间小于当前时间（已经开始）；成团用户数量>0且成团时间>0; 结束时间不限或者结束时间大于当前时间「满足条件为拼团商品」
                if($pt_deal_info['is_pt']==1){
                    $flag=1;
                }
            }

        //}
        return $pt_deal_info;
    }

    /**
     * 获取会员已购买的商品数量
     * 买单订单和商品订单都算
     * @param  int $deal_id
     * @param  int $user_id
     * @return int
     */
    function countUserBuy($deal_id, $user_id)
    {
        $dealBuySql = 'SELECT sum(oi.number) FROM '.DB_PREFIX.'deal_order_item AS oi LEFT JOIN '.DB_PREFIX.'deal_order AS o ON oi.order_id=o.id WHERE o.is_main=0 AND o.user_id='.$user_id.' AND o.pay_status=2 AND oi.deal_id='.$deal_id;
        $dealBuy = $GLOBALS['db']->getOne($dealBuySql);
        //if (isOpenStoreDealOrder()) {
        if (false) {
            $storeDealBuySql = 'SELECT sum(soi.number) FROM '.DB_PREFIX.'store_deal_order_item AS soi LEFT JOIN '.DB_PREFIX.'store_deal_order As so ON soi.order_id=so.id WHERE (so.pay_status=1 OR (so.pay_status=0 AND so.order_status=0)) AND so.user_id='.$user_id.' AND soi.deal_id='.$deal_id;
            $storeDealBuy = $GLOBALS['db']->getOne($storeDealBuySql);
            $dealBuy += $storeDealBuy;
        }
        return $dealBuy;
    }

    /**
     * 获取指定会员的折扣率
     * @param  int $user_info 会员id
     * @return float            折扣率
     */
    function getUserDiscount($user_id)
    {
        static $user_discounts = array();
        if (isset($user_discounts[$user_id])) {
            return $user_discounts[$user_id];
        }
        $user_discount = 1.0;
        /*$user_id = intval($user_id);
        $user_group_info = $GLOBALS['db']->getRow("select g.* from ".DB_PREFIX."user as u left join ".DB_PREFIX."user_group as g on u.group_id = g.id where u.id = ".$user_id);
        if (intval($user_group_info['id']) > 0 && floatval($user_group_info['discount']) > 0) {
            $user_discount = $user_group_info['discount'];
        }*/
        $user_discounts[$user_id] = $user_discount;
        return $user_discount;
    }

    /**
     *
     * @param unknown $deal_id 商品ID
     * @param array $attr  属性
     */
    function check_deal_status($deal_id,$attr ,$number,$check_stock=false)
    {
        $res=array();
        $res['status']=1;
        $res['info']='正常';
        $deal_info =  $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$deal_id."' and is_delete = 0 or (uname= '".$deal_id."' and uname <> '')");

        //团购的时间验证
        /*$check = $this->check_deal_time($deal_id);
        if($check['status'] == 0)
        {
            $res['status'] = 0;
            $res['info'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];

        }*/

        //注释以下这段，是为了把可编辑数量的商品，调到可编辑区，不再在失效区


        //团购数量验证
        $check = $this->check_deal_number($deal_id,$number);

        if($check['stock']==0){
            $check_stock=true;
        }
        //echo "<pre>";print_r($check);exit;
        if($check['status']==0 && $check_stock)
        {
            $res['status'] = 0;
            $res['info'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];

        }
        //开始运算购物车的验证
        $attr_setting_str = $attr['attr_str'];

        if($attr_setting_str!='')
        {//团购的属性数量验证

            $check = $this->check_deal_number_attr($deal_id,$attr_setting_str,$number);

            if($check['stock']==0){
                $check_stock=true;
            }
            if($check['status']==0 && $check_stock)
            {
                $res['status'] = 0;
                $res['info'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];

            }
            $res['stock'] = $check['stock'];
        }

        if(!isset($res['stock'])){  //如果有属性库存，则统计属性库存，没有属性，则统计默认库存
            $res['stock'] = $check['stock'];
        }

        if(!$deal_info){  //已下架
            $res['status']=0;
            $res['info']='商品已下架';
        }


        return $res;


    }


    function get_express_fee($cart_list,$user_consignee_id){

        $express_fee_arr = array(); //运费数组
        if (intval($user_consignee_id)){//用户的配送地址
            $consignee_item = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user_consignee where id=".intval($user_consignee_id));
            if (empty($consignee_item)){   //如果用户没有配送地址，直接返回空数组
                return $express_fee_arr;
            }
        }else//用户地址ID 必须存在
            return $express_fee_arr;


        //先计算用户等级折扣
        $user_id = intval($GLOBALS['user_info']['id']);
        $user_discount_percent = 1;
        if ($user_id) {
            $user_discount_percent = $this->getUserDiscount($user_id);
        }
        //获取商品的配送模板
        $deal_ids = array();
        $deal_number_arr=array();
        foreach ($cart_list as $k=>$v){
            $deal_ids[] = $v['deal_id'];
            $deal_number_arr[$v['deal_id']]['number'] += $v['number'];
            //$deal_user_discount = 0;
            $user_discount = 1;
            if ($v['allow_user_discount']) {
                if($v['allow_user_fx_discount']==1&&$GLOBALS['user_info']['is_fx']){
                    $user_discount = $user_discount_percent * $v['user_fx_discount'];
                }else{
                    $user_discount = $user_discount_percent;
                }
            }elseif($v['allow_user_fx_discount']==1&&$GLOBALS['user_info']['is_fx']){
                $user_discount = $v['user_fx_discount'];
            }
            $deal_user_discount =  round($v['unit_price'] * (1-$user_discount), 2) * $v['number'];

            $deal_number_arr[$v['deal_id']]['total_price'] += $v['total_price'] - $deal_user_discount ;
        }

        $deal_list = $GLOBALS['db']->getAll("select id,name,supplier_id,weight,platform_type,delivery_type,carriage_template_id from ".DB_PREFIX."deal where id in (".implode($deal_ids,",").")");
        $carriage_template_ids = array();
        foreach ($deal_list as $k=>$v){
            //购买的件数
            $deal_list[$k]['number'] = $deal_number_arr[$v['id']]['number'];
            $deal_list[$k]['total_price'] = $deal_number_arr[$v['id']]['total_price'];
            $carriage_template_ids[] = $v['carriage_template_id'];
        }


        if ($carriage_template_ids){



            //获取运费模板价格
            $sql = "select ct.id,ct.name,ct.supplier_id,ct.carriage_type,ct.valuation_type,cd.id as carriage_detail_id,cd.express_start,cd.express_plus,cd.express_postage,cd.express_postage_plus,cd.region_ids from  ".DB_PREFIX."carriage_template ct LEFT JOIN ".DB_PREFIX."carriage_detail cd on ct.id=cd.carriage_id WHERE ct.id in (".implode($carriage_template_ids,",").")";
            $carriage_template_list = $GLOBALS['db']->getAll($sql);
            $format_carriage_templat_list = array();
            foreach ($carriage_template_list as $k=>$v){
                $format_carriage_templat_list[$v['id']][]=$v;
            }


            //根据模板类型来计费，同一类型的模板用重量或者件数累加计费
            //把商品根据商户或者平台来分割数组-根据配送方式分组
            $supplier_by_deals = array();
            $supplier_cart_total = array();
            $supplier_list = array();
            foreach ($deal_list as $k=>$v){
                $supplier_list[] = $v['supplier_id'];
                if($v['supplier_id']>0){
                    $supplier_by_deals[$v['supplier_id']][] = $v;
                    $supplier_cart_total[$v['supplier_id']]['total_price'] += $v['total_price'];
                }else{
                    if($v['delivery_type']==1){//平台物流配送
                        $supplier_by_deals['p_wl'][] = $v;
                        $supplier_cart_total['p_wl']['total_price'] += $v['total_price'];
                    }elseif ($v['delivery_type']==3){//平台驿站配送
                        $supplier_by_deals['p_yz'][] = $v;
                        $supplier_cart_total['p_yz']['total_price'] += $v['total_price'];
                    }
                }
            }
            $sql = "select * from ".DB_PREFIX."promote where supplier_id in(".implode(',',$supplier_list).") and class_name='Freebyprice'";
            $delivery_free_info = $GLOBALS['db']->getAll($sql);
            $delivery_free = array();
            if($delivery_free_info){
                foreach($delivery_free_info as $k=>$v){
                    $delivery_free[$v['supplier_id']] = unserialize($v['config']);
                }

            }
            foreach($supplier_cart_total as $k=>$v){
                $supplier_cart_total[$k]['is_delivery_free']=0;
                if($delivery_free[intval($k)]['is_open_delivery_free']==1 && $delivery_free[intval($k)]['delivery_free_money'] <= $v['total_price'] ){
                    $supplier_cart_total[$k]['is_delivery_free']=1;
                    $supplier_cart_total[$k]['delivery_free_money']=$delivery_free[intval($k)]['delivery_free_money'];
                }
            }


            foreach ($supplier_by_deals as $k=>$v){


                if($supplier_cart_total[$k]['is_delivery_free']==1){
                    $express_fee_arr[$k]['is_delivery_free'] = 1;
                    $express_fee_arr[$k]['delivery_free_money'] = $supplier_cart_total[$k]['delivery_free_money'];
                }else{


                    $carriage_temp = array();

                    foreach($v as $deal_k=>$deal_v){
                        $carriage_id = 0;           //模板运费ID
                        $carriage_type = 0;         //运费类型：1自定义，2平台/卖家承担运费（免运费）
                        $carriage_detail_id = 0;    //模板运费详情ID
                        $express_start = 0;         //初始运费商品：多少件或kg
                        $express_postage = 0;       //运费金额
                        $express_plus = 0;          //增加运费商品：多少件或kg
                        $express_postage_plus = 0;  //运费根据商品增加增加的金额
                        $valuation_type = 0;        //计价类型：1按件数，2按重量
                        $express_info_temp = array();   //运费计算详情数组

                        //根据模板ID 遍历，模板运费详情
                        foreach ($format_carriage_templat_list[$deal_v['carriage_template_id']] as $ck=>$cv){
                            $carriage_id = $cv['id'];
                            if ($cv['carriage_type']==2){//如果是卖家承担运费直接跳过，运费详情的计算
                                $carriage_type = $cv['carriage_type'];
                                break;
                            }
                            if($cv['region_ids']){  //判断是否存在指定运费地区内
                                if (in_array($consignee_item['region_lv3'],explode(",",$cv['region_ids']))){
                                    $carriage_detail_id = intval($cv['carriage_detail_id']);
                                    $express_start = $cv['express_start'];
                                    $express_postage = $cv['express_postage'];
                                    $express_plus = $cv['express_plus'];
                                    $express_postage_plus = $cv['express_postage_plus'];
                                    $valuation_type = $cv['valuation_type'];
                                    break;
                                }
                            }else{  //默认运费详情
                                $carriage_detail_id = intval($cv['carriage_detail_id']);
                                $express_start = $cv['express_start'];
                                $express_postage = $cv['express_postage'];
                                $express_plus = $cv['express_plus'];
                                $express_postage_plus = $cv['express_postage_plus'];
                                $valuation_type = $cv['valuation_type'];
                            }
                        }
                        //运费计算详情数组
                        $express_info_temp = array(
                            'carriage_id'=>$carriage_id,
                            'carriage_detail_id'=>$carriage_detail_id,
                            'carriage_type' => $carriage_type,
                            'express_start'=>$express_start,
                            'express_postage'=>$express_postage,
                            'express_plus'=>$express_plus,
                            'express_postage_plus'=>$express_postage_plus,
                            'valuation_type'=>$valuation_type,
                        );

                        //根据运费计算详情建立数组
                        if (empty($carriage_temp[$carriage_id."_".$carriage_detail_id]['express_detail'])){
                            $carriage_temp[$carriage_id."_".$carriage_detail_id]['express_detail']=$express_info_temp;
                        }
                        //同一个运费模板并且计算规则相同的商品放入一个数组
                        $carriage_temp[$carriage_id."_".$carriage_detail_id]['deals'][] = $deal_v;
                    }

                    //遍历获取到使用的运费模板计算规则
                    foreach ($carriage_temp as $ctemp_k=>$ctemp_v){
                        $temp_express_fee = 0;
                        $temp_weight = 0;
                        $temp_number = 0;
                        $temp_express_detail = $ctemp_v['express_detail'];

                        //同一运费计算规则的商品，件数或者数量进行累加计算总运费
                        foreach ($ctemp_v['deals'] as $deal_item_k=>$deal_item_v){
                            $express_fee_arr[$k]['deal_ids'][] = array("id"=>$deal_item_v['id'],"name"=>$deal_item_v['name'],'weight'=>$deal_item_v['weight'],'number'=>$deal_item_v['number'],'express_detail'=>$temp_express_detail);
                            $temp_weight += $deal_item_v['weight']*$deal_item_v['number'];
                            $temp_number += $deal_item_v['number'];
                        }

                        if($temp_express_detail['carriage_type']==2){//卖家承担运费
                            $temp_express_fee=0;
                        }else{

                            //按件计费，按照重量计费计算最后不能有小数
                            if($temp_express_detail['valuation_type']==1){//按件计费
                                if($temp_number <= $temp_express_detail['express_start']){
                                    $temp_express_fee = $temp_express_detail['express_postage'];
                                }else{
                                    $temp_express_fee = $temp_express_detail['express_postage']+ceil(($temp_number - $temp_express_detail['express_start'])/$temp_express_detail['express_plus'])*$temp_express_detail['express_postage_plus'];
                                }
                            }elseif($temp_express_detail['valuation_type']==2){//按照重量计费
                                if($temp_weight <= $temp_express_detail['express_start']){
                                    $temp_express_fee = $temp_express_detail['express_postage'];
                                }else{
                                    $temp_express_fee = $temp_express_detail['express_postage']+ceil(($temp_weight - $temp_express_detail['express_start'])/$temp_express_detail['express_plus'])*$temp_express_detail['express_postage_plus'];
                                }

                            }
                        }
                        $express_fee_arr[$k][$ctemp_k]+=$temp_express_fee;
                        //分类 平台自营-物流、平台自营-驿站、商户
                        $express_fee_arr[$k]['total_fee'] += $temp_express_fee;

                    }
                }
            }
            //运费只能为整数
            foreach ($express_fee_arr as $k=>$v){
                $express_fee_arr[$k]['total_fee'] = round($v['total_fee'],2);
            }
        }

        return $express_fee_arr;
    }
/*
* 购物车提交时判断是否允许提交订单
*$checked_ids = Array
* (
*     [0] => 	Array(
[id] => 11 int 购物车id
[attr] => 11 varchar 购买的相关属性的ID，用半角逗号分隔
[attr_str] => 11 varchar 购买的相关属性的ID，用半角逗号分隔
[number] => 11 int 数量
)
* )

*/
    function cheak_wap_cart($checked_ids){

        $user_info=$GLOBALS['user_info'];
        if($checked_ids){

            $deal_num=array();
            foreach($checked_ids as $k=>$v){
                $cart_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_cart where user_id = " . $user_info['id']." and id=".$v['id']);
                $deal_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id=".$cart_info['deal_id']);
                $checked_ids[$k]['deal_id'] = $deal_info['id'];
                $checked_ids[$k]['deal_info'] = $deal_info;

                if($v['attr_str']){
                    $deal_num[$deal_info['id']][$v['attr_str']]+=$v['number'];
                }
                $deal_num[$deal_info['id']]['number']+=$v['number'];


            }
            $checked_id = array();
            foreach($checked_ids as $k=>$v){
                $checked_id[] = $v['id'];
                $checked_ids[$k]['attr_number']=0;
                if($v['attr_str']){
                    $checked_ids[$k]['attr_number']=intval($deal_num[$v['deal_id']][$v['attr_str']]);   //有属性的商品购物车一共有几个
                }
                $checked_ids[$k]['total_number']=intval($deal_num[$v['deal_id']]['number']);  //无属性的商品购物车一共有几个

            }

            /* 判断购物车选择中的商品，是否有平台自营的商品和入驻商家的商品，平台自营商品不能与商家商品一起下单   */
// 		$deal_cart_status = check_deal_cart($checked_id);
// 		if(!$deal_cart_status){ //平台自营商品不能与商家商品一起下单
// 			$result['status'] = 0;
// 			$result['info'] = '平台自营商品不能与商家商品一起下单';
// 			return $result;
// 		}

            foreach($checked_ids as $k=>$v){

                $deal_info=$v['deal_info'];

                /*验证数量*/
                //定义几组需要的数据
                //1. 本团购记录下的购买量
                $deal_buy_count = $deal_info['buy_count'];
                //2. 本团购当前会员的购物车中数量
                $number = $v['number'];
                $total_number = $v['total_number'];

                $attr_number = $v['attr_number'];
                $attr_str = $v['attr_str'];
                //3. 本团购当前会员已付款的数量
                $deal_user_paid_count = intval($GLOBALS['db']->getOne("select sum(oi.number) from ".DB_PREFIX."deal_order_item as oi left join ".DB_PREFIX."deal_order as o on oi.order_id = o.id where o.is_main=0 and o.user_id = ".intval($GLOBALS['user_info']['id'])." and o.pay_status = 2 and oi.deal_id = ".$deal_info['id']." and o.is_delete = 0"));

                if($total_number<=0)
                {
                    $result['status'] = 0;
                    $result['data'] = DEAL_ERROR_MIN_USER_BUY;  //用户最小购买数不足
                    $result['info'] = $deal_info['sub_name']." ".sprintf('每个用户最少要买%s件',1)." ".$GLOBALS['lang']['DEAL_ERROR_'.$result['data']];
                    return $result;
                    break;
                }

                if($deal_info['max_bought'] == 0||($total_number>$deal_info['max_bought']&&$deal_info['max_bought']>=0))
                {
                    $result['status'] = 0;
                    $result['data'] = DEAL_OUT_OF_STOCK;  //库存不足
                    $result['info'] = $deal_info['sub_name']." ".sprintf('总库存为%s件',$deal_info['max_bought'])." ".$GLOBALS['lang']['DEAL_ERROR_'.$result['data']];
                    return $result;
                    break;
                }

                if( $total_number < $deal_info['user_min_bought'] && $deal_info['user_min_bought'] > 0)
                {
                    $result['status'] = 0;
                    $result['data'] = DEAL_ERROR_MIN_USER_BUY;  //用户最小购买数不足
                    $result['info'] = $deal_info['sub_name']." ".sprintf('每个用户最少要买%s件',$deal_info['user_min_bought']).",购物车数量为".($total_number)."件, ".$GLOBALS['lang']['DEAL_ERROR_'.$result['data']];
                    return $result;
                    break;
                }

                if( $deal_user_paid_count  + $total_number > $deal_info['user_max_bought'] && $deal_info['user_max_bought'] > 0)
                {
                    $deal_buy_num =  $deal_user_paid_count;
                    $result['status'] = 0;
                    $result['data'] = DEAL_ERROR_MAX_USER_BUY;  //用户最大购买数超出
                    $result['info'] = $deal_info['sub_name']." ".sprintf('每个用户最多买%s件',$deal_info['user_max_bought']).",已购买".$deal_buy_num."件, ".$GLOBALS['lang']['DEAL_ERROR_'.$result['data']];
                    return $result;
                    break;
                }


                $now = NOW_TIME;
                //开始验证团购时间
                if($deal_info['begin_time']!=0)
                {
                    //有开始时间
                    if($now<$deal_info['begin_time'])
                    {
                        $result['status'] = 0;
                        $result['data'] = DEAL_NOTICE;  //未上线
                        $result['info'] = $deal_info['sub_name']." ".$GLOBALS['lang']['DEAL_ERROR_'.$result['data']];
                        return $result;
                        break;
                    }
                }


                if($deal_info['end_time']!=0)
                {
                    //有结束时间
                    if($now>=$deal_info['end_time'])
                    {
                        $result['status'] = 0;
                        $result['data'] = DEAL_HISTORY;  //过期
                        $result['info'] = $deal_info['sub_name']." ".$GLOBALS['lang']['DEAL_ERROR_'.$result['data']];
                        return $result;
                        break;
                    }
                }

                $attr_stock_cfg = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."attr_stock where deal_id = ".$deal_info['id']." and locate(attr_str,'".$attr_str."') > 0 ");

                $stock_setting = $attr_stock_cfg?intval($attr_stock_cfg['stock_cfg']):-1;
                $stock_attr_setting = $attr_stock_cfg['attr_str'];
                // 获取到当前规格的库存

                /*验证数量*/
                //定义几组需要的数据
                //1. 本团购记录下的购买量
                $deal_buy_count = intval($attr_stock_cfg['buy_count']);

                if($stock_setting == 0||($attr_number>$stock_setting&&$stock_setting>=0))
                {
                    $result['status'] = 0;
                    $result['data'] = DEAL_OUT_OF_STOCK;  //库存不足
                    $result['info'] = $deal_info['sub_name'].$stock_attr_setting." ".sprintf('总库存为%s件',$stock_setting)." ".$GLOBALS['lang']['DEAL_ERROR_'.$result['data']];
                    $result['attr'] = $stock_attr_setting;
                    return $result;
                    break;
                }

            }
            $result['status'] = 1;
            $result['info'] = '验证通过';
            return $result;

        }

    }

    function check_order($order_id){
        $deal_order = $GLOBALS['db']->getRow("select is_main,is_presell_order,pay_status,create_time,presell_end_time,order_id from ".DB_PREFIX."deal_order where id=".$order_id);
        $has_pay=0;
        if($deal_order['is_main']==1){ //如果是主单，则检测子单的支付情况
            $deal_order_check = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_order where pay_amount > 0 and is_main=0 and order_id=".$order_id);
            if(intval($deal_order_check) > 0){
                $has_pay=1;
            }

        }else{//如果子单，则检测主单的支付情况


            if($deal_order['is_presell_order']==1){
                $result = $this->check_order_allow_pay($deal_order);
                if($result['allow_pay']==1){
                    return true;
                }else{
                    return false;
                }
            }else{
                $deal_order_check = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_order where pay_amount > ecv_money + youhui_money and is_main=1 and id=".$deal_order['order_id']);
                if(intval($deal_order_check) > 0){
                    $has_pay=1;
                }
            }


        }
        return $has_pay==1?false:true;
    }

    function check_order_allow_pay($order_info){
        $allow_pay=1;
        $result = array();
        $error_tip='';
        if($order_info['pay_status']==0){
                $result['count_time'] = $order_info['create_time']+1800;  //下单后半小时要付款，否则取消订单
                if(($order_info['create_time']+1800) < NOW_TIME){
                    $allow_pay=0;
                    $error_tip='支付超时';
                }else{
                    $allow_pay=1;
            }

        }elseif($order_info['pay_status']==1){
            if(NOW_TIME >= $order_info['presell_end_time'] && NOW_TIME < ($order_info['presell_end_time']+3600 * 24 *7)){//
                $result['count_time'] = $order_info['presell_end_time']+3600 * 24 *7;//预售结束后7天内要付尾款，否则取消订单
                $result['count_type']=1;
                $allow_pay=1;
            }elseif(NOW_TIME > ($order_info['presell_end_time']+3600 * 24 *7)){//尾款支付截止时间已到，不可支付尾款
                $result['count_time'] = 0;
                $error_tip='尾款支付截止';
                $result['count_type']=3;
                $allow_pay=0;
            }else{//剩余多少时间可以尾款
                $result['count_time'] = $order_info['presell_end_time'];//剩余多少时间可以付尾款
                $result['count_type']=2;
                $error_tip='未到尾款支付时间';
                $allow_pay=0;
            }
        }else{
            $allow_pay=0;
        }
        $result['allow_pay'] = $allow_pay;
        if($error_tip){
            $result['error_tip'] = $error_tip;
        }
        return $result;
    }


    //计算购买价格
    /**
     * region_id      //配送最终地区
     * delivery_id    //配送方式
     * payment        //支付ID
     * account_money  //支付余额
     * all_account_money  //是否全额支付
     * ecvsn  //代金券帐号
     * ecvpassword  //代金券密码
     * goods_list   //统计的商品列表
     * $paid_account_money 已支付过的余额
     * $paid_ecv_money 已支付过的代金券
     *
     * 返回 array(
    'total_price'	=>	$total_price,	商品总价
    'pay_price'		=>	$pay_price,     支付费用
    'pay_total_price'		=>	$total_price+$delivery_fee+$payment_fee-$user_discount,  应付总费用
    'delivery_fee'	=>	$delivery_fee,  运费
    'delivery_fee_supplier' => array(supplier_id=>delivery_fee,......) //每个商家的运费
    'delivery_info' =>  $delivery_info, 配送方式
    'payment_fee'	=>	$payment_fee,   支付手续费
    'payment_info'  =>	$payment_info,  支付方式
    'user_discount'	=>	$user_discount, 会员折扣
    'account_money'	=>	$account_money, 余额支付
    'ecv_money'		=>	$ecv_money,		代金券金额
    'ecv_data'		=>	$ecv_data,      代金券数据
    'region_info'	=>	$region_info,	地区数据
    'is_delivery'	=>	$is_delivery,   是否要配送
    'return_total_score'	=>	$return_total_score,   购买返积分
    'return_total_money'	=>	$return_total_money    购买返现
    'buy_type'	=>	0,1 //1为积分商品

     */
    function count_buy_total($region_id,$consignee_id,$payment,$account_money,$all_account_money,$ecvsn,$ecvpassword,$goods_list,$paid_account_money = 0,$paid_ecv_money = 0,$bank_id = '',$all_score=0,$paid_exchange_money=0,$youhui_ids=array(),$paid_youhui_money=0,$order_is_cs_score=0,$order_cs_score_data=array())
    {

        //$data = count_buy_total($region_id,$consignee_id,0,0,$all_account_money,0,0,$goods_list,0,0,'',0,0,0);
        //获取商品总价
        //计算运费
        $pay_price = 0;   //支付总价
        $total_price = 0;
        $return_total_score = 0;
        $return_total_money = 0;
        $is_delivery = 0;
        $is_consignment=1;
        $free_delivery_item = 0;  //默认为免运费   需要所有商品满足免运费条件
        $deal_id = 0;
        $deal_count = 0;
        $deal_item_count = 0;
        $buy_type = 0;
        $is_pick = 1;
        $delivery_id = 0;
        $delivery_fee = 0;  //默认运费为0;
        $is_pt_buy = 0;	//拼团方式购买

        //先计算用户等级折扣
        $user_id = intval($GLOBALS['user_info']['id']);
        $user_discount_percent = 1;
        $user_discount = 0; // 折扣金额
        if ($user_id) {
            $user_discount_percent = $this->getUserDiscount($user_id);
        }

        if($youhui_ids){
            $youhui_ids = array_unique($youhui_ids);
        }

        $deal_ids = array();
        $supplier_ids=array();

        $good_total_data = array();
        $good_origin_total_data = array();

        foreach($goods_list as $k=>$v)
        {
            $is_presell=0;
            $is_pt=0;
            if($paid_account_money==0){ //未生成订单
                if(IS_PRESELL && $v['is_presell']==1 && $v['presell_begin_time']<NOW_TIME && $v['presell_end_time']  >NOW_TIME){
                    $is_presell=1;
                }
            }else{//已生成订单
                if(IS_PRESELL && ( $v['presell_end_time'] + 3600 * 24 *7 ) >NOW_TIME){
                    $is_presell=1;
                }
            }

            if(($v['is_pt_order']==1 || $v['is_pt_buy']==1)){ //开启拼团且是拼团商品,初始化拼团商品的值
                $pt=$this->is_pt_deal($v['deal_id']);
                $is_pt = intval($pt['is_pt']); //判断拼团商品是否有效使用
                $pt_group_id = $v['pt_group_id'];
                $pt_id = $v['pt_id'];
                $is_pt_order = $v['is_pt_order'];
            }


            if(in_array($v['supplier_id'], $supplier_ids)==false){
                $supplier_ids[]=$v['supplier_id'];
            }

            if(!isset($youhui_ids[$v['supplier_part']])){
                $youhui_ids[$v['supplier_part']] = 0;
            }

            $deal_ids[] = $v['deal_id'];
            if($v['buy_type']==1)$buy_type=1;

            //会员打折后的价格
            //$deal_user_discount = 0;
            $user_discount_local =1;
            if ($v['allow_user_discount'] && $is_presell==0 && $is_pt ==0) {
                if($v['allow_user_fx_discount']==1&&$GLOBALS['user_info']['is_fx']){
                    $user_discount_local = $user_discount_percent*$v['user_fx_discount'];
                }else{
                    $user_discount_local = $user_discount_percent;
                }
            }elseif($v['allow_user_fx_discount']==1 && $GLOBALS['user_info']['is_fx'] && $is_presell==0 && $is_pt ==0){
                $user_discount_local = $v['user_fx_discount'];
            }
            //$deal_user_discount =  round($v['unit_price'] * (1-$user_discount_local), 2) * $v['number'];
            $deal_user_discount = ($v['unit_price']-round($v['unit_price']*$user_discount_local,2)) * $v['number'];

            $user_discount += $deal_user_discount;

            $good_total_data[$v['supplier_part']]+=$v['total_price']-$deal_user_discount;
            $good_origin_total_data[$v['supplier_part']]+=$v['total_price'];
            $total_price += $v['total_price'];

            //预售相关逻辑...开始...
            $presell_deposit_money=0;	//预售定金
            $presell_discount_money=0;	//预售抵扣金额
            $presell_end_time=0;		//预售结束时间
            $presell_type = 0;			//0.订金1.定金

            //默认情况下返利和返积分
            $return_total_money+=$v['return_total_money'];
            $return_total_score+=$v['return_total_score'];

            //预售模块部分金额的计算...开始...
            if($is_presell==1){
                $presell_type = $v['presell_type'];
                $presell_end_time = $v['presell_end_time'];
                if($v['attr']==0){	//没有商品属性的价格
                    $presell_deposit_money = $v['presell_deposit_money'];
                    $presell_discount_money = $v['presell_discount_money'];
                }else{
                    $presell_price_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."attr_stock where deal_id = ".$v['deal_id']." and  locate(attr_str,'".$v['attr_str']."')");
                    $presell_deposit_money = $presell_price_info['presell_deposit_money'];
                    $presell_discount_money = $presell_price_info['presell_discount_money'];
                }
            }
            //预售模块部分金额计算...结束



            //拼团模块部分金额计算...开始...
            if($is_pt){
                $pt_money = $v['pt_money'];
                //获取规格属性数据
                if ($v['attr_str']) {
                    $pt_money = $GLOBALS['db']->getOne("select pt_money from 
															".DB_PREFIX."attr_stock where attr_str='{$v['attr_str']}' and deal_id='{$v['deal_id']}'");
                }
                $total_price = $pt_money*$v['number'];
                $return_total_money=0;
                $return_total_score=0;
            }


            //拼团模块部分金额计算...结束



            if($v['is_pick']==0)$is_pick=0;	//是否自提

            //$deal_info = load_auto_cache("deal",array("id"=>$v['deal_id']));
            $deal_info=$GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$v['deal_id']."' and is_delete = 0 or (uname= '".$v['deal_id']."' and uname <> '')");
            if($deal_info['is_delivery'] == 1 && $is_pick == 0) //需要配送叠加重量
            {
                $is_delivery = 1;
            }
            //delivery_type:配送方式(默认物流配送)1物流、2无需配送、3驿站;无需配送也需要发货流程。只有团购is_shop = 0 的时候 不要发货
            if($deal_info['is_shop']==1 && $deal_info['delivery_type']==2){
                $is_consignment=1;
            }elseif($deal_info['is_shop']==1 && $is_pick == 0){
                $is_consignment=1;
            }elseif($deal_info['is_shop']==0){
                $is_consignment=0;
            }
        }

        $presell_left_price = 0; //预售商品尾款
        if($is_presell==1){  //预售不可使用红包，积分兑换，优惠劵,不能自提
            $ecvsn='';
            $ecvpassword='';
            $all_score=0;
            $is_pick=0;
            $youhui_ids=array();

            $presell_left_price = $total_price - $presell_deposit_money ;
        }

        //预售相关逻辑...结束




        //运费计算相关部分...开始...

        if($is_pick == 1 && $consignee_id ==0 ){ //自提且配送地址id为0 的时候，不需要发货
            $is_consignment=0;
        }

        $region_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."delivery_region where id = ".intval($region_id));

        if($region_info['region_level']!=4
            &&$GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."delivery_region where pid = ".intval($region_id))==0)
        {
            $region_info['region_level'] = 4;
        }
        //获取运费数据
        $delivery_list = array();
        if($consignee_id > 0){
            $delivery_list = $this->get_express_fee($goods_list,$consignee_id);//运费价格列表
            if($delivery_list){
                foreach($delivery_list as $k=>$v){//根据不同商户运费格式化
                    $supplier_data[$k]['delivery_fee'] = $v['total_fee'];
                    $delivery_fee+=$v['total_fee'];//总费用
                }
            }
        }


        foreach($good_total_data as $k=>$v){ //每块的总费用格式化
            $good_total_data[$k]=$v + $supplier_data[$k]['delivery_fee'];
        }

        $count_delivery_fee = $delivery_fee; //统计总运费
        $pay_price = $total_price + $delivery_fee; //加上运费

        //运费计算相关部分...结束






        //应支付金额 = 支付金额 - 余额 - 代金券(红包) - 抵扣积分 - 优惠券抵扣
        $pay_price = $pay_price - $paid_account_money - $paid_ecv_money-$paid_exchange_money - $paid_youhui_money;

        $pay_price = $pay_price - $user_discount; //扣除用户折扣

        //余额支付
        $user_money = $GLOBALS['db']->getRow("select diamonds from ".DB_PREFIX."user where id = ".$user_id);

        $user_money = $user_money['diamonds']*0.1;//可提现+不可提现

        if($all_account_money == 1) //全款支付
        {

            //$account_money = $user_money;
            $account_money = $pay_price;
            $payment = 0;
        }

        /*if($account_money>$user_money)//余额支付量不能超过帐户余额 pc端部分支付涉及
            $account_money = $user_money;
        */

        //不同商品不同的费用计算流程...开始...

        if($is_presell){//预售商品流程

            if($paid_account_money > 0){ //支付金额= 应支付金额（包括运费） -（定金抵扣金额-已支付订金）
                $pay_price = $pay_price - ($presell_discount_money - $presell_deposit_money);
            }else{
                $pay_price = $presell_deposit_money ; //如果没有付过定金就是定金金额（没有运费）
            }

        }elseif ($is_pt) { //拼团商品流程
            //没有参与任何优惠
            //$account_money = $pay_price;
        }else{

            //计算优惠券支付
            $youhui_price=0;
            $youhui_data = array();

            $pay_limit_price = $total_price + $delivery_fee - $user_discount;	//优惠券满多少可付的金额条件

            if($youhui_ids){
                foreach ($youhui_ids as $t => $v){
                    $youhui_sql="select y.youhui_value from ".DB_PREFIX."youhui_log as yl left join ".DB_PREFIX."youhui as y on y.id=yl.youhui_id
                      where yl.confirm_time=0 and (yl.start_time=0 or yl.start_time<" . NOW_TIME . ") and (yl.expire_time=0 or yl.expire_time>".NOW_TIME.") and yl.id=".$v." and y.supplier_id in (".implode(",", $supplier_ids).")
    	              and (y.start_use_price<=".$pay_limit_price." or y.start_use_price=0) and yl.user_id=".$user_id;

                    $youhui_price_item=$GLOBALS['db']->getOne($youhui_sql);

                    if($good_total_data[$t] < $youhui_price_item){
                        $youhui_price_item = $good_total_data[$t];
                    }
                    $youhui_data[$t]['youhui_log_id']=$v;
                    $youhui_data[$t]['youhui_money']=$youhui_price_item;
                    $youhui_data[$t]['total_price']=$good_total_data[$t];
                    $youhui_data[$t]['origin_total_price']=$good_origin_total_data[$t];
                    $youhui_price+=$youhui_price_item;
                }
            }

            if($youhui_price>=$pay_price){	//优惠券足够的时候不使用红包
                $ecv_no_use_status=1;
                $youhui_price=$pay_price;
            }

            $pay_price=$pay_price-$youhui_price;
            if($paid_youhui_money > 0){	//已经支付的优惠券
                $youhui_price = $paid_youhui_money;
            }


            //开始计算代金券
            $now = NOW_TIME;
            /*$ecv_sql = "select e.* from ".DB_PREFIX."ecv as e left join ".
                DB_PREFIX."ecv_type as et on e.ecv_type_id = et.id where e.sn = '".
                $ecvsn."' and ((e.begin_time <> 0 and e.begin_time < ".$now.") or e.begin_time = 0) and ".
                "((e.end_time <> 0 and e.end_time > ".$now.") or e.end_time = 0) and ((e.use_limit <> 0 and e.use_limit > e.use_count) or (e.use_limit = 0)) ".
                "and (e.user_id = ".$user_id." or e.user_id = 0) and (et.start_use_price<=".$pay_limit_price." or et.start_use_price=0)";

            $ecv_data = $GLOBALS['db']->getRow($ecv_sql);
            $ecv_money = $ecv_data['money'];*/
            $ecv_money=0;
            $exchange_money=0;
            if($paid_exchange_money>0){
                $exchange_money=$paid_exchange_money;
            }else{
                if($return_total_score<0){//积分订单不参与
                    $score_purchase['score_purchase_switch']=0;
                }elseif($is_presell==1){//预售订单不参与
                    $score_purchase['score_purchase_switch']=0;
                }else{
                    if(app_conf("SCORE_PURCHASE_SWITCH")==1){
                        if($paid_exchange_money>0){//已参与过积分抵现
                            $exchange_money=$paid_exchange_money;
                            $score_purchase['score_purchase_switch']=0;
                        }else{
                            if($pay_price<=$ecv_money){
                                $score_purchase['score_purchase_switch']=0;
                            }else{

                                $user_score = $GLOBALS['db']->getOne("select score from ".DB_PREFIX."user where id = ".$user_id);
                                $score_purchase=score_purchase_count($user_score,($total_price + $delivery_fee-$user_discount),($pay_price-$ecv_money));

                                $score_purchase['score_purchase_switch']=1;

                                if($score_purchase['exchange_money']>0){

                                    if($all_score==1){
                                        $exchange_money=$score_purchase['exchange_money'];//扣除积分抵现
                                        $pay_price=$pay_price-$exchange_money;
                                    }
                                }else{
                                    $score_purchase['score_purchase_switch']=0;
                                }
                            }
                        }
                    }else{
                        $score_purchase['score_purchase_switch']=0;
                    }
                }
            }

            // 当余额 + 代金券 > 支付总额时优先用代金券付款  ,代金券不够付，余额为扣除代金券后的余额

            if($ecv_money + $account_money > $pay_price)
            {
                if($ecv_money >= $pay_price)
                {
                    $ecv_use_money = $pay_price;
                    $account_money = 0;
                }
                else
                {
                    $ecv_use_money = $ecv_money;
                    $account_money = $pay_price - $ecv_use_money;
                }
            }
            else
            {
                $ecv_use_money = $ecv_money;
            }

            $pay_price = $pay_price - $ecv_use_money - $account_money;

        }

        //不同商品不同的费用计算流程...结束





        //支付手续费计算部分...开始...

        if($account_money<0)$account_money = 0;//支付金额小于0 直接为0

        $payment_fee = 0; //支付手续费默认为0

        if($payment!=0) //有选择支付方式
        {


            if($pay_price>0) //支付金额大于0
            {
                $payment_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."payment where id = ".$payment);
                $directory = APP_ROOT_PATH."system/payment/";
                $file = $directory. '/' .$payment_info['class_name']."_payment.php";
                if(file_exists($file))
                {
                    require_once($file);
                    $payment_class = $payment_info['class_name']."_payment";
                    $payment_object = new $payment_class();
                    if(method_exists($payment_object,"get_name"))
                    {
                        $payment_info['name'] = $payment_object->get_name($bank_id);
                    }
                }



                if($payment_info['fee_type']==0) //定额
                {
                    $payment_fee = $payment_info['fee_amount'];
                }
                else //比率
                {
                    $payment_fee = $pay_price * $payment_info['fee_amount'];
                }
                $pay_price = $pay_price + $payment_fee;
            }
            $account_money = 0;
            $all_account_money=0;
        }
        else
        {
            if($is_presell==1 && $account_money>=$pay_price){
                $account_money = $pay_price;
                $pay_price = $pay_price - $account_money;
            }elseif($is_pt==1 && $account_money>=$pay_price){
                $account_money = $pay_price;
                $pay_price = $pay_price - $account_money;
            }

        }





        //营销模块-消费送积分...开始...

        $is_consumer_send_score=0;	//是否参与消费送积分
        $consumer_send_score=0;		//消费送积分的积分数量
        $consumer_send_score_data=array();	//消费送积分配置信息
        $is_open_consumer_send_score=0; //是否开启了消费送积分

        //实际支付金额
        if($account_money>0){
            $payment_fee=0;
            $actual_price=$account_money;
        }else{
            $actual_price=$pay_price;
        }
        //$order_is_cs_score=0,$order_cs_score_data
        if($order_is_cs_score==1){	//订单
            $is_open_consumer_send_score=1;
            if($actual_price>0&&$return_total_score>=0&&$buy_type==0&&$is_presell==0&&$is_pt==0){
                if($order_cs_score_data['send_integral_proportion']>0){
                    $return_total_score=0;
                    $consumer_send_score = intval($actual_price)*$order_cs_score_data['send_integral_proportion'];
                    $is_consumer_send_score=1;
                    $consumer_send_score_data=$order_cs_score_data;
                    $consumer_send_score_data['actual_pay_price']=$actual_price;
                    $consumer_send_score_data['actual_pay_price_send_score']=$consumer_send_score;
                }
            }
        }else{
            //$send_integral_data=send_integral_conf();
            $arr=array();
            $arr['send_integral_switch']=0;
            $send_integral_data=arr;
            if($send_integral_data['send_integral_switch']==1&&$return_total_score>=0){
                $is_open_consumer_send_score=1;
                $return_total_score=0;
            }
            if($actual_price>0&&$return_total_score>=0&&$buy_type==0&&$is_presell==0&&$is_pt==0){//非积分订单,非预售订单,非拼团订单，支付金额大于0
                if($send_integral_data['send_integral_switch']==1&&$send_integral_data['send_integral_proportion']>0){
                    $is_open_consumer_send_score=1;
                    $return_total_score=0;
                    $consumer_send_score = intval($actual_price)*$send_integral_data['send_integral_proportion'];
                    $is_consumer_send_score=1;
                    $consumer_send_score_data=array(
                        'send_integral_proportion'=>$send_integral_data['send_integral_proportion'],
                        'actual_pay_price'=>$actual_price,
                        'actual_pay_price_send_score'=>$consumer_send_score,
                    );
                }
            }
        }

        //营销模块-消费送积分...结束



        if($ecv_use_money==0){
            $ecv_data = array();
        }

        $result = array(
            'total_price'	=>	$total_price,
            'pay_price'		=>	$pay_price,
            'pay_total_price'		=>	$total_price+$delivery_fee+$payment_fee-$user_discount-$exchange_money,
            'delivery_fee'	=>	$delivery_fee,
            'record_delivery_fee'	=>	$delivery_fee,
            'count_delivery_fee'  => $count_delivery_fee,
            'delivery_fee_supplier' => array(),
            'delivery_info' =>  $delivery_list,
            'payment_fee'	=>	$payment_fee,
            'payment_info'  =>	$payment_info,
            'user_discount'	=>	$user_discount,
            'account_money'	=>	$account_money,
            'all_account_money'=>$all_account_money,
            'youhui_money'  =>	$youhui_price,
            'youhui_data'  =>	$youhui_data,
            'ecv_money'		=>	$ecv_use_money,
            'ecv_data'		=>	$ecv_data,
            'exchange_money'=>	$exchange_money,
            'score_purchase'=>	$score_purchase,
            'region_info'	=>	$region_info,
            'is_delivery'	=>	$is_delivery,
            'return_total_score'	=>	$return_total_score,
            'return_total_money'	=>	$return_total_money,
            'paid_account_money'	=>	$paid_account_money,
            'paid_ecv_money'	=>	$paid_ecv_money,
            'buy_type'	=> $buy_type,
            'is_pick' => $is_pick,
            'is_consignment'  =>  $is_consignment,
            'ecv_no_use_status'=>$ecv_no_use_status,
            'presell_deposit_money'=>$presell_deposit_money,
            'presell_discount_money'=>$presell_discount_money,
            'presell_left_price'=>$presell_left_price,
            'is_presell'=>$is_presell,
            'presell_type'=>$presell_type,
            'presell_end_time'=>$presell_end_time,
            'is_consumer_send_score'	=>	$is_consumer_send_score,
            'consumer_send_score'	=>	$consumer_send_score,
            'consumer_send_score_data'	=>	$consumer_send_score_data,
            'is_open_consumer_send_score'	=>	$is_open_consumer_send_score,
            'is_pt'=>intval($is_pt),
            'is_pt_order'=>intval($is_pt_order),
            'pt_id'=>intval($pt_id),
            'pt_group_id'=>intval($pt_group_id),
        );

        return $result;
    }


    /**
     * 拆单：把主单按商家拆成若干子单
     * @param int $order_id 主单ID
     */
    function syn_order($order_id){
        $deal_order = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id=".$order_id);
        $main_order=$deal_order;
        $main_score_purchase=unserialize($deal_order['score_purchase']);
        $deal_order_item = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order_item where order_id=".$order_id);
        $order_item = array();
        $return_data = array();
        foreach($deal_order_item as $k=>$v){

            if($v['supplier_id']==0){  //如果是平台自营商品，得区分是驿站配送和物流配送，驿站配送和物流配送进行拆单
                if($v['delivery_type']==1){  //物流配送
                    $order_item['p_wl'][] = $v;
                    $return_data['p_wl']['return_total_score']+=$v['return_total_score'];
                    $return_data['p_wl']['return_total_money']+=$v['return_total_money'];
                }elseif($v['delivery_type']==3){  //驿站配送
                    $order_item['p_yz'][] = $v;
                    $return_data['p_yz']['return_total_score']+=$v['return_total_score'];
                    $return_data['p_yz']['return_total_money']+=$v['return_total_money'];
                }

            }else{
                $order_item[$v['supplier_id']][] = $v;
                $return_data[$v['supplier_id']]['return_total_score']+=$v['return_total_score'];
                $return_data[$v['supplier_id']]['return_total_money']+=$v['return_total_money'];
            }
        }
        unset($deal_order['id']);

        $order_sn = $deal_order['order_sn'];
        $deal_total_price_main = $deal_order['deal_total_price'];
        $discount_price_main = $deal_order['discount_price'];
        $supplier_data = unserialize($deal_order['supplier_data']);
        //log_result($deal_order['supplier_data']);log_result($supplier_data);
        $order_sub_data = array();
        $order_sub_arr = array();

        /*$tempDiscount = 0;

        */
        $tempOrderCount = count($order_item);
        $tempIndex = 0;
        $tempExchangeMoney=0;
        $tempUserUseScore=0;
        $no_total_price= 0 ;  //优惠足可抵扣所有费用大的的子单的总金额
        foreach($supplier_data as $k=>$v){
            if($v['youhui_data']['total_price'] <=  $v['youhui_data']['youhui_money']){
                $no_total_price += $v['youhui_data']['total_price'];
                $tempOrderCount--;
            }
        }

        //生成订单的子单
        foreach($order_item as $k=>$v){
            if($k=='p_wl'){  //物流配送
                $deal_order['type'] = 3;
                $deal_order['supplier_id'] = 0;
            }elseif($k=='p_yz'){  //驿站配送
                $deal_order['type'] = 4;
                $deal_order['supplier_id'] = 0;
            }else{
                $deal_order['type'] = 6; //商家商品订单,商家团购订单，不走拆单流程
                $deal_order['supplier_id'] = $k;
            }


            $is_consignment = 0;
            $deal_order['is_main'] = 0;
            $deal_order['order_sn'] = $order_sn.str_pad($k, 6,'0',STR_PAD_LEFT);
            $deal_ids=array();
            $deal_total_price=0;
            $total_price = 0;
            foreach($v as $kk=>$vv){
                $deal_ids[] = $vv['deal_id'];
                $total_price+=$vv['discount_unit_price'] * $vv['number'];
                $deal_total_price+=$vv['total_price'];

                if($vv['is_shop']==1 && $vv['is_pick'] == 0){
                    $is_consignment=1;
                }

            }
            $deal_order['deal_ids'] = implode(',',$deal_ids);
            $deal_order['delivery_status'] = $is_consignment==0?5:0;
            $deal_order['ecv_id'] = 0;
            $deal_order['ecv_money'] = 0;
            $deal_order['account_money'] = 0;
            $deal_order['payment_id'] = 0;
            $deal_order['payment_fee'] = 0;
            $deal_order['deal_total_price'] = $deal_total_price;

            /*$tempIndex++;
            if ($tempIndex == $tempOrderCount) {
                $deal_order['discount_price'] = $discount_price_main - $tempDiscount;
            } else {
                $deal_order['discount_price'] =  round($discount_price_main * $deal_total_price / $deal_total_price_main, 2);
                $tempDiscount += $deal_order['discount_price'];
            }*/
            $deal_order['discount_price'] = $deal_total_price - $total_price;


            $deal_order['delivery_fee'] = $supplier_data[$k]['delivery_fee'];
            $deal_order['record_delivery_fee'] =  $deal_order['delivery_fee'];
            $deal_order['memo'] =  $supplier_data[$k]['memo'];
            //log_result('memo');log_result($supplier_data[$k]['memo']);log_result($k);
            $deal_order['agency_id'] =  $supplier_data[$k]['agency_id'];
            $deal_order['youhui_log_id'] =  $supplier_data[$k]['youhui_data']['youhui_log_id'];
            $deal_order['youhui_money'] =  $supplier_data[$k]['youhui_data']['youhui_money'];

            // 补充一个发票
            $deal_order['invoice_info'] = $supplier_data[$k]['invoice_info'];
            //积分抵现拆分

            //商家的优惠券足可抵扣该子单的所有费用时，该商家的子单就不参与积分抵现拆分
            if($supplier_data[$k]['youhui_data']['total_price'] > $supplier_data[$k]['youhui_data']['youhui_money']){
                $rate = ($deal_order['deal_total_price'] + $deal_order['delivery_fee'] - $deal_order['discount_price']) / ($main_order['total_price'] - $main_order['payment_fee']+$main_order['exchange_money'] - $no_total_price); //主单和子单的比率
                if(($tempIndex+1)==$tempOrderCount){

                    $deal_order['exchange_money']=$main_order['exchange_money']-$tempExchangeMoney;
                    $son_score_purchase=$main_score_purchase;
                    $son_score_purchase['user_use_score']=$main_score_purchase['user_use_score']-$tempUserUseScore;
                    $son_score_purchase['exchange_money']=$main_order['exchange_money']-$tempExchangeMoney;
                    $deal_order['score_purchase']=serialize($son_score_purchase);
                }else{
                    $deal_order['exchange_money'] = round(($main_order['exchange_money'] * $rate), 2);
                    $tempExchangeMoney+=$deal_order['exchange_money'];
                    $son_score_purchase=$main_score_purchase;
                    $son_score_purchase['user_use_score']=round($main_score_purchase['user_use_score'] * $rate, 0);
                    $tempUserUseScore+=$son_score_purchase['user_use_score'];
                    $son_score_purchase['exchange_money']=round($main_order['exchange_money'] * $rate, 2);
                    $deal_order['score_purchase']=serialize($son_score_purchase);
                }
            }else{
                $deal_order['score_purchase']='';
                $deal_order['exchange_money'] = 0;
            }

            $deal_order['total_price'] = $deal_order['deal_total_price'] + $deal_order['delivery_fee'] - $deal_order['discount_price']-$deal_order['exchange_money'];

            $deal_order['pay_amount'] = 0;
            $deal_order['promote_description'] = '';
            $deal_order['promote_arr'] = '';
            $deal_order['return_total_score'] = $return_data[$k]['return_total_score'];
            $deal_order['return_total_money'] = $return_data[$k]['return_total_money'];
            $deal_order['deal_order_item'] = serialize($v);
            $deal_order['supplier_data'] = '';
            $deal_order['order_id'] = $order_id;
            if(($deal_order['type']==5||$deal_order['type']==6)&&defined("FX_LEVEL")){
                $ref_salary_conf = unserialize(app_conf("REF_SALARY"));
                $ref_salary_switch=intval($ref_salary_conf['ref_salary_switch']);
                if($ref_salary_switch==1){//判断后台是否开启推荐商家入驻三级分销
                    $deal_order['is_participate_ref_salary'] = 1;
                }
            }

            //消费送积分
            if($deal_order['is_consumer_send_score']==1){
                $order_cs_score_data=unserialize($deal_order['consumer_send_score_data']);
                $actual_price=$deal_order['total_price']-$deal_order['youhui_money'];
                $actual_price=$actual_price>0?$actual_price:0;
                if($actual_price>0&&$order_cs_score_data['send_integral_proportion']>0){
                    $consumer_send_score = intval($actual_price)*$order_cs_score_data['send_integral_proportion'];
                    $consumer_send_score_data=array(
                        'send_integral_proportion'=>$order_cs_score_data['send_integral_proportion'],
                        'done_pay_price'=>$deal_order['total_price'],
                        'done_pay_price_send_score'=>$consumer_send_score,
                    );
                    $deal_order['consumer_send_score']=$consumer_send_score;
                    $deal_order['consumer_send_score_data']=unserialize($consumer_send_score_data);
                }else{
                    $deal_order['is_consumer_send_score']=0;
                    $deal_order['consumer_send_score']=0;
                    $deal_order['consumer_send_score_data']=unserialize(array());
                }
            }

            do
            {
                $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order",$deal_order); //插入
                $order_sub_id = intval($GLOBALS['db']->insert_id());

            }while($order_sub_id==0);
            $order_sub_arr[] = $order_sub_id;
            $order_temp = array();
            $order_temp['order_sn'] = $deal_order['order_sn'];
            $order_temp['order_id'] = $order_sub_id;
            $order_sub_data[$k] = $order_temp;
            $tempIndex++;

        }

        //生成子单的订单商品

        foreach($deal_order_item as $k=>$v){
            unset($v['id']);

            if($v['supplier_id']==0){
                if($v['delivery_type']==1){  //物流配送
                    $v['order_id'] = $order_sub_data['p_wl']['order_id'];
                    $v['order_sn'] = $order_sub_data['p_wl']['order_sn'];
                }elseif($v['delivery_type']==3){  //驿站配送
                    $v['order_id'] = $order_sub_data['p_yz']['order_id'];
                    $v['order_sn'] = $order_sub_data['p_yz']['order_sn'];
                }
            }else{
                $v['order_id'] = $order_sub_data[$v['supplier_id']]['order_id'];
                $v['order_sn'] = $order_sub_data[$v['supplier_id']]['order_sn'];
            }


            $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order_item",$v); //插入

        }

        foreach($order_sub_arr as $k=>$order_id){
            $this->update_order_cache($order_id);
        }

    }

    /**
     * 重新缓存订单的缓存，订单商品
     * @param unknown_type $order_id
     */
    function update_order_cache($order_id)
    {
        $order_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);
        if($order_info)
        {
            $order_info['deal_order_item'] = serialize($GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order_item where order_id = ".$order_id));
            $order_is=$this->get_order_status_is($order_info,unserialize($order_info['deal_order_item']));
            $order_info['order_process_status']=$order_is['status'];
            $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order",$order_info,'UPDATE','id='.$order_id,'SILENT');
        }
    }


    /**
     * 获得订单状态
     * $order_info：订单信息
     * 输出
     * order_status:0,待付款1,待发货2,待确认3,待评价4,已取消5,已完成6,退款中7,已删除8
     * Array
    (
    [status] => 1             订单状态:待付款1,待发货2,待确认3,待评价4,已取消5,已完成6,退款中7,已删除8
    [is_dp]=>0                待评价商品，0：不存在，1：存在
    [is_do_delivery]=>0       待发货商品，0：不存在，1：存在
    [is_do_verify]=>0		  待收货商品，0：不存在，1：存在
    [is_use_coupon]=>0		  待验证的消费券，0：不存在，1：存在
    [is_return_item]=>0		  退款中的商品，0：不存在，1：存在
    )
     */
    function get_order_status_is($order_info,$deal_order_item)
    {

        $is_dp=0;//待评价商品，0：不存在，1：存在             	订单有效
        $is_do_delivery=0;//待发货商品，0：不存在，1：存在    	订单有效
        $is_do_verify=0;//待收货商品，0：不存在，1：存在      	订单有效
        $is_use_coupon=0;//待验证的消费券，0：不存在，1：存在 	订单有效
        $is_return_item=0;//退款中的商品，0：不存在，1：存在 	订单有效
        if($order_info['pay_status']==2){
            foreach ($deal_order_item as $k=>$v){ //遍历订单商品
                if($v['is_shop']==1){//商城商品 无需配送or物流配送or驿站配送or自提商品
                    if($v['is_coupon']==1){//自提商品
                        $coupon=$GLOBALS['db']->getRow("select id,deal_type,refund_status,confirm_time from " . DB_PREFIX . "deal_coupon where order_deal_id=".$v['id']);
                        if($coupon['confirm_time']>0&&$v['dp_id']==0){//已验证，未评价
                            $is_dp=1;
                        }
                        if($coupon['confirm_time']==0&&$coupon['refund_status']!=1&&$coupon['refund_status']!=2){
                            //未验证and不是退款中and不是已退款
                            $is_use_coupon=1;
                        }
                        if($coupon['confirm_time']==0&&$coupon['refund_status']==1){//未验证and退款中
                            $is_return_item=1;//退款中
                        }
                    }else{ //无需配送or物流配送or驿站配送
                        if($v['is_balance']>0){//已结算
                            if($v['dp_id']==0){//未评价
                                $is_dp=1;
                            }
                        }else{//未结算
                            if($v['delivery_status']==0&&$v['refund_status']!=1&&$v['refund_status']!=2){//未发货and不是退款中and不是已退款
                                $is_do_delivery=1;//存在待发货的商品
                            }
                            if($v['delivery_status']==1&&$v['is_arrival']==0&&$v['refund_status']!=1&&$v['refund_status']!=2){
                                //已发货and未收货and不是退款中and不是已退款
                                $is_do_verify=1;
                            }
                            if($v['refund_status']==1){//退款中
                                $is_return_item=1;//退款中
                            }
                        }
                    }
                }else{
                    $is_confirm=0;//已使用的团购券，0：不存在，1：存在，2，没有券   当前商品有效
                    $is_tuan_end=0;//团购流程是否结束，0：未结束，1：结束           当前商品有效
                    $is_available_coupon=0;//存在可使用的券，0：不存在，1：存在     当前商品有效
                    if($v['is_coupon']==0){//不发券的团购
                        $is_confirm=2;//没有券
                        $is_tuan_end=1;//结束
                    }else{//发券的团购
                        $coupon_arr = $GLOBALS['db']->getAll("select id,end_time,is_valid,confirm_time,is_balance,refund_status from " . DB_PREFIX . "deal_coupon where order_deal_id = " . $v['id']);
                        //print_r($coupon_arr);
                        if(count($coupon_arr)==1){//一张券的情况
                            if($coupon_arr['0']['confirm_time']>0){//已使用
                                $is_confirm=1;//存在已使用的团购券
                                $is_tuan_end=1;//结束
                            }
                            if($coupon_arr['0']['refund_status']==2){//已退款
                                //$is_confirm=0;//存在已使用的团购券
                                $is_tuan_end=1;//结束
                            }
                            if($coupon_arr['0']['confirm_time']==0&&$coupon_arr['0']['refund_status']==1){//券退款中
                                //$is_tuan_end=0;//未结束
                                $is_return_item=1;//退款中

                            }
                            if($coupon_arr['0']['confirm_time']==0&&$coupon_arr['0']['refund_status']!=1&&$coupon_arr['0']['refund_status']!=2&&(($coupon_arr['0']['end_time']==0||$coupon_arr['0']['end_time']>=NOW_TIME)||($coupon_arr['0']['end_time']!=0&&NOW_TIME>$coupon_arr['0']['end_time']&&$coupon_arr['0']['is_valid']!=2))){//未使用and(未过期or(已过期and退款未禁用))=可验证的券
                                $is_available_coupon=1;//存在可使用的券
                                //$is_tuan_end=0;//未结束
                            }
                        }elseif(count($coupon_arr)>1){//多张券的情况
                            $is_tuan_end=1;
                            foreach ($coupon_arr as $kk=>$vv){//遍历该团购的团购券 券状态：待使用，退款中，待评价，已完成，已退款
                                if($vv['confirm_time']>0){//已使用
                                    $is_confirm=1;//存在已使用的团购券
                                }
                                if($vv['confirm_time']==0&&$vv['refund_status']!=1&&$vv['refund_status']!=2&&(($vv['end_time']==0||$vv['end_time']>=NOW_TIME)||($vv['end_time']!=0&&NOW_TIME>$vv['end_time']&&$vv['is_valid']!=2))){//未使用and(未过期or(已过期and退款未禁用))=可验证的券
                                    $is_available_coupon=1;//存在可使用的券
                                    $is_tuan_end=0;//团购流程未结束
                                }
                                if($vv['refund_status']==1){//券退款中
                                    $is_tuan_end=0;//团购流程未结束
                                    $is_return_item=1;//退款中
                                }
                            }
                        }else{ //券不存在
                            //从未发生
                        }

                        if($is_available_coupon==1){
                            $is_use_coupon=1;
                        }
                    }
                    if($is_tuan_end==1&&($is_confirm==1||$is_confirm==2)&&$v['dp_id']==0){//团购流程结束and (不发券的or至少使用了一张)and未评估
                        $is_dp=1;
                    }
                }
            }
        }
        $status=0;
        if($order_info['pay_status']!=2){ //下单未支付
            $status=1;//待付款
            if($order_info['is_delete'] == 1&&$order_info['is_cancel']=1){ //会员执行取消订单
                $status=5;//已取消
            }elseif($order_info['is_delete'] == 1&&$order_info['is_cancel']=0){//会员取消完，执行删除订单，或者后台关闭交易
                $status=8;  //已删除
            }
        }else{//下单付款完成
            if($order_info['order_status']==1){  //订单所有商品流程结束(退款or已使用or过期无法退款)，订单结单
                if($is_dp==1){
                    $status=4;//待评价
                }else{
                    $status=6;//已完成
                }
            }else{
                if($is_do_delivery==1){//存在待发货的商品
                    $status=2;//待发货
                }elseif($is_do_delivery==0&&($is_do_verify==1||$is_use_coupon==1)){//不存在待发货and存在待收货的商品and存在待验证的券
                    $status=3;//待确认
                }elseif($is_do_delivery==0&&$is_do_verify==0&&$is_use_coupon==0&&$is_dp==1){//不存在待发货and存在待收货的商品and存在待验证的券
                    $status=4;//待评价
                }
                if($is_do_delivery==0&&$is_do_verify==0&&$is_use_coupon==0&&$is_dp==0&&$is_return_item==1){
                    $status=7;//退款中
                }

            }

        }
        $data=array();
        $data['status']=$status;//订单状态
        $data['is_dp']=$is_dp;//是否存在待评价商品
        $data['is_do_delivery']=$is_do_delivery;//是否存在待发货商品
        $data['is_do_verify']=$is_do_verify;//是否存在待收货商品
        $data['is_use_coupon']=$is_use_coupon;//是否存在待验证券
        $data['is_return_item']=$is_return_item;//是否存在退款中的商品
        return $data;
    }


    /**
     * 订单分片,用户散列订单表，商户散列订单商品表
     * @param unknown_type $order_id
     */
    function distribute_order($order_id)
    {
        if(0&&$GLOBALS['distribution_cfg']['ORDER_DISTRIBUTE_COUNT']>1)
        {
            //定义建表sql
            $rs = $GLOBALS['db']->getRow("show create table ".DB_PREFIX."deal_order");
            $order_sql = $rs['Create Table'];
            $order_sql = preg_replace("/create table/i", "create table if not exists ", $order_sql);
            $rs_item = $GLOBALS['db']->getRow("show create table ".DB_PREFIX."deal_order_item");
            $order_item_sql = $rs_item['Create Table'];
            $order_item_sql = preg_replace("/create table/i", "create table if not exists ", $order_item_sql);

            //散列订单
            $order_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);
            $order_end = hash_table($order_info['user_id'], $GLOBALS['distribution_cfg']['ORDER_DISTRIBUTE_COUNT']); //通过订单用户ID的散列后缀
            $order_table_name = DB_PREFIX."deal_order_u_".$order_end; //散列订单表
            $sql = preg_replace("/".DB_PREFIX."deal_order/", $order_table_name, $order_sql);
            $GLOBALS['db']->query($sql); //创建散列表
            $GLOBALS['db']->query("delete from ".$order_table_name." where id = ".$order_id);
            $GLOBALS['db']->autoExecute($order_table_name,$order_info);

            //开始散列订单商品
            $order_items = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order_item where order_id = ".$order_id);
            foreach($order_items as $k=>$item)
            {
                $order_end = hash_table($item['supplier_id'], $GLOBALS['distribution_cfg']['ORDER_DISTRIBUTE_COUNT']); //通过订单用户ID的散列后缀
                $order_table_name = DB_PREFIX."deal_order_s_".$order_end; //散列订单表
                $sql = preg_replace("/".DB_PREFIX."deal_order/", $order_table_name, $order_sql);
                $GLOBALS['db']->query($sql); //创建散列表
                $GLOBALS['db']->query("delete from ".$order_table_name." where id = ".$order_id);
                $GLOBALS['db']->autoExecute($order_table_name,$order_info);

                $order_item_end = hash_table($item['supplier_id'], $GLOBALS['distribution_cfg']['ORDER_DISTRIBUTE_COUNT']); //通过订单商品商家ID的散列后缀
                $order_item_table_name = DB_PREFIX."deal_order_item_s_".$order_item_end; //散列订单商品表
                $sql = preg_replace("/".DB_PREFIX."deal_order_item/", $order_item_table_name, $order_item_sql);
                $GLOBALS['db']->query($sql); //创建散列表
                $GLOBALS['db']->query("delete from ".$order_item_table_name." where id = ".$item['id']);
                $GLOBALS['db']->autoExecute($order_item_table_name,$item);

                $order_item_end = hash_table($order_info['user_id'], $GLOBALS['distribution_cfg']['ORDER_DISTRIBUTE_COUNT']); //通过订单商品用户ID的散列后缀
                $order_item_table_name = DB_PREFIX."deal_order_item_u_".$order_item_end; //散列订单商品表
                $sql = preg_replace("/".DB_PREFIX."deal_order_item/", $order_item_table_name, $order_item_sql);
                $GLOBALS['db']->query($sql); //创建散列表
                $GLOBALS['db']->query("delete from ".$order_item_table_name." where id = ".$item['id']);
                $GLOBALS['db']->autoExecute($order_item_table_name,$item);
            }
        }
    }


    /**
     * 验证购物车
     */
    function check_cart1($id,$number)
    {

        $cart_data = $GLOBALS['db']->getRow("select deal_id,id from ".DB_PREFIX."deal_cart where id=".$id);

        $cart_result = $this->load_cart_list($cart_data['deal_id']);
        $cart_item = $cart_result['cart_list'][$id];

        if(empty($cart_item))
        {
            $result['info'] = "非法的数据";
            $result['status'] = 0;
            return $result;
        }
        if($number<=0)
        {
            $result['info'] = "数量不能为0";
            $result['status'] = 0;
            return $result;
        }
        $add_number = $number - $cart_item['number'];

        //属性库存的验证

        $attr_setting_str = '';
        if($cart_item['attr']!='')
        {
            $attr_setting_str = $cart_item['attr_str'];
        }
        if($attr_setting_str!='')
        {
            $check = $this->check_deal_number_attr($cart_item['deal_id'],$attr_setting_str,$add_number);
            if($check['status']==0)
            {
                $result['info'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];
                $result['status'] = 0;
                return $result;
            }
        }

        //属性库存的验证
        $check = $this->check_deal_number($cart_item['deal_id'],$add_number,true);
        if($check['status']==0)
        {
            $result['info'] = $check['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$check['data']];
            $result['status'] = 0;
            return $result;
        }

        //验证时间
        /*$checker = $this->check_deal_time($cart_item['deal_id']);
        if($checker['status']==0)
        {
            $result['info'] = $checker['info']." ".$GLOBALS['lang']['DEAL_ERROR_'.$checker['data']];
            $result['status'] = 0;
            return $result;
        }*/
        //验证时间


        $result['status'] = 1;
        return $result;
    }

    //同步订单支付状态
    function order_paid($order_id)
    {
        $order_id  = intval($order_id);
        $order = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);
        if($order['is_presell_order']==1){
            if($order['pay_amount'] >= $order['presell_deposit_money'] && $order['pay_amount'] < $order['total_price'])
            {
                $result = true; //预付款已支付
                $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set pay_status = 1 where id =".$order_id." and pay_status <> 1");
                //增加商品预售数量
                $this->syn_presell_deal_buy_count($order_id);  //订单未支付成功

                /*if(intval(app_conf("PRESELL_SEND_MESSAGE"))==1){
                    fanwe_require(APP_ROOT_PATH.'system/model/Schedule.php');
                    $schedule = new Schedule();
                    $name_array=$GLOBALS['db']->getRow("select user_name,name from (select user_name from ".DB_PREFIX."user where id={$order['user_id']}) User,(select name from ".DB_PREFIX."deal_order_item where order_id={$order['id']}) DealOrderItem");
                    $schedule->send_schedule_plan("sms","消息任务「预售开始付尾款手机短信通知」",array("type"=>0,"dest"=>$order['mobile'],"content"=>"{$name_array['user_name']}您好，您预订的{$name_array['name']}可以开始支付尾款了，请在7天内前往支付尾款"),$order['presell_end_time'],"{$name_array['user_name']}手机:{$order['mobile']}");
                    $schedule->send_schedule_plan("sms","消息任务「预售开始付尾款站内信通知」",array("type"=>1,"dest"=>$order['user_id'],"content"=>"{$name_array['user_name']}您好，您预订的{$name_array['name']}可以开始支付尾款了，请在7天内前往支付尾款"),$order['presell_end_time'],"{$name_array['user_name']}用户ID:{$order['user_id']}");
                    $schedule->send_schedule_plan("sms","消息任务「预售付尾款将结束手机短信通知」",array("type"=>0,"dest"=>$order['mobile'],"content"=>"{$name_array['user_name']}您好，您预订的{$name_array['name']}将在30分钟后结束，请及时付完尾款"),$order['presell_end_time']+7*24*60*60-30*60,"{$name_array['user_name']}手机:{$order['mobile']}");
                }*/
            }elseif($order['pay_amount'] >=$order['total_price']){
                $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set pay_status = 2 where id =".$order_id." and pay_status <> 2");
                $rs = $GLOBALS['db']->affected_rows();
                if($rs)
                {
                    //支付完成
                    $this->order_paid_done($order_id);
                    $order = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);
                    //fanwe_require(APP_ROOT_PATH.'system/model/AppPush.php');
                    //AppPush::new_order($order);

                    /*if (isOpenFeierPrinter()) {
                        $order_type_num = getPaidOrderNum($order);
                        $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set order_type_num = ".$order_type_num.' where id='.$order['id']);
                    }*/

                    if($order['pay_status']==2&&$order['after_sale']==0)
                    {
                        //require_once(APP_ROOT_PATH."system/model/deal_order.php");
                        $this->distribute_order($order_id);
                        $result = true;
                    }
                    else
                        $result = false;
                }
            }else{
                $result = false;
            }
        }else{

            if($order['pay_amount'] + $order['youhui_money'] >=$order['total_price'])
            {
                $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set pay_status = 2 , pay_amount = pay_amount + youhui_money where id =".$order_id." and pay_status <> 2");
                $rs = $GLOBALS['db']->affected_rows();
                if($rs)
                {

                    //支付完成
                    $this->order_paid_done($order_id);
                    $order = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);
                    //fanwe_require(APP_ROOT_PATH.'system/model/AppPush.php');
                    //AppPush::new_order($order);
                    if($order['pay_status']==2&&$order['after_sale']==0)
                    {
                        //require_once(APP_ROOT_PATH."system/model/deal_order.php");
                        $this->distribute_order($order_id);
                        $result = true;
                    }
                    else
                        $result = false;
                }
            }
            elseif($order['pay_amount']<$order['total_price']&&$order['pay_amount']!=0)
            {
                //by hc 0507
                $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set pay_status = 0 where id =".$order_id);
                $result = false;  //订单未支付成功
            }
            elseif($order['pay_amount']==0)
            {
                $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set pay_status = 0 where id =".$order_id);
                $result = false;  //订单未支付成功
            }
        }
        return $result;
    }

    function syn_presell_deal_buy_count($order_id){

        $stock_status = true;

        $order_goods_list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order_item where order_id = ".$order_id);
        foreach($order_goods_list as $k=>$v)
        {
            $attr_stock = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."attr_stock where deal_id = ".$v['deal_id']." and locate(attr_str,'".$v['attr_str']."')");
            if($attr_stock)
            {
                if($attr_stock['stock_cfg']>=0)
                {
                    $sql = "update ".DB_PREFIX."attr_stock set presell_buy_count = presell_buy_count + ".$v['number']." , buy_count = buy_count + ".$v['number'].",stock_cfg = stock_cfg - ".$v['number']." where deal_id = ".$v['deal_id'].
                        " and (stock_cfg - ".$v['number']." >= 0)".
                        " and locate(attr_str,'".$v['attr_str']."') > 0 ";
                }
                else
                {
                    $sql = "update ".DB_PREFIX."attr_stock set presell_buy_count = presell_buy_count + ".$v['number']." , buy_count = buy_count + ".$v['number']." where deal_id = ".$v['deal_id'].
                        " and locate(attr_str,'".$v['attr_str']."') > 0 ";
                }
                $GLOBALS['db']->query($sql); //增加商品的发货量
                $rs = $GLOBALS['db']->affected_rows();

                if($rs)
                {
                    $affect_attr_list[] = $v;
                }
                else
                {

                    $stock_status = false;
                    break;
                }
            }
        }

        if($stock_status)
        {
            $goods_list = $GLOBALS['db']->getAll("select buy_type,deal_id,sum(number) as num,sum(add_balance_price_total) as add_balance_price_total,sum(balance_total_price) as balance_total_price,sum(return_total_money) as return_total_money,sum(return_total_score) as return_total_score,min(is_pick) as is_pick,total_price from ".DB_PREFIX."deal_order_item where order_id = ".$order_id." group by deal_id");
            foreach($goods_list as $k=>$v)
            {
                $deal_stock = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_stock where deal_id = '".$v['deal_id']."'");
                if($deal_stock['stock_cfg']>=0)
                    $sql = "update ".DB_PREFIX."deal_stock set buy_count = buy_count + ".$v['num'].
                        ",stock_cfg = stock_cfg - ".$v['num']." where deal_id=".$v['deal_id'].
                        " and (stock_cfg - ".$v['num'].">= 0) and time_status <> 2";
                else
                    $sql = " update ".DB_PREFIX."deal_stock set buy_count = buy_count + ".$v['num'].
                        " where deal_id=".$v['deal_id']." and time_status <> 2";

                $GLOBALS['db']->query($sql); //增加商品的发货量
                $rs = $GLOBALS['db']->affected_rows();

                if($rs)
                {
                    $affect_list[] = $v;  //记录下更新成功的团购商品，用于回滚
                }
                else
                {
                    //失败成功，即过期支付，超量支付
                    $stock_status = false;
                    break;
                }
            }
        }

        if($stock_status)
        {
            foreach($goods_list as $k=>$v)
            {
                $deal_info =$GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = '".$v['deal_id']."' and is_delete = 0 or (uname= '".$v['deal_id']."' and uname <> '')"); //load_auto_cache("deal",array("id"=>$v['deal_id']));
                //统计商户销售额

                if($deal_info['max_bought']>=0)
                    $sql = "update ".DB_PREFIX."deal set presell_buy_count = presell_buy_count + ".$v['num']." , buy_count = buy_count + ".$v['num'].
                        ",user_count = user_count + 1,max_bought = max_bought - ".$v['num']." where id=".$v['deal_id'];
                else
                    $sql = "update ".DB_PREFIX."deal set presell_buy_count = presell_buy_count + ".$v['num']." , buy_count = buy_count + ".$v['num'].
                        ",user_count = user_count + 1 where id=".$v['deal_id'];
                $GLOBALS['db']->query($sql);

            }
        }
    }

    //订单付款完毕后执行的操作,充值单也在这处理，未实现
    function order_paid_done($order_id)
    {
        //处理支付成功后的操作
        /**
         * 1. 发货
         * 2. 超量发货的存到会员中心
         * 3. 发券
         * 4. 发放抽奖
         */

        $order_id = intval($order_id);
        $stock_status = true;  //团购状态
        $order_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);
        $payment_notice = $GLOBALS['db']->getRow("select pn.id,p.class_name from ".DB_PREFIX."payment_notice as pn left join ".DB_PREFIX."payment AS p on pn.payment_id=p.id where order_id = ".$order_info['id']." and payment_id = ".$order_info['payment_id']." and is_paid=1 and money>0");
        //积分充值订单
        if($order_info["type"]==7){
            require_once(APP_ROOT_PATH."system/libs/user.php");
            $money = $order_info['total_price'] - $order_info['payment_fee'];
            modify_account(array('score'=>$order_info['return_total_score']),$order_info['user_id'],'充值'.format_price($money).'成功，可用积分增加'.$order_info['return_total_score']);
            modify_account(array('frozen_score'=>$order_info['frozen_score']),$order_info['user_id'],'充值'.format_price($money).'成功，冻结积分增加'.$order_info['frozen_score']);
            //报表
            //modify_statements($order_info['total_price'], 0, $order_info['order_sn']."会员充值积分订单，含手续费"); //总收入
            $msg_content = '您已成功充值'.format_price($money).', 积分增加'.$order_info['return_score'].",冻结积分增加".$order_info['frozen_score'];
            //send_msg_new($order_info['user_id'], $msg_content, 'account', array('type' => 4, 'data_id' => $order_id));
            $this->auto_over_status($order_id);
        }
        else if($order_info['type'] != 1)
        {
            $GLOBALS['db']->query("START TRANSACTION");

            if($order_info['is_presell_order'] == 0)
            {   //预售订单，预售订单中付订金时，就已经加销量减库存了，付尾款完，不用重复
                //首先验证所有的规格库存
                $order_goods_list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order_item where order_id = ".$order_id);
                foreach($order_goods_list as $k=>$v)
                {
                    $attr_stock = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."attr_stock where deal_id = ".$v['deal_id']." and locate(attr_str,'".$v['attr_str']."')");

                    if($attr_stock)
                    {
                        $pt_buy_count_field = '';
                        if($order_info['is_pt_order']){
                            $pt_buy_count_field = ', pt_buy_count = pt_buy_count+'.$v['number'];
                        }

                        if($attr_stock['stock_cfg']>=0)
                        {
                            $sql = "update ".DB_PREFIX."attr_stock set 
    							buy_count = buy_count + ".$v['number'].",
    							stock_cfg = stock_cfg - ".$v['number'].$pt_buy_count_field." 
    							where deal_id = ".$v['deal_id'].
                                " and (stock_cfg - ".$v['number']." >= 0)".
                                " and locate(attr_str,'".$v['attr_str']."') > 0 ";
                        }
                        else
                        {
                            $sql = "update ".DB_PREFIX."attr_stock set 
    							buy_count = buy_count + ".$v['number'].$pt_buy_count_field." 
    							where deal_id = ".$v['deal_id']." and locate(attr_str,'".$v['attr_str']."') > 0 ";
                        }

                        $GLOBALS['db']->query($sql); //增加商品的发货量
                        $rs = $GLOBALS['db']->affected_rows();

                        if($rs)
                        {
                            $affect_attr_list[] = $v;
                        }
                        else
                        {

                            $stock_status = false;
                            break;
                        }
                    }
                }

            }
            $return_money = 0; //非发券非配送的即时返还
            $return_score = 0; //非发券非配送的即时返还
            $use_score = 0;  //积分商品所耗费的积分
            if($stock_status)
            {
                $GLOBALS['db']->query("COMMIT");
                $goods_list = $GLOBALS['db']->getAll("select buy_type,deal_id,sum(number) as num,sum(add_balance_price_total) as add_balance_price_total,sum(balance_total_price) as balance_total_price,sum(return_total_money) as return_total_money,sum(return_total_score) as return_total_score,min(is_pick) as is_pick,total_price from ".DB_PREFIX."deal_order_item where order_id = ".$order_id." group by deal_id");

                foreach($goods_list as $k=>$v)
                {
                    //如果是拼团商品
                    if($order_info['is_pt_order']){
                        $pt_buy_count_field = '';
                        $pt_buy_count_field = ', pt_buy_count = pt_buy_count+'.$v['num'];
                    }

                    $deal_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal where id = ".intval($v['deal_id']));
                    //$deal_info = load_auto_cache("deal",array("id"=>$v['deal_id']));
                    //统计商户销售额
                    if($order_info['is_presell_order'] == 0){//预售订单，预售订单中付订金时，就已经加销量减库存了，付尾款完，不用重复
                        if($deal_info['max_bought']>=0)
                            $sql = "update ".DB_PREFIX."deal set 
     								buy_count = buy_count + ".$v['num'].",
     								user_count = user_count + 1,
     								max_bought = max_bought - ".$v['num'].$pt_buy_count_field." 
     								where id=".$v['deal_id'];
                        else
                            $sql = "update ".DB_PREFIX."deal set 
    								buy_count = buy_count + ".$v['num'].",
    								user_count = user_count + 1 ".$pt_buy_count_field." 
    								where id=".$v['deal_id'];
                        $GLOBALS['db']->query($sql);
                    }
                    if($payment_notice['class_name']=='Cod'||$order_info['cod_money']>0){
                        //货到付款，目前不产生商户报表和平台报表，不结算金额给商家
                    }else{
                        //变更商家账户资金
                        $supplier_log =  "ID:".$deal_info['id']." ".$deal_info['sub_name']." 订单：".$order_info['order_sn'];
                        //modify_supplier_account($v['total_price'], $deal_info['supplier_id'], 0,$supplier_log);
                        //modify_supplier_account($v['balance_total_price']+$v['add_balance_price_total'], $deal_info['supplier_id'], 1, $supplier_log);  //冻结资金
                    }


                    //不发券的实时返还
                    if($deal_info['is_coupon']==0&&$deal_info['is_shop']==0&&$v['buy_type']==0)
                    {
                        $return_money+=$v['return_total_money'];
                        $return_score+=$v['return_total_score'];
                    }
                    if($v['buy_type']==1)
                    {
                        $use_score+=$v['return_total_score'];
                    }
                    $balance_price+=$v['balance_total_price'];
                    $add_balance_price+=$v['add_balance_price_total'];

                    //发券
                    if($deal_info['is_coupon'] == 1||$v['is_pick']==1)
                    {
                        if($deal_info['deal_type'] == 1||$v['is_pick']==1) //按单发券
                        {
                            $deal_order_item_list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order_item where order_id = ".$order_info['id']." and deal_id = ".$v['deal_id']);
                            foreach($deal_order_item_list as $item)
                            {
                                //							for($i=0;$i<$item['number'];$i++) //按单
                                //							{
                                //需要发券
                                /**
                                 * 1. 先从已有消费券中发送
                                 * 2. 无有未发送的券，自动发送
                                 * 3. 发送状态的is_valid 都是 0, 该状态的激活在syn_deal_status中处理
                                 */
                                /*修正后台手动建团购券，购买的时候按单发送团购券，数量不一致*/
                                $sql = "update ".DB_PREFIX."deal_coupon set user_id=".$order_info['user_id'].
                                    ",order_id = ".$order_info['id'].
                                    ",order_deal_id = ".$item['id'].
                                    ",expire_refund = ".$deal_info['expire_refund'].
                                    ",any_refund = ".$deal_info['any_refund'].
                                    ",coupon_price = ".$item['total_price'].
                                    ",coupon_score = ".$item['return_total_score'].
                                    ",coupon_money = ".$item['return_total_money'].
                                    ",add_balance_price = ".$item['add_balance_price_total'].
                                    ",deal_type = ".$deal_info['deal_type'].
                                    ",balance_price = ".$item['balance_total_price'].
                                    " where deal_id = ".$v['deal_id'].
                                    " and user_id = 0 ".
                                    " and is_delete = 0 order by id ASC limit 1";
                                $GLOBALS['db']->query($sql);
                                $exist_coupon = $GLOBALS['db']->affected_rows();
                                if(!$exist_coupon)
                                {
                                    //未发送成功，即无可发放的预设消费券
                                    //add_coupon($v['deal_id'],$order_info['user_id'],0,'','',0,0,$item['id'],$order_info['id']);
                                }
                                //							}
                            }
                        }
                        else
                        {
                            $deal_order_item_list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order_item where order_id = ".$order_info['id']." and deal_id = ".$v['deal_id']);
                            foreach($deal_order_item_list as $item)
                            {
                                for($i=0;$i<$item['number'];$i++) //按件
                                {
                                    //需要发券
                                    /**
                                     * 1. 先从已有消费券中发送
                                     * 2. 无有未发送的券，自动发送
                                     * 3. 发送状态的is_valid 都是 0, 该状态的激活在syn_deal_status中处理
                                     */
                                    $sql = "update ".DB_PREFIX."deal_coupon set user_id=".$order_info['user_id'].
                                        ",order_id = ".$order_info['id'].
                                        ",order_deal_id = ".$item['id'].
                                        ",expire_refund = ".$deal_info['expire_refund'].
                                        ",any_refund = ".$deal_info['any_refund'].
                                        ",coupon_price = ".$item['unit_price'].
                                        ",coupon_score = ".$item['return_score'].
                                        ",coupon_money = ".$item['return_money'].
                                        ",add_balance_price = ".$item['add_balance_price'].
                                        ",deal_type = ".$deal_info['deal_type'].
                                        ",balance_price = ".$item['balance_unit_price'].
                                        " where deal_id = ".$v['deal_id'].
                                        " and user_id = 0 ".
                                        " and is_delete = 0 limit 1";
                                    $GLOBALS['db']->query($sql);
                                    $exist_coupon = $GLOBALS['db']->affected_rows();
                                    if(!$exist_coupon)
                                    {
                                        //未发送成功，即无可发放的预设消费券
                                        //add_coupon($v['deal_id'],$order_info['user_id'],0,'','',0,0,$item['id'],$order_info['id']);
                                    }
                                }
                            }
                        }//发券结束
                    } elseif ($deal_info['is_coupon'] == 0 && $deal_info['is_shop'] == 0) {
                        // 不发券的团购商品直接结算
                        //$order_item_id = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."deal_order_item where order_id = ".$order_info['id']);

                        //auto_balance_coupon($order_item_id);
                    }
                }
                //开始处理返还的积分或现金,此处返还不用发货不用配送不用发券的产品返还
                //require_once(APP_ROOT_PATH."system/model/user.php");
                if($return_money!=0)
                {
                    $msg = sprintf('%s订单返现金',$order_info['order_sn']);
                    modify_account(array('money'=>$return_money,'score'=>0),$order_info['user_id'],$msg);
                }

                if($return_score < 0)   //积分兑换，积分变动提醒
                {
                    $msg = sprintf('%s订单返积分',$order_info['order_sn']);
                    modify_account(array('money'=>0,'score'=>$return_score),$order_info['user_id'],$msg);
                    //send_score_sms($order_info['id']);
                    //send_score_mail($order_info['id']);
                }else if($return_score>0){
                    $msg = sprintf('%s订单返积分',$order_info['order_sn']);
                    modify_account(array('money'=>0,'score'=>$return_score),$order_info['user_id'],$msg);
                }



                //判断订单状态
                if($order_info['is_pt_order']){
                    //支付成功后，开团或者参团
                    fanwe_require(APP_ROOT_PATH.'/system/model/Pintuan.php');
                    $pintuan = new Pintuan();
                    $user_info = $GLOBALS['user_info'];

                    $pintuan->setPtUser(array('user_id'=>intval($GLOBALS['user_info']['id']),'ref_uid'=>$GLOBALS['ref_uid'],'user_name'=>$user_info['nick_name'],'mobile'=>$user_info['mobile']));

                    if($order_info['pt_group_id']>0){
                        $pt_rel = $pintuan->addToGroupItem($order_info['pt_group_id']);

                        if($pt_rel['status']==0){
                            //加团失败，直接开新团
                            $pt_rel = $pintuan->createGroup($order_id);
                        }
                    }else{
                        $pt_rel = $pintuan->createGroup($order_id);
                    }
                    //log_result($pt_rel);
                    //logger::write("pt_rel:".print_r($pt_rel,1));
                }



                /*if($use_score!=0)
                {
                    $user_score =  $GLOBALS['db']->getOne("select score from ".DB_PREFIX."user where id = ".$order_info['user_id']);
                    if($user_score+$use_score<0)
                    {
                        //积分不足，不能支付
                        $msg = $order_info['order_sn']."订单支付失败，积分不足";
                        if($payment_notice['class_name']=='Cod'||$order_info['cod_money']>0){
                            //货到付款的金额不参与退款
                            $refund_money = $order_info['pay_amount']-$order_info['cod_money'];
                        }else{
                            $refund_money = $order_info['pay_amount'];
                        }

                        if($order_info['account_money']>$refund_money) //天书部分
                            $account_money_now = $order_info['account_money'] - $refund_money;
                        else
                            $account_money_now = 0;

                        $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set account_money = ".$account_money_now." where id = ".$order_info['id']);

                        //if($order_info['ecv_money']>$refund_money)$ecv_money_now = $order_info['ecv_money'] - $refund_money; else $ecv_money_now = 0;
                        //$GLOBALS['db']->query("update ".DB_PREFIX."deal_order set ecv_money = ".$ecv_money_now." where id = ".$order_info['id']);

                        if($order_info['ecv_money']>0){//红包不参与退款
                            $refund_money = $refund_money-$order_info['ecv_money'];
                        }
                        if($refund_money>0)
                        {
                            modify_account(array('money'=>$refund_money,'score'=>0),$order_info['user_id'],$msg);
                            $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set refund_money = refund_money + ".$refund_money.",refund_amount = refund_amount + ".$refund_money.",after_sale = 1,refund_status = 2 where id = ".$order_info['id']);
                            $GLOBALS['db']->query("update ".DB_PREFIX."deal_order_item set refund_status = 2 where order_id = ".$order_info['id']);

                            order_log($order_info['order_sn']."积分不足，".format_price($refund_money)."已退到会员余额", $order_info['id']);

                            //修改 hc 0507 去掉退款报表
// 						modify_statements("-".$refund_money, 1, $order_info['order_sn']."积分不足，退款");
                            modify_statements($refund_money, 2, $order_info['order_sn']."积分不足，退款");
                            //收入, by hc 0507,积分商品付款退款，增加退款
                            modify_statements($refund_money, 0, $order_info['order_sn']."积分不足，退到余额"); //总收入
                        }
                        else
                        {
                            order_log($order_info['order_sn']."积分不足", $order_info['id']);
                        }
                        require_once(APP_ROOT_PATH."system/model/deal_order.php");
                        over_order($order_info['id']);
                    }
                    else
                    {
                        modify_account(array('score'=>$use_score),$order_info['user_id'],"积分商品兑换");
                        send_score_sms($order_info['id']);
                        send_score_mail($order_info['id']);
                        order_log($order_info['order_sn']."积分订单支付成功", $order_info['id']);

                        $msg_content = '您的积分订单<'.$order_info['order_sn'].'>兑换成功';
                        send_msg_new($order_info['user_id'], $msg_content, 'account', array('type' => 11, 'data_id' => $order_id));
                        // send_msg($order_info['user_id'], "订单".$order_info['order_sn']."兑换成功", "orderitem", $order_goods_list[0]['id']);
                        if($payment_notice['class_name']=='Cod'){
                            //货到付款，目前不产生商户报表和平台报表，不结算金额给商家
                        }else{
                            if($order_info['total_price']>0)
                                //收入, by hc 0507,积分商品付款成功，增加报表
                                modify_statements($order_info['total_price'], 0, $order_info['order_sn']."订单成功付款"); //总收入
                            modify_statements($order_info['total_price'], 1, $order_info['order_sn']."订单成功付款"); //订单支付收入

                            modify_statements($order_info['total_price'], 8, $order_info['order_sn']."订单成功付款");  //增加营业额
                            fanwe_require(APP_ROOT_PATH.'system/model/AppPush.php');
                            AppPush::singleUserPush('do_delivery',$order_info['user_id'],$msg_content,$order_info['id']);
                        }
                    }
                }
                else
                */{
                    //超出充值
                    if($order_info['pay_amount']>$order_info['total_price'])
                    {
                        require_once(APP_ROOT_PATH."system/model/user.php");
                        if($order_info['total_price']<0)
                            $msg = sprintf('%s订单冲值',$order_info['order_sn']);
                        else
                            $msg = sprintf('%s订单冲值',$order_info['order_sn']);
                        $refund_money = $order_info['pay_amount']-$order_info['total_price'];

                        if($order_info['account_money']>$refund_money)$account_money_now = $order_info['account_money'] - $refund_money; else $account_money_now = 0;
                        $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set account_money = ".$account_money_now." where id = ".$order_info['id']);

                        if($order_info['ecv_money']>$refund_money)$ecv_money_now = $order_info['ecv_money'] - $refund_money; else $ecv_money_now = 0;
                        $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set ecv_money = ".$ecv_money_now." where id = ".$order_info['id']);

                        modify_account(array('money'=>$refund_money,'score'=>0),$order_info['user_id'],$msg);
                        $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set refund_money = refund_money + ".$refund_money.",refund_amount = refund_amount + ".$refund_money." where id = ".$order_info['id']);

                        //order_log($order_info['order_sn']."订单超额支付，".format_price($refund_money)."已退到会员余额", $order_info['id']);
                        //by hc,0507 不再退款时记录
// 					modify_statements("-".$refund_money, 1, $order_info['order_sn']."订单超额");
                        //modify_statements($refund_money, 2, $order_info['order_sn']."订单超额");
                        //收入, by hc 0507
                        //modify_statements($refund_money, 0, $order_info['order_sn']."订单超额"); //总收入
                    }

                    //生成抽奖
                    $lottery_list = $GLOBALS['db']->getAll("select d.id as did,doi.number from ".DB_PREFIX."deal_order_item as doi left join ".DB_PREFIX."deal_order as do on doi.order_id = do.id left join ".DB_PREFIX."deal as d on doi.deal_id = d.id where d.is_lottery = 1 and do.id = ".$order_info['id']);
                    $lottery_user = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user where id = ".intval($order_info['user_id']));

                    //如为首次抽奖，先为推荐人生成抽奖号
                    //$lottery_count = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."lottery where user_id = ".intval($order_info['user_id']));
                    if($lottery_count == 0&&$lottery_user['pid']!=0)
                    {
                        $lottery_puser = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user where id = ".intval($lottery_user['pid']));
                        foreach($lottery_list as $lottery)
                        {
                            $k = 0;
                            do{
                                if($k>10)break;
                                $buy_count = $GLOBALS['db']->getOne("select buy_count from ".DB_PREFIX."deal where id = ".$lottery['did']);
                                $max_sn = $buy_count - $lottery['number'] + intval($GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."lottery where deal_id = ".intval($lottery['did'])." and buyer_id <> 0 "));
                                //$max_sn = intval($GLOBALS['db']->getOne("select lottery_sn from ".DB_PREFIX."lottery where deal_id = '".$lottery['did']."' order by lottery_sn desc limit 1"));
                                $sn = $max_sn + 1;
                                $sn = str_pad($sn,"6","0",STR_PAD_LEFT);
                                $sql = "insert into ".DB_PREFIX."lottery (`lottery_sn`,`deal_id`,`user_id`,`mobile`,`create_time`,`buyer_id`) select '".$sn."','".$lottery['did']."',".$lottery_puser['id'].",'".$lottery_puser['lottery_mobile']."',".NOW_TIME.",".$order_info['user_id']." from dual where not exists( select * from ".DB_PREFIX."lottery where deal_id = ".$lottery['did']." and lottery_sn = '".$sn."')";
                                $GLOBALS['db']->query($sql);
                                //send_lottery_sms(intval($GLOBALS['db']->insert_id()));
                                $k++;
                            }while(intval($GLOBALS['db']->insert_id())==0);
                        }
                    }


                    foreach($lottery_list as $lottery)
                    {
                        for($i=0;$i<$lottery['number'];$i++) //按购买数量生成抽奖号
                        {
                            $k = 0;
                            do{
                                if($k>10)break;
                                $buy_count = $GLOBALS['db']->getOne("select buy_count from ".DB_PREFIX."deal where id = ".$lottery['did']);
                                $max_sn = $buy_count - $lottery['number'] + intval($GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."lottery where deal_id = ".intval($lottery['did'])." and buyer_id <> 0 "));
                                //$max_sn = intval($GLOBALS['db']->getOne("select lottery_sn from ".DB_PREFIX."lottery where deal_id = '".$lottery['did']."' order by lottery_sn desc limit 1"));
                                $sn = $max_sn + $i + 1;
                                $sn = str_pad($sn,"6","0",STR_PAD_LEFT);
                                $sql = "insert into ".DB_PREFIX."lottery (`lottery_sn`,`deal_id`,`user_id`,`mobile`,`create_time`,`buyer_id`) select '".$sn."','".$lottery['did']."',".$order_info['user_id'].",'".$lottery_user['mobile']."',".NOW_TIME.",0 from dual where not exists( select * from ".DB_PREFIX."lottery where deal_id = ".$lottery['did']." and lottery_sn = '".$sn."')";
                                $GLOBALS['db']->query($sql);
                                //send_lottery_sms(intval($GLOBALS['db']->insert_id()));
                                $k++;
                            }while(intval($GLOBALS['db']->insert_id())==0);
                        }
                    }

                }

                $this->syn_order_done($order_info['id']);
            }
            else
            {
                //开始模拟事务回滚
// 			foreach($affect_attr_list as $k=>$v)
// 			{
// 				$sql = "update ".DB_PREFIX."attr_stock set buy_count = buy_count - ".$v['number']." where deal_id = ".$v['deal_id'].
//    			           " and locate(attr_str,'".$v['attr_str']."') > 0 ";

// 				$GLOBALS['db']->query($sql); //回滚已发的货量
// 			}
// 			foreach($affect_list as $k=>$v)
// 			{
// 				$sql = "update ".DB_PREFIX."deal set buy_count = buy_count - ".$v['num'].
// 				   	   ",user_count = user_count - 1 where id=".$v['deal_id'];
// 				$GLOBALS['db']->query($sql); //回滚已发的货量
// 			}
                $GLOBALS['db']->query("ROLLBACK");

                $GLOBALS['db']->query("update ".DB_PREFIX."deal_order_item set refund_status = 2 where order_id = ".$order_info['id']);

                if($payment_notice['class_name']=='Cod'||$order_info['cod_money']>0){
                    //货到付款的金额不参与退款
                    $return_money=$order_info['pay_amount']-$order_info['cod_money'];
                }else{
                    $return_money=$order_info['pay_amount'];
                }
                if($order_info['ecv_money']>0){
                    $return_money=$return_money-$order_info['ecv_money'];
                }
                //超出充值
                require_once(APP_ROOT_PATH."system/model/user.php");
                $msg = sprintf('%s订单中有团购已卖光，订单商品支付到会员中心',$order_info['order_sn']);
                modify_account(array('money'=>$return_money,'score'=>0),$order_info['user_id'],$msg);

                order_log($order_info['order_sn']."订单库存不足，".format_price($return_money)."已退到会员余额", $order_info['id']);

                //by hc 0507
// 			modify_statements("-".$order_info['total_price'], 1, $order_info['order_sn']."订单库存不足");
                //modify_statements($return_money, 2, $order_info['order_sn']."订单库存不足，退到余额");

                //收入, by hc 0507,积分商品付款成功，增加报表
                //modify_statements($return_money, 0, $order_info['order_sn']."订单库存不足，退到余额"); //总收入


                //将订单的extra_status 状态更新为2，并自动退款
                $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set extra_status = 2, after_sale = 1, refund_money = pay_amount where id = ".intval($order_info['id']));
                //记录退款的订单日志
                $log['log_info'] = $msg;
                $log['log_time'] = NOW_TIME;
                $log['order_id'] = intval($order_info['id']);
                $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order_log",$log);
            }
            $this->update_order_cache($order_info['id']);
            //同步所有未过期的团购状态
            /*foreach($goods_list as $item)
            {
                $this->syn_deal_status($item['deal_id'],true);
            }*/

            $this->auto_over_status($order_id); //自动结单
        }//end 普通团购
        else
        {

            //订单充值
// 		$GLOBALS['db']->query("update ".DB_PREFIX."deal_order set order_status = 1 where id = ".$order_info['id']); //充值单自动结单
            require_once(APP_ROOT_PATH."system/model/user.php");
            $msg = sprintf('充值单%s支付成功',$order_info['order_sn']);
// 		modify_account(array('money'=>$order_info['total_price']-$order_info['payment_fee'],'score'=>0),$order_info['user_id'],$msg);

            //by hc 0507
// 		modify_statements("-".($order_info['total_price']), 1, $order_info['order_sn']."会员充值");
            //modify_statements(($order_info['total_price']), 2, $order_info['order_sn']."会员充值，含手续费");

            //收入, by hc 0507
            //modify_statements($order_info['total_price'], 0, $order_info['order_sn']."会员充值，含手续费"); //总收入

            $money = $order_info['total_price'] - $order_info['payment_fee'];
            $msg_content = '您已成功充值<'.format_price($money).', 余额增加'.$money;
            //send_msg_new($order_info['user_id'], $msg_content, 'account', array('type' => 4, 'data_id' => $order_id));

            //充值送红包
           //$is_open_activity = $GLOBALS['db']->getRow("select is_open_active,activity_begin_time,activity_end_time from ".DB_PREFIX."ecv_activity_rules where type = 2 and is_open_active = 1");
            if(REGISTER_SEND && $is_open_activity['is_open_active']==1&&$payment_notice['sdk_type']=='wap'){
                if(($is_open_activity['activity_end_time']>=NOW_TIME ||$is_open_activity['activity_end_time']==0)&&NOW_TIME>=$is_open_activity['activity_begin_time']){
                    //是否开启充值送红包活动
                    require_once(APP_ROOT_PATH.'system/model/send_ecv_activity.php');
                    send_ecv_activity::send_ecv($order_info['user_id'],2,$money);
                    $status = 1; //活动里充值金额全部不可提现
                }
            }
            if($status == 1){
                //赠送不可提现金额
                modify_account(array('non_pre_money'=>$order_info['total_price']-$order_info['payment_fee'],'score'=>0),$order_info['user_id'],$msg);
            }else{
                modify_account(array('money'=>$order_info['total_price']-$order_info['payment_fee'],'score'=>0),$order_info['user_id'],$msg);
            }
            // send_msg($order_info['user_id'], "成功充值".format_price($order_info['total_price']-$order_info['payment_fee']), "notify", $order_id);

            $this->auto_over_status($order_id); //自动结单
        }
    }

    /**
     * 自动结单检测，如通过则结单
     * 自动结单规则
     * 注：自动结单条件
     * 1. 消费券全部验证成功
     * 2. 商品全部已收货
     * 3. 商品验证部份收货部份，其余退款
     * 结单后的商品不可再退款，不可再验证，不可再发货，可删除
     * @param unknown_type $order_id
     * return array("status"=>bool,"info"=>str)
     */
    function auto_over_status($order_id)
    {
        $order_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);
        if($order_info)
        {
            if($order_info['pay_status']<>2)
            {
                return array("status"=>false,"info"=>"订单未支付");
            }
            if($order_info['order_status']<>0)
            {
                return array("status"=>false,"info"=>"订单已结单");
            }

            if($order_info['type'] <> 1)
            {
                //消费券未验证且未退款的数量为0
                $coupon_less = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_coupon where order_id = ".$order_id." and confirm_time = 0 and refund_status <> 2  and ( end_time=0 or ( end_time>=".NOW_TIME." or (end_time<".NOW_TIME." and expire_refund=1 and refund_status in (0,1))))");

                //全部未收货且未退款的数量为0
                $delivery_less = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_order_item where order_id = ".$order_id." and is_arrival <> 1 and refund_status <> 2 and is_shop=1 and is_coupon=0");

                if(($coupon_less==0&&$delivery_less==0)||$order_info['extra_status']==2)//补充，发货失败自动结单
                {
                    $this->over_order($order_id);
                }
            }
            else
            {
                $this->over_order($order_id); //充值单只要支付过就结单
            }
            return array("status"=>true,"info"=>"结单成功");
        }
        else
        {
            return array("status"=>false,"info"=>"订单不存在");
        }
    }

    /**
     * 主单支付成功后，更新子单的金额和订单状态，优惠数据等
     * @param unknown $order_id
     */
    function syn_order_done($order_id){
        $order_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);

        if($order_info['is_main']==1){  //如果是主单，则更新子单的金额和订单状态
            $sub_order_info = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order where is_main=0 and pay_status <> 2 and order_id = ".$order_id);
            $ecv_payment_memo='';
            $account_payment_memo='';
            $third_payment_memo='';
            $third_payment_id = $order_info['payment_id'];
            $ecv_order_data = array();
            $account_order_data = array();
            $third_order_data = array();

            // 订单拆分时部分均摊金额临时变量
            $tempfee = 0; // 运费
            $tempecv = 0; // 红包抵扣
            $tempaccount = 0; // 余额支付部分
            // $tempdisc = 0; // 折扣金额
            $tempCount = 0; // 拆分的订单数量

            $total_price_all = 0;
            foreach($sub_order_info as $k=>$v){
                if($v['total_price'] > $v['youhui_money']){  //子单由商家的优惠券全部抵扣，不参与红包，余额支付，第三方支付拆分
                    $total_price_all+=($v['total_price']-$v['youhui_money']);
                    $tempCount++;
                }
            }

            foreach($sub_order_info as $k=>$v){

                if($v['total_price'] > $v['youhui_money']){  //子单由商家的优惠券全部抵扣，不参与红包，余额支付，第三方支付拆分

                    $rate = ($v['total_price'] -  $v['youhui_money'] ) / $total_price_all; //主单和子单的比率

                    $sub_data = array();

                    if (($k+1) == $tempCount) {
                        $sub_data['payment_fee'] = $order_info['payment_fee'] - $tempfee;
                        $sub_data['ecv_money'] = $order_info['ecv_money'] - $tempecv;
                        $sub_data['account_money'] = $order_info['account_money'] - $tempaccount;
                        // $sub_data['discount_price'] = $order_info['discount_price'] - $tempdisc;
                    } else {
                        $sub_data['payment_fee'] = round($order_info['payment_fee'] * $rate, 2);
                        $tempfee += $sub_data['payment_fee'];
                        $sub_data['ecv_money'] = round($order_info['ecv_money'] * $rate, 2);
                        $tempecv += $sub_data['ecv_money'];
                        $sub_data['account_money'] = round($order_info['account_money'] * $rate, 2);
                        $tempaccount += $sub_data['account_money'];
                        // $sub_data['discount_price'] = round($order_info['discount_price'] * $rate, 2);
                        // $tempdisc += $sub_data['discount_price'];
                    }

                    $sub_data['pay_status'] = $order_info['pay_status'];
                    $sub_data['total_price'] = $sub_data['pay_amount'] = $v['total_price'] + $sub_data['payment_fee'];
                    $sub_data['order_status'] = $order_info['order_status'];
                    /*  $sub_data['return_total_score'] = $order_info['return_total_score']; */
                    /*  $sub_data['return_total_money'] = $order_info['return_total_money']; */
                    $sub_data['ecv_id'] = $order_info['ecv_id'];
                    $sub_data['payment_id'] = $order_info['payment_id'];

                    $sub_data['bank_id'] = $order_info['bank_id'];
                    $sub_data['promote_description'] = $order_info['promote_description'];
                    $sub_data['promote_arr'] = $order_info['promote_arr'];

                    //消费送积分
                    if($v['is_consumer_send_score']==1){
                        $order_cs_score_data=unserialize($v['consumer_send_score_data']);
                        $actual_price=$sub_data['total_price'] - $sub_data['ecv_money'] - $v['youhui_money'];
                        $actual_price=$actual_price>0?$actual_price:0;
                        if($actual_price>0&&$order_cs_score_data['send_integral_proportion']>0){
                            $consumer_send_score = intval($actual_price)*$order_cs_score_data['send_integral_proportion'];
                            $consumer_send_score_data=$order_cs_score_data;
                            $consumer_send_score_data['actual_pay_price']=$actual_price;
                            $consumer_send_score_data['actual_pay_price_send_score']=$consumer_send_score;

                            $deal_order['consumer_send_score']=$consumer_send_score;
                            $deal_order['consumer_send_score_data']=unserialize($consumer_send_score_data);
                        }else{
                            $deal_order['is_consumer_send_score']=0;
                            $deal_order['consumer_send_score']=0;
                            $deal_order['consumer_send_score_data']=unserialize(array());
                        }

                    }

                    $consumer_send_score_data=$order_cs_score_data;
                    $consumer_send_score_data['actual_pay_price']=$actual_price;
                    $consumer_send_score_data['actual_pay_price_send_score']=$consumer_send_score;

                    $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order", $sub_data, $mode = 'UPDATE', "id=".$v['id'], $querymode = 'SILENT');

                }else{
                    $sub_data = array();
                    $sub_data['pay_status'] = $order_info['pay_status'];
                    $sub_data['pay_amount'] = $v['total_price'];
                    $sub_data['promote_description'] = $order_info['promote_description'];
                    $sub_data['promote_arr'] = $order_info['promote_arr'];
                    $sub_data['order_status'] = $order_info['order_status'];
                    $GLOBALS['db']->autoExecute(DB_PREFIX."deal_order", $sub_data, $mode = 'UPDATE', "id=".$v['id'], $querymode = 'SILENT');

                }

                $rs = $GLOBALS['db']->affected_rows();
                if($rs){
                    //生成子单的付款单号

                    if($sub_data['ecv_money'] > 0){  //生成代金券付款单号
                        $ecv_payment_memo .='订单号：'.$v['order_sn']."(".round($sub_data['ecv_money'],2)."元),";
                        $data=array();
                        $data['order_id'] = $v['id'];
                        $data['order_sn'] = $v['order_sn'];
                        $data['type'] = $v['type'];
                        $data['money'] = $sub_data['ecv_money'];
                        $ecv_order_data[] = $data;
                    }
                    if($sub_data['account_money'] > 0){  //生成余额付款单号

                        $account_payment_memo .='订单号：'.$v['order_sn']."(".round($sub_data['account_money'],2)."元),";
                        $data=array();
                        $data['order_id'] = $v['id'];
                        $data['order_sn'] = $v['order_sn'];
                        $data['type'] = $v['type'];
                        $data['money'] = $sub_data['account_money'];
                        $account_order_data[] = $data;
                    }
                    if($sub_data['payment_id'] > 0){  //生成第三方支付付款单号
                        $third_payment_fee =  $sub_data['total_price'] - $sub_data['ecv_money'] - $sub_data['account_money'] -  $sub_data['youhui_money'];
                        $third_payment_memo .='订单号：'.$v['order_sn']."(".round($third_payment_fee,2)."元),";
                        $data=array();
                        $data['order_id'] = $v['id'];
                        $data['order_sn'] = $v['order_sn'];
                        $data['type'] = $v['type'];
                        $data['money'] = $third_payment_fee;
                        $third_order_data[] = $data;
                    }



                    $msg_content = '您的订单<'.$v['order_sn'].'>已成功付款';
                    //send_msg_new($v['user_id'], $msg_content, 'account', array('type' => 9, 'data_id' => $v['id']));


                    //收入, by hc 0507,增加报表
                    //modify_statements($sub_data['total_price'], 0, $v['order_sn']."订单成功付款"); //总收入
                    //modify_statements($sub_data['total_price'], 1, $v['order_sn']."订单成功付款"); //订单支付收入

                    //modify_statements($sub_data['total_price'], 8, $v['order_sn']."订单成功付款");  //增加营业额
                    $balance_total = $GLOBALS['db']->getOne("select sum(balance_total_price)+sum(add_balance_price_total) from ".DB_PREFIX."deal_order_item where order_id = ".$v['id']);
                    //modify_statements($balance_total, 9, $v['order_sn']."订单成功付款");  //增加营业额中的成本

                    //order_log($v['order_sn']."订单付款完成", $v['id']);

                    //通知商户

                    //发送微信通知
                    if ($v['type'] != 5) { // 团购券不发商户消息
                        //send_supplier_msg($v['supplier_id'], 'send', $v['id']);
                    }

                    $weixin_conf = load_auto_cache("weixin_conf");
                    if($weixin_conf['platform_status']==1)
                    {
                        $wx_account = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."weixin_account where user_id = ".$v['supplier_id']);
                        //send_wx_msg("OPENTM201490080", $v['user_id'], $wx_account,array("order_id"=>$v['id']));
                    }

                    //如果是驿站订单，则进行驿站配送
                    if($v['type']==4){
                        //setDist($v);
                    }
                    //自营新订单邮件通知
                    if(in_array($v['type'],array(3,4))){
                        //send_platform_new_order_mail($order_id);
                    }elseif(in_array($v['type'],array(5,6))){
                        //send_supplier_new_order_mail($order_id);
                    }

                    if($v['consignee_id']>0&&$v['type']==6&&$v['supplier_id']>0){
                        //fanwe_require(APP_ROOT_PATH.'system/model/AppPush.php');
                       //AppPush::biz_new_deliver_goods_order($v);
                    }

                }

            }
            $payment_notice = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."payment_notice where order_type=3 and is_paid = 1 and order_id =" .$order_info['id']);
            $ecv_payment_id = intval($GLOBALS['db']->getOne("select id from ".DB_PREFIX."payment where class_name='Voucher'"));
            $account_payment_id = intval($GLOBALS['db']->getOne("select id from ".DB_PREFIX."payment where class_name='Account'"));
            foreach($payment_notice as $kk=>$vv){
                $payment_memo='';
                $sub_order_data='';
                if($vv['payment_id']==$ecv_payment_id){
                    $payment_memo = $ecv_payment_memo;
                    $sub_order_data=serialize($ecv_order_data);
                }elseif($vv['payment_id']==$account_payment_id){
                    $payment_memo = $account_payment_memo;
                    $sub_order_data=serialize($account_order_data);
                }elseif($vv['payment_id']==$third_payment_id){
                    $payment_memo = $third_payment_memo;
                    $sub_order_data=serialize($third_order_data);
                }
                $payment_memo = substr($payment_memo,0,-1);
                $GLOBALS['db']->query("update ".DB_PREFIX."payment_notice set sub_order_data ='".$sub_order_data."' , memo='".$payment_memo."' where id=".$vv['id']);
            }
        }else{
            $payment_notice = $GLOBALS['db']->getRow("select pn.id,p.class_name from ".DB_PREFIX."payment_notice as pn left join ".DB_PREFIX."payment AS p on pn.payment_id=p.id where order_id = ".$order_info['id']." and payment_id = ".$order_info['payment_id']." and is_paid=1 and money>0");
            if($payment_notice['class_name']=="Cod"){
                $msg_content = '您的订单<'.$order_info['order_sn'].'>已下单成功';
                //send_msg_new($order_info['user_id'], $msg_content, 'account', array('type' => 9, 'data_id' => $order_id));
                //order_log($order_info['order_sn']."订单成功下单", $order_id);
            }else{
                $msg_content = '您的订单<'.$order_info['order_sn'].'>已成功付款';
                //send_msg_new($order_info['user_id'], $msg_content, 'account', array('type' => 9, 'data_id' => $order_id));


                //收入, by hc 0507,增加报表
                //modify_statements($order_info['total_price'], 0, $order_info['order_sn']."订单成功付款"); //总收入
                //modify_statements($order_info['total_price'], 1, $order_info['order_sn']."订单成功付款"); //订单支付收入

                //modify_statements($order_info['total_price'], 8, $order_info['order_sn']."订单成功付款");  //增加营业额
                $balance_total = $GLOBALS['db']->getOne("select sum(balance_total_price)+sum(add_balance_price_total) from ".DB_PREFIX."deal_order_item where order_id = ".$order_info['id']);
                //modify_statements($balance_total, 9, $order_info['order_sn']."订单成功付款");  //增加营业额中的成本

                //order_log($order_info['order_sn']."订单付款完成", $order_id);


            }
            //自营新订单邮件通知
            if(in_array($order_info['type'],array(3,4))){
                //send_platform_new_order_mail($order_id);
            }elseif(in_array($order_info['type'],array(5,6))){
                //send_supplier_new_order_mail($order_id);
            }

            if($order_info['consignee_id']>0&&$order_info['type']==6&&$order_info['supplier_id']>0){
                //fanwe_require(APP_ROOT_PATH.'system/model/AppPush.php');
                //AppPush::biz_new_deliver_goods_order($order_info);
            }


            //通知商户
            $supplier_list = $GLOBALS['db']->getAll("select distinct(supplier_id) from ".DB_PREFIX."deal_order_item where order_id = ".$order_id);
            foreach($supplier_list as $row)
            {
                //发送微信通知
                if ($order_info['type'] != 5) {
                    //send_supplier_msg($row['supplier_id'], 'send', $order_id);
                }

                //$weixin_conf = load_auto_cache("weixin_conf");
                /*if($weixin_conf['platform_status']==1)
                {
                    //$wx_account = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."weixin_account where user_id = ".$row['supplier_id']);
                    //send_wx_msg("OPENTM201490080", $order_info['user_id'], $wx_account,array("order_id"=>$order_id));
                }*/
            }

            //如果是驿站订单，则进行驿站配送
            if($order_info['type']==4){
                //setDist($order_info);
            }
        }

    }

    /**
     * 订单支付页，包含检测状态，获取支付代码与消费券
     *
     * 输入:
     * id: int 订单ID
     *
     * 输出:
     * status:int 状态 0:失败 1:成功
     * info: string 失败的原因
     * 以下参数为成功时返回
     * pay_status: int 支付状态 0:未支付 1:已支付
     * order_id: int 订单ID
     * order_sn: string 订单号
     * buy_type int 0 代表普通购买，1为积分兑换
     * order_type int 1为充值订单，否则为商品或者团购下单
     * pay_info: string 显示的信息
     * consignee_info 收货地址
     * 当pay_status 为1时
     * coupon_name  消费券的名称
     * couponlist: array 消费券列表
     * Array
     * (
     * 		Array(
     * 			"password" => string 验证码
     * 			"qrcode"  => string 二维码地址
     * 		)
     * )
     *
     * 当pay_status 为0时
     * payment_code: Array() 相关支付接口返回的支付数据
     */
    public function payment_done()
    {
        $root = array();
        //$order_id = intval($GLOBALS['request']['id']);
        $order_id = intval($_REQUEST['order_id']);

        $order_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where id = ".$order_id);
        if(empty($order_info))
        {
            $root['status'] = 0;
            $root['error']  = "订单不存在";
            api_ajax_return($root);
        }

        $root['order_sn'] = $order_info['order_sn'];
        $root['order_id'] = $order_id;
        $root['is_main'] = $order_info['is_main'];

        $root['order_type']=$order_info['type'];
        if($order_info['pay_status']==2 || ($order_info['is_presell_order']==1 &&  $order_info['pay_status']==1))
        {
            if($order_info['type'] !=1)
            {
                $buy_type=0;
                $deal_ids = $GLOBALS['db']->getOne("select group_concat(deal_id) from ".DB_PREFIX."deal_order_item where order_id = ".$order_id);
                if(!$deal_ids)
                    $deal_ids = 0;
                $order_deals = $GLOBALS['db']->getAll("select d.* from ".DB_PREFIX."deal as d where id in (".$deal_ids.")");

                $is_lottery = 0;
                foreach($order_deals as $k=>$v)
                {
                    if($v['is_lottery'] == 1&&$v['buy_status']>0)
                    {
                        $is_lottery = 1;
                    }

                    if($v['buy_type'] == 1)
                    {
                        $buy_type = 1;
                    }

                }

                $order_info['region_lv1'] = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."delivery_region where id = ".$order_info['region_lv1']);
                $order_info['region_lv2'] = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."delivery_region where id = ".$order_info['region_lv2']);
                $order_info['region_lv3'] = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."delivery_region where id = ".$order_info['region_lv3']);
                $order_info['region_lv4'] = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."delivery_region where id = ".$order_info['region_lv4']);
                $consignee_info=array();
                if($order_info['consignee']){
                    $consignee_info['consignee'] = $order_info['consignee'];
                    $consignee_info['mobile'] = $order_info['mobile'];
                    $consignee_info['address'] = $order_info['region_lv1']['name'].$order_info['region_lv2']['name'].$order_info['region_lv3']['name'].$order_info['region_lv4']['name'].$order_info['address'].$order_info['street'].$order_info['doorplate'];
                }
                $root['buy_type']=$buy_type;
                $root['consignee_info'] = count($consignee_info)>0?$consignee_info:null;
                $root['pay_status'] = 1;
                $root['pay_info'] = '订单已经收款';
                if($is_lottery)
                    $root['pay_info'] .= '，您已经获得抽奖序列号';

                //有消费券,再显示消费券列表
                $list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_coupon where user_id = ".$order_info['user_id']." and order_id = ".$order_id);

                $couponlist = array();
                foreach($list as $k=>$v)
                {
                    $couponlist[$k]['password'] = $v['password'];
                    $couponlist[$k]['qrcode'] = get_spec_image(gen_qrcode($v['password']));
                }
                $root['couponlist'] = $couponlist;
                $root['coupon_count'] = intval(count($couponlist));

                if($order_info['location_id']==0){
                    $root['coupon_name'] = app_conf('COUPON_NAME');
                }else{
                    $root['coupon_name'] = '自提劵';
                }


                return ($root);
            }
            else
            {
                $root['pay_status'] = 1;
                $root['pay_info'] = round($order_info['pay_amount'],2)." 元 充值成功";
                return ($root);
            }
        }
        else
        {
            if($order_info['type']==1){//用户充值订单
                //$pay_price = $order_info['total_price'];
                //$payment_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."payment where id = ".$order_info['payment_id']);
            }else{


                $delivery_id =  $order_info['delivery_id']; //配送方式
                $region_id = 0; //配送地区
                if($delivery_id)
                {
                    $consignee_id = $GLOBALS['db']->getOne("select id from ".DB_PREFIX."user_consignee where user_id = ".$GLOBALS['user_info']['id']." and is_default = 1");
                    $consignee_info = load_auto_cache("consignee_info",array("consignee_id"=>$consignee_id));
                    $consignee_info = $consignee_info['consignee_info']?$consignee_info['consignee_info']:array();
                    if($consignee_info)
                        $region_id = intval($consignee_info['region_lv4']);
                }
                $payment = $order_info['payment_id'];

                //  $goods_list = $GLOBALS['db']->getAll("select * from ".DB_PREFIX."deal_order_item where order_id = ".$order_info['id']);

                // $data = count_buy_total($region_id,$order_info['consignee_id'],$payment,0,0,'','',$goods_list,$order_info['account_money'],$order_info['ecv_money']);



                $pay_price = $order_info['total_price'] - $order_info['pay_amount'] - $order_info['youhui_money'];
                $payment_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."payment where id = ".$order_info['payment_id']);
                if(empty($payment_info))
                {
                    $root['status'] = 0;
                    $root['error']  = "支付方式不存在";
                    api_ajax_return($root);
                }
                if($pay_price<=0)
                {
                    $root['status'] = 0;
                    $root['error']  = "无效的支付方式";
                    api_ajax_return($root);
                }
                $app_index = APP_INDEX;
                if($app_index=="wap")
                {
                    if ($payment_info['online_pay']!=2&&$payment_info['online_pay']!=4&&$payment_info['online_pay']!=5)
                    {
                        $root['status'] = 0;
                        $root['error']  = "该支付方式不支持wap支付";
                        api_ajax_return($root);
                    }
                }
                else
                {
                    if ($payment_info['online_pay']!=2&&$payment_info['online_pay']!=3&&$payment_info['online_pay']!=4&&$payment_info['online_pay']!=5)
                    {
                        $root['status'] = 0;
                        $root['error']  = "该支付方式不支持手机支付";
                        api_ajax_return($root);
                    }
                }
            }


            $payment_notice_id = $this->make_payment_notice($pay_price,$order_id,$order_info['payment_id']);
            require_once(APP_ROOT_PATH."system/payment/".$payment_info['class_name']."_payment.php");
            $payment_class = $payment_info['class_name']."_payment";
            $payment_object = new $payment_class();
            $payment_code = $payment_object->get_payment_code($payment_notice_id);
            $root['title'] = $payment_info['name'];

            if($payment_info['class_name']=='Account'){
                $is_account_pay = 1;
            }else{
                $is_account_pay = 0;
            }
            $root['is_account_pay'] = $is_account_pay;
            $root['pay_status'] = 0;
            $root['payment_code'] = $payment_code;
            $root['app_index'] = $app_index;
            return ($root);
        }
    }

    /**
     *
     * 创建付款单号
     * @param $money 付款金额
     * @param $order_id 订单ID
     * @param $payment_id 付款方式ID
     * @param $memo 付款单备注
     * @param $ecv_id 如为代金券支付，则指定代金券ID
     * @param $sdk_type 请求来源 wap或者pc
     * return payment_notice_id 付款单ID
     *
     */
    function make_payment_notice($money,$order_id,$payment_id,$memo='',$ecv_id=0,$payment_config=array(),$sdk_type='')
    {
        if($money > 0){
            $notice['create_time'] = NOW_TIME;
            $notice['order_id'] = $order_id;
            $notice['user_id'] = $GLOBALS['db']->getOne("select user_id from ".DB_PREFIX."deal_order where id = ".$order_id);
            $notice['payment_id'] = $payment_id;
            $notice['memo'] = $memo;
            $notice['money'] = $money;
            $notice['ecv_id'] = $ecv_id;
            $notice['order_type'] = 3;  //普通订单
            $notice['sdk_type'] = $sdk_type;  //普通订单

            $notice['payment_config']=serialize($payment_config);
            do{
                $notice['notice_sn'] = to_date(NOW_TIME,"Ymdhis").rand(10,99);
                $GLOBALS['db']->autoExecute(DB_PREFIX."payment_notice",$notice,'INSERT','','SILENT');
                $notice_id = intval($GLOBALS['db']->insert_id());
            }while($notice_id==0);
            return $notice_id;
        }
    }

    /**
     * 结单操作，结单操作将发放邀请返利
     * @param unknown_type $order_id
     */
    function over_order($order_id)
    {

        $order_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."deal_order where order_status = 0 and id = ".$order_id);
        if($order_info)
        {
            $GLOBALS['db']->query("update ".DB_PREFIX."deal_order set order_status = 1,is_refuse_delivery = 0 where order_status = 0 and id = ".$order_id);
            if(!$GLOBALS['db']->affected_rows())
            {
                return;  //结单失败
            }


            $GLOBALS['db']->query("update ".DB_PREFIX."deal_coupon set is_valid = 2 where order_id = ".$order_id);
            $this->distribute_order($order_id);
            $m_config = load_auto_cache("m_config");
            if($order_info['cod_money']>0){
                //货到付款
            }else{
                //订单完结，统一结算运费给商家
                $delivery_fee = $order_info['delivery_fee'];
                if($delivery_fee > 0){
                    $total_diamonds=$delivery_fee*10;
                    $total_ticket=$delivery_fee*10*$m_config['ticket_to_rate'];
                    $log = $order_info['order_sn']."订单完结,结算运费,获得".$total_ticket.$m_config['ticket_name'];
                    require_once(APP_ROOT_PATH."system/libs/user.php");
                    $supplier_user_id= $GLOBALS['db']->getOne("select user_id from ".DB_PREFIX."supplier where id = ".intval($order_info['supplier_id'])." ");

                    modify_account(array("ticket" =>$total_ticket), $supplier_user_id, $log,array("type" => 8));

                    if (defined('OPEN_TEAM_DISTRIBUTION') && OPEN_TEAM_DISTRIBUTION == 1) {
                        distribution_earnings(intval($order_info['user_id']),2, $total_diamonds,$order_info['id']);
                        distribution_earnings($supplier_user_id,4,  $total_ticket,$order_info['id']);
                    }

                    if (defined('OPEN_CITY_PARTNER') && OPEN_CITY_PARTNER == 1) {
                        // 城市合伙人收益,type:1虚拟消费收益；2产品消费收益
                        city_partner_earnings(intval($order_info['user_id']),2,$total_diamonds,$order_info['id']);
                    }

                }
            }
            //结单要有后只未退款的才可返利
            $coupon_refunded = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_coupon where order_id = ".$order_id." and refund_status = 2");
            $order_item_refunded = $GLOBALS['db']->getOne("select count(*) from ".DB_PREFIX."deal_order_item where order_id = ".$order_id." and refund_status = 2");
            if($order_item_refunded>0||$coupon_refunded>0||$order_info['is_delete']==1)
            {
                return; //不再返利
            }

            //结算





            /*
                        $goods_list = $GLOBALS['db']->getAll("select deal_id,sum(number) as num from ".DB_PREFIX."deal_order_item where order_id = ".$order_id." group by deal_id");
                        //返利
                        //开始处理返利，只创建返利， 发放将与msg_list的自动运行一起执行
                        $user_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user where id = ".$order_info['user_id']);
                        //开始查询所购买的列表中支不支持促销

                        $is_referrals = 1; //默认为返利
                        foreach($goods_list as $k=>$v)
                        {
                            $is_referrals = $GLOBALS['db']->getOne("select is_referral from ".DB_PREFIX."deal where id = ".$v['deal_id']);
                            if($is_referrals == 0)
                            {
                                break;
                            }
                        }
                        if($user_info['referral_count']<app_conf("REFERRAL_LIMIT")&&$is_referrals == 1)
                       {
                           //开始返利给推荐人
                           $parent_info = $GLOBALS['db']->getRow("select * from ".DB_PREFIX."user where id = ".$user_info['pid']);
                           if($parent_info)
                           {
                               if((app_conf("REFERRAL_IP_LIMIT")==1&&$parent_info['login_ip']!=CLIENT_IP)||app_conf("REFERRAL_IP_LIMIT")==0) //IP限制
                               {
                                   if(app_conf("INVITE_REFERRALS_TYPE")==0) //现金返利
                                   {
                                       $referral_data['user_id'] = $parent_info['id']; //初返利的会员ID
                                       $referral_data['rel_user_id'] = $user_info['id'];	 //被推荐且发生购买的会员ID
                                       $referral_data['create_time'] = NOW_TIME;
                                       $referral_data['money']	=	app_conf("INVITE_REFERRALS");
                                       $referral_data['order_id']	=	$order_info['id'];
                                       $GLOBALS['db']->autoExecute(DB_PREFIX."referrals",$referral_data); //插入
                                   }
                                   else
                                   {
                                       $referral_data['user_id'] = $parent_info['id']; //初返利的会员ID
                                       $referral_data['rel_user_id'] = $user_info['id'];	 //被推荐且发生购买的会员ID
                                       $referral_data['create_time'] = NOW_TIME;
                                       $referral_data['score']	=	app_conf("INVITE_REFERRALS");
                                       $referral_data['order_id']	=	$order_info['id'];
                                       $GLOBALS['db']->autoExecute(DB_PREFIX."referrals",$referral_data); //插入
                                   }
                                   $GLOBALS['db']->query("update ".DB_PREFIX."user set referral_count = referral_count + 1 where id = ".$user_info['id']);
                               }

                           }
                       }*/

            $this->update_order_cache($order_id);
        }
    }
}
?>

