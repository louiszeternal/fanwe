3.1;

update `%DB_PREFIX%m_config` set `desc` = '用于网页应用微信登录' where `code` = 'wx_web_appid';
update `%DB_PREFIX%m_config` set `desc` = '用于网页应用微信登录' where `code` = 'wx_web_secrit';
update `%DB_PREFIX%m_config` set `desc` = '用于网页应用微博登录' where `code` = 'sina_web_app_key';
update `%DB_PREFIX%m_config` set `desc` = '用于网页应用微博登录' where `code` = 'sina_web_app_secret';

UPDATE `%DB_PREFIX%conf` SET `value`='2.24' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%role_node` (`action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES ('index', '列表', '1', '0', '0', (select id from `%DB_PREFIX%role_module` where module='PlugIn'));
INSERT INTO `%DB_PREFIX%role_node` (`action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES ('edit', '编辑', '1', '0', '0', (select id from `%DB_PREFIX%role_module` where module='PlugIn'));

INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'pc_has_private_chat', 'PC端允许私信', 'PC端设置', '1', 4, 0, '0,1', '否,是', '是否开启私信功能，1开启 0关闭');

ALTER TABLE `%DB_PREFIX%recharge_rule`
ADD COLUMN `iap_diamonds`  int(11) NULL DEFAULT 0 COMMENT '苹果支付获取钻石';

UPDATE `%DB_PREFIX%conf` SET `value`='2.25' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('distribution_wx', '显示微信登录', '分销模块', '0', 4, 0, '0,1', '否,是', '屏蔽微信登录方式，不影响分享等其他微信相关功能 1是 0否');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('distribution_qq', '显示QQ登录', '分销模块', '0', 4, 0, '0,1', '否,是', '屏蔽QQ登录方式，不影响分享等其他QQ相关功能 1是 0否');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('distribution_sina', '显示微博登录', '分销模块', '0', 4, 0, '0,1', '否,是', '屏蔽微博登录方式，不影响分享等其他微博相关功能 1是 0否');

UPDATE `%DB_PREFIX%conf` SET `value`='2.26' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%family`
ADD COLUMN `user_count` int(11) NOT NULL DEFAULT 1 COMMENT '成员数量' after `user_id`;

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_page_size', '监控界面分页', '基础配置', '10', 0, 0, NULL, NULL, '(条) 监控界面分页数量，默认为10，最低数量不能低于10条，否则取默认值');
CREATE TABLE `%DB_PREFIX%pc_goods` (
`id`  int(11) NOT NULL AUTO_INCREMENT COMMENT '自增字段' ,
`user_id`  int(11) NOT NULL COMMENT '主播ID' ,
`name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品名称' ,
`imgs`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片（JSON数据）' ,
`price`  decimal(20,2) NOT NULL COMMENT '商品价钱' ,
`url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品详情URL地址' ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品描述' ,
`is_delete`  tinyint(1) NOT NULL COMMENT '商品状态 0为正常,1为删除（值为1时不在前端展示）' ,
`kd_cost`  decimal(20,2) NOT NULL COMMENT '快递费用' ,
PRIMARY KEY (`id`)
)
;
UPDATE `%DB_PREFIX%m_config` SET val=NULL WHERE `code`='pc_download_slogan';
UPDATE `%DB_PREFIX%m_config` SET val=NULL WHERE `code`='pc_logo';
UPDATE `%DB_PREFIX%conf` SET `value`='2.27' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `is_hot_on`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '禁热门 0-正常；1-禁止';

CREATE TABLE `%DB_PREFIX%warning_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL COMMENT '警告内容',
  `is_effect` tinyint(1) DEFAULT '1' COMMENT '是否有效 0:无效;1:有效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UPDATE `%DB_PREFIX%conf` SET `value`='2.29' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%conf` (`name`, `value`, `group_id`, `input_type`, `value_scope`, `is_effect`, `is_conf`, `sort`) VALUES ('COPYRIGHT', 'Copyright 2016-2017 sjzhsd.cn All rights reserved.', 1, 0, '', 1, 1, 24);

UPDATE `%DB_PREFIX%conf` SET `is_effect`='0' WHERE (`name`='USER_VERIFY_STATUS');

INSERT INTO `%DB_PREFIX%conf` (`name`, `value`, `group_id`, `input_type`, `value_scope`, `is_effect`, `is_conf`, `sort`) VALUES ('BG_PAGE', '', 1, 2, '', 1, 1, 24);

INSERT INTO `%DB_PREFIX%conf` (`name`, `value`, `group_id`, `input_type`, `value_scope`, `is_effect`, `is_conf`, `sort`) VALUES ('BG_APP', '', 1, 2, '', 1, 1, 24);

UPDATE `%DB_PREFIX%conf` SET `value`='2.291' WHERE (`name`='DB_VERSION');

UPDATE `%DB_PREFIX%m_config` SET `desc`='默认：否；IOS版本只有苹果支付；（开启其他支付(微信,支付宝等)选择：是；注：若开启，则IOS会有被苹果下架风险）' WHERE (`code`='ios_open_pay');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('domain_list', '备用域名', '应用设置', '', 3, 100, NULL, NULL, '备用域名列表，每行填写一个域名');

UPDATE `%DB_PREFIX%conf` SET `value`='2.292' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%video`
ADD COLUMN `len_time`  int(11) NOT NULL COMMENT '直播的时长';

ALTER TABLE `%DB_PREFIX%video_history`
ADD COLUMN `len_time`  int(11) NOT NULL COMMENT '直播的时长';

UPDATE `%DB_PREFIX%conf` SET `value`='2.3' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%video`
ADD COLUMN `is_concatvideo`  tinyint(1) NOT NULL COMMENT '视频是否合并 0 未合并，1 已合并';

ALTER TABLE `%DB_PREFIX%video_history`
ADD COLUMN `is_concatvideo`  tinyint(1) NOT NULL COMMENT '视频是否合并 0 未合并，1 已合并';

UPDATE `%DB_PREFIX%m_config` SET `desc`='备用域名列表，每行填写一个域名，头部要包含http或https，例如 http://www.xx.com' WHERE (`code`='domain_list');

CREATE TABLE `%DB_PREFIX%login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '会员ID',
  `create_time` varchar(20) NOT NULL COMMENT '登录时间',
  `ip` varchar(20) NOT NULL COMMENT 'ip',
  `login_time` int(11) NOT NULL COMMENT '登录时间',
  `login_date` datetime NOT NULL COMMENT '登录时间',
  `login_type` tinyint(1) NOT NULL COMMENT '登录方式',
  `request` text NOT NULL COMMENT '请求参数',
  `ctl_act` varchar(20) NOT NULL COMMENT '请求接口',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

UPDATE `%DB_PREFIX%conf` SET `value`='2.31' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%video`
ADD COLUMN `stick`  tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否置顶 0 不置顶 1 置顶';

ALTER TABLE `%DB_PREFIX%video_history`
ADD COLUMN `stick`  tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否置顶 0 不置顶 1 置顶';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `investor_time`  int(10) NOT NULL COMMENT '审核失败时间';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `alone_ticket_ratio`  varchar(255) NOT NULL COMMENT '设置主播提现比例,如果为空,则使用后台通用比例';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `open_game`  tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启游戏  0为 不禁用 1 为禁用';
ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `open_pay`  tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启付费  0为 不禁用 1 为禁用';
ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `open_auction`  tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启竞拍  0为 不禁用 1 为禁用';
ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `family_recom`  varchar(6) NOT NULL COMMENT '家族推荐号 填写后审核通过自动加入相对应的家族';
ALTER TABLE `%DB_PREFIX%family`
ADD COLUMN `family_recom`  varchar(6) NOT NULL COMMENT '家族推荐号 创建家族后随机生成，用于主播审核时填写';

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('attestation_time', '认证审核时间', '应用设置', '0', 0, 2, '', '', '审核失败后下次可申请的时间（单位：秒）');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('top_weight', '置顶权重值', '应用设置', '1', 0, 2, '', '', '设置视频置顶时增加的权重(单位：亿)');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('refund_explain', '提现说明', '提现设置', '', 3, 100, '', '', '');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('speak_level', '发言等级', '应用设置', '0', 0, 2, '', '', '设置多少级才可以发言');

CREATE TABLE `%DB_PREFIX%video_classified` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) NOT NULL COMMENT '分类名称',
  `is_effect` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效 1-有效 0-无效',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '从大到小排',
  PRIMARY KEY (`id`),
  KEY `idx_vc_001` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='分类表';

ALTER TABLE `%DB_PREFIX%video`
ADD COLUMN `classified_id`  int(11) NOT NULL DEFAULT 0 COMMENT '分类id';

ALTER TABLE `%DB_PREFIX%video_history`
ADD COLUMN `classified_id`  int(11) NOT NULL DEFAULT 0 COMMENT '分类id';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `classified_id`  int(11) NOT NULL DEFAULT 0 COMMENT '分类id';

ALTER TABLE `%DB_PREFIX%user_refund`
ADD COLUMN `confirm_cash_ip`  varchar(255) NOT NULL  COMMENT '确认提现操作IP';

UPDATE `%DB_PREFIX%conf` SET `value`='2.32' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%login_log`
MODIFY COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID' FIRST ;

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('forced_upgrade', '客户端是否强制升级', 'APP版本管理', '0', 4, 99, '0,1', '否,是', '开启强制升级，不升级无法进入直播间 0:否;1:是');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('forced_upgrade_tips', '强制升级', 'APP版本管理', '请升级后,观看视频【我的==>设置==>检查版本】', 3, 100, '', '', '开启强制升级的提醒');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('agora_app_id', '声网AppID', '应用设置', '', 0, 111, '', '', '声网AppID');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('agora_app_certificate', '声网AppCertificate', '应用设置', '', 0, 112, '', '', '声网AppCertificate');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('agora_anchor_resolution', '主播分辨率', '应用设置', '0', 4, 113, '0,1,2,3', '240*424,360*640,480*848,720*1280', '主播分辨率');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('agora_audience_resolution', '连麦观众分辨率', '应用设置', '0', 4, 114, '0,1,2,3', '180*320,240*424,360*640,480*848', '连麦观众分辨率');

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `allinpay_user_id`  VARCHAR (20) NOT NULL COMMENT '通联支付的用户ID(在通联网站的注册的userID)';

UPDATE `%DB_PREFIX%conf` SET `value`='2.33' WHERE (`name`='DB_VERSION');


UPDATE `%DB_PREFIX%m_config` SET `title`='腾讯云直播appid', `desc`='腾讯云直播APP_ID' WHERE (`code`='vodset_app_id');

UPDATE `%DB_PREFIX%m_config` SET `sort`='1',`desc`='腾讯云云通信SdkAppId' WHERE (`code`='tim_sdkappid');
UPDATE `%DB_PREFIX%m_config` SET `sort`='2',`desc`='腾讯云云通信账号管理员' WHERE (`code`='tim_identifier');
UPDATE `%DB_PREFIX%m_config` SET `sort`='3',`desc`='腾讯云云通信accountType' WHERE (`code`='tim_account_type');

UPDATE `%DB_PREFIX%m_config` SET `sort`='10',`desc`='腾讯云直播管理推流防盗key' WHERE (`code`='qcloud_security_key');
UPDATE `%DB_PREFIX%m_config` SET `sort`='11',`desc`='腾讯云直播管理API鉴权key' WHERE (`code`='qcloud_auth_key');
UPDATE `%DB_PREFIX%m_config` SET `sort`='12',`desc`='腾讯云直播APP_ID' WHERE (`code`='vodset_app_id');
UPDATE `%DB_PREFIX%m_config` SET `sort`='13',`desc`='腾讯云直播bizid' WHERE (`code`='qcloud_bizid');

UPDATE `%DB_PREFIX%m_config` SET `title`='云API帐户SecretId', `sort`='37',`desc`='腾讯【云API帐户SecretId】' WHERE (`code`='qcloud_secret_id');
UPDATE `%DB_PREFIX%m_config` SET `title`='云API密钥SecretKey', `sort`='37',`desc`='腾讯【云API密钥SecretKey】' WHERE (`code`='qcloud_secret_key');
UPDATE `%DB_PREFIX%m_config` SET `sort`='38',`desc`='保存视频（可用于回播）;0:否;1:是' WHERE (`code`='has_save_video');
UPDATE `%DB_PREFIX%m_config` SET `sort`='38',`desc`='清晰度越高,流量费用越高' WHERE (`code`='video_resolution_type');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('open_usersig_cache', '强制更新usersig', '腾讯直播', '0', 4, 4, '0,1', '否,是', '开启强制更新usersig缓存，0关闭 1开启 默认不开启');

INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'mission_switch', '每日在线任务开关', '基础配置', '1', '4', '0', '0,1', '关,开', '');
INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'mission_money', '每日在线任务每次领取金额', '基础配置', '10', '0', '0', null, null, '每日在线任务每次领取金额');
INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'mission_max_times', '每日在线任务最大领取次数', '基础配置', '10', '0', '0', null, null, '每日在线任务最大领取次数');
INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'mission_time', '每日在线任务每次领取间隔', '基础配置', '600', '0', '0', null, null, '每日在线任务每次领取间隔，单位秒');

INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'mission_name', '每日在线任务奖励标题', '基础配置', '免费领取10钻石', '0', '0', null, null, '每日在线任务奖励标题');
INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'mission_desc', '每日在线任务奖励说明', '基础配置', '等级越高免费领取钻石越多', '0', '0', null, null, '每日在线任务奖励说明');
DELETE FROM `%DB_PREFIX%m_config` WHERE `code` = 'mission_desc';
DELETE FROM `%DB_PREFIX%m_config` WHERE `code` = 'mission_name';
DELETE FROM `%DB_PREFIX%m_config` WHERE `code` = 'mission_time';
DELETE FROM `%DB_PREFIX%m_config` WHERE `code` = 'mission_max_times';
DELETE FROM `%DB_PREFIX%m_config` WHERE `code` = 'mission_money';

CREATE TABLE `%DB_PREFIX%mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '任务标题',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `money` int(255) DEFAULT NULL COMMENT '奖励数',
  `time` int(11) DEFAULT NULL COMMENT '间隔时间',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `is_effect` tinyint(4) DEFAULT NULL COMMENT '是否有效',
  `is_order` tinyint(4) DEFAULT NULL COMMENT '是否排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO `%DB_PREFIX%mission` VALUES ('1', '领取10钻石', '', '10', '0', '1', '1', '1');
INSERT INTO `%DB_PREFIX%mission` VALUES ('2', '领取20钻石', null, '20', '10', '2', '1', '1');
INSERT INTO `%DB_PREFIX%mission` VALUES ('3', '领取30钻石', null, '30', '20', '3', '1', '1');


INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'index_recommend', 'PC端首页推荐主播', 'PC端设置', '', 0, 12, NULL, NULL, 'PC端首页推荐主播,输入主播ID(多个主播以，分开，最多6个)，当主播直播时，推荐至首页直播');
INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'tourist_chat', '直播间游客发言', 'PC端设置', '', 4, 1,'0,1', '否，是', '是否开启游客发言，开启后未登录游客可在直播间发言');

UPDATE `%DB_PREFIX%conf` SET `value`='2.35' WHERE (`name`='DB_VERSION');

UPDATE `%DB_PREFIX%m_config` SET `desc`='手机端配置版本号格式(yyyymmddnn)(用接口初始化)' WHERE (`code`='init_version');

-- INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'robot_prop_num', '机器人送礼个数', '基础配置', '1', '0', '5', null, null, '机器人送礼个数');
-- INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'robot_prop_diamonds', '机器人每个礼物的价值', '基础配置', '50', '0', '5', null, null, '机器人送礼价值');
-- INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'robot_prop_total_diamonds', '机器人所有礼物价值', '基础配置', '200', '0', '5', null, null, '机器人送礼总价值');
-- INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'robot_prop_interval', '机器人送礼间隔', '基础配置', '300', '0', '5', null, null, '机器人送礼间隔时间，单位（秒）');
-- INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'robot_prop_real_interval', '机器人真人送礼间隔', '基础配置', '600', '0', '5', null, null, '机器人送礼与真人送礼间隔时间，单位（秒）');



UPDATE `%DB_PREFIX%conf` SET `value`='2.36' WHERE (`name`='DB_VERSION');

CREATE TABLE `%DB_PREFIX%key_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(255) NOT NULL COMMENT '手机端类型',
  `aes_key` text NOT NULL COMMENT '加密KEY',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除 1：是 0：否',
  `is_init` tinyint(1) NOT NULL COMMENT '是否打包填写 1是 、0否',
  `version` varchar(255) NOT NULL COMMENT '版本',
  `is_effect` varchar(255) NOT NULL COMMENT '是否有效',
  PRIMARY KEY (`id`),
  KEY `idx_v_001` (`is_delete`,`is_effect`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='加密KET列表';

UPDATE `%DB_PREFIX%conf` SET `value`='2.36' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%video_lianmai`
ADD COLUMN `channelid`  varchar(255) NOT NULL COMMENT '有些早期提供的API中直播码参数被定义为channel_id，新的API则称直播码为stream_id，仅历史原因而已';
ALTER TABLE `%DB_PREFIX%video_lianmai`
ADD COLUMN `play_rtmp`  varchar(255) NOT NULL COMMENT '小主播的rtmpAcc地址';
ALTER TABLE `%DB_PREFIX%video_lianmai`
ADD COLUMN `play_rtmp_acc`  varchar(255) NOT NULL COMMENT '';
ALTER TABLE `%DB_PREFIX%video_lianmai`
ADD COLUMN `v_play_rtmp_acc`  varchar(255) NOT NULL COMMENT '';

ALTER TABLE `%DB_PREFIX%video_lianmai_history`
ADD COLUMN `channelid`  varchar(255) NOT NULL COMMENT '有些早期提供的API中直播码参数被定义为channel_id，新的API则称直播码为stream_id，仅历史原因而已';
ALTER TABLE `%DB_PREFIX%video_lianmai_history`
ADD COLUMN `play_rtmp`  varchar(255) NOT NULL COMMENT '小主播的rtmpAcc地址';
ALTER TABLE `%DB_PREFIX%video_lianmai_history`
ADD COLUMN `play_rtmp_acc`  varchar(255) NOT NULL COMMENT '';
ALTER TABLE `%DB_PREFIX%video_lianmai_history`
ADD COLUMN `v_play_rtmp_acc`  varchar(255) NOT NULL COMMENT '';

INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'obs_download', '直播工具下载地址', 'PC端设置', null, '0', '12', null, null, '直播工具下载地址');

UPDATE `%DB_PREFIX%conf` SET `value`='2.37' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES
('open_visitors_login', '游客登录', '第三方帐户', '0', 4, 4, '0,1', '否,是', '是否开启游客登录 0:否;1:是');

ALTER TABLE `%DB_PREFIX%user`
MODIFY COLUMN `login_type`  tinyint(1) NOT NULL COMMENT '0：微信；1：QQ；2：手机；3：微博 ;4 : 游客登录';

UPDATE `%DB_PREFIX%conf` SET `value`='2.38' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` VALUES ('', 'search_change', 'APP搜索类型', '应用设置', '0', '4', '150', '0,1', '精确搜索,模糊搜索', '设置APP搜索类型 0精确 1模糊');

ALTER TABLE `%DB_PREFIX%mission`
ADD COLUMN `type`  tinyint DEFAULT 0 COMMENT '任务类型：0在线任务,1玩游戏任务,2打赏主播任务,3分享主播任务,4关注主播任务',
ADD COLUMN `target`  int DEFAULT 1 COMMENT '目标数量';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `no_ticket` int(11) NOT NULL COMMENT '特权机器人送出的亲贝（不可提现）';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `roboter` tinyint(1) NOT NULL COMMENT '0未开启机器人礼物账号特权，1开启机器人礼物账号特权';

UPDATE `%DB_PREFIX%conf` SET `value`='2.39' WHERE (`name`='DB_VERSION');

UPDATE `%DB_PREFIX%m_config` SET `value_scope`= '1,2,5', `title_scope`= '腾讯云直播,金山云,阿里云' WHERE (`code`='video_type');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('aliyun_access_key', 'Access Key ID', '阿里云', '', 0, 0, NULL, NULL, NULL);
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('aliyun_access_secret', 'Access Key Secret', '阿里云', '', 0, 0, NULL, NULL, NULL);
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('aliyun_region', '阿里云节点', '阿里云', 'cn-shanghai', 0, 0, NULL, NULL, NULL);
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('aliyun_private_key', '推流鉴权key', '阿里云', '', 0, 0, NULL, NULL, NULL);
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('aliyun_vhost', '加速域名', '阿里云', '', 3, 0, NULL, NULL, '一行一个域名');

CREATE TABLE `%DB_PREFIX%video_aliyun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vhost` varchar(255) NOT NULL,
  `stream_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `video_num` (`stream_id`),
  KEY `vhost` (`vhost`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

UPDATE `%DB_PREFIX%conf` SET `value`='2.40' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('account_ip', '后台登录IP', '基础配置', '', 3, 120, '', '', '后台允许登录的ip,每行填写一个ip');
UPDATE `%DB_PREFIX%m_config` SET `type`='3',`desc`= '后台允许登录的ip,每行填写一个ip' WHERE (`code`='account_ip' and `type`<>'3');


INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('account_mobile', '后台登录绑定手机号', '基础配置', '', 0, 121, '', '', '后台登录接收验证码手机');
ALTER TABLE `%DB_PREFIX%plugin`
ADD COLUMN `price`  int NOT NULL DEFAULT 0 COMMENT '价格';
CREATE TABLE `%DB_PREFIX%plugin_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin_id` int(11) NOT NULL COMMENT '插件id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `create_time` int(11) NOT NULL COMMENT '创建时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
UPDATE `%DB_PREFIX%user_log` SET user_id = podcast_id WHERE type = 7 AND user_id = 1;
UPDATE `%DB_PREFIX%conf` SET `value`='2.41' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('alipay_cache_time', '支付宝更换缓存', '应用设置', '60', 0, 130, '', '', '（秒）多支付宝账号更新间隔，时间最少不能低于60秒');

UPDATE `%DB_PREFIX%conf` SET `value`='2.41' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%video_aliyun`
ADD COLUMN `create_time`  int(11) NOT NULL AFTER `stream_id`;

INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'pc_is_open_recharge', 'PC充值开关', 'PC端设置', '1', 4, 0, '0,1', '否,是', '是否开启充值功能，1开启 0关闭');
INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'pc_is_open_exchange', 'PC提现开关', 'PC端设置', '1', 4, 0, '0,1', '否,是', '是否开启提现功能，1开启 0关闭');

insert into `%DB_PREFIX%role_module` values('','WarningMsg','警告列表',1,0);
insert into `%DB_PREFIX%role_node` values('','index','首页',1,0,0,(select id from `%DB_PREFIX%role_module` where module='WarningMsg'));
insert into `%DB_PREFIX%role_node` values('','add','添加',1,0,0,(select id from `%DB_PREFIX%role_module` where module='WarningMsg'));
insert into `%DB_PREFIX%role_node` values('','edit','编辑',1,0,0,(select id from `%DB_PREFIX%role_module` where module='WarningMsg'));
insert into `%DB_PREFIX%role_node` values('','foreverdelete','删除',1,0,0,(select id from `%DB_PREFIX%role_module` where module='WarningMsg'));
insert into `%DB_PREFIX%role_node` values('','set_effect','设置有效',1,0,0,(select id from `%DB_PREFIX%role_module` where module='WarningMsg'));

UPDATE `%DB_PREFIX%conf` SET `value`='2.42' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('send_msg_lv', '发言等级限制', '应用设置', '1', 0, 160, NULL, NULL, '会员等级>=当前设定的等级时,才能进行发言');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('is_show_identify_number', '是否需要身份验证', '应用设置', '1', 4, 160, '0,1', '否,是', '认证时是否需要输入身份证号码 0否 1是');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('identify_hold_example', '手持身份证示例图片', '应用设置', '', 2, 160, NULL, NULL, '手持身份证示例图片');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('show_follow_msg', '是否显示关注提示信息', '应用设置', '1', 4, 165, '0,1', '否,是', '是否发送用户关注提示信息到直播间 0否 1是');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('show_follow_msg_lv', '显示关注提示所需等级', '应用设置', '1', 0, 166, NULL, NULL, '会员等级>=当前设定的等级时,才显示关注信息到直播间');



INSERT INTO `%DB_PREFIX%m_config` (`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'is_change_name', '更改昵称付费', '基础配置', '0', '4', '0', '0,1', '关,开', '更改昵称付费开关，用户可免费更改一次昵称');
INSERT INTO `%DB_PREFIX%m_config` (`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'change_name', '更改昵称收费值', '基础配置', '100', '0', '0', '', '', '更改昵称收费值，用户可免费更改一次昵称（钻石）');

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `is_change_name` int(1) NOT NULL DEFAULT '0' COMMENT '是否修改过昵称（1=是，0否）';

INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'ksyun_app', 'Ksyun App', '金山云', '', '0', '0', null, null, null);
INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'ksyun_domain', 'Ksyun Domain', '金山云', '', '0', '0', null, null, null);
INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'ks3_accesskey', 'ks3 Accesskey', '金山云', '', '0', '0', null, null, null);
INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'ks3_secretkey', 'ks3 Secretkey', '金山云', '', '0', '0', null, null, null);

ALTER TABLE `%DB_PREFIX%video_aliyun`
ADD COLUMN `create_time` int(11) NOT NULL;

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('app_theme', '手机端主题', '应用设置', '0', 4, 180, '0,1', '默认,绿色', '手机端主题');

UPDATE `%DB_PREFIX%conf` SET `value`='2.43' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%video_lianmai`
ADD COLUMN `push_rtmp`  varchar(255) NOT NULL COMMENT '小主播推流地址' AFTER `channelid`;

ALTER TABLE `%DB_PREFIX%video_lianmai_history`
ADD COLUMN `push_rtmp`  varchar(255) NOT NULL COMMENT '小主播推流地址' AFTER `channelid`;

UPDATE `%DB_PREFIX%conf` SET `value`='2.42' WHERE (`name`='DB_VERSION');

UPDATE `%DB_PREFIX%m_config` SET `desc`='家族收取主播收益的比例(如 10% 则填10)' WHERE (`code`='profit_ratio');

UPDATE `%DB_PREFIX%conf` SET `value`='2.42' WHERE (`name`='DB_VERSION');

CREATE TABLE `%DB_PREFIX%video_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id,也是房间room_id',
  `title` varchar(255) NOT NULL COMMENT '直播标题',
  `user_id` int(11) NOT NULL COMMENT '项目id',
  `live_in` tinyint(1) DEFAULT '0' COMMENT '是否直播中 1-直播中 0-已停止;2:正在创建直播;3:回看',
  `watch_number` int(11) DEFAULT '0' COMMENT '当前实时观看人数（实际,不含虚拟人数,不包含机器人)',
  `virtual_watch_number` int(10) NOT NULL DEFAULT '0' COMMENT '当前虚拟观看人数',
  `vote_number` int(11) DEFAULT '0' COMMENT '获得票数',
  `cate_id` int(11) NOT NULL DEFAULT '0' COMMENT '话题id',
  `province` varchar(20) NOT NULL COMMENT '省份',
  `city` varchar(20) NOT NULL COMMENT '城市',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `begin_time` int(11) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `end_date` date NOT NULL COMMENT '结束日期',
  `group_id` varchar(50) NOT NULL COMMENT '群组ID,通过create_group后返回的值;直播结束后解散群',
  `destroy_group_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1：未解散;0:已解散;其它为ErrorCode错码',
  `long_polling_key` varchar(255) NOT NULL COMMENT '通过create_group后返回的LongPollingKey值',
  `max_watch_number` int(10) NOT NULL DEFAULT '0' COMMENT '最大观看人数(每进来一人次加1）',
  `room_type` tinyint(1) NOT NULL COMMENT '房间类型 : 1私有群（Private）,0公开群（Public）,2聊天室（ChatRoom）,3互动直播聊天室（AVChatRoom）',
  `is_playback` tinyint(1) DEFAULT '0' COMMENT '是否可回放 0-否 ；1-是',
  `video_vid` varchar(255) NOT NULL COMMENT '视频地址',
  `monitor_time` datetime NOT NULL COMMENT '最后心跳监听时间；如果超过监听时间，则说明主播已经掉线了',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:删除;0:未删除;私有聊天或小于5分钟的视频，不保存',
  `robot_num` int(10) NOT NULL DEFAULT '0' COMMENT '聊天群中机器人数量',
  `robot_time` int(10) NOT NULL DEFAULT '0' COMMENT '添加机器人时间（每隔20秒左右加几个人）',
  `channelid` varchar(50) NOT NULL COMMENT '旁路直播,频道ID',
  `is_aborted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:被服务器异常终止结束(主要是心跳超时)',
  `is_del_vod` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:表示已经清空了,录制视频;0:未做清空操作',
  `online_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '主播在线状态;1:在线(默认); 0:离开',
  `tipoff_count` int(10) NOT NULL DEFAULT '0' COMMENT '举报次数',
  `private_key` varchar(32) NOT NULL COMMENT '私密直播key',
  `share_type` varchar(30) NOT NULL COMMENT '分享类型WEIXIN,WEIXIN_CIRCLE,QQ,QZONE,SINA',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '热门排序',
  `pai_id` int(11) NOT NULL DEFAULT '0' COMMENT '竞拍id',
  `sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别 0:未知, 1-男，2-女',
  `video_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:腾讯云互动直播;1:腾讯云直播',
  `sort_num` int(10) NOT NULL DEFAULT '0' COMMENT 'sort_init + share_count * 分享权重 + like_count * 点赞权重 + fans_count * 关注权重 + sort * 排序权重 + ticket(本场收到的印票) * 印票权重',
  `create_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:APP端创建的直播;1:PC端创建的直播',
  `max_robot_num` int(10) NOT NULL DEFAULT '0' COMMENT '默认最大机器人头像数',
  `share_count` int(10) NOT NULL DEFAULT '0' COMMENT '分享数',
  `like_count` int(10) NOT NULL DEFAULT '0' COMMENT '点赞数,每个用户只记录一次',
  `fans_count` int(10) NOT NULL DEFAULT '0' COMMENT '本场直播净添加的粉丝数即：被关注数，关注加1，取消减1',
  `sort_init` int(10) NOT NULL DEFAULT '0' COMMENT 'sort_init(初始排序权重) = (用户可提现印票：fanwe_user.ticket - fanwe_user.refund_ticket) * 保留印票权重+ 直播/回看[回看是：0; 直播：9000000000 直播,需要排在最上面 ]+ fanwe_user.user_level * 等级权重+ fanwe_user.fans_count * 当前有的关注数权重',
  `push_rtmp` varchar(255) NOT NULL COMMENT '推流地址',
  `play_flv` varchar(255) NOT NULL COMMENT '播放地址；当video_type=0时，记录：傍路直播地址',
  `play_rtmp` varchar(255) NOT NULL COMMENT '播放地址；当video_type=0时，记录：傍路直播地址',
  `play_mp4` varchar(255) NOT NULL COMMENT '播放地址；当video_type=0时，记录：傍路直播地址',
  `play_hls` varchar(255) NOT NULL COMMENT '播放地址；当video_type=0时，记录：傍路直播地址',
  `xpoint` decimal(10,6) NOT NULL DEFAULT '0.000000' COMMENT 'x座标(用来计算：附近)',
  `ypoint` decimal(10,6) NOT NULL DEFAULT '0.000000' COMMENT 'y座标(用来计算：附近)',
  `head_image` varchar(255) NOT NULL COMMENT '直播时，可自定义封面图; 如果不存在,则取会员头像',
  `thumb_head_image` varchar(255) NOT NULL COMMENT '模糊图片',
  `play_url` varchar(255) NOT NULL COMMENT '播放地址',
  `live_image` varchar(255) NOT NULL COMMENT '视频封面',
  `is_recommend` int(1) NOT NULL COMMENT '推荐视频 0不推荐、1推荐',
  `virtual_number` int(11) NOT NULL COMMENT '最大虚拟人数',
  `room_title` varchar(100) DEFAULT NULL COMMENT '直播间名称',
  `live_pay_time` int(11) NOT NULL COMMENT '开始收费时间',
  `is_live_pay` tinyint(1) NOT NULL COMMENT '是否收费模式  1是 0否',
  `live_fee` int(11) NOT NULL COMMENT '付费直播 收取多少费用； 每分钟收取多少钻石，主播端设置',
  `live_is_mention` tinyint(1) NOT NULL COMMENT '是否已经提档 1是、0否',
  `live_pay_count` tinyint(1) NOT NULL COMMENT '付费人数',
  `pay_room_id` int(11) NOT NULL COMMENT '付费直播的ID , 用于标示直播间付费 模式 ',
  `prop_table` varchar(255) NOT NULL DEFAULT 'fanwe_video_prop' COMMENT '直播礼物表',
  `live_pay_type` tinyint(1) NOT NULL DEFAULT '2' COMMENT '收费类型 0按时收费，1按场次收费,未开启收费模式,默认为2',
  `len_time` int(11) NOT NULL COMMENT '直播的时长',
  `is_concatvideo` tinyint(1) NOT NULL COMMENT '视频是否合并 0 未合并，1 已合并',
  `stick` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否置顶 0 不置顶 1 置顶',
  `classified_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类id',
  `tags` text NOT NULL COMMENT '标签',
  `video_status` tinyint(1) NOT NULL COMMENT '//视频当前状态, 0:无状态 、1：已保存、2：有分片、3：分片已合并、4：合并完成已删除分片，5：拉取完成',
  `vodtaskid` varchar(50) NOT NULL COMMENT '//任务id，用户根据此字段匹配服务端事件通知',
  `file_id` varchar(50) NOT NULL COMMENT '//腾讯云 的 视频ID',
  `source_url` varchar(255) NOT NULL COMMENT '//拉流原视频地址',
  PRIMARY KEY (`id`),
  KEY `idx_v_001` (`user_id`) USING BTREE,
  KEY `idx_v_003` (`live_in`) USING BTREE,
  KEY `idx_v_002` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='直播历史表';

INSERT INTO `%DB_PREFIX%video_check` (`id`, `title`, `user_id`, `live_in`, `watch_number`, `virtual_watch_number`, `vote_number`, `cate_id`, `province`, `city`, `create_time`, `begin_time`, `end_time`, `end_date`, `group_id`, `destroy_group_status`, `long_polling_key`, `max_watch_number`, `room_type`, `is_playback`, `video_vid`, `monitor_time`, `is_delete`, `robot_num`, `robot_time`, `channelid`, `is_aborted`, `is_del_vod`, `online_status`, `tipoff_count`, `private_key`, `share_type`, `sort`, `pai_id`, `sex`, `video_type`, `sort_num`, `create_type`, `max_robot_num`, `share_count`, `like_count`, `fans_count`, `sort_init`, `push_rtmp`, `play_flv`, `play_rtmp`, `play_mp4`, `play_hls`, `xpoint`, `ypoint`, `head_image`, `thumb_head_image`, `play_url`, `live_image`, `is_recommend`, `virtual_number`, `room_title`, `live_pay_time`, `is_live_pay`, `live_fee`, `live_is_mention`, `live_pay_count`, `pay_room_id`, `prop_table`, `live_pay_type`, `len_time`, `is_concatvideo`, `stick`, `classified_id`, `tags`, `video_status`, `vodtaskid`, `file_id`, `source_url`) VALUES (1, '新人直播', 167222, 0, 0, 0, 0, 740, '其他', '福州', 1472663047, 1472663047, 1472663149, '2016-9-1', '@TGS#aG6EBTBEO', 0, '', 23, 3, 0, '', '0000-0-0 00:00:00', 0, 23, 0, '9896587163584396075', 0, 0, 1, 0, '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', 0.000000, 0.000000, './public/attachment/test/noavatar_2.JPG', '', './public/flash/2.mp4', '', 0, 0, '', 0, 0, 0, 0, 0, 0, 'fanwe_video_prop', 2, 0, 0, 0, 0, '', 0, '', '', '');
INSERT INTO `%DB_PREFIX%video_check` (`id`, `title`, `user_id`, `live_in`, `watch_number`, `virtual_watch_number`, `vote_number`, `cate_id`, `province`, `city`, `create_time`, `begin_time`, `end_time`, `end_date`, `group_id`, `destroy_group_status`, `long_polling_key`, `max_watch_number`, `room_type`, `is_playback`, `video_vid`, `monitor_time`, `is_delete`, `robot_num`, `robot_time`, `channelid`, `is_aborted`, `is_del_vod`, `online_status`, `tipoff_count`, `private_key`, `share_type`, `sort`, `pai_id`, `sex`, `video_type`, `sort_num`, `create_type`, `max_robot_num`, `share_count`, `like_count`, `fans_count`, `sort_init`, `push_rtmp`, `play_flv`, `play_rtmp`, `play_mp4`, `play_hls`, `xpoint`, `ypoint`, `head_image`, `thumb_head_image`, `play_url`, `live_image`, `is_recommend`, `virtual_number`, `room_title`, `live_pay_time`, `is_live_pay`, `live_fee`, `live_is_mention`, `live_pay_count`, `pay_room_id`, `prop_table`, `live_pay_type`, `len_time`, `is_concatvideo`, `stick`, `classified_id`, `tags`, `video_status`, `vodtaskid`, `file_id`, `source_url`) VALUES (2, '新人直播', 167222, 0, 0, 0, 0, 740, '其他', '福州市', 1472663136, 1472663136, 1472663151, '2016-9-1', '@TGS#aALFBTBEA', 0, '', 24, 3, 0, '', '0000-0-0 00:00:00', 0, 24, 0, '9896587163584396381', 0, 0, 1, 0, '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', 0.000000, 0.000000, './public/attachment/test/noavatar_2.JPG', '', './public/flash/3.mp4', '', 0, 0, '', 0, 0, 0, 0, 0, 0, 'fanwe_video_prop', 2, 0, 0, 0, 0, '', 0, '', '', '');
INSERT INTO `%DB_PREFIX%video_check` (`id`, `title`, `user_id`, `live_in`, `watch_number`, `virtual_watch_number`, `vote_number`, `cate_id`, `province`, `city`, `create_time`, `begin_time`, `end_time`, `end_date`, `group_id`, `destroy_group_status`, `long_polling_key`, `max_watch_number`, `room_type`, `is_playback`, `video_vid`, `monitor_time`, `is_delete`, `robot_num`, `robot_time`, `channelid`, `is_aborted`, `is_del_vod`, `online_status`, `tipoff_count`, `private_key`, `share_type`, `sort`, `pai_id`, `sex`, `video_type`, `sort_num`, `create_type`, `max_robot_num`, `share_count`, `like_count`, `fans_count`, `sort_init`, `push_rtmp`, `play_flv`, `play_rtmp`, `play_mp4`, `play_hls`, `xpoint`, `ypoint`, `head_image`, `thumb_head_image`, `play_url`, `live_image`, `is_recommend`, `virtual_number`, `room_title`, `live_pay_time`, `is_live_pay`, `live_fee`, `live_is_mention`, `live_pay_count`, `pay_room_id`, `prop_table`, `live_pay_type`, `len_time`, `is_concatvideo`, `stick`, `classified_id`, `tags`, `video_status`, `vodtaskid`, `file_id`, `source_url`) VALUES (3, '新人直播', 167222, 0, 0, 0, 0, 740, '其他', '福州', 1472663779, 1472663779, 1472663848, '2016-9-1', '639857', 1, '', 22, 3, 0, '', '0000-0-0 00:00:00', 0, 22, 0, '9896587163584398658', 0, 0, 1, 0, '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', 0.000000, 0.000000, './public/attachment/test/noavatar_2.JPG', '', './public/flash/5.mp4', '', 0, 0, '', 0, 0, 0, 0, 0, 0, 'fanwe_video_prop', 2, 0, 0, 0, 0, '', 0, '', '', '');
INSERT INTO `%DB_PREFIX%video_check` (`id`, `title`, `user_id`, `live_in`, `watch_number`, `virtual_watch_number`, `vote_number`, `cate_id`, `province`, `city`, `create_time`, `begin_time`, `end_time`, `end_date`, `group_id`, `destroy_group_status`, `long_polling_key`, `max_watch_number`, `room_type`, `is_playback`, `video_vid`, `monitor_time`, `is_delete`, `robot_num`, `robot_time`, `channelid`, `is_aborted`, `is_del_vod`, `online_status`, `tipoff_count`, `private_key`, `share_type`, `sort`, `pai_id`, `sex`, `video_type`, `sort_num`, `create_type`, `max_robot_num`, `share_count`, `like_count`, `fans_count`, `sort_init`, `push_rtmp`, `play_flv`, `play_rtmp`, `play_mp4`, `play_hls`, `xpoint`, `ypoint`, `head_image`, `thumb_head_image`, `play_url`, `live_image`, `is_recommend`, `virtual_number`, `room_title`, `live_pay_time`, `is_live_pay`, `live_fee`, `live_is_mention`, `live_pay_count`, `pay_room_id`, `prop_table`, `live_pay_type`, `len_time`, `is_concatvideo`, `stick`, `classified_id`, `tags`, `video_status`, `vodtaskid`, `file_id`, `source_url`) VALUES (4, '新人直播', 167222, 0, 0, 0, 0, 740, '其他', '福州市', 1472629428, 1472629428, 1472629706, '2016-8-31', '639863', 1, '', 22, 3, 0, '', '0000-0-0 00:00:00', 0, 22, 0, '9896587163584309061', 0, 0, 1, 0, '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', 0.000000, 0.000000, './public/attachment/test/noavatar_2.JPG', '', './public/flash/1.mp4', '', 0, 0, '', 0, 0, 0, 0, 0, 0, 'fanwe_video_prop', 2, 0, 0, 0, 0, '', 0, '', '', '');
INSERT INTO `%DB_PREFIX%video_check` (`id`, `title`, `user_id`, `live_in`, `watch_number`, `virtual_watch_number`, `vote_number`, `cate_id`, `province`, `city`, `create_time`, `begin_time`, `end_time`, `end_date`, `group_id`, `destroy_group_status`, `long_polling_key`, `max_watch_number`, `room_type`, `is_playback`, `video_vid`, `monitor_time`, `is_delete`, `robot_num`, `robot_time`, `channelid`, `is_aborted`, `is_del_vod`, `online_status`, `tipoff_count`, `private_key`, `share_type`, `sort`, `pai_id`, `sex`, `video_type`, `sort_num`, `create_type`, `max_robot_num`, `share_count`, `like_count`, `fans_count`, `sort_init`, `push_rtmp`, `play_flv`, `play_rtmp`, `play_mp4`, `play_hls`, `xpoint`, `ypoint`, `head_image`, `thumb_head_image`, `play_url`, `live_image`, `is_recommend`, `virtual_number`, `room_title`, `live_pay_time`, `is_live_pay`, `live_fee`, `live_is_mention`, `live_pay_count`, `pay_room_id`, `prop_table`, `live_pay_type`, `len_time`, `is_concatvideo`, `stick`, `classified_id`, `tags`, `video_status`, `vodtaskid`, `file_id`, `source_url`) VALUES (5, '新人直播', 167222, 0, 0, 40, 0, 740, '其他', '福州', 1472663301, 1472663301, 1472663409, '2016-9-1', '639858', 1, '', 67, 3, 0, '', '0000-0-0 00:00:00', 0, 26, 0, '9896587163584396075', 0, 0, 1, 0, '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', 0.000000, 0.000000, './public/attachment/test/noavatar_2.JPG', '', './public/flash/4.mp4', '', 0, 0, '', 0, 0, 0, 0, 0, 0, 'fanwe_video_prop', 2, 0, 0, 0, 0, '', 0, '', '', '');

UPDATE `%DB_PREFIX%conf` SET `value`='2.44' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('family_profit_platform', '是否平台支付家族收益', '应用设置', '0', 4, 170, '0,1', '否,是', '家族长收益是否由平台支付 0否 1是；由平台支付则不从家族的主播进行抽成');

INSERT INTO `%DB_PREFIX%role_module` (`module`, `name`, `is_effect`, `is_delete`) VALUES ('Indexs', '网站数据统计', '1', '0');
INSERT INTO `%DB_PREFIX%role_module` (`module`, `name`, `is_effect`, `is_delete`) VALUES ('Index', '快速导航', '1', '0');
INSERT INTO `%DB_PREFIX%role_node` (`action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES ('main', '导航信息', '1', '0', '0', (select id from `fanwe_role_module` where module='Index'));
INSERT INTO `%DB_PREFIX%role_node` (`action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES ('statistics', '网站统计信息', '1', '0', '0', (select id from `fanwe_role_module` where module='Indexs'));

INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'pc_live_fee', 'PC端付费直播收费', '付费直播配置', '', '0', '12', null, null, '(钻石)PC付费直播默认值，需不低于按场付费最低收费，不高于按场付费最高收费');

ALTER TABLE `%DB_PREFIX%user`
MODIFY COLUMN `nick_name`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称';

ALTER TABLE `%DB_PREFIX%user`
MODIFY COLUMN `signature`  text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '个性签名';

UPDATE `%DB_PREFIX%conf` SET `value`='2.45' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('is_prop_notify', '是否进行大礼物赠送的全服飞屏通告', '应用设置', '1', 4, 181, '0,1', '否,是', '赠送大礼物时是否要进行全服通告 0否 1是');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('private_letter_lv', '私信等级限制', '应用设置', '1', 0, 182, NULL, NULL, '等级>=当前设定的等级时,才能发送私信');

UPDATE `%DB_PREFIX%conf` SET `value`='2.491' WHERE (`name`='DB_VERSION');

UPDATE `%DB_PREFIX%m_config` SET `group_id`='第三方帐户', `sort`='5' WHERE (`code`='open_visitors_login');

UPDATE `%DB_PREFIX%m_config` SET `group_id`='排序权重', `sort`='21',`val`='1' WHERE (`code`='top_weight');

UPDATE `%DB_PREFIX%conf` SET `value`='2.492' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%user_log` ADD COLUMN `contribution_id` int(11) default 0 COMMENT '公会贡献成员ID';

ALTER TABLE `%DB_PREFIX%video_prop` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201610` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201611` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201612` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201701` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201702` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201703` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201704` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201705` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201706` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201707` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201708` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201709` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201710` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201711` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201712` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201801` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201802` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201803` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';
ALTER TABLE `%DB_PREFIX%video_prop_201804` ADD COLUMN `is_private` int(4) default 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信';

UPDATE `%DB_PREFIX%conf` SET `value`='2.493' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%distribution_log`
ADD COLUMN `diamonds`  decimal(13,2) NOT NULL COMMENT '邀请奖励的钻石';

ALTER TABLE `%DB_PREFIX%distribution_log`
ADD COLUMN `type`  tinyint(1) NOT NULL COMMENT '功能类型 0 分销功能 1邀请奖励';

INSERT INTO `%DB_PREFIX%m_config` (`code`, `val`, `type`, `sort`) VALUES ('reward_point_diamonds', '0', '0', '90');
UPDATE `%DB_PREFIX%m_config` SET `title`='邀请奖励钻石', `group_id`='分享设置', `desc`='邀请奖励功能 0代表关闭；大于0的整数代表开启,并且奖励对应数量的钻石' WHERE (`code`='reward_point_diamonds');

UPDATE `%DB_PREFIX%conf` SET `value`='2.494' WHERE (`name`='DB_VERSION');


UPDATE `%DB_PREFIX%conf` SET `value`='2.495' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('is_no_light', '是否关闭点赞', '应用设置', '0', 4, 190, '0,1', '否,是', '直播页面是否关闭点赞功能');

ALTER TABLE `%DB_PREFIX%prop`
ADD COLUMN `gif_gift_show_style` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'GIF礼物模式 0:按像素显示模式 1:全屏显示模式 2:至少两条边贴边模式';

UPDATE `%DB_PREFIX%conf` SET `value`='2.496' WHERE (`name`='DB_VERSION');

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `is_replace_qq`  tinyint(1) NOT NULL COMMENT '是否过更换QQ头像 ，0 否；1是';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `is_replace_wx`  tinyint(1) NOT NULL COMMENT '是否过更换微信头像 ，0 否；1是';

UPDATE `%DB_PREFIX%conf` SET `value`='2.497' WHERE (`name`='DB_VERSION');


CREATE TABLE `%DB_PREFIX%weibo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '动态ID',
  `user_id` int(11) NOT NULL COMMENT '产生动态的用户UID',
  `type` char(50) NOT NULL COMMENT 'imagetext 图文 red_photo 红包图片 weixin 出售微信  video 视频动态  goods 商品 photo 写真',
  `content` varchar(255) NOT NULL COMMENT '文字内容',
  `photo_image` varchar(255) NOT NULL COMMENT '写真封面',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '售价',
  `sale_num` int(11) NOT NULL DEFAULT '0' COMMENT '购买数量',
  `data` text NOT NULL COMMENT '链接序列化，存储 图片列表和视频',
  `create_time` datetime NOT NULL COMMENT '产生时间戳',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0为下架 1为上架',
  `from` tinyint(2) NOT NULL DEFAULT '0' COMMENT '客户端类型，0：网站；1：手机网页版；2：android；3：iphone',
  `comment_count` int(10) NOT NULL DEFAULT '0' COMMENT '评论数',
  `repost_count` int(10) NOT NULL DEFAULT '0' COMMENT '分享数',
  `video_count` int(10) NOT NULL DEFAULT '0' COMMENT '视频点击数',
  `red_count` int(10) NOT NULL DEFAULT '0' COMMENT '红包数量',
  `tipoff_count` int(10) NOT NULL DEFAULT '0' COMMENT '被举报次数',
  `comment_all_count` int(10) NOT NULL DEFAULT '0' COMMENT '全部评论数目',
  `digg_count` int(11) NOT NULL DEFAULT '0' COMMENT '点赞数',
  `is_repost` int(2) NOT NULL DEFAULT '0' COMMENT '是否转发 0-否  1-是',
  `is_audit` int(2) NOT NULL DEFAULT '1' COMMENT '是否已审核 0-未审核 1-已审核',
  `xpoint` varchar(25) NOT NULL DEFAULT '0' COMMENT '纬度',
  `ypoint` varchar(25) NOT NULL DEFAULT '0' COMMENT '经度',
  `address` varchar(255) NOT NULL COMMENT '发布地址',
  `province` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `is_recommend` tinyint(2) DEFAULT '1',
  `recommend_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '推荐时间',
  `sort_num` int(11) NOT NULL DEFAULT '0' COMMENT '排序权重',
  `is_top` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶 0 未置顶 1置顶',
  PRIMARY KEY (`id`),
  KEY `is_del` (`status`,`create_time`) USING BTREE,
  KEY `uid` (`user_id`,`status`,`create_time`) USING BTREE
) COMMENT='动态列表';

CREATE TABLE `%DB_PREFIX%weibo_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，评论编号',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型 1-评论 2- 点赞',
  `weibo_id` int(11) NOT NULL DEFAULT '0' COMMENT '评论的微博',
  `weibo_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '微博会员ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '评论者编号',
  `content` text NOT NULL COMMENT '评论内容',
  `to_comment_id` int(11) NOT NULL DEFAULT '0' COMMENT '被回复的评论的编号',
  `to_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '被回复的评论的作者的编号',
  `data` text NOT NULL COMMENT '所评论的内容的相关参数（序列化存储）',
  `create_time` datetime NOT NULL COMMENT '评论发布的时间',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '标记删除（0：没删除，1：已删除）',
  `is_audit` tinyint(1) DEFAULT '1' COMMENT '是否已审核 0-未审核 1-已审核',
  `storey` int(11) DEFAULT '0' COMMENT '评论绝对楼层',
  `client_ip` char(15) DEFAULT NULL,
  `client_port` char(5) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT '0' COMMENT '读取时间',
  PRIMARY KEY (`comment_id`)
) COMMENT='评论和回复';

CREATE TABLE `%DB_PREFIX%weibo_distribution_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `from_user_id` int(11) NOT NULL COMMENT '用户ID',
  `to_user_id` int(11) NOT NULL COMMENT '获得抽成的 用户ID',
  `create_date` date NOT NULL COMMENT '日期字段,按日期归档',
  `weibo_money` double(20,4) DEFAULT '0.0000' COMMENT '动态金额',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `create_ym` varchar(12) NOT NULL COMMENT '年月 如:201610',
  `create_d` tinyint(2) NOT NULL COMMENT '日',
  `create_w` tinyint(2) NOT NULL COMMENT '周',
  `memo` varchar(50) NOT NULL COMMENT '消费描述',
  `type` tinyint(1) DEFAULT NULL,
  `type_cate` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_1` (`to_user_id`,`from_user_id`,`weibo_money`) USING BTREE
) ;

ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weibo_count int(11) NOT NULL DEFAULT '0' COMMENT '动态数';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weibo_sort_num int(11) NOT NULL DEFAULT '0' COMMENT '动态权重';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weibo_recommend_weight int(11) NOT NULL DEFAULT '0' COMMENT '推荐权重';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weixin_account varchar(100) NULL COMMENT '微信账号';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weixin_price decimal(10,2) NOT NULL DEFAULT '0' COMMENT '微信价格';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN xpoint varchar(50) NOT NULL DEFAULT '0' COMMENT '经度' , ADD COLUMN ypoint varchar(50) NOT NULL DEFAULT '0' COMMENT '纬度';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN show_image text NOT NULL COMMENT '展示图片列表';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weibo_refund_money decimal(20,2) NOT NULL DEFAULT 0 COMMENT '已提现金额' ;
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weibo_money decimal(20,2) NOT NULL DEFAULT 0 COMMENT '主播获得的金额';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN tipoff_count int(11) NOT NULL DEFAULT '0' COMMENT '被举报的次数';
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weibo_photo_img varchar(255) NOT NULL COMMENT '会员中心海报' ;
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weibo_chat_price decimal(10,2) NOT NULL DEFAULT 0 COMMENT '聊天价格' ;
ALTER TABLE `%DB_PREFIX%user` ADD COLUMN weixin_account_time datetime NULL COMMENT '微信更新时间' ;
ALTER TABLE `%DB_PREFIX%tipoff` ADD COLUMN weibo_id int(11) NOT NULL DEFAULT 0 COMMENT '被举报的动态ID';

CREATE TABLE `%DB_PREFIX%qk_svideo_favor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL COMMENT '用户ID',
  `weibo_id` varchar(255) NOT NULL COMMENT '收藏的ID',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_001` (`user_id`,`weibo_id`)
) COMMENT='我收藏的小视屏';

ALTER TABLE `%DB_PREFIX%weibo` ADD COLUMN `unlike_count` int(11) NOT NULL DEFAULT '0' COMMENT '踩一下数' AFTER `comment_all_count`;
INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'diamonds_name', '钻石名称', '基础配置', '钻石', '0', '1', null, null, '名称');

insert into `%DB_PREFIX%role_module` values('','PropStatistics','消耗统计',1,0);
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (301, 'consume_statistics', '道具消耗统计', 1, 0, 0, (select id from `%DB_PREFIX%role_module` where module='PropStatistics'));
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (302, 'detail', '道具消耗明细', 1, 0, 0, (select id from `%DB_PREFIX%role_module` where module='PropStatistics'));
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (303, 'export_csv', '导出', 1, 0, 0, (select id from `%DB_PREFIX%role_module` where module='PropStatistics'));

INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (305, 'statistics', '公会长收益统计', 1, 0, 0, (select id from `%DB_PREFIX%role_module` where module='Society'));

insert into `%DB_PREFIX%role_module` values('','UserStatistics','私信收礼统计',1,0);
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (306, 'private_statistics', '统计列表', 1, 0, 0, (select id from `%DB_PREFIX%role_module` where module='UserStatistics'));
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (307, 'private_detail', '私信收礼明细', 1, 0, 0, (select id from `%DB_PREFIX%role_module` where module='UserStatistics'));
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (308, 'export_csv', '导出', 1, 0, 0, (select id from `%DB_PREFIX%role_module` where module='UserStatistics'));

CREATE TABLE `%DB_PREFIX%ban_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
	`user_id` int(11) NOT NULL COMMENT '用户ID',
	`ban_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '禁播类型  0 禁止单账号直播, 1 禁止同IP直播，2 禁止同设备直播',
	`ban_ip` varchar(50) NOT NULL COMMENT '禁播时用户登陆的IP',
	`apns_code` varchar(64) NOT NULL COMMENT '友盟消息推送服务对设备的唯一标识。Android的device_token是44位字符串, iOS的device-token是64位。',
  `ban_time` int(11) NOT NULL DEFAULT '0' COMMENT '设置禁播时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='禁播名单';

UPDATE `%DB_PREFIX%conf` SET `value`='2.498' WHERE (`name`='DB_VERSION');

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('view_page_size', '观众列表数量', '应用设置', '50', 0, 195, NULL, NULL, '直播间观众列表返回数量');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('is_classify', '是否需要强制选择分类', '应用设置', '0', 4, 191, '0,1', '否,是', '创建直播时是否需要强制选择分类');

UPDATE `%DB_PREFIX%conf` SET `value`='2.499' WHERE (`name`='DB_VERSION');

UPDATE `%DB_PREFIX%m_config` SET `desc`=' (%)IOS默认美颜度(10%则填10,不能小于 1)' WHERE (`code`='beauty_ios');
UPDATE `%DB_PREFIX%m_config` SET `desc`=' (%)ANDROID默认美颜度(10%则填10,不能小于 1)' WHERE (`code`='beauty_android');

INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'sts_video_limit', '小视频时间定义(秒)', '应用设置', '60', '0', '0', null, null, '小视频允许上传的最大秒数');
INSERT INTO `%DB_PREFIX%m_config` VALUES (null, 'svideo_must_authentication', '认证才能发布小视频', '基础配置', '0', '4', '88', '0,1', '否,是', '发布小视频,是否强制认证');

ALTER TABLE `%DB_PREFIX%payment_notice`
ADD COLUMN `pay_ip` varchar(50) DEFAULT NULL COMMENT '充值操作IP';

ALTER TABLE `%DB_PREFIX%weibo`
ADD COLUMN `first_image`  varchar(255) NOT NULL COMMENT '视频首帧截图';

CREATE TABLE `%DB_PREFIX%vehicle` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT '座驾名',
  `score` int(11) NOT NULL COMMENT '积分',
  `diamonds` int(11) NOT NULL COMMENT '消费钻石',
  `icon` varchar(255) NOT NULL COMMENT '图标',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序，从大到小;越大越靠前',
  `is_animated` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0:普通座驾 1:gif座驾 2:大型动画座驾 3：SVGA座驾',
  `is_effect` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:禁用;1:启用;默认启用',
  `gif_gift_show_style` tinyint(1) NOT NULL DEFAULT '0' COMMENT '动画模式 0:按像素显示模式 1:全屏显示模式 2:至少两条边贴边模式',
  `open_time` varchar(50) NOT NULL COMMENT '开通时间，使用英文逗号分开',
  `show_num` int(10) DEFAULT '0' COMMENT '单个直播间座驾显示次数 默认0 不限制',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='座驾列表';

CREATE TABLE `%DB_PREFIX%vehicle_animated` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `prop_id` int(10) NOT NULL COMMENT '道具ID',
  `url` varchar(255) NOT NULL COMMENT 'gif动画地址',
  `play_count` tinyint(1) NOT NULL DEFAULT '1' COMMENT '播放次数;play_count>1时duration无效',
  `delay_time` int(10) NOT NULL DEFAULT '0' COMMENT '延时播放时间;从第delay_time毫秒开始播放',
  `duration` int(10) NOT NULL DEFAULT '0' COMMENT '播放时长（毫秒）play_count>1时duration无效',
  `show_user` tinyint(1) NOT NULL DEFAULT '1' COMMENT '在顶部显示：用户名（曾送者）',
  `type` tinyint(1) NOT NULL DEFAULT '2' COMMENT '0：使用path路径；1：屏幕上部；2：屏幕中间；3：屏幕底部',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序，从大到小;越大越靠前',
  `path` text COMMENT '动画播放路径,格式待定;比如：从上到下；从左到右等',
  `animation_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT ' 0 gif类型 1 svga类型',
  `animation_width` int(10) NOT NULL DEFAULT '0' COMMENT '动画宽度',
  `animation_height` int(10) NOT NULL DEFAULT '0' COMMENT '动画高度',
  `gif_gift_show_style` tinyint(4) DEFAULT NULL COMMENT '动画模式 0:按像素显示模式 1:全屏显示模式 2:至少两条边贴边模式',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='坐骑gif动画轨迹';

CREATE TABLE `%DB_PREFIX%pay_vehicle_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `vehicle_id` int(11) NOT NULL COMMENT '座驾ID',
  `create_time` int(11) NOT NULL COMMENT '购买时间',
  `expire_time` int(11) NOT NULL COMMENT '到期时间',
  `money` decimal(13,2) NOT NULL COMMENT '座驾金额',
  `time` tinyint(5) NOT NULL COMMENT '座驾时长',
  `msg` varchar(150) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_1` (`user_id`,`vehicle_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='//座驾购买记录';

ALTER TABLE `%DB_PREFIX%user_log`
MODIFY COLUMN `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型 0表示充值 1表示提现 2赠送道具 3兑换印票 4分享获得印票 5登录赠送积分 6观看付费直播 7游戏收益 8有抽成公会收益 9分销收益 10无抽成公会收益 11平台收益 12公会操作13私信收入 14购买座驾';

INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `val`, `type`, `value_scope`, `title_scope`, `desc`) VALUES ('open_speaking_mobile', '发言手机号限制', '应用设置', '1', 4, '0,1', '否,是', '发言时是否禁止发送手机号码 0否 1是');

CREATE TABLE `%DB_PREFIX%music_classified` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) NOT NULL COMMENT '分类名称',
  `is_effect` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效 1-有效 0-无效',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '从大到小排',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_vc_001` (`title`) USING BTREE,
	KEY `idx_vc_002` (`id`,`title`,`is_effect`,`sort`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='音乐分类表';

CREATE TABLE `%DB_PREFIX%music_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `audio_id` varchar(255) NOT NULL COMMENT '音乐标识',
  `classified_id` varchar(255) NOT NULL COMMENT '音乐分类id',
  `audio_link` varchar(255) NOT NULL COMMENT '音乐下载地址',
  `lrc_link` varchar(255) NOT NULL COMMENT '歌词下载地址',
  `audio_name` varchar(255) NOT NULL COMMENT '歌曲名',
  `artist_name` varchar(255) DEFAULT NULL COMMENT '演唱者',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `time_len` int(10) NOT NULL DEFAULT '0' COMMENT '时长（秒）',
  `api_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'api类型;1:tingapi.ting.baidu.com',
  `lrc_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '歌词类型',
  `lrc_content` longtext COMMENT '歌词',
  `music_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '音乐类型  0-音乐，1-伴奏',
  `is_effect` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效 1-有效 0-无效',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '从大到小排',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_um_001` (`classified_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='//音乐列表';

UPDATE `%DB_PREFIX%conf` SET `is_effect` = 1 WHERE `name` = 'MAX_IMAGE_SIZE';

ALTER TABLE`%DB_PREFIX%weibo`
ADD COLUMN `comment_restrict` int(1) NOT NULL DEFAULT 1 COMMENT '评论限制 1允许所有人评论 2仅关注的人 3不允许任何人';

ALTER TABLE`%DB_PREFIX%weibo`
ADD COLUMN `title` varchar(50) NOT NULL DEFAULT 1 COMMENT '评论标题（@的好友）';

ALTER TABLE`%DB_PREFIX%weibo`
ADD COLUMN `video_width` varchar(10) NOT NULL DEFAULT '0' COMMENT '视频宽度';

ALTER TABLE`%DB_PREFIX%weibo`
ADD COLUMN `video_height` varchar(10) NOT NULL DEFAULT '0' COMMENT '视频高度';

CREATE TABLE `%DB_PREFIX%svideo_music` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) NOT NULL COMMENT '观众id',
  `audio_id` varchar(255) NOT NULL COMMENT '音乐标识',
  `audio_link` varchar(255) NOT NULL COMMENT '音乐下载地址',
  `lrc_link` varchar(255) NOT NULL COMMENT '歌词下载地址',
  `audio_name` varchar(255) NOT NULL COMMENT '歌曲名',
  `artist_name` varchar(255) DEFAULT NULL COMMENT '演唱者',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `time_len` int(10) NOT NULL DEFAULT '0' COMMENT '时长（秒）',
  `api_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'api类型;1:tingapi.ting.baidu.com',
  `lrc_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '歌词类型',
  `lrc_content` longtext COMMENT '歌词',
  `music_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '音乐类型  0-音乐，1-伴奏',
  `is_effect` tinyint(1) DEFAULT '1' COMMENT '是否有效 0 无效 1 有效',
  `classified_id` int(11) NOT NULL COMMENT '分类id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_um_001` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户的小视频音乐';

CREATE TABLE `%DB_PREFIX%share_profit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `from_user_id` int(11) NOT NULL COMMENT '用户ID',
  `to_user_id` int(11) NOT NULL COMMENT '获得抽成的 用户ID',
  `create_date` date NOT NULL COMMENT '日期字段,按日期归档',
  `ticket` decimal(13,2) NOT NULL COMMENT '抽取的印票',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `create_ym` varchar(12) NOT NULL COMMENT '年月 如:201610',
  `create_d` tinyint(2) NOT NULL COMMENT '日',
  `create_w` tinyint(2) NOT NULL COMMENT '周',
  `consume_money` decimal(13,2) NOT NULL COMMENT '充值金额',
  PRIMARY KEY (`id`),
  KEY `idx_1` (`to_user_id`,`from_user_id`,`ticket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `share_up_id` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `qr_code` varchar(255) NOT NULL COMMENT '分享二维码';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `qrcode_url` varchar(255) NOT NULL COMMENT '分享二维码地址';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `share_ticket` decimal(13,2) unsigned DEFAULT '0.00' COMMENT '分销收益';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `share_up_ticket` decimal(13,2) unsigned DEFAULT '0.00' COMMENT '给上级用户贡献的收益';

ALTER TABLE `%DB_PREFIX%user`
ADD COLUMN `vehicle_id` tinyint(5) NOT NULL DEFAULT 0 COMMENT '座驾id';

INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('open_vehicle_module', '是否开启座驾', '应用设置', '1', 4, 0, '0,1', '否,是', '是否开启座驾');
INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `val`, `type`,`desc`) VALUES ('share_distribution_ratio', '分销分成比例', '分销配置', 30,0,'(%)邀请好友，好友充值后分成比例 如 10% 则填10');
INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `val`, `type`,`desc`) VALUES ('svideo_shoot_time_min', '小视频拍摄最小时长', '小视频设置', 30,0,'小视频拍摄最小时长（秒）');
INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `val`, `type`,`desc`) VALUES ('svideo_shoot_time_max', '小视频拍摄最大时长', '小视频设置', 60,0,'小视频拍摄最大时长（秒）');
INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `val`, `type`,`desc`) VALUES ('publish_text_limit', '发布小视频文本内容最大字数', '小视频设置', 300,0,'小视频文本内容最大字数');
INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `type`, `sort`, `desc`) VALUES ('watermark_image', '小视频水印图片', '小视频设置', 2, 6, '小视频水印');
INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('watermark_position', '小视频位置', '小视频设置', '1', 4, 7, '1,2,3,4', '左上,右上,左下,右下', '小视频水印位置');

ALTER TABLE `%DB_PREFIX%prop_animated` ADD `gif_gift_show_style` tinyint(4) DEFAULT NULL COMMENT '动画模式 0:按像素显示模式 1:全屏显示模式 2:至少两条边贴边模式';
ALTER TABLE `%DB_PREFIX%prop_animated` add `animation_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT ' 0 gif类型 1 svga类型';
ALTER TABLE `%DB_PREFIX%prop_animated` add `animation_width` int(10)  NOT NULL DEFAULT '0' COMMENT '礼物宽度';
ALTER TABLE `%DB_PREFIX%prop_animated` add `animation_height` int(10)  NOT NULL DEFAULT '0' COMMENT '礼物高度';
ALTER TABLE `%DB_PREFIX%index_image` ADD `ad_time` tinyint(3) NOT NULL DEFAULT 4 NULL COMMENT '启动广告时长 默认 4秒';
ALTER TABLE `%DB_PREFIX%video` ADD `has_video_control` tinyint(1) NOT NULL DEFAULT 1 NULL COMMENT '是否显示视频控制进度条 0否 1是 默认1';
ALTER TABLE `%DB_PREFIX%video_history` ADD `has_video_control` tinyint(1) NOT NULL DEFAULT 1 NULL COMMENT '是否显示视频控制进度条 0否 1是 默认1';

INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `type`, `desc`) VALUES ('push_url', '腾讯云推流域名', '腾讯直播', 0, '腾讯云创建的推流域名');
INSERT INTO `%DB_PREFIX%m_config`(`code`, `title`, `group_id`, `type`, `desc`) VALUES ('play_url', '腾讯云播放域名', '腾讯直播', 0, '腾讯云创建的播放域名');

INSERT INTO `%DB_PREFIX%role_module` (`id`, `module`, `name`, `is_effect`, `is_delete`) VALUES (171, 'WeiboList', '小视频列表', 1, 0);
INSERT INTO `%DB_PREFIX%role_module` (`id`, `module`, `name`, `is_effect`, `is_delete`) VALUES (172, 'WeiboComment', '小视频评论', 1, 0);
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (950, 'index', '列表', 1, 0, 0, 171);
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (951, 'foreverdelete', '删除', 1, 0, 0, 171);
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (951, 'index', '列表', 1, 0, 0, 172);
INSERT INTO `%DB_PREFIX%role_node` (`id`, `action`, `name`, `is_effect`, `is_delete`, `group_id`, `module_id`) VALUES (951, 'delete', '删除', 1, 0, 0, 172);
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'tim_key', '腾讯云云通信秘钥', '腾讯直播', NULL, 0, 0, NULL, NULL, '云通信秘钥-2019.10.20后创建的云通信需填写');

INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'app_type', 'app模式', '小视频设置', '1', 4, 0, '0,1', '直播,小视频', 'app模式 0：普通直播模式 1：小视频模式');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'new_user_time', '新人注册时间', '小视频设置', '7', 0, 100, NULL, NULL, '天（多少天内）');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'new_user_number', '新人一页条数', '小视频设置', '0', 0, 101, NULL, NULL, '条');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'adv_number', '广告一页条数', '小视频设置', '1', 0, 102, NULL, NULL, '条');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'hot_number', '热门一页条数', '小视频设置', '1', 0, 103, NULL, NULL, '条');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'total_user_number', '个人队列总数量', '小视频设置', '10', 0, 104, NULL, NULL, '条');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'update_user_number', '个人队列少于多少条开始更新', '小视频设置', '8', 0, 105, NULL, NULL, '条');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'is_openweibotest', '是否开启小视频队列模式', '小视频设置', '1', 4, 0, '0,1', '否,是', NULL);
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'is_name_num', '是否限制纯数字昵称', '应用设置', '0', 4, 0, '0,1', '否,是', '选择是，用户昵称不能为纯数字（注册和修改昵称时有效）');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'third_party_beauty_type', '美颜设置', '应用设置', '1', 4, 0, '0,1', '不开启额外功能（默认腾讯美颜、滤镜),萌颜 (新增加 贴纸、美型）', '美颜设置 0 默认美颜 1萌颜');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'publish_svideo_level', '多少级才可以发布小视频', '小视频设置', '2', 0, 0, NULL, NULL, '用户等级大于等于该值，才可以发布小视频');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'open_burst', '是否开启连发礼物', '礼物设置', '1', 4, 0, '0,1', '否,是', '是否开启连发礼物（关闭后，APP不会显示连发配置）');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'open_host_mission', '是否开启主播每日任务', '主播每日任务', '1', 4, 0, '0,1', '否,是', '是否开启主播每日任务');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'reward_live_time', '直播时长', '主播每日任务', '120', 0, 1, '', '', '单场直播达到一定时间，奖励印票（时间单位：分钟）( 必须满足时长与票数 )');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'reward_ticket', '奖励数量', '主播每日任务', '1', 0, 3, '', '', '奖励的印票');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'reward_live_ticket', '直播票数', '主播每日任务', '1000', 0, 2, '', '', '单场直播获得票数，奖励蚁票 ( 必须满足时长与票数 )');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'history_mission_day', '可以领取多少天前的奖励', '主播每日任务', '1', 0, 0, '0', '', '设置天数，主播可以领取X天前已完成但未领取的奖励');
INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'reward_title', '主播任务标题', '主播每日任务', '每日任务', 0, 4, NULL, NULL, '主播任务标题');


SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE  `%DB_PREFIX%_goods_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `sort` int(255) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%award_config`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置名称',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `group_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组名称',
  `val` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置值',
  `type` tinyint(1) NOT NULL COMMENT '类型',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `value_scope` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值的范围',
  `title_scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应value_scope的中文解释',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '中奖礼物配置信息表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%award_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '中奖用户ID',
  `prop_id` int(11) NOT NULL COMMENT '礼物ID',
  `video_id` int(11) NOT NULL COMMENT '直播间ID',
  `group_id` int(11) NOT NULL COMMENT '聊天组ID',
  `award_pool` int(11) NOT NULL COMMENT '当前资金池资金总数',
  `award_ratio` int(11) NOT NULL COMMENT '转换可用资金比例',
  `usable_amount` int(11) NOT NULL COMMENT '实际可用奖金 = 理论可用金额-已用金额',
  `commission_charge_ratio` tinyint(2) NOT NULL COMMENT '平台手续比例',
  `bonus` int(11) NOT NULL COMMENT '中奖金额 ',
  `commission_charge` int(5) NOT NULL COMMENT '平台手续 (中奖金额*平台手续费)',
  `receive_bonus` int(11) NOT NULL COMMENT '实际到账奖金 = 中奖金额-手续费 ',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `create_date` date NOT NULL COMMENT '日期字段',
  `create_ym` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '月',
  `create_d` tinyint(2) NOT NULL COMMENT '日',
  `create_w` tinyint(2) NOT NULL COMMENT '周',
  `from_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '送礼物人IP',
  `ActionStatus` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息发送，请求处理的结果，OK表示处理成功，FAIL表示失败。',
  `ErrorInfo` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息发送，错误信息',
  `ErrorCode` int(10) NOT NULL COMMENT '错误码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '中奖记录表' ROW_FORMAT = Dynamic;

CREATE TABLE  `%DB_PREFIX%award_multiple`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `multiple` int(200) NOT NULL COMMENT '中奖的倍数',
  `probability` tinyint(3) NOT NULL COMMENT '中奖概率 (所有概率总数不能大于100%)',
  `is_effect` tinyint(1) NOT NULL COMMENT '是否有效 1有效 0无效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '中奖的倍数设置表' ROW_FORMAT = Dynamic;

CREATE TABLE  `%DB_PREFIX%banker_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '上庄ID',
  `video_id` int(11) NOT NULL COMMENT '直播间ID',
  `user_id` int(11) NOT NULL COMMENT '上庄用户ID',
  `coin` int(11) NOT NULL COMMENT '上庄金额',
  `status` int(11) NOT NULL COMMENT '上庄状态，1：申请上庄，2：取消上庄，3：正在上庄，4：下庄',
  `create_time` int(11) NOT NULL COMMENT '记录时间戳',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `video_id_status`(`video_id`, `status`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%banker_log_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '上庄ID',
  `video_id` int(11) NOT NULL COMMENT '直播间ID',
  `user_id` int(11) NOT NULL COMMENT '上庄用户ID',
  `coin` int(11) NOT NULL COMMENT '上庄金额',
  `status` int(11) NOT NULL COMMENT '上庄状态，1：申请上庄，2：取消上庄，3：正在上庄，4：下庄',
  `create_time` int(11) NOT NULL COMMENT '记录时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%bm_config`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置名称',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `group_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组名称',
  `val` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置值',
  `type` tinyint(1) NOT NULL COMMENT '类型',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `value_scope` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值的范围',
  `title_scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应value_scope的中文解释',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '百媚配置信息表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%bm_promoter`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '推广商名称',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `user_id` int(11) NOT NULL COMMENT '绑定的会员ID',
  `pid` int(11) NOT NULL COMMENT '父级ID',
  `child_count` int(11) NOT NULL COMMENT '子级个数',
  `is_effect` tinyint(1) NOT NULL COMMENT '是否有效：1是，0否',
  `status` tinyint(1) NOT NULL COMMENT '状态：0待审核，1通过，2未通过',
  `memo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `member_id` int(2) NOT NULL COMMENT '是否员工：-1是平台员工，0否，其他为归属的代理商id',
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账户登录的用户名：平台员工：\"admin_\"+id;其他员工：归属id_id',
  `bm_role_id` int(11) NOT NULL COMMENT '权限分组id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推广商列表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%bm_promoter_game_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bm_pid` int(11) NOT NULL DEFAULT 0 COMMENT '推广商id',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `game_id` int(11) NOT NULL DEFAULT 0 COMMENT '游戏id（类型）',
  `game_log_id` int(11) NOT NULL,
  `sum_bet` int(11) NOT NULL DEFAULT 0 COMMENT '下注金额',
  `sum_gain` int(11) NOT NULL DEFAULT 0 COMMENT '中奖金额',
  `sum_win` int(11) NOT NULL DEFAULT 0 COMMENT '游戏流水',
  `promoter_gain` int(11) NOT NULL DEFAULT 0 COMMENT '推广中心流水收入',
  `platform_gain` int(11) NOT NULL DEFAULT 0 COMMENT '平台流水收入',
  `user_gain` int(11) NOT NULL DEFAULT 0 COMMENT '会员收支',
  `gain` int(11) NOT NULL DEFAULT 0 COMMENT '我的收支',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '时间',
  `is_count` int(11) NOT NULL DEFAULT 0 COMMENT '是否结算',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%bm_qrcode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '二维码名称',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `user_id` int(11) NOT NULL COMMENT '归属的推广员',
  `img` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片地址',
  `p_user_id` int(11) NOT NULL DEFAULT 0 COMMENT '归属的代理商user_id',
  `is_effect` tinyint(1) NOT NULL DEFAULT 1 COMMENT '有效性标识',
  `promoter_id` int(11) NOT NULL COMMENT '添加者id',
  `qrcode_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邀请码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `sn_is_effect`(`qrcode_sn`, `is_effect`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%bm_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `promoter_id` int(11) NOT NULL DEFAULT 0 COMMENT '归属人的id',
  `is_effect` tinyint(1) NOT NULL COMMENT '有效性标识',
  `is_delete` tinyint(1) NOT NULL COMMENT '删除标识',
  `all_role_list` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最大权限列表，记录id数组的json',
  `role_list` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '拥有权限列表，记录id数组的json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%bm_role_node`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `action` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方法',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方法名',
  `is_effect` tinyint(1) NOT NULL COMMENT '有效性标识',
  `is_delete` tinyint(1) NOT NULL COMMENT '删除标识',
  `promoter_lv` int(11) NOT NULL COMMENT '归属的权限等级',
  `cate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `action`(`action`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%coin_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '游戏币id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `game_log_id` int(11) NOT NULL COMMENT '游戏记录id',
  `create_time` int(11) NOT NULL COMMENT '创建日期',
  `diamonds` int(11) NOT NULL COMMENT '记录金额',
  `account_diamonds` int(11) NOT NULL COMMENT '用户余额',
  `memo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '游戏币记录' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%courier`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单编号',
  `courier_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '物流单号',
  `courier_offic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流公司',
  `courier_details` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '物流信息详情',
  `view_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上次查看时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '物流信息表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%game_distribution`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分销记录id',
  `room_id` int(11) NOT NULL DEFAULT 0 COMMENT '直播间id',
  `game_log_id` int(11) NOT NULL DEFAULT 0 COMMENT '游戏id',
  `money` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT '总金额',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '分销人',
  `distreibution_money` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT '分销金额',
  `first_distreibution_id` int(11) NULL DEFAULT 0 COMMENT '一级分销人',
  `first_distreibution_money` int(11) NULL DEFAULT 0 COMMENT '一级分销金额',
  `second_distreibution_id` int(11) NULL DEFAULT 0 COMMENT '二级分销人',
  `second_distreibution_money` int(11) NULL DEFAULT 0 COMMENT '二级分销金额',
  `dec` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT 0 COMMENT '创建时间',
  `is_ticket` int(11) NULL DEFAULT 0 COMMENT '是否是印票',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%game_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `podcast_id` int(11) NOT NULL COMMENT '主播id',
  `long_time` int(11) NOT NULL DEFAULT 60 COMMENT '游戏时间',
  `game_id` int(11) NOT NULL COMMENT '游戏id',
  `create_time` int(11) NOT NULL COMMENT '发布时间',
  `create_date` datetime(0) NOT NULL COMMENT '发布时间（格式化）',
  `create_time_ymd` date NOT NULL COMMENT '发布时间（年月日）',
  `create_time_y` int(4) NOT NULL COMMENT '发布时间（年）',
  `create_time_m` int(2) NOT NULL COMMENT '发布时间（月）',
  `create_time_d` int(2) NOT NULL COMMENT '发布时间（日）',
  `bet` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'json格式下注情况,结算时候统计',
  `podcast_income` int(11) NULL DEFAULT NULL COMMENT '主播收益',
  `suit_patterns` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'json格式，记录牌型',
  `result` int(11) NULL DEFAULT 0 COMMENT '中奖结果：1/2/3，无结果，退款设置为-1',
  `game_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '游戏名称（冗余字段）',
  `status` int(11) NULL DEFAULT 1 COMMENT '1：进行，2：结束，',
  `income` int(11) NOT NULL DEFAULT 0 COMMENT '平台收入',
  `banker_id` int(11) NOT NULL DEFAULT 0 COMMENT '上庄玩家ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%game_log_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `podcast_id` int(11) NOT NULL COMMENT '主播id',
  `long_time` int(11) NOT NULL DEFAULT 60 COMMENT '游戏时间',
  `game_id` int(11) NOT NULL COMMENT '游戏id',
  `create_time` int(11) NOT NULL COMMENT '发布时间',
  `create_date` datetime(0) NOT NULL COMMENT '发布时间（格式化）',
  `create_time_ymd` date NOT NULL COMMENT '发布时间（年月日）',
  `create_time_y` int(4) NOT NULL COMMENT '发布时间（年）',
  `create_time_m` int(2) NOT NULL COMMENT '发布时间（月）',
  `create_time_d` int(2) NOT NULL COMMENT '发布时间（日）',
  `bet` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'json格式下注情况,结算时候统计',
  `podcast_income` int(11) NULL DEFAULT NULL COMMENT '主播收益',
  `suit_patterns` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'json格式，记录牌型',
  `result` int(11) NULL DEFAULT 0 COMMENT '中奖结果：1/2/3，无结果，退款设置为-1',
  `game_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '游戏名称（冗余字段）',
  `status` int(11) NULL DEFAULT 1 COMMENT '1：进行，2：结束，',
  `income` int(11) NOT NULL DEFAULT 0 COMMENT '平台收入',
  `banker_id` int(11) NOT NULL DEFAULT 0 COMMENT '上庄玩家ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%games`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `principal` int(11) NOT NULL COMMENT '主播开启游戏抵金',
  `is_effect` tinyint(1) NOT NULL COMMENT '有效性标识',
  `commission_rate` int(11) NOT NULL DEFAULT 50 COMMENT '主播佣金比例：50表示收益的50%最为佣金；收益金额=总金额-中奖返还；',
  `long_time` int(11) NOT NULL COMMENT '当轮时长，单位s',
  `rate` int(11) NOT NULL DEFAULT 0 COMMENT '干预系数，0-100,0表示无干预，全部随机结果、100表示完全干预，收益最大',
  `option` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '{\"option1\":1,\"option2\":2,\"option3\":3}' COMMENT '投注选项',
  `bet_option` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '[10,100,1000,10000]' COMMENT '投注金额',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '游戏描述',
  `class` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '游戏操作类',
  `player_num` int(4) NOT NULL DEFAULT 3,
  `ticket_rate` int(11) NOT NULL DEFAULT 100 COMMENT '主播收益印票转化率',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增字段',
  `user_id` int(11) NOT NULL COMMENT '主播ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品名称',
  `imgs` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片（JSON数据）',
  `imgs_details` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品详情图片',
  `price` decimal(20, 2) NOT NULL COMMENT '商品价钱',
  `pai_diamonds` decimal(20, 2) NOT NULL COMMENT '商品直播价格（钻石）',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品详情URL地址',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品描述',
  `is_delete` tinyint(1) NOT NULL COMMENT '商品状态 0为正常,1为删除（值为1时不在前端展示）',
  `kd_cost` decimal(20, 2) NOT NULL COMMENT '快递费用',
  `score` int(255) NOT NULL COMMENT '经验',
  `inventory` int(255) NOT NULL COMMENT '库存',
  `is_effect` int(1) NOT NULL COMMENT '商品状态 1为正常,0为删除（值为0时不在前端展示）',
  `sales` int(11) NOT NULL COMMENT '初始销售量',
  `number` int(11) NOT NULL COMMENT '初始售卖人数',
  `bz_diamonds` int(11) NOT NULL COMMENT '竞拍保证金',
  `jj_diamonds` int(11) NOT NULL COMMENT '竞拍加价幅度',
  `pai_time` float(11, 1) NOT NULL COMMENT '竞拍时间',
  `cate_id` int(11) NOT NULL DEFAULT 0 COMMENT '商品分类',
  `podcast_ticket` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '主播商品抽佣',
  `tags_id` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签ID(JSON格式)',
  `seen_num` int(11) NOT NULL COMMENT '看过人数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%goods_cate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id自增长，分类ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `is_effect` int(1) NOT NULL DEFAULT 1 COMMENT '分类是否有效  1有效 0无效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分类表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%goods_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '购物订单ID',
  `order_source` enum('remote','local') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'local' COMMENT '订单来源（local:本地 remote:远程）',
  `order_type` enum('shop','pai_goods','pai') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'shop' COMMENT '订单类型（shop:购物单 pai:竞拍单 pai_goods:实物竞拍）',
  `order_sn` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单编号(本地的系统生成，远程的第三方同步)',
  `order_status` tinyint(2) NOT NULL COMMENT '当前订单状态 1:待付款 2:待发货 3:待收货(主播确认约会)  4:已收货(观众确认约会)5:\r\n退款成功 6未付款 7结单',
  `no_refund` tinyint(1) NOT NULL COMMENT '是否允许退款 0是 1否是否允许退款 0是 1否，实物竞拍（remote+pai）不允许退款退货',
  `refund_buyer_status` tinyint(1) NOT NULL COMMENT '退款买方状态 0：无 1:退款中 2:退货中 3:退款成功 4:主动撤销退款 5:被动关\r\n闭',
  `refund_buyer_delivery` tinyint(1) NOT NULL COMMENT '退款买方配货状态 0：无 1:未发货 2:已发货， 虚拟商品不涉及该项',
  `refund_seller_status` tinyint(1) NOT NULL COMMENT '退款卖方状态 0：无 1:退款成功',
  `refund_platform` tinyint(1) NOT NULL COMMENT '退款平台申诉 0:无 1:申诉中(卖方) 2:申诉完成（结果同步到相关refund状态）',
  `number` int(11) NOT NULL COMMENT '该订单的商品总数量',
  `total_diamonds` decimal(10, 2) NOT NULL COMMENT '订单应付总金额(含运费，竞拍单为竞拍价)',
  `remote_total_diamonds` decimal(10, 2) NOT NULL COMMENT '第三方购物平台冗余的应付总额，本地订单该项为0',
  `remote_cost_diamonds` decimal(10, 2) NOT NULL COMMENT '购物平台的订单成本',
  `goods_diamonds` decimal(10, 2) NOT NULL COMMENT '订单中商品的金额（竞拍单直接为竞拍价）',
  `pay_diamonds` decimal(10, 2) NOT NULL COMMENT '已付金额',
  `podcast_ticket` decimal(10, 2) NOT NULL COMMENT '主播佣金主播佣金，pay_diamonds-cost_diamonds(成本，即结算给购物平台的钱)，\r\n虚拟竞拍无成本',
  `refund_diamonds` decimal(10, 2) NOT NULL COMMENT '退款金额',
  `freight_diamonds` decimal(10, 2) NOT NULL COMMENT '运费',
  `memo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单备注',
  `consignee` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人',
  `consignee_mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人手机号',
  `consignee_district` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人所在地行政地区信息,json格式\r\n{\r\n    \"province\":\"福建省\",\r\n\r\n\"city\":\"福州市\",\r\n    \"area\":\"鼓楼区\", //行政区\r\n    \"zip\":\"350001\", //邮编\r\n    \"lng\":\"xxxxxxxxxxx\" //经度\r\n\r\n\"lat\":\"xxxxxxxxxxx\" //纬度\r\n}',
  `consignee_address` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '详细地址',
  `create_time` int(11) NOT NULL COMMENT 'GMT+0 时间戳',
  `create_date` datetime(0) NOT NULL COMMENT 'GMT8时间',
  `create_time_ymd` date NOT NULL COMMENT 'GMT8日期',
  `create_time_y` int(4) NOT NULL,
  `create_time_m` int(2) NOT NULL,
  `create_time_d` int(2) NOT NULL,
  `podcast_id` int(11) NOT NULL COMMENT '主播人id',
  `viewer_id` int(11) NOT NULL COMMENT '购买人id',
  `pai_id` int(11) NOT NULL COMMENT '竞拍ID',
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `pay_time` datetime(0) NOT NULL COMMENT '付款时间 GMT+8时间',
  `refund_over_time` datetime(0) NOT NULL COMMENT '退款完成时间（格式化）',
  `order_status_time` int(11) NOT NULL COMMENT '状态更新时间戳',
  `delivery_time` datetime(0) NULL DEFAULT NULL COMMENT '发货时间',
  `refund_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '退款原因',
  `courier_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流单号',
  `courier_offic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流公司',
  `buy_type` int(1) NULL DEFAULT NULL COMMENT '0购买给自己 1购买给主播',
  `is_p` int(1) NOT NULL DEFAULT 0 COMMENT '是否是父订单  1 是  0否',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '父订单id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `order_sn_unk`(`order_sn`) USING BTREE,
  INDEX `order_type`(`order_source`) USING BTREE,
  INDEX `order_sn`(`order_sn`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%goods_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `sort` int(255) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%host_mission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NULL DEFAULT NULL COMMENT '直播id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '直播创建时间',
  `end_time` int(11) NULL DEFAULT NULL COMMENT '直播结束时间',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '任务状态 0 未完成 1已完成未领取 2已领取',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%index_image` MODIFY COLUMN `ad_time` tinyint(3) NULL DEFAULT NULL COMMENT '启动广告时长' AFTER `show_position`;

ALTER TABLE  `%DB_PREFIX%key_list` DROP INDEX `idx_v_001`;

CREATE TABLE  `%DB_PREFIX%membership_chain`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ancestral_id` int(11) NOT NULL COMMENT '祖先id(会员的上级...上N级)',
  `level` int(11) NULL DEFAULT NULL COMMENT '当前会员所处关键链的层次，顶端是0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ss`(`user_id`, `ancestral_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员关系链' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%men_yan_list`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '贴纸名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '展示图片',
  `download_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载地址',
  `is_effect` tinyint(1) NULL DEFAULT 1 COMMENT '是否有效 0无效 1有效',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `file_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '贴纸压缩包名称',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_num_iseffect`(`sort`, `is_effect`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%music_list` ADD COLUMN `audio_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '音乐封面' AFTER `sort`;

ALTER TABLE  `%DB_PREFIX%music_list` ADD COLUMN `weight` int(10) NULL DEFAULT 0 COMMENT '权重' AFTER `audio_image`;

CREATE TABLE  `%DB_PREFIX%pai_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `podcast_id` int(11) NOT NULL COMMENT '主播id',
  `podcast_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主播名称',
  `imgs` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片（JSON数据）',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签 (约会、逛街，以 、 隔开保存)  ',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '拍品名称',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '竞拍描述[虚拟]',
  `date_time` datetime(0) NOT NULL COMMENT '约会时间[虚拟]',
  `place` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '约会地点[虚拟]',
  `district` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `contact` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人[虚拟]',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系电话[虚拟]',
  `is_true` tinyint(4) NOT NULL COMMENT '0虚拟 1实物  ',
  `goods_id` int(11) NOT NULL COMMENT '商品ID (实物 此项不为空)',
  `bz_diamonds` int(11) NOT NULL COMMENT '竞拍保证金',
  `qp_diamonds` int(11) NOT NULL COMMENT '起拍价',
  `jj_diamonds` int(11) NOT NULL COMMENT '每次加价',
  `pai_time` decimal(4, 2) NOT NULL COMMENT '竞拍时长 （单位小时）',
  `pai_yanshi` int(2) NOT NULL COMMENT '每次竞拍延时（单位分）',
  `max_yanshi` int(2) NOT NULL COMMENT '最大延时(次)',
  `pai_less_time` int(2) NOT NULL COMMENT '少于几秒才会触发 更新now_yanshi事件',
  `now_yanshi` int(2) NOT NULL,
  `create_time` int(11) NOT NULL COMMENT '发布时间',
  `create_date` datetime(0) NOT NULL COMMENT '发布时间（格式化）',
  `create_time_ymd` date NOT NULL COMMENT '发布时间（年月日）',
  `create_time_y` int(4) NOT NULL COMMENT '发布时间（年）',
  `create_time_m` int(2) NOT NULL COMMENT '发布时间（月）',
  `create_time_d` int(2) NOT NULL COMMENT '发布时间（日）',
  `pai_nums` int(11) NOT NULL COMMENT '参与竞拍人次',
  `user_id` int(11) NOT NULL COMMENT '拍到的人的ID',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '拍到的人',
  `status` tinyint(1) NOT NULL COMMENT '0竞拍中 1竞拍成功（结算中） 2流拍 3失败 4竞拍成功（完成）',
  `order_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单ID',
  `order_status` tinyint(1) NOT NULL COMMENT '订单状态',
  `order_time` datetime(0) NOT NULL,
  `pay_time` datetime(0) NOT NULL COMMENT '支付时间（格式化）',
  `refund_over_time` datetime(0) NOT NULL COMMENT '退款完成时间（格式化）',
  `last_user_id` int(11) NOT NULL COMMENT '最后竞拍用户id',
  `last_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后出价竞拍用户名称',
  `last_pai_diamonds` int(11) NOT NULL COMMENT '最后出价金额',
  `is_delete` tinyint(1) NOT NULL COMMENT '删除标识',
  `end_time` int(11) NOT NULL COMMENT '竞拍结束时间（module=1使用）',
  `shop_id` int(11) NULL DEFAULT NULL COMMENT '店铺id[如果为第三方竞拍，此项必选]',
  `shop_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '店铺名称[如果为第三方竞拍，此项必选]',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '竞拍商品表' ROW_FORMAT = Dynamic;

CREATE TABLE  `%DB_PREFIX%pai_join`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `pai_id` int(11) NOT NULL COMMENT '竞拍ID',
  `user_id` int(11) NOT NULL COMMENT '参与会员',
  `bz_diamonds` int(11) NOT NULL COMMENT '参与保证金(冗余字段)',
  `status` tinyint(1) NOT NULL COMMENT '0未处理 1已返还 2已扣除',
  `create_time` int(11) NOT NULL COMMENT '参与时间',
  `create_date` datetime(0) NOT NULL COMMENT '参与时间',
  `create_time_ymd` date NOT NULL COMMENT '参与时间(年月日)',
  `create_time_y` int(4) NOT NULL COMMENT '参与时间(年)',
  `create_time_m` int(2) NOT NULL COMMENT '参与时间(月)',
  `create_time_d` int(2) NOT NULL COMMENT '参与时间(日)',
  `consignee` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人姓名',
  `consignee_mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人手机号',
  `consignee_district` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人所在地行政地区信息,json格式，主要用于可能的运费计算',
  `consignee_address` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人详细地址',
  `pai_diamonds` int(11) NOT NULL COMMENT '最终出价',
  `pai_number` int(11) NOT NULL COMMENT '出价几次',
  `pai_status` tinyint(1) NOT NULL COMMENT '0 出局 1待付款 2排队中 3超时出局 4 付款完成',
  `pai_left_start_time` datetime(0) NOT NULL COMMENT '前3名排队付款开始时间,用于倒计时',
  `order_id` int(11) NOT NULL COMMENT '订单id',
  `order_status` tinyint(1) NOT NULL COMMENT '订单状态',
  `order_time` int(11) NOT NULL COMMENT '下单时间',
  `pay_time` datetime(0) NOT NULL COMMENT '支付时间（格式化）',
  `refund_over_time` datetime(0) NOT NULL COMMENT '退款完成时间（格式化）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参与竞拍表' ROW_FORMAT = Dynamic;

CREATE TABLE  `%DB_PREFIX%pai_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `podcast_id` int(11) NOT NULL COMMENT '主播ID',
  `user_id` int(11) NOT NULL COMMENT '竞拍人id',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '竞拍人',
  `pai_id` int(11) NOT NULL COMMENT '竞拍商品ID',
  `bz_diamonds` int(11) NOT NULL COMMENT '竞拍保证金(冗余字段)',
  `qp_diamonds` int(11) NOT NULL COMMENT '起拍价(冗余字段)',
  `jj_diamonds` int(11) NOT NULL COMMENT '每次加价(冗余字段)',
  `pai_diamonds` int(11) NOT NULL COMMENT '当前出价',
  `pai_sort` int(1) NOT NULL COMMENT '当前价的第n次出价',
  `pai_time_ms` decimal(14, 0) NOT NULL COMMENT '竞拍时间（毫秒）',
  `pai_time` int(11) NOT NULL COMMENT '竞拍时间',
  `pai_date` datetime(0) NOT NULL COMMENT '竞拍时间（格式化）',
  `pai_time_ymd` date NOT NULL COMMENT '竞拍时间（年月日）',
  `pai_time_y` int(4) NOT NULL COMMENT '竞拍时间（年）',
  `pai_time_m` int(2) NOT NULL COMMENT '竞拍时间（月）',
  `pai_time_d` int(2) NOT NULL COMMENT '竞拍时间（日）',
  `status` tinyint(1) NOT NULL COMMENT '0待支付  1支付成功 2已流拍',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk`(`pai_id`, `pai_diamonds`, `pai_sort`) USING BTREE,
  INDEX `pai_date`(`pai_date`) USING BTREE,
  INDEX `pai_time_ymd`(`pai_time_ymd`) USING BTREE,
  INDEX `pai_time_y`(`pai_time_y`) USING BTREE,
  INDEX `pai_time_m`(`pai_time_m`) USING BTREE,
  INDEX `pai_time_d`(`pai_time_d`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '竞拍记录表' ROW_FORMAT = Dynamic;

CREATE TABLE  `%DB_PREFIX%pai_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签名称',
  `sort` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签列表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%pai_violations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `podcast_id` int(11) NOT NULL COMMENT '主播id',
  `create_time` int(11) NOT NULL COMMENT '违规事件 时间戳   GMT+0',
  `create_date` date NOT NULL COMMENT '违规事件 年月日   GMT+8',
  `create_time_y` int(4) NOT NULL COMMENT '违规事件 年   GMT+8',
  `create_time_m` int(2) NOT NULL COMMENT '违规事件 月   GMT+8',
  `create_time_d` int(2) NOT NULL COMMENT '违规事件 日   GMT+8',
  `create_time_ym` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '违规事件 年月   GMT+8',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '主播拍卖违规记录表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%podcast_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '主播ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  `imgs` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片（JSON数据）',
  `price` decimal(20, 2) NOT NULL COMMENT '商品价钱',
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品URL地址',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品描述',
  `is_effect` tinyint(1) NOT NULL DEFAULT 1 COMMENT '商品状态 1为正常,0为删除（值为0时不在前端展示）',
  `imgs_details` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品详情图片',
  `seen_num` int(11) NOT NULL COMMENT '看过人数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '主播个人商品表' ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%prop` ADD COLUMN `is_award` tinyint(1) NOT NULL COMMENT '是否为可中奖礼物 1为 是、0为否 ' AFTER `gif_gift_show_style`;

ALTER TABLE  `%DB_PREFIX%prop` ADD COLUMN `prop_notify` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否全服飘屏' AFTER `is_award`;

ALTER TABLE  `%DB_PREFIX%prop` ADD COLUMN `burst_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连发id字符串' AFTER `prop_notify`;

ALTER TABLE  `%DB_PREFIX%prop` MODIFY COLUMN `is_animated` tinyint(4) NULL DEFAULT NULL COMMENT '0:普通礼物 1:gif礼物 2:大型动画礼物 3:svga动画礼物' AFTER `is_red_envelope`;

ALTER TABLE  `%DB_PREFIX%prop` MODIFY COLUMN `gif_gift_show_style` tinyint(4) NULL DEFAULT NULL COMMENT '动画模式 0:按像素显示模式 1:全屏显示模式 2:至少两条边贴边模式' AFTER `pc_gif`;

CREATE TABLE  `%DB_PREFIX%prop_burst`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连发名称',
  `num` int(11) NULL DEFAULT NULL COMMENT '连发数量',
  `is_effect` tinyint(1) NULL DEFAULT 1 COMMENT '是否有效 0无效 1有效',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_num_iseffect`(`num`, `is_effect`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%qk_svideo_favor` COMMENT = '我收藏的小视频';

ALTER TABLE  `%DB_PREFIX%qk_svideo_favor` MODIFY COLUMN `weibo_id` int(10) NOT NULL COMMENT '收藏的ID' AFTER `user_id`;

CREATE TABLE  `%DB_PREFIX%shopping_cart`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增字段',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '购买的产品显示名称(包含购买的规格)',
  `imgs` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品图片（JSON数据）',
  `attr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '购买的相关属性的ID，用半角逗号分隔',
  `unit_price` decimal(20, 2) NOT NULL COMMENT '单价',
  `number` int(11) NOT NULL COMMENT '数量',
  `total_price` decimal(20, 2) NOT NULL COMMENT '总价',
  `verify_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '验证唯一的标识码（由商品ID与属性ID组合加密生成）',
  `create_time` datetime(0) NOT NULL COMMENT '加入购物车的时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新的时间',
  `return_money` decimal(20, 2) NOT NULL COMMENT '返现金的单价',
  `return_total_money` decimal(20, 2) NOT NULL COMMENT '返现金的总价',
  `return_score` int(11) NOT NULL COMMENT '返积分的单价',
  `return_total_score` int(11) NOT NULL COMMENT '返积分的总价',
  `cate_id` int(11) NOT NULL COMMENT '商品分类',
  `sub_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简短名称',
  `podcast_id` int(11) NOT NULL COMMENT '商品所属的主播ID',
  `attr_str` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '属性组合的显示名称',
  `is_effect` int(1) NOT NULL DEFAULT 1 COMMENT '购物车中商品的失效状态，1有效，0失效，该字段用于统计商户或后台更改价格或者\r\n属性导致购物车中的商品与原商品不符而失效',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `goods_id`(`goods_id`) USING BTREE,
  CONSTRAINT `goods_id` FOREIGN KEY (`goods_id`) REFERENCES  `%DB_PREFIX%goods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES  `%DB_PREFIX%user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物车表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%store_condition`  (
  `id` int(11) NOT NULL COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `type` int(10) NOT NULL COMMENT '类别 1：小视频条件，2：个人认证条件',
  `value` int(10) NULL DEFAULT 0 COMMENT '参数',
  `is_effect` tinyint(1) NOT NULL COMMENT '有效性标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%svideo_music` COMMENT = '//用户';

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `is_pid` tinyint(1) NOT NULL DEFAULT 0 AFTER `weixin_account_time`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `score_pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '积分资产密码' AFTER `is_pid`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `rate` int(11) NOT NULL DEFAULT 0 COMMENT '干预系数，0-100,0表示无干预，全部随机结果、100表示完全干预，收益最大' AFTER `score_pwd`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `game_distribution_id` int(11) NOT NULL DEFAULT 0 COMMENT '上级分销者ID' AFTER `rate`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `game_distribution1` int(11) NOT NULL DEFAULT 0 COMMENT '游戏一级分销抽成比例' AFTER `game_distribution_id`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `game_distribution2` int(11) NOT NULL DEFAULT 0 COMMENT '游戏二级分销抽成比例' AFTER `game_distribution1`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `invitation_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '邀请码' AFTER `game_distribution2`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `invitation_id` int(11) NULL DEFAULT 0 COMMENT '邀请人' AFTER `invitation_code`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `game_distribution_top_id` int(11) NULL DEFAULT 0 AFTER `invitation_id`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `open_store` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启小店  0为 不禁用 1 为禁用' AFTER `society_code`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `open_goods` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启购物 0为 不禁用 1 为禁用' AFTER `share_up_ticket`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `store_status` int(4) NOT NULL DEFAULT 0 COMMENT '小店状态 0未审核 1审核中 2通过 3未通过' AFTER `vehicle_id`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `store_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝理由' AFTER `store_status`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `weight` int(10) NULL DEFAULT 0 COMMENT '权重' AFTER `store_reason`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `weibo_num` int(11) NOT NULL DEFAULT 0 COMMENT '推送小视频数量' AFTER `weight`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `is_update` tinyint(1) NULL DEFAULT 0 COMMENT '是否更新过小视频：0否，1是' AFTER `weibo_num`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `is_login` tinyint(1) NULL DEFAULT 0 COMMENT '今天是否登录：0否，1是' AFTER `is_update`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `log_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志目录，每次成功上传更新一次' AFTER `is_login`;

ALTER TABLE  `%DB_PREFIX%user` ADD COLUMN `uuid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'uuid，每次推送生成新的并保存' AFTER `log_path`;

ALTER TABLE  `%DB_PREFIX%user` MODIFY COLUMN `society_code` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公会邀请码 填写后审核通过自动加入相对应的公会' AFTER `game_distribution_top_id`;

ALTER TABLE  `%DB_PREFIX%user` MODIFY COLUMN `vehicle_id` tinyint(5) NULL DEFAULT 0 COMMENT '座驾id' AFTER `open_goods`;

ALTER TABLE  `%DB_PREFIX%user` ADD INDEX `idx_ecs_user_cc_7`(`id`, `is_robot`) USING BTREE;

ALTER TABLE  `%DB_PREFIX%user` ADD INDEX `idx_ecs_user_cc_8`(`luck_num`, `is_robot`) USING BTREE;

ALTER TABLE  `%DB_PREFIX%user` ADD INDEX `invitation_code`(`invitation_code`) USING BTREE;

CREATE TABLE  `%DB_PREFIX%user_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` int(11) NOT NULL COMMENT '会员id',
  `consignee` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '收货人姓名',
  `consignee_mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人手机号',
  `consignee_district` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人所在地行政地区信息，json格式，主要用于可能的运费计算',
  `consignee_address` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人详细地址',
  `is_default` tinyint(1) NOT NULL COMMENT '是否默认',
  `create_time` datetime(0) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收货地址表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%user_diamonds_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `pai_id` int(11) NOT NULL DEFAULT 0 COMMENT '竞拍id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `diamonds` int(11) NOT NULL COMMENT '变更数额',
  `account_diamonds` int(11) NOT NULL COMMENT '账户余额',
  `memo` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '发生时间 GMT+0',
  `create_date` datetime(0) NOT NULL COMMENT '发生时间 年月日 GMT+8',
  `create_time_ymd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建年月日',
  `create_time_y` int(4) NOT NULL COMMENT '发生时间 年 GMT+8',
  `create_time_m` int(2) NOT NULL COMMENT '发生时间 月 GMT+8',
  `create_time_d` int(2) NOT NULL COMMENT '发生时间 日 GMT+8',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '变更类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户钻石日志表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%user_game_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `game_log_id` int(11) NULL DEFAULT NULL COMMENT '游戏轮数id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `money` int(11) NULL DEFAULT NULL COMMENT '下注金额',
  `bet` int(11) NULL DEFAULT NULL COMMENT '下注选项：1/2/3',
  `podcast_id` int(11) NULL DEFAULT NULL COMMENT '主播id',
  `create_time` int(11) NOT NULL COMMENT '发布时间',
  `create_date` datetime(0) NOT NULL COMMENT '发布时间（格式化）',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1、下注，2、收益',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%user_game_log_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `game_log_id` int(11) NULL DEFAULT NULL COMMENT '游戏轮数id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `money` int(11) NULL DEFAULT NULL COMMENT '下注金额',
  `bet` int(11) NULL DEFAULT NULL COMMENT '下注选项：1/2/3',
  `podcast_id` int(11) NULL DEFAULT NULL COMMENT '主播id',
  `create_time` int(11) NOT NULL COMMENT '发布时间',
  `create_date` datetime(0) NOT NULL COMMENT '发布时间（格式化）',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1、下注，2、收益',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%user_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '主播ID',
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  `imgs` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片（JSON数据）',
  `imgs_details` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品详情图片',
  `price` decimal(20, 2) NOT NULL COMMENT '商品价钱',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品描述',
  `kd_cost` decimal(20, 2) NOT NULL COMMENT '快递费用',
  `score` int(255) NOT NULL COMMENT '经验',
  `inventory` int(255) NOT NULL COMMENT '库存',
  `is_effect` tinyint(1) NOT NULL COMMENT '商品状态 1为正常,0为删除（值为0时不在前端展示）',
  `pai_diamonds` decimal(20, 2) NOT NULL COMMENT '商品直播价格（钻石）',
  `sales` int(11) NOT NULL COMMENT '初始销售量',
  `number` int(11) NOT NULL COMMENT '初始售卖人数',
  `bz_diamonds` int(11) NOT NULL COMMENT '竞拍保证金',
  `jj_diamonds` int(11) NOT NULL COMMENT '竞拍加价幅度',
  `pai_time` int(11) NOT NULL COMMENT '竞拍时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '主播商品表' ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%user_log` ADD COLUMN `coin` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '游戏币' AFTER `contribution_id`;

ALTER TABLE  `%DB_PREFIX%user_log` ADD COLUMN `coin_ins_success` tinyint(1) NULL DEFAULT 0 COMMENT '游戏币是否添加成功 0是 1否 默认0' AFTER `coin`;

ALTER TABLE  `%DB_PREFIX%user_log` MODIFY COLUMN `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型 0表示充值 1表示提现 2赠送道具 3兑换印票 4分享获得印票 5登录赠送积分 6观看付费直播 7游戏收益 8公会收益 9分销收益 10公会长获取 11平台收益 12中奖纪录' AFTER `user_id`;

ALTER TABLE  `%DB_PREFIX%user_music` ADD COLUMN `is_effect` tinyint(1) NULL DEFAULT 1 COMMENT '是否有效 0 无效 1 有效' AFTER `music_type`;

ALTER TABLE  `%DB_PREFIX%vehicle` MODIFY COLUMN `is_animated` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0:普通礼物 1:gif礼物 2:大型动画礼物' AFTER `sort`;

ALTER TABLE  `%DB_PREFIX%vehicle` MODIFY COLUMN `gif_gift_show_style` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'GIF礼物模式 0:按像素显示模式 1:全屏显示模式 2:至少两条边贴边模式' AFTER `is_effect`;

CREATE TABLE  `%DB_PREFIX%video_lianmai_pk`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `play_rtmp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受PK的主播rtmpAcc地址',
  `form_user_ticket` decimal(13, 2) NOT NULL DEFAULT 0.00 COMMENT '开始PK时，发起PK的主播当前印票数',
  `form_user_id` int(11) NOT NULL COMMENT '发起PK的主播ID',
  `form_video_id` int(11) NOT NULL COMMENT '发起PK的主播房间ID',
  `play_rtmp_acc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起PK的主播rtmpAcc地址',
  `to_user_id` int(10) NOT NULL DEFAULT 0 COMMENT '接受PK的主播ID',
  `to_video_id` int(11) NOT NULL COMMENT '接受PK的主播房间ID',
  `to_user_ticket` decimal(12, 2) NOT NULL DEFAULT 0.00 COMMENT '开始PK时，接受PK的主播当前印票数',
  `v_play_rtmp_acc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起PK的主播rtmpAcc地址',
  `start_time` int(11) NOT NULL DEFAULT 0 COMMENT '连麦开始时间',
  `stop_time` int(11) NOT NULL DEFAULT 0 COMMENT '连麦结束时间',
  `lianmai_time` tinyint(2) NOT NULL DEFAULT 0 COMMENT '连麦时间',
  `is_update` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否推送过胜利消息',
  `to_channelid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `form_channelid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `end_time` int(255) NOT NULL DEFAULT 0 COMMENT '主播主动关闭',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_vm_001`(`form_video_id`) USING BTREE,
  INDEX `idx_vm_002`(`form_user_ticket`, `form_user_id`, `form_video_id`, `to_user_id`, `to_video_id`, `to_user_ticket`, `start_time`, `lianmai_time`, `stop_time`) USING BTREE,
  INDEX `rds_idx_0`(`form_video_id`, `to_video_id`) USING BTREE,
  INDEX `rds_idx_1`(`to_video_id`) USING BTREE,
  INDEX `rds_idx_3`(`stop_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '主播连麦pk记录' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%video_lianmai_pk_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `play_rtmp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受PK的主播rtmpAcc地址',
  `form_user_ticket` decimal(13, 2) NOT NULL DEFAULT 0.00 COMMENT '开始PK时，发起PK的主播当前印票数',
  `form_user_id` int(11) NOT NULL COMMENT '发起PK的主播ID',
  `form_video_id` int(11) NOT NULL COMMENT '发起PK的主播房间ID',
  `play_rtmp_acc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起PK的主播rtmpAcc地址',
  `to_user_id` int(10) NOT NULL DEFAULT 0 COMMENT '接受PK的主播ID',
  `to_video_id` int(11) NOT NULL COMMENT '接受PK的主播房间ID',
  `to_user_ticket` decimal(12, 2) NOT NULL DEFAULT 0.00 COMMENT '开始PK时，接受PK的主播当前印票数',
  `v_play_rtmp_acc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起PK的主播rtmpAcc地址',
  `start_time` int(11) NOT NULL DEFAULT 0 COMMENT '连麦开始时间',
  `stop_time` int(11) NOT NULL DEFAULT 0 COMMENT '连麦结束时间',
  `lianmai_time` tinyint(2) NOT NULL DEFAULT 0 COMMENT '连麦时间',
  `is_update` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否推送过胜利消息',
  `to_channelid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `form_channelid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `end_time` int(255) NOT NULL DEFAULT 0 COMMENT '主播主动关闭',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_vm_001`(`form_video_id`) USING BTREE,
  INDEX `idx_vm_002`(`form_user_ticket`, `form_user_id`, `form_video_id`, `to_user_id`, `to_video_id`, `to_user_ticket`, `start_time`, `lianmai_time`, `stop_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '主播连麦pk记录历史表' ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%video_prop` ADD COLUMN `is_coin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '双币礼物，0是钻石，1是游戏币' AFTER `is_private`;

CREATE TABLE  `%DB_PREFIX%video_prop_all`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_id` int(10) NOT NULL DEFAULT 0 COMMENT '礼物id',
  `prop_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '道具名',
  `total_score` int(11) NOT NULL COMMENT '积分（from_user_id可获得的积分）合计',
  `total_diamonds` int(11) NOT NULL COMMENT '钻石（from_user_id减少的钻石）合计',
  `total_ticket` int(11) NOT NULL DEFAULT 0 COMMENT '印票(to_user_id增加的印票）合计;is_red_envelope=1时,为主播获得的：钻石 数量',
  `from_user_id` int(10) NOT NULL DEFAULT 0 COMMENT '送',
  `to_user_id` int(10) NOT NULL DEFAULT 0 COMMENT '收',
  `create_time` int(10) NOT NULL COMMENT '时间',
  `create_date` date NOT NULL COMMENT '日期字段,按日期归档；要不然数据量太大了；不好维护',
  `create_d` tinyint(2) NOT NULL COMMENT '日',
  `create_w` tinyint(2) NOT NULL COMMENT '周',
  `num` int(10) NOT NULL COMMENT '送的数量',
  `video_id` int(10) NOT NULL DEFAULT 0 COMMENT '直播ID',
  `group_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组ID',
  `is_red_envelope` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1:红包',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '弹幕内容',
  `ActionStatus` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息发送，请求处理的结果，OK表示处理成功，FAIL表示失败。',
  `ErrorInfo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息发送，错误信息',
  `ErrorCode` int(10) NOT NULL COMMENT '错误码',
  `create_ym` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '年月 如:201610',
  `from_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '送礼物人IP',
  `is_award` tinyint(1) NOT NULL COMMENT '是否为可中奖礼物 1为 是、0为否',
  `is_coin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '双币礼物，0是钻石，1是游戏币',
  `is_private` int(4) NULL DEFAULT 0 COMMENT '判断是否为私信送礼 1表示私信 2表示不是私信',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_ecs_video_prop_cc_1`(`create_ym`, `create_d`, `from_user_id`, `total_diamonds`) USING BTREE,
  INDEX `from_user_id`(`from_user_id`, `total_diamonds`) USING BTREE,
  INDEX `idx_ecs_video_prop_cc_2`(`create_ym`, `from_user_id`, `total_diamonds`) USING BTREE,
  INDEX `to_user_id`(`is_red_envelope`, `to_user_id`, `total_ticket`) USING BTREE,
  INDEX `idx_ecs_video_prop_cc_3`(`create_ym`, `create_d`, `is_red_envelope`, `to_user_id`, `total_ticket`) USING BTREE,
  INDEX `idx_ecs_video_prop_cc_4`(`create_ym`, `is_red_envelope`, `to_user_id`, `total_ticket`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%vip_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `day_num` int(11) NOT NULL DEFAULT 0 COMMENT '天数',
  `money` decimal(20, 2) NOT NULL COMMENT '价格',
  `iap_money` decimal(20, 2) NOT NULL COMMENT '苹果支付价格',
  `product_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '苹果应用内支付项目ID',
  `is_effect` tinyint(1) NULL DEFAULT 1 COMMENT '是否有效 1-有效 0-无效',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购买VIP会员规则表' ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `audio_id` int(11) NULL DEFAULT NULL COMMENT '小视频使用到的音乐id' AFTER `video_height`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `goods_id` int(11) NULL DEFAULT NULL COMMENT '商品id' AFTER `audio_id`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `is_podcast_goods` int(11) NULL DEFAULT NULL COMMENT '是否主播商品' AFTER `goods_id`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `classify_id` int(11) NULL DEFAULT NULL COMMENT '分类id' AFTER `is_podcast_goods`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `tags` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标签' AFTER `classify_id`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `is_open_tencent` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否腾讯云获取分类和标签' AFTER `tags`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `is_adv` tinyint(1) NULL DEFAULT 0 COMMENT '是否是商业广告：0否，1是' AFTER `is_open_tencent`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `file_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '视频云点播fileId' AFTER `is_adv`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `check_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '短视频(标签分类)审核' AFTER `file_id`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `ai_analysis_list` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '短视频腾讯云分析内容(标签和分类)' AFTER `check_status`;

ALTER TABLE  `%DB_PREFIX%weibo` ADD COLUMN `is_hot` tinyint(1) NULL DEFAULT 0 COMMENT '是否是热门推荐：0否，1是' AFTER `ai_analysis_list`;

ALTER TABLE  `%DB_PREFIX%weibo` MODIFY COLUMN `digg_count` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点赞数' AFTER `unlike_count`;

ALTER TABLE  `%DB_PREFIX%weibo` MODIFY COLUMN `comment_restrict` int(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '评论限制 1允许所有人评论 2仅关注的人 3不允许任何人' AFTER `first_image`;

ALTER TABLE  `%DB_PREFIX%weibo` MODIFY COLUMN `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发布时@的好友，目前只做展示用' AFTER `comment_restrict`;

CREATE TABLE  `%DB_PREFIX%weibo_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `weight` int(11) NULL DEFAULT 0 COMMENT '权重',
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `u_prc`(`province`, `city`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//小视频地点' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_address_weight`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `weight` int(11) NULL DEFAULT 0 COMMENT '对象权重',
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '变更时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `u_prc`(`user_id`, `province`, `city`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//小视频地点权重明细' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_classify`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `brief` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类简介(备用字段)',
  `pid` int(11) NOT NULL COMMENT '父ID，程序分类可分二级',
  `is_effect` tinyint(4) NOT NULL COMMENT '有效性标识',
  `sort` int(11) NOT NULL COMMENT '排序',
  `weight` int(10) NULL DEFAULT 0 COMMENT '权重',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '小视频分类' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_classify_weight`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `weight` int(11) NULL DEFAULT 0 COMMENT '对象权重',
  `classify_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名字',
  `classify_id` int(11) NULL DEFAULT NULL COMMENT '分类id',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '变更时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `u_cli`(`user_id`, `classify_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//小视频分类权重明细' ROW_FORMAT = Compact;

ALTER TABLE  `%DB_PREFIX%weibo_comment` ADD COLUMN `digg_count` int(11) NULL DEFAULT 0 COMMENT '点赞数' AFTER `is_read`;

ALTER TABLE  `%DB_PREFIX%weibo_comment` ADD COLUMN `comment_level` tinyint(1) NOT NULL COMMENT '评论等级' AFTER `digg_count`;

ALTER TABLE  `%DB_PREFIX%weibo_comment` ADD COLUMN `father_comment_id` int(11) NULL DEFAULT 0 COMMENT '父评论id' AFTER `comment_level`;

ALTER TABLE  `%DB_PREFIX%weibo_comment` MODIFY COLUMN `type` tinyint(1) NULL DEFAULT 1 COMMENT '类型 1-评论 2- 点赞,3-踩一下' AFTER `comment_id`;

CREATE TABLE  `%DB_PREFIX%weibo_comment_digg`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '点赞的用户id',
  `weibo_id` int(11) NOT NULL DEFAULT 0 COMMENT '评论所属的小视频id',
  `comment_id` int(11) NOT NULL COMMENT '评论或回复id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user`(`user_id`, `comment_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户对某个评论或回复点赞' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_digg`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `weibo_id` int(11) NOT NULL COMMENT '小视频id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_interest`(`user_id`, `weibo_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户对某个小视频点赞' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_music_weight`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `weight` int(11) NULL DEFAULT 0 COMMENT '对象权重',
  `music_id` int(11) NULL DEFAULT NULL COMMENT '音乐id',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '变更时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `u_mu`(`user_id`, `music_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//小视频音乐权重明细' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_not_interest`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `weibo_id` int(11) NOT NULL COMMENT '小视频id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_interest`(`user_id`, `weibo_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户对某个小视频不感兴趣' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_same_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `weibo_id` int(11) NOT NULL COMMENT '小视频id',
  `music_id` int(11) NOT NULL COMMENT '音乐id',
  `tag_id` int(11) NOT NULL COMMENT '标签id',
  `tag_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签名称',
  `type` tinyint(1) NOT NULL COMMENT '类型 1 音乐 2 标签',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '小视频相同音乐、标签记录' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_see`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weibo_id` int(11) NOT NULL DEFAULT 0 COMMENT '小视频id',
  `weibo_user_id` int(11) NOT NULL DEFAULT 0 COMMENT '小视频发布者',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '评论者编号',
  `number` int(11) NOT NULL DEFAULT 0 COMMENT '观看次数',
  `create_time` int(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `we_u`(`weibo_id`, `user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//个人小视频观看列表' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签名称',
  `is_effect` tinyint(4) NOT NULL COMMENT '有效性标识',
  `sort` int(11) NOT NULL COMMENT '排序',
  `weight` int(10) NULL DEFAULT 0 COMMENT '权重',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '小视频标签' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_tag_weight`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `weight` int(11) NULL DEFAULT 0 COMMENT '对象权重',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `tag_id` int(11) NULL DEFAULT NULL COMMENT '标签id',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '变更时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `u_tai`(`user_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//小视频标签权重明细' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_user_weight`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `object_user_id` int(11) NULL DEFAULT NULL COMMENT '对象用户id',
  `weight` int(11) NULL DEFAULT 0 COMMENT '对象权重',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '变更时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `u_ouid`(`user_id`, `object_user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//小视频人权重明细' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_weight_config`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置名称',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `is_effect` tinyint(4) NOT NULL DEFAULT 1 COMMENT '有效性标识',
  `user_weight` int(10) NULL DEFAULT 0 COMMENT '人权重',
  `classify_weight` int(10) NULL DEFAULT 0 COMMENT '分类权重',
  `tag_weight` int(10) NULL DEFAULT 0 COMMENT '标签权重',
  `music_weight` int(10) NULL DEFAULT 0 COMMENT '歌曲权重',
  `address_weight` int(10) NULL DEFAULT 0 COMMENT '地点权重（省市）',
  `type` int(10) NULL DEFAULT 1 COMMENT '权重类型：1点赞,2评论，3关注，4转发，5发布视频，6使用同歌曲,7连续观看同一作者3个视频，8重复播放同一视频3次,9观看作者个人中心，10正常观看视频，11快速翻过，12取消点赞,13删除评论，14取消关注，15不感兴趣',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `c_d`(`code`) USING BTREE,
  UNIQUE INDEX `t_t`(`title`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//小视频权重配置' ROW_FORMAT = Compact;

CREATE TABLE  `%DB_PREFIX%weibo_weight_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `change_weight` double(3, 1) NULL DEFAULT 0.0 COMMENT '变化权重值',
  `memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型：0增加,1减少',
  `now_weight` double(3, 1) NULL DEFAULT 0.0 COMMENT '目前权重（未变化之前的）',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `weight` double(3, 1) NULL DEFAULT 0.0 COMMENT '变化之后的权重',
  `user_id` int(11) NOT NULL COMMENT ' 会员id',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '权重类型：1人权重，2分类权重，3标签权重,4音乐权重,5地址权重',
  `weibo_id` int(11) NOT NULL DEFAULT 0 COMMENT '小视频id',
  `operation_type` int(10) NULL DEFAULT 1 COMMENT '权重类型：1点赞,2评论，3关注，4转发，5发布同标签视频，6使用同歌曲,7连续观看同一作者3个视频，8重复播放同一视频,9观看作者个人中心，10正常观看视频，11快速翻过，12取消点赞,13删除评论，14取消关注，15不感兴趣',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '//小视频用户权重日志' ROW_FORMAT = Dynamic;

INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1634, NULL, '点赞', 0, 1, 0, 1, 0, 1, 0, 1);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1635, NULL, '评论', 0, 1, 0, 2, 0, 2, 0, 2);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1636, NULL, '关注', 0, 1, 0, 0, 0, 0, 0, 3);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1637, NULL, '转发', 0, 1, 0, 3, 0, 3, 0, 4);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1638, NULL, '发布视频', 0, 1, 0, 4, 0, 0, 0, 5);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1639, NULL, '连续观看同一作者3个视频', 0, 1, 0, 0, 0, 0, 0, 7);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1640, NULL, '重复播放同一视频3次', 0, 1, 0, 5, 0, 5, 0, 8);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1641, NULL, '观看作者个人中心', 0, 1, 0, 0, 0, 0, 0, 9);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1642, NULL, '正常观看视频', 0, 1, 0, 6, 0, 6, 0, 10);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1643, NULL, '取消点赞', 0, 1, 0, -1, 0, -1, 0, 12);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1644, NULL, '删除评论', 0, 1, 0, -2, 0, -2, 0, 13);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1645, NULL, '取消关注', 0, 1, 0, 0, 0, 0, 0, 14);
INSERT INTO `%DB_PREFIX%weibo_weight_config` VALUES (1646, NULL, '不感兴趣', 0, 1, 0, -4, 0, -3, 0, 15);

INSERT INTO `%DB_PREFIX%store_condition`(`id`, `title`, `content`, `img`, `type`, `value`, `is_effect`) VALUES (1, '发布视频', '发布大于等于2个短视频', '/public/attachment/201908/26/11/5d634fdc2317f.jpg', 1, 2, 1);
INSERT INTO `%DB_PREFIX%store_condition`(`id`, `title`, `content`, `img`, `type`, `value`, `is_effect`) VALUES (2, '主播认证', '完成直播认证', '/public/attachment/201908/26/11/5d6353a6997e7.jpg', 2, 0, 1);

INSERT INTO `%DB_PREFIX%plugin`(`id`, `child_id`, `image`, `name`, `is_effect`, `class`, `type`, `price`) VALUES (10, 12, '/public/images/pk.png', '连麦对战', 1, 'lianmai', 1, 0);

INSERT INTO `%DB_PREFIX%plugin` (`id`,`child_id`, `image`, `name`, `is_effect`, `class`, `type`) VALUES ('3', '0', './public/attachment/201612/15/09/5851f15e9d962.png', '按时收费', '1', 'live_pay', '1');
INSERT INTO `%DB_PREFIX%plugin` (`id`,`child_id`, `image`, `name`, `is_effect`, `class`, `type`) VALUES ('4', '0', './public/images/5851f15e9d962.png', '按场收费', 1, 'live_pay_scene', 1);

CREATE TABLE `%DB_PREFIX%live_pay_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_time` int(11) NOT NULL COMMENT '观看时间（from_user_id累计观看时间）合计',
  `total_ticket` decimal(13,2) NOT NULL COMMENT '主播获得的印票',
  `total_diamonds` int(11) NOT NULL COMMENT '钻石（from_user_id减少的钻石）合计',
  `from_user_id` int(10) NOT NULL DEFAULT '0' COMMENT '观众',
  `to_user_id` int(10) NOT NULL DEFAULT '0' COMMENT '主播',
  `create_time` int(10) NOT NULL COMMENT '时间',
  `create_date` date NOT NULL COMMENT '日期字段,按日期归档；要不然数据量太大了；不好维护',
  `create_ym` varchar(12) NOT NULL COMMENT '年月 如:201610',
  `create_d` tinyint(2) NOT NULL COMMENT '日',
  `create_w` tinyint(2) NOT NULL COMMENT '周',
  `live_fee` int(10) NOT NULL COMMENT '收取费用（钻石/分钟）',
  `live_pay_time` int(10) NOT NULL COMMENT '直播间开始收费时间',
  `live_pay_date` date NOT NULL COMMENT '直播间开始收费 日期字段',
  `video_id` int(10) NOT NULL DEFAULT '0' COMMENT '直播ID',
  `group_id` varchar(20) NOT NULL COMMENT '群组ID',
  `pay_time_end` int(11) NOT NULL COMMENT '最后一次扣款时间',
  `pay_time_next` int(11) NOT NULL COMMENT '下次扣款时间',
  `live_is_mention_time` int(11) NOT NULL COMMENT '提档后开始收费时间',
  `live_is_mention_pay` int(11) NOT NULL COMMENT '提档前扣费合计',
  `live_pay_type` tinyint(1) NOT NULL COMMENT '直播类型 0 按时收费 1按场收费',
  `new_room_id` int(11) NOT NULL COMMENT '新付费直播的ID , 用于异常终止直播间付费，主播新开的主播ID ',
  `total_score` int(11) NOT NULL COMMENT '积分（from_user_id可获得的积分）合计',
  `uesddiamonds_to_score` float(11,2) NOT NULL COMMENT '观众（from_user_id）获得积分的转换比例',
  `ticket_to_rate` float(11,2) NOT NULL COMMENT '主播（to_user_id）获得的印票转换比例',
  PRIMARY KEY (`id`),
  KEY `idx_vp_002` (`from_user_id`,`to_user_id`,`video_id`) USING BTREE,
  KEY `idx_vp_003` (`video_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='//付费直播记录';

CREATE TABLE `%DB_PREFIX%live_pay_log_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_time` int(11) NOT NULL COMMENT '观看时间（from_user_id累计观看时间）合计',
  `total_ticket` decimal(13,2) NOT NULL COMMENT '主播获得的印票',
  `total_diamonds` int(11) NOT NULL COMMENT '钻石（from_user_id减少的钻石）合计',
  `from_user_id` int(10) NOT NULL DEFAULT '0' COMMENT '观众',
  `to_user_id` int(10) NOT NULL DEFAULT '0' COMMENT '主播',
  `create_time` int(10) NOT NULL COMMENT '时间',
  `create_date` date NOT NULL COMMENT '日期字段,按日期归档；要不然数据量太大了；不好维护',
  `create_ym` varchar(12) NOT NULL COMMENT '年月 如:201610',
  `create_d` tinyint(2) NOT NULL COMMENT '日',
  `create_w` tinyint(2) NOT NULL COMMENT '周',
  `live_fee` int(10) NOT NULL COMMENT '收取费用（钻石/分钟）',
  `live_pay_time` int(10) NOT NULL COMMENT '直播间开始收费时间',
  `live_pay_date` date NOT NULL COMMENT '直播间开始收费 日期字段',
  `video_id` int(10) NOT NULL DEFAULT '0' COMMENT '直播ID',
  `group_id` varchar(20) NOT NULL COMMENT '群组ID',
  `pay_time_end` int(11) NOT NULL COMMENT '最后一次扣款时间',
  `pay_time_next` int(11) NOT NULL COMMENT '下次扣款时间',
  `live_is_mention_time` int(11) NOT NULL COMMENT '提档后开始收费时间',
  `live_is_mention_pay` int(11) NOT NULL COMMENT '提档前扣费合计',
  `live_pay_type` tinyint(1) NOT NULL COMMENT '直播类型 0 按时收费 1按场收费',
  `new_room_id` int(11) NOT NULL COMMENT '新付费直播的ID , 用于异常终止直播间付费，主播新开的主播ID ',
  `total_score` int(11) NOT NULL COMMENT '积分（from_user_id可获得的积分）合计',
  `uesddiamonds_to_score` float(11,2) NOT NULL COMMENT '观众（from_user_id）获得积分的转换比例',
  `ticket_to_rate` float(11,2) NOT NULL COMMENT '主播（to_user_id）获得的印票转换比例',
  PRIMARY KEY (`id`),
  KEY `idx_vp_002` (`from_user_id`,`to_user_id`,`video_id`) USING BTREE,
  KEY `idx_vp_003` (`video_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='//付费直播历史记录';

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_pay_max', '按时付费收费最高', '付费直播配置', '100', 0, 97, NULL, NULL, '（钻石）付费直播,主播填写最高的收费,0为不限制');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_pay_min', '按时付费收费最低', '付费直播配置', '1', 0, 97, NULL, NULL, '（钻石）付费直播,主播填写最低的收费 默认1');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_pay_scene_max', '按场付费收费最高', '付费直播配置', '100', 0, 98, NULL, NULL, '（钻石）付费直播,主播填写最高的收费,0为不限制');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_pay_scene_min', '按场付费收费最低', '付费直播配置', '1', 0, 98, NULL, NULL, '（钻石）付费直播,主播填写最低的收费 默认1');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('countdown', '预览倒计时', '付费直播配置', '10', 0, 0, NULL, NULL, '(秒) 付费直播间预览倒计时，默认为10，0为关闭倒计时预览');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('is_only_play_video', '是否预览画面', '付费直播配置', '1', 4, 0, '0,1', '否,是', '付费直播间是否预览画面 1是 0否');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_pay_num', '付费开始最低人数', '付费直播配置', '0', 0, 88, NULL, NULL, '（人）允许切换付费模式的最低人数，含机器人');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_pay_rule', '提档要求分钟', '付费直播配置', '1', 0, 90, NULL, NULL, '（分钟）几分钟后出现提档按钮,输入整数');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_pay_fee', '提档扣费设置', '付费直播配置', '2', 0, 90, NULL, NULL, '（钻石）提档累加的费用,输入整数');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('live_count_down', '倒计时时间', '付费直播配置', '30', 0, 89, NULL, NULL, '（秒）主播切换收费模式，多少时间后开始计费');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('ticket_to_rate', '钻石转印票比例', '付费直播配置', '1', 0, 91, NULL, NULL, '（印票）付费直播主播收到的钻石转印票比例，如1钻转多少印票');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('uesddiamonds_to_score', '钻石转积分比例', '付费直播配置', '2', 0, 91, NULL, NULL, '（积分）付费直播观众送出的钻石转积分比例，如1钻石转多少积分');

ALTER TABLE `%DB_PREFIX%plugin`
MODIFY COLUMN `type`  int(11) NOT NULL COMMENT '类别 1 付费播 2游戏 3竞拍';

ALTER TABLE `%DB_PREFIX%video` ADD COLUMN `live_pay_time`  int(11) NOT NULL COMMENT '开始收费时间' ;
ALTER TABLE `%DB_PREFIX%video` ADD COLUMN `is_live_pay`  tinyint(1) NOT NULL COMMENT '是否收费模式  1是 0否';
ALTER TABLE `%DB_PREFIX%video` ADD COLUMN `live_fee`  int(11) NOT NULL COMMENT '付费直播 收取多少费用； 每分钟收取多少钻石，主播端设置' ;
ALTER TABLE `%DB_PREFIX%video` ADD COLUMN `live_is_mention`  tinyint(1) NOT NULL COMMENT '是否已经提档 1是、0否';
ALTER TABLE `%DB_PREFIX%video` ADD COLUMN `live_pay_type`  tinyint(1) NOT NULL COMMENT '收费类型 0按时收费，1按场次收费' ;
ALTER TABLE `%DB_PREFIX%video` ADD COLUMN `live_pay_count`  tinyint(1) NOT NULL COMMENT '付费人数' ;

ALTER TABLE `%DB_PREFIX%video_history` ADD COLUMN `create_type`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0:APP端创建的直播;1:PC端创建的直播' ;
ALTER TABLE `%DB_PREFIX%video_history` ADD COLUMN `live_pay_time`  int(11) NOT NULL COMMENT '开始收费时间' ;
ALTER TABLE `%DB_PREFIX%video_history` ADD COLUMN `is_live_pay`  tinyint(1) NOT NULL COMMENT '是否收费模式  1是 0否';
ALTER TABLE `%DB_PREFIX%video_history` ADD COLUMN `live_fee`  int(11) NOT NULL COMMENT '付费直播 收取多少费用； 每分钟收取多少钻石，主播端设置' ;
ALTER TABLE `%DB_PREFIX%video_history` ADD COLUMN `live_is_mention`  tinyint(1) NOT NULL COMMENT '是否已经提档 1是、0否' ;
ALTER TABLE `%DB_PREFIX%video_history` ADD COLUMN `live_pay_count`  tinyint(1) NOT NULL COMMENT '付费人数' ;

DELETE FROM %DB_PREFIX%live_pay_log_history WHERE ID not IN (SELECT MAX(ID) FROM %DB_PREFIX%live_pay_log_history GROUP BY from_user_id,to_user_id,video_id)

ALTER TABLE `%DB_PREFIX%live_pay_log`
ADD UNIQUE INDEX `idx_vp_007` (`from_user_id`, `to_user_id`, `video_id`) USING BTREE ;

ALTER TABLE `%DB_PREFIX%live_pay_log_history`
ADD UNIQUE INDEX `idx_vp_007` (`from_user_id`, `to_user_id`, `video_id`) USING BTREE ;

ALTER TABLE `%DB_PREFIX%plugin`
ADD UNIQUE INDEX `idx_01` (`name`, `class`) USING BTREE ;

INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('lianmai_time','连麦对战时间','连麦对战设置','3,4,5,6,7',0,0,null,null,'用英文半角逗号分开');
INSERT INTO `%DB_PREFIX%m_config` (`code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('punish_time','惩罚时间','连麦对战设置','1',0,0,null,null,'惩罚时间（分钟数）');

INSERT INTO `%DB_PREFIX%m_config`(`id`, `code`, `title`, `group_id`, `val`, `type`, `sort`, `value_scope`, `title_scope`, `desc`) VALUES ('', 'is_open_tencent', '是否开启腾讯云获取分类和标签', '小视频设置', '0', 4, 5, '0,1', '否,是', NULL);

INSERT INTO `%DB_PREFIX%plugin`(`id`, `child_id`, `image`, `name`, `is_effect`, `class`, `type`, `price`) VALUES (1, 1, './public/attachment/201905/20/15/5ce25b6f1c7b3.png', '炸金花', 1, 'game', 2, 0);
INSERT INTO `%DB_PREFIX%plugin`(`id`, `child_id`, `image`, `name`, `is_effect`, `class`, `type`, `price`) VALUES (2, 2, './public/attachment/201905/20/15/5ce25b62cf847.png', '斗牛', 1, 'game', 2, 0);
INSERT INTO `%DB_PREFIX%plugin`(`id`, `child_id`, `image`, `name`, `is_effect`, `class`, `type`, `price`) VALUES (5, 4, './public/attachment/201701/24/16/58870cc1be91e.png', '竞拍', 1, 'pai', 1, 0);
INSERT INTO `%DB_PREFIX%plugin`(`id`, `child_id`, `image`, `name`, `is_effect`, `class`, `type`, `price`) VALUES (6, 5, '.public/attachment/201701/24/16/58870ca97c72a.png', '购物', 1, 'shop', 1, 0);
INSERT INTO `%DB_PREFIX%plugin`(`id`, `child_id`, `image`, `name`, `is_effect`, `class`, `type`, `price`) VALUES (7, 6, '.public/attachment/201701/24/16/58870ca97c72a.png', '小店', 1, 'podcast_goods', 1, 0);
INSERT INTO `%DB_PREFIX%games`(`id`, `image`, `name`, `principal`, `is_effect`, `commission_rate`, `long_time`, `rate`, `option`, `bet_option`, `description`, `class`, `player_num`, `ticket_rate`) VALUES (1, './public/images/game_icon.jpg', '炸金花', 5000, 1, 5, 30, 100, '{\"1\":3,\"2\":3,\"3\":3}', '[10,100,1000,10000]', '炸金花的特长描述', 'Poker', 3, 100);
INSERT INTO `%DB_PREFIX%games`(`id`, `image`, `name`, `principal`, `is_effect`, `commission_rate`, `long_time`, `rate`, `option`, `bet_option`, `description`, `class`, `player_num`, `ticket_rate`) VALUES (2, './public/images/game_icon.jpg', '斗牛', 5000, 1, 1, 60, 100, '{\"1\":3,\"2\":3,\"3\":3}', '[10,100,1000,10000]', '斗牛描述', 'NiuNiu', 3, 100);

UPDATE `%DB_PREFIX%conf` SET `value`='3.1' WHERE (`name`='DB_VERSION');
