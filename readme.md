1. 运行`fanwe/update/update_games.sql`数据库文件（遇到错误继续）
2. 覆盖`fanwe/`文件夹至站点目录
3. 修改站点目录下`/home/wwwroot/fanwe/java/conf.txt`文件添加
  ```
  2,http://127.0.0.1/crontab_game.php,1,auth
  3,http://127.0.0.1/crontab_autogame.php,1,auth
  ```
  (说明：2为行号，127.0.0.1改为SLB域名)
4. 根据站点目录下`/home/wwwroot/fanwe/java/readme.txt`重启定时器
  具体命令如下
  ```shell
  cd /home/wwwroot/fanwe/java
  kill -9 `ps -ef | grep 'js.jar' | grep -v 'grep' | awk '{ print $2}'`
  nohup java -jar js.jar >/dev/null &
  ```
5. 根据站点目录下`/home/wwwroot/fanwe/system/define.php`配置文件添加所需配置
  ```php
  //是否开启游戏
  define('OPEN_GAME_MODULE',1);//是否开启游戏
  define('OPEN_DIAMOND_GAME_MODULE',1);//是否开启钻石游戏
  define('SHOW_IS_GAMING',1);//首页显示正在游戏中
  define('GAME_GAIN_FOR_ALERT',1);//游戏获胜弹幕
  define('GAME_AUTO_START',1);//自动游戏开关 
  ```


1. 變更目錄權限
chmod 777 -R /home/wwwroot/fanwe/public
chmod 777 -R /home/wwwroot/fanwe/install/Runtime
chmod 777 -R /home/wwwroot/fanwe/system/tim
chmod 777 -R /home/wwwroot/fanwe/update

2. 瀏覽器開啟 http://localhost/install 進行安裝資料