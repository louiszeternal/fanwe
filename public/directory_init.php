<?php

$distribution_cfg = array(

  "CACHE_CLIENT" => "127.0.0.1", //必填,redis链接
  "CACHE_PORT" => "6379", //必填（redis使用的端口，默认为6379）
  "CACHE_USERNAME" => "",
  "CACHE_PASSWORD" => "abc123", //必填 redis密码
  "CACHE_DB" => "",
  "CACHE_TABLE" => "",
  "SESSION_CLIENT" => "127.0.0.1", //必填,redis链接
  "SESSION_PORT" => "6379", //必填（redis使用的端口，默认为6379）
  "SESSION_USERNAME" => "",
  "SESSION_PASSWORD" => "abc123", //必填 redis密码
  "SESSION_DB" => "",
  "SESSION_TABLE" => "",
  "SESSION_FILE_PATH" => "", //session保存路径(为空表示web环境默认路径)
  "DB_CACHE_APP" => array(
    "index",
  ),

  "DB_CACHE_TABLES" => array(
    "admin",
  ), //支持查询缓存的表

  "DB_DISTRIBUTION" => array(
  ), //数据只读查询的分布

  "OSS_DOMAIN" => "http://zbimage.vingtaolive.com", //远程存储域名
  "OSS_DOMAIN_HTTPS" => "", //远程https存储域名 例如：https://osshttps.fanwe.net
  "OSS_FILE_DOMAIN" => "http://zbimage.vingtaolive.com", //远程存储文件域名(主要指脚本与样式)
  "OSS_BUCKET_NAME" => "jingpengzhibo", //针对阿里oss的bucket_name
  "OSS_ACCESS_ID" => "YCtWm0rmxi4PLtL5",
  "OSS_ACCESS_KEY" => "YmLmzZWI7YL4uHJuOUoyd1YCMCSJik",
  "OSS_ENDPOINT" => "oss-cn-hangzhou.aliyuncs.com", //OSS的endpoint
  "OSS_ENDPOINT_WITH_BUCKET_NAME" => false, //域名中是否已带有bucket属性，一般为三级域名将有此特性
  "OSS_INTERNAL_ENDPOINT" => "tfossnew.oss-cn-hangzhou-internal.aliyuncs.com",
);

//关于分布式配置

$distribution_cfg["CACHE_TYPE"] = "Rediscache"; //File,Memcached,MemcacheSASL,Xcache,Db,Rediscache
$distribution_cfg["CACHE_LOG"] = false; //是否需要在本地记录cache的key列表
$distribution_cfg["SESSION_TYPE"] = "Rediscache"; //"Db/MemcacheSASL/File,Rediscache"
$distribution_cfg['ALLOW_DB_DISTRIBUTE'] = false; //是否支持读写分离
$distribution_cfg['IS_JQ'] = true; //是否是集群状态
$distribution_cfg['JQ_URL'] = 'http://localhost'; //集群的网址 http://开头，不带斜杠结尾
$distribution_cfg["REDIS_PREFIX"] = "fanwe0000001:";
$distribution_cfg["REDIS_PREFIX_DB"] = 0; //选择redis的db，默认是0
$distribution_cfg["CSS_JS_OSS"] = true; //脚本样式是否同步到oss
$distribution_cfg["OSS_TYPE"] = "ALI_OSS"; //同步文件存储的类型: ES_FILE,ALI_OSS,NONE 分别为原es_file.php同步,阿里云OSS,以及无OSS分布
$distribution_cfg["LOCAL_IMAGE_URL"] = ""; //当同步文件存储的类型:NONE，开启第三方图片服务器时候 必须配置此参数，为图片服务器的域名 例如:http://ilvb.fanwe.net
$distribution_cfg["ORDER_DISTRIBUTE_COUNT"] = "5"; //订单表分片数量
$distribution_cfg['DOMAIN_ROOT'] = ''; //域名根
$distribution_cfg['COOKIE_PATH'] = '/';

return $distribution_cfg;

