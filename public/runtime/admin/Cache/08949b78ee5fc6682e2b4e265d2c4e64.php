<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html><html><head><title></title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><link rel="stylesheet" type="text/css" href="__TMPL__Common/layui/layui/css/layui.css" /><link rel="stylesheet" type="text/css" href="__TMPL__Common/style/style.css" /><script type="text/javascript">	var ACTION_ID ='<?php echo $action_id ?>';
 	var VAR_MODULE = "<?php echo conf("VAR_MODULE");?>";
	var VAR_ACTION = "<?php echo conf("VAR_ACTION");?>";
	var MODULE_NAME	=	'<?php echo MODULE_NAME; ?>';
	var ACTION_NAME	=	'<?php echo ACTION_NAME; ?>';
	var ROOT = '__APP__';
	var ROOT_PATH = '<?php echo APP_ROOT; ?>';
	var CURRENT_URL = '<?php echo trim($_SERVER['REQUEST_URI']);?>';
	var INPUT_KEY_PLEASE = "<?php echo (L("INPUT_KEY_PLEASE")); ?>";
	var TMPL = '__TMPL__';
	var APP_ROOT = '<?php echo APP_ROOT; ?>';
	var LOGINOUT_URL = '<?php echo u("Public/do_loginout");?>';
	var WEB_SESSION_ID = '<?php echo es_session::id(); ?>';
	var EMOT_URL = '<?php echo APP_ROOT; ?>/public/emoticons/';
	var MAX_FILE_SIZE = "<?php echo (app_conf("MAX_IMAGE_SIZE")/1000000)."MB"; ?>";
	var FILE_UPLOAD_URL ='<?php echo u("File/do_upload");?>' ;
	CHECK_DOG_HASH = '<?php $adm_session = es_session::get(md5(conf("AUTH_KEY"))); echo $adm_session["adm_dog_key"]; ?>';
	function check_dog_sender_fun()
	{
		window.clearInterval(check_dog_sender);
		check_dog2();
	}
	var check_dog_sender = window.setInterval("check_dog_sender_fun()",5000);
	
</script></head><body id="childrenBody" onLoad="javascript:DogPageLoad();"><div id="info"></div><link rel="stylesheet" type="text/css" href="__TMPL__Common/style/weebox.css" /><link rel="stylesheet" type="text/css" href="__TMPL__Common/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__TMPL__Common/css/fileinput.min.css" /><style>	.line {
		min-height: 20px;
		font-size: 13px;
		border-bottom: 1px solid silver;
		padding: 5px 0px;
	}
	.delete {
		cursor: pointer;
		padding: 0px 5px;
		text-decoration: underline;
		color: red;
	}
	.uploaderMsgBox {
		width: 100%;
		border-bottom: 1px solid #888;
	}
	[act=cancel-upload]{
		text-decoration: none;
		cursor:pointer;
	}
</style><div class="main"><div class="main_title"><?php echo (L("ADD")); ?><a href="<?php echo u("MenYanList/index");?>" class="back_list"><?php echo (L("BACK_LIST")); ?></a></div><div class="blank5"></div><form name="edit" action="__APP__" method="post" enctype="multipart/form-data"><table class="form" cellpadding=0 cellspacing=0><tr><td colspan=2 class="topTd"></td></tr><tr><td class="item_title">名称:</td><td class="item_input"><input type="text" class="require" name="name" /></td></tr><tr><td class="item_title">贴纸压缩包名称:</td><td class="item_input"><input type="text" class="require" name="file_name" /><span class="tip_span" id="tip_span">压缩包名称，不要带后缀名,请确保与压缩包内的文件夹名称一致，建议不要使用中文命名</span></td></tr><tr><td class="item_title">展示图片:</td><td class="item_input"><span><div style='float:left; height:35px; padding-top:1px;'><input type='hidden' value='' name='image' id='keimg_h_image' /><div class='buttonActive' style='margin-right:5px;'><div class='buttonContent'><button type='button' class='keimg ke-icon-upload_image' rel='image'>选择图片</button></div></div></div><a href='./admin/Tpl/default/Common/images/no_pic.gif' target='_blank' id='keimg_a_image' ><img src='./admin/Tpl/default/Common/images/no_pic.gif' id='keimg_m_image' width=35 height=35 style='float:left; border:#ccc solid 1px; margin-left:5px;' /></a><div style='float:left; height:35px; padding-top:1px;'><div class='buttonActive'><div class='buttonContent'><img src='/admin/Tpl/default/Common/images/del.gif' style='display:none; margin-left:10px; float:left; border:#ccc solid 1px; width:35px; height:35px; cursor:pointer;' class='keimg_d' rel='image' title='删除'></div></div></div></span></td></tr><tr><td class="item_title">添加文件:</td><td class="item_input"><div class="row"><div class="col-md-6"><div id="errorBlock" class="help-block"></div><input type="file" class="file" id="test-upload" accept="file"/></div><span class="tip_span" id="tip_span">请上传贴纸压缩包</span></div></td></tr><tr id="file_url"><td class="item_title">贴纸压缩包下载地址:</td><td><input class="require" type="text" name="download_link" id="download_link" style="width: 600px;" /><span class="tip_span" id="tip_span">如果上传失败，请使用 OSS 工具上传后，手动填写下载地址</span></td></tr><tr><td class="item_title"><?php echo (L("IS_EFFECT")); ?>:</td><td class="item_input"><label><?php echo (L("IS_EFFECT_1")); ?><input type="radio" name="is_effect" value="1" checked="checked" /></label><label><?php echo (L("IS_EFFECT_0")); ?><input type="radio" name="is_effect" value="0" /></label></td></tr><tr><td class="item_title"><?php echo (L("SORT")); ?>:</td><td class="item_input"><input type="text" class="" name="sort" value="<?php echo ($new_sort); ?>" /></td></tr><tr><td class="item_title"></td><td class="item_input"><!--隐藏元素--><input type="hidden" name="<?php echo conf("VAR_MODULE");?>" value="MenYanList" /><input type="hidden" name="<?php echo conf("VAR_ACTION");?>" value="insert" /><!--隐藏元素--><input type="submit" class="button" value="<?php echo (L("ADD")); ?>" /><input type="reset" class="button" value="<?php echo (L("RESET")); ?>" /></td></tr><tr><td colspan=2 class="bottomTd"></td></tr></table></form></div><script type="text/javascript" src="__TMPL__Common/layui/layui/layui.js"></script><script type="text/javascript" src="__TMPL__Common/js/check_dog.js"></script><script type="text/javascript" src="__TMPL__Common/js/IA300ClientJavascript.js"></script><script type="text/javascript" src="__TMPL__Common/js/cropper/jquery.min.js"></script><script type="text/javascript" src="__TMPL__Common/js/jquery.timer.js"></script><link rel="stylesheet" type="text/css" href="__TMPL__Common/style/weebox.css" /><script type="text/javascript" src="__TMPL__Common/js/jquery.bgiframe.js"></script><script type="text/javascript" src="__TMPL__Common/js/jquery.weebox.js"></script><link rel="stylesheet" type="text/css" href="__TMPL__Common/js/cropper/cropper.min.css" /><link rel="stylesheet" type="text/css" href="__TMPL__Common/js/cropper/main.css" /><script type="text/javascript" src="__TMPL__Common/js/cropper/cropper.min.js"></script><script type="text/javascript" src="__TMPL__Common/js/script.js"></script><script type="text/javascript" src="__ROOT__/public/runtime/admin/lang.js"></script><script type='text/javascript'  src='__ROOT__/admin/public/kindeditor/kindeditor.js'></script><script type='text/javascript'  src='__ROOT__/admin/public/kindeditor/lang/zh_CN.js'></script><script>    function insert_video_oss(){
        var form_data = $("#video_form").serialize();
        $.ajax({
            url:ROOT+"?"+VAR_MODULE+"="+MODULE_NAME+"&"+VAR_ACTION+"=upload_oss",
            data:form_data,
            dataType:"json",
            type:"post",
            success:function(result){
                if (result.status == '1') {
                    alert(result.error);
                    location.href = ROOT+"?"+VAR_MODULE+"="+MODULE_NAME+"&"+VAR_ACTION+"=playback_index";
                }else{
                    alert(result.error);
                }
            }
        });
    }
</script><script src="//qzonestyle.gtimg.cn/open/qcloud/js/vod/sdk/uploaderh5.js" charset="utf-8"></script><script type="text/javascript" src="__TMPL__Common/js/user_live.js"></script><script type="text/javascript" src="__TMPL__Common/js/jquery.weebox.js"></script><script type="text/javascript" src="__TMPL__Common/js/fileinput.min.js"></script><script type="text/javascript" src="__TMPL__Common/js/locales/zh.js"></script><script type="text/javascript">    $("#test-upload").fileinput({
        uploadUrl: "<?php echo u('PublicFile/ZipUpload',array('dir'=>'file'));?>",
        language: 'zh',
        showPreview: false,
        allowedFileTypes: ['zip'],
        elErrorContainer: '#errorBlock'
    }).on('fileuploaded', function(event, data) {
        if (data.response.fullname) {
            $('#download_link').val(data.response.fullname);
        } else {
            $('#errorBlock').html(data.response.message).show();
        }
    });
</script></body></html>