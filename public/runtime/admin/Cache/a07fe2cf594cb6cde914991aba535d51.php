<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html><html class="loginHtml"><head><meta charset="utf-8"><title><?php echo app_conf("SITE_NAME");?> - <?php echo l("SYSTEM_LOGIN");?></title><meta name="renderer" content="webkit"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><meta name="apple-mobile-web-app-status-bar-style" content="black"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="format-detection" content="telephone=no"><link rel="icon" href="./favicon.ico"><link rel="stylesheet" href="__TMPL__Common/layui/layui/css/layui.css" media="all" /><link rel="stylesheet" href="__TMPL__Common/layui/css/public.css" media="all" /><link rel="stylesheet" type="text/css" href="__TMPL__Common/style/login.css" /><script type="text/javascript">		//定义JS语言
		var ADM_NAME_EMPTY = '<?php echo l("ADM_NAME_EMPTY");?>';
		var ADM_PASSWORD_EMPTY = '<?php echo l("ADM_PASSWORD_EMPTY");?>';
		var ADM_VERIFY_EMPTY = '<?php echo l("ADM_VERIFY_EMPTY");?>';
		var L_jumpUrl = '<?php echo U("Index/index");?>';
		var ROOT = '__APP__';
		var __LOGIN_KEY = "<?php echo LOGIN_DES_KEY();?>";
		var open_check_account='<?php echo ($open_check_account); ?>';
	</script></head><body class="loginBody" onLoad="javascript:DogPageLoad();"><form class="layui-form" action="<?php echo u("Public/do_login");?>" ><div class="login_face"><img src="<?php echo ($app_logo); ?>" class="userAvatar"></div><div class="layui-form-item input-item"><label for="userName">用户名</label><input type="text" name="adm_name" placeholder="请输入用户名" autocomplete="off" id="userName" class="layui-input adm_name" lay-verify="required"></div><div class="layui-form-item input-item" id="paddwordBox"><label for="password">密码</label><input type="password" name="adm_password" placeholder="请输入密码" autocomplete="off" id="password" class="layui-input adm_password" lay-verify="required"></div><div class="layui-form-item input-item" id="CHECK_DOG_BOX" style="display:none;"></div><?php if($open_check_account == 1): ?><div class="layui-form-item input-item"><label for="mobile">绑定手机号</label><input type="text" name="mobile" placeholder="请输入用户名" autocomplete="off" id="mobile" class="layui-input mobile" lay-verify="required"></div><div class="layui-form-item input-item" id="mobileCode"><label for="mobile_verify">短信验证码</label><input type="text" name="mobile_verify" placeholder="请输入验证码" autocomplete="off" id="mobile_verify" class="layui-input sms_verify mobile_verify" lay-verify="required"><span type="button" class="button_smsVerify" id="smsVerify">获取验证码</span></div><?php endif; ?><div class="layui-form-item input-item" id="imgCode"><label for="code">验证码</label><input type="text" name="adm_verify" placeholder="请输入验证码" autocomplete="off" id="code" class="layui-input adm_verify" lay-verify="required"><img src="__ROOT__/verify.php?name=verify" alt="__ROOT__/verify.php?name=verify"  id="verify" align="absmiddle" width="116" height="36"/></div><div class="layui-form-item"><input type="hidden" name="is_check_account" value="<?php echo ($open_check_account); ?>" ><button class="layui-btn layui-block" lay-filter="login" id="login_btn" lay-submit>登录</button></div></form><script type="text/javascript" src="__TMPL__Common/layui/layui/layui.js"></script><script type="text/javascript" src="__TMPL__Common/layui/js/login.js"></script><script type="text/javascript" src="__TMPL__Common/layui/js/cache.js"></script><script type="text/javascript" src="__TMPL__Common/js/des.js"></script><script type="text/javascript" src="__TMPL__Common/js/jquery.js"></script><script type="text/javascript" src="__TMPL__Common/js/jquery.timer.js"></script><script type="text/javascript" src="__TMPL__Common/js/login.js"></script><script type="text/javascript" src="__TMPL__Common/js/check_dog.js"></script><script type="text/javascript" src="__TMPL__Common/js/IA300ClientJavascript.js"></script><script type="text/javascript">	function resetwindow()
	{
		if(top.location != self.location)
		{
			top.location.href = self.location.href;
			return
		}
	}

	$(document).ready(function () {
		resetwindow();
		if(CHECK_DOG){
			$("#CHECK_DOG_BOX").show();

			var form_heigth=$(".layui-form").height();
			var html= '\t\t<label for="adm_dog_key">USBKEY</label>\n' +
					'\t\t<input type="password" name="adm_dog_key" placeholder="请输入USBKEY" autocomplete="off" id="adm_dog_key" class="layui-input adm_dog_key" lay-verify="required">\n' +
					'\t</div>';
			$("#CHECK_DOG_BOX").append(html);
			$(".layui-form").height(form_heigth+53);
		}

		if(open_check_account =='1'){
			var form_heigth=$(".layui-form").height();
			$(".layui-form").height(form_heigth+106);
		}

	});
</script></body></html>