一、结构说明
1.1系统目录结构说明： 
````
www  WEB部署目录（或者子目录）
├─admin     		后台管理目录
├─callback        支付回调目录
├─install			系统安装目录
├─mapi				api接口目录
├─public			公共文件目录
├─system			系统方法及组件
 ├─tool				OSS目录
 ├─update			系统升级目录
 ├─wap			    H5模板文件目录
 ├─wxpay_web		微信支付回调目录
├─admin.php       后台入口
├─appdown.php     APP下载文件
├─crontab.php     定时器文件
├─imcallback.php  腾讯云回调文件
├─index.php       入口文件
├─license.php     授权文件
├─m.php       		后台入口跳转文件
├─verify.php      验证码文件

1.2 API 接口目录说明：
├─mapi API系统目录
│  ├─lib         类库目录
│  │  ├─core     核心类库包目录
│  │  │  ├─ common.php				公共函数类库
│  │  │  ├─ Connect.class.php		异步函数类库
│  │  │  ├─ Core.class.php			报错方法函数
│  │  │  ├─ mapi_function.php		mapi目录功能函数
│  │  │  ├─ Model.class.php			db函数类库
│  │  │  ├─ push.php			       友盟推送函数
│  │  │  ├─ shorturl.php			    短链接函数
│  │  │  ├─ transport.php			    异步函数类库
│  │  │  ├─ video_factory.php		 腾讯云API函数
 │  │  ├─models    						 redis类库目录
│  │  │  ├─ userModel.class.php		 基础类库
│  │  │  ├─ videoModel.class.php		 关注类库
│  │  ├─redis    						 				redis类库目录
│  │  │  ├─ BaseRedisService.php						基础类库
│  │  │  ├─ UserFollwRedisService.php				关注类库
│  │  │  ├─ UserRedisService.php						用户信息类库
│  │  │  ├─ VideoContributionRedisService.php		第三方类库目录
│  │  │  ├─ VideoGiftRedisService.php				礼物类库
│  │  │  ├─ VideoPrivateRedisService.php			私密直播类库
│  │  │  ├─ VideoRedisService.php					视频类库
│  │  │  ├─ VideoRedRedisService.php 				红包类库
│  │  │  ├─ VideoViewerRedisService.php				观众类库
│  │  ├─ app.action.php   			初始化类库
│  │  ├─ app_download.action.php  APP下载类库
│  │  ├─ avatar.action.php		 	图片上传类库
│  │  ├─ base.action.php 			redis类库
│  │  ├─ deal.action.php			礼物类库
│  │  ├─ family.action.php			家族类库
│  │  ├─ family_user.action.php	家族会员类库
│  │  ├─ index.action.php			礼物类库
│  │  ├─ login.action.php			登录类库
│  │  ├─ music.action.php     	音乐类库
│  │  ├─ pay.action.php        	支付类库
│  │  ├─ rank.action.php        	排行榜类库
│  │  ├─ settings.action.php      用户中心设置类库
│  │  ├─ share.action.php      	 分享类库
│  │  ├─ user_center.action.php   用户中心类库
│  │  ├─ user.action.php      	 用户处理类库
│  │  ├─ Video.action.php   		 视频处理类库
│  │  ├─ wx_bind.action.php		 微信公众绑定类库
│  └─index.php    API入口文件 
````







